import { fromJS } from 'immutable';
import { appendData, register, removeData } from 'reducers/helpers';
import * as cookie from 'utils/shims/cookie';

const { beforeFetch, setData } = register('metadata');

const defaultState = fromJS({
  config: {
    data: {},
    status: 'SUCCESS',
  },
});

export const setPartnerConfig = (partnerConfig = {}) => {
  return {
    type: 'SET_PARTNER_CONFIG',
    partnerConfig,
  };
};


export default function MetadataReducer(state = defaultState, action) {
  const { data, extras, error } = action;
  switch (action.type) {
    case 'FETCH_MORE_ORDERS_HISTORY':
      return appendData(state, 'orders', action.data, null, extras);
    case 'FETCH_ORDERS_HISTORY_PENDING':
      return beforeFetch(state, 'orders', true);
    case 'FETCH_ORDERS_HISTORY':
      return setData(state, 'orders', action.data, null, extras);

    case 'SET_PARTNER_CONFIG':
      return setData(state, 'partnerConfig', action.partnerConfig);

    case 'ACCESSCONTROL_AUTHEN_FAILED':
      if (error.status !== 401) return state;
      return defaultState;
    case 'ACCESSCONTROL_AUTHEN': {
      let cfgDataSvr = state.getIn(['config', 'data']);
      cfgDataSvr = cfgDataSvr.set('currentUser', data.username);
      cookie.set('accessToken', action.accessToken);
      return state.setIn(['config', 'data'], cfgDataSvr);
    }
    case 'ACCESSCONTROL_LOGOUT': {
      let newState = removeData(state, 'userExtraInfo');
      newState = removeData(newState, 'orders');
      return newState.setIn(['config', 'data', 'currentUser'], null);
    }
    default:
      return state;
  }
}

export function getConfigs(state) {
  return state.metadata.get('config') || new Map();
}

export function getCurrentUsername(state) {
  const configs = getConfigs(state);
  return configs.getIn(['data', 'currentUser']);
}

export function getCurrentUserOrders(state) {
  return state.metadata.get('orders');
}

export function getPartnerConfig(state) {
  return state.metadata.get('partnerConfig');
}
