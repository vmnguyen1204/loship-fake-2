import { Map } from 'immutable';
import { defaultState, getKey, register } from 'reducers/helpers';

const { setData } = register('coreCategory');

const parseData = (data) => {
  return {
    id: data.id,
    value: data.value,
    imageWeb: data.imageWeb,
    slug: data.slug,
    type: data.type,
  };
};

export default (state = defaultState, action) => {
  const { data, key, ref } = action;
  switch (action.type) {
    case 'CORE_CATEGORY_APPEND':
      return setData(state, data.slug, parseData(data), ref);
    case 'CORE_CATEGORY_FETCH_ALL': {
      if (!data) return state;

      let newState = state;
      const dataIds = data.map((category) => {
        newState = setData(newState, category.slug, parseData(category), category.id);
        return category.id;
      });
      return setData(newState, key, dataIds);
    }
    default:
      return state;
  }
};

export function getCoreCategories(state) {
  const categoryIds = state.coreCategory.getIn(['all', 'data']);
  if (!categoryIds) return;

  return new Map({
    status: 'SUCCESS',
    data: categoryIds
      .map((id) => state.coreCategory.get(getKey(state.coreCategory, id)))
      .map((category) => category?.toJS().data)
      .filter(Boolean),
  });
}

export function getCoreCategoriesByIdOrSlugs(state, { slugs, ids }) {
  const idOrSlugs = ids || slugs;

  if (!Array.isArray(idOrSlugs)) return [];
  const categories = idOrSlugs
    .map((idOrSlug) => {
      const category = state.coreCategory.get(getKey(state.coreCategory, idOrSlug));
      return category || idOrSlug;
    })
    .map((category) => {
      if (typeof category.toJS !== 'function') return;
      return category.toJS().data;
    })
    .filter(Boolean);
  return categories;
}
