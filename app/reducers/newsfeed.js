import produce from 'immer';
import { appendData, defaultState, register } from 'reducers/helpers';
import { getCurrentCity } from 'reducers/new/metadata';
import { APP_INFO } from 'settings/variables';
import { getDistrictBySlug } from 'utils/data';
import { iife } from 'utils/helper';
import { getCategoriesByIdOrSlugs } from './category';

const { beforeFetch, setError, setData } = register('newsfeed');

export function parseQuery(query = {}, state) {
  const currentCity = getCurrentCity(state, { globalAddress: query.globalAddress });

  const parsedQuery = produce(query, (draft) => {
    if (!draft.superCategoryId) draft.superCategoryId = APP_INFO.shipServiceId;

    if (!draft.cityId && currentCity) {
      if (currentCity.type === 'district') {
        draft.districtIds = [currentCity.district.id];
        draft.cityId = currentCity.district.city.id;
      } else {
        draft.cityId = currentCity.city.id;
      }
    }

    if (draft.districts?.length > 0) {
      draft.districtIds = draft.districts
        .map((slug) => getDistrictBySlug(slug)?.id)
        .filter((id) => !!id)
        .join(',');
    }

    if (draft.chainId) {
      delete draft.superCategoryId;
      delete draft.cityId;
      delete draft.districtIds;
    }

    draft.categories = [];
    if (draft.dishGroupIds) draft.categories.push(...draft.dishGroupIds);
    else if (draft.dishGroupSlugs) {
      const categories = getCategoriesByIdOrSlugs(state, { slugs: draft.dishGroupSlugs });
      draft.categories.push(...categories.map((category) => category.id));
    }
    if (draft.themeIds) draft.categories.push(...draft.themeIds);
    else if (draft.themeSlugs) {
      const categories = getCategoriesByIdOrSlugs(state, { slugs: draft.themeSlugs });
      draft.categories.push(...categories.map((category) => category.id));
    }

    if (draft.categories.length === 0) delete draft.categories;
    else draft.categories = draft.categories.join(',');

    if (draft.q) draft.q = draft.q.trim();

    // Delete unexpected query params
    if (draft.q === '') delete draft.q;
    delete draft.dishGroupSlugs;
    delete draft.dishGroup;
    delete draft.themeSlugs;
    delete draft.theme;

    delete draft.districts;
    delete draft.district;

    if (draft.ignoreCity) {
      delete draft.ignoreCity;
      delete draft.cityId;
      delete draft.districtId;
    }

    delete draft.username;
    delete draft.slug;
    delete draft.shouldShowSortFilter;
  });

  return Object.entries(parsedQuery)
    .sort()
    .reduce(
      (_sortedObj, [k, v]) => ({
        ..._sortedObj,
        [k]: v,
      }),
      {},
    );
}

export function getKey(query) {
  const entries = Object.entries(query);
  entries.sort((a, b) => a[0].localeCompare(b[0]));

  return entries
    .map(([key, value]) => {
      if (key === 'superCategoryId') return '';
      if (key === 'page' && (!value || value === 1)) return '';
      if (key === 'globalAddress') return 'globalAddress';

      if (Array.isArray(value)) return value.length > 0 ? `${key}_${value.join('-')}` : '';
      return value ? `${key}_${value}` : '';
    })
    .filter((v) => !!v)
    .join('-')
    .replace(/ /g, '_');
}

export default function SearchReducer(state = defaultState, action) {
  const { key, data, extras } = action;
  switch (action.type) {
    case 'NEWSFEED_FETCH_PENDING':
    case 'NEWSFEED_FETCH_MORE_PENDING':
    case 'CHAIN_DETAIL_FETCH_PENDING':
    case 'CHAIN_DETAIL_FETCH_MORE_PENDING':
    case 'BANNER_FETCH_ALL_PENDING':
    case 'BANNER_EVENT_FETCH_PENDING':
    case 'EATERY_RATING_FETCH_PENDING':
    case 'NEWSFEED_SUGGEST_DISHES_PENDING':
    case 'NEWSFEED_SUGGEST_EATERIES_PENDING':
      return beforeFetch(state, key, action.force);

    case 'NEWSFEED_FETCH_FAILED':
    case 'NEWSFEED_FETCH_MORE_FAILED':
    case 'CHAIN_DETAIL_FETCH_FAILED':
    case 'CHAIN_DETAIL_FETCH_MORE_FAILED':
    case 'BANNER_FETCH_ALL_FAILED':
    case 'BANNER_EVENT_FETCH_FAILED':
    case 'EATERY_RATING_FETCH_FAILED':
    case 'NEWSFEED_SUGGEST_DISHES_FAILED':
    case 'NEWSFEED_SUGGEST_EATERIES_FAILED':
      return setError(state, key);

    case 'NEWSFEED_FETCH':
    case 'CHAIN_DETAIL_FETCH':
    case 'BANNER_FETCH_ALL':
    case 'NEWSFEED_SUGGEST_DISHES':
    case 'NEWSFEED_SUGGEST_EATERIES':
    case 'BANNER_EVENT_FETCH':
    case 'EATERY_RATING_FETCH': {
      return setData(state, key, data, null, extras);
    }
    case 'NEWSFEED_FETCH_MORE':
    case 'CHAIN_DETAIL_FETCH_MORE': {
      return appendData(state, key, data, null, extras);
    }

    case 'MERCHANT_DISLIKE': {
      let newData = state.getIn([action.currentKey, 'data']);
      if (!newData) return state;

      newData = newData.filter((merchant) => merchant.id !== action.eateryId);
      return state.setIn([action.currentKey, 'data'], newData);
    }

    case 'EATERY_RATING_UPDATE_PENDING':
    case 'EATERY_RATING_UPDATE_FAILED': {
      state = iife(() => {
        let newData = state.getIn([action.currentKey, 'data']);
        if (!newData) return state;

        const ratingIndex = newData.findIndex((rating) => rating.id === action.key);
        if (ratingIndex < 0) return state;

        const rating = newData.get(ratingIndex);
        if (rating.liked) {
          rating.liked = false;
          rating.count && rating.count.like--;
        } else {
          rating.liked = true;
          rating.count && rating.count.like++;
        }

        newData = newData.splice(ratingIndex, 1, rating);
        return state.setIn([action.currentKey, 'data'], newData);
      });
      state = iife(() => {
        const ratingKey = `${key}.eateryRating`;
        const rating = state.getIn([ratingKey, 'data'])?.toJS();
        if (!rating) return state;

        if (rating.liked) {
          rating.liked = false;
          rating.count && rating.count.like--;
        } else {
          rating.liked = true;
          rating.count && rating.count.like++;
        }
        return setData(state, ratingKey, rating);
      });

      return state;
    }

    default:
      return state;
  }
}

export function getNewsFeed(state, params) {
  const query = parseQuery(params, state);
  const key = getKey(query);
  return state.newsfeed.get(key);
}

export function getChainDetail(state, params) {
  const query = parseQuery(params, state);
  return state.newsfeed.get('chain.' + getKey({ ...query, globalAddress: {} })) ||
    state.newsfeed.get('chain.' + getKey(query));
}

export function getBanners(state) {
  const currentCity = getCurrentCity(state);
  if (!currentCity) return;

  const cityId = currentCity.type === 'city' ? currentCity.city.id : currentCity.district.city.id;
  return state.newsfeed.get(`banners.c_${cityId}`);
}

export function getEventById(state, params) {
  if (!params.eventId) return;
  return state.newsfeed.get(`banners.event.${params.eventId}`);
}

export function getSuggestDishes(state) {
  return state.newsfeed.get('newsfeed.suggestDishes');
}

export function getSuggestEateries(state, params) {
  if (!params.eateryId) return;
  const query = parseQuery(params, state);
  return state.newsfeed.get(getKey({ ...query, globalAddress: {} })) ||
    state.newsfeed.get(getKey(query));
}

export function getEateryRatingById(state, params) {
  if (!params.ratingId) return;
  return state.newsfeed.get(`${params.ratingId}.eateryRating`);
}
