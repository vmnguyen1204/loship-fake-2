import { appendData, defaultState, register } from 'reducers/helpers';

const parseData = (data) => {
  const newData = data.map((val) => {
    if (val.sources[0].type === 'youtube') {
      const regex = /embed\/(.*?)\?autoplay/;
      const result = regex.exec(val.sources[0].url);
      return { ...val, videoId: result[1] };
    }
    return { ...val };
  });
  return newData;
};

const { beforeFetch, setData } = register('discover');

export default function DiscoverReducer(state = defaultState, action) {
  const { data, key, extras } = action;
  switch (action.type) {
    case 'DISCOVER_FETCH_VIDEO_PENDING':
      return beforeFetch(state, key);
    case 'DISCOVER_FETCH_VIDEO':
      return setData(state, key, parseData(data), null, extras);
    case 'DISCOVER_FETCH_VIDEO_FAILED':
      return state.setIn([key, 'status'], 'FAILED');
    case 'DISCOVER_FETCH_TOP_PENDING':
      return beforeFetch(state, key);
    case 'DISCOVER_FETCH_TOP_VIDEO':
      return setData(state, key, parseData(data));
    case 'DISCOVER_FETCH_TOP_VIDEO_FAILED':
      return state.setIn([key, 'status'], 'FAILED');
    case 'FETCH_VIDEO_MORE_PEDDING':
      return beforeFetch(state, key);
    case 'FETCH_VIDEO_MORE':
      return appendData(state, key, parseData(data), null, extras);
    case 'FETCH_VIDEO_MORE_FAILED':
      return state.setIn([key, 'status'], 'FAILED');
    default:
      return state;
  }
}

export function getVideoMerchant(state) {
  return state.discover.get('all');
}

export function getTopVideoMerchant(state) {
  return state.discover.get('top');
}
