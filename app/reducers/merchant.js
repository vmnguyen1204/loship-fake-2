import { Map } from 'immutable';
import { appendData, defaultState, getKey, register, setError } from 'reducers/helpers';
import { shuffleArray } from 'utils/format';

const { beforeFetch, setData } = register('merchant');

export const parseData = (data) => {
  const result = {
    id: data.id,
    avatar: data.avatar,
    name: data.name,
    photos: data.photos,
    categories: data.categories,
    orderCount: data.orderCount,
    minimumShippingFee: data.minimumShippingFee,
    shippingFee: data.shippingFee,
    currentShippingFee: data.currentShippingFee >= 0 ? data.currentShippingFee : data.shippingFee,
    rating: data.rating,
    nonBuyable: (data.count && data.count.nonBuyable) || 0,
    buyable: (data.count && data.count.buyable) || 0,
    operatingTime: data.operatingTime,
    operatingStatus: data.operatingStatus,
    comment: (data.count && data.count.comment) || 0,
    address: data.address && data.address.full,
    slug: data.slug,
    lat: data.latitude,
    lng: data.longitude,
    cityId: data.cityId,
    city: data.address && data.address.city,
    district: data.address && data.address.district,
    street: data.address && data.address.street,
    ownedBy: data.ownedBy,
    missingData: data.missingData,
    promotions: data.promotions,
    isManager: data.isManager,
    username: data.username,
    brand: data.brand,
    extraFees: data.extraFees,
    isHonored: data.isHonored,
    paymentFeeMethods: data.paymentFeeMethods,
    closed: data.closed,
    freeShippingMilestone: data.freeShippingMilestone,
    recommendedEnable: data.recommendedEnable,
    recommendedRatio: data.recommendedRatio,
    promotionCampaigns: data.promotionCampaigns,
    count: data.count,
    distance: data.distance,
    isFavorite: data.isFavorite,
    eateryChain: data.eateryChain,
    supplyGroupDishes: data.supplyGroupDishes,
  };

  if (typeof data.isActive !== 'undefined') {
    result.isActive = data.isActive;
  } else {
    result.isActive = true;
  }

  if (typeof data.isCheckedIn !== 'undefined') {
    result.isCheckedIn = data.isCheckedIn;
  } else {
    result.isCheckedIn = true;
  }

  if (typeof data.isLoshipPartner !== 'undefined') result.isLoshipPartner = data.isLoshipPartner;
  if (typeof data.shippingService !== 'undefined') result.shippingService = data.shippingService.name;
  return result;
};

export default function MerchantReducer(state = defaultState, action) {
  const { data, key, extras } = action;
  switch (action.type) {
    case 'MERCHANT_FETCH_PENDING':
      return beforeFetch(state, key);
    case 'MERCHANT_APPEND':
      data.missingData = true;
      return setData(state, data.slug, parseData(data), data.id);
    case 'MERCHANT_FETCH':
      data.missingData = false;
      return setData(state, data.slug, parseData(data), [data.id, data.username]);
    case 'MERCHANT_FETCH_FAILED':
      return setError(state, key);

    case 'MERCHANT_MANAGER_FETCH_PENDING':
      return beforeFetch(state, key + '.manager');
    case 'MERCHANT_MANAGER_FETCH':
      return setData(state, data.slug + '.manager', data, [data.id + '.manager', data.username + '.manager']);
    case 'MERCHANT_MANAGER_FETCH_FAILED':
      return setError(state, key + '.manager');

    case 'MERCHANT_FETCH_RATINGS_PENDING':
      return beforeFetch(state, key + '.ratings', true);
    case 'MERCHANT_FETCH_RATINGS':
      return setData(state, key + '.ratings', data, null, extras);
    case 'MERCHANT_FETCH_MORE_RATINGS':
      return appendData(state, key + '.ratings', data, null, extras);
    case 'MERCHANT_FETCH_RATINGS_FAILED':
      return setError(state, key + '.ratings');

    case 'MERCHANT_FETCH_BLOCK_IMAGES_PENDING':
      return beforeFetch(state, key);
    case 'MERCHANT_FETCH_BLOCK_IMAGES': {
      let newState = state.setIn([key, 'data'], shuffleArray(data));
      newState = newState.setIn([key, 'status'], 'SUCCESS');
      return newState;
    }

    case 'FETCH_MERCHANT_LOSHIP_BY_ID':
      return setData(state, key, data);

    case 'MERCHANT_ALLOW_PRE_ORDER': {
      let newData = state.getIn([key, 'data']);
      if (!newData) return state;

      newData = newData.set('isAllowPreOrder', true);
      return state.setIn([key, 'data'], newData);
    }

    case 'MERCHANT_PROMOTIONS_FETCH_PENDING':
    case 'MERCHANT_PROMOTION_SEARCH_PENDING':
    case 'MERCHANT_PROMOTION_FETCH_PENDING':
    case 'MERCHANT_CAMPAIGNS_FETCH_PENDING':
    case 'MERCHANT_CAMPAIGN_FETCH_PENDING':
    case 'MERCHANT_THOUGHTFUL_PENDING':
      return beforeFetch(state, key, true);
    case 'MERCHANT_PROMOTIONS_FETCH_FAILED':
    case 'MERCHANT_THOUGHTFUL_FAILED':
    case 'MERCHANT_PROMOTION_SEARCH_FAILED':
    case 'MERCHANT_PROMOTION_FETCH_FAILED':
    case 'MERCHANT_CAMPAIGNS_FETCH_FAILED':
    case 'MERCHANT_CAMPAIGN_FETCH_FAILED':
      return setError(state, key);
    case 'MERCHANT_PROMOTION_FETCH':
    case 'MERCHANT_THOUGHTFUL_FETCH':
    case 'MERCHANT_PROMOTION_SEARCH':
    case 'MERCHANT_CAMPAIGN_FETCH':
      return setData(state, key, data);
    case 'MERCHANT_PROMOTIONS_FETCH':
    case 'MERCHANT_CAMPAIGNS_FETCH': {
      let newState = state;
      const listData = data.map((promo) => {
        const promoKey = (promo.code || promo.id) + action.childKeyPostfix;
        newState = setData(newState, promoKey, { ...promo, missingData: true });
        return promoKey;
      });
      return action.isAppend
        ? appendData(state, key, listData, null, extras)
        : setData(newState, key, listData, null, extras);
    }
    case 'MERCHANT_PROMOTION_SEARCH_CLEAR':
      return state.delete('promo.search');

    case 'MERCHANT_LIKE':
    case 'MERCHANT_DISLIKE': {
      let newData = state.getIn([key, 'data']);
      if (!newData) return state;

      newData = newData.set('isFavorite', !newData.get('isFavorite'));
      return setData(state, key, newData);
    }

    default:
      return state;
  }
}

export function getMerchant(state, params) {
  const username = params.merchantUsername;
  const merchantKey = getKey(state.merchant, params.merchant || username || params.eateryId);

  return state.merchant.get(merchantKey);
}

export function getMerchantSupplied(state, params) {
  const id = params.merchantUsername;
  const merchantKey = getKey(state.merchant, params.merchant || id || params.eateryId);

  return state.merchant.get(merchantKey + '.supplied');
}

export function getMerchantManager(state, params) {
  const username = params.merchantUsername;
  const merchantKey = getKey(state.merchant, params.merchant || username || params.eateryId) + '.manager';

  return state.merchant.get(merchantKey);
}

export function getBlockImages(state, params) {
  const username = params.merchantUsername;
  const merchantKey = getKey(state.merchant, params.merchant || username);
  return state.merchant.get(`${merchantKey}.blockImages`);
}

export function getMerchants(state, params) {
  const merchantSlug = params.merchants.map((merchant) => getKey(state.merchant, merchant));
  if (!merchantSlug) return;
  const data = merchantSlug.map((slug) => state.merchant.getIn([slug, 'data']).toJS());
  return new Map({ data });
}

export function getMerchantLoship(state, params) {
  const merchantKey = `${params.merchant}.loship`;
  return state.merchant.get(merchantKey);
}

export function getWhitelistMerchants(state) {
  return state.merchant.get('whitelist');
}

export function getMerchantKey(state, params) {
  const username = params.merchantUsername;
  const merchantKey = getKey(state.merchant, params.merchant || username);

  const merchant = state.merchant.get(merchantKey);
  return typeof merchant === 'string' ? merchant : merchantKey;
}

export function getMerchantRatings(state, params) {
  const username = params.merchantUsername;
  const merchantKey = getKey(state.merchant, params.merchant || username || params.eateryId) + '.ratings';
  return state.merchant.get(merchantKey);
}

export function getPromotionByCode(state, params) {
  if (!params.promotionCode) return;
  return state.merchant.get(`${params.promotionCode}.promo`);
}

export function getSearchedPromotion(state) {
  return state.merchant.get('promo.search');
}

export function getPromotionListByEateryId(state, params) {
  if (!params.eateryId) return;
  let res = state.merchant.get(`${params.eateryId}.promo`);
  if (!res || !res.get('data')) return res;

  res = res.set('data', res.get('data').map((promoKey) => state.merchant.getIn([promoKey, 'data'])));
  return res;
}

export function getCampaignById(state, params) {
  if (!params.campaignId) return;
  return state.merchant.get(`${params.campaignId}.campaign`);
}

export function getCampaignListByEateryId(state, params) {
  if (!params.eateryId) return;
  let res = state.merchant.get(`${params.eateryId}.campaigns`);
  if (!res || !res.get('data')) return res;

  res = res.set('data', res.get('data').map((promoKey) => state.merchant.getIn([promoKey, 'data'])));
  return res;
}
