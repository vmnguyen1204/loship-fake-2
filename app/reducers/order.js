import { defaultState, getKey, register } from 'reducers/helpers';

const { beforeFetch, setData, setError } = register('order');
const nonUserOrderKey = ['configs', 'freeShipInfo', 'shipperOrder'];

export default function OrderReducer(state = defaultState, action) {
  const { key } = action;
  const { data } = action;

  switch (action.type) {
    case 'USER_ORDER_FETCH_DETAIL_PENDING':
    case 'MERCHANT_ORDER_FETCH_DETAIL_PENDING':
    case 'MOMO_ORDER_FETCH_DETAIL_PENDING':
    case 'PARTNER_ORDER_FETCH_DETAIL_PENDING': {
      return beforeFetch(state, key);
    }
    case 'USER_ORDER_RATING_SHIPPER': {
      if (!state.get(action.code)) return state;
      return state.setIn([action.code, 'data', 'rating'], data);
    }
    case 'USER_ORDER_RATING_MERCHANT': {
      if (!state.get(action.code)) return state;
      return state.setIn([action.code, 'data', 'merchantRating'], data);
    }
    case 'USER_ORDER_CANCEL':
      return state
        .setIn([data.code, 'data', 'cancelNote'], data.cancelNote)
        .setIn([data.code, 'data', 'isCancellable'], data.isCancellable)
        .setIn([data.code, 'data', 'status'], data.status);
    case 'USER_REPORT_SHIPPER_FRAUD_CANCEL':
      return state.setIn([key, 'data', 'canReportShipper'], false);

    case 'USER_ORDER_FETCH_DETAIL_FAILED':
    case 'MERCHANT_ORDER_FETCH_DETAIL_FAILED':
    case 'MOMO_ORDER_FETCH_DETAIL_FAILED':
    case 'PARTNER_ORDER_FETCH_DETAIL_FAILED':
      return setError(state, key);
    case 'USER_ORDER_FETCH_DETAIL': {
      const sharingRef = data.sharing && [data.sharing.shareId];
      return setData(state, data.code, data, sharingRef);
    }
    case 'MERCHANT_ORDER_FETCH_DETAIL':
      return setData(state, key, data);
    case 'MOMO_ORDER_FETCH_DETAIL':
    case 'PARTNER_ORDER_FETCH_DETAIL':
      return setData(state, key, data);
    case 'ORDER_FETCH_FREE_SHIP_DURATION_PENDING':
    case 'FETCH_PAYMENT_BANK_PENDING':
    case 'FETCH_LOSEND_CONFIGS_PENDING':
    case 'FETCH_LOSHIP_CONFIGS_PENDING':
    case 'FETCH_CONFIGS_PENDING':
    case 'FETCH_PAYMENT_SUPPORT_TOKENIZED_PENDING':
    case 'FETCH_PAYMENT_SUPPORT_PAY_DEBT_PENDING':
      return beforeFetch(state, key);
    case 'ORDER_FETCH_FREE_SHIP_DURATION_FAILED':
    case 'FETCH_PAYMENT_BANK_FAILED':
    case 'FETCH_LOSEND_CONFIGS_FAILED':
    case 'FETCH_LOSHIP_CONFIGS_FAILED':
    case 'FETCH_CONFIGS_FAILED':
    case 'FETCH_PAYMENT_SUPPORT_TOKENIZED_FAILED':
    case 'FETCH_PAYMENT_SUPPORT_PAY_DEBT_FAILED':
      return setError(state, key);
    case 'ORDER_FETCH_FREE_SHIP_DURATION':
    case 'FETCH_PAYMENT_BANK':
    case 'FETCH_LOSEND_CONFIGS':
    case 'FETCH_LOSHIP_CONFIGS':
    case 'FETCH_PAYMENT_SUPPORT_TOKENIZED':
    case 'FETCH_PAYMENT_SUPPORT_PAY_DEBT':
      return setData(state, key, data);
    case 'FETCH_CONFIGS':
      return setData(state, key, data);

    case 'ACCESSCONTROL_LOGOUT': {
      return state.keySeq().reduce((acc, cKey) => {
        if (!nonUserOrderKey.includes(cKey)) return acc.delete(cKey);
        return acc;
      }, state);
    }

    default:
      return state;
  }
}

export function getUserOrder(state, params) {
  const orderKey = getKey(state.order, params.order || params.shareId);
  return state.order.get(orderKey);
}

export function getMerchantOrder(state, params) {
  const orderKey = getKey(state.order, params.order + '.merchant');
  return state.order.get(orderKey);
}

export function getMoMoOrder(state) {
  return state.order.get('@PARTNER/MOMO');
}

export function getPartnerOrder(state, params) {
  if (!params.partnerName) return;
  return state.order.get(`@PARTNER/${params.partnerName}`);
}

export function getLosendConfigs(state) {
  return state.order.get('losendConfigs');
}

export function getConfigs(state) {
  return state.order.get('configs');
}

export function getPaymentSupportTokenized(state) {
  return state.order.get('payment.supportTokenized');
}

export function getPaymentSupportPayDebt(state) {
  return state.order.get('payment.supportPayDebt');
}
