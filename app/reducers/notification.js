import { Map } from 'immutable';
import moment from 'moment';
import { uniqBy } from 'lodash';

const UPDATE_ORDER_NOTIFICATIONS = '@@notifications/UPDATE_ORDER_NOTIFICATIONS';
const UPDATE_NOTIFICATIONS = '@@notifications/UPDATE_NOTIFICATIONS';

const initialState = new Map({
  order_notifications: new Map({
    count: 0,
    data: [],
  }),
  notifications: new Map({
    count: 0,
    data: [],
  }),
});

const URL = {
  FETCH_ORDERS: '/users/me/orders',
  FETCH_NOTI: '/users/me/notifications',
};

export const getStatus = (order) => {
  switch (order.status) {
    case 'pre-order':
      return 'pre_order';
    case 'ordered':
    case 'assigning':
      return 'processing';
    case 'purchasing':
    case 'deliverying':
      return 'deliverying';
    case 'cancel':
    case 'return': {
      return 'cancelled';
    }
    case 'done':
      return 'done';
    default:
      return '';
  }
};

export function clearOrderNotifications() {
  return async (dispatch, getState) => {
    const noti = getOrderNotifications(getState());
    const notifications = noti.get('data');

    const localNotifications = notifications.reduce((acc, notification) => {
      acc[notification.code] = notification;
      return acc;
    }, {});

    localStorage.setItem('order_notifications', JSON.stringify(localNotifications));
    dispatch({
      type: UPDATE_ORDER_NOTIFICATIONS,
      count: 0,
      data: notifications.map((n) => {
        return { ...n, isNew: false };
      }),
    });
  };
}

export function clearNotifications() {
  return async (dispatch, getState) => {
    const noti = getNotifications(getState());
    const notifications = noti.get('data');

    const localNotifications = notifications.reduce((acc, notification) => {
      acc[notification.id] = notification;
      return acc;
    }, {});
    localStorage.setItem('notifications', JSON.stringify(localNotifications));
    dispatch({
      type: UPDATE_NOTIFICATIONS,
      data: notifications.map((n) => {
        return { ...n, seen: true };
      }),
    });
  };
}

export function fetchOrderNotifications() {
  return async (dispatch, getState, { api }) => {
    const res = await api.callAPI('GET', { url: URL.FETCH_ORDERS, query: { limit: 10, isIncludingGroupOrder: true } });
    if (res.status !== 200) return;
    const newOrders = res.body.data;
    const oldOrders = JSON.parse(localStorage.getItem('order_notifications') || '{}');

    const notifications = [];
    let newNotifications = 0;

    newOrders.forEach((order) => {
      const oldOrder = oldOrders[order.code];
      const isNew = !oldOrder || oldOrder.orderStatus !== order.status;
      if (isNew) {
        newNotifications += 1;
      }
      const canReview = order.rating === undefined && order.status === 'done' && Date.parse(order.doneAt) + 86400000 > Date.now();
      notifications.push({
        id: order.id,
        code: order.code,
        eatery: order.eatery,
        orderStatus: order.status,
        shipper: order.shipper,
        rating: order.rating,
        status: order.status,
        statusText: getStatus(order),
        time: moment(order.createdAt).format('DD/MM/YYYY'),
        canReview,
        isNew,
        serviceName: order.serviceName,
        total: order.total,
        totalUserFee: order.totalUserFee,
        lines: order.lines,
        createdAt: order.createdAt,
        willBeDeliveredAt: order.willBeDeliveredAt,
        privileges: order.privileges,
        sharing: order.sharing,
      });
    });
    dispatch({
      type: UPDATE_ORDER_NOTIFICATIONS,
      count: newNotifications,
      data: notifications,
    });
  };
}

export function fetchNotifications(nextUrl) {
  return async (dispatch, getState, { api }) => {
    const res = await api.callAPI('GET', { url: nextUrl || URL.FETCH_NOTI });
    if (res.status !== 200) return;

    const notifications = res.body.data;
    const { pagination } = res.body;

    dispatch({
      type: UPDATE_NOTIFICATIONS,
      data: notifications,
      pagination,
      isAppend: !!nextUrl,
    });
  };
}

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_ORDER_NOTIFICATIONS: {
      const { count, data } = action;
      return state.set('order_notifications', new Map({ count, data }));
    }
    case UPDATE_NOTIFICATIONS: {
      const { data, pagination = {}, isAppend } = action;
      const oldData = typeof state.getIn(['notifications', 'data']).toArray === 'function'
        ? state.getIn(['notifications', 'data']).toArray()
        : state.getIn(['notifications', 'data']);

      const newData = uniqBy(isAppend ? oldData.concat(data) : data.concat(oldData), 'id');

      const count = newData.filter((noti) => !noti.seen).length;
      const nextUrl = !isAppend && oldData.length ? state.getIn(['notifications', 'nextUrl']) : pagination.nextUrl;

      return state.set('notifications', new Map({ count, data: newData, nextUrl }));
    }
    case 'USER_ORDER_RATING_SHIPPER': {
      const newData = state.getIn(['order_notifications', 'data']).map((notification) => {
        if (notification.id === action.data.orderId) {
          return { ...notification, rating: action.data, canReview: false };
        }
        return notification;
      });
      return state.setIn(['order_notifications', 'data'], newData);
    }
    default:
      return state;
  }
};

export function getOrderNotifications(state) {
  return state.notification.get('order_notifications');
}

export function getNotifications(state) {
  return state.notification.get('notifications');
}

export function getRecentOrderedMerchants(state) {
  const notifications = state.notification.getIn(['order_notifications', 'data']);
  if (!notifications || !notifications.length) return [];

  const merchants = uniqBy(notifications.map((noti) => noti.eatery), 'id');
  const res = merchants.map((merchant) => {
    return { ...merchant, address: merchant.address.street };
  });

  return res;
}
