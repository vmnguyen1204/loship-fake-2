import { defaultState, getKey, register } from 'reducers/helpers';

const { beforeFetch, setError, setData } = register('buy');

export default function BuyReducer(state = defaultState, action) {
  const { key, data } = action;
  switch (action.type) {
    case 'FETCH_BLOCKS_ITEM_OFFERS_PENDING':
      return beforeFetch(state, key);
    case 'FETCH_BLOCKS_ITEM_OFFERS_FAILED': {
      return setError(state, key);
    }

    case 'FETCH_BLOCKS_ITEM_OFFERS': {
      data.eatery && (data.eatery.lng = data.eatery.lng || data.eatery.long);
      return setData(state, key, data);
    }

    case 'FETCH_ITEM_BY_ID':
      return setData(state, key, data, action.shareId && [action.shareId + '.shareId']);

    default:
      return state;
  }
}

export const getBlocksItemOffers = (state) => {
  return state.buy.get('all');
};

export const getItemById = (state, params = {}) => {
  if (params.itemId) return state.buy.get(`${params.itemId}.item`);
  if (params.shareId) {
    const key = getKey(state.buy, `${params.shareId}.shareId`);
    return state.buy.get(key);
  }

  return null;
};
