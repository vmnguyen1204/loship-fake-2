import { defaultState, getKey, register } from 'reducers/helpers';
import * as cookie from 'utils/shims/cookie';

const parseData = (data) => {
  return {
    id: data.id,
    avatar: data.avatar,
    roles: data.roles,
    username: data.username,
    fullname: data.name.full,
    name: data.name,
    shortName: data.name.short,
    cityId: data.cityId,
    status: data.status,
    verifiedFacebook: data.verifiedFacebook,
    verifiedPhoneNumber: data.verifiedPhoneNumber,
    profile: data.profile,
    providers: data.providers,
    fbAccountInfo: data.fbAccountInfo,
  };
};

const parseCurrentUserData = (data) => {
  return {
    ...parseData(data),
    phoneNumber: data.profile.phoneNumber,
    countryCode: data.profile.countryCode,
    nextUpdatePhoneNumber: data.nextUpdatePhoneNumber,
  };
};

const { beforeFetch, setData, removeData } = register('user');

export default function UserReducer(state = defaultState, action) {
  const { key, data, ref, error } = action;
  switch (action.type) {
    case 'USER_FETCH_PENDING':
    case 'USER_FETCH_SHIPPING_ADDRESSES_PENDING':
    case 'USER_FETCH_PAYMENT_CARDS_PENDING':
    case 'USER_FETCH_DEBT_ORDERS_PENDING':
    case 'USER_FETCH_PAYMENT_CARDS_SUPPORT_PAY_DEBT_PENDING':
    case 'USER_FETCH_REFERRAL_CODE_PENDING':
    case 'USER_FETCH_REFEREES_PENDING':
      return beforeFetch(state, key);
    case 'USER_APPEND':
    case 'USER_FETCH':
      return setData(state, key || parseData(data).username, parseData(data), ref);

    case 'USER_FETCH_SHIPPING_ADDRESSES':
    case 'USER_FETCH_REFERRAL_CODE':
    case 'USER_FETCH_REFEREES': {
      return setData(state, key, action.data);
    }
    case 'USER_UPDATE_PROFILE':
    case 'USER_UPDATE_AVATAR':
    case 'USER_VERIFIED_TOKEN_FOR_FB':
    case 'USER_LINK_FACEBOOK':
    case 'USER_UNLINK_FACEBOOK': {
      const newState = setData(state, '@CURRENT_USER', parseCurrentUserData(data), data._id);
      return setData(newState, data.username, parseData(data), data._id);
    }

    case 'USER_FETCH_PAYMENT_CARDS':
    case 'USER_FETCH_DEBT_ORDERS':
    case 'USER_FETCH_PAYMENT_CARDS_SUPPORT_PAY_DEBT': {
      return setData(state, key, data);
    }
    case 'USER_DELETE_PAYMENT_CARD': {
      let newData = state.getIn([key, 'data']);
      newData = newData.filter((card) => card.id !== action.cardId);

      return state.setIn([key, 'data'], newData);
    }

    case 'USER_AUTO_PAY_DEBT_ORDER': {
      let newData = state.getIn([key, 'data']);
      newData = newData.filter((order) => order.code !== action.code);

      return state.setIn([key, 'data'], newData);
    }

    case 'ACCESSCONTROL_AUTHEN_FAILED':
      if (error.status !== 401) return state;
      return defaultState;
    case 'ACCESSCONTROL_AUTHEN': {
      const newState = setData(state, '@CURRENT_USER', parseCurrentUserData(data), data._id);
      return setData(newState, data.username, parseData(data), data._id);
    }

    case 'ACCESSCONTROL_LOGOUT':
      typeof window !== 'undefined' && window.localStorage.clear();
      cookie.expire('accessToken');
      return removeData(state, '@CURRENT_USER');

    default:
      return state;
  }
}

export function getUser(state, params) {
  const userKey = getKey(state.user, params.user);
  return state.user.get(userKey);
}
export function getCurrentUser(state) {
  return getUser(state, { user: '@CURRENT_USER' });
}
export function getShippingAddresses(state) {
  const currentUser = getCurrentUser(state);
  if (!currentUser) return;

  return state.user.get(`${currentUser.getIn(['data', 'username'])}.shippingAddresses`);
}
export function getPaymentCards(state) {
  const currentUser = getCurrentUser(state);
  if (!currentUser) return;

  return state.user.get(`${currentUser.getIn(['data', 'username'])}.paymentCards`);
}
export function getPaymentCardsSupportPayDebt(state) {
  const currentUser = getCurrentUser(state);
  if (!currentUser) return;

  return state.user.get(`${currentUser.getIn(['data', 'username'])}.paymentCardsSupportPayDebt`);
}

export function getReferralCode(state) {
  const currentUser = getCurrentUser(state);
  if (!currentUser) return;

  return state.user.get(`${currentUser.getIn(['data', 'username'])}.referralCode`);
}

export function getReferees(state) {
  const currentUser = getCurrentUser(state);
  if (!currentUser) return;

  return state.user.get(`${currentUser.getIn(['data', 'username'])}.referees`);
}

export function getDebtOrders(state) {
  const currentUser = getCurrentUser(state);
  if (!currentUser) return;

  return state.user.get(`${currentUser.getIn(['data', 'username'])}.debtOrders`);
}
