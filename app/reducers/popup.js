import { defaultState } from 'reducers/helpers';
import { Map } from 'immutable';

function setData(state, key, isOpen = false, params) {
  const popupData = new Map({
    data: new Map({ name: key, params }),
    status: isOpen && 'SUCCESS',
  });

  return state.set(key, popupData);
}

let openedPopup = [];

export default function PopupReducer(state = defaultState, action) {
  const { type, key, params } = action;
  switch (type) {
    case 'POPUP_OPEN': {
      if (!params || params.wrapperMode !== 'grow') {
        openedPopup = openedPopup
          .filter((popup) => popup !== key)
          .concat(key);
        document.body.style.overflow = 'hidden';
        document.body.style.paddingRight = 8;
      }
      return setData(state, key, true, params);
    }
    case 'POPUP_CLOSE': {
      openedPopup = openedPopup
        .filter((popup) => popup !== key);
      if (openedPopup.length === 0) {
        document.body.style.overflow = '';
        document.body.style.paddingRight = '';
      }

      return setData(state, key, false);
    }
    default:
      return state;
  }
}

export function get(state, params) {
  return state.popup.get(params.popup);
}
