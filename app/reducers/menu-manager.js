import { List, fromJS } from 'immutable';
import { defaultState, getKey, register } from 'reducers/helpers';

import { keepOldData } from 'utils/merchant';

const parseData = (data, extras = {}) => {
  return fromJS({ group: data, extras });
};

const parseDataSupplyMenu = (data) => {
  const groups = [];
  const industries = [];
  data.forEach((industry) => {
    const { groupDishes = [] } = industry;
    industries.push({ ...industry, groupDishes: null });
    groupDishes.forEach((groupDish) => {
      groupDish.industryId = industry.id;
      groups.push(groupDish);
    });
  });
  return {
    industries,
    groups,
  };
};

const sortIndustryByOrder = (groups) => {
  return groups.sort((a, b) => b.get('order') - a.get('order'));
};

const { beforeFetch, setData } = register('menuManager');

export default function MenuReducer(state = defaultState, action) {
  const { data, key, extras } = action;
  switch (action.type) {
    case 'MCM_FETCH_MERCHANT_SUPPLY_MENU_PENDING':
    case 'MCM_FETCH_MENU_PENDING':
      return beforeFetch(state, key, true);
    case 'MCM_FETCH_MENU':
    case 'MCM_COPY_MENU':
      return setData(state, key, parseData(data, extras), data.id);

    case 'MCM_FETCH_MERCHANT_SUPPLY_MENU': {
      let newState = state;
      const { industries, groups } = parseDataSupplyMenu(data);
      newState = setData(newState, key, fromJS({ group: groups, extras }), data.id);
      newState = setData(newState, key + '.industries', fromJS({ industry: industries, extras }));
      return newState;
    }
    case 'FETCH_INDUSTRIES_SEARCH': {
      return setData(state, key, fromJS({ industries: data }));
    }
    case 'MCM_FETCH_MORE_MENU': {
      const groups = state.getIn([key, 'data', 'group']) || new List();
      const gData = groups.merge(fromJS(data));
      return setData(state, key, parseData(gData, extras));
    }
    case 'MCM_FETCH_MORE_DISH': {
      const groups = state.getIn([key, 'data', 'group']) || new List();
      const gIndex = groups.findIndex((g) => g.get('id') === action.groupDishId);
      const pagination = extras || {};
      if (gIndex === -1) break;

      const dishes = groups.getIn([gIndex, 'dishes']);
      const cData = dishes.merge(fromJS(data));

      state = state.setIn([key, 'data', 'group', gIndex, 'dishes'], cData);
      return state.setIn([key, 'data', 'group', gIndex, 'pagination'], fromJS(pagination));
    }
    case 'MCM_FETCH_DETAIL_DISH_LOSUPPLY':
      return setData(state, key, parseData(data));
    case 'MCM_NOTE_STOCK':
      return state.setIn([key, 'data', 'group', 'notes'], data);
    case 'MCM_MENU_INDUSTRY_CREATE':
    case 'MCM_MENU_INDUSTRY_UPDATE':
    case 'MCM_MENU_INDUSTRY_DELETE': {
      let industries = state.getIn([key, 'data', 'industry']) || new List();
      const gIndex = industries.findIndex((g) => g.get('id') === (action.deleteId || data.id));
      /**
       * CREATE - gIndex < 0, append to end of list
       * UPDATE - gIndex >= 0 && !action.deleteId, merge into current index
       * DELETE - gIndex >= 0 && action.deleteId, remove current index
       */
      if (gIndex < 0) industries = industries.push(fromJS(data));
      else {
        let cData = industries.get(gIndex);
        if (cData.get('name') !== data.name) {
          cData = cData.set('oldName', cData.get('name'));
          keepOldData({ id: cData.get('id'), name: cData.get('name'), type: 'industry' }, key);
        }
        if (!action.deleteId) industries = industries.splice(gIndex, 1, cData.merge(fromJS(data)));
        else industries = industries.delete(gIndex);
      }
      return state.setIn([key, 'data', 'industry'], sortIndustryByOrder(industries));
    }
    case 'MCM_MENU_GROUP_CREATE':
    case 'MCM_MENU_GROUP_UPDATE':
    case 'MCM_MENU_GROUP_DELETE': {
      const groupData = fromJS({ ...data, industryId: action.industryId });
      let groups = state.getIn([key, 'data', 'group']) || new List();
      const gIndex = groups.findIndex((g) => g.get('id') === (action.deleteId || data.id));
      /**
       * CREATE - gIndex < 0, append to end of list
       * UPDATE - gIndex >= 0 && !action.deleteId, merge into current index
       * DELETE - gIndex >= 0 && action.deleteId, remove current index
       */
      if (gIndex < 0) groups = groups.push(groupData);
      else {
        let cData = groups.get(gIndex);
        if (cData.get('name') !== data.name) {
          cData = cData.set('oldName', cData.get('name'));
          keepOldData({ id: cData.get('id'), name: cData.get('name'), type: 'group' }, key);
        }
        if (!action.deleteId) groups = groups.splice(gIndex, 1, cData.merge(groupData));
        else groups = groups.delete(gIndex);
      }

      return state.setIn([key, 'data', 'group'], groups);
    }
    case 'MCM_DISH_COPY':
    case 'MCM_DISH_CREATE':
    case 'MCM_DISH_UPDATE':
    case 'MCM_DISH_DELETE': {
      const groups = state.getIn([key, 'data', 'group']);
      const gIndex = groups.findIndex((g) => g.get('id') === action.groupDishId);

      let dishes = groups.getIn([gIndex, 'dishes']) || new List();
      const dIndex = dishes.findIndex((d) => d.get('id') === (action.deleteId || data.id));
      /**
       * CREATE - gIndex < 0, append to end of list
       * UPDATE - gIndex >= 0 && !action.deleteId, merge into current index
       * DELETE - gIndex >= 0 && action.deleteId, remove current index
       */
      if (dIndex < 0) {
        dishes = dishes.push(fromJS(data)).sort((x, y) => x.get('name').localeCompare(y.get('name')));
      } else {
        let cData = dishes.get(dIndex);
        if (cData.get('name') !== data.name) {
          cData = cData.set('oldName', cData.get('name'));
          keepOldData({ id: cData.get('id'), name: cData.get('name'), type: 'dish' }, key);
        }
        if (!action.deleteId) dishes = dishes.splice(dIndex, 1, cData.merge(fromJS(data)));
        else dishes = dishes.delete(dIndex);
      }

      return state.setIn([key, 'data', 'group', gIndex, 'dishes'], dishes);
    }
    case 'MCM_DISH_CUSTOM_CREATE':
    case 'MCM_DISH_CUSTOM_UPDATE': {
      const groups = state.getIn([key, 'data', 'group']);
      const gIndex = groups.findIndex((g) => g.get('id') === action.groupDishId);

      const dishes = groups.getIn([gIndex, 'dishes']);
      const dIndex = dishes.findIndex((d) => d.get('id') === action.dishId);

      let customs = dishes.getIn([dIndex, 'customs']) || new List();
      const cIndex = customs.findIndex((c) => c.get('id') === data.id);
      /**
       * CREATE - gIndex < 0, append to end of list
       * UPDATE - gIndex >= 0 && !action.deleteId, merge into current index
       * DELETE - gIndex >= 0 && action.deleteId, remove current index
       */
      if (cIndex < 0) {
        customs = customs.push(fromJS(data)).sort((x, y) => x.get('name').localeCompare(y.get('name')));
      } else {
        let cData = customs.get(cIndex);
        if (cData.get('name') !== data.name) {
          cData = cData.set('oldName', cData.get('name'));
          keepOldData({ id: cData.get('id'), name: cData.get('name'), type: 'custom' }, key);
        }
        if (!action.deleteId) customs = customs.splice(cIndex, 1, cData.merge(fromJS(data)));
        else customs = customs.delete(cIndex);
      }

      customs = customs.filter((c) => c.get('active'));
      return state.setIn([key, 'data', 'group', gIndex, 'dishes', dIndex, 'customs'], customs);
    }
    case 'MCM_DISH_CUSTOM_OPTION_CREATE':
    case 'MCM_DISH_CUSTOM_OPTION_UPDATE': {
      const groups = state.getIn([key, 'data', 'group']);
      const gIndex = groups.findIndex((g) => g.get('id') === action.groupDishId);

      const dishes = groups.getIn([gIndex, 'dishes']);
      const dIndex = dishes.findIndex((d) => d.get('id') === action.dishId);

      const customs = dishes.getIn([dIndex, 'customs']);
      const cIndex = customs.findIndex((c) => c.get('id') === action.customId);

      let customOptions = customs.getIn([cIndex, 'customOptions']) || new List();
      const coIndex = customOptions.findIndex((c) => c.get('id') === data.id);
      /**
       * CREATE - gIndex < 0, append to end of list
       * UPDATE - gIndex >= 0 && !action.deleteId, merge into current index
       * DELETE - gIndex >= 0 && action.deleteId, remove current index
       */
      if (coIndex < 0) {
        customOptions = customOptions.push(fromJS(data)).sort((x, y) => x.get('value').localeCompare(y.get('value')));
      } else {
        let cData = customOptions.get(coIndex);
        if (cData.get('value') !== data.value) {
          cData = cData.set('oldValue', cData.get('value'));
          keepOldData({ id: cData.get('id'), name: cData.get('value'), type: 'customOption' }, key);
        }
        if (!action.deleteId) {
          customOptions = customOptions.splice(coIndex, 1, cData.merge(fromJS(data)));
        } else customOptions = customOptions.delete(coIndex);
      }

      customOptions = customOptions.filter((c) => c.get('active'));
      return state.setIn(
        [key, 'data', 'group', gIndex, 'dishes', dIndex, 'customs', cIndex, 'customOptions'],
        customOptions,
      );
    }
    case 'MCM_DISH_STOCK_UPDATE': {
      const groups = state.getIn([key, 'data', 'group']);
      const gIndex = groups.findIndex((g) => g.get('id') === action.groupDishId);

      const dishes = groups.getIn([gIndex, 'dishes']) || new List();
      const dIndex = dishes.findIndex((d) => d.get('id') === (action.data.dishId || data.id));
      if (dIndex > -1) {
        const newData = dishes.setIn([dIndex, 'wallet'], data);
        return state.setIn([key, 'data', 'group', gIndex, 'dishes'], newData);
      }
      break;
    }
    case 'MCM_UPDATE_SHOWN_LOSUPPLY': {
      const groups = state.getIn([key, 'data', 'group']);
      const gIndex = groups.findIndex((g) => g.get('id') === action.groupDishId);
      const group = groups.get(gIndex);

      if (gIndex !== -1) {
        const newData = group.set('isShownSupplyItemsForUser', data.isShownSupplyItemsForUser);
        return state.setIn([key, 'data', 'group', gIndex], newData);
      }

      return state;
    }
    default:
      return state;
  }
}

export function getMerchantMenu(state, params) {
  const merchantKey = getKey(state.menuManager, params.merchant);
  return state.menuManager.get(merchantKey);
}

export function getMerchantMenuUser(state, params) {
  const merchantKey = getKey(state.menuManager, params.merchant);
  const groupState = state.menuManager.get(merchantKey);
  if (groupState?.get('status') !== 'SUCCESS') return groupState;

  const groupData = groupState?.get('data');
  const groups = groupData?.get('group');
  if (groups?.size === 0) return fromJS({ data: [], status: 'SUCCESS' });

  const extras = groupData?.get('extras');
  const data = [{ industry: null, groups, extras }];
  return fromJS({ data, status: 'SUCCESS' });
}

export function getMerchantMenuLosupply(state, params) {
  const groupKey = getKey(state.menuManager, params.merchant);
  const industryKey = getKey(state.menuManager, params.merchant + '.industries');

  const groupState = state.menuManager.get(groupKey);
  if (groupState?.get('status') !== 'SUCCESS') return groupState;

  const industries = state.menuManager.getIn([industryKey, 'data', 'industry'])?.toJS() || [];
  const groupData = groupState?.get('data');
  const groups = groupData?.get('group');

  const data = industries.map((industry) => ({
    industry,
    groups: groups.filter((o) => o.get('industryId') === industry.id),
  }));
  return fromJS({ data, status: 'SUCCESS' });
}

export function getMenuIndustrySearch(state, params) {
  const industrySearchKey = getKey(state.menuManager, params.merchant + '.industries');
  return state.menuManager.get(industrySearchKey);
}

export function getGroup(state, params) {
  const menu = getMerchantMenu(state, params);
  const group = menu.getIn(['data', 'group']).find((g) => g.get('id') === params.groupDishId);
  if (!group) return null;
  return menu.set('data', group);
}

export function getDish(state, params) {
  const group = getGroup(state, params);
  const dish = group.getIn(['data', 'dishes']).find((d) => d.get('id') === params.dishId);
  if (!dish) return null;
  return group.set('data', dish);
}

export function getNote(state, params) {
  const dishKey = getKey(state.menuManager, params.dishId);
  return state.menuManager.get(dishKey);
}

export function getDetailDishLosupply(state, params) {
  const dishKey = getKey(state.menuManager, params.dishId);
  return state.menuManager.get(dishKey);
}
