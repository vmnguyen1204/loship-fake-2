import { appendData as _appendData, defaultState, getKey, register } from 'reducers/helpers';
import moment from 'utils/shims/moment';

const { setData } = register('alert', null);

function initAlert(type, message, data) {
  return {
    type,
    message,
    data,
    status: 'active', // active, expired
  };
}

function appendData(state, alertKey) {
  const maxAlertItems = 5;
  return _appendData(state, 'all', [alertKey], null, null, null, maxAlertItems);
}

export default function AlertReducer(state = defaultState, action) {
  const { key, data } = action;
  const alertKey = `alr-${moment().unix()}`;
  switch (action.type) {
    case 'ALERT_EXPIRE': {
      const alr = state.get(key);
      let alrData = alr.get('data');
      alrData = alrData.set('status', 'expired');
      return state.setIn([key, 'data'], alrData);
    }
    case 'ALERT_DESTROY': {
      const newList = state.getIn(['all', 'data']).filter((x) => x !== key);
      return state.setIn(['all', 'data'], newList);
    }
    case 'DISH_SELECT_WARNING': {
      const newState = setData(state, alertKey, initAlert('error', action.message, data));
      return appendData(newState, alertKey);
    }
    default:
      return state;
  }
}

export function get(state, params) {
  const alertKey = getKey(state.alert, params.alert);
  return state.alert.get(alertKey);
}

export function getAll(state) {
  return state.alert.get('all');
}
