import { defaultState, register } from 'reducers/helpers';

const { beforeFetch, setData } = register('menuActiveRequests');

const parseData = (data) => {
  return data;
};

export default function Reducer(state = defaultState, action) {
  const { data, key } = action;

  switch (action.type) {
    case 'ACTIVE_MENU_REQUEST_FETCH_PENDING':
      return beforeFetch(state, key);
    case 'ACTIVE_MENU_REQUEST_FETCH':
      return setData(state, key, parseData(data));
    case 'ACTIVE_MENU_REQUEST_APPROVE_PENDING':
      return beforeFetch(state, key);
    case 'ACTIVE_MENU_REQUEST_APPROVE':
      return setData(state, key, parseData(data));
    case 'ACTIVE_MENU_REQUEST_REJECT_PENDING':
      return beforeFetch(state, key);
    case 'ACTIVE_MENU_REQUEST_REJECT':
      return setData(state, key, parseData(data));
    default:
      return state;
  }
}
