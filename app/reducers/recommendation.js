import { defaultState, getKey, register } from 'reducers/helpers';


const { beforeFetch, setData, setError } = register('recommendation');

export default function reducer(state = defaultState, action) {
  const { data, key } = action;

  switch (action.type) {
    case 'FETCH_EATERY_SUGGEST_DISHES_PENDING':
      return beforeFetch(state, key);

    case 'FETCH_EATERY_SUGGEST_DISHES_FAILED':
      return setError(state, key);

    case 'FETCH_EATERY_SUGGEST_DISHES':
      return setData(state, key, data);

    default:
      return state;
  }
}

export function getEaterySuggestDishes(state, params) {
  const merchantKey = getKey(state.merchant, params.merchantUsername || params.merchant || params.eateryId);
  return state.recommendation.get(`${merchantKey}.dishes`);
}
