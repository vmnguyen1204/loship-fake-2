import { MQTTReducer } from 'lib/mqtt';
import alert from 'reducers/alert';
import buy from 'reducers/buy';
import cart from 'reducers/new/cart';
import category from 'reducers/category';
import code from 'reducers/code';
import { combineReducers } from 'redux';
import coreCategory from 'reducers/category-core';
import discover from 'reducers/discover';
import eatery from 'reducers/new/eatery';
import map from 'reducers/new/map';
import menu from 'reducers/menu';
import menuActiveRequest from 'reducers/menu-active-request';
import menuManager from 'reducers/menu-manager';
import merchant from 'reducers/merchant';
import merchantManager from 'reducers/merchant-manager';
import metadata from 'reducers/metadata';
import metadataNew from 'reducers/new/metadata';
import newsfeed from 'reducers/newsfeed';
import notification from 'reducers/notification';
import order from 'reducers/order';
import popup from 'reducers/popup';
import recommendation from 'reducers/recommendation';
import user from 'reducers/user';

export default combineReducers({
  alert,
  buy,
  cart,
  category,
  code,
  coreCategory,
  discover,
  eatery,
  map,
  menu,
  menuActiveRequest,
  menuManager,
  merchant,
  merchantManager,
  metadata,
  metadataNew,
  mqtt: MQTTReducer,
  newsfeed,
  notification,
  order,
  popup,
  recommendation,
  user,
});
