import { defaultState, getKey, register } from 'reducers/helpers';

const parseData = (data) => {
  return {
    id: data.id,
    address: data.address,
    cityId: data.cityId,
    avatar: data.avatar,
    name: data.name,
    slug: data.slug,
    username: data.username,
    commissionValue: data.commissionValue,
    lat: data.latitude,
    lng: data.longitude,
    street: data.address.street,
    isManager: data.isManager,
    shippingService: data.shippingService,
  };
};

const { beforeFetch, setData } = register('merchantManager');

export default function MerchantManagerReducer(state = defaultState, action) {
  const { data, key } = action;
  switch (action.type) {
    case 'MCM_FETCH_MERCHANT_PENDING':
      return beforeFetch(state, key);
    case 'MCM_FETCH_MERCHANT':
      return setData(state, data.slug, parseData(data), [data.id, data.username]);
    case 'UPDATE_AVATAR': {
      const merchantKey = getKey(state, key);
      return state.setIn([merchantKey, 'data', 'avatar'], data.avatar);
    }
    default:
      return state;
  }
}

export function getMerchant(state, params) {
  const username = params.merchantUsername;
  const merchantKey = getKey(state.merchantManager, params.merchant || username);
  return state.merchantManager.get(merchantKey);
}
