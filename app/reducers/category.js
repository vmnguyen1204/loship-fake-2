import { Map } from 'immutable';
import { getCoreCategoriesByIdOrSlugs } from 'reducers/category-core';
import { defaultState, getKey, register } from 'reducers/helpers';
import { getCurrentCity } from 'reducers/new/metadata';
import { APP_INFO } from 'settings/variables';
import { getCityByIdOrNameOrSlug } from 'utils/data';

const { setData } = register('category');

const parseData = (data) => {
  return {
    id: data.id,
    value: data.value,
    eateries: (data.count && data.count.eateries) || 0,
    imageWeb: data.imageWeb,
    slug: data.slug,
    type: data.type,
    order: data.order,
  };
};

export default (state = defaultState, action) => {
  const { data, key } = action;
  switch (action.type) {
    case 'CATEGORY_FETCH_ALL': {
      if (!data) return state;

      let newState = state;
      const dataIds = data.map((category) => {
        newState = setData(newState, category.slug, parseData(category), category.id);
        return category.id;
      });
      return setData(newState, key, dataIds);
    }
    default:
      return state;
  }
};

export function getCategory(state, params) {
  const categoryKey = getKey(state.category, params.category);
  return state.category.get(categoryKey);
}

export function getCategories(state) {
  const superCategoryId = APP_INFO.shipServiceId;
  const currentCity = getCurrentCity(state);
  if (!currentCity) return;

  const cityId = currentCity.type === 'city' ? currentCity.city.id : currentCity.district.city.id;

  const categoryIds = state.category.getIn([`all.c_${cityId}.s_${superCategoryId}`, 'data']);
  if (!categoryIds) return;

  return new Map({
    status: 'SUCCESS',
    data: categoryIds
      .map((id) => state.category.get(getKey(state.category, id)))
      .map((category) => category?.toJS().data)
      .filter(Boolean),
  });
}
export function getCategoriesBySuperCategoryIdCityIdDistrictId(state, params) {
  const superCategoryId = params.superCategoryId || 1;
  const currentCity = getCityByIdOrNameOrSlug(params.cityId);
  if (!currentCity) return;

  const cityId = currentCity.type === 'city' ? currentCity.city.id : currentCity.district.city.id;

  const categoryIds = state.category.getIn([`all.c_${cityId}.s_${superCategoryId}`, 'data']);
  if (!categoryIds) return;

  return new Map({
    status: 'SUCCESS',
    data: categoryIds
      .map((id) => state.category.get(getKey(state.category, id)))
      .map((category) => category?.toJS().data)
      .filter(Boolean),
  });
}

export function getCategoriesByIdOrSlugs(state, { slugs, ids }) {
  const idOrSlugs = ids || slugs;
  const unsupported = [];

  if (!Array.isArray(idOrSlugs)) return [];
  const categories = idOrSlugs
    .map((idOrSlug) => {
      const category = state.category.get(getKey(state.category, idOrSlug));
      return category || idOrSlug;
    })
    .map((category) => {
      if (typeof category.toJS !== 'function') {
        unsupported.push(category);
        return;
      }
      return category.toJS().data;
    })
    .filter(Boolean);
  return categories.concat(
    getCoreCategoriesByIdOrSlugs(state, { slugs: unsupported }),
  );
}
