import { defaultState, getKey, register } from 'reducers/helpers';

const { setData } = register('order');

export default function Reducer(state = defaultState, action) {
  const { data } = action;
  switch (action.type) {
    case 'PROMOTION_FETCH':
      return setData(state, data.code, data, ['@CURRENT_PROMOTION']);

    case 'PROMOTION_CLEAR':
      return defaultState;

    default:
      return state;
  }
}

export const getPromotionCode = (state) => {
  const promoKey = getKey(state.code, '@CURRENT_PROMOTION');
  return state.code.get(promoKey);
};
