import { createAction, createReducer } from '@reduxjs/toolkit';
import { publicData } from 'lib/mqtt';
import { getMerchantMenu } from 'reducers/menu';
import { getMerchant } from 'reducers/merchant';
import { createAsyncAction, initialState, setData, setError } from 'reducers/new/helper';
import { getCurrentUser } from 'reducers/user';
import { filterDishQuantity, getOrderLine, saveCartLocal } from 'utils/cart';
import { iife } from 'utils/helper';

export const API = {
  CREATE_GROUP: '/orders/group',
  JOIN_GROUP: '/orders/group/join',
  LOCK_MEMBER: '/order-groups/${groupTopic}/members/me/status',
  LOCK_MEMBER_BY_ID: '/order-groups/${groupTopic}/members/${memberId}/status',
};

export const cartAddUser = createAsyncAction('CART_ADD_USER');
export const cartConfirmPreorder = createAction('CART_CONFIRM_PREORDER');
export const cartOrderSuccess = createAction('CART_ORDER_SUCCESS');

export const cartFetchLocal = createAsyncAction('CART_FETCH_LOCAL', ({ getState, params }) => {
  const state = getState();

  const cartMetadata = getCartMetadata(state, params);
  if (cartMetadata && cartMetadata.status === 'SUCCESS') return;

  const cartMetadataKey = getCartMetadataKey(params);
  const currentUser = state.metadata.getIn(['config', 'data', 'currentUser']);
  const cartMetadataLocal = iife(() => {
    try {
      const res = JSON.parse(localStorage.getItem(cartMetadataKey));
      if (!res) throw new Error(`No cart metadata with key ${cartMetadataKey}`);
      return res.filter((user) => user.username);
    } catch (e) {
      return { users: [currentUser ? { username: currentUser } : { username: 'anonymous' }] };
    }
  });

  const cartLocal = cartMetadataLocal.users.reduce((order, user) => {
    const cartUser = iife(() => {
      try {
        const res = JSON.parse(localStorage.getItem(`${cartMetadataKey}.${user.username}`));
        if (!res) throw new Error(`No user order with key ${cartMetadataKey}.${user.username}`);
        if (!res.data || !res.data.length) throw new Error(`Invalid user order with key ${cartMetadataKey}.${user.username}`);

        if (!cartMetadataLocal.merchant) cartMetadataLocal.merchant = res.merchant;
        if (!cartMetadataLocal.serviceName) cartMetadataLocal.serviceName = res.serviceName;
        return res.data;
      } catch (e) {
        return [];
      }
    });
    const items = filterDishQuantity(cartUser);
    return { ...order, [user.username]: items };
  }, {});

  return { key: cartMetadataKey, data: { cartMetadata: cartMetadataLocal, cart: cartLocal } };
});

export const cartAddItem = createAsyncAction(
  'CART_ADD_ITEM',
  ({ getState, params }) => {
    if (!params.merchant) throw new Error('No merchant provided in cartAddItem');

    const currentUser = getCurrentUser(getState());
    const user = (currentUser && currentUser.toJS().data) || { username: 'anonymous' };

    const cartMetadataKey = getCartMetadataKey(params);
    return {
      key: `${cartMetadataKey}.${user.username}`,
      data: { ...params.data, taggedUserId: user.id },
      user,
      cartMetadataKey,
      merchant: params.merchant,
      serviceName: params.serviceName,
      groupTopic: params.groupTopic,
      lock: params.lock,
      overwrite: params.overwrite,
    };
  },
  ({ dispatch, getState, params }) => {
    const state = getState();

    const currentUser = getCurrentUser(state);
    const user = (currentUser && currentUser.toJS().data) || { username: 'anonymous' };
    const cartMetadataKey = getCartMetadataKey(params);
    const cartMetadata = getCartMetadata(state, params);
    const { data: { lockedUsers = {} } = {} } = cartMetadata || {};

    if (params.groupTopic && user.id) {
      const cart = getState().cart[`${cartMetadataKey}.${user.username}`];
      if (!cart) return;
      dispatch(
        publicData(`orders/${params.groupTopic}/${user.id}`, {
          key: `${cartMetadataKey}.${user.username}`,
          data: cart.data || [],
          lock: lockedUsers[user.username],
        }),
      );
    }
  },
);

export const cartUpdateQuantity = createAsyncAction(
  'CART_UPDATE_QUANTITY',
  ({ params }) => {
    const user = params.user || { username: 'anonymous' };

    const cartMetadataKey = getCartMetadataKey(params);
    return {
      key: `${cartMetadataKey}.${user.username}`,
      itemIndex: params.itemIndex,
      quantity: params.quantity,
      cartMetadataKey,
    };
  },
  ({ dispatch, getState, params }) => {
    const state = getState();

    const user = params.user || { username: 'anonymous' };
    const cartMetadataKey = getCartMetadataKey(params);
    const cartMetadata = getCartMetadata(state, params);
    const { data: { lockedUsers = {} } = {} } = cartMetadata || {};

    if (params.groupTopic && user.id) {
      const cart = getState().cart[`${cartMetadataKey}.${user.username}`];
      if (!cart) return;
      dispatch(
        publicData(`orders/${params.groupTopic}/${user.id}`, {
          key: `${cartMetadataKey}.${user.username}`,
          data: cart.data || [],
          lock: lockedUsers[user.username],
        }),
      );
    }
  },
);

export const cartLockUser = createAsyncAction(
  'CART_LOCK_USER',
  ({ params, getState }) => {
    if (!params.merchant || !params.groupTopic) return;

    const currentUser = getCurrentUser(getState());
    if (!currentUser) return;
    const user = params.user || (currentUser && currentUser.toJS().data);

    const cartMetadataKey = getCartMetadataKey(params);
    return {
      key: `${cartMetadataKey}.${user.username}`,
      lock: params.lock,
    };
  },
  ({ dispatch, getState, params }) => {
    const currentUser = getCurrentUser(getState());
    const user = params.user || (currentUser && currentUser.toJS().data);

    const cartMetadataKey = getCartMetadataKey(params);
    if (params.groupTopic && user.id) {
      const cart = getState().cart[`${cartMetadataKey}.${user.username}`];
      if (!cart) return;
      dispatch(
        publicData(`orders/${params.groupTopic}/${user.id}`, {
          key: `${cartMetadataKey}.${user.username}`,
          data: cart.data || [],
          lock: cart.lock,
        }),
      );
    }
  },
);

export const cartUpdateSoldout = createAsyncAction(
  'CART_UPDATE_ITEM_SOLDOUT',
  ({ params }) => {
    const user = params.user || { username: 'anonymous' };

    const cartMetadataKey = getCartMetadataKey(params);
    return {
      key: `${cartMetadataKey}.${user.username}`,
      itemIndex: params.itemIndex,
      quantity: params.quantity,
      cartMetadataKey,
    };
  },
  ({ dispatch, getState, params }) => {
    const currentUser = getCurrentUser(getState());
    const user = params.user || (currentUser && currentUser.toJS().data);

    const cartMetadataKey = getCartMetadataKey(params);
    if (params.groupTopic && user.id) {
      const cart = getState().cart[`${cartMetadataKey}.${user.username}`];
      if (!cart) return;
      dispatch(
        publicData(`orders/${params.groupTopic}/${user.id}`, {
          key: `${cartMetadataKey}.${user.username}`,
          data: cart.data || [],
          lock: cart.lock,
        }),
      );
    }
  },
);

export const cartGroupCreate = createAsyncAction('CART_GROUP_CREATE', ({ params }) => {
  if (!params.eateryId) return;
  return { method: 'POST', url: API.CREATE_GROUP, body: { eateryId: params.eateryId } };
});

export const cartGroupJoin = createAsyncAction('CART_GROUP_CREATE', ({ params }) => {
  if (!params.groupTopic) return;
  return { method: 'POST', url: API.JOIN_GROUP, body: { topic: params.groupTopic } };
});

export const cartClear = createAsyncAction('CART_CLEAR', ({ params, dispatch, getState }) => {
  const cartMetadataKey = getCartMetadataKey(params);

  if (params.groupTopic) {
    const cartMetadata = getCartMetadata(getState(), params);
    if (!cartMetadata || !cartMetadata.data) return;

    const { users = [] } = cartMetadata.data;
    users.forEach((user) => {
      dispatch(publicData(`orders/${params.groupTopic}/${user.id}`));
    });
    dispatch(publicData(`orders/${params.groupTopic}/information`, { data: { success: true } }));
  }

  return { key: cartMetadataKey };
});

// ==============================================================

export default createReducer(initialState, {
  [cartFetchLocal.SUCCESS]: (state, action) => {
    const { payload: { key, data } = {} } = action;

    state[key] = { status: 'SUCCESS', data: data.cartMetadata };
    data.cartMetadata.users.forEach((user) => {
      state[`${key}.${user.username}`] = { status: 'SUCCESS', data: data.cart[user.username] };
    });
  },
  [cartAddUser.SUCCESS]: (state, action) => {
    let { payload: { key, data } = {} } = action;
    // exception payload for Mqtt
    if (!data) data = action.user;

    let cartMetadata = state[key];
    if (!cartMetadata || !cartMetadata.data || !cartMetadata.data.users) {
      cartMetadata = { status: 'SUCCESS', data: { users: [data] } };
    } else if (!cartMetadata.data.users.some((u) => u.username === data.username)) {
      cartMetadata.data.users.push(data);
    }

    state[key] = cartMetadata;
    if (!state[`${key}.${data.username}`]) {
      state[`${key}.${data.username}`] = { status: 'SUCCESS', data: [] };
    }
    localStorage.setItem(key, JSON.stringify(cartMetadata.data));
  },
  [cartAddItem.SUCCESS]: (state, action) => {
    const { payload: { key, data, user, cartMetadataKey, overwrite, merchant, serviceName } = {}, lock } = action;
    // exception payload for Mqtt
    let items = data;
    if (!items) items = action.data;
    if (!Array.isArray(items)) items = [items];

    let cartMetadata = state[cartMetadataKey];
    if (!cartMetadata || !cartMetadata.data || !cartMetadata.data.users) {
      cartMetadata = { status: 'SUCCESS', data: { users: [user] } };
    } else if (!cartMetadata.data.users.some((u) => u.username === user.username)) {
      cartMetadata.data.users.push(user);
    }
    const lockedUsers = cartMetadata.data.lockedUsers || {};
    lockedUsers[user.username] = lock;

    cartMetadata.data = { ...cartMetadata.data, merchant, serviceName, lockedUsers };
    state[cartMetadataKey] = cartMetadata;
    localStorage.setItem(cartMetadataKey, JSON.stringify(cartMetadata.data));

    const { data: lines } = saveCartLocal({ key, data: items, merchant, serviceName, overwrite });
    state[key] = { status: 'SUCCESS', data: lines };
  },
  [cartUpdateQuantity.SUCCESS]: (state, action) => {
    const { payload: { key, itemIndex, quantity = 0 } = {} } = action;
    const lines = state[key].data || [];
    if (!lines[itemIndex]) return;

    lines[itemIndex].dishQuantity += quantity;
    if (lines[itemIndex].dishQuantity <= 0) lines.splice(itemIndex, 1);
    saveCartLocal({ key, data: lines, overwrite: true });
  },
  [cartConfirmPreorder]: (state, action) => {
    const { payload: { key } = {} } = action;
    if (!state[key] || !state[key].data) return;

    state[key].data.preorderConfirm = true;
  },
  [cartOrderSuccess]: (state, action) => {
    const { payload: { key } = {} } = action;
    if (!state[key] || !state[key].data) return;

    state[key].data.success = true;
  },
  // Cart join / group create
  [cartGroupCreate.SUCCESS]: (state, action) => {
    const { payload: { data } = {} } = action;
    const key = getCartMetadataKey({ groupTopic: data.topic });

    const newData = iife(() => {
      if (!state[key] || !state[key].data) return data;
      return { ...state[key].data, ...data };
    });
    setData(state, key, newData);
  },
  [cartGroupCreate.FAILED]: (state, action) => {
    const { payload: { body } = {} } = action;
    const key = getCartMetadataKey({ groupTopic: body.topic });

    setError(state, key, { keep: true });
  },
  [cartLockUser.SUCCESS]: (state, action) => {
    const { payload: { key, lock } = {} } = action;
    if (!state[key] || !state[key].data) return;
    state[key].lock = lock;
  },
  [cartUpdateSoldout.SUCCESS]: (state, action) => {
    const { payload: { key, itemIndex } = {} } = action;
    const lines = state[key].data || [];
    if (!lines[itemIndex]) return;

    lines[itemIndex].soldOut = true;
    saveCartLocal({ key, data: lines, overwrite: true });
  },
  [cartClear.SUCCESS]: (state, action) => {
    const { payload: { key } = {} } = action;
    if (!state[key] || !state[key].data) return;

    const { users = [] } = state[key].data;
    users.forEach((user) => {
      localStorage.removeItem(`${key}.${user.username}`);
      delete state[`${key}.${user.username}`];
    });

    delete state[key];
  },
  ACCESSCONTROL_AUTHEN: (state, action) => {
    const { data: { username } = {} } = action;
    if (!username) return;

    const cartMetadataKey = getCartMetadataKey();
    try {
      // @ move anonymous order to current user
      const cartMetadata = state[cartMetadataKey];
      const { users = [] } = cartMetadata.data;
      cartMetadata.data = {
        ...cartMetadata.data,
        users: users.filter((u) => ![username, 'anonymous'].includes(u.username)).concat({ username }),
      };
      state[`${cartMetadataKey}.${username}`] = state[`${cartMetadataKey}.anonymous`];
      delete state[`${cartMetadataKey}.anonymous`];

      // @ also update localStorage
      saveCartLocal({
        key: `${cartMetadataKey}.${username}`,
        data: state[`${cartMetadataKey}.${username}`].data,
        merchant: cartMetadata.data.merchant,
        serviceName: cartMetadata.data.serviceName,
        overwrite: true,
      });
      localStorage.setItem(cartMetadataKey, JSON.stringify(cartMetadata.data));
      localStorage.removeItem(`${cartMetadataKey}.anonymous`);
    } catch (e) {
      //
    }
  },
  ACCESSCONTROL_LOGOUT: () => {
    return initialState;
  },
});

// ==============================================================

export function getCartMetadataKey({ groupTopic } = {}) {
  return `cart.${groupTopic || '000000'}`;
}

/** GET CART METADATA
 * data: {
 *    users: [{ id, username, name: {full, short} }],
 *    %createdAt, createdBy, topic, expireAt, lockUsers: [username]%
 * },
 * merchant, serviceName
 */
export function getCartMetadata(state, params) {
  const key = getCartMetadataKey(params);
  return state.cart[key];
}

/** GET CART
 * data: {
 *   [username]: { status, data: [OrderLine] }
 * }
 */
// TODO: cart - handle append anonymous order to currentUser
export function getCart(state, params) {
  const cartMetadata = getCartMetadata(state, params);
  if (!cartMetadata || cartMetadata.status !== 'SUCCESS') return;

  const cartMetadataKey = getCartMetadataKey(params);

  const cart = (cartMetadata.data.users || []).reduce((acc, user) => {
    acc[user.username] = state.cart[`${cartMetadataKey}.${user.username}`] || { status: 'SUCCESS', data: [] };
    return acc;
  }, {});
  const currentUser = getCurrentUser(state);
  if (!currentUser) {
    cart.anonymous = state.cart[`${cartMetadataKey}.anonymous`] || { status: 'SUCCESS', data: [] };
  }

  let slug = params.merchant;
  if (!slug) {
    const merchant = getMerchant(state, params);
    slug = merchant && merchant.getIn(['data', 'slug']);
  }
  const isMerchantMatching = !slug || slug === cartMetadata.data.merchant;
  const menu = getMerchantMenu(state, { merchant: cartMetadata.data.merchant });
  const groups = (menu && menu.getIn(['data', 'group'])) || [];

  Object.entries(cart).forEach(([user, cartUser]) => {
    const newData = isMerchantMatching
      ? cartUser.data
        .map((line) => {
          if (line.name) return line;

          const group = iife(() => {
            if (line.groupDishId) return groups.find((g) => g.id === line.groupDishId);
            return groups.find((g) => g.dishes.some((d) => d.id === line.dishId));
          });
          if (!group) return null;
          const dish = group.dishes.find((d) => d.id === line.dishId);
          if (!dish) return null;

          return getOrderLine(dish, line, { groupDishId: group.id, isExtraGroupDish: group.isExtraGroupDish });
        })
        .filter((line) => !!line)
      : [];
    cart[user] = { status: 'SUCCESS', data: newData };
  });

  return { status: 'SUCCESS', data: cart };
}
