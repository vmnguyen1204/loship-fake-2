import { createAction } from '@reduxjs/toolkit';
import qs from 'qs';
import { LINK_API } from 'settings/variables';
import { iife, produce } from 'utils/helper';
import { getClientLocale } from 'utils/i18n';
import * as cookie from 'utils/shims/cookie';

function injectParams(url, { params, query } = {}) {
  if (!url) return console.error('Empty url', url, params);
  let parsedUrl = url.includes('http') ? url : `${LINK_API}${url}`;

  if (params) {
    Object.keys(params).forEach((key) => {
      parsedUrl = parsedUrl.replace('${' + key + '}', params[key]);
    });
    if (parsedUrl.indexOf('$') > -1) console.error('Missing params for url', parsedUrl, params);
  }
  if (query) {
    parsedUrl = parsedUrl + '?' + qs.stringify(query);
  }

  return parsedUrl;
}

export const initialState = produce({});

export function setPending(state, key, { keep } = {}) {
  if (state[key] && keep) state[key] = { ...state[key], status: 'PENDING' };
  state[key] = { status: 'PENDING' };
}

export function setError(state, key, { keep } = {}) {
  if (state[key] && keep) state[key] = { ...state[key], status: 'ERROR' };
  state[key] = { status: 'ERROR' };
}

export function setData(state, key, data, { pagination, refs = [] } = {}) {
  state[key] = { status: 'SUCCESS', data };
  if (pagination) state[key].pagination = pagination;

  refs.forEach((ref) => {
    state[ref] = key;
  });
}

export function appendData(state, key, data, { pagination, refs = [] } = {}){
  state[key] = { ...state[key], status: 'SUCCESS', data: [...state[key]?.data, ...data] };
  if (pagination) state[key].pagination = pagination;

  refs.forEach((ref) => {
    state[ref] = key;
  });
}

export async function callAPI({ method, url, params, query, body, additionalHeaders, accessToken }) {
  const fetchAPI = iife(() => {
    if (typeof fetch !== 'undefined') return fetch;
    return require('node-fetch');
  });

  const queryUrl = injectParams(url, { params, query });

  const res = await fetchAPI(queryUrl, {
    method: method || 'GET',
    headers: {
      'Accept-Language': getClientLocale(),
      'Content-Type': 'application/json',
      'X-Access-Token': accessToken || cookie.get('accessToken'),
      'X-City-ID': query?.cityId ?? 50,
      'X-Lozi-Client': 1,
      ...(additionalHeaders || {}),
    },
    body: body && JSON.stringify(body),
  });

  return res;
}

export function createAsyncAction(type, parser = () => {}, postAction = () => {}) {
  const PENDING = createAction(type + '_PENDING');
  const SUCCESS = createAction(type + '_SUCCESS');
  const FAILED = createAction(type + '_FAILED');

  function actionCreator(params = {}, query = {}) {
    return async (dispatch, getState) => {
      const nParams = await parser({ params, query, dispatch, getState });

      if (!nParams) return;
      await iife(async () => {
        try {
          if (nParams.key && nParams.rootKey && !nParams.loadMore) {
            const state = getState();
            let data = state[nParams.rootKey]?.[nParams.key];
            if (data?.data) data = data.data;

            if (data) {
              if (typeof params.callback === 'function') params.callback({ data });
              return;
            }
          }
          if (nParams.error) {
            dispatch(FAILED(nParams));
            if (typeof params.callback === 'function') params.callback({ error: nParams.error });
            return;
          }
          if (!nParams.url || !!nParams.data) {
            dispatch(SUCCESS(nParams));
            if (typeof params.callback === 'function') params.callback({ data: nParams.data });
            return;
          }
          dispatch(PENDING(nParams));

          const raw = await callAPI({
            method: nParams.method, url: nParams.url,
            params: nParams.params, query: nParams.query, body: nParams.body,
            additionalHeaders: nParams.additionalHeaders,
            accessToken: nParams.accessToken,
          });
          if (raw.status !== 200) throw raw;

          const res = await raw.json();
          if (typeof params.callback === 'function') params.callback(res);
          dispatch(SUCCESS({ ...nParams, ...res }));
        } catch (e) {
          console.error(`Error in createAsyncAction('${type}'):\n`, e);
          if (typeof params.callback === 'function') params.callback({ error: e });
          dispatch(FAILED({ ...nParams, error: { status: e.status, message: e.statusText } }));
        }
      });

      postAction({ params, dispatch, getState });
    };
  }

  return Object.assign(actionCreator, { PENDING, SUCCESS, FAILED });
}

export function getKey(state, key) {
  key = String(key);
  if (!state?.[key]) return key;
  if (typeof state[key] === 'string') return getKey(state, state[key]);

  return key;
}
