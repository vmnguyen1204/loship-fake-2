import { createReducer } from '@reduxjs/toolkit';
import { createAsyncAction, initialState, setData } from 'reducers/new/helper';

export const API = {
  FETCH_GLOBAL_ADDRESS: '/maps/api/global-address',
};

export const fetchGlobalAddress = createAsyncAction('MAP_GLOBAL_ADDRESS_FETCH', ({ params }) => {
  return {
    url: API.FETCH_GLOBAL_ADDRESS,
    key: `geocode.latlng_${params.latlng || '0'}.address_${params.address || 'none'}`,
    rootKey: 'map',
    query: { latlng: params.latlng, address: params.address },
  };
});

// ==============================================================

export default createReducer(initialState, {
  [fetchGlobalAddress.SUCCESS]: (state, action) => {
    const { payload: { key, data } = {} } = action;
    setData(state, key, data);
  },
});

// ==============================================================
