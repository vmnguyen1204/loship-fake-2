import { createReducer } from '@reduxjs/toolkit';
import { callAPI, createAsyncAction, initialState, setData, setError, setPending } from 'reducers/new/helper';

export const API = {
  FETCH_EATERY_CHAIN: '/eatery/chains/${chainIdOrUsername}',
  FETCH_EATERY_CHAIN_EATERIES: '/search/eateries',
  FETCH_EATERY_BY_ID: '/eateries/${eatery}',
};

export const fetchEateryChain = createAsyncAction('EATERY_CHAIN_FETCH', async ({ params }) => {
  if (!params.id && !params.username) return;

  const eateryChainRaw = await callAPI({
    url: API.FETCH_EATERY_CHAIN,
    params: { chainIdOrUsername: `${params.id || `username:${params.username}`}` },
  });
  if (!eateryChainRaw.ok) return {
    id: params.id,
    username: params.username,
    error: eateryChainRaw.status,
  };

  const eateryChainRes = await eateryChainRaw.json();
  const data = eateryChainRes.data;

  const eateryChainEateriesRaw = await callAPI({
    url: API.FETCH_EATERY_CHAIN_EATERIES,
    query: { chainId: data.id },
  });
  if (eateryChainEateriesRaw.ok) {
    const eateryChainEateriesRes = await eateryChainEateriesRaw.json();
    const firstEatery = eateryChainEateriesRes.data && eateryChainEateriesRes.data[0];
    if (firstEatery) {
      const eateryRaw = await callAPI({
        url: API.FETCH_EATERY_BY_ID,
        params: { eatery: firstEatery.id },
      });
      if (eateryRaw.ok) {
        const eateryRes = await eateryRaw.json();
        if (eateryRes.data) data.freeShippingMilestone = eateryRes.data.freeShippingMilestone;
      }
    }
  }

  return {
    data,
    id: params.id,
    username: params.username,
  };
});

// ==============================================================

export default createReducer(initialState, {
  [fetchEateryChain.PENDING]: (state, action) => {
    const { payload: { id, username } = {} } = action;
    setPending(state, `${id || username}.chain`);
  },
  [fetchEateryChain.SUCCESS]: (state, action) => {
    const { payload: { data } = {} } = action;
    if (!data || !data.id) return;

    setData(state, `${data.id}.chain`, data, { refs: [`${data.username}.chain`] });
  },
  [fetchEateryChain.FAILED]: (state, action) => {
    const { payload: { id, username } = {} } = action;
    return setError(state, `${id || username}.chain`);
  },
});

// ==============================================================

export function getKey(state, key) {
  if (!state[key] || typeof state[key] !== 'string') return key;
  return getKey(state, state[key]);
}

export function getEateryChain(state, params) {
  const key = getKey(state.eatery, `${params.id || params.username}.chain`);
  return state.eatery[key] && { ...state.eatery[key] };
}
