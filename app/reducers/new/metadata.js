import { createReducer } from '@reduxjs/toolkit';
import { createAsyncAction, setData } from 'reducers/new/helper';
import { iife, produce } from 'utils/helper';

const initialState = produce({
  globalAddress: {
    data: { status: 'LOADING' },
  },
});

export const API = {
  FETCH_EATERY_CHAIN: '/eatery/chains/${chainIdOrUsername}',
  FETCH_EATERY_CHAIN_EATERIES: '/search/eateries',
  FETCH_EATERY_BY_ID: '/eateries/${eatery}',
};

export const updateGlobalAddress = createAsyncAction('GLOBAL_ADDRESS_FETCH', async ({ params }) => {
  return {
    key: 'globalAddress',
    data: params.data,
  };
});

// ==============================================================

export default createReducer(initialState, {
  [updateGlobalAddress.SUCCESS]: (state, action) => {
    const { payload: { key, data } = {} } = action;
    setData(state, key, data);

    const savedGlobalAddresses = iife(() => {
      try {
        const res = JSON.parse(localStorage.getItem('savedGlobalAddresses'));
        if (Array.isArray(res)) return res;
        return [];
      } catch (e) {
        return [];
      }
    });
    const newSavedGlobalAddresses = JSON.stringify(
      [data].concat(
        savedGlobalAddresses
          .filter((address) => {
            return address.lat !== data.lat || address.lng !== data.lng;
          }),
      ),
    );
    localStorage.setItem('savedGlobalAddresses', newSavedGlobalAddresses);
  },
});

// ==============================================================

export function getGlobalAddress(state) {
  return state.metadataNew.globalAddress;
}

export function getCurrentCity(state, params = {}) {
  const globalAddress = state && getGlobalAddress(state)?.data || params.globalAddress;
  if (!globalAddress) return;

  let { cityId, districtId } = globalAddress;
  districtId = parseInt(districtId);
  return global.availableAreas?.find((area) => {
    if (area.type === 'city' && cityId === area.city.id) return true;
    if (area.type === 'district' && districtId === area.district.id && cityId === area.district.city.id) return true;
    return false;
  });
}
