import { defaultState, register } from 'reducers/helpers';
import { getMerchantKey } from 'reducers/merchant';

const parseData = (data) => {
  // TODO clear when has real backend api
  const newData = data.map((group) => {
    const dishes = group.dishes.map((dish) => {
      const t = Math.random() * 150 + 50; // eslint-disable-line
      return { ...dish, buyCount: parseInt(t, 10) };
    });
    return { ...group, dishes };
  });
  return { group: newData };
};

const { beforeFetch, setData } = register('menu');

export default function MenuReducer(state = defaultState, action) {
  const { data, key } = action;
  switch (action.type) {
    case 'MERCHANT_FETCH_MENU_PENDING':
      return beforeFetch(state, key);
    case 'MERCHANT_FETCH_MENU':
      return setData(state, key, parseData(data), [data.id, data.username]);
    default:
      return state;
  }
}

export function getMerchantMenu(state, params) {
  const merchant = getMerchantKey(state, params);
  const merchantKey = typeof merchant === 'string' ? merchant : params.merchant;
  return state.menu.get(merchantKey);
}
