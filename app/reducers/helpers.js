/* eslint-disable */

import { List, Map } from 'immutable';
import * as identifiers from 'settings/identifiers';

// Need to IMPROVE ALL

function initPending() {
  return new Map({ status: 'PENDING' });
}

function initError() {
  return new Map({ status: 'ERROR' });
}

function init(Type, data, extras, status = 'SUCCESS') {
  return new Map({
    data: typeof data === 'object' ? new Type(data) : data,
    extras: typeof extras === 'object' ? new Map(extras) : extras, // wanted to use new Map(extras) but then again, the immutable.merge let me down: Merging with an object makes the extras become IMMUTABLE COMPLETELY
    status,
  });
}

function reset(state, key) {
  // Reset data of key to pending
  return state.set(key, initPending());
}

function set(Type, state, key, value, extras) {
  // Create/overwrite data of key
  const keyStr = state.get(key);

  if (typeof keyStr === 'string') key = state.get(keyStr);

  return state.set(key, init(Type, value, extras));
}

function update(Type, state, key, value, extras) {
  // Create/Update data of key
  let record = state.get(key);

  if (typeof record === 'string') record = state.get(record);

  if (!record || !record.get('data')) {
    // If empty => create new Map.
    // There is no data with the given key.
    // We need to create a new data space.
    record = init(Type, value, extras);
  } else {
    // If not, merge with old data.
    // There is an existing data of the given key.
    // We need to update it.
    let data = record.get('data');

    if (data && typeof data.merge === 'function') {
      // Process immutable data
      data = data.merge(value);
    } else {
      // Process mutable data
      data = value;
    }

    record = record.merge({
      data,
      extras,
      status: 'SUCCESS',
    });
  }
  return state.set(key, record);
}

function append(state, key, value, extras, reversed, limit) {
  // Create/upxdate data of key
  let record = state.get(key);
  if (!record || !record.get('data')) {
    // If empty => create new List.
    // There is no data with the given key.
    // We need to create a new data space.
    record = init(List, value);
  } else {
    // If not, merge with old data.
    // There is an existing data of the given key.
    // We need to update it.
    let data = record.get('data');
    value = new List(value);

    if (data && typeof data.concat === 'function') {
      if (limit && limit > 0) {
        data = data.slice(1 - limit);
      }
      // Process immutable data
      data = !reversed ? data.concat(value) : value.concat(data);
    } else {
      // Process mutable data
      data = value;
    }

    const newExtras = extras || record.get('extras');
    record = record.merge({
      data,
      extras: typeof newExtras === 'object' ? new Map(newExtras) : newExtras, // read line 38. Again, merging turns half-map into a full-map, which run well on first time but fail on next.
      status: 'SUCCESS',
    });
  }
  return state.set(key, record);
}

// setList(state, key, value)
function setList(...params) {
  return set(List, ...params);
}

function appendList(...params) {
  return append(...params);
}

function get(state, params) {
  const { id } = params;

  if (typeof id !== 'string') return state.get(id.toString());

  return state.get(id);
}

function getWithRef(idRef, state, params) {
  let secondKey = params[idRef];

  const { id } = params;
  if (!secondKey && typeof id !== 'undefined') {
    if (typeof id !== 'string') {
      // we have id here, get secondKey from it
      secondKey = state.get(id.toString());
    } else secondKey = state.get(id);
  }

  if (typeof secondKey === 'string') return state.get(secondKey);

  return secondKey;
}

export function register(name) {
  const idRef = identifiers[name];
  let getFunc;

  if (!idRef) getFunc = (state, params) => get(state[name], params);
  else getFunc = (state, params) => getWithRef(idRef, state[name], params);

  return {
    setData: (...params) => setData(...params),
    setError,
    beforeFetch,
    removeData: (...params) => removeData(...params),
  };
}


export const defaultState = new Map();

export function initMap(...params) {
  return init(Map, ...params);
}

export function initList(...params) {
  return init(List, ...params);
}

// setMap(state, key, value)
export function setMap(...params) {
  return set(Map, ...params);
}

export function updateMap(...params) {
  return update(Map, ...params);
}


export function appendData(state, key, data, ref, extras, reversed, limit) {
  if (ref && ref !== key) state = state.set(ref, key);

  if (!Array.isArray(data)) return state;

  return appendList(state, `${key}`, data, extras, reversed, limit);
}

export function setData(state, key, data, ref, extras, overwrite = true) {
  if (typeof key === 'undefined') return console.warn('Undefined reducer key', data);

  if (!!key && typeof key !== 'string') key = key.toString();

  if (ref) {
    if (Array.isArray(ref)) {
      ref
        .filter((r) => !!r && r !== key && r !== '')
        .forEach((r) => {
          state = state.set(r.toString(), key);
        });
    } else {
      if (typeof ref !== 'string') ref = ref.toString();
      if (ref !== key) state = state.set(ref, key);
    }
  }
  if (Array.isArray(data)) return setList(state, key, data, extras);
  if (!overwrite) return updateMap(state, key, data);
  return setMap(state, key, data);
}

export function beforeFetch(state, key, keepData = false) {
  if (!key) return state;
  if (keepData) {
    let data = state.get(key);
    if (data) {
      data = data.merge(initPending());
      return state.set(key, data);
    }
  }
  return state.set(key, initPending());
}

export function setError(state, key) {
  if (!key) return state;
  return state.set(key, initError());
}

export function getId(state, key, field = 'id') {
  if (!key) return;
  const record = state.get(`${key}`);
  let ret = key;
  // if (typeof record === 'string')
  // Record is string => key is id
  // return key;

  if (typeof record === 'object') {
    // key is key => try to find id in data
    ret = record.getIn(['data', field]) || ret;
  }

  return ret;
}

export function getKey(state, key) {
  if (!key) return;
  const record = state.get(`${key}`);
  let ret;
  if (typeof record === 'string') {
    // Record is string => key is id
    ret = record;
  } else ret = `${key}`;

  // if (typeof record === 'object')
  // key id key
  // return key;
  return ret;
}

export function removeData(state, key) {
  if (typeof key === 'undefined') return console.warn('Cannot remove empty key!');

  if (!!key && typeof key !== 'string') key = key.toString();

  const ref = state.get(key);
  state = state.remove(key);

  if (typeof ref === 'string') state = state.remove(ref);

  return state;
}

export function setFormState(state, key, type, message = '') {
  let defaultMessage;
  switch (type) {
    case 'INIT':
      defaultMessage = '';
      break;
    case 'PENDING':
      defaultMessage = key === 'create' ? 'Creating...' : 'Saving...';
      break;
    case 'FAILED':
      defaultMessage = key === 'create' ? 'Error creating!' : 'Error updating!';
      break;
    case 'SUCCESS':
      defaultMessage = key === 'create' ? 'Created!' : 'Saved!';
      break;
    default:
      break;
  }
  state = state.set(`${key}.status`, type);
  state = state.set(`${key}.message`, message || defaultMessage);
  return state;
}

export function getFormState(state, key) {
  return new Map({
    status: state.get(`${key}.status`),
    message: state.get(`${key}.message`),
  });
}
