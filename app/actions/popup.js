export const DISH_SELECT = 'dish_select';
export const ORDER_FAILED = 'order_failed';
export const LOGIN = 'login';
export const CUSTOM_CONTENT = 'custom_content';
export const STOP_SERVICE_POPUP = 'stop-service-popup';
export const CONFIRM_POPUP = 'confirm-popup';
export const SELECT_PAYMENT_METHOD = 'payment-method';
export const MERCHANT_THOUGHTFUL_POPUP = 'merchant-thoughtful';
export const SELECT_BANK = 'select-bank';

export const MENU_ADD_CATEGORIES = 'menu_add_categories';
export const MENU_UPDATE_GROUP = 'menu_update_group';
export const MENU_GROUP_DISH = 'menu_group_dish';
export const MENU_INDUSTRY = 'menu_industry';
export const MENU_QUICKLY_GROUP_DISH = 'menu_quickly_group_dish';
export const MENU_UPDATE_DISH = 'menu_update_dish';
export const MENU_UPDATE_DISH_CUSTOMS = 'menu_update_dish_customs';
export const MENU_UPDATE_DISH_CUSTOM = 'menu_update_dish_custom';
export const MENU_CLONE_DISH_CUSTOM = 'menu_clone_dish_custom';
export const MENU_UPDATE_DISH_CUSTOM_OPTION = 'menu_update_dish_custom_option';
export const MENU_CLONE_DISH = 'menu_clone_dish';
export const MENU_SEARCH_DISH_IMAGE = 'menu_search_dish_image';
export const MENU_UPDATE_SURGE_PRICE = 'menu_update_surge_price';
export const MENU_UPDATE_FRANCHISED = 'menu_update_franchised';
export const MENU_REMOVE_DISH = 'menu_remove_dish';
export const MENU_WAREHOUSING_DISH = 'menu_warehousing_dish';
export const MENU_STOCK_DISH_HISTORY = 'menu_stock_dish_history';

export const RATING_POPUP = '@popup/RATING_POPUP';
export const MERCHANT_RATING_POPUP = '@popup/MERCHANT_RATING_POPUP';

export const UPDATE_AVATAR = '@popup/UPDATE_AVATAR';
export const PHOTO_POPUP = '@popup/PHOTO_POPUP';

export const GROUP_ORDER_INVITE = '@popup/GROUP_ORDER_INVITE';
export const GROUP_ORDER_MEMBER_LIST = '@popup/GROUP_ORDER_MEMBER_LIST';
export const SHARING_POPUP = '@popup/SHARING_POPUP';

export const GMAP_POPUP = '@popup/GMAP_POPUP';
export const PROFILE_UPDATE_INFO = '@popup/PROFILE_UPDATE_INFO';
export const PROFILE_UPDATE_PHONE = '@popup/PROFILE_UPDATE_PHONE';
export const PROFILE_CHANGE_PASSWORD = '@popup/PROFILE_CHANGE_PASSWORD';
export const GENERIC_POPUP = '@popup/GENERIC_POPUP';

export function open(key, params) {
  return {
    type: 'POPUP_OPEN',
    key,
    params,
  };
}

export function close(key) {
  return (dispatch, getState) => {
    const state = getState();
    const popup = state.popup.get(key);
    if (!popup || popup.get('status') !== 'SUCCESS') return;
    return dispatch({
      type: 'POPUP_CLOSE',
      key,
    });
  };
}
