export function expire(params) {
  return {
    type: 'ALERT_EXPIRE',
    key: params.alert,
  };
}

export function destroy(params) {
  return {
    type: 'ALERT_DESTROY',
    key: params.alert,
  };
}

export function login() {
  return { type: 'ALERT_LOGIN' };
}

export function loginToComment() {
  return { type: 'ALERT_LOGIN_TO_COMMENT' };
}

export function generatePromote() {
  return { type: 'ALERT_GENERATE_PROMOTE' };
}

export function alertText(text) {
  return {
    type: 'ALERT_TEXT',
    data: text,
  };
}

export function warnDishSelect(message) {
  return {
    type: 'DISH_SELECT_WARNING',
    message,
  };
}
