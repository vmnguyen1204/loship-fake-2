import { LINK_LZI } from 'settings/variables';

import { register } from 'actions/helpers';
import request from 'utils/shims/request';

const { getAPI, postAPI } = register('buy');

export const API = {
  FETCH: '/dishes/block-item-offer',

  FETCH_RAW_LINK: LINK_LZI + '/${sharedId}/raw',
  FETCH_ITEM_BY_ID: '/dishes/${dishId}',
  CREATE_SHORT_LINK: LINK_LZI + '/short-links',
};

export function fetchBlockItemOfferByIds(params = {}) {
  const { blockId, offerItemId, callback, callbackFailure, accessToken } = params;
  const body = {
    blockIds: blockId && [parseInt(blockId)],
    offerItemIds: offerItemId && [parseInt(offerItemId)],
  };

  return postAPI({
    type: 'FETCH_BLOCKS_ITEM_OFFERS',
    url: API.FETCH,
    key: 'all',
    content: { ...body },
    callback,
    callbackFailure,
    accessToken,
  });
}

export function fetchRawLinkBySharedId(params = {}) {
  const { sharedId, callback, callbackFailure } = params;
  return getAPI({
    type: 'FETCH_RAW_LINK_BY_SHARED_ID',
    url: API.FETCH_RAW_LINK,
    key: `${sharedId}.raw`,
    params: { sharedId },
    callback,
    callbackFailure,
  });
}

export function fetchItemById(params = {}) {
  const { callback, callbackFailure } = params;
  if (!params.shareId && !params.itemId) return;

  return async (dispatch, getState) => {
    const itemId = await (async () => {
      if (params.itemId) return params.itemId;

      const rawData = await new Promise((resolve) => {
        try {
          dispatch(
            fetchRawLinkBySharedId({
              sharedId: params.shareId,
              callback: (res) => {
                resolve(res.body.data);
              },
              callbackFailure: (e) => {
                console.error('ERROR: ', e);
                resolve();
              },
            }),
          );
        } catch (e) {
          console.error('ERROR: ', e);
          resolve();
        }
      });

      const rawItemId = rawData && rawData.rawLink.split('/').pop();
      return Buffer.from(rawItemId || '', 'base64').toString();
    })();

    if (!itemId || !itemId.match(/^[a-zA-Z0-9]+$/)) {
      return console.error('Invalid item id', itemId);
    }

    return getAPI({
      type: 'FETCH_ITEM_BY_ID',
      url: API.FETCH_ITEM_BY_ID,
      key: `${itemId}.item`,
      params: { dishId: itemId },
      callback,
      callbackFailure,
      shareId: params.shareId,
    })(dispatch, getState);
  };
}

export function generateShortLinkSharing({ rawUrl, fallbackUrl, baseSharingUrl, callback }) {
  if (!rawUrl) return;
  return async () => {
    try {
      const shortLink = await request
        .post(API.CREATE_SHORT_LINK)
        .send({ url: rawUrl })
        .end();
      const shortLinkData = JSON.parse(shortLink.text);
      typeof callback === 'function' && callback(`${baseSharingUrl}/${shortLinkData.data.id}`);
    } catch (e) {
      console.error('ERROR: cannot generate short link, fallback to fallbackUrl');
      typeof callback === 'function' && callback(fallbackUrl);
    }
  };
}
