import * as sentry from '@sentry/react';
import { callAPI } from 'lib/api';
import { FB_APP_ID, LINK_DOMAIN, SENTRY_BASE_URL, SENTRY_DSN } from 'settings/variables';
import { sha256 } from 'utils/data';
import * as cookie from 'utils/shims/cookie';

export function loadFacebook() {
  if (window.FB) return window.FB;

  return new Promise((resolve) => {
    const firstScriptElement = document.getElementsByTagName('script')[0];

    window.fbAsyncInit = () => {
      const facebook = window.FB;

      facebook.init({
        appId: FB_APP_ID,
        version: 'v2.12',
      });
      resolve(facebook);
    };

    const locale = cookie.get('locale') || 'vi_VN';
    const scriptElement = document.createElement('script');
    scriptElement.id = 'facebook-jssdk';
    scriptElement.src = '//connect.facebook.net/' + locale + '/sdk.js#xfbml=1&version=v2.8';

    firstScriptElement.parentNode.insertBefore(scriptElement, firstScriptElement);
  });
}

export function loadSentry() {
  const { version } = require('../../package.json');
  sentry.init({
    dsn: SENTRY_DSN,
    release: version,
    environment: process.env.NODE_ENV,
    beforeSend: async (event, hint) => {
      const { event_id: eventId, originalException = {} } = hint || {};

      if (originalException.message.includes('zaloJSV2 is not defined')) return null;
      if (['getReadModeRender', 'getReadModeExtract', 'getReadModeConfig'].some(
        (blockKeyword) => originalException.message.includes(blockKeyword),
      )) return null;

      const errorStack = originalException.stack.split('\n');
      const errorStr = `*${originalException.message}*\n_${errorStack[1].trim()}_`;
      const hash = await sha256(errorStr);
      const message = `An unexpected error occurred:\n\n\`\`\`\n${errorStr}\n\`\`\`\nMore info: [Click here](${SENTRY_BASE_URL + eventId})`;
      const error = { message, hash };

      const res = await callAPI('post', { url: LINK_DOMAIN + '/er', content: error });
      if (res.status !== 200) return null;

      return event;
    },
  });
}

export function loadSDKs() {
  loadFacebook();
}

export function sendGAEvent(eventCategory, eventAction, eventLabel) {
  if (!window || !window.gtag) return;
  window.gtag('event', eventAction, {
    event_category: eventCategory,
    event_label: eventLabel,
  });
}

export function sendPageView() {
  if (!window || !window.gtag) return;
  window.gtag('event', 'page_view');
}
