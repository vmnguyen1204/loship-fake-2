import request from 'utils/shims/request';
import * as cookie from 'utils/shims/cookie';

import { API_CALL } from 'lib/apiMiddleware';
import { iife } from 'utils/helper';

function shouldFetch(state, key, storeName) {
  return !state[storeName].get(key) || state[storeName].getIn([key, 'data', 'missingData']);
}

function generateApiCall(method, options) {
  return {
    [API_CALL]: {
      ...options,
      method,
    },
  };
}

function getAPI(name, options) {
  return (dispatch, getState) => {
    const state = getState();
    const modelName = options.store || name;

    const parsedOptions = iife(() => {
      if (typeof options === 'function') return options(state);
      return options;
    });

    if (!parsedOptions.killAction) {
      if (parsedOptions.force || !parsedOptions.key || shouldFetch(getState(), parsedOptions.key, modelName)) {
        return dispatch(generateApiCall('get', parsedOptions));
      }
    }
  };
}

function getMoreAPI(options) {
  return (dispatch, getState) => {
    const state = getState();

    const parsedOptions = iife(() => {
      if (typeof options === 'function') return options(state);
      return options;
    });

    if (!parsedOptions) return false;
    return dispatch(generateApiCall('get', parsedOptions));
  };
}

function postAPI(options) {
  return (dispatch, getState) => {
    const state = getState();

    const parsedOptions = iife(() => {
      if (typeof options === 'function') return options(state);
      return options;
    });

    if (!parsedOptions) return;

    return dispatch(generateApiCall('post', parsedOptions));
  };
}

function putAPI(options) {
  return (dispatch, getState) => {
    const state = getState();

    const parsedOptions = iife(() => {
      if (typeof options === 'function') return options(state);
      return options;
    });

    return dispatch(generateApiCall('put', parsedOptions));
  };
}

function delAPI(options) {
  return (dispatch, getState) => {
    const state = getState();

    const parsedOptions = iife(() => {
      if (typeof options === 'function') return options(state);
      return options;
    });

    return dispatch(generateApiCall('del', parsedOptions));
  };
}

// Inject params to url
export function injectParams(url, params) {
  if (!url) return console.error('Empty url', url, params);

  if (!params) return url;

  let parsedUrl = url;
  Object.keys(params).forEach((key) => {
    parsedUrl = parsedUrl.replace('${' + key + '}', params[key]);
  });

  if (parsedUrl.indexOf('$') > -1) return console.error('Missing params for url', parsedUrl, params);

  return parsedUrl;
}

export function register(name) {
  return {
    getAPI: (options) => getAPI(name, options),
    getMoreAPI,
    putAPI,
    postAPI,
    delAPI,
  };
}

export default function callAPI(method, options) {
  const { url, params, query, content, accessToken: accessTokenParam, additionalHeaders = {} } = options;
  let accessToken = accessTokenParam || cookie.get('accessToken') || 'unknown';
  const link = injectParams(url, params);

  if (options.accessToken) {
    accessToken = options.accessToken;
  }

  const req = request[method](link)
    .query(query)
    .send(content)
    .accessToken(accessToken)
    .additionalHeaders(additionalHeaders);

  // set X-City-ID header if specified (usually in newsfeed and search actions)
  if (query && query.cityId) req.set('X-City-ID', query.cityId);

  return req.end(options.end);
}
