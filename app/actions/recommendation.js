import { getKey } from 'reducers/helpers';
import { register } from 'actions/helpers';

const { getAPI } = register('recommendation');

export const API = { FETCH_EATERY_SUGGEST_DISHES: '/eateries/${eatery}/suggest/dishes' };

export function fetchEaterySuggestDishes(params) {
  if (!params.merchantUsername && !params.merchant && !params.eateryId) return;

  return async (dispatch, getState) => {
    const merchantKey = getKey(
      getState().merchant, params.merchantUsername || params.merchant || params.eateryId,
    );

    return getAPI({
      type: 'FETCH_EATERY_SUGGEST_DISHES',
      key: `${merchantKey}.dishes`,
      url: API.FETCH_EATERY_SUGGEST_DISHES,
      params: {
        eatery:
          (params.merchantUsername && `username:${params.merchantUsername}`) ||
          (params.merchant && `slug:${params.merchant}`) ||
          params.eateryId,
      },
      query: { dishIds: (params.dishIds || []).join(',') },
      force: true,
    })(dispatch, getState);
  };
}
