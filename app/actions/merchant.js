import callAPI, { register } from 'actions/helpers';
import { getKey } from 'reducers/helpers';
import { getCurrentCity } from 'reducers/new/metadata';
import { APP_INFO, LINK_API, LINK_API_MANAGER, LINK_AUTH } from 'settings/variables';
import { iife } from 'utils/helper';
import { getClientLocale } from 'utils/i18n';
import loziServices from 'utils/json/loziServices';

const { getAPI, getMoreAPI, postAPI, delAPI } = register('merchant');

export const API = {
  FETCH: '/eateries/slug:${merchant}',
  FETCH_BY_USERNAME: '/eateries/username:${username}',
  FETCH_BY_ID: '/eateries/${eateryId}',

  FETCH_SHIP_PRICE: '/eateries/slug:${merchant}',
  FETCH_SHIP_PRICE_BY_USERNAME: '/eateries/username:${username}',
  FETCH_ORDER_COUNT: '/eateries/slug:${merchant}/ship',
  FETCH_MENU: '/eateries/slug:${merchant}/menu',
  FETCH_ID: '/eateries/${merchant}',
  FETCH_MERCHANT_THOUGHTFUL: '/eateries/${eateryId}/supply-items',
  FETCH_BLOCK_IMAGES: LINK_AUTH + '/eateries/slug:${merchant}/block-images',
  FETCH_MERCHANT_BRAND: LINK_AUTH + '/eateries/slug:${merchant}/branches',
  FETCH_MERCHANT_BRAND_BY_USERNAME: LINK_AUTH + '/eateries/username:${username}/branches',

  FETCH_SHIPPING_FEE: '/eateries/${eatery}/shipping-fees',
  FETCH_PAYMENT_FEE: '/eateries/${eatery}/payment-fees',
  FETCH_SHIPPING_FEE_BY_SERVICE: '/services/${serviceName}/shipping-fees',
  FETCH_PAYMENT_FEE_BY_SERVICE: '/services/${serviceName}/payment-fees',
  FETCH_PAYMENT_BANK: '/payment/methods/method:${paymentMethod}/banks',
  FETCH_PAYMENT_CARD_FEES: '/users/me/payment-card/fees',

  CHECK_PROMOTION: '/promotions',
  FETCH_PROMOTION_BY_CODE: '/promotions/code:${code}',
  FETCH_CAMPAIGN_BY_ID: '/promotion/campaigns/${campaignId}',

  UPDATE_LIKE: '/eateries/${eatery}/likes',
};

export async function fetchMerchantShipPrice(params) {
  if (!params.merchant && !params.merchantUsername) return false;
  const username = params.merchantUsername;

  const query = iife(() => {
    const globalAddress = params.globalAddress;
    if (globalAddress?.lat && globalAddress?.lng) {
      return { lat: globalAddress.lat, lng: globalAddress.lng };
    }
  });

  const res = username
    ? await callAPI('get', {
      url: API.FETCH_SHIP_PRICE_BY_USERNAME,
      params: { username },
      query,
      accessToken: params.accessToken,
      additionalHeaders: params.additionalHeaders,
    })
    : await callAPI('get', {
      url: API.FETCH_SHIP_PRICE,
      params: { merchant: params.merchant },
      query,
      accessToken: params.accessToken,
      additionalHeaders: params.additionalHeaders,
    });

  if (!res || res.status !== 200) return;
  return res.body.data;
}

export function fetchMerchant(params) {
  if (!params.merchant && !params.merchantUsername && !params.eateryId) return false;
  const username = params.merchantUsername;

  const { callback, ...rest } = params;

  return async (dispatch, getState) => {
    const store = getState();
    let mcKey = getKey(store.merchant, username || params.merchant);
    const mc = store.merchant.get(mcKey);
    if (mc && mc.get('status') === 'PENDING') return;
    if (!params.force && mc && mc.get('status') === 'SUCCESS') {
      typeof callback === 'function' && callback(mc.toJS().data);
      return;
    }

    // dispatch({
    //   type: 'MERCHANT_FETCH_PENDING',
    //   key: params.merchant || username || params.eateryId,
    // });

    let data = {};

    const fetchMerchantInfo = await (async () => {
      if (username) {
        return callAPI('get', {
          url: LINK_AUTH + API.FETCH_BY_USERNAME,
          params: { username },
          accessToken: params.accessToken,
          // additionalHeaders: params.additionalHeaders,
        });
      }
      if (params.merchant) {
        return callAPI('get', {
          url: LINK_AUTH + API.FETCH,
          params: { merchant: params.merchant },
          accessToken: params.accessToken,
          // additionalHeaders: params.additionalHeaders,
        });
      }

      return callAPI('get', {
        url: LINK_AUTH + API.FETCH_BY_ID,
        params: { eateryId: params.eateryId },
        accessToken: params.accessToken,
        // additionalHeaders: params.additionalHeaders,
      });
    })();

    if (fetchMerchantInfo.statusCode !== 200) {
      return dispatch({
        type: 'MERCHANT_FETCH_FAILED',
        key: params.merchant || username || params.eateryId,
      });
    }

    data = { ...fetchMerchantInfo.body.data }; // save merchant info
    if (params.eateryId) mcKey = getKey(store.merchant, data.username ? data.username : data.slug);

    const shipPrice = await fetchMerchantShipPrice({
      ...rest,
      username: data.username,
      merchant: data.slug,
    });
    if (!shipPrice) {
      dispatch({ type: 'MERCHANT_FETCH', data });
      typeof callback === 'function' && callback(data);
      return;
    }

    data = {
      ...fetchMerchantInfo.body.data,
      ...shipPrice,
      categories: shipPrice.categories,
      count: { ...data.count, ...shipPrice.count },
    }; // save merchant shipping info

    const fetchMerchantBrand = data.username
      ? await callAPI('get', {
        url: API.FETCH_MERCHANT_BRAND_BY_USERNAME,
        params: { username: data.username },
        accessToken: params.accessToken,
      })
      : await callAPI('get', {
        url: API.FETCH_MERCHANT_BRAND,
        params: { merchant: data.slug },
        accessToken: params.accessToken,
      });

    if (fetchMerchantBrand.statusCode !== 200) {
      dispatch({ type: 'MERCHANT_FETCH', data });
      typeof callback === 'function' && callback(data);
      return;
    }

    data.brand = fetchMerchantBrand.body.data;

    dispatch({ type: 'MERCHANT_FETCH', data });
    typeof callback === 'function' && callback(data);
  };
}

export function fetchMerchantManager(params) {
  if (!params.merchant && !params.merchantUsername && !params.eateryId) return false;
  const username = params.merchantUsername;

  return async (dispatch, getState) => {
    const store = getState();
    const mcKey = getKey(store.merchant, username || params.merchant);
    const mc = store.merchant.get(mcKey);
    if (mc && mc.get('status') === 'PENDING') return;

    let data = {};

    const fetchMerchantInfo = await (async () => {
      if (username) {
        return callAPI('get', {
          url: LINK_API_MANAGER + API.FETCH_BY_USERNAME,
          params: { username },
          accessToken: params.accessToken,
          // additionalHeaders: params.additionalHeaders,
        });
      }
      if (params.merchant) {
        return callAPI('get', {
          url: LINK_API_MANAGER + API.FETCH,
          params: { merchant: params.merchant },
          accessToken: params.accessToken,
          // additionalHeaders: params.additionalHeaders,
        });
      }

      return callAPI('get', {
        url: LINK_API_MANAGER + API.FETCH_BY_ID,
        params: { eateryId: params.eateryId },
        accessToken: params.accessToken,
        // additionalHeaders: params.additionalHeaders,
      });
    })();

    if (fetchMerchantInfo.statusCode !== 200) {
      return dispatch({
        type: 'MERCHANT_MANAGER_FETCH_FAILED',
        key: params.merchant || username || params.eateryId,
      });
    }

    data = { ...fetchMerchantInfo.body.data }; // save merchant info
    dispatch({ type: 'MERCHANT_MANAGER_FETCH', data });
    typeof params.callback === 'function' && params.callback(data);
  };
}

export function fetchMerchantListItem({ params }) {
  if (!params.merchant && !params.merchantUsername && !params.eateryId) return false;
  const eateryId = params.merchantUsername;
  return async (dispatch, getState) => {
    const store = getState();
    const mcKey = getKey(store.merchant, eateryId || params.merchant);
    const mc = store.merchant.get(mcKey);
    if (mc && mc.get('status') === 'PENDING') return;
    const res = await callAPI('get', {
      url: LINK_API + API.FETCH_MERCHANT_THOUGHTFUL,
      params: { eateryId: eateryId },
    });

    dispatch({
      type:'MERCHANT_THOUGHTFUL_FETCH',
      key: mcKey +'.supplied',
      data: res.body.data,
    });
  };
}
export function fetchMerchantRatings(params) {
  if (!params.merchant && !params.merchantUsername && !params.eateryId) return false;
  const username = params.merchantUsername;

  return async (dispatch, getState) => {
    const store = getState();
    const mcKey = getKey(store.merchant, username || params.merchant);
    const mc = store.merchant.get(mcKey);
    if (mc && mc.get('status') === 'PENDING') return;

    dispatch({
      type: 'MERCHANT_FETCH_RATINGS_PENDING',
      key: mcKey,
    });

    const data = {
      data: [],
      extras: {},
    };

    const fetchMerchantInfo = await (async () => {
      if (params.nextUrl) {
        return callAPI('get', {
          url: params.nextUrl,
          accessToken: params.accessToken,
          // additionalHeaders: params.additionalHeaders,
        });
      }

      if (username) {
        return callAPI('get', {
          url: API.FETCH_BY_USERNAME + '/ratings',
          params: { username },
          accessToken: params.accessToken,
        // additionalHeaders: params.additionalHeaders,
        });
      }
      if (params.merchant) {
        return callAPI('get', {
          url: API.FETCH + '/ratings',
          params: { merchant: params.merchant },
          accessToken: params.accessToken,
        // additionalHeaders: params.additionalHeaders,
        });
      }

      return callAPI('get', {
        url: API.FETCH_BY_ID + '/ratings',
        params: { eateryId: params.eateryId },
        accessToken: params.accessToken,
        // additionalHeaders: params.additionalHeaders,
      });
    })();

    if (fetchMerchantInfo.statusCode !== 200) {
      return dispatch({
        type: 'MERCHANT_FETCH_RATINGS_FAILED',
        key: mcKey,
      });
    }

    data.data = fetchMerchantInfo.body.data;
    data.extras = fetchMerchantInfo.body.pagination;
    dispatch({ type: params.nextUrl ? 'MERCHANT_FETCH_MORE_RATINGS' : 'MERCHANT_FETCH_RATINGS', key: mcKey, ...data });
    typeof params.callback === 'function' && params.callback(data);
  };
}

export function fetchMerchantMenu(params) {
  if (!params.merchant) return false;
  const currentLocale = getClientLocale();
  return getAPI({
    type: 'MERCHANT_FETCH_MENU',
    url: API.FETCH_MENU,
    store: 'menu',
    key: params.merchant,
    params: { merchant: params.merchant },
    additionalHeaders: { 'X-Language': currentLocale === 'vi_VN' ? 'vi' : 'en' },
  });
}

export function fetchMerchantOrderCount(params) {
  if (!params.merchant) return false;
  return getAPI({
    type: 'MERCHANT_FETCH_ORDER_COUNT',
    url: API.FETCH_ORDER_COUNT,
    key: params.merchant,
    params: { merchant: params.merchant },
  });
}

export function fetchBlockImages(params) {
  return getAPI((state) => {
    const merchantKey = getKey(state.merchant, params.merchant);

    return {
      type: 'MERCHANT_FETCH_BLOCK_IMAGES',
      key: `${merchantKey}.blockImages`,
      url: API.FETCH_BLOCK_IMAGES,
      params: { merchant: merchantKey },
      query: params.query,
    };
  });
}

export function fetchMerchantLoshipById(params) {
  const merchantKey = `${params.merchant}.loship`;

  return getAPI({
    type: 'FETCH_MERCHANT_LOSHIP_BY_ID',
    key: merchantKey,
    url: API.FETCH_ID,
    params: { merchant: params.merchant },
  });
}

export function fetchShippingFees(params) {
  return async (dispatch, getState) => {
    const state = getState();

    const currentCity = getCurrentCity(state);
    const city = iife(() => {
      if (!currentCity) return {};
      if (currentCity.type === 'city') {
        return { cityId: currentCity.city.id };
      }
      return {
        cityId: currentCity.district.city.id,
        districtId: currentCity.district.id,
      };
    });

    const service = loziServices.find((s) => s.name === params.serviceName) ||
      loziServices.find((s) => s.name === APP_INFO.serviceName);

    const res = await (async () => {
      if (params.merchant || params.merchantUsername || params.eateryId) {
        const eatery = (() => {
          if (params.merchantUsername) return `username:${params.merchantUsername}`;
          if (params.merchant) return `slug:${params.merchant}`;
          return params.eateryId;
        })();

        return callAPI('get', {
          url: API.FETCH_SHIPPING_FEE,
          params: { eatery },
          query: { distance: params.distance },
          additionalHeaders: { 'X-Loship-Service': service && service.appApiHeader },
        });
      }

      if (params.serviceName) {
        return callAPI('get', {
          url: API.FETCH_SHIPPING_FEE_BY_SERVICE,
          params: { serviceName: params.serviceName },
          query: { distance: params.distance, ...city },
        });
      }
    })();

    if (!res) return;
    typeof params.callback === 'function' && params.callback(res);
    return res;
  };
}

export function fetchPaymentFees(params) {
  return async (dispatch, getState) => {
    const state = getState();

    const currentCity = getCurrentCity(state);
    const city = iife(() => {
      if (!currentCity) return {};
      if (currentCity.type === 'city') {
        return { cityId: currentCity.city.id };
      }
      return {
        cityId: currentCity.district.city.id,
        districtId: currentCity.district.id,
      };
    });

    const service = loziServices.find((s) => s.name === params.serviceName) ||
      loziServices.find((s) => s.name === APP_INFO.serviceName);

    const res = await (async () => {
      if (params.merchant || params.merchantUsername || params.eateryId) {
        const eatery = (() => {
          if (params.merchantUsername) return `username:${params.merchantUsername}`;
          if (params.merchant) return `slug:${params.merchant}`;
          return params.eateryId;
        })();

        return callAPI('get', {
          url: API.FETCH_PAYMENT_FEE,
          params: { eatery },
          additionalHeaders: { 'X-Loship-Service': service && service.appApiHeader },
        });
      }

      if (params.serviceName) {
        return callAPI('get', {
          url: API.FETCH_PAYMENT_FEE_BY_SERVICE,
          params: { serviceName: params.serviceName },
          query: { ...city },
        });
      }
    })();

    if (!res) return;
    typeof params.callback === 'function' && params.callback(res);
    return res;
  };
}

export async function fetchPaymentBank(params) {
  if (!params.paymentMethod) return;

  const res = await callAPI('get', {
    url: API.FETCH_PAYMENT_BANK,
    params: { paymentMethod: params.paymentMethod },
  });

  typeof params.callback === 'function' && params.callback(res);
  return res;
}

export function fetchPaymentCardFees(params) {
  return async (dispatch, getState) => {
    const state = getState();

    const currentCity = getCurrentCity(state);
    const city = iife(() => {
      if (!currentCity) return {};
      if (currentCity.type === 'city') {
        return { cityId: currentCity.city.id };
      }
      return {
        cityId: currentCity.district.city.id,
        districtId: currentCity.district.id,
      };
    });

    const service = loziServices.find((s) => s.name === params.serviceName) ||
      loziServices.find((s) => s.name === APP_INFO.serviceName);

    const res = await callAPI('get', {
      url: API.FETCH_PAYMENT_CARD_FEES,
      query: { eateryId: params.eateryId, serviceName: service.name, ...city },
      additionalHeaders: { 'X-Loship-Service': service && service.appApiHeader },
    });

    if (!res) return;
    typeof params.callback === 'function' && params.callback(res);
    return res;
  };
}

export function checkPromotion(params) {
  if (!params.promotionCode) return;
  return postAPI({
    ...(params.mode === 'SEARCH'
      ? { type: 'MERCHANT_PROMOTION_SEARCH', key: 'promo.search' }
      : { type: 'MERCHANT_PROMOTION_FETCH', key: `${params.promotionCode}.promo` }),
    url: API.CHECK_PROMOTION,
    content: { code: params.promotionCode, eateryId: params.eateryId },
    force: true,
  });
}

export function fetchPromotionByCode(params) {
  if (!params.promotionCode) return;
  return getAPI({
    ...(params.mode === 'SEARCH'
      ? { type: 'MERCHANT_PROMOTION_SEARCH', key: 'promo.search' }
      : { type: 'MERCHANT_PROMOTION_FETCH', key: `${params.promotionCode}.promo` }),
    url: API.FETCH_PROMOTION_BY_CODE,
    params: { code: params.promotionCode },
    force: true,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function fetchPromotionListByEateryId(params) {
  if (!params.eateryId) return;

  if (params.nextUrl) {
    return getMoreAPI({
      type: 'MERCHANT_PROMOTIONS_FETCH',
      key: `${params.eateryId}.promo`,
      url: params.nextUrl,
      isAppend: true,
      childKeyPostfix: '.promo',
    });
  }

  return getAPI({
    type: 'MERCHANT_PROMOTIONS_FETCH',
    key: `${params.eateryId}.promo`,
    url: API.FETCH_BY_ID + '/promotions',
    params: { eateryId: params.eateryId },
    childKeyPostfix: '.promo',
  });
}

export function fetchCampaignById(params) {
  if (!params.campaignId) return;
  return getAPI({
    type: 'MERCHANT_CAMPAIGN_FETCH',
    key: `${params.campaignId}.campaign`,
    url: API.FETCH_CAMPAIGN_BY_ID,
    params: { campaignId: params.campaignId },
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function fetchCampaignListByEateryId(params) {
  if (!params.eateryId) return;

  // TODO: fetch all campaigns
  if (params.nextUrl) {
    return getMoreAPI({
      type: 'MERCHANT_CAMPAIGNS_FETCH',
      key: `${params.eateryId}.campaigns`,
      url: params.nextUrl,
      isAppend: true,
      childKeyPostfix: '.campaign',
    });
  }

  return getAPI({
    type: 'MERCHANT_CAMPAIGNS_FETCH',
    key: `${params.eateryId}.campaigns`,
    url: API.FETCH_BY_ID + '/promotion/campaigns',
    params: { eateryId: params.eateryId },
    childKeyPostfix: '.campaign',
  });
}

export function updateLikeEatery(params) {
  if (!params.merchant && !params.merchantUsername && !params.eateryId) return false;
  const username = params.merchantUsername;

  return async (dispatch, getState) => {
    const store = getState();

    const mcKey = getKey(store.merchant, params.merchant || username || params.eateryId);
    const mc = store.merchant.get(mcKey);
    if (mc && mc.get('status') === 'PENDING') return;

    return postAPI({
      type: 'MERCHANT_LIKE',
      key: mcKey,
      url: API.UPDATE_LIKE,
      params: iife(() => {
        if (username) return { eatery: `username:${username}` };
        if (params.merchant) return { eatery: `slug:${params.merchant}` };
        return { eatery: params.eateryId };
      }),
      callback: params.callback,
    })(dispatch, getState);
  };
}

export function deleteLikeEatery(params) {
  if (!params.merchant && !params.merchantUsername && !params.eateryId) return false;
  const username = params.merchantUsername;

  return async (dispatch, getState) => {
    const store = getState();

    const mcKey = getKey(store.merchant, params.merchant || username || params.eateryId);
    const mc = store.merchant.get(mcKey);
    if (mc && mc.get('status') === 'PENDING') return;

    return delAPI({
      type: 'MERCHANT_DISLIKE',
      key: mcKey,
      url: API.UPDATE_LIKE,
      params: iife(() => {
        if (username) return { eatery: `username:${username}` };
        if (params.merchant) return { eatery: `slug:${params.merchant}` };
        return { eatery: params.eateryId };
      }),
      callback: params.callback,
      currentKey: params.currentKey, // Use for remove data from redux store
      eateryId: params.eateryId,
    })(dispatch, getState);
  };
}
