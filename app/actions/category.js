import { register } from 'actions/helpers';
import { getCurrentCity } from 'reducers/new/metadata';
import { APP_INFO, LINK_AUTH } from 'settings/variables';
import { getCityByIdOrNameOrSlug } from 'utils/data';

const { getAPI: getCategory } = register('category');
const { getAPI: getCoreCategory } = register('coreCategory');

export function fetchCategories(params = {}) {
  return async (dispatch, getState) => {
    const currentCity = getCurrentCity(getState());
    if (!currentCity) return;

    const query = {
      superCategoryId: APP_INFO.shipServiceId,
      cityId: currentCity.type === 'city' ? currentCity.city.id : currentCity.district.city.id,
      districtId: currentCity.type === 'district' ? currentCity.district.id : undefined,
    };

    return getCategory({
      type: 'CATEGORY_FETCH_ALL',
      key: `all.c_${query.cityId}.s_${query.superCategoryId}`,
      url: '/categories',
      query,
      callback: params.callback,
    })(dispatch, getState);
  };
}

export function fetchCategoriesBySuperCategoryCityIdDistrictId(params = {}) {
  return async (dispatch, getState) => {
    const currentCity = getCityByIdOrNameOrSlug(params.cityId);
    if (!currentCity) return;

    const query = {
      superCategoryId: params.superCategoryId,
      cityId: currentCity.type === 'city' ? currentCity.city.id : currentCity.district.city.id,
      districtId: currentCity.type === 'district' ? currentCity.district.id : undefined,
    };

    return getCategory({
      type: 'CATEGORY_FETCH_ALL',
      key: `all.c_${query.cityId}.s_${query.superCategoryId}`,
      url: '/categories',
      query,
      callback: params.callback,
    })(dispatch, getState);
  };
}

export function fetchCoreCategories(params = {}) {
  return getCoreCategory({
    type: 'CORE_CATEGORY_FETCH_ALL',
    key: 'all',
    url: LINK_AUTH + '/categories',
    callback: params.callback,
  });
}
