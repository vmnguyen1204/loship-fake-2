import { LINK_AUTH } from 'settings/variables';
import callAPI from 'actions/helpers';
import uaParser from 'ua-parser-js';

export const API = {
  AUTH_PHONE_INITIAL: LINK_AUTH + '/auth/register/phone/initial',
  AUTH_PHONE: LINK_AUTH + '/auth/register/phone',
  AUTH_FACEBOOK: LINK_AUTH + '/auth/facebook/login',
  AUTH_BY_TOKEN: LINK_AUTH + '/users/me',

  OPEN_ACCOUNT_FACEBOOK: LINK_AUTH + '/auth/open-account/facebook',
};

export function getClientInfo() {
  let device, platform;
  if (typeof navigator !== 'undefined' && navigator.userAgent) {
    const ua = uaParser(navigator.userAgent);
    device = `${ua.os.name} ${ua.os.version}`;
    platform = `${ua.browser.name}/${ua.browser.version}`;
  }
  return { device, platform };
}

export async function initialPhone({ countryCode, phoneNumber, callback, callbackFailure }) {
  const formData = {
    ...getClientInfo(),
    countryCode,
    phoneNumber,
  };

  const res = await callAPI('post', {
    url: API.AUTH_PHONE_INITIAL,
    content: formData,
  });

  if ([200, 409].includes(res.status)) {
    return callback({ countryCode, phoneNumber, status: res.status });
  }
  return callbackFailure({ status: res.status, message: res.text });
}

export async function loginByPhone({
  countryCode, phoneNumber, password,
  callback, callbackFailure,
}) {
  const formData = {
    ...getClientInfo(),
    countryCode,
    phoneNumber,
    password,
  };

  const res = await callAPI('post', {
    url: API.AUTH_PHONE,
    content: formData,
  });

  if (res.status === 200) {
    return callback({
      countryCode,
      phoneNumber,
      password,
      accessToken: res.body.accessToken,
      data: res.body.data,
    });
  }
  return callbackFailure({ status: res.status, message: res.text });
}

export async function loginByFacebook({ callback, callbackFailure }) {
  const fbAccessToken = await getFacebookAccessToken();
  if (!fbAccessToken) {
    return callbackFailure({
      status: 400,
      message:
        '{"errors":[{"key":"accessToken","message":"cannot_log_in_using_facebook_please_try_again_or_use_your_phone_number"}]}',
    });
  }

  const res = await callAPI('post', {
    url: API.AUTH_FACEBOOK,
    content: { accessToken: fbAccessToken },
  });

  if (res.status === 200) return callback({ accessToken: res.body.accessToken, data: res.body.data, fbAccessToken });
  return callbackFailure({ status: res.status, message: res.text, fbAccessToken });
}

export function loginWithData(data, accessToken) {
  return {
    type: 'ACCESSCONTROL_AUTHEN',
    data,
    accessToken,
  };
}

export function logout() {
  return {
    type: 'ACCESSCONTROL_LOGOUT',
    key: 'accessToken',
  };
}

export async function getFacebookAccessToken({ callback } = {}) {
  // Get fbSDK from somewhere
  const facebook = window && window.FB;
  if (!facebook) return;

  const fbRes = await new Promise((resolve) => {
    facebook.login(resolve, { scope: 'email' });
  });

  const { authResponse, status } = fbRes;
  const fbAccessToken = status === 'connected' && authResponse && authResponse.accessToken;

  typeof callback === 'function' && callback(fbAccessToken);
  return fbAccessToken;
}

export async function openAccountFacebook({ fbAccessToken, callback, callbackFailure }) {
  const res = await callAPI('post', {
    url: API.OPEN_ACCOUNT_FACEBOOK,
    content: { accessToken: fbAccessToken },
  });

  if (res.status === 200) return callback({ data: res.body.data, fbAccessToken });
  return callbackFailure({ status: res.status, message: res.text, fbAccessToken });
}
