import callAPI, { register } from 'actions/helpers';
import { LINK_API_MANAGER, LINK_API_MANAGER_ADMIN, LINK_AUTH } from 'settings/variables';
import { iife } from 'utils/helper';

const { getAPI, putAPI, postAPI, delAPI } = register('merchantManager');

// FORM PARSING

function parseDishData(dishData, groupDishId) {
  const data = {
    name: dishData.get('name'),
    description: dishData.get('description'),
    price: parseInt(dishData.get('price')),
    additionalFee: parseInt(dishData.get('additionalFee')),
    copyImageFromDish: parseInt(dishData.get('copyImageFromDish')),
    groupDishId: parseInt(groupDishId),
  };

  if (dishData.get('unit')) {
    data.unit = dishData.get('unit');
    data.unitQuantity = parseFloat(
      dishData
        .get('unitQuantity')
        .toString()
        .replace(',', '.'),
    );
  }

  const dishImage = dishData.get('image');
  if (dishImage) {
    if (dishImage.indexOf('http') < 0) {
      data.image = dishImage;
      delete data.copyImageFromDish;
    }
  }

  const isDishActive = dishData.get('active');
  if (typeof isDishActive !== 'undefined') {
    data.active = isDishActive;
  }

  return data;
}

function parseDishDataForCopying(dishData, groupDishId) {
  const data = parseDishData(dishData, groupDishId);
  // maintaining additionalFee portion
  const portion = dishData.get('additionalFeePortion');
  if (portion && parseInt(portion) > 0) { data.additionalFee = Math.ceil((data.price / 1000) * (parseInt(portion) / 100)) * 1000; } else if (portion === '0') {
    data.additionalFee = 0;
  }
  return data;
}

// temp comment code. Because approved role content_manager call api domain: ale
// const hasRolesAdmin = (isAdmin) => !isAdmin ? LINK_API_MANAGER : LINK_API_MANAGER_ADMIN;

const hasRolesAdmin = () => LINK_API_MANAGER_ADMIN;

const MAGIC_API = {
  COPY_MENU: '/eateries/${merchant}/menu/copy',
  COPY_GROUP: LINK_API_MANAGER + '/eateries/${merchant}/menu/groups/${groupDishId}/copy',
  COPY_DISH: '/eateries/${merchant}/menu/dishes/${dishId}/copy',
  COPY_CUSTOM: LINK_API_MANAGER + '/eateries/${merchant}/menu/customs/${customId}/copy',
  COPY_OPTION: LINK_API_MANAGER + '/eateries/${merchant}/menu/custom-options/${optionId}/copy',
};

const ACTIVE_MENU = {
  FETCH_ACTIVE_REQUEST: LINK_API_MANAGER + '/eateries/slug:${merchant}/menu/claim-active-menus',
  CREATE_ACTIVE_REQUEST: LINK_API_MANAGER + '/eateries/slug:${merchant}/menu/claim-active-menus',
  UPDATE_ACTIVE_REQUEST: LINK_API_MANAGER + '/eateries/slug:${merchant}/menu/claim-active-menus',
};

export const API = {
  MERCHANT: LINK_AUTH + '/eateries/slug:${merchant}',
  MERCHANT_BY_USERNAME: LINK_AUTH + '/eateries/username:${username}',
  MERCHANT_ADMIN: LINK_API_MANAGER + '/eateries/slug:${merchant}',
  MERCHANT_ADMIN_BY_USERNAME: '/eateries/username:${username}',
  MENU: '/eateries/slug:${merchant}/menu',

  CREATE_GROUP: '/eateries/slug:${merchant}/menu/groups',
  UPDATE_GROUP: '/eateries/slug:${merchant}/menu/groups/${group}',
  DELETE_GROUP: '/eateries/slug:${merchant}/menu/groups/${group}',

  CREATE_INDUSTRY: '/eateries/slug:${merchant}/industries',
  UPDATE_INDUSTRY: '/eateries/slug:${merchant}/industries/${id}',
  ACTIVE_INDUSTRY: '/eateries/slug:${merchant}/industries/${id}/status',
  FETCH_MERCHANT_SUPPLY_MENU: '/eateries/slug:${merchant}/supplies/menu',
  FETCH_INDUSTRY_SEARCH: LINK_API_MANAGER_ADMIN + '/eateries/slug:${merchant}/industries',

  CREATE_DISH: '/eateries/slug:${merchant}/menu/dishes',
  UPDATE_DISH: '/eateries/slug:${merchant}/menu/dishes/${dish}',
  DELETE_DISH: '/eateries/slug:${merchant}/menu/dishes/${dish}',
  DETAIL_DISH: '/eateries/slug:${merchant}/menu/dishes/${dish}',

  CREATE_CUSTOM: '/eateries/slug:${merchant}/menu/customs',
  UPDATE_CUSTOM: '/eateries/slug:${merchant}/menu/customs/${dishCustom}',

  CREATE_CUSTOM_OPTION: '/eateries/slug:${merchant}/menu/custom-options',
  UPDATE_CUSTOM_OPTION: '/eateries/slug:${merchant}/menu/custom-options/${customOption}',

  UPDATE_AVATAR: LINK_AUTH + '/eateries/${merchant}/avatar',
  CAN_EDIT_MENU: '/eateries/${id}/can-edit-menu',
  CAN_EDIT_MENU_BY_USERNAME: '/eateries/username:${username}/can-edit-menu',
  STOCK_DISH: '/eateries/${merchantId}/menu/dishes/${id}/stocks',
  FETCH_NOTE_STOCK_DISH: '/eateries/${merchantId}/menu/dishes/${id}/stocks?page=${page}',
  FETCH_VENDOR: LINK_API_MANAGER_ADMIN + '/dishes-vendors',

  UPDATE_SHOWN_SUPPLY_ITEM: '/eateries/${merchant}/menu/groups/${group}/supply-for-user',
};

export function fetchMerchant(params) {
  if (!params.merchant && !params.merchantUsername) return false;
  const username = params.merchantUsername;
  const key = username || params.merchant;
  return async (dispatch) => {
    dispatch({
      type: 'MCM_FETCH_MERCHANT_PENDING',
      key,
    });

    const res = await callAPI('get', {
      url: username ? API.MERCHANT_BY_USERNAME : API.MERCHANT,
      params: { username, merchant: params.merchant },
      accessToken: params.accessToken,
    });

    if (res.status !== 200) {
      console.error('FETCH_MERCHANT_FAILED', key, res.text);
      return dispatch({
        type: 'MCM_FETCH_MERCHANT_FAILED',
        key,
      });
    }

    const fullData = res.body.data;

    const urlRolesAdmin = (() => {
      if (username) return hasRolesAdmin(params.isAdmin) + API.MERCHANT_ADMIN_BY_USERNAME;
      return hasRolesAdmin(params.isAdmin) + API.MERCHANT_ADMIN;
    })();

    const resLoship = await callAPI('get', {
      url: urlRolesAdmin,
      params: { username, merchant: params.merchant },
      accessToken: params.accessToken,
    });

    if (resLoship.status === 200) {
      const resLoshipData = resLoship.body.data;
      if (typeof resLoshipData.commissionValue !== 'undefined') { fullData.commissionValue = resLoshipData.commissionValue; }
    }

    return dispatch({
      type: 'MCM_FETCH_MERCHANT',
      key,
      data: fullData,
    });
  };
}

export function fetchMerchantMenu(params) {
  if (!params.merchant) return false;
  const urlTypeQuery = iife(() => {
    if (params.isMenuLosupply) {
      return {
        type: 'MCM_FETCH_MERCHANT_SUPPLY_MENU',
        url: hasRolesAdmin(params.isAdmin) + API.FETCH_MERCHANT_SUPPLY_MENU,
        query: '',
      };
    }
    return {
      type: 'MCM_FETCH_MENU',
      url: hasRolesAdmin(params.isAdmin) + API.MENU,
      query: hasRolesAdmin(params.isAdmin).includes(LINK_API_MANAGER_ADMIN) && { pagination: 1 },
    };
  });
  return getAPI({
    ...urlTypeQuery,
    key: params.merchant,
    store: 'menuManager',
    params: { merchant: params.merchant },
  });
}

export function fetchMoreMerchantMenu(params) {
  if (!params.merchant || !params.nextUrl) return;
  return getAPI({
    type: 'MCM_FETCH_MORE_MENU',
    url: hasRolesAdmin(params.isAdmin) + params.nextUrl,
    key: params.merchant,
    store: 'menuManager',
    force: true,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function fetchMoreDish(params = {}){
  const { merchant, groupDishId, nextUrl, callback, callbackFailure } = params;
  if (!merchant || !nextUrl || !groupDishId) return;
  return getAPI({
    type: 'MCM_FETCH_MORE_DISH',
    url: hasRolesAdmin(params.isAdmin) + nextUrl,
    key: merchant,
    groupDishId,
    store: 'menuManager',
    force: true,
    callback,
    callbackFailure,
  });
}
export function fetchDishInfo(params = {}) {
  if (!params.dishId || !params.slug) return false;

  return async (dispatch) => {
    const res = await callAPI('get', {
      url: hasRolesAdmin(params.isAdmin) + API.DETAIL_DISH,
      params: {
        merchant: params.slug,
        dish: params.dishId,
      },
    });
    if (res.status !== 200) {
      console.error('FETCH_DETAIL_LOSUPPLY_FAILED', res.text);
      return dispatch({
        type: 'MCM_FETCH_DETAIL_LOSUPPLY_FAILED',
      });
    }
    dispatch({
      type: 'MCM_FETCH_DETAIL_DISH_LOSUPPLY',
      key: params.dishId,
      data: res.body.data,
    });
    if (typeof params.callback === 'function') params.callback();
  };
}

// CREATE

export function createMenuIndustry({
  industryData, merchant,
  callback, callbackFailure, isAdmin,
}) {
  return async (dispatch) => {
    const res = await callAPI('post', {
      url: hasRolesAdmin(isAdmin) + API.CREATE_INDUSTRY,
      params: { merchant },
      content: {
        name: industryData.get('name'),
        active: industryData.get('active'),
        isPublished: industryData.get('active'),
        order: industryData.get('order'),
        image: industryData.get('image'),
      },
    });
    if (res.status === 200) {
      if (typeof callback === 'function') callback(res);
      dispatch({ type: 'MCM_MENU_INDUSTRY_CREATE', key: merchant + '.industries', data: res.body.data });
    } else if (typeof callbackFailure === 'function') {
      callbackFailure();
    }
  };
}

export function createMenuGroup({
  groupData, merchant,
  callback, callbackFailure, refetchMenu = true, isAdmin,
}) {
  return async (dispatch, getState) => {
    const res = await callAPI('post', {
      url: hasRolesAdmin(isAdmin) + API.CREATE_GROUP,
      params: { merchant },
      content: {
        name: groupData.get('name'),
        active: groupData.get('active'),
        isExtraGroupDish: groupData.get('isExtraGroupDish'),
        isPinned: groupData.get('isPinned'),
        industryId: groupData.get('industryId'),
      },
    });
    if (res.status === 200) {
      if (typeof callback === 'function') callback(res);
      dispatch({
        type: 'MCM_MENU_GROUP_CREATE',
        key: merchant,
        data: res.body.data,
        industryId: groupData.get('industryId'),
      });

      refetchMenu && fetchMerchantMenu({ merchant, isMenuLosupply: !!groupData.get('industryId') })(dispatch, getState);
    } else if (typeof callbackFailure === 'function') callbackFailure();
  };
}

export function createDish({ dishData, merchant, groupDishId, callback, callbackFailure, isAdmin }) {
  return postAPI({
    type: 'MCM_DISH_CREATE',
    key: merchant,
    url: hasRolesAdmin(isAdmin) + API.CREATE_DISH,
    params: { merchant },
    content: parseDishData(dishData, groupDishId),
    groupDishId,
    callback,
    callbackFailure,
  });
}

export function createDishCustom({
  customData, merchant, dishId, groupDishId,
  callback, callbackFailure, isAdmin,
}) {
  return postAPI({
    type: 'MCM_DISH_CUSTOM_CREATE',
    key: merchant,
    url: hasRolesAdmin(isAdmin) + API.CREATE_CUSTOM,
    params: { merchant },
    content: {
      name: customData.get('name'),
      note: customData.get('note'),
      selectionType: customData.get('selectionType'),
      limitQuantity: parseInt(customData.get('limitQuantity')),
      dishId: parseInt(dishId),
    },
    groupDishId,
    dishId,
    callback,
    callbackFailure,
  });
}

export function createDishCustomOption({
  customOptionData,
  merchant,
  customId,
  dishId,
  groupDishId,
  callback,
  callbackFailure,
  isAdmin,
}) {
  return postAPI({
    type: 'MCM_DISH_CUSTOM_OPTION_CREATE',
    key: merchant,
    url: hasRolesAdmin(isAdmin) + API.CREATE_CUSTOM_OPTION,
    params: { merchant },
    content: {
      value: customOptionData.get('value'),
      price: parseInt(customOptionData.get('price')),
      limitQuantity: parseInt(customOptionData.get('limitQuantity')),
      customId: parseInt(customId),
    },
    groupDishId,
    dishId,
    customId,
    callback,
    callbackFailure,
  });
}

// UPDATE

export function updateMenuIndustry({
  content, merchant, id,
  callback, callbackFailure, isAdmin,
}) {
  return async (dispatch) => {
    const res = await callAPI('put', {
      url: hasRolesAdmin(isAdmin) + API.UPDATE_INDUSTRY,
      params: {
        merchant,
        id,
      },
      content,
    });
    if (res.status === 200) {
      if (typeof callback === 'function') callback(res);
      dispatch({ type: 'MCM_MENU_INDUSTRY_UPDATE', key: merchant + '.industries', data: res.body.data });
    } else if (typeof callbackFailure === 'function') callbackFailure();
  };
}

export function activeMenuIndustry({
  id, merchant, content,
  callback, isAdmin, callbackFailure,
}) {
  return async (dispatch) => {
    const res = await callAPI('put', {
      url: hasRolesAdmin(isAdmin) + API.ACTIVE_INDUSTRY,
      params: {
        merchant,
        id,
      },
      content,
    });
    if (res.status === 200) {
      if (typeof callback === 'function') callback(res);
      dispatch({ type: 'MCM_MENU_INDUSTRY_UPDATE', key: merchant + '.industries', data: res.body.data });
    } else if (typeof callbackFailure === 'function') callbackFailure();
  };
}

export function updateMenuGroup({
  groupDishId, groupData, merchant, previousShownSupplyItems,
  callback, callbackFailure, refetchMenu = true, isAdmin, eateryId,
}) {
  return async (dispatch, getState) => {
    let isChangeShownSupplyItem;
    if (typeof previousShownSupplyItems !== 'undefined' && previousShownSupplyItems !== groupData.get('isShownSupplyItemsForUser')) {
      isChangeShownSupplyItem = await updateShownSupplyItem({ groupDishId, groupData, isAdmin, eateryId });
    }
    const res = await callAPI('put', {
      url: hasRolesAdmin(isAdmin) + API.UPDATE_GROUP,
      params: {
        merchant,
        group: groupDishId,
      },
      content: {
        name: groupData.get('name'),
        active: groupData.get('active'),
        isExtraGroupDish: groupData.get('isExtraGroupDish'),
        isPinned: groupData.get('isPinned'),
        industryId: groupData.get('industryId'),
      },
    });
    if (res.status === 200) {
      if (typeof callback === 'function') callback(res);
      if (isChangeShownSupplyItem?.status === 200) {
        res.body.data.isShownSupplyItemsForUser = groupData.get('isShownSupplyItemsForUser');
      }
      dispatch({
        type: 'MCM_MENU_GROUP_UPDATE',
        key: merchant,
        data: res.body.data,
        industryId: groupData.get('industryId'),
      });
      refetchMenu && fetchMerchantMenu({ merchant, isMenuLosupply: !!groupData.get('industryId') })(dispatch, getState);
    } else if (typeof callbackFailure === 'function') callbackFailure();
  };
}

export async function updateShownSupplyItem({
  groupDishId, groupData,
  callback, isAdmin, eateryId,
}) {
  const merchantId = eateryId || groupData.get('eateryId');
  if (!merchantId) return;
  const res = await callAPI('put', {
    url:  hasRolesAdmin(isAdmin) + API.UPDATE_SHOWN_SUPPLY_ITEM,
    params: {
      merchant: merchantId,
      group: groupDishId,
    },
    content: {
      isShown: !!groupData.get('isShownSupplyItemsForUser'),
    },
  });
  if (res.status === 200 && typeof callback === 'function') callback();
  return res.status;
}

export function updateDish({ dishId, dishData, merchant, groupDishId, callback, callbackFailure, isAdmin }) {
  return putAPI({
    type: 'MCM_DISH_UPDATE',
    key: merchant,
    url: hasRolesAdmin(isAdmin) + API.UPDATE_DISH,
    params: {
      merchant,
      dish: parseInt(dishId),
    },
    content: parseDishData(dishData, groupDishId),
    dishId,
    groupDishId,
    callback,
    callbackFailure,
  });
}

export function updateDishCustom({
  customId, customData, merchant, dishId, groupDishId,
  callback, callbackFailure, isAdmin,
}) {
  return putAPI({
    type: 'MCM_DISH_CUSTOM_UPDATE',
    key: merchant,
    url: hasRolesAdmin(isAdmin) + API.UPDATE_CUSTOM,
    params: {
      merchant,
      dishCustom: parseInt(customId),
    },
    content: {
      name: customData.get('name'),
      note: customData.get('note'),
      selectionType: customData.get('selectionType'),
      limitQuantity: parseInt(customData.get('limitQuantity')),
      active: customData.get('active'),
    },
    groupDishId,
    dishId,
    callback,
    callbackFailure,
  });
}

export function updateDishCustomOption({
  customOptionId,
  customOptionData,
  merchant,
  customId,
  dishId,
  groupDishId,
  callback,
  callbackFailure,
  isAdmin,
}) {
  return putAPI({
    type: 'MCM_DISH_CUSTOM_OPTION_UPDATE',
    key: merchant,
    url: hasRolesAdmin(isAdmin) + API.UPDATE_CUSTOM_OPTION,
    params: {
      merchant,
      customOption: parseInt(customOptionId),
    },
    content: {
      value: customOptionData.get('value'),
      price: parseInt(customOptionData.get('price')),
      limitQuantity: parseInt(customOptionData.get('limitQuantity')),
      active: customOptionData.get('active'),
    },
    groupDishId,
    dishId,
    customId,
    callback,
    callbackFailure,
  });
}

// DELETE

export function deleteMenuIndustry({
  id, merchant,
  callback, callbackFailure, isAdmin,
}) {
  return async (dispatch) => {
    const res = await callAPI('delete', {
      url: hasRolesAdmin(isAdmin) + API.UPDATE_INDUSTRY,
      params: {
        merchant,
        id,
      },
    });

    if (res.status === 200) {
      if (typeof callback === 'function') callback(res);
      dispatch({ type: 'MCM_MENU_INDUSTRY_DELETE', key: merchant + '.industries', data: { id }, deleteId: id });
    } else if (typeof callbackFailure === 'function') callbackFailure();
  };
}

export function deleteMenuGroup({
  groupDishId, merchant, industryId,
  callback, callbackFailure, refetchMenu = true, isAdmin,
}) {
  return async (dispatch, getState) => {
    const res = await callAPI('delete', {
      url: hasRolesAdmin(isAdmin) + API.DELETE_GROUP,
      params: {
        merchant,
        group: groupDishId,
      },
    });

    if (res.status === 200) {
      if (typeof callback === 'function') callback(res);
      dispatch({ type: 'MCM_MENU_GROUP_DELETE', key: merchant, data: { id: groupDishId }, deleteId: groupDishId });

      refetchMenu && fetchMerchantMenu({ merchant, isMenuLosupply: !!industryId })(dispatch, getState);
    } else if (typeof callbackFailure === 'function') callbackFailure();
  };
}

export function deleteDish({ dishId, merchant, groupDishId, callback, callbackFailure, isAdmin }) {
  return delAPI({
    type: 'MCM_DISH_DELETE',
    key: merchant,
    url: hasRolesAdmin(isAdmin) + API.DELETE_DISH,
    params: {
      merchant,
      dish: parseInt(dishId),
    },
    dishId,
    groupDishId,
    deleteId: dishId,
    callback,
    callbackFailure,
  });
}

// MAGIC

export function dishStock({ groupDishId, dishId, amount, note, isAdmin, merchant, price, dishVendorName, dishVendorId, callback }) {
  const content = { amount, note, price };
  if (dishVendorId) {
    content.dishVendorId = dishVendorId;
  } else {
    content.dishVendorName = dishVendorName;
  }
  return putAPI({
    type: 'MCM_DISH_STOCK_UPDATE',
    url: hasRolesAdmin(isAdmin) + API.STOCK_DISH,
    params: { merchantId: merchant.id, id: dishId },
    content,
    key: merchant.slug,
    groupDishId,
    callback,
  });
}

export function fetchNoteStock({ dishId, merchant, isAdmin, page, callback }) {
  return async (dispatch) => {
    const resCopy = await callAPI('get', {
      url: hasRolesAdmin(isAdmin) + API.FETCH_NOTE_STOCK_DISH,
      params: { merchantId: merchant.id, id: dishId, page: page || 1 },
    });
    if (typeof callback === 'function') callback(resCopy.body.pagination);
    return dispatch({
      type: 'MCM_NOTE_STOCK',
      data: resCopy.body.data,
      key: dishId.toString(),
    });
  };
}

export function copyMenu({ merchant, sourceMerchantId, merchantId, callback, isAdmin }) {
  return postAPI({
    type: 'MCM_COPY_MENU',
    url: hasRolesAdmin(isAdmin) + MAGIC_API.COPY_MENU,
    key: merchant,
    params: { merchant: sourceMerchantId },
    content: { targetId: merchantId },
    callback,
  });
}

export function copyDish({ merchant, merchantId, dishId, groupDishId, dishData, callback, isAdmin }) {
  return async (dispatch) => {
    const resCopy = await callAPI('post', {
      url: hasRolesAdmin(isAdmin) + MAGIC_API.COPY_DISH,
      params: { merchant: merchantId, dishId },
      content: { targetId: groupDishId },
    });

    if (resCopy.status !== 200 || !resCopy.body.data) {
      // TODO: Proper alert please
      console.error(`Error copying dish ${dishId}`);
      return alert('Có lỗi xảy ra! Vui lòng thử lại sau.');
    }

    const newDish = resCopy.body.data;
    const resUpdate = await callAPI('put', {
      url: hasRolesAdmin(isAdmin) + API.UPDATE_DISH,
      params: { merchant, dish: newDish.id },
      content: parseDishDataForCopying(dishData, groupDishId, newDish),
    });

    if (resUpdate.status !== 200 || !resUpdate.body.data) {
      // TODO: Proper alert please
      console.error(`Error updating dish ${newDish.id} <= ${dishId}`);
      return alert('Có lỗi xảy ra! Vui lòng thử lại sau.');
    }

    // Mimic apiMiddleware behaviour
    if (typeof callback === 'function') callback(resUpdate);

    const updateData = { ...newDish, ...resUpdate.body.data };
    return dispatch({
      type: 'MCM_DISH_COPY',
      key: merchant,
      data: updateData,
      groupDishId,
    });
  };
}

export const createActiveRequest = ({ merchant }) => {
  return postAPI({
    type: 'ACTIVE_MENU_REQUEST_FETCH',
    url: ACTIVE_MENU.CREATE_ACTIVE_REQUEST,
    key: merchant,
    params: { merchant },
  });
};

export const approveActiveMenuRequest = ({ merchant }) => {
  return putAPI({
    type: 'ACTIVE_MENU_REQUEST_APPROVE',
    url: ACTIVE_MENU.UPDATE_ACTIVE_REQUEST,
    key: merchant,
    params: { merchant },
    content: { accept: true },
  });
};

export const rejectActiveMenuRequest = ({ merchant }) => {
  return putAPI({
    type: 'ACTIVE_MENU_REQUEST_REJECT',
    url: ACTIVE_MENU.UPDATE_ACTIVE_REQUEST,
    key: merchant,
    params: { merchant },
    content: { accept: false },
  });
};

export const fetchActiveRequest = ({ merchant }) => {
  return getAPI({
    type: 'ACTIVE_MENU_REQUEST_FETCH',
    url: ACTIVE_MENU.FETCH_ACTIVE_REQUEST,
    key: merchant,
    params: { merchant },
  });
};

export function updateAvatar(merchantId, params) {
  return putAPI({
    type: 'UPDATE_AVATAR',
    key: merchantId,
    params: { merchant: merchantId },
    url: API.UPDATE_AVATAR,
    content: { image: params.image },
    data: { avatar: params.avatar },
    callback: params.callback,
  });
}

export async function checkCanEditMenu(params) {
  const { username, isAdmin, merchant } = params;
  const res = username
    ? await callAPI('get', {
      url: hasRolesAdmin(isAdmin) + API.CAN_EDIT_MENU_BY_USERNAME,
      params: { username },
    })
    : await callAPI('get', {
      url: hasRolesAdmin(isAdmin) + API.CAN_EDIT_MENU,
      params: { id: merchant },
    });

  if (res.status !== 200) return false;

  if (typeof params.callback === 'function') {
    params.callback();
  }

  return true;
}
