import callAPI, { register } from 'actions/helpers';
import { getCurrentUsername } from 'reducers/metadata';
import { APP_INFO, LINK_API_MANAGER } from 'settings/variables';
import { getPaymentSupportPayDebt, getUserOrder } from 'reducers/order';
import { getOrderLinesData } from 'utils/cart';
import { iife } from 'utils/helper';
import { getClientLocale } from 'utils/i18n';
import loziServices from 'utils/json/loziServices';

const { getAPI, getMoreAPI, postAPI, putAPI } = register('order');

export const API = {
  CREATE_ORDER: '/eateries/${merchant}/orders',
  CREATE_ORDER_ZALOPAY: '/eateries/${merchant}/orders/paid-by-zalo',
  CREATE_ORDER_ZALOPAY_MOBILE: '/eateries/${merchant}/orders/paid-by-zalo-app',
  CREATE_ORDER_MOMO: '/eateries/${merchant}/orders/paid-by-momo',
  CREATE_ORDER_MOMO_MOBILE: '/eateries/${merchant}/orders/paid-by-momo-app',
  CREATE_ORDER_MOMO_IN_APP: '/eateries/${merchant}/orders/paid-by-momo-in-app',
  CREATE_ORDER_EPAY: '/eateries/${merchant}/orders/paid-by-epay',
  CREATE_ORDER_SACOMBANK: '/eateries/${merchant}/orders/paid-by-sacombank',
  CREATE_ORDER_VIETTEL: '/eateries/${merchant}/orders/paid-by-viettel',

  FETCH_PAYMENT_SUPPORT_TOKENIZED: '/payment/payment-card/fees',
  CREATE_PAYMENT_CARD: '/payment/payment-cards',
  FETCH_PAYMENT_CARD_BY_APPTRANSID: '/payment/payment-cards/${appTransId}',
  FETCH_PAYMENT_SUPPORT_PAY_DEBT: '/payment/debt-fees',

  PAY_DEBT_AUTO: '/users/me/debt-orders/${order}/pay',
  PAY_DEBT_ZALOPAY: '/users/me/debt-orders/${order}/paid-by-zalo',
  PAY_DEBT_ZALOPAY_MOBILE: '/users/me/debt-orders/${order}/paid-by-zalo-app',
  PAY_DEBT_MOMO: '/users/me/debt-orders/${order}/paid-by-momo',
  PAY_DEBT_MOMO_MOBILE: '/users/me/debt-orders/${order}/paid-by-momo-app',
  PAY_DEBT_MOMO_IN_APP: '/users/me/debt-orders/${order}/paid-by-momo-in-app',
  PAY_DEBT_EPAY: '/users/me/debt-orders/${order}/paid-by-epay',
};

export const SERVICE_API = {
  CREATE_ORDER: '/services/${serviceName}/orders',
  CREATE_ORDER_ZALOPAY: '/services/${serviceName}/orders/paid-by-zalo',
  CREATE_ORDER_ZALOPAY_MOBILE: '/services/${serviceName}/orders/paid-by-zalo-app',
  CREATE_ORDER_MOMO: '/services/${serviceName}/orders/paid-by-momo',
  CREATE_ORDER_MOMO_MOBILE: '/services/${serviceName}/orders/paid-by-momo-app',
  CREATE_ORDER_MOMO_IN_APP: '/services/${serviceName}/orders/paid-by-momo-in-app',
  CREATE_ORDER_EPAY: '/services/${serviceName}/orders/paid-by-epay',
  CREATE_ORDER_SACOMBANK: '/services/${serviceName}/orders/paid-by-sacombank',
  CREATE_ORDER_VIETTEL: '/services/${serviceName}/orders/paid-by-viettel',

  FETCH_LOSEND_CONFIGS: '/services/losend/configs',
  FETCH_CONFIGS: '/configs',
};

export const USER_ORDER_API = {
  FETCH_ORDERS: '/users/me/orders',

  FETCH: '/orders/${order}',
  FETCH_BY_APPTRANSID: '/orders/${appTransId}',
  FETCH_BY_APPTRANSID_VIETTEL: '/partners/viettelpay/orders/${appTransId}',
  FETCH_FOR_MERCHANT: LINK_API_MANAGER + '/orders/code:${code}/merchant',

  FETCH_BY_MOMOTRANSID: '/partners/momopay/${phoneNumber}/orders/${loziMomoTransId}',
  FETCH_FOR_PARTNER_BY_APPTRANSID: '/partners/${partnerName}/${phoneNumber}/orders/${appTransId}',

  CANCEL_ORDER: '/orders/code:${code}/cancel',
  RATING_SHIPPER: '/orders/code:${code}/rate',
  RATING_MERCHANT: '/orders/code:${code}/rate/merchant',
  FETCH_ORDER_TRACKING_MAP: '/orders/${order}/tracking-map',
  CREATE_ORDER_SHARING: '/orders/code:${code}/share',

  REPORT_SHIPPER: '/order/reports/code:${code}',
};

export function sendOrder(params = {}) {
  const {
    cart, payment,
    promotionCode,
    senderInfo, receiverInfo, distances,
    accessToken, serviceName,
  } = params;
  const lines = getOrderLinesData(cart, params.defaultTaggedUserId);
  const service = loziServices.find((s) => s.name === (serviceName || APP_INFO.serviceName)) || loziServices[0];

  const additionalHeaders = { 'X-Loship-Service': service.appApiHeader };
  if (typeof params.clientId !== 'undefined') additionalHeaders['X-Lozi-Client'] = params.clientId;
  additionalHeaders['X-Lozi-App-Client'] = 11;

  const orderAPI = iife(() => {
    const baseAPI = ['losend', 'loxe'].includes(service.name) ? SERVICE_API : API;

    switch (payment.method) {
      case 'zalopayapp':
        return !params.isMobileDevice ? baseAPI.CREATE_ORDER_ZALOPAY : baseAPI.CREATE_ORDER_ZALOPAY_MOBILE;
      case 'momopay':
        return baseAPI.CREATE_ORDER_MOMO;
      case 'momopayinapp':
        return baseAPI.CREATE_ORDER_MOMO_IN_APP;
      case 'fullpayment':
      case 'CC':
        return baseAPI.CREATE_ORDER_ZALOPAY;
      case 'epayCC':
      case 'epayfullpayment':
        if (!params.isPayImmediate && payment.card && payment.card.id) return baseAPI.CREATE_ORDER;
        return baseAPI.CREATE_ORDER_EPAY;
      case 'sacombankpay':
        return baseAPI.CREATE_ORDER_SACOMBANK;
      case 'viettelpay':
        return baseAPI.CREATE_ORDER_VIETTEL;
      default:
        return baseAPI.CREATE_ORDER;
    }
  });

  const content = iife(() => {
    const source = {
      name: senderInfo.customerName,
      phoneNumber: senderInfo.customerPhone,
      lat: senderInfo.lat,
      lng: senderInfo.lng,
      address: senderInfo.address,
      placeName: senderInfo.placeName,
      cityId: senderInfo.cityId || 50,
      districtId: senderInfo.districtId?.toString() || '0',
      suggestedAddress: receiverInfo.suggestedAddress,
      suggestedDistrictId: receiverInfo.suggestedDistrictId,
      countryCode: senderInfo.countryCode,
    };
    const destination = {
      name: receiverInfo.customerName,
      phoneNumber: receiverInfo.customerPhone,
      lat: receiverInfo.lat,
      lng: receiverInfo.lng,
      address: receiverInfo.address,
      placeName: receiverInfo.placeName,
      cityId: receiverInfo.cityId || 50,
      districtId: receiverInfo.districtId?.toString() || '0',
      suggestedAddress: receiverInfo.suggestedAddress,
      suggestedDistrictId: receiverInfo.suggestedDistrictId,
      isRecipientChanged: receiverInfo.isRecipientChanged,
      countryCode: receiverInfo.countryCode,
    };

    return {
      ...(['losend', 'loxe'].includes(service.name)
        ? {
          name: senderInfo.customerName,
          phone: senderInfo.customerPhone,
          address: senderInfo.address,
          districtId: senderInfo.districtId?.toString() || '0',
        }
        : {
          name: receiverInfo.customerName,
          phone: receiverInfo.customerPhone,
          address: receiverInfo.address,
          districtId: receiverInfo.districtId?.toString() || '0',
          countryCode: receiverInfo.countryCode,
        }),

      lines,
      promotionCode: promotionCode && promotionCode.code,
      sourceOrderId: params.sourceOrderId,
      note: params.note,
      notePhoto: params.notePhoto,
      buyInSameBranch: params.buyInSameBranch,
      isConfirmed: params.isConfirmed,
      orderGroupTopic: params.groupTopic,
      distance: distances && distances.value,
      routes: {
        distance: distances && distances.value,
        source,
        destination,
      },
      payment: {
        paymentMethod: payment.method,
        bankCode: (payment.method.includes('fullpayment') && payment.bank && payment.bank.code) || undefined,
        paymentCardId: (payment.method.includes('CC') && payment.card && payment.card.id) || undefined,
        paymentOption: (() => {
          if (!payment.method.includes('CC')) return;
          if (payment.card) return 'card';
          if (payment.saveCard) return 'create_card_token';
        })(),
      },
    };
  });

  return postAPI({
    type: 'USER_CREATE_ORDER',
    url: orderAPI,
    params: { merchant: params.eateryId, serviceName: service.name },
    content,
    additionalHeaders,
    accessToken,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function fetchPaymentSupportTokenized(params = {}) {
  return getAPI({
    type: 'FETCH_PAYMENT_SUPPORT_TOKENIZED',
    url: API.FETCH_PAYMENT_SUPPORT_TOKENIZED,
    key: 'payment.supportTokenized',
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function createPaymentCard(params = {}) {
  const { callback, callbackFailure } = params;
  return postAPI({
    type: 'CREATE_PAYMENT_CARD',
    url: API.CREATE_PAYMENT_CARD,
    params: { paymentMethod: params.paymentMethod },
    content: {
      paymentMethod: params.paymentMethod,
      paymentOption: 'add_token_transaction',
    },
    callback,
    callbackFailure,
  });
}

export function fetchPaymentSupportPayDebt(params = {}) {
  return async (dispatch, getState) => {
    const existedData = getPaymentSupportPayDebt(getState());
    if (existedData && existedData.get('status') === 'SUCCESS') {
      return typeof params.callback === 'function' && params.callback({ body: { data: existedData.toJS().data } });
    }

    return getAPI({
      type: 'FETCH_PAYMENT_SUPPORT_PAY_DEBT',
      url: API.FETCH_PAYMENT_SUPPORT_PAY_DEBT,
      key: 'payment.supportPayDebt',
      callback: params.callback,
      callbackFailure: params.callbackFailure,
    })(dispatch, getState);
  };
}

export function fetchLosendConfigs({ callback } = {}) {
  return getAPI({
    type: 'FETCH_LOSEND_CONFIGS',
    url: SERVICE_API.FETCH_LOSEND_CONFIGS,
    key: 'losendConfigs',
    callback,
  });
}

export function fetchGlobalConfigs({ callback, callbackFailure } = {}) {
  const currentLocale = getClientLocale();

  return getAPI({
    type: 'FETCH_CONFIGS',
    url: SERVICE_API.FETCH_CONFIGS,
    key: 'configs',
    callback,
    callbackFailure,
    additionalHeaders: { 'X-Language': currentLocale === 'vi_VN' ? 'vi' : 'en' },
  });
}

export function fetchOrdersHistory(params) {
  return getAPI({
    type: 'FETCH_ORDERS_HISTORY',
    url: USER_ORDER_API.FETCH_ORDERS,
    params: {},
    query: { limit: 10, isIncludingGroupOrder: true },
    force: params.force,
  });
}

export function fetchMoreOrdersHistory(url, params = {}) {
  return getMoreAPI({
    type: 'FETCH_MORE_ORDERS_HISTORY',
    url,
    params,
  });
}

export function fetchUserOrderDetail(params = {}, query = {}) {
  const orderOrShareId = params.order || params.shareId;

  return (dispatch, getState) => {
    const store = getState();
    const existedOrder = getUserOrder(store, params);

    if (existedOrder && params.soft) {
      // soft -> no fetch if existed
      return typeof params.callback === 'function' && params.callback({ body: { data: existedOrder.toJS().data } });
    }

    const currentLocale = getClientLocale();
    return getAPI({
      type: 'USER_ORDER_FETCH_DETAIL',
      url: USER_ORDER_API.FETCH,
      key: query.usp !== 'sharing' && orderOrShareId,
      params: query.usp ? { order: `shareId:${orderOrShareId}` } : { order: `code:${orderOrShareId}` },
      query,
      additionalHeaders: { 'X-Language': currentLocale === 'vi_VN' ? 'vi' : 'en' },
      accessToken: params.accessToken,
      callback: params.callback,
      callbackFailure: params.callbackFailure,
    })(dispatch, getState);
  };
}

export function fetchMerchantOrderDetail(params) {
  return (dispatch, getState) => {
    const store = getState();
    const existedOrder = store.order.get(`${params.order}.merchant`);

    if (existedOrder && params.soft) {
      // soft -> no fetch if existed
      return typeof params.callback === 'function' && params.callback({ body: { data: existedOrder.toJS().data } });
    }

    const currentLocale = getClientLocale();
    return getAPI({
      type: 'MERCHANT_ORDER_FETCH_DETAIL',
      url: USER_ORDER_API.FETCH_FOR_MERCHANT,
      key: params.order + '.merchant',
      params: { code: params.order },
      additionalHeaders: { 'X-Language': currentLocale === 'vi_VN' ? 'vi' : 'en' },
      accessToken: params.accessToken,
      callback: params.callback,
    })(dispatch, getState);
  };
}

export async function fetchOrderByAppTransId(params) {
  const urlAndParams = iife(() => {
    if (params.partnerCode === 'mm') {
      return {
        url: USER_ORDER_API.FETCH_BY_APPTRANSID,
        params: { appTransId: `lozimomotransid:${params.appTransId}` },
      };
    }
    if (params.partnerCode === 'viettel') {
      return {
        url: USER_ORDER_API.FETCH_BY_APPTRANSID_VIETTEL,
        params: { appTransId: params.appTransId },
      };
    }
    return {
      url: USER_ORDER_API.FETCH_BY_APPTRANSID,
      params: { appTransId: `apptransid:${params.appTransId}` },
    };
  });

  const res = await callAPI('get', {
    ...urlAndParams,
    accessToken: params.accessToken,
  });

  return params.callback(res);
}

export async function fetchPaymentCardByAppTransId(params) {
  const urlAndParams = iife(() => {
    return {
      url: API.FETCH_PAYMENT_CARD_BY_APPTRANSID,
      params: { appTransId: `apptransid:${params.appTransId}` },
    };
  });

  const res = await callAPI('get', {
    ...urlAndParams,
    accessToken: params.accessToken,
  });

  return params.callback(res);
}

// Fetch MoMo Order (use in /g/momo/order)
export function fetchUserOrderDetailByMoMoTransID({ loziMomoTransId, phoneNumber, token } = {}) {
  return async (dispatch) => {
    dispatch({
      type: 'MOMO_ORDER_FETCH_DETAIL_PENDING',
      key: '@PARTNER/MOMO',
    });

    const res = await callAPI('get', {
      url: USER_ORDER_API.FETCH_BY_MOMOTRANSID,
      params: {
        loziMomoTransId,
        phoneNumber,
      },
      query: { token },
    });

    if (res.statusCode !== 200) {
      console.error(res.statusCode, res.error);
      return dispatch({
        type: 'MOMO_ORDER_FETCH_DETAIL_FAILED',
        key: '@PARTNER/MOMO',
        error: res.error,
      });
    }

    return dispatch({
      type: 'MOMO_ORDER_FETCH_DETAIL',
      key: '@PARTNER/MOMO',
      data: res.body.data,
    });
  };
}

// TODO: viettelpay unify auth for order
export function fetchUserOrderDetailByAppTransId({ appTransId, phoneNumber, partnerName, token } = {}) {
  return async (dispatch) => {
    dispatch({
      type: 'PARTNER_ORDER_FETCH_DETAIL_PENDING',
      key: `@PARTNER/${partnerName}`,
    });

    const res = await callAPI('get', {
      url: USER_ORDER_API.FETCH_FOR_PARTNER_BY_APPTRANSID,
      params: {
        partnerName,
        appTransId,
        phoneNumber,
      },
      query: { token },
    });

    if (res.statusCode !== 200) {
      console.error(res.statusCode, res.error);
      return dispatch({
        type: 'PARTNER_ORDER_FETCH_DETAIL_FAILED',
        key: `@PARTNER/${partnerName}`,
        error: res.error,
      });
    }

    return dispatch({
      type: 'PARTNER_ORDER_FETCH_DETAIL',
      key: `@PARTNER/${partnerName}`,
      data: res.body.data,
    });
  };
}

export function cancelUserOrder(params) {
  return putAPI({
    type: 'USER_ORDER_CANCEL',
    key: params.code,
    url: USER_ORDER_API.CANCEL_ORDER,
    params: { code: params.code },
    content: { cancelNoteId: params.cancelNoteId },
    accessToken: params.accessToken,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export const ratingOrder = (code, rating, review) => {
  return async (dispatch) => {
    const res = await callAPI('put', {
      url: USER_ORDER_API.RATING_SHIPPER,
      params: { code },
      content: { rating, review },
    });
    if (res.status === 200) {
      return dispatch({
        type: 'USER_ORDER_RATING_SHIPPER',
        code,
        data: res.body.data,
      });
    }
    if (res.status === 500) {
      throw new Error('Lỗi hệ thống!');
    }
    if (res.body?.errors) throw new Error(res.body.errors);
    throw new Error(res.text);
  };
};

export const ratingMerchant = ({ code, rating, content, photos, callback }) => {
  return async (dispatch) => {
    const res = await callAPI('put', {
      url: USER_ORDER_API.RATING_MERCHANT,
      params: { code },
      content: { rating, content, photos },
    });
    if (res.status === 200) {
      typeof callback === 'function' && callback(res);
      return dispatch({
        type: 'USER_ORDER_RATING_MERCHANT',
        code,
        data: res.body.data,
      });
    }
    if (res.status === 500) {
      throw new Error('Lỗi hệ thống!');
    }
    if (res.body?.errors) throw new Error(res.body.errors);
    throw new Error(res.text);
  };
};

export function fetchOrderTrackingMap(params = {}, query) {
  return async () => {
    if (!params.code && !params.shareId) return;

    const res = await callAPI('get', {
      url: USER_ORDER_API.FETCH_ORDER_TRACKING_MAP,
      params: query.usp === 'sharing' ? { order: `shareId:${params.shareId}` } : { order: `code:${params.code}` },
      query,
      accessToken: params.accessToken,
    });

    if (res.status !== 200) {
      typeof params.failureCallbackAction === 'function' && params.failureCallbackAction(res);
      return;
    }

    return params.callbackAction(res.body.data);
  };
}

export function fetchOrderSharing(params = {}) {
  return async () => {
    if (!params.code) return;

    const res = await callAPI('put', {
      url: USER_ORDER_API.CREATE_ORDER_SHARING,
      params: { code: params.code },
      accessToken: params.accessToken,
    });

    if (res.status !== 200) {
      typeof params.failureCallbackAction === 'function' && params.failureCallbackAction(res);
      return;
    }

    return params.callbackAction(res.body.data);
  };
}

export function autoPayDebtOrder(params) {
  if (!params.order) return;

  return postAPI((state) => {
    const username = getCurrentUsername(state);
    return {
      type: 'USER_AUTO_PAY_DEBT_ORDER',
      url: API.PAY_DEBT_AUTO,
      params: { order: `code:${params.order}` },
      key: `${username}.debtOrders`,
      callback: params.callback,
      callbackFailure: params.callbackFailure,
    };
  });
}

export function payDebtOrder(params) {
  const { payment } = params;

  const orderAPI = iife(() => {
    switch (payment.method) {
      case 'zalopayapp':
        return !params.isMobileDevice ? API.PAY_DEBT_ZALOPAY : API.PAY_DEBT_ZALOPAY_MOBILE;
      case 'momopay':
        return API.PAY_DEBT_MOMO;
      case 'momopayinapp':
        return API.PAY_DEBT_MOMO_IN_APP;
      case 'fullpayment':
      case 'CC':
        return API.PAY_DEBT_ZALOPAY;
      case 'epayCC':
      case 'epayfullpayment':
        return API.PAY_DEBT_EPAY;
      default:
        return;
    }
  });

  return postAPI({
    type: 'USER_PAY_DEBT_ORDER',
    url: orderAPI,
    params: { order: `code:${params.order}` },
    content: {
      paymentMethod: payment.method,
      bankCode: (payment.method.includes('fullpayment') && payment.bank && payment.bank.code) || undefined,
      paymentCardId: (payment.method.includes('CC') && payment.card && payment.card.id) || undefined,
      paymentOption: (() => {
        if (!payment.method.includes('CC')) return;
        if (payment.card) return 'card';
        if (payment.saveCard) return 'create_card_token';
      })(),
    },
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function reportShipperFraudCancel(params) {
  return postAPI({
    type: 'USER_REPORT_SHIPPER_FRAUD_CANCEL',
    key: params.order,
    url: USER_ORDER_API.REPORT_SHIPPER,
    params: { code: params.order },
    content: {
      isWrongCancelReason: true,
    },
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}
