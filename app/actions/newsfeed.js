import callAPI, { register } from 'actions/helpers';
import produce from 'immer';
import { getCurrentCity } from 'reducers/new/metadata';
import { getEventById, getKey, parseQuery } from 'reducers/newsfeed';

const { getAPI, postAPI, delAPI } = register('newsfeed');

const ACTION_API = {
  FETCH_EATERY_RATING_BY_ID: '/eatery-ratings/${ratingId}',
  UPDATE_LIKE_EATERY_RATING: '/eatery-ratings/${ratingId}/likes',
};

export const API = {
  SEARCH_EATERIES: '/search/eateries',
  SEARCH_DISHES: '/search/dishes',

  SEARCH_EATERY_CHAINS: '/search/eatery/chains',
  SEARCH_EATERY_DISHES: '/search/eatery/dishes',
  SEARCH_STATS_EATERY_CHAIN: '/search/eatery/chains/count',
  SEARCH_STATS_DISH: '/search/eatery/dishes/count',
  FETCH_CHAIN_DETAIL: '/eatery/chains/${eateryChainId}/eateries',

  SEARCH_EATERIES_NEARBY: '/search/eateries/near-by',
  FETCH_FEATURE_CHAINS_MERCHANT: '/search/eateries/featured-chains',
  FETCH_MERCHANT_THOUGHTFUL: '/search/eateries/nice-eateries',
  FETCH_PROMOTIONS_MERCHANT: '/search/eateries/deals',
  FETCH_RECENT_MERCHANT: '/search/eateries/recent',
  FETCH_NEWEST_MERCHANT: '/search/eateries/promoted',
  FETCH_GROUP_PROMOTIONS_MERCHANT: '/search/eateries/group-deals',
  FETCH_FAVORITE_MERCHANTS: '/users/me/favorite-eateries',

  FETCH_NEWSFEED_BANNERS: '/newsfeed/banners',
  FETCH_EVENT_BY_ID: '/events/${eventId}',
  FETCH_EVENT_FEATURED_EATERIES: '/events/${eventId}/featured-eateries',

  FETCH_NEWSFEED_EATERY_RATINGS: '/newsfeed/eatery-ratings',

  SEARCH_SUGGEST_DISHES: '/search/suggest/dishes',
  SEARCH_SUGGEST_EATERIES_BY_DISH_ID: '/search/suggest/eatery/dishes',
  SEARCH_SUGGEST_EATERIES: '/search/suggest/eateries',
};

export function getAPIUrlAndQuery(query = {}, overrideAPI) {
  let api = query.q ? API.SEARCH_EATERY_CHAINS : API.SEARCH_EATERIES;
  if (overrideAPI) api = overrideAPI;

  const apiParams = {};
  const apiQuery = produce(query, (draft) => {
    draft.lat = draft.globalAddress?.lat;
    draft.lng = draft.globalAddress?.lng;

    if (!draft.cityId && !draft.chainId) { // chainId will ignore cityId
      draft.cityId = draft.globalAddress?.cityId;
      draft.districtId = draft.globalAddress?.districtId;
    }

    if (draft.favoriteEateriesApi) {
      api = API.FETCH_FAVORITE_MERCHANTS;
      delete draft.favoriteEateriesApi;
      return;
    }

    if (draft.eateryRatingsApi) {
      api = API.FETCH_NEWSFEED_EATERY_RATINGS;
      delete draft.eateryRatingsApi;
    }

    if (draft.eventId) {
      api = API.FETCH_EVENT_FEATURED_EATERIES;
      apiParams.eventId = draft.eventId;
      delete draft.eventId;
    }

    if (Array.isArray(draft.filters) && draft.filters.length > 0) draft.filters = draft.filters.join(',');
    if (Array.isArray(draft.sorts) && draft.sorts.length > 0) {
      const nearbySortIndex = draft.sorts.findIndex((value) => value === 'nearby');
      if (nearbySortIndex > -1 && (!draft.lat || !draft.lng)) {
        draft.sorts.splice(nearbySortIndex, 1);
      }
      if (draft.sorts.length > 0) draft.sorts = draft.sorts.join(',');
    }

    delete draft.shouldShowSortFilter;
    delete draft.hideEmpty;
    delete draft.globalAddress;

    if (draft.searchSuggestEateriesByDishId) {
      api = API.SEARCH_SUGGEST_EATERIES_BY_DISH_ID;
      delete draft.searchSuggestEateriesByDishId;
      return;
    }

    if (draft.searchSuggestEateriesApi) {
      api = API.SEARCH_SUGGEST_EATERIES;
      delete draft.searchSuggestEateriesApi;
      return;
    }

    if (draft.nearbyApi) {
      api = API.SEARCH_EATERIES_NEARBY;
      delete draft.nearbyApi;
      return;
    }

    if (draft.searchDishesApi) {
      api = API.SEARCH_EATERY_DISHES;
      delete draft.searchDishesApi;
      return;
    }

    if (draft.newestApi) {
      api = API.FETCH_NEWEST_MERCHANT;
      delete draft.newestApi;
      return;
    }
    if (draft.featureChainApi) {
      api = API.FETCH_FEATURE_CHAINS_MERCHANT;
      delete draft.featureChainApi;
      return;
    }
    if (draft.merchanteateries) {
      api = API.FETCH_MERCHANT_THOUGHTFUL;
      delete draft.merchanteateries;
      return;
    }
    if (draft.promotionApi) {
      api = API.FETCH_PROMOTIONS_MERCHANT;
      delete draft.promotionApi;
      return;
    }
    if (draft.recentApi) {
      api = API.FETCH_RECENT_MERCHANT;
      delete draft.recentApi;
      return;
    }
    if (draft.groupApi) {
      api = API.FETCH_GROUP_PROMOTIONS_MERCHANT;
      delete draft.groupApi;
      return;
    }
    if (draft.snacks) {
      delete draft.snacks;
      return;
    }

    delete draft.breakfast;
    delete draft.midnight;
    delete draft.food;
  });

  return { url: api, query: apiQuery, params: apiParams };
}

export function fetchNewsFeed(params = {}) {
  return async (dispatch, getState) => {
    const query = parseQuery(params.query, getState());

    const apiTypeAndKey = (() => {
      if (params.loadMore) {
        return {
          type: 'NEWSFEED_FETCH_MORE',
          key: getKey({ ...query, page: 1 }),
          force: true,
        };
      }
      return { type: 'NEWSFEED_FETCH', key: getKey(query), force: params.force };
    })();

    return getAPI({
      ...apiTypeAndKey,
      ...getAPIUrlAndQuery(query),
      callback: params.callback,
      callbackFailure: params.callbackFailure,
    })(dispatch, getState);
  };
}

export function fetchNewsFeedStats(params = {}) {
  return async (dispatch, getState) => {
    const query = parseQuery(params.query, getState());

    const eateryStats = await callAPI('get', {
      url: API.SEARCH_STATS_EATERY_CHAIN,
      query: { q: query.q, sorts: query.sorts, filters: query.filters },
    });
    const dishStats = await callAPI('get', {
      url: API.SEARCH_STATS_DISH,
      query: { q: query.q },
    });

    if (eateryStats.status !== 200 || dishStats.status !== 200) return null;
    typeof params.callback === 'function' && params.callback({
      eateries: eateryStats.body.data,
      dishes: dishStats.body.data,
    });
  };
}

export function fetchBanners() {
  return async (dispatch, getState) => {
    const currentCity = getCurrentCity(getState());
    if (!currentCity) return;

    const cityId = currentCity.type === 'city' ? currentCity.city.id : currentCity.district.city.id;
    const query = parseQuery({ screen: 'loship_service', cityId }, getState());

    return getAPI({
      type: 'BANNER_FETCH_ALL',
      key: `banners.c_${cityId}`,
      url: API.FETCH_NEWSFEED_BANNERS,
      query,
    })(dispatch, getState);
  };
}

export function fetchEventById(params = {}) {
  const { eventId, callback } = params;
  return async (dispatch, getState) => {
    const existedData = getEventById(getState(), { eventId });
    if (existedData) {
      if (existedData.get('data') && typeof callback === 'function') callback(existedData.get('data').toObject());
      return;
    }

    dispatch({
      type: 'BANNER_EVENT_FETCH_PENDING',
      key: `banners.event.${eventId}`,
    });
    const eventRes = await callAPI('get', { url: API.FETCH_EVENT_BY_ID, params: { eventId } });

    if (eventRes.status !== 200) {
      return dispatch({
        type: 'BANNER_EVENT_FETCH_FAILED',
        key: `banners.event.${eventId}`,
      });
    }

    const data = { ...eventRes.body.data };

    if (typeof callback === 'function') callback(data);
    return dispatch({
      type: 'BANNER_EVENT_FETCH',
      key: `banners.event.${eventId}`,
      data,
    });
  };
}

export function searchSuggestDishes(params = {}) {
  return getAPI({
    type: 'NEWSFEED_SUGGEST_DISHES',
    key: 'newsfeed.suggestDishes',
    url: API.SEARCH_SUGGEST_DISHES,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function fetchEateryRatingById(params) {
  if (!params.ratingId) return;

  return getAPI({
    type: 'EATERY_RATING_FETCH',
    key: `${params.ratingId}.eateryRating`,
    store: 'newsfeed',
    url: ACTION_API.FETCH_EATERY_RATING_BY_ID,
    params:{ ratingId: params.ratingId },
    accessToken: params.accessToken,
  });
}

export function updateLikeEateryRating(params) {
  if (!params.ratingId) return;

  return postAPI({
    type: 'EATERY_RATING_UPDATE',
    key: params.ratingId,
    url: ACTION_API.UPDATE_LIKE_EATERY_RATING,
    params: { ratingId: params.ratingId },
    currentKey: params.currentKey,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function deleteLikeEateryRating(params) {
  if (!params.ratingId) return;

  return delAPI({
    type: 'EATERY_RATING_UPDATE',
    key: params.ratingId,
    url: ACTION_API.UPDATE_LIKE_EATERY_RATING,
    params: { ratingId: params.ratingId },
    currentKey: params.currentKey,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}
