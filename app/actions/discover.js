import { register } from 'actions/helpers';
import { LINK_AUTH } from 'settings/variables';

const { getAPI, getMoreAPI } = register('discover');

export const API = { FETCH_VIDEO: LINK_AUTH + '/videos' };

export function fetchVideo({ cityId = 50, limit = 10 }) {
  return getAPI({
    type: 'DISCOVER_FETCH_VIDEO',
    key: 'all',
    url: API.FETCH_VIDEO,
    params: {},
    query: {
      limit,
      isShipping: true,
      cityId,
    },
  });
}

export function fetchMoreVideo(url) {
  return getMoreAPI({
    type: 'FETCH_VIDEO_MORE',
    key: 'all',
    url: LINK_AUTH + url,
  });
}

export function fetchTopVideo({ cityId = 50, limit = 6 }) {
  return getAPI({
    type: 'DISCOVER_FETCH_TOP_VIDEO',
    key: 'top',
    url: API.FETCH_VIDEO,
    query: {
      limit,
      isTop: true,
      isShipping: true,
      cityId,
    },
  });
}
