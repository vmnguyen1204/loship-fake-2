import { register } from 'actions/helpers';

const { getAPI, postAPI } = register('user');

export const API = {
  SEARCH: '/search/eateries?page=${0}',
  FETCH_SHIP_PRICE: '/eateries/slug:${merchant}',

  INVITE_USE_APP: '/invites/use-app',
};

export function searchMerchant(params) {
  const { key } = params;

  return getAPI({
    type: 'SEARCH_RESULT',
    url: API.SEARCH,
    key,
    child: {
      store: 'merchant',
      id: 'slug',
      type: 'MERCHANT_APPEND',
    },
  });
}

export function sendUseAppInvitation({ countryCode, phoneNumber, callback, callbackFailure }) {
  return postAPI({
    type: 'INVITE_USE_APP',
    url: API.INVITE_USE_APP,
    content: { countryCode, phoneNumber },
    callback,
    callbackFailure,
  });
}
