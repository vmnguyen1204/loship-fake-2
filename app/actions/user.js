import { LINK_AUTH } from 'settings/variables';
import { getCurrentUsername } from 'reducers/metadata';
import { register } from 'actions/helpers';
import { getDebtOrders, getPaymentCardsSupportPayDebt } from 'reducers/user';

const { getAPI, putAPI, postAPI, delAPI } = register('user');

export const API = {
  FETCH: '/users/username:${username}',
  FETCH_LATTE: LINK_AUTH + '/users/username:${username}',
  FETCH_TURN_OVER: '/insightly/salesmans/${userId}/income',
  FETCH_ADDRESSES: '/users/me/addresses',
  FETCH_REFERRAL_CODE: '/users/me/referral',
  FETCH_REFEREES: '/users/me/referees',

  UPDATE_PROFILE: LINK_AUTH + '/users/me/profile',
  UPDATE_AVATAR: LINK_AUTH + '/users/me/avatar',
  CHANGE_PASSWORD: LINK_AUTH + '/auth/change-password',
  LINK_FACEBOOK: LINK_AUTH + '/users/me/facebook-link',

  FETCH_PAYMENT_CARDS: '/users/me/payment-cards',
  DELETE_PAYMENT_CARD: '/users/me/payment-cards/${cardId}',
  FETCH_PAYMENT_CARDS_SUPPORT_PAY_DEBT: '/users/me/payment-card/debt-fees',
  FETCH_DEBT_ORDERS: '/users/me/debt-orders',

  GET_SECRET_PASSWORD: LINK_AUTH + '/users/me/update-phone-login',
  GET_PHONE_NUMBER_VERIFIED_CODE: LINK_AUTH + '/users/me/verify-phone-number',
  RESEND_TOKEN: LINK_AUTH + '/users/me/update-phone-login/resend-token',
  RESEND_TOKEN_FOR_FB: LINK_AUTH + '/users/me/verify-phone-number/resend-token',

  VERIFIED_TOKEN: LINK_AUTH + '/users/me/update-phone-login/verify-token',
  VERIFIED_TOKEN_FOR_FB: LINK_AUTH + '/users/me/update-phone-number',
};

export function fetch(params) {
  const url = API.FETCH;
  const key = params.user;

  return getAPI({
    type: 'USER_FETCH',
    key,
    url,
    params: { username: params.user },
  });
}

export function fetchShippingAddresses(params = {}) {
  const force = params.force;

  return getAPI((state) => {
    const username = getCurrentUsername(state);
    return {
      type: 'USER_FETCH_SHIPPING_ADDRESSES',
      url: API.FETCH_ADDRESSES,
      key: `${username}.shippingAddresses`,
      force,
      accessToken: params.accessToken,
    };
  });
}

export function fetchReferralCode() {
  return postAPI((state) => {
    const username = getCurrentUsername(state);
    return {
      type: 'USER_FETCH_REFERRAL_CODE',
      url: API.FETCH_REFERRAL_CODE,
      key: `${username}.referralCode`,
    };
  });
}

export function fetchReferees() {
  return getAPI((state) => {
    const username = getCurrentUsername(state);
    return {
      type: 'USER_FETCH_REFEREES',
      url: API.FETCH_REFEREES,
      key: `${username}.referees`,
    };
  });
}

export function updateProfile(params) {
  return putAPI({
    type: 'USER_UPDATE_PROFILE',
    url: API.UPDATE_PROFILE,
    key: params.user,
    content: params.data,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function updateAvatar(params) {
  return putAPI({
    type: 'USER_UPDATE_AVATAR',
    url: API.UPDATE_AVATAR,
    key: params.user,
    content: { avatar: params.image },
  });
}

// TODO: countryCode
export function changePassword(params) {
  return postAPI({
    type: 'USER_CHANGE_PASSWORD',
    url: API.CHANGE_PASSWORD,
    key: params.user,
    content: {
      countryCode: params.countryCode,
      phoneNumber: params.phoneNumber,
      oldPassword: params.oldPassword,
      password: params.password,
    },
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function linkFacebook(params) {
  return postAPI({
    type: 'USER_LINK_FACEBOOK',
    url: API.LINK_FACEBOOK,
    key: params.user,
    content: { accessToken: params.fbAccessToken },
    callback: params.callback,
    callbackFailure: params.callbackFailure,
    accessToken: params.accessToken,
  });
}

export function unlinkFacebook(params) {
  return delAPI({
    type: 'USER_UNLINK_FACEBOOK',
    url: API.LINK_FACEBOOK,
    key: params.user,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

/** Send SMS verified code + get back secret password */
export function getSecretPassword(params) {
  return postAPI({
    type: 'USER_GET_SECRET_PASSWORD',
    url: API.GET_SECRET_PASSWORD,
    key: params.user,
    content: params.data,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

/** Send SMS verified code (use for Facebook account) */
export function getPhoneNumberVerifiedCode(params) {
  return postAPI({
    type: 'USER_GET_PHONE_NUMBER_VERIFIED_CODE',
    url: API.GET_PHONE_NUMBER_VERIFIED_CODE,
    key: params.user,
    content: params.data,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function resendToken(params) {
  return postAPI({
    type: 'USER_RESEND_TOKEN',
    url: API.RESEND_TOKEN,
    key: params.user,
    content: params.data,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function resendTokenForFb(params) {
  return postAPI({
    type: 'USER_RESEND_TOKEN_FOR_FB',
    url: API.RESEND_TOKEN_FOR_FB,
    key: params.user,
    content: params.data,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function verifiedToken(params) {
  return postAPI({
    type: 'USER_VERIFIED_TOKEN',
    url: API.VERIFIED_TOKEN,
    key: params.user,
    content: params.data,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function verifiedTokenForFb(params) {
  return postAPI({
    type: 'USER_VERIFIED_TOKEN_FOR_FB',
    url: API.VERIFIED_TOKEN_FOR_FB,
    key: params.user,
    content: params.data,
    callback: params.callback,
    callbackFailure: params.callbackFailure,
  });
}

export function fetchPaymentCards(params) {
  const force = params && params.force;

  return getAPI((state) => {
    const username = getCurrentUsername(state);
    return {
      type: 'USER_FETCH_PAYMENT_CARDS',
      url: API.FETCH_PAYMENT_CARDS,
      key: `${username}.paymentCards`,
      force,
    };
  });
}

export function deletePaymentCard(params) {
  return delAPI((state) => {
    const username = getCurrentUsername(state);
    return {
      type: 'USER_DELETE_PAYMENT_CARD',
      url: API.DELETE_PAYMENT_CARD,
      key: `${username}.paymentCards`,
      params: { cardId: params.cardId },
      cardId: params.cardId,
      callback: params.callback,
    };
  });
}


export function fetchPaymentCardsSupportPayDebt(params) {
  const force = params && params.force;

  return async (dispatch, getState) => {
    const existedData = getPaymentCardsSupportPayDebt(getState());
    if (existedData && existedData.get('status') === 'SUCCESS') {
      return typeof params.callback === 'function' && params.callback({ body: { data: existedData.toJS().data } });
    }

    return getAPI((state) => {
      const username = getCurrentUsername(state);
      return {
        type: 'USER_FETCH_PAYMENT_CARDS_SUPPORT_PAY_DEBT',
        url: API.FETCH_PAYMENT_CARDS_SUPPORT_PAY_DEBT,
        key: `${username}.paymentCardsSupportPayDebt`,
        force,
        callback: params.callback,
      };
    })(dispatch, getState);
  };
}

export function fetchDebtOrders(params = {}) {
  const force = params.force;

  return async (dispatch, getState) => {
    const existedData = getDebtOrders(getState());
    if (existedData && existedData.get('status') === 'SUCCESS') {
      return typeof params.callback === 'function' && params.callback({ body: { data: existedData.toJS().data } });
    }

    return getAPI((state) => {
      const username = getCurrentUsername(state);
      return {
        type: 'USER_FETCH_DEBT_ORDERS',
        url: API.FETCH_DEBT_ORDERS,
        key: `${username}.debtOrders`,
        force,
        callback: params.callback,
      };
    })(dispatch, getState);
  };
}
