import Button from 'components/shared/button';
import React from 'react';
import { addCurrency } from 'utils/format';
import { getImageUrl } from 'utils/image';
import { getString } from 'utils/i18n';

const AVATAR_SIZE = 320;

class UseApp extends React.Component {
  render() {
    const { data } = this.props;

    if (!data) return <div>{getString('only_available_on_mobile_app')}</div>;
    return (
      <div className="lzi-item">
        <div className="lzi-item-header">
          <img src={getImageUrl(data.image, AVATAR_SIZE)} alt="Buy on Loship app" />
          <div className="bg-shade" />
        </div>
        <div className="lzi-item-content">
          <h3>{data.name}</h3>
          <b>{addCurrency(data.price)}</b>
          {data.eatery && data.eatery.address && data.eatery.address.full}
          <Button type="primary" className="lzi-btn">
            {getString('buy_now_with_loship_app')}
          </Button>
        </div>
      </div>
    );
  }
}

export default UseApp;
