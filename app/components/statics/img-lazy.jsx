import cx from 'classnames';
import enqueue from 'lib/request-pool';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Waypoint } from 'react-waypoint';
import { getImageUrl } from 'utils/image';

const CACHED = {};

class ImageLazy extends PureComponent {
  static propTypes = {
    src: PropTypes.string,
    placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  };

  static contextTypes = { isMobile: PropTypes.bool };

  static defaultProps = {
    placeholder: '/dist/images/avatar.png',
  };

  state = {};

  componentDidMount() {
    this._isMount = true;

    if (CACHED[this.props.src + '.main']) {
      this.setState({ mainImg: CACHED[this.props.src + '.main'] });
      this.loadLevel = 'mainImg';
    } else if (CACHED[this.props.src + '.full']) {
      this.setState({ fullImg: CACHED[this.props.src + '.full'] });
      this.loadLevel = 'fullImg';
    } else {
      this.loadLevel = 'placeholder'; // placeholder | mainImg | fullImg
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.src !== prevProps.src) {
      this.setState({ loadFailed: false });
      this.loadLevel = 'placeholder';
    }
  }

  componentWillUnmount() {
    this._isMount = false;
  }

  render() {
    const { src, className, shape, mode, placeholder } = this.props;
    const { mainImg, fullImg, loadFailed, showBigAvatar } = this.state;
    const imgSrc = fullImg || mainImg || placeholder;

    const staticMode = !['hover', 'click'].includes(mode);
    const onClick = mode === 'click' ? this.showBigAvatar : this.props.onClick;
    const scrollableAncestor = this.props.scrollableAncestor !== undefined ? this.props.scrollableAncestor : 'window';

    return (
      <div
        className={cx('img-lazy', className, shape, { 'hover-mode': mode === 'hover' })}
        onClick={onClick}
        onMouseEnter={mode === 'hover' ? this.loadFullImage : undefined}
        onMouseLeave={mode === 'hover' ? this.cancelLoadFullImg : undefined}
      >
        <div ref="img" className="img" style={{ backgroundImage: imgSrc && `url(${imgSrc})` }} data-src={src} />
        {src && !mainImg && !loadFailed && <Waypoint onEnter={this.loadImage} bottomOffset={-100} scrollableAncestor={scrollableAncestor} />}

        {!staticMode && (
          <div
            className={cx('big-avatar-wrapper', { hidden: !showBigAvatar })}
            onClick={this.hideBigAvatar}
          >
            <div ref="big-img" className="big-avatar" style={{ backgroundImage: `url(${imgSrc})` }} />
          </div>
        )}
      </div>
    );
  }

  loadImage = () => {
    if (this.loadLevel !== 'placeholder') return;
    if (this.state.loadFailed) return;

    const { placeholder, size, type } = this.props;
    const ref = this.refs['img'];
    if (!ref || !ref.getAttribute('data-src')) return;

    const url = ref.getAttribute('data-src');
    // ref.removeAttribute('data-src'); // keep data-src for load full img
    const mainUrl = getImageUrl(url, size, type || 'o', null, placeholder);

    enqueue(
      () => {
        /** LOAD TINY IMAGE */
        return new Promise((resolve, reject) => {
          const tinyImage = new Image();
          tinyImage.setAttribute('src', mainUrl);
          tinyImage.onerror = () => {
            return reject(new Error('ERROR: cannot loading tiny image'));
          };
          tinyImage.onload = () => {
            return resolve(tinyImage);
          };
        });
      },
      (img, err) => {
        if (err) {
          if (!this._isMount) return;
          return this.setState({ loadFailed: true });
        }

        if (!this._isMount) return;
        if (this.loadLevel !== 'fullImg') {
          this.loadLevel = 'mainImg';
          ref.setAttribute('style', `background-image: url(${img.getAttribute('src')})`);
          this.loadHoverImage(img, true);
          this.setState({ mainImg: mainUrl });
          CACHED[url + '.main'] = mainUrl;
        }
      },
      { priority: true },
    );
  };

  loadFullImage = () => {
    if (!['hover', 'click'].includes(this.props.mode)) return;

    if (this.loadLevel === 'fullImg') return;
    if (this.state.loadFailed) return;

    const { placeholder } = this.props;
    const ref = this.refs['img'];
    if (!ref || !ref.getAttribute('data-src')) return;

    const url = ref.getAttribute('data-src');
    ref.removeAttribute('data-src');
    const fullUrl = getImageUrl(url.replace('/resized', '/original'), 640, 'o', null, placeholder);

    enqueue(
      () => {
        return new Promise((resolve, reject) => {
          /** LOAD FULL IMAGE */
          const fullImage = new Image();
          fullImage.setAttribute('src', fullUrl);
          fullImage.onerror = () => {
            return reject(new Error('ERROR: cannot loading full image'));
          };
          fullImage.onload = () => {
            return resolve(fullImage);
          };
        });
      },
      (img, err) => {
        if (err) {
          if (!this._isMount) return;
          return this.setState({ loadFailed: true });
        }

        if (!this._isMount) return;
        this.loadLevel = 'fullImg';
        ref.setAttribute('style', `background-image: url(${img.getAttribute('src')})`);
        this.loadHoverImage(img);
        this.setState({ fullImg: fullUrl });
        CACHED[url + '.full'] = fullUrl;
      },
      { level: 2 },
    );
  };

  cancelLoadFullImg = () => {
  };

  loadHoverImage = (image, blur) => {
    const ref = this.refs['big-img'];
    if (!ref) return;

    const hoverMode = this.props.mode === 'hover' && !this.context.isMobile;

    let [imgWidth, imgHeight] = [
      window.innerWidth - 48,
      ((window.innerWidth - 48) / image.naturalWidth) * image.naturalHeight,
    ];
    const [maxWidth, maxHeight] = [window.innerWidth - 48, hoverMode ? 400 : window.innerHeight * 0.8];

    if (imgHeight > maxHeight) {
      imgWidth = (imgWidth / imgHeight) * maxHeight;
      imgHeight = maxHeight;
    }
    if (imgWidth > maxWidth) {
      imgHeight = (imgHeight / maxWidth) * imgWidth;
      imgWidth = maxWidth;
    }

    if (blur) {
      ref.setAttribute(
        'style',
        `width: ${imgWidth}px; height: ${imgHeight}px;
          background-image: url(${image.getAttribute('src')});
          filter: blur(4px);
        }`,
      );
    } else {
      ref.setAttribute(
        'style',
        `width: ${imgWidth}px; height: ${imgHeight}px;
          background-image: url(${image.getAttribute('src')});
          transition: width 0.25s, height 0.25s, blur 0.25s;`,
      );
    }
  };

  showBigAvatar = (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.setState({ showBigAvatar: true });
    this.loadFullImage();
  };

  hideBigAvatar = (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (!e.target.classList.contains('big-avatar')) this.setState({ showBigAvatar: false });
  };
}

export default ImageLazy;
