import React from 'react';
import { getRawPhoneNumber, phoneNumber } from 'utils/format';

class CSKHSuggestLosupply extends React.Component {
  render() {
    const { phoneNumber: phone, countryCode } = this.props;
    const rawPhoneNumber = getRawPhoneNumber(phoneNumber(phone, { countryCode }));

    return (
      <div className="cskh-suggest-losupply">
        <iframe
          title="CSKH suggest losupply"
          src={`https://docs.google.com/forms/d/e/1FAIpQLSeEL4_c0qDXsY9fJkZV_zGgXKCzHoRjZn1HDtBHJfkapATAwA/viewform?usp=pp_url&entry.231808458=${rawPhoneNumber}`}
        />
        <input id="cpn" type="hidden" value={rawPhoneNumber} />
      </div>
    );
  }
}

export default CSKHSuggestLosupply;
