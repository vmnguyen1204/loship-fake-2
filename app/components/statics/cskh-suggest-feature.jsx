import React from 'react';
import { getRawPhoneNumber, phoneNumber } from 'utils/format';

class CSKHSuggestFeature extends React.Component {
  render() {
    const { phoneNumber: phone, countryCode } = this.props;
    const rawPhoneNumber = getRawPhoneNumber(phoneNumber(phone, { countryCode }));

    return (
      <div className="cskh-suggest-feature">
        <iframe
          title="CSKH suggest feature"
          src={`https://docs.google.com/forms/d/e/1FAIpQLSdAncNscS4E5EzSFHBbkdjLsGkgQwKyhjcw41EDubdTpoo5DA/viewform?usp=pp_url&entry.222268239=${rawPhoneNumber}`}
        />
        <input id="cpn" type="hidden" value={rawPhoneNumber} />
      </div>
    );
  }
}

export default CSKHSuggestFeature;
