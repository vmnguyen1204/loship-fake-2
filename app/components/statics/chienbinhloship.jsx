import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import Button from 'components/shared/button';

if (process.env.BROWSER) {
  require('assets/styles/components/trothanhchienbinhloship.scss');
}

class TroThanhChienBinhLoship extends BaseComponent {
  render() {
    return (
      <div className="section-newsfeed wrap-section-chien-binh-loship">
        <div className="title">
          <h2>{this.getString('become_a_loship_warrior_now')}</h2>
        </div>
        <div className="section-chien-binh-loship">
          <div className="linear-bg" />
          <div className="content">
            <h3>{this.getString('become_a_loship_warrior_fast_and_free')}</h3>
            <p>{this.getString('easily_become_a_loship_warrior')}</p>
            <p>
              {this.getString(
                'loship_an_app_that_help_deliver_milk_tea_and_food_within_1_hour_using_your_own_motorbike_and_smart_phone_android_or_iphone',
              )}
            </p>
          </div>
          <div>
            <Button type="primary" className="btn-banner" href="http://chienbinhloship.com" target="_blank">
              {this.getString('register_now')}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(TroThanhChienBinhLoship);
