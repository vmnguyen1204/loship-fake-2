import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { CUSTOM_CONTENT, open } from 'actions/popup';

if (process.env.BROWSER) {
  require('assets/styles/components/hint.scss');
}
class Hint extends BaseComponent {
  static defaultProps = { className: '' };

  render() {
    const { title, text, className, onClick, noHoverHint, noClickHint } = this.props;
    const isArray = text && Array.isArray(text);
    const isFunc = text && typeof text === 'function';
    const isString = text && typeof text === 'string';
    return (
      <span
        className={`hint-icon ${className}`}
        onClick={(e) => {
          if (noClickHint) return;
          if (onClick) {
            onClick(e);
          } else {
            this.props.dispatch(
              open(CUSTOM_CONTENT, {
                title,
                content: text,
                wrapperClassName: this.props.popupWrapperClassName,
              }),
            );
          }
          e.stopPropagation();
          e.preventDefault();
          return false;
        }}
      >
        {this.props.children || <div className="question">?</div>}
        {!noHoverHint && (
          <div className="hint-content">
            {isArray && text.map((str) => <p key={str}>{str}</p>)}
            {isFunc && text()}
            {isString && <p>{text}</p>}
          </div>
        )}
      </span>
    );
  }
}

export default initComponent(Hint);
