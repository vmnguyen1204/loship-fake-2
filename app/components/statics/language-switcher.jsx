import React from 'react';
import BaseComponent from 'components/base';
import { changeLanguage, getClientLocale, getLanguageList, localeToString } from 'utils/i18n';

if (process.env.BROWSER) {
  require('assets/styles/components/_language-switcher.scss');
}

class LanguageSwitcher extends BaseComponent {
  render() {
    const languages = getLanguageList();
    if (!languages) return false;
    const currentLocale = getClientLocale();

    return (
      <div className={`language-switcher dropdown-wrapper ${this.props.className}`}>
        <div className="selected">
          <a>
            <img src={`/dist/images/flags/${currentLocale}.png`} className="icon" alt={currentLocale} />
            {this.getString(localeToString(currentLocale))}
          </a>
          <i className="lz lz-triangle-down" />
        </div>
        {this.renderMenu()}
      </div>
    );
  }

  renderMenu() {
    const languages = getLanguageList();

    return (
      <div className="dropdown-menu centered">
        <ul>
          {languages.map((lang) => {
            return (
              <li key={lang.locale} className="dropdown-item">
                <a className="has-icon change-language" data-change={lang.locale} onClick={this.handleChangeLanguage.bind(this, lang.locale)}>
                  <img src={`/dist/images/flags/${lang.locale}.png`} className="icon" alt={lang.locale} />
                  <span>{this.getString(lang.name)}</span>
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }

  handleChangeLanguage = (locale) => {
    return changeLanguage(locale);
  };
}

export default LanguageSwitcher;
