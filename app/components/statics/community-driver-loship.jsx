import cx from 'classnames';
import Button from 'components/shared/button';
import Footer from 'components/shared/footer';
import moment from 'moment';
import React from 'react';
import { LINK_DOMAIN_SHIPPER } from 'settings/variables';
import { getString } from 'utils/i18n';
import { getAvatarUrl } from 'utils/image';

class CongDongChienBinh extends React.Component {
  renderStickyBanner = () => {
    if (!this.props.isMobile) return;

    const { driver } = this.props;

    return (
      <div
        className="banner-sticky"
        data-ios-url={`loshipdriver://loship.vn/rating?id=${driver.id}`}
        data-android-url={`loshipdriver://loship.vn/rating?id=${driver.id}`}
      >
        <div className="viewer-title">
          {getString('banner_sticky_driver_viewer')}
        </div>
        <a className="close">
          <i className="fas fa-times" />
        </a>
        <Button type="primary" fw className="btn-open-app">
          <span>{getString('continue_on_loship_driver')}</span>
          <i className="fas fa-chevron-right pull-right" />
        </Button>
      </div>
    );
  }

  render() {
    const { isMobile, driver } = this.props;
    if (!driver) return null;

    const driverTime = driver.createdAt;
    const createdAt = moment(driverTime).format('DD/MM/YYYY');

    return (
      <div className={cx('CongDongChienBinh', isMobile ? 'mobile' : 'desktop')}>
        <div className="nav-wrapper">
          {isMobile ? (
            <div className="navbar-mobile">
              <div className="container">
                <div>{getString('driver_community')}</div>
              </div>
            </div>
          ):(
            <div className="navbar">
              <div className="container">
                <div className="logo">
                  <img src="/dist/images/logo.png" />
                </div>
              </div>
            </div>
          )}
          <div className="section">
            <div className="title-section">
              <h3>{getString('see_review_driver')} {(driver.ratedShipper.name)}</h3>
            </div>
            <div className="content-section">
              <div className="review-driver">
                <div className="time-review">
                  <div>{getString('Praised')}</div>
                  <span>{createdAt}</span>
                </div>
                <div className="profile-driver">
                  <img className="avatar" src={getAvatarUrl(driver.ratedShipper.avatar)} />
                  <div className="name-driver">
                    <h4>{(driver.ratedShipper.name)}</h4>
                    <div className="rate-driver">
                      <img src="/dist/images/emotion-good.png" />
                      <div className="rate">{(driver.ratedShipper.count.goodRate)}%</div>
                    </div>
                  </div>
                </div>
                <div className="description nice-scroll">
                  {getString(driver.content)}
                </div>
                <div className="action">
                  <div className="btn-action btn-clap">
                    <img src="/dist/images/icon-like.png" />
                    <span>{(driver.likeTotal) + ' '}Hoan Hô</span>
                  </div>
                  <div className="btn-action btn-share">
                    <img src="/dist/images/icon-share.png" />
                    <span>{getString('share')}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer location={this.props.location} />

        {this.handleBtnlike()}
        {this.handleBtnshare()}
        {this.renderStickyBanner()}
      </div>
    );
  }

  handleBtnlike = () => {
    return (
      <div className="popup-wrapper popup-clap">
        <div className="popup-bg" />
        <div className="popup popup-like">
          <div className="btn-back">
            <i className="fas fa-times icon-close" />
          </div>
          <div className="content">
            <img src="/dist/images/lodriver.png" />
            <div className="text-comtent">Để có thể tương tác với lời khen
              bạn cần đăng nhập trên ứng dụng Loship chiến binh bằng cách quét
              mã QR bên dưới
            </div>
            <img src="/dist/images/driver-qrcode.png" />
            <div className="dow-app">
              <a href="https://apps.apple.com/vn/app/id1335050744" target="_blank" rel="noopener noreferrer">
                <img src="/dist/images/app-store.png" />
              </a>
              <a href="https://play.google.com/store/apps/details?id=lozi.loship_driver" target="_blank" rel="noopener noreferrer">
                <img src="/dist/images/google-play.png" />
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  };

  handleBtnshare = () => {
    const { driver } = this.props;
    const sharingUrl = `${LINK_DOMAIN_SHIPPER}/danh-gia/${driver.id}`;

    return (
      <div className="popup-wrapper popup-link">
        <div className="popup-bg" />
        <div className="popup popup-share">
          <div className="btn-back">
            <i className="fas fa-times icon-close" />
            <div className="header"> Chia sẻ lời khen</div>
          </div>
          <div className="input-link">
            <img src="/dist/images/icon-linked.png" />
            <a className="input-link-value" data-sharing-url={sharingUrl}>{sharingUrl}</a>
          </div>
          <div className="btn-copy">
            <p>Sao chép</p>
          </div>
        </div>
      </div>
    );
  };
}

export default CongDongChienBinh;
