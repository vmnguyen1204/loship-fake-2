import cx from 'classnames';
import React from 'react';
import { iife } from 'utils/helper';
import * as cookies from 'utils/shims/cookie';
import Link from 'utils/shims/link';
import moment from 'utils/shims/moment';

class FeatureIntroducer extends React.Component {
  state = { visible: false }

  componentDidMount() {
    this.__isMounted = true;

    if (cookies.get(this.props.storedKey)) return;
    if (this.props.deadline) {
      const now = moment();
      const deadline = moment(this.props.deadline, 'HH:mm:ss DD/MM/YYYY');
      if (now.isAfter(deadline)) return;
    }
    if (typeof this.props.shouldAutoClose === 'function') {
      const result = this.props.shouldAutoClose();
      if (result === true) return this.handleCloseFeatureIntroduction(true);
    }

    this.setState({ visible: true });
  }

  componentWillUnmount() {
    this.__isMounted = false;
  }

  render() {
    const { text, className, mode } = this.props;
    const eventHandlers = mode === 'hover'
      ? { onMouseEnter: this.handleCloseFeatureIntroduction }
      : { onClick: this.handleCloseFeatureIntroduction };

    const [Component, compProps] = iife(() => {
      if (this.props.to || this.props.href) return [Link, { to: this.props.to, href: this.props.href }];
      return ['span', {}];
    });

    return (
      <Component className={cx('feature-introducer-wrapper', className)} {...eventHandlers} {...compProps}>
        {this.props.children}
        {this.state.visible && <label className={cx('feature-introducer', text && 'has-text')}>{text}</label>}
      </Component>
    );
  }

  handleCloseFeatureIntroduction = (force) => {
    if (!this.state.visible && force !== true) return;

    cookies.set(this.props.storedKey, 1);
    setTimeout(() => {
      if (!this.__isMounted) return;
      this.setState({ visible: false });
    }, 500);
  }
}

export default FeatureIntroducer;
