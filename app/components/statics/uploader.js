import { CUSTOM_CONTENT, open } from 'actions/popup';

import BaseComponent from 'components/base';
import React from 'react';
import cx from 'classnames';
import { getImageFromFile } from 'utils/LoshipImage';
import initComponent from 'lib/initComponent';

const MIN_SIZE = {
  avatar: [1000, 1000],
  dish: [800, 800],
};

class FileUploader extends BaseComponent {
  static defaultProps = { thumbSize: [] };

  render() {
    const { className, accept } = this.props;
    return (
      <div className={cx('file-uploader', className)} onClick={this.openFileLoader}>
        {this.props.children}
        <input
          ref={this.getRef}
          type="file"
          className="file-upload-js"
          onChange={this.handleFileChange}
          accept={accept}
        />
      </div>
    );
  }

  getRef = (ref) => {
    if (ref) this.ref = ref;
  };

  openFileLoader = () => {
    this.ref.value = '';
    this.ref.click();
  };

  handleFileChange = (e) => {
    if (typeof this.props.onFileChange === 'function') return this.props.onFileChange(e);

    const { target } = e;
    Promise.race([
      // eslint-disable-next-line no-async-promise-executor
      new Promise(async (resolve, reject) => {
        const data = await getImageFromFile(target);
        if (MIN_SIZE[this.props.type]) {
          data.__rawFile.file.__dimensions.then((err, dimensions) => {
            if (dimensions.width < MIN_SIZE[this.props.type][0] || dimensions.height < MIN_SIZE[this.props.type][1]) {
              return reject(new Error('image_size_not_allow'));
            }
          });
        }

        resolve(data);
      }),
      new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), 60e3)),
    ])
      .then(async (res) => {
        const thumb = await res.getThumb(...this.props.thumbSize);
        if (typeof this.props.onChange === 'function') return this.props.onChange(res, thumb);
      })
      .catch((err) => {
        console.error('ERROR: ', err);

        if (err.message === 'image_size_not_allow') {
          this.props.dispatch(
            open(CUSTOM_CONTENT, {
              title: this.getString('image_requirement'),
              content: this.getString('image_requirement_size', '', [...MIN_SIZE[this.props.type]]),
            }),
          );
        }
      });
  };
}

export default initComponent(FileUploader);

/**
 * getImageFromFile -> getURL
 * getImageFromFile -> getThumb
 */
