import React from 'react';
import { getRawPhoneNumber, phoneNumber } from 'utils/format';

class CSKHSuggestMerchant extends React.Component {
  render() {
    const { phoneNumber: phone, countryCode } = this.props;
    const rawPhoneNumber = getRawPhoneNumber(phoneNumber(phone, { countryCode }));

    return (
      <div className="cskh-suggest-merchant">
        <iframe
          title="CSKH suggest merchant"
          src={`https://docs.google.com/forms/d/e/1FAIpQLScwLPcFMzjAX9htsOR7oiN5JxCh5-mbCLxWvY51dht79YcTww/viewform?usp=pp_url&entry.798303555=${rawPhoneNumber}`}
        />
        <input id="cpn" type="hidden" value={rawPhoneNumber} />
      </div>
    );
  }
}

export default CSKHSuggestMerchant;
