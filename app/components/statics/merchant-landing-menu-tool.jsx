import React from 'react';
import initComponent from 'lib/initComponent';
import Button from 'components/shared/button';
import { getString } from 'utils/i18n';

class MerchantLandingMenuTool extends React.Component {
  renderStickyBanner = () => {
    const { isMobile, username } = this.props;
    if (!isMobile) return;
    return (
      <div className="banner-sticky"
        data-ios-url={`loshipmerchant://loship.vn/${username}/menu`}
        data-android-url={`loshipmerchant://loship.vn/${username}/menu`}>
        <div className="viewer-title">
          {getString('banner_sticky_merchant_menu_tool')}
        </div>
        <a className="close">
          <i className="fas fa-times" />
        </a>
        <Button type="primary" fw className="btn-open-app">
          <span>{getString('continue_on_merchant_app')}</span>
          <i className="fas fa-chevron-right pull-right" />
        </Button>
      </div>
    );
  }
  render() {
    return (
      <div className="merchant-landing-menu-tool">
        <div className="container">
          <div className="header">
            <div className="logo">
              <a href="#">
                <img src="/dist/images/logo-loship-white@2x.png" />
              </a>
            </div>
          </div>
          <div className="content">
            <div className="info">
              <div className="title">
                Dành riêng cho <br />
                Chủ cửa hàng
              </div>
              <div className="describe">
                Loship - Chủ cửa hàng là ứng dụng giúp cho anh chị quản lý cửa hàng của mình tiện lợi hơn.
              </div>
              <div className="action">
                <a href="http://bit.ly/iOSLoshipChuCuaHang"><img className="img-action ios" src="/dist/images/btn-ios@2x.png" /></a>
                <a href="http://bit.ly/AndroidLoshipChuCuaHang"><img className="img-action android" src="/dist/images/btn-android@2x.png" />
                </a>
              </div>
            </div>
          </div>
          <div className="phone-image">
            <img src="/dist/images/top-phone.png" />
          </div>
        </div>
        {this.renderStickyBanner()}
      </div>
    );
  }
}

export default initComponent(MerchantLandingMenuTool);
