import React from 'react';
import { getString } from 'utils/i18n';

export default class Base extends React.Component {
  callActionRef = (refId, fnName, ...params) => {
    const ref = this.refs[refId];
    if (!ref) return;
    const instance = typeof ref.getWrappedInstance === 'function' && ref.getWrappedInstance();

    if (!instance) return;
    const fn = instance[fnName];
    return typeof fn === 'function' && fn.call(this, ...params);
  };

  getString = (field, context, values) => {
    return getString(field, context, values);
  };
}
