import Button from 'components/shared/button';
import React from 'react';
import { getString } from 'utils/i18n';

class CashbagApp extends React.Component {
  render() {
    return (
      <div className="cashbag">
        <div className="cashbag-header">
          <img
            className="ic-cashbag"
            src="/dist/images/ic/favicon-16x16.png"
            srcSet="/dist/images/ic/favicon-16x16.png 1x,/dist/images/ic/favicon-32x32.png 2x"
            alt="Icon cashbag"
          />
        </div>
        <div className="cashbag-content">
          <Button type="primary" className="cashbash-btn app open">
            {getString('open_app')}
          </Button>
          <a className="app download" href="https://loship.vn/use-app">
            {getString('download_app')}
          </a>
        </div>
      </div>
    );
  }
}

export default CashbagApp;
