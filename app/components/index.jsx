import crossStorage from '@colorye/cross-storage';
import * as sentry from '@sentry/react';
import { fetchCategories, fetchCoreCategories } from 'actions/category';
import { fetchGlobalConfigs } from 'actions/order';
import { CUSTOM_CONTENT, open } from 'actions/popup';
import { fetchShippingAddresses } from 'actions/user';
import cx from 'classnames';
import BasePage from 'components/pages/base';
import { globalAddressRequireHandler } from 'components/pages/search/_globalAddress';
import Page4xx5xx from 'components/shared/4xx-5xx-page';
import AlertContainer from 'components/shared/alert/container';
import Footer from 'components/shared/footer';
import Navbar from 'components/shared/navbar';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { ToastContainer, Zoom } from 'react-toastify';
import { getPartnerConfig } from 'reducers/metadata';
import { fetchGlobalAddress } from 'reducers/new/map';
import { getGlobalAddress, updateGlobalAddress } from 'reducers/new/metadata';
import { getConfigs } from 'reducers/order';
import { getCurrentUser, getShippingAddresses } from 'reducers/user';
import { ALLOWED_DOMAINS, APP_INFO } from 'settings/variables';
import { lazyComp } from 'utils/async';
import { getCityByIdOrNameOrSlug } from 'utils/data';
import { getDistanceBirdFlies } from 'utils/distance';
import { detectMobileDevice, iife } from 'utils/helper';
import cities from 'utils/json/cities';
import * as cookies from 'utils/shims/cookie';
import AppHandler from './handler';

const PopupContainer = lazyComp(() => import('components/shared/popup/container'));

const toastifyOptions = {
  className: 'toast-container',
  position: 'bottom-center',
  autoClose: 2000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnVisibilityChange: true,
  pauseOnHover: true,
  newestOnTop: true,
  transition: Zoom,
};

class App extends BasePage {
  static needs = [fetchShippingAddresses]

  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();

      const partnerConfig = getPartnerConfig(state);
      if (partnerConfig) data = data.set('partnerConfig', partnerConfig.get('data'));

      const config = getConfigs(state);
      if (config) data = data.set('config', config.get('data'));

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      const shippingAddresses = getShippingAddresses(state);
      if (shippingAddresses) data = data.set('shippingAddresses', shippingAddresses.toJS().data);

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      return data;
    },
    customRender: true,
  };

  static childContextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
    geoLocation: PropTypes.object,
    getGeoLocation: PropTypes.func.isRequired,
    setGlobalAddressAuto: PropTypes.func.isRequired,
    setGlobalAddress: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.partnerName = (props.partnerConfig && props.partnerConfig.get('partner')) || 'none';
    this.isMobile = detectMobileDevice() === 'mobile';
  }

  getChildContext() {
    return {
      isMobile: this.isMobile,
      partnerName: this.partnerName,
      geoLocation: this.state.geoLocation,
      getGeoLocation: this.getGeoLocation,
      setGlobalAddressAuto: this.setGlobalAddressAuto,
      setGlobalAddress: this.setGlobalAddress,
    };
  }

  componentDidMount() {
    this.handleCoreData();

    if (process.env.BROWSER) {
      crossStorage.open(ALLOWED_DOMAINS);
    }
  }

  componentDidUpdate(prevProps) {
    const { globalAddress } = this.props;
    const { globalAddress: prevGlobalAddress } = prevProps;
    if (globalAddress && (globalAddress.lat !== prevGlobalAddress?.lat || globalAddress.lng !== prevGlobalAddress?.lng)) {
      this.handleCoreData();
    }
  }

  componentWillUnmount = () => {
    if (process.env.BROWSER) {
      crossStorage.close();
    }
  };

  handleCoreData = () => {
    this.props.dispatch(fetchGlobalConfigs());
    this.props.dispatch(fetchCoreCategories());

    if (this.props.globalAddress) {
      this.props.dispatch(fetchCategories());
    }
  }

  getGeoLocationStatus = async () => {
    try {
      const status = navigator.permissions.query({ name: 'geolocation' });
      return status;
    } catch (e) {
      return { state: 'prompt' };
    }
  }

  getGeoLocation = async ({ force, callback, cityId, districtId, useDefault } = {}) => {
    // No request permission when on partner webview or desktop
    if (this.partnerName !== 'none' || !this.isMobile || useDefault) {
      // 1. prefer geoLocationFromCookies (usually on partner webview).
      const geoLocationFromCookies = iife(() => {
        try { return JSON.parse(cookies.get('geoLocation')); }
        catch { return; }
      });
      if (geoLocationFromCookies?.latitude && geoLocationFromCookies?.longitude) {
        return getGeoLocationCallback.call(this, geoLocationFromCookies, () => { cookies.expire('geoLocation'); });
      }

      // 2. use previousGlobalAddress, which some extra config (always fallback for desktop as geolocation is not very accuracy)
      if (!this.isMobile && !cityId) {
        const savedGlobalAddresses = iife(() => {
          try { return JSON.parse(localStorage.getItem('savedGlobalAddresses')); }
          catch { return; }
        });

        const city = getCityByIdOrNameOrSlug(savedGlobalAddresses?.[0].cityId);
        if (city.type === 'city') {
          cityId = city.city.id;
        } else {
          cityId = city.district.city.id;
          districtId = city.district.id;
        }
      }
      let key = 'previousGlobalAddress';
      if (cityId) key += `.c_${cityId}`;
      if (parseInt(districtId)) key += `.d_${parseInt(districtId)}`;

      const res = iife(() => {
        try { return JSON.parse(localStorage.getItem(key)); }
        catch { return; }
      });
      if (res?.lat && res?.lng) {
        return getGeoLocationCallback.call(this, { latitude: res.lat, longitude: res.lng });
      }

      if (!this.isMobile) {
        const city = cities.find((c) => c.id === cityId) || cities.find((c) => c.id === 50);
        return getGeoLocationCallback.call(this, { latitude: city.lat, longitude: city.lng });
      }
      return;
    }

    const permissionStatus = await this.getGeoLocationStatus();

    const currentLocation = await iife(async () => {
      if (permissionStatus.state === 'granted' || (force && permissionStatus.state !== 'denied')) {
        return new Promise((resolve) => {
          navigator.geolocation.getCurrentPosition(
            (location) => getGeoLocationCallback.call(this, location.coords, resolve),
            null,
            { enableHighAccuracy: true },
          );
        });
      }
    });
    if (currentLocation) {
      return getGeoLocationCallback.call(this, currentLocation);
    }

    if (force && permissionStatus.state === 'denied') {
      this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('allow_location_title'),
          content: this.getString('turn_on_location_service', '', [
            this.getString('allow_location'),
            <a
              key="view_guide"
              className="ml-4"
              href="https://support.google.com/chrome/answer/142065?co=GENIE.Platform%3DDesktop&hl=en"
              target="_blank"
              rel="noopener"
            >
              {this.getString('view_guide')}
            </a>,
          ]),
          wrapperClassName: 'layer-toaa',
          contentClassName: 'mw-400',
        }),
      );
    }

    function getGeoLocationCallback(geoLocation, resolve) {
      this.setState({ geoLocation }, () => {
        typeof resolve === 'function' && resolve(geoLocation);
        typeof callback === 'function' && callback(geoLocation);
      });
      return geoLocation;
    }
  };

  setGlobalAddressAuto = async ({ cityId, districtId, useDefault } = {}) => {
    let geoLocation = await Promise.race([
      this.getGeoLocation({ cityId, districtId, useDefault }),
      new Promise((resolve) => setTimeout(resolve, 5000)),
    ]);

    let handleClosePopup;

    if ((!geoLocation || this.fetchGlobalAddressFailed)) {
      setTimeout(() => {
        window.scrollTo(0, 0);
      }, 1000);
      geoLocation = await globalAddressRequireHandler.call(this, {
        geoLocationStatus: await this.getGeoLocationStatus(),
        saveClosedHandler: (handleClose) => {
          handleClosePopup = handleClose;
        },
      });
    }

    const savedGlobalAddresses = iife(() => {
      try {
        const res = JSON.parse(localStorage.getItem('savedGlobalAddresses'));
        if (Array.isArray(res)) return res;
        return [];
      } catch (e) {
        return [];
      }
    });

    const previousGlobalAddress = (this.props.shippingAddresses || [])
      .concat(savedGlobalAddresses)
      .find((address) => {
        if (!address.lat || !address.lng || !address.address) return false;
        if (getDistanceBirdFlies(address.lat, address.lng, geoLocation.latitude, geoLocation.longitude) > 0.1) return false;
        return true;
      });
    if (previousGlobalAddress) {
      return this.setGlobalAddress(previousGlobalAddress, handleClosePopup);
    }

    this.props.dispatch(fetchGlobalAddress({
      latlng: `${geoLocation.latitude},${geoLocation.longitude}`,
      callback: ({ error, data: results }) => {
        if (error) {
          this.fetchGlobalAddressFailed = true;
          return this.setGlobalAddressAuto();
        }

        const addresses = results || [];
        if (addresses[0]?.geometry) {
          const placeName = addresses[0].formattedAddress?.split(',')[0];
          const address = {
            placeName,
            address: addresses[0].formattedAddress,
            lat: addresses[0].geometry.location.lat,
            lng: addresses[0].geometry.location.lng,
            cityId: addresses[0].cityId,
            districtId: addresses[0].districtId,
            suggestedAddress: '',
            suggestedDistrictId: '0',
          };

          this.setGlobalAddress(address, handleClosePopup);
        }
      },
    }));
  }

  setGlobalAddress = (globalAddress, handleClosePopup) => {
    this.props.dispatch(updateGlobalAddress({ data: globalAddress }));
    typeof handleClosePopup === 'function' && handleClosePopup();

    let key = 'previousGlobalAddress';
    localStorage.setItem(key, JSON.stringify(globalAddress));

    const city = getCityByIdOrNameOrSlug(globalAddress.cityId);
    if (city.type === 'city') {
      key += `.c_${city.city.id}`;
    } else {
      if (globalAddress.cityId) key += `.c_${city.district.city.id}`;
      if (parseInt(globalAddress.districtId)) key += `.d_${parseInt(city.district.id)}`;
    }
    if (key !== 'previousGlobalAddress')localStorage.setItem(key, JSON.stringify(globalAddress));
  }

  renderErrorComponent = () => {
    return (
      <div id="screen" className={cx('screen', this.isMobile ? 'mobile' : 'desktop', APP_INFO.serviceName)}>
        <Navbar getString={this.getString} />
        <Page4xx5xx />
        <Footer location={this.props.location} />
      </div>
    );
  }

  render() {
    return (
      <sentry.ErrorBoundary fallback={this.renderErrorComponent()}>
        <div id="screen" className={cx('screen', this.isMobile ? 'mobile' : 'desktop', APP_INFO.serviceName)}>
          <Navbar getString={this.getString} />
          {this.props.children}
          <Footer location={this.props.location} />

          <PopupContainer />
          <AlertContainer />
          <ToastContainer {...toastifyOptions} />

          <AppHandler />
        </div>
      </sentry.ErrorBoundary>
    );
  }
}

export default initComponent(App);
