import React from 'react';
import cx from 'classnames';
import { getString } from 'utils/i18n';

class ExpandContainer extends React.Component {
  static defaultProps = {
    line: 2,
    mode: 'DEFAULT', // ARROW | VIEW_MORE | FIXED | DEFAULT
    content: '',
    padding: 16,
  };

  state = { isExpanded: false };

  componentDidMount() {
    this.computeShouldShowViewMore();
  }

  render() {
    const { title, className, line, mode, padding } = this.props;
    const { isExpanded, canViewMore } = this.state;
    return (
      <div
        ref={this.getRef}
        className={cx('expand-container', className, { [`lines-${line}`]: !isExpanded && canViewMore })}
        style={{ paddingTop: padding, paddingBottom: Math.max(padding + 4, 20), ...this.props.style }}
        onClick={!['VIEW_MORE', 'FIXED'].includes(mode) && !isExpanded ? this.toggleExpanded : undefined}
      >
        {title && (typeof title === 'function'
          ? title()
          : <div className="title">{title}</div>
        )}
        {typeof this.props.content === 'function' ? this.props.content() : this.props.content}
        {mode === 'VIEW_MORE' && canViewMore && (
          typeof this.props.arrow === 'function' ? (
            this.props.arrow(isExpanded, this.toggleExpanded)
          ) : (
            <a className="view-more" onClick={this.toggleExpanded}>
              {getString(isExpanded ? 'view_less' : 'view_more')}
            </a>
          )
        )}
        {mode === 'ARROW' && canViewMore && line !== 0 &&
          (typeof this.props.arrow === 'function' ? (
            this.props.arrow(isExpanded, this.toggleExpanded)
          ) : (
            <i className={cx('fas btn-expand', isExpanded ? 'fa-caret-up' : 'fa-caret-down')} onClick={this.toggleExpanded} />
          ))}
      </div>
    );
  }

  getRef = (ref) => {
    if (ref) this.ref = ref;
  };

  toggleExpanded = (e) => {
    e?.stopPropagation();
    if (!this.state.canViewMore) return;
    this.setState((state) => ({ isExpanded: !state.isExpanded }));
  };

  computeShouldShowViewMore = () => {
    if (!this.ref) return false;
    const offsetWidth = this.ref.offsetWidth;
    const { content, line } = this.props;

    if (typeof content.split !== 'function') return this.setState({ canViewMore: false });
    /** Heuristic way to determine should show view more or not.
     * 7.2 -> average character width (in px)
     */
    const totalLines = content.split('\n').reduce((res, l) => {
      return res + Math.ceil(l.length / (offsetWidth / 7.2));
    }, 0);
    this.setState({ canViewMore: totalLines > line });
  };
}
export default ExpandContainer;
