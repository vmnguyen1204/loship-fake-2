import { fetchUserOrderDetail } from 'actions/order';
import BaseComponent from 'components/base';
import PropTypes from 'prop-types';
import React from 'react';
import Desktop from './detail';
import Mobile from './detail.mobile';

if (process.env.BROWSER) {
  require('assets/styles/pages/order.scss');
  require('assets/styles/pages/order-mobile.scss');
  require('assets/styles/pages/order-history.scss');
  require('assets/styles/pages/order-history-mobile.scss');
}

export default class OrderDetailPage extends BaseComponent {
  static contextTypes = { isMobile: PropTypes.bool };

  static needs = [fetchUserOrderDetail];

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}
