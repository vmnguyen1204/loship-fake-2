import Button from 'components/shared/button';
import React from 'react';
import cx from 'classnames';
import { getProcessStep } from 'utils/cart';
import initComponent from 'lib/initComponent';
import OrderDetailBase from './detail-base';
import MerchantView from './merchant';

class OrderDetailComponent extends OrderDetailBase {
  renderOrderActions = () => {
    const { data } = this.props;
    const { lineGroups = [] } = data.toObject();
    const params = this.getParams();
    const serviceName = data && data.get('serviceName');
    const status = data.get('status');
    const canReorder = status === 'done' || status === 'cancel';
    const totalUserGroup = lineGroups.length;
    const isShowingReOrderBtn = params.serviceName !== 'lozi' && canReorder && this.getPrivileges() === 'EDIT';
    const isShowingOrderPriceBtn = totalUserGroup > 1 && status !== 'cancel' && status !== 'return';

    if (!isShowingReOrderBtn && !isShowingOrderPriceBtn) return null;

    return (
      <div className="order-actions">
        {isShowingOrderPriceBtn && (
          <Button className="btn-order-price-detail" onClick={this.openOrderPriceDetailGroup}>
            {this.getString('order_price_detail')}
          </Button>
        )}
        {isShowingReOrderBtn && (
          <Button
            type="feature"
            className={cx('btn-sm', { disabled: serviceName !== 'loship' && serviceName !== 'lomart' })}
            onClick={this.reOrder}
          >
            <i className="fas fa-redo" />
            <span>{this.getString('re_order')}</span>
          </Button>
        )}
      </div>
    );
  };

  render() {
    const { data, merchantData, status: loadingStatus } = this.props;

    if (merchantData) return <MerchantView data={merchantData} />;

    if (!data && !loadingStatus) return null;
    if (loadingStatus === 'PENDING') return this.renderLoadingPage();
    if (loadingStatus !== 'SUCCESS') return this.renderErrorPage();

    const status = data.get('status');
    const processStep = getProcessStep(status);

    return (
      <div className="order-page">
        <div className="order-step-detail">
          {data.get('isCancellable') && (
            <a className="pull-right p8_8 m8_8" onClick={this.cancelOrder}>
              {this.getString('cancel_order', 'order')}
            </a>
          )}
          {this.renderShippingProcessComplete(processStep)}
          {this.renderShippingProcess(processStep)}
          {this.renderShippingInfo()}
          {this.renderPaymentUnsuccessful()}

          {this.renderShipperInfo(data.get('shipper'), processStep)}
          {this.renderOrderInfo()}

          {this.renderRefundInfo()}
          {this.renderContactInfo()}
        </div>
      </div>
    );
  }
}

export default initComponent(OrderDetailComponent);
