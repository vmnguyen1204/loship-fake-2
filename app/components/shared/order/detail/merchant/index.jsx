import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Desktop from './detail';
import Mobile from './detail.mobile';

if (process.env.BROWSER) {
  require('assets/styles/pages/order.scss');
  require('assets/styles/pages/order-mobile.scss');
  require('assets/styles/pages/order-history.scss');
  require('assets/styles/pages/order-history-mobile.scss');
}

export default class OrderDetailPage extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}
