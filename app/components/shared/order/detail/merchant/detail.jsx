import React from 'react';
import { getProcessStep } from 'utils/cart';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';
import OrderDetailBase from './detail-base';

class OrderDetailComponent extends OrderDetailBase {
  render() {
    const { data } = this.props;
    if (!data) return null;

    const processStep = getProcessStep(data.get('status'));
    const { title: cancelNoteTitle, message: cancelNoteMessage } = this.getCancelNote();

    return (
      <div className="order-page">
        <div className="order-step-detail merchant">
          <div className="page-content">
            <span className="label">{this.getString('order_code')}</span>
            &nbsp;
            <b>#{data.get('code')}</b>
            {this.renderOrderInfo()}
            {processStep >= 2 && this.renderShipperInfo(data.get('shipper'))}
            {processStep === -1 && (
              <div className="cancel-note">
                <h4>{cancelNoteTitle}</h4>
                <p>{cancelNoteMessage}</p>
              </div>
            )}
            {this.renderNote(data.get('note'))}
            {this.renderOrderDetail()}
            {this.renderSummary()}
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(OrderDetailComponent, withRouter);
