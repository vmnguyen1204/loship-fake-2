import MobileNavbar from 'components/shared/mobile-nav';
import React from 'react';
import { getProcessStep } from 'utils/cart';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';
import OrderDetailBase from './detail-base';

class OrderDetailComponent extends OrderDetailBase {
  render() {
    const { data } = this.props;
    if (!data) return null;

    const processStep = getProcessStep(data.get('status'));
    const { title: cancelNoteTitle, message: cancelNoteMessage } = this.getCancelNote();
    const hideNavbar = location.pathname.includes('/m/tai-khoan/don-hang');

    return (
      <div>
        <div className="order-page">
          <div className="order-step-detail merchant">
            {!hideNavbar && (
              <MobileNavbar
                hasBackButton
                title={this.getString('code_0', '', [data.get('code')])}
                backHandler={() => this.props.history.push('/tai-khoan/don-hang')}
                actionText={this.getString('cancel_order', 'order')}
                action={data.get('isCancellable') && this.cancelOrder}
              />
            )}
            <div className="scroll-container">
              {this.renderOrderInfo()}
              {processStep >= 2 && this.renderShipperInfo(data.get('shipper'))}
              {processStep === -1 && (
                <div className="cancel-note">
                  <h4>{cancelNoteTitle}</h4>
                  <p>{cancelNoteMessage}</p>
                </div>
              )}
              {this.renderNote(data.get('note'))}
              {this.renderOrderDetail()}
              {this.renderSummary()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(OrderDetailComponent, withRouter);
