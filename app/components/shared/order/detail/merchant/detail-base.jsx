import * as SEOTYPE from 'settings/seo-types';

import { PHOTO_POPUP, open } from 'actions/popup';
import { addCurrency, phoneNumber } from 'utils/format';

import BasePage from 'components/pages/base';
import Footer from 'components/shared/footer';
import { Map } from 'immutable';
import React from 'react';
import Spinner from 'components/statics/spinner';
import cx from 'classnames';
import { getCurrentUser } from 'reducers/user';
import { getMerchantProcessStep } from 'utils/cart';
import moment from 'utils/shims/moment';

class OrderDetailBase extends BasePage {
  constructor(props, context) {
    super(props, {
      ...context,
      seo: {
        type: SEOTYPE.ORDER_DETAIL,
        order: props.data,
      },
    });
    this.state = {};
  }

  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const currentUser = getCurrentUser(state, params);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
    customRender: true,
  };

  renderLoadingPage = () => {
    return (
      <div className="order-page">
        <div className="order-step-detail loading">
          <Spinner type="wave">
            <span className="ml-8">{this.getString('loading')}</span>
          </Spinner>
        </div>
      </div>
    );
  };

  renderErrorPage = (isMobile) => {
    return (
      <div>
        <div className="step-unauthorized">
          <div className="page-merchant page-merchant-manager">
            <div className="alone-message">
              <h3>{this.getString('buy_on_loship', 'order')}</h3>
              [MERCHANT VIEW]
              <p>{this.getString('sorry_you_are_not_authorized_to_access_this_page')}</p>
              <p>
                {this.getString('if_you_think_this_is_a_bug_please_contact_us_via')}
                <a href="mailto:hotroloship@lozi.vn" title={this.getString('Contact us')}>
                  hotroloship@lozi.vn
                </a>
                {this.getString('for_assistance')}
              </p>
              <p>{this.getString('thank_you_for_choosing_lozi')}</p>
            </div>
          </div>
        </div>

        {isMobile && <Footer location={this.props.location} />}
      </div>
    );
  };

  renderOrderInfo = () => {
    const { data } = this.props;

    const code = data.get('code');
    const processStatus = getMerchantProcessStep(data.get('status'), data.get('isBillValuePaid'));

    return (
      <div className="order-info-header">
        <div className="order-info-header-item">
          <p>{this.getString('order_code')}</p>
          <div className="order-block spacing">#{code}</div>
        </div>
        <div className="order-info-header-item">
          <p>{this.getString('status')}:</p>
          <div className={cx('order-block', processStatus)}>{this.getString(processStatus, 'order_merchant')}</div>
        </div>
      </div>
    );
  };

  renderShipperInfo = (shipper) => {
    if (!shipper) return null;

    const { data } = this.props;
    const processStatus = getMerchantProcessStep(data.get('status'), data.get('isBillValuePaid'));

    return (
      <div className="section shipper">
        <div className="title">
          <span className="light">{this.getString('your_order_is_being_shipped_by', 'order')}</span>
        </div>
        <div className="content">
          <div className="shipper-item">
            <div className="avatar" onClick={this.openShipperPhotoPopup}>
              <img src={shipper.avatar || '/dist/images/avatar.png'} alt="Avatar" />
            </div>
            <div className="info">
              <div>
                <b>{shipper.name}</b>
                <br />
                {phoneNumber(shipper.phone)}
              </div>
            </div>

            <a href={`tel:${shipper.phone}`} className="btn btn-call-shipper">
              {this.getString('call_shipper', 'order')}
            </a>
          </div>
        </div>
        {processStatus !== 'done' && (
          <div className="content p16_16">
            <b>
              {this.getString('prepare_order_before', 'order_merchant')}{' '}
              <span className="highlight">{moment(data.get('willBeDeliveredAt')).format('HH:mm')}</span>
            </b>
          </div>
        )}
      </div>
    );
  };

  renderNote = (note) => {
    if (!note) return null;
    return (
      <div className="section note">
        <div className="content">
          <b>{this.getString('note')}</b> {note}
        </div>
      </div>
    );
  };

  renderOrderDetail = () => {
    const { data } = this.props;

    const lines = data.get('lines');
    const lineCustom = (line) => {
      if (!Array.isArray(line.customs)) return null;

      return line.customs.flatMap((custom) => {
        if (!Array.isArray(custom.customOptions)) return [];
        return (
          custom.customOptions.map((customOption) => (
            <p key={customOption.id}>
              {customOption.value}
              {customOption.quantity > 1 ? ' x' + customOption.quantity : ''}
            </p>
          )) || []
        );
      });
    };

    return (
      <div className="section order-detail">
        <div className="content">
          <div className="order-detail-item txt-gray">
            <div className="main">{this.getString('order_detail', 'order')}:</div>
            <div className="sub">{this.getString('quantity', 'order')}:</div>
          </div>
          {lines.map((line, index) => (
            <div className="order-detail-item" key={line.id}>
              <div className="main">
                <b>
                  {index}. {line.name}
                </b>
                {lineCustom(line)}
              </div>
              <div className="sub">x{line.quantity}</div>
            </div>
          ))}
        </div>
      </div>
    );
  };

  renderSummary = () => {
    const { data } = this.props;

    const eatery = data.get('eatery');
    const commissionFee = (eatery && (eatery.commissionValue / 100) * data.get('total')) || 0;
    const promotion = data.get('promotion') || data.get('userPromotion');
    const discountValue = data.get('populateDiscount') || data.get('discount');

    return (
      <div className="merchant-content bg-info">
        <p>
          {this.getString('6vDaz_GcUI', 'order_merchant')}:{' '}
          <b className="highlight pull-right">{addCurrency(data.get('total'))}</b>
        </p>
        {commissionFee > 0 && (
          <p>
            {eatery.commissionValue}% {this.getString('commission', 'order_merchant')}:{' '}
            <b className="highlight pull-right">{addCurrency(-commissionFee)}</b>
          </p>
        )}
        {promotion && (
          <p>
            {this.getString('promotion', 'order_merchant')}:{' '}
            <b className="highlight pull-right">{addCurrency(-discountValue)}</b>
            <br />
            <small>
              <b className="highlight">{promotion.code}</b>
            </small>
          </p>
        )}
        <div className="separator" />

        <p className="total-fee">
          {this.getString('shipper_price', 'order_merchant')}:
          <b className="highlight pull-right">{addCurrency(data.get('totalMerchantFee'))}</b>
        </p>
      </div>
    );
  };

  getCancelNote = () => {
    const { data } = this.props;
    const message = data.get('cancelNote').split('|');
    return {
      title: (message.length > 1 && message[0]) || '',
      message: message[1] || message[0],
    };
  };

  openShipperPhotoPopup = () => {
    const { data } = this.props;

    this.props.dispatch(
      open(PHOTO_POPUP, {
        photo: (data.get('shipper') && data.get('shipper').avatar) ||
        '/dist/images/avatar.png',
      }),
    );
  };
}

export default OrderDetailBase;
