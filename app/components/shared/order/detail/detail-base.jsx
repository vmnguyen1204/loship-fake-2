import { generateShortLinkSharing } from 'actions/buy';
import { sendGAEvent } from 'actions/factory';
import { injectParams } from 'actions/helpers';
import { fetchMerchant } from 'actions/merchant';
import { fetchMerchantOrderDetail, fetchOrderSharing, fetchOrderTrackingMap, fetchUserOrderDetail, reportShipperFraudCancel } from 'actions/order';
import { CUSTOM_CONTENT, GENERIC_POPUP, GMAP_POPUP, MERCHANT_RATING_POPUP, ORDER_FAILED, PHOTO_POPUP, RATING_POPUP, SHARING_POPUP, close, open } from 'actions/popup';
import cx from 'classnames';
import BasePage from 'components/pages/base';
import Button from 'components/shared/button';
import ExpandContainer from 'components/shared/expand-container';
import Footer from 'components/shared/footer';
import PaymentSelector from 'components/shared/payment';
import Hint from 'components/statics/hint';
import ImageLazy from 'components/statics/img-lazy';
import Spinner from 'components/statics/spinner';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import Clipboard from 'react-clipboard.js';
import { toast } from 'react-toastify';
import { getMerchantOrder, getMoMoOrder, getPartnerOrder, getUserOrder } from 'reducers/order';
import { getCurrentUser } from 'reducers/user';
import * as SEOTYPE from 'settings/seo-types';
import { LINK_DOMAIN, LINK_LZI } from 'settings/variables';
import { calculateDishPrice, getDeliveryTime, getOrderLine, getOrderLineData } from 'utils/cart';
import { parseErrorV2 } from 'utils/error';
import { addCurrency, phoneNumber } from 'utils/format';
import { iife } from 'utils/helper';
import loziServices, { servicesAvailable } from 'utils/json/loziServices';
import { isMerchantReady } from 'utils/merchant';
import Link from 'utils/shims/link';
import moment from 'utils/shims/moment';

const NEW_SHIPPER_THRESHOLD = 7;
const MILISECOND_IN_A_DAY = 24 * 60 * 60 * 1000;
const ratingsMap = { good: 'recommended', neutral: 'neutral', bad: 'not_recommended' };
const SHIPPING_PROCESS_TITLE = {
  0: 'this_is_pre_ordered',
  1: 'loship_is_finding_shipper',
  2: 'shipper_is_deliverying',
  3: 'delivered',
};

const isNewShipper = (shipper) => {
  if (!shipper) return false;

  const joinedDate = moment(shipper.createdAt).startOf('day');
  const currentDate = moment().endOf('day');
  return currentDate.diff(joinedDate, 'days') <= NEW_SHIPPER_THRESHOLD;
};

class SharingPopup extends React.Component {
  state = { copied: null };

  render() {
    const { getString, ownedBy = { name: {} }, recipient = {}, isRecipientChanged = true, shortLink } = this.props;

    let sharingText = getString('share_content_2', '', [ownedBy.name.full, shortLink]);
    if (isRecipientChanged && ownedBy.name.full !== recipient.name) sharingText = getString('share_content_1', '', [recipient.name, ownedBy.name.full, shortLink]);

    return (
      <div className="sharing-content">
        <textarea
          ref={(ref) => { this.inputRef = ref; }}
          rows={3}
          className="sharing-content-data"
          value={sharingText}
          readOnly
        />
        <div className="actions">
          <Clipboard className="action" component="a" data-clipboard-text={sharingText} onSuccess={this.handleCopyCode}>
            <i className="far fa-clone" />
            {this.state.copied ? getString('copy_success', 'order') : getString('copy', 'order')}
          </Clipboard>
          <a className="action" onClick={this.handleShare}>
            <i className="fas fa-share-alt" />
            {getString('share')}
          </a>
        </div>
      </div>
    );
  }

  handleCopyCode = () => {
    this.setState({ copied: true });
    this.inputRef && this.inputRef.select();

    if (this.copyCodeTimeout) clearTimeout(this.copyCodeTimeout);
    this.copyCodeTimeout = setTimeout(() => {
      this.setState({ copied: false });
    }, 3000);
  };

  handleShare = () => {
    const { shortLink } = this.props;

    this.props.dispatch(
      open(SHARING_POPUP, {
        url: shortLink,
        subject: this.props.getString('buy_on_loship', 'order'),
        body: this.props.getString('click_on_the_link_to_order_on_loship_with_me_0', 'order'),
      }),
    );
  };
}

class OrderDetailBase extends BasePage {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      if (params.partnerName === 'momo') {
        const order = getMoMoOrder(state, params);
        data = data.merge(order);
      } else if (params.partnerName) {
        const order = getPartnerOrder(state, params);
        data = data.merge(order);
      } else {
        const order = getUserOrder(state, params);
        data = data.merge(order);
      }

      const merchantOrder = getMerchantOrder(state, params);
      if (merchantOrder) data = data.set('merchantData', merchantOrder.get('data'));

      const currentUser = getCurrentUser(state, params);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
    customRender: true,
    getMetadata: (props, seoOptions) => {
      const seo = { ...seoOptions };
      seo.order = props.data;

      return seo;
    },
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  constructor(props, context) {
    super(props, {
      ...context,
      seo: {
        type: SEOTYPE.ORDER_DETAIL,
        order: props.data,
      },
    });
    this.state = {
      // overview | detail | map
      step: 'overview',
    };
  }

  componentDidMount() {
    const query = this.getQuery();

    if (!this.props.data) {
      this.props.dispatch(fetchUserOrderDetail(this.getParams(), query));
    }

    const { pathname } = this.props.location;
    if (!this.props.merchantData && pathname.includes('/m/tai-khoan/don-hang')) {
      this.props.dispatch(fetchMerchantOrderDetail(this.getParams(), query));
    }
  }

  componentDidUpdate(prevProps) {
    const prevParams = this.getParams(prevProps);
    const params = this.getParams();
    const prevQuery = this.getQuery(prevProps);
    const query = this.getQuery();

    if (params.order !== prevParams.order) {
      this.props.dispatch(fetchUserOrderDetail(params, query));
    }

    // Close popup when press native back button
    if (!query.rating && prevQuery.rating) {
      this.props.dispatch(close(RATING_POPUP));
      this.props.dispatch(close(MERCHANT_RATING_POPUP));
    }

    if (!query.map && prevQuery.map) {
      this.props.dispatch(close(GMAP_POPUP));
      this.setState({ step: 'overview' });
    }
  }

  renderLoadingPage = () => {
    return (
      <div className="order-page">
        <div className="order-step-detail loading">
          <Spinner type="wave">
            <span className="ml-8">{this.getString('loading')}</span>
          </Spinner>
        </div>
      </div>
    );
  };

  renderErrorPage = () => {
    const { isMobile } = this.context;

    return (
      <div>
        <div className="order-page">
          <div className="order-step-detail error">
            <h3>{this.getString('buy_on_loship', 'order')}</h3>
            <p>{this.getString('sorry_you_are_not_authorized_to_access_this_page')}</p>
            <p>
              {this.getString('if_you_think_this_is_a_bug_please_contact_us_via')}
              <a href="mailto:hotroloship@lozi.vn" title={this.getString('Contact us')}>
                hotroloship@lozi.vn
              </a>
              {this.getString('for_assistance')}
            </p>
            <p>{this.getString('thank_you_for_choosing_loship')}</p>
          </div>
        </div>

        {isMobile && <Footer location={this.props.location} />}
      </div>
    );
  };

  renderSharing = (wrapper) => {
    return (
      <div className={cx('sharing', wrapper)}>
        <img src="/dist/images/order-sharing.png" alt="Sharing" />
        <div className="sharing-content">{this.getString('sharing_order_to_relative')}</div>
        <Button className="btn-sm" onClick={this.openSharingPopup}>
          {this.getString('share')}
        </Button>
      </div>
    );
  };

  renderShippingProcess = (processStep) => {
    if (processStep === -1 || processStep === 3) return this.renderSharing('section');

    const { data } = this.props;
    const { status, willBeDeliveredAt, serviceName, isStandbying, standbyUntil } = data.toObject();

    const isPreOrder = data.get('isPreOrder') && status === 'pre-order';
    const deliveryTime = getDeliveryTime(willBeDeliveredAt, isPreOrder);

    const shippingProcessDetail = (() => {
      if (processStep === 0) {
        return `${this.getString('loship_will_process_your_order', 'order')} ${this.getString(
          deliveryTime.string,
          'order',
          [deliveryTime.time],
        )}`;
      }
      if (processStep === 2 && serviceName === 'lozat') {
        const isDone = [
          status === 'purchasing',
          status === 'purchasing' && isStandbying && standbyUntil,
          status === 'deliverying' && !isStandbying && standbyUntil,
        ];

        return (
          <div className="step-2">
            <div className={cx('content', { done: isDone[0] })}>
              <i className={cx('lz', { 'lz-check-mark': isDone[0] })} />
              {this.getString('took_clothes_from_recipient_address', 'order')}
            </div>
            <div className={cx('content', { done: isDone[1] })}>
              <i className={cx('lz', { 'lz-check-mark': isDone[1] })} />
              {this.getString('sent_clothes_to_the_laundry', 'order')}
            </div>
            <div className={cx('content', { done: isDone[2] })}>
              <i className={cx('lz', { 'lz-check-mark': isDone[2] })} />
              {this.getString('get_clean_clothes_from_the_laundry', 'order')}
            </div>
          </div>
        );
      }
      if (processStep === 2 && serviceName === 'losend') {
        const isDone = [status === 'purchasing', status === 'done'];

        return (
          <div className="step-2">
            <div className={cx('content', { done: isDone[0] })}>
              <i className={cx('lz', { 'lz-check-mark': isDone[0] })} />
              {this.getString('get_supplies_from_the_sender', 'order')}
            </div>
            <div className={cx('content', { done: isDone[1] })}>
              <i className={cx('lz', { 'lz-check-mark': isDone[1] })} />
              {this.getString('delivered_supplies_to_recipient', 'order')}
            </div>
          </div>
        );
      }
      if (processStep === 2) {
        return `${this.getString('estimate_time_for_delivered', 'order')} ${this.getString(
          deliveryTime.string,
          'order',
          [deliveryTime.time],
        )}`;
      }

      return '';
    })();

    return (
      <div className="section shipping-process">
        <div className="visualize">
          {[1, 2, 3].map((key) => (
            <div key={key} className={cx('dot', { done: key < processStep, doing: key === processStep })}>
              {key < processStep && <i className="lz lz-check-mark" />}
            </div>
          ))}
        </div>
        <div className="title">{this.getString(SHIPPING_PROCESS_TITLE[processStep], 'order')}</div>
        {shippingProcessDetail && <div className="description">{shippingProcessDetail}</div>}

        {this.renderSharing()}
      </div>
    );
  };

  renderShippingProcessComplete = (processStep) => {
    const { data } = this.props;
    const { isMobile } = this.context;
    const params = this.getParams();

    const status = data.get('status');
    const isReorderable = (status === 'done' || status === 'cancel') &&
      params.serviceName !== 'lozi' &&
      this.getPrivileges() === 'EDIT';
    const canReportShipper = data.get('canReportShipper');

    if (processStep === -1) {
      const cancelNote = this.getCancelNote();
      return [
        <ExpandContainer
          key="cancel-note"
          className="cancel-note"
          title={cancelNote.title}
          content={cancelNote.message}
          line={isMobile ? 3 : 0}
          mode="ARROW"
        />,
        canReportShipper && (
          <div key="report-shipper" className="report-shipper">
            <Button fw={isMobile} onClick={this.reportShipperFraudCancel}>
              {this.getString('report_shipper_fraud_cancel')}
            </Button>
          </div>
        ),
        isMobile && isReorderable && (
          <div key="reorder" className="section section-cta">
            <Button type="feature" fw onClick={this.reOrder}>
              {this.getString('re_order')}
            </Button>
          </div>
        ),
      ];
    }

    if (processStep === 3) {
      return [
        <div key="shipping-done" className="section shipping-process order-first">
          <div className="visualize done">
            <div className="icon">
              <i className="lz lz-check-mark" />
            </div>
          </div>
          <div className="title">{this.getString(SHIPPING_PROCESS_TITLE[processStep], 'order')}</div>
        </div>,
        isMobile && isReorderable && (
          <div key="reorder" className="section section-cta">
            <div className="text-bold mb-8">
              {this.getString('total')}
              <span className="pull-right">{addCurrency(data.get('totalUserFee'))}</span>
            </div>
            <Button type="feature" fw onClick={this.reOrder}>
              {this.getString('re_order')}
            </Button>
          </div>
        ),
      ];
    }

    return null;
  };

  renderShipperInfo = (shipper, processStep) => {
    if (!shipper || processStep < 2) return null;

    const { isMobile } = this.context;
    const { data } = this.props;
    const params = this.getParams();
    const { step } = this.state;

    const isDone = data.get('status') === 'done';
    const rating = data.get('rating');
    const canReview = !params.partner &&
      !rating &&
      Date.parse(data.get('doneAt')) + MILISECOND_IN_A_DAY > Date.now() && // Can review within a day after done
      this.getPrivileges() === 'EDIT';

    return (
      <div className={cx('section shipper-info', { 'section-cta': isMobile && step === 'map' })}>
        <span className="text-dark-gray">{this.getString('your_order_was_shipped_by', 'order')}</span>
        {!isDone && step === 'overview' && this.getPrivileges() && (
          <Button type="link" className="text-bold pull-right" onClick={this.changeStep('map')}>
            {this.getString('view_on_map', 'order')}
          </Button>
        )}
        <div className="shipper-info-content">
          <ImageLazy src={shipper.avatar} placeholder="/dist/images/avatar.png" size={40} mode="click" />
          <div className="main">
            <b>{shipper.name}</b>
            <br />
            {shipper.count && shipper.count.goodRate > 0 && (
              <div className="good-rate">
                <img src="/dist/images/good-face.png" alt="Good" /> {this.getString('recommended_0', 'order', [shipper.count.goodRate])}
              </div>
            )}
          </div>
          <div className="action">
            {!rating && (isDone
              ? canReview && (
                <Button className="btn-sm" onClick={this.openRatingPopup}>
                  {this.getString('rating', 'order')}
                </Button>
              )
              : !!shipper.phone && (
                <Button className="btn-sm" href={`tel:${shipper.phone}`} target="_blank">
                  <i className="fas fa-phone" />
                  <span>{this.getString('call')}</span>
                </Button>
              ))}
          </div>
        </div>
        {!rating && canReview && (
          <div className="rating-invite">
            {isNewShipper(shipper)
              ? this.getString(
                'this_shipper_is_a_new_comer_and_may_lack_of_experiences_should_there_be_any_inconveniences_regards_of_our_shippers_services_loship_would_like_to_offer_our_sincerest_apologize_to_you_and_hope_you_understand',
              )
              : this.getString(
                'we_would_love_to_hear_from_you_in_order_for_us_to_improve_our_service_quality_please_kindly_leave_a_feedback_about_your_experience',
              )}
          </div>
        )}

        {!!rating && (
          <div className={`rating ${ratingsMap[rating.rating]}`}>
            <div className="title">{this.getString('your_review', 'rating')}</div>
            <img src={`/dist/images/${rating.rating}-face.png`} alt={rating.rating} />
            <b>{this.getString(ratingsMap[rating.rating], 'rating')}</b>

            <a className="pull-right" onClick={this.openRatingPopup}>
              {this.getString('view_detail', 'rating')}
            </a>
          </div>
        )}
      </div>
    );
  };

  renderShippingInfo = ({ verbose = true } = {}) => {
    const { data } = this.props;
    const { isMobile } = this.context;

    const serviceName = data.get('serviceName');
    const service = loziServices.find((s) => s.name === serviceName) || loziServices.find((s) => s.name === 'loship');

    const eatery = data.get('eatery');
    const source = (data.get('routes') && data.get('routes').source) || {};
    const destination = (data.get('routes') && data.get('routes').destination) || {};

    const lines = data.get('lines') || [];
    const lineStr = lines
      .map((line) => !line.isDeleted && line.name)
      .filter(Boolean)
      .join(', ');

    const rating = data.get('merchantRating');
    const paymentStatus = data.get('paymentStatus');

    return (
      <div className="section shipping-info">
        {!verbose ? (
          <span className="text-dark-gray">{moment(data.get('willBeDeliveredAt')).format('DD/MM/YYYY, H:mm A')}</span>
        ) : (
          <div>
            <span className="text-dark-gray">
              {this.getString('order_code_0_1', '', [service.title, data.get('code')])}
            </span>
            {isMobile && (
              <Button type="link" className="text-bold pull-right" onClick={this.changeStep('detail')}>
                {this.getString('view_details')}
              </Button>
            )}
          </div>
        )}

        <div>
          {serviceName === 'lozat' && (
            <div className="shipping-info-item">
              <div className="connector dot">
                <i className="far fa-dot-circle fa-fw" />
              </div>
              <div className="font-title">{this.getString('pickup_address_for_laundry', 'order')}</div>
              <div>{destination.address}</div>
            </div>
          )}

          <div className="shipping-info-item">
            <div className="connector dot">
              <i className="far fa-dot-circle fa-fw" />
            </div>
            <div className="font-title">
              {(() => {
                if (serviceName === 'losend') return this.getString('pickup_address', 'order');
                if (serviceName === 'loxe') return this.getString('pickup_address_loxe', 'order');
                if (eatery) {
                  return (
                    <Link
                      to={eatery.username ? `/${eatery.username}` : `/b/${eatery.slug}`}
                      className="shipping-info-eatery"
                    >
                      {eatery.name}
                      <span>
                        {this.getString('open')} <i className="fas fa-chevron-right" />
                      </span>
                    </Link>
                  );
                }
              })()}
            </div>
            <div>{source.address}</div>
            {verbose && serviceName === 'losend' && (
              <div>
                {this.getString('sender')}: {source.name}
                <br />
                {this.getString('phone_number', 'shippingAddress')}: {phoneNumber(source.phoneNumber)}
              </div>
            )}
          </div>

          <div className="shipping-info-item">
            <div className="connector">
              <i className="fas fa-map-marker-alt fa-fw" />
            </div>
            <div className="font-title">
              {(() => {
                if (serviceName === 'lozat') return this.getString('shipping_address_for_laundry');
                if (serviceName === 'loxe') return this.getString('shipping_address_loxe');
                return this.getString('shipping_address');
              })()}
            </div>
            <div>{destination.address}</div>

            {verbose && serviceName !== 'loxe' && (
              <div>
                {this.getString('receiver')}: {destination.name}
                <br />
                {this.getString('phone_number', 'shippingAddress')}: {phoneNumber(destination.phoneNumber)}
              </div>
            )}
          </div>

          {serviceName === 'loxe' && (
            <div className="shipping-info-item mb-8">
              <div className="connector">
                <i className="lz lz-person lz-fw" />
              </div>
              <div>
                {this.getString('customer_information')}: {phoneNumber(destination.phoneNumber)} ({destination.name})
              </div>
            </div>
          )}
        </div>

        {iife(() => {
          if (!verbose) return null;
          if (data.get('status') !== 'done') {
            return (
              <div>
                {lineStr && (
                  <div className="shipping-info-item text-trimmed pb-8">
                    <div className="connector">
                      <i className="far fa-list-alt fa-fw" />
                    </div>
                    {lineStr}
                  </div>
                )}
                {this.getString('total')} {paymentStatus === 'paid' && `(${this.getString('paid')})`}
                <b className="pull-right">{addCurrency(data.get('totalUserFee'))}</b>
              </div>
            );
          }
          if (rating) {
            return (
              <div className={`rating ${rating.rating}`}>
                <div className="title">{this.getString('your_review', 'rating')}</div>
                <img src={`/dist/images/${rating.rating === 'recommended' ? 'good' : 'bad'}-face.png`} alt={rating.rating} />
                <b>{this.getString(rating.rating, 'rating')}</b>

                {this.getPrivileges() === 'EDIT' && (
                  <a className="pull-right" onClick={this.openRatingMerchantPopup}>
                    {this.getString('view_detail', 'rating')}
                  </a>
                )}
              </div>
            );
          }
          return (
            this.getPrivileges() === 'EDIT' && (
              <Button
                type={isMobile ? 'optional' : 'link'}
                fw={isMobile}
                className="btn-rating-merchant"
                onClick={this.openRatingMerchantPopup}
              >
                {this.getString('share_your_feeling_about_this_merchant', 'rating')}
              </Button>
            )
          );
        })}

      </div>
    );
  };

  renderOrderInfo = () => {
    const { data } = this.props;
    const { isMobile } = this.context;

    const {
      note,
      notePhoto,
      lines = [],
      lineGroups = [],
      serviceName,
      paymentStatus,
      shippingFee = 0,
      actualShippingFee = 0,
      extraFees = [],
      promotion,
      lpDiscount,
      discount,
      paymentMethod,
      paymentFee,
      paymentCard,
      paymentBank,
      totalUserFee,
      status,
    } = data.toObject();

    const payment = {
      method: paymentMethod,
      fee: paymentFee,
      card: paymentCard,
      bank: paymentBank,
    };
    const renderOrderActions = this.renderOrderActions || (() => {});
    const linesCount = lines.length;
    const totalUserGroup = lineGroups.length;
    const showBtnOrderPriceDetail = totalUserGroup > 1 && status !== 'cancel' && status !== 'return';

    return (
      <div>
        {isMobile && this.renderShippingInfo({ verbose: false })}
        {note && (
          <div className="section" style={{ marginTop: 1 }}>
            {this.getString('note')} “{note}”
            {notePhoto && (
              <div className="btn-attach-photo uploaded mt-8" onClick={this.openPhotoPopup}>
                <i className="lz lz-photo" /> {this.getString('view_photo_attached')}
              </div>
            )}
          </div>
        )}

        {renderOrderActions()}
        {linesCount > 0 && (
          <div className="section order-info">
            {!isMobile && <div className="text-bold">{this.getString('order_detail', 'order')}</div>}
            {lineGroups.map((lineGroup) => {
              const name = (() => {
                if (lineGroups.length < 1 || !lineGroup.taggedUser) return '';
                if (!lineGroup.taggedUser.name || !lineGroup.taggedUser.name.full) return lineGroup.taggedUser.username;
                return lineGroup.taggedUser.name.full;
              })();
              return [
                name && (
                  <div key={`line-user-${name}`} className="order-line order-line-user">
                    <ImageLazy src={lineGroup.taggedUser.avatar} size={32} />
                    <div className="order-line-content pl-8">
                      <b>{name}</b>
                    </div>
                    <div />
                  </div>
                ),
                ...lineGroup.lineIds
                  .flatMap((lineId) => {
                    const line = lines.find((cLine) => cLine.id === lineId);
                    return getOrderLine(line, getOrderLineData({
                      ...line, dishQuantity: line.quantity,
                    }, lineGroup.taggedUser.id));
                  })
                  .map((line) => {
                    const price = calculateDishPrice(line, line.customs, line.dishQuantity);
                    const rawPrice = calculateDishPrice(line, line.customs, line.dishQuantity, true);

                    return (
                      <div key={`line-${line.dishId}`} className="order-line">
                        <div className="quantity">
                          <span>x{line.dishQuantity}</span>
                        </div>
                        <div className="order-line-content">
                          <b>{line.name}</b>
                          <br />
                          {line.customs
                            .flatMap((custom) => custom.options || [])
                            .map((option) => (
                              <div key={`custom-${option.optionId}`} className="custom">
                                {option.value}
                              </div>
                            ))}
                          {line.note && (
                            <div className="custom mt-4">&quot;{line.note}&quot;</div>
                          )}
                        </div>
                        <div className="price">
                          <b>{addCurrency(price)}</b>
                          {rawPrice !== price && <span className="old">{addCurrency(rawPrice)}</span>}
                        </div>
                      </div>
                    );
                  }),
              ];
            })}
          </div>
        )}
        {linesCount > 5 && renderOrderActions()}

        <div className="section order-info" style={{ marginTop: linesCount > 0 && 1 }}>
          <div className="d-flex justify-between">
            {(() => {
              if (serviceName === 'loxe') return this.getString('fare');
              return <span>{this.getString('shipping_fee')}</span>;
            })()}

            {actualShippingFee > shippingFee ? (
              <span className="shipping-fee">
                <i className="lz lz-discount" />
                <b>{addCurrency(shippingFee)}</b>
                <span className="old">{addCurrency(actualShippingFee)}</span>
              </span>
            ) : (
              <span className="shipping-fee">
                <b>{addCurrency(shippingFee)}</b>
              </span>
            )}
          </div>

          {extraFees.map((extraFee) => (
            <div key={extraFee.id}>
              {this.getString(extraFee.title, 'extraFee')}{' '}
              {extraFee.description && <Hint text={this.getString(extraFee.description, 'extraFee')} />}
              <span className="pull-right">{addCurrency(extraFee.value)}</span>
            </div>
          ))}

          {!!promotion && (
            <div className="promotion">
              <i className="lz lz-check-mark" />
              <b>{promotion.code}</b>
              <span className="pull-right">-{addCurrency(discount)}</span>
            </div>
          )}
          {!!lpDiscount && (
            <div className="promotion">
              <i className="lz lz-check-mark" />
              <b>{this.getString('using_lopoint')}</b>
              <span className="pull-right">-{addCurrency(lpDiscount)}</span>
            </div>
          )}

          {isMobile && showBtnOrderPriceDetail && (
            <Button className="btn-order-price-detail" onClick={this.openOrderPriceDetailGroup}>
              {this.getString('order_price_detail')}
            </Button>
          )}
        </div>

        <div className={cx('section', { 'section-cta': isMobile })}>
          <b>{this.getString('total')}</b> {paymentStatus === 'paid' && `(${this.getString('paid')})`}
          <b className="pull-right">{addCurrency(totalUserFee)}</b>
        </div>

        <div className={cx('section payment-method', payment.method === 'cod' && 'pb-0')}>
          <div className="text-bold mb-8">{this.getString('payment_method')}</div>
          <PaymentSelector defaultValue={payment} paymentFees={[payment]} bank={payment.bank} />

          {payment.fee > 0 && (
            <div>
              {this.getString('convenience_fee')}{' '}
              <Hint text={this.getString('the_payment_method_you_selected_is_applied_convenient_fee')} />
              <div className="pull-right">{addCurrency(payment.fee)}</div>
            </div>
          )}
        </div>
      </div>
    );
  };

  renderPaymentUnsuccessful = () => {
    const { data } = this.props;
    const { paymentStatus } = data.toObject();

    if (!['paid_later_fail'].includes(paymentStatus)) return null;

    return (
      <ExpandContainer
        key="cancel-note"
        className="cancel-note text-red"
        content={this.getString('payment_charge_unsuccessful', '', [], true)}
        line={0}
      />
    );
  };

  renderRefundInfo = () => {
    const { isMobile } = this.context;
    const { data } = this.props;
    const { paymentMethod, paymentStatus } = data.toObject();

    if (!['refunded', 'refunding', 'refundfail'].includes(paymentStatus)) return null;

    const [title, content] = iife(() => {
      if (['refunded', 'refunding'].includes(paymentStatus)) {
        if (['momopay', 'momopayinapp'].includes(paymentMethod)) {
          return [
            this.getString('payment_refunding_notice'),
            this.getString('refunding_momo') +
              '\n' +
              this.getString('in_case_it_takes_longer_than_noted_please_contact_us_for_assistance'),
          ];
        }
        if (['zalopayapp'].includes(paymentMethod)) {
          return [
            this.getString('payment_refunding_notice'),
            this.getString('refunding_zalo') +
              '\n' +
              this.getString('in_case_it_takes_longer_than_noted_please_contact_us_for_assistance'),
          ];
        }
        if (['sacombankpay'].includes(paymentMethod)) {
          return [
            this.getString('payment_refunding_notice'),
            this.getString('refunding_sacombank') +
              '\n' +
              this.getString('in_case_it_takes_longer_than_noted_please_contact_us_for_assistance'),
          ];
        }

        if (paymentMethod.includes('fullpayment') || paymentMethod.includes('CC')) {
          return [
            this.getString('payment_refunding_notice'),
            this.getString('refunding_bank_account') +
              '\n' +
              this.getString('in_case_it_takes_longer_than_noted_please_contact_us_for_assistance'),
          ];
        }
        return [
          this.getString('payment_refunding_notice'),
          this.getString('refunding_payment_account') +
            '\n' +
            this.getString('in_case_it_takes_longer_than_noted_please_contact_us_for_assistance'),
        ];
      }

      if (['refundfail'].includes(paymentStatus)) {
        return [
          this.getString('not_refunded'),
          this.getString('we_are_sorry_for_this_inconvenience_please_contact_us_for_assistance'),
        ];
      }
    });

    return <ExpandContainer className="section" title={title} content={content} line={isMobile ? 2 : 0} mode="ARROW" />;
  };

  renderContactInfo = () => {
    return (
      <div className="section contact-info">
        <div className="text-bold mb-8">{this.getString('contact_loship_cs')}</div>

        <p>{this.getString('contact_loship_content')}</p>
        <p>
          {this.getString('hotline')}
          <b>
            <a href="tel:1900638058">{phoneNumber('1900638058', { injectZero: false })}</a>
          </b>
        </p>
        <p>
          {this.getString('email')}
          <b>
            <a href="mailto:hotroloship@lozi.vn">hotroloship@lozi.vn</a>
          </b>
        </p>
        <p>
          {this.getString('facebook')}
          <b>
            <a href="https://facebook.com/LoshipVN" target="_blank" rel="noopener">
              facebook.com/LoshipVN
            </a>
          </b>
        </p>
      </div>
    );
  };

  getCancelNote = () => {
    const { data } = this.props;
    const message = data.get('cancelNote').split('|');
    return {
      title: (message.length > 1 && message[0]) || '',
      message: message[1] || message[0],
    };
  };

  changeStep = (step) => () => {
    const { isMobile } = this.context;

    if (step === 'map') {
      const {
        data,
        location: { pathname },
      } = this.props;
      const query = this.getQuery();
      const { source = { lat: 0, lng: 0 }, destination = { lat: 0, lng: 0 } } = data.get('routes') || {};

      delete query.map;
      const q = qs.stringify(query) ? `?${qs.stringify(query)}` : '';
      const qMap = `?${qs.stringify({ ...query, map: true })}`;

      this.props.dispatch(
        open(GMAP_POPUP, {
          markers: [
            {
              position: { lat: source.lat, lng: source.lng },
              icon: '/dist/images/marker-merchant.png',
              hideInfo: true,
            },
            { position: { lat: destination.lat, lng: destination.lng }, hideInfo: true },
          ],
          mode: 'static',
          centerIcon: '/dist/images/shipper.png',
          popupClassName: 'shipping-process',
          handleRefresh: this.refreshTrackingMap,
          handleClose: () => {
            this.setState({ step: 'overview' });
            this.props.history.push(`${pathname}${q}`);
          },
        }),
      );

      this.props.history.push(`${pathname}${qMap}`);
    }

    isMobile && this.setState({ step });
  };

  refreshTrackingMap = (callback) => {
    const { data } = this.props;
    this.props.dispatch(
      fetchOrderTrackingMap(
        {
          code: data.get('code'),
          shareId: data.get('sharing') && data.get('sharing').shareId,
          callbackAction: ({ shipperGeoLocations } = {}) => callback(shipperGeoLocations),
        },
        this.getQuery(),
      ),
    );
  };

  cancelOrder = () => {
    const {
      data,
      location: { pathname },
    } = this.props;
    if (!data || !data.get('code')) return;

    const query = this.getQuery();
    query.pCancel = data.get('code');
    this.props.history.push(`${pathname}?${qs.stringify(query)}`);
  };

  openOrderPriceDetailGroup = () => {
    const {
      data,
      location: { pathname },
    } = this.props;
    if (!data || !data.get('code')) return;

    const query = this.getQuery();
    query.t = data.get('code');
    this.props.history.push(`${pathname}?${qs.stringify(query)}`);
  }

  openPhotoPopup = () => {
    const { data } = this.props;
    this.props.dispatch(open(PHOTO_POPUP, { photo: data.get('notePhoto') }));
  };

  openShipperPhotoPopup = () => {
    const { data } = this.props;

    this.props.dispatch(
      open(PHOTO_POPUP, {
        photo: (data.get('shipper') && data.get('shipper').avatar) ||
          '/dist/images/avatar.png',
      }),
    );
  };

  openRatingPopup = () => {
    const {
      data,
      location: { pathname },
    } = this.props;
    const query = this.getQuery();

    const id = data.get('id');
    const code = data.get('code');
    const shipper = data.get('shipper');
    const rating = data.get('rating');

    if (!id || !code || !shipper) return;

    const currentUser = this.props.currentUser;
    const isShipperHimself = currentUser && currentUser.get('id') === shipper.userId;

    if (isShipperHimself) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('you_cant_rate_your_own_delivery_order'),
          content: this.getString('to_maintain_fairness_you_cant_rate_your_self_order_and_delivery_order'),
        }),
      );
    }

    delete query.rating;
    const q = qs.stringify(query) ? `?${qs.stringify(query)}` : '';
    const qRating = `?${qs.stringify({ ...query, rating: true })}`;

    this.props.dispatch(
      open(RATING_POPUP, {
        shipper,
        rating,
        code,
        callback: () => this.props.history.push(`${pathname}${q}`),
      }),
    );
    this.props.history.push(`${pathname}${qRating}`);
  };

  openRatingMerchantPopup = () => {
    const {
      data,
      location: { pathname },
    } = this.props;
    const query = this.getQuery();

    const code = data.get('code');
    const merchant = data.get('eatery');
    const merchantRating = data.get('merchantRating');

    delete query.rating;
    const q = qs.stringify(query) ? `?${qs.stringify(query)}` : '';
    const qRating = `?${qs.stringify({ ...query, rating: true })}`;

    this.props.dispatch(
      open(MERCHANT_RATING_POPUP, {
        merchant,
        rating: merchantRating,
        code,
        orderLines: data.get('lines'),
        serviceName: data.get('serviceName'),
        callback: () => this.props.history.push(`${pathname}${q}`),
      }),
    );
    this.props.history.push(`${pathname}${qRating}`);
  };

  openSharingPopup = async () => {
    const { data } = this.props;

    let sharedId = data.get('sharing') && data.get('sharing').shareId;
    if (!sharedId) {
      sharedId = await new Promise((resolve) => {
        this.props.dispatch(
          fetchOrderSharing({
            code: data.get('code'),
            callbackAction: async (res) => resolve(res.shareId),
          }),
        );
      });
    }

    this.props.dispatch(
      generateShortLinkSharing({
        rawUrl: injectParams(LINK_DOMAIN + '/orders/${sharedId}?usp=sharing', { sharedId }),
        fallbackUrl: injectParams(LINK_DOMAIN + '/tai-khoan/don-hang/${sharedId}?usp=sharing', { sharedId }),
        baseSharingUrl: `${LINK_LZI}/o`,
        callback: (sharingUrl) => {
          sendGAEvent('loship_action', 'ACTION_SHARE_ORDER');

          this.props.dispatch(
            open(GENERIC_POPUP, {
              title: this.getString('sharing_order_status'),
              className: 'sharing-popup',
              closeBtnClassName: 'lz-close',
              content: (props) => (
                <SharingPopup
                  {...props}
                  getString={this.getString}
                  dispatch={this.props.dispatch}
                  ownedBy={data.get('ownedBy')}
                  recipient={data.get('routes').destination}
                  shortLink={sharingUrl}
                />
              ),
              animationStack: 1,
            }),
          );
        },
      }),
    );
  };

  reOrder = () => {
    const { data } = this.props;

    const serviceName = data.get('serviceName');
    const isAvailable = servicesAvailable.includes(serviceName);
    if (!isAvailable) {
      return this.props.dispatch(
        open(ORDER_FAILED, {
          title: this.getString('only_available_on_mobile_app'),
          error: iife(() => (
            <div>
              <p>
                {this.getString(
                  'the_service_you_are_requesting_is_only_available_on_loship_app_download_app_on_your_phone_to_use_the_service',
                )}
              </p>
              <div className="promo-btns" style={{ marginBottom: 32, marginTop: 8 }}>
                <a href="https://goo.gl/N5Vs4V" className="app-img app-android" target="_blank" rel="noopener">
                  <img src="/dist/images/app-android.png" alt="android" style={{ maxWidth: 120, marginRight: 8 }} />
                </a>
                <a href="https://goo.gl/CDFMhZ" className="app-img app-ios" target="_blank" rel="noopener">
                  <img src="/dist/images/app-ios.png" alt="ios" style={{ maxWidth: 120 }} />
                </a>
              </div>
            </div>
          )),
          hideBanner: true,
        }),
      );
    }

    const { username, slug } = data.get('eatery') || {};
    const redirectUrl = serviceName === 'losend' ? '/send' : (username && `/${username}`) || `/b/${slug}`;

    if (serviceName === 'losend') {
      return this.props.history.push(redirectUrl + `?status=re-order&code=${data.get('code')}`);
    }

    this.props.dispatch(
      fetchMerchant({
        merchantUsername: username,
        merchant: slug,
        callback: (resData) => {
          if (
            !isMerchantReady(
              { ...resData, shippingService: resData.shippingService && resData.shippingService.name },
              {
                dispatch: this.props.dispatch,
                getString: this.getString,
                history: this.props.history,
                callback: () => {
                  this.props.history.push(redirectUrl + `?status=re-order&code=${data.get('code')}&preorder=true`);
                },
              },
            )
          ) return;

          this.props.history.push(redirectUrl + `?status=re-order&code=${data.get('code')}`);
        },
      }),
    );
  };

  reportShipperFraudCancel = () => {
    const { data } = this.props;
    if (!data || !data.get('code')) return;

    this.props.dispatch(open(GENERIC_POPUP, {
      title: this.getString('report_shipper_fraud_cancel'),
      content: this.getString('report_shipper_fraud_cancel_content'),
      className: 'mw-400 text-left',
      actionClassName: 'flex-column-reverse',
      confirmBtn: {
        text: this.getString('report_shipper_fraud_cancel_btn'),
        type: 'optional', className: 'btn-fw',
      },
      cancelBtn: {
        text: this.getString('go_back'),
        type: 'link', className: 'text-dark-gray p8_8',
      },
      onConfirm: handleConfirm.bind(this),
      preventCloseOnConfirm: true,
    }));

    async function handleConfirm() {
      return new Promise((resolve) => {
        this.setState({ isSendingReport: true }, () => {
          this.props.dispatch(reportShipperFraudCancel({
            ...this.getParams(),
            callback: () => {
              this.setState({ isSendingReport: false });
              this.props.dispatch(open(GENERIC_POPUP, {
                content: (
                  <div>
                    <i className="lz lz-check-o text-green" style={{ fontSize: 40 }} />
                    <h4 className="my-16">{this.getString('report_shipper_fraud_cancel_success')}</h4>
                    <p className="text-left">{this.getString('report_shipper_fraud_cancel_success_content', '', [], true)}</p>
                  </div>
                ),
                contentClassName: 'mw-400',
                cancelBtn: {
                  text: this.getString('close'),
                  type: 'optional', className: 'btn-fw',
                },
                canNotDismiss: true,
              }));
              resolve(true);
            },
            callbackFailure: (err) => {
              this.setState({ isSending: false });

              const error = parseErrorV2(err);
              const errorMessage = iife(() => {
                if (!error.code) return this.getString(error.message);
                return this.getString('report_shipper_fraud_cancel_failed') + ' ' +
                  this.getString('error_code_0', '', [error.code]);
              });
              toast(errorMessage);
              resolve(errorMessage);
            },
          }));
        });
      });
    }
  }

  getPrivileges = () => {
    const { data } = this.props;
    const privileges = data.get('privileges') || [];

    if (privileges.some((privilege) => privilege.includes('edit_all'))) return 'EDIT';
    return 'VIEW';
  };
}

export default OrderDetailBase;
