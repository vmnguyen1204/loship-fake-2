import { GENERIC_POPUP, open } from 'actions/popup';
import BaseComponent from 'components/base';
import ImageLazy from 'components/statics/img-lazy';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import { cloneDeep } from 'lodash';
import qs from 'qs';
import React from 'react';
import { getConfigs, getUserOrder } from 'reducers/order';
import { roundPriceWithThreshold } from 'utils/cart';
import { addCurrency } from 'utils/format';
import { getRoute } from 'utils/routing';

class OrderPriceDetail extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const order = getUserOrder(state, params);
      data = data.merge(order);

      const config = getConfigs(state);
      if (config) data = data.set('config', config.get('data'));

      return data;
    },
  };

  calculatePrice = ({ lineGroups = [], lines = [], createdBy = 0, total = 0, totalUserFee = 0 }) => {
    const groupSize = lineGroups.length;
    const extraFee = Math.ceil((totalUserFee - total) / groupSize);
    let lineGroupIds = [];
    lineGroups.forEach((lineGroup) => {
      let userLinesValue = 0;
      lineGroupIds = [...lineGroupIds, ...lineGroup.lineIds];
      lineGroup.lineIds.forEach((lineId) => {
        const line = lines.find((o) => o.id === lineId);
        if (line && !line.isDeleted) {
          userLinesValue += line.quantity * line.price;
        }
      });
      lineGroup.userLinesValue = userLinesValue + extraFee;
    });

    const priceLineWithoutStaggedUser = (() => {
      let price = 0;
      lines.forEach((line) => {
        const lineData = lineGroupIds.find((o) => o === line.id);
        if (!lineData && !line.isDeleted) {
          price += line.quantity * line.price;
        }
      });
      return price;
    })();

    if (priceLineWithoutStaggedUser > 0) {
      const personCreated = lineGroups.find((o) => o.taggedUser.id === createdBy);
      if (personCreated) {
        personCreated.userLinesValue += priceLineWithoutStaggedUser;
      }
    }

    return lineGroups;
  }

  render() {
    const { data } = this.props;
    const {
      lineGroups = [],
      lines = [],
      total = 0,
      totalUserFee = 0,
      createdBy = 0,
    } = data.toObject();
    if (!this.props.data || !this.props.config) return null;

    return (
      <div className="order-price-detail-content">
        <div className="heading">
          <h4>{this.getString('order_price_detail_booking_groups')}</h4>
          <p>{this.getString('guide_group_order_detail')}</p>
        </div>
        <div className="section-money-user">
          {this.calculatePrice({
            lineGroups: cloneDeep(lineGroups),
            lines,
            createdBy,
            total,
            totalUserFee,
          }).map((lineGroup) => {
            const name = lineGroup.taggedUser?.name?.full || lineGroup.taggedUser?.username;
            return [
              name && (
                <div key={lineGroup.taggedUser?.id || lineGroup.taggedUser?.username} className="line-money-user">
                  <ImageLazy src={lineGroup.taggedUser.avatar} size={32} />
                  <div className="line-user-content">
                    <b>{name}</b>
                  </div>
                  <div className="line-user-money">
                    {addCurrency(roundPriceWithThreshold(lineGroup.userLinesValue))}
                  </div>
                </div>
              ),
            ];
          })}
        </div>
      </div>
    );
  }
}
const OrderPriceDetailGroup = initComponent(OrderPriceDetail);

export function OrderPriceDetailHandler({ callback }) {
  if (!this || !this.props) return;

  const { match } = getRoute(null, this.props.location.pathname);
  const query = this.getQuery();

  this.props.dispatch(
    open(GENERIC_POPUP, {
      title: this.getString('order_price_detail'),
      content: (popupProps) => <OrderPriceDetailGroup params={match.params} {...popupProps} />,
      className: 'order-price-detail',
      animationStack: 3,
      onCancel: () => {
        typeof callback === 'function' && callback();

        delete query.t;
        this.props.history.push(this.props.location.pathname + '?' + qs.stringify(query));
      },
      autoCloseHandler: () => {
        const cQuery = this.getQuery();
        if (!cQuery.t) return true;
      },
    }),
  );
}
