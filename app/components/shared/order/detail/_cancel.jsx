import { cancelUserOrder } from 'actions/order';
import { CUSTOM_CONTENT, GENERIC_POPUP, open } from 'actions/popup';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Checkbox from 'components/shared/checkbox';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import qs from 'qs';
import React from 'react';
import { getConfigs, getUserOrder } from 'reducers/order';
import { getRoute } from 'utils/routing';

class OrderCancelPopupComp extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const order = getUserOrder(state, params);
      data = data.merge(order);

      const config = getConfigs(state);
      if (config) data = data.set('config', config.get('data'));

      return data;
    },
  };

  render() {
    if (!this.props.data || !this.props.config) return null;
    const orderCancelNotes = this.props.config.get('orderCancelNotes');

    return (
      <div className="order-cancel-popup-content">
        <div className="heading">
          <h3>{this.getString('thank_for_using_loship')}</h3>
          <p>{this.getString('please_let_us_know_why_cancel')}</p>
        </div>

        <div className="cancel-list">
          {orderCancelNotes.map((cancelNote) => (
            <Checkbox
              key={cancelNote.id}
              className="cancel-item"
              checked={this.state.currentSelected === cancelNote.id}
              onChange={() => this.setState({ currentSelected: cancelNote.id })}
              isRadio
            >
              {cancelNote.title}
            </Checkbox>
          ))}
        </div>

        <div className="section section-cta">
          <Button
            type="primary"
            fw
            disabled={!this.state.currentSelected || this.state.isCancelling}
            onClick={this.cancelOrder}
          >
            {this.state.isCancelling && <i className="fas fa-spinner fa-spin" />}
            <span>{this.getString('send')}</span>
          </Button>
        </div>
      </div>
    );
  }

  cancelOrder = () => {
    if (!this.props.data) return;
    if (!this.state.currentSelected) return;

    this.setState({ isCancelling: true }, () => {
      this.props.dispatch(
        cancelUserOrder({
          code: this.props.data.get('code'),
          cancelNoteId: this.state.currentSelected,
          callback: (res) => {
            this.setState({ isCancelling: false });

            const data = (res.body && res.body.data) || {};
            if (data.shipper) {
              return this.props.dispatch(
                open(CUSTOM_CONTENT, {
                  title: this.getString('order_cancelled_but'),
                  content: this.getString(
                    'a_shipper_is_going_for_your_order_and_may_not_know_that_you_have_cancelled_it_we_would_very_appreciate_if_you_could_call_to_notice_him_about_the_cancellation',
                  ),
                  actionText: this.getString('i_got_it'),
                  contentClassName: 'content-left',
                  action: () => {
                    typeof this.props.handleClose === 'function' && this.props.handleClose();
                  },
                }),
              );
            }

            typeof this.props.handleClose === 'function' && this.props.handleClose();
          },
          callbackFailure: () => {
            this.setState({ isCancelling: false });

            this.props.dispatch(
              open(CUSTOM_CONTENT, {
                title: this.getString('order_not_cancelled'),
                content: this.getString(
                  'sorry_there_was_a_problem_cancelling_your_order_please_call_the_shipper_for_help',
                ).split('\n'),
                actionText: this.getString('go_back'),
                contentClassName: 'content-left',
              }),
            );
          },
        }),
      );
    });
  };
}
const OrderCancelPopup = initComponent(OrderCancelPopupComp);

export function OrderCancelPopupHandler({ callback }) {
  if (!this || !this.props) return;

  const { match } = getRoute(null, this.props.location.pathname);
  const query = this.getQuery();

  this.props.dispatch(
    open(GENERIC_POPUP, {
      title: this.getString('cancel_order_reason'),
      content: (popupProps) => <OrderCancelPopup params={match.params} {...popupProps} />,
      className: 'order-cancel-popup',
      animationStack: 3,
      onCancel: () => {
        typeof callback === 'function' && callback();

        delete query.pCancel;
        this.props.history.push(this.props.location.pathname + '?' + qs.stringify(query));
      },
      autoCloseHandler: () => {
        const cQuery = this.getQuery();
        if (!cQuery.pCancel) return true;
      },
    }),
  );
}
