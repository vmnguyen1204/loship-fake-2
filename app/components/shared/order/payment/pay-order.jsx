import { sendGAEvent } from 'actions/factory';
import { sendOrder } from 'actions/order';
import { GENERIC_POPUP, open } from 'actions/popup';
import { fetchDebtOrders } from 'actions/user';
import qs from 'qs';
import React from 'react';
import { cartClear } from 'reducers/new/cart';
import { APP_INFO, LINK_DOMAIN, LINK_DOMAIN_LOSHIP, LINK_DOMAIN_LOZI, SACOMBANK_GATEWAY, VIETTEL_GATEWAY, ZALOPAY_REDIRECT_URL } from 'settings/variables';
import { parseErrorV2 } from 'utils/error';
import { addCurrency, phoneNumber } from 'utils/format';
import { iife } from 'utils/helper';
import { hasString } from 'utils/i18n';
import { LIST_PAYMENT_BANKCODE } from 'utils/json/payment';
import * as cookie from 'utils/shims/cookie';
import { getLoXStorage } from 'utils/shims/localStorage';
import moment from 'utils/shims/moment';
import { payDebtOrderFunc } from './debt-order';

function handleSacombankOrder(resData) {
  const form = document.createElement('form');
  form.method = 'POST';
  form.action = SACOMBANK_GATEWAY;

  ['data', 'checkSum'].forEach((key) => {
    const hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = key;
    hiddenField.value = resData[key];

    form.appendChild(hiddenField);
  });

  document.body.appendChild(form);
  form.submit();
}

export function sendOrderFunc({
  isPayImmediate,
  payment, clientId, senderInfo, receiverInfo, serviceName,
  note, notePhoto, promotionCode, buyInSameBranch, isConfirmed,
  accessToken, toggleIsSending, onSaveLocal, onPaylaterFailed,
}) {
  if (!this || !this.props) return;

  const params = this.getParams();
  const {
    location: { pathname },
    merchant, cart, currentUser,
    sourceOrderId, distances,
  } = this.props;

  const groupTopic = params.groupTopic;

  typeof toggleIsSending === 'function' && toggleIsSending(true);

  if (!this.props.partnerConfig) localStorage.setItem('lastPaymentInfo', JSON.stringify(payment));
  sendGATracking({ type: 'place_order' });

  this.props.dispatch(sendOrder({
    isMobileDevice: this.context.isMobile,
    countryCode: serviceName !== 'losend' && receiverInfo.countryCode,
    clientId, senderInfo, receiverInfo, sourceOrderId,
    eateryId: merchant.get('id'),
    payment, distances, cart, groupTopic,
    note, notePhoto, promotionCode, buyInSameBranch, isConfirmed,
    defaultTaggedUserId: currentUser.get('id'),
    serviceName: serviceName || merchant.get('shippingService'),
    accessToken,
    isPayImmediate,
    callback: async (res) => {
      const { data: resData = {} } = res.body || {};

      if (!resData.code && LIST_PAYMENT_BANKCODE.indexOf(payment.method) > -1) {
        if (promotionCode.code && promotionCode.value && promotionCode.promotionType) {
          localStorage.setItem(
            `${merchant.username}.${groupTopic || '000000'}.lastPromotion`,
            JSON.stringify(this.props.promotionCode),
          );
        }

        let appTransId = resData.apptransid || resData.loziTransId;
        if (payment.method.includes('viettel')) appTransId = resData.billcode;

        if (appTransId) {
          // Save apptransid to local for redirect
          const storage = await getLoXStorage('loship');

          await storage.setItem(`${appTransId}.user`, this.props.currentUser.get('id'));
          await storage.setItem(`${appTransId}.service`, APP_INFO.serviceName);
          await storage.setItem(`${appTransId}.paymentMethod`, payment.method);
          await storage.setItem('current-agent', 'web');

          storage.disconnect();
        }

        typeof onSaveLocal === 'function' && onSaveLocal(appTransId);

        switch (payment.method) {
          case 'momopay':
            window.location = resData.payUrl;
            break;
          case 'momopayinapp':
            window.location = resData.deeplinkWebInApp;
            break;
          case 'sacombankpay':
            handleSacombankOrder(resData);
            break;
          case 'viettelpay': {
            const callbackParams = qs.stringify({ billcode: appTransId });
            const parsedData = {
              ...resData,
              return_url: LINK_DOMAIN + `/payment/notice?${callbackParams}`,
              cancel_url: LINK_DOMAIN + `/payment/notice?${callbackParams}`,
            };
            window.location = VIETTEL_GATEWAY + '?' + qs.stringify(parsedData);
            break;
          }
          case 'zalopayapp':
            if (this.context.isMobile && serviceName !== 'lozi') {
              // Open ZaloPay App
              const { appId, zpTransToken } = resData;
              window.location = ZALOPAY_REDIRECT_URL + '?' + qs.stringify({ appid: appId, zptranstoken: zpTransToken });
              break;
            }
          // IMPORTANT: break is omitted intentionally, zalopayapp on desktop will open gateway
          default: {
            const partnerCode = (() => {
              if (payment.method.includes('epay')) return 'epay';
              return 'zp';
            })();

            const xhr = new XMLHttpRequest();
            xhr.open(
              'GET',
              `${LINK_DOMAIN_LOSHIP}/payment/gateway?${qs.stringify({ appTransId, partnerCode })}`,
              true,
            );
            xhr.setRequestHeader('X-Access-Token', cookie.get('accessToken'));
            xhr.withCredentials = true;
            xhr.onload = function () {
              window.location.href = xhr.responseURL;
            };
            xhr.send();

            break;
          }
        }

        return;
      }

      sendGATracking({ type: 'order_success', order: resData, sourceOrderId: this.props.sourceOrderId });
      this.props.dispatch(
        cartClear({ merchant: merchant.get('slug'), groupTopic }),
      );
      this.props.dispatch({ type: 'PROMOTION_CLEAR' });

      // REDIRECT TO ORDER DETAIL PAGE AFTER SUCCESS
      if (serviceName === 'lozi') {
        const redirectedPathname = pathname.indexOf('/d/buy') >= 0 ? LINK_DOMAIN_LOZI + pathname : pathname;
        window.location = `${redirectedPathname}?${qs.stringify({
          status: 'success', code: resData.code, ...this.getQuery(),
        })}`;
        return;
      }

      this.props.history.push(`/tai-khoan/don-hang/${resData.code}`);
    },
    callbackFailure: (err) => {
      typeof toggleIsSending === 'function' && toggleIsSending(true);

      const error = parseErrorV2(err);
      this.setState({ isSending: false });

      if (error.code === 400508) return onPaylaterFailed();
      if (error.code === 400503) return showNoticeDebtOrder.call(this);

      const errorMessage = iife(() => {
        if (!error.code) return this.getString(error.message);

        return hasString(`error_${error.code}`, '', [error.code])
          ? this.getString(`error_${error.code}`)
          : this.getString('order_unsuccessful') + '.';
      });

      this.props.dispatch(
        open(GENERIC_POPUP, {
          title: this.getString('order_unsuccessful'),
          content: () => (
            <div>
              <p>{errorMessage}</p>
              {error.code && <p>({this.getString('error_code_0', '', [error.code])})</p>}
              <p>
                {this.getString('order_phone_number', '', [
                  <b key="phone">{phoneNumber(['losend'].includes(serviceName)
                    ? senderInfo.customerPhone
                    : receiverInfo.customerPhone)}
                  </b>,
                ])}
              </p>
              {promotionCode && promotionCode.code && (
                <p>
                  {this.getString('promotion')}: <b>{promotionCode.code}</b>
                </p>
              )}

              <br />
              <a href="https://loship.vn/app/lien-he/" target="_blank" rel="noopener">
                {this.getString('contact_loship')}
              </a>
            </div>
          ),
          className: 'mw-320 text-left',
          cancelBtn: { text: this.getString('order_preview'), type: 'primary', className: 'btn-fw' },
        }),
      );
    },
  }));

  function sendGATracking({ type, order, message }) {
    const time = moment().format('DD/MM/YY HH:mm:ss');
    const currentUserId = currentUser.get('id');
    const merchantId = merchant.get('id');

    if (!currentUserId) return;

    switch (type) {
      case 'order_success':
        sendGAEvent('loship_order', 'order_success', `U:${currentUserId}/C:${order.code}`);
        sourceOrderId && sendGAEvent(
          'loship_order',
          'reorder_success',
          `U:${currentUserId}/C:${order.code}/S:${sourceOrderId}`,
        );
        fbq('track', 'Purchase', {
          currency: 'VND',
          value: order.totalUserFee,
          extraParameters: { merchantId: merchant.get('id'), orderCode: order.code, purchaseType: 'order' },
        });
        break;

      case 'order_failed':
        sendGAEvent(
          'loship_order',
          'order_failed',
          `U:${currentUserId}${merchantId ? '/M:' + merchantId : ''}/E:[${message}]`,
        );
        sourceOrderId && sendGAEvent(
          'loship_order',
          'reorder_failed',
          `U:${currentUserId}${merchantId ? '/M:' + merchantId : ''}/E:[${message}]/S:${sourceOrderId}`,
        );
        break;

      default:
        sendGAEvent('loship_order', 'place_order', `U:${currentUserId}${merchantId ? '/M:' + merchantId : ''}@${time}`);
        sourceOrderId && sendGAEvent(
          'loship_order',
          'place_reorder',
          `U:${currentUserId}${merchantId ? '/M:' + merchantId : ''}/S:${sourceOrderId}@${time}`,
        );
        break;
    }
  }

  function showNoticeDebtOrder() {
    this.props.dispatch(fetchDebtOrders({
      callback: (res) => {
        const { body: { data } = {} } = res;
        if (Array.isArray(data) && data.length > 0) {
          const debtOrder = data[0];
          this.props.dispatch(
            open(GENERIC_POPUP, {
              title: this.getString('order_unsuccessful'),
              content: () => (
                <div>
                  <p>{this.getString('payment_unsuccessful_debt_order', '',
                    [debtOrder.code, addCurrency(debtOrder.totalUserFee)])}
                  </p>
                </div>
              ),
              className: 'mw-320 text-left',
              actionClassName: 'flex-column-reverse',
              confirmBtn: { text: this.getString('pay_now'), className: 'btn-fw' },
              cancelBtn: { text: this.getString('order_preview'), type: 'link', className: 'text-gray p8_8' },
              onConfirm: payDebtOrderFunc.bind(this, { orderCode: debtOrder.code }),
            }),
          );
        }
      },
    }));
  }
}
