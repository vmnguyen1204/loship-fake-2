import { GENERIC_POPUP, open } from 'actions/popup';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import qs from 'qs';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { iife } from 'utils/helper';

class OrderPaymentNotice extends BaseComponent {
  componentDidMount() {
    this.handlePaymentNotice();
  }

  componentDidUpdate(prevProps) {
    const query = this.getQuery();
    const prevQuery = this.getQuery(prevProps);

    if (query.step === 'confirm-order' && prevQuery.step !== 'confirm-order') this.handlePaymentNotice();
  }

  render() {
    return null;
  }

  handlePaymentNotice = () => {
    const { match } = this.props;
    const query = this.getQuery();
    if (query.step !== 'confirm-order') return;
    if (!query.addCard && !query.payDebt && !query.paymentMethod) return;

    this.props.dispatch(
      open(GENERIC_POPUP, {
        ...iife(() => {
          if (query.addCard === 'success') {
            return {
              header: iife(() => (
                <div className="popup-header dynamic-height">
                  <i className="lz lz-check-o text-green icon-header-popup" />
                  <br />
                  {this.getString('add_card_successful_title')}
                </div>
              )),
              content: this.getString('add_card_successful'),
              ...match.path === '/:merchantUsername/:groupTopic?'
                ? { cancelBtn: { text: this.getString('return'), type: 'primary' } }
                : {
                  confirmBtn: { text: this.getString('buy_on_loship_now'), className: 'btn-fw' },
                  cancelBtn: { text: this.getString('return'), type: 'link', className: 'text-gray p8_8' },
                  actionClassName: 'flex-column-reverse',
                  onConfirm: () => { this.props.history.push('/'); },
                },
            };
          }
          if (query.addCard && query.addCard !== 'success') {
            return {
              title: this.getString('add_card_unsuccessful_title'),
              content: this.getString('add_card_unsuccessful'),
              cancelBtn: { text: this.getString('i_got_it'), type: 'primary' },
            };
          }
          if (query.payDebt === 'success') {
            return {
              header: iife(() => (
                <div className="popup-header dynamic-height">
                  <i className="lz lz-check-o text-green icon-header-popup" />
                  <br />
                  {this.getString('payment_successful')}
                </div>
              )),
              content: this.getString('payment_debt_successful'),
              cancelBtn: { text: this.getString('close'), type: 'primary' },
            };
          }
          if (query.payDebt && query.payDebt !== 'success') {
            return {
              title: this.getString('payment_unsuccessful_title'),
              content: this.getString('payment_unsuccessful'),
              cancelBtn: { text: this.getString('i_got_it'), type: 'primary' },
            };
          }
          if (query.paymentMethod) {
            return {
              title: this.getString('payment_unsuccessful_title'),
              content: this.getString('payment_unsuccessful'),
              cancelBtn: { text: this.getString('i_got_it'), type: 'primary' },
            };
          }
        }),
        className: 'mw-400',
        contentClassName: 'text-left',
        onCancel: () => {
          delete query.step;
          delete query.addCard;
          delete query.payDebt;
          delete query.paymentMethod;
          this.props.history.replace(`${this.props.location.pathname}?${qs.stringify(query)}`);
        },
      }),
    );
  }
}

export default initComponent(OrderPaymentNotice, withRouter);
