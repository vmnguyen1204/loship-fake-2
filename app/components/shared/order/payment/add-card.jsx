import { createPaymentCard } from 'actions/order';
import qs from 'qs';
import { APP_INFO, LINK_DOMAIN_LOSHIP } from 'settings/variables';
import * as cookie from 'utils/shims/cookie';
import { getLoXStorage } from 'utils/shims/localStorage';

export function addCardFunc({ payment, onSaveLocal }) {
  if (!this || !this.props) return;

  const query = this.getQuery();
  const { location: { pathname } } = this.props;

  this.props.dispatch(
    createPaymentCard({
      paymentMethod: payment.method,
      callback: async (res) => {
        const { data: resData = {} } = res.body || {};
        const appTransId = resData.apptransid || resData.loziTransId;

        if (!appTransId) throw new Error('Add card failed');

        // Save apptransid to local for redirect
        if (appTransId) {
          const storage = await getLoXStorage('loship');

          await storage.setItem(`${appTransId}.service`, APP_INFO.serviceName);
          await storage.setItem(`${appTransId}.paymentMethod`, payment.method);
          await storage.setItem(`${appTransId}.redirectUrl`, pathname);
          await storage.setItem(`${appTransId}.addCard`, true);
          await storage.setItem('current-agent', 'web');

          storage.disconnect();
        }
        typeof onSaveLocal === 'function' && onSaveLocal(appTransId);

        switch (payment.method) {
          default: {
            const partnerCode = (() => {
              if (payment.method.includes('epay')) return 'epay';
              return 'zp';
            })();

            const xhr = new XMLHttpRequest();
            xhr.open(
              'GET',
              `${LINK_DOMAIN_LOSHIP}/payment/gateway?${qs.stringify({ appTransId, partnerCode })}`,
              true,
            );
            xhr.setRequestHeader('X-Access-Token', cookie.get('accessToken'));
            xhr.withCredentials = true;
            xhr.onload = function () {
              window.location.href = xhr.responseURL;
            };
            xhr.send();

            break;
          }
        }
      },
      callbackFailure: () => {
        this.props.history.replace(
          `${pathname}?${qs.stringify({ ...query, step: 'confirm-order', addCard: 'failed' })}`,
        );
      },
    }),
  );
}
