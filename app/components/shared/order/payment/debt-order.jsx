import { autoPayDebtOrder, fetchPaymentSupportPayDebt, payDebtOrder } from 'actions/order';
import { SELECT_PAYMENT_METHOD, open } from 'actions/popup';
import { fetchDebtOrders, fetchPaymentCardsSupportPayDebt } from 'actions/user';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import ExpandContainer from 'components/shared/expand-container';
import OrderPaymentNotice from 'components/shared/order/payment/notice';
import Popup from 'components/shared/popup';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getCurrentUser, getDebtOrders } from 'reducers/user';
import { APP_INFO, LINK_DOMAIN_LOSHIP, ZALOPAY_REDIRECT_URL } from 'settings/variables';
import { addCurrency } from 'utils/format';
import * as cookie from 'utils/shims/cookie';
import { getLoXStorage } from 'utils/shims/localStorage';

class OrderDebt extends BaseComponent {
  static contextTypes = { isMobile: PropTypes.bool }

  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      const debtOrder = getDebtOrders(state);
      if (debtOrder) data = data.merge(debtOrder);

      return data;
    },
    customRender: true,
  }

  componentDidMount() {
    if (this.props.currentUser) this.props.dispatch(fetchDebtOrders());
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.currentUser && this.props.currentUser) this.props.dispatch(fetchDebtOrders());
  }

  renderOrderDebt = () => {
    if (!this.props.data) return;
    const debtOrder = this.props.data.get(0);

    return (
      <ExpandContainer
        key="debt-order"
        className="order-debt-container"
        title={() => (
          <div className="title">
            <i className="lz lz-info-square" />
            {this.getString('payment_debt_order_title', '', [debtOrder.code])}
          </div>
        )}
        content={() => (
          <div className="content">
            {this.getString('payment_debt_order', '', [addCurrency(debtOrder.totalUserFee)])}
            <Button
              type="primary" fw className="mt-16"
              disabled={this.state.isPaying}
              onClick={payDebtOrderFunc.bind(this,
                { orderCode: debtOrder.code, toggleIsPaying: this.toggleIsPaying })}>
              {this.getString('pay_now')}
            </Button>
          </div>
        )}
        line={0} />
    );
  }

  renderOrderDebtPopup = () => {
    return (
      <Popup
        className={cx('generic-popup confirm-popup order-debt-popup', { fullscreen: this.context.isMobile })}
        handleClose={() => this.toggleViewOrderDebt(false)}
        animationStack={2}
      >
        <Button className={cx('btn-back bg-transparent')} onClick={() => this.toggleViewOrderDebt(false)}>
          <i className="lz lz-close" />
        </Button>
        <div className="content">
          {this.renderOrderDebt()}
        </div>
      </Popup>
    );
  };

  render() {
    if (!this.props.data || this.props.data.size === 0) return <OrderPaymentNotice />;

    if (this.context.isMobile) {
      return (
        <div className="order-debt">
          {this.renderOrderDebt()}
          <OrderPaymentNotice />
        </div>
      );
    }

    return (
      <div className="order-debt-icon">
        <Button className="btn-order-debt" onClick={() => this.toggleViewOrderDebt(true)}>
          <i className="fas fa-exclamation-triangle" />
        </Button>
        <OrderPaymentNotice />
        {this.state.viewOrderDebt && this.renderOrderDebtPopup()}
      </div>
    );
  }

  toggleIsPaying = (isPaying) => {
    this.setState({ isPaying });
  };

  toggleViewOrderDebt = (isViewing) => {
    if (this.state.viewOrderDebt === isViewing) return;
    this.setState({ viewOrderDebt: isViewing });
  }
}

export default initComponent(OrderDebt, withRouter);

export async function payDebtOrderFunc({ orderCode, toggleIsPaying }) {
  if (!this || !this.props) return;

  const query = this.getQuery();
  const { location: { pathname } } = this.props;

  const [paymentSupportPayDebt, paymentCardSupportPayDebt] = await Promise.all([
    new Promise((resolve) => {
      this.props.dispatch(fetchPaymentSupportPayDebt({
        callback: (res) => {
          const data = res.body && res.body.data || [];
          return resolve(data);
        },
      }));
    }),
    new Promise((resolve) => {
      this.props.dispatch(fetchPaymentCardsSupportPayDebt({
        callback: (res) => {
          const data = res.body && res.body.data || [];
          return resolve(data);
        },
      }));
    }),
  ]);

  function handleAutoPayDebtOrder() {
    typeof toggleIsPaying === 'function' && toggleIsPaying(true);
    this.props.dispatch(autoPayDebtOrder({
      order: orderCode,
      callback: () => {
        typeof toggleIsPaying === 'function' && toggleIsPaying(false);
        this.props.history.replace(
          `${pathname}?${qs.stringify({ ...query, step: 'confirm-order', payDebt: 'success' })}`,
        );
      },
      callbackFailure: () => {
        this.props.dispatch(open(SELECT_PAYMENT_METHOD, {
          paymentFees: paymentSupportPayDebt,
          paymentCardFees: paymentCardSupportPayDebt,
          wrapperClassName: 'layer-toaa',
          onSelectPaymentMethod: (paymentMethod, { bank, card, type, saveCard } = {}) => {
            const payment = {
              method: paymentMethod,
              bank,
              card,
              type,
              saveCard,
            };
            handlePayDebtOrder.call(this, { payment });
          },
          onClose: () => {
            typeof toggleIsPaying === 'function' && toggleIsPaying(false);
          },
        }));
      },
    }));
  }

  function handlePayDebtOrder({ payment }) {
    this.props.dispatch(payDebtOrder({
      isMobileDevice: this.context.isMobile,
      order: orderCode,
      payment,
      callback: async (res) => {
        const { data: resData = {} } = res.body || {};
        const appTransId = resData.apptransid || resData.loziTransId;

        if (!appTransId) throw new Error('Paydebt failed');

        // Save apptransid to local for redirect
        const storage = await getLoXStorage('loship');

        await storage.setItem(`${appTransId}.service`, APP_INFO.serviceName);
        await storage.setItem(`${appTransId}.paymentMethod`, payment.method);
        await storage.setItem(`${appTransId}.redirectUrl`, pathname);
        await storage.setItem(`${appTransId}.payDebt`, true);
        await storage.setItem('current-agent', 'web');

        storage.disconnect();

        switch (payment.method) {
          case 'momopay':
            window.location = resData.payUrl;
            break;
          case 'momopayinapp':
            window.location = resData.deeplinkWebInApp;
            break;
          case 'zalopayapp':
            if (this.context.isMobile) {
              // Open ZaloPay App
              const { appId, zpTransToken } = resData;
              window.location = ZALOPAY_REDIRECT_URL + '?' + qs.stringify({ appid: appId, zptranstoken: zpTransToken });
              break;
            }
          // IMPORTANT: break is omitted intentionally, zalopayapp on desktop will open gateway
          default: {
            const partnerCode = (() => {
              if (payment.method.includes('epay')) return 'epay';
              return 'zp';
            })();

            const xhr = new XMLHttpRequest();
            xhr.open(
              'GET',
              `${LINK_DOMAIN_LOSHIP}/payment/gateway?${qs.stringify({ appTransId, partnerCode })}`,
              true,
            );
            xhr.setRequestHeader('X-Access-Token', cookie.get('accessToken'));
            xhr.withCredentials = true;
            xhr.onload = function () {
              window.location.href = xhr.responseURL;
            };
            xhr.send();

            break;
          }
        }
      },
      callbackFailure: () => {
        this.props.history.replace(
          `${pathname}?${qs.stringify({ ...query, step: 'confirm-order', payDebt: 'failed' })}`,
        );
        typeof toggleIsPaying === 'function' && toggleIsPaying(false);
      },
    }));
  }

  return handleAutoPayDebtOrder.call(this);
}
