import Button from 'components/shared/button';
import AuthButton from 'components/shared/button/auth';
import Checkbox from 'components/shared/checkbox';
import { STEP_INFO } from 'components/shared/order';
import StepProcess from 'components/shared/step-process/order';
import Hint from 'components/statics/hint';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { addCurrency } from 'utils/format';
import ConfirmPageBase from './confirm-base';

class OrderConfirmPage extends ConfirmPageBase {
  render() {
    const { step, serviceName, merchant, senderInfo, receiverInfo, extraFees } = this.props;
    const { isSending } = this.state;

    const {
      isDisabled,
      shippingFee,
      total,
      discountValue,
      deliveryTime,
      allowSwitchBranch,
      payment,
      _error,
    } = this.computeRenderData();

    return (
      <div className="order-step-confirm">
        <StepProcess step={step} labels={this.props.stepLabels} />

        <div className="page-content">
          <h3>{this.getString('order_information')}</h3>

          {serviceName !== 'lozi' && this.renderMerchantInfo(merchant)}
          {serviceName === 'losend' && this.renderShippingAddress({
            title: this.getString('sender_information', 'shippingAddress'),
            shippingAddress: senderInfo,
            isSender: true,
          })}

          {this.renderShippingAddress({
            title: serviceName === 'losend' && this.getString('receiver_information', 'shippingAddress'),
            shippingAddress: receiverInfo,
          })}

          {this.renderOrderDetail({ merchant, shippingFee, extraFees, discount: discountValue })}
          {this.renderPaymentMethod()}
          {payment.method !== 'cod' && (
            <div className="section extra-option">
              <div className="content">
                <Checkbox className="reverse" checked={this.state.isConfirmed} onChange={this.updateIsConfirmed}>
                  {this.getString('no_order_confirm_call_needed')}
                  <Hint text={this.getString('no_order_confirm_call_needed_description')} />
                </Checkbox>
              </div>
            </div>
          )}
          {allowSwitchBranch && (
            <div className="section extra-option">
              <div className="content">
                <Checkbox className="reverse" checked={this.state.buyInSameBranch} onChange={this.updateBuyInSameBranch}>
                  {this.getString('allow_buy_in_same_branch')}
                  <Hint text={this.getString('allow_buy_in_same_branch_description')} />
                </Checkbox>
              </div>
            </div>
          )}

          {serviceName === 'losend' && this.renderPhotoTaken({ title: this.getString('attach_photo') })}

          <div className="summary">
            <b>{this.getString('total')}</b>
            <span className="highlight pull-right">
              <b>{addCurrency(total)}</b>
            </span>
          </div>

          <div className="section note">
            <label>{this.getString('note')}</label>
            <div className="content">
              <textarea
                className="note"
                ref="note"
                defaultValue={this.props.srcNote}
                placeholder={this.getString('building_name_floor_room_etc')}
                rows={3}
              />
            </div>
          </div>

          {!!deliveryTime && (
            <div className="pre-order-confirm-note">
              {this.getString('please_be_noticed_that', 'order')}
              &nbsp;
              {this.getString('you_are_now_pre_ordering_at_0_your_pre_order_will_be_delivered_at', 'order', [
                merchant.get('name'),
              ])}
              &nbsp;
              <b>{this.getString(deliveryTime.string, 'order', [deliveryTime.time])}</b>.
            </div>
          )}

          {!!_error && <div className="_error">{this.getString(_error)}</div>}

          <div className="form-footer">
            {['losend', 'lozi'].indexOf(serviceName) < 0 && (
              <Button type="link" className="text-gray" onClick={() => this.props.changeStep(STEP_INFO)}>
                <i className="lz lz-arrow-head-left" />
                <span>{this.getString('back_to_menu')}</span>
              </Button>
            )}
            <span />

            <AuthButton
              type="primary"
              disabled={isDisabled}
              onClick={this.sendOrder}
              onLoginSuccess={
                isDisabled
                  ? () => {}
                  : () => {
                    const isSender = serviceName === 'losend';
                    this.props.updateShippingInfo(
                      isSender ? this.props.senderInfo : this.props.receiverInfo,
                      isSender,
                      this.sendOrder,
                    );
                  }
              }
            >
              {this.getString(!isSending ? 'confirm_order' : 'sending_order')}
            </AuthButton>
          </div>
        </div>

        {this.renderPaymentNotice()}
      </div>
    );
  }
}

export default initComponent(OrderConfirmPage, withRouter);
