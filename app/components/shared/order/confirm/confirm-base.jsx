import { fetchPaymentCardFees, fetchPaymentFees, fetchShippingFees } from 'actions/merchant';
import { fetchPaymentSupportTokenized } from 'actions/order';
import { CONFIRM_POPUP, GENERIC_POPUP, PHOTO_POPUP, STOP_SERVICE_POPUP, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import ButtonSwitch from 'components/shared/button/switcher';
import { STEP_ADDRESS, STEP_ADDRESS_FROM, STEP_INFO } from 'components/shared/order';
import CartOrder from 'components/shared/order/cart/cart-order';
import { addCardFunc } from 'components/shared/order/payment/add-card';
import OrderPaymentNotice from 'components/shared/order/payment/notice';
import { sendOrderFunc } from 'components/shared/order/payment/pay-order';
import PaymentSelector from 'components/shared/payment';
import Hint from 'components/statics/hint';
import FileUploader from 'components/statics/uploader';
import { Map } from 'immutable';
import moment from 'moment';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { getPromotionCode } from 'reducers/code';
import { getCart, getCartMetadata } from 'reducers/new/cart';
import { getOrderNotifications } from 'reducers/notification';
import { getPaymentSupportTokenized } from 'reducers/order';
import { STOP_SERVING_TIMESTAMP } from 'settings/variables';
import { calculateOrderPrice, getDeliveryTime, getOrderLines } from 'utils/cart';
import { getDistance } from 'utils/distance';
import { addCurrency, getRawPhoneNumber, isValidPhoneNumber, phoneNumber } from 'utils/format';
import { iife } from 'utils/helper';
import { LIST_PAYMENT_TYPES } from 'utils/json/payment';
import * as LoshipImage from 'utils/LoshipImage';
import { isPast } from 'utils/new-time';
import * as cookie from 'utils/shims/cookie';
import { getLoXStorage } from 'utils/shims/localStorage';

class ConfirmPageBase extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const cartMetadata = getCartMetadata(state, params);
      if (cartMetadata) data = data.set('cartMetadata', cartMetadata);

      const cart = getCart(state, params);
      if (cart) data = data.set('cart', cart.data);

      const promotionCode = getPromotionCode(state);
      if (promotionCode) data = data.set('promotionCode', promotionCode.get('data'));

      const paymentsSupportTokenized = getPaymentSupportTokenized(state);
      if (paymentsSupportTokenized) data = data.set('paymentsSupportTokenized', paymentsSupportTokenized.get('data'));

      // For cart invalidation
      const orders = getOrderNotifications(state);
      const latestOrder = orders && orders.get('data') && orders.get('data')[0];
      if (latestOrder) data = data.set('latestOrder', latestOrder);

      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props) {
    super(props);

    this.state = {
      buyInSameBranch: true,
      customSenderInfo: {
        active: false,
        customerName: '',
        customerPhone: '',
      },
      customReceiverInfo: {
        active: false, // TODO: Losend -> true
        customerName: '',
        customerPhone: '',
      },
      finalPriceStatus: 'INIT',
    };
  }

  componentDidMount() {
    this.identifyClient();
    this.updateDistances(this.props, true);
    this.updateShippingFee();
    this.updatePaymentFee();

    if (!isPast(STOP_SERVING_TIMESTAMP)) this.props.dispatch(open(STOP_SERVICE_POPUP));

    // auto close popup item
    const query = this.getQuery();
    if (query.pItem) {
      delete query.pItem;
      this.props.history.replace(`${this.props.location.pathname}?${qs.stringify(query)}`);
    }
  }

  componentDidUpdate(prevProps) {
    const { senderInfo = {}, receiverInfo = {}, distances = {}, payment } = this.props;
    const {
      senderInfo: prevSenderInfo = {},
      receiverInfo: prevReceiverInfo = {},
      distances: prevDistances = {},
      payment: prevPayment,
    } = prevProps;

    if (
      senderInfo.lat !== prevSenderInfo.lat ||
      senderInfo.lng !== prevSenderInfo.lng ||
      receiverInfo.lat !== prevReceiverInfo.lat ||
      receiverInfo.lng !== prevReceiverInfo.lng
    ) this.updateDistances(this.props, true);

    if (distances.value !== prevDistances.value) this.updateShippingFee(distances.value);
    if (prevPayment.method === 'cod' && payment.method !== 'cod') this.scrollToIsConfirmedToggle();
  }

  renderShippingAddress = ({ shippingAddress = {}, isSender = false, isMobile }) => {
    const { customSenderInfo, customReceiverInfo } = this.state;
    const shippingInfo = (() => {
      const result = isSender
        ? {
          ...shippingAddress,
          active: customSenderInfo.active,
          customerName: shippingAddress.customerName || customSenderInfo.customerName || '',
          customerPhone: shippingAddress.customerPhone || customSenderInfo.customerPhone || '',
          title: this.getString('pickup_address', 'shippingAddress'),
          userTitle: this.getString('send_from_relative', 'shippingAddress'),
          userEmptyContent: this.getString('switch_this_on_to_pickup_order_from_your_relative', 'shippingAddress'),
        }
        : {
          ...shippingAddress,
          active: customReceiverInfo.active,
          title: this.getString('shipping_address', 'shippingAddress'),
          userTitle: this.getString('i_want_to_order_for_my_relative', 'shippingAddress'),
          userEmptyContent: this.getString('switch_this_on_to_send_order_for_you_relative', 'shippingAddress'),
        };

      // TODO: logic for sender
      if (result.active) {
        result.customerName = customReceiverInfo.customerName;
        result.customerPhone = customReceiverInfo.customerPhone;
      }

      return result;
    })();

    return (
      <div>
        <div className={cx('section shipping', { separator: !isMobile })}>
          <div className="content">
            <b>{shippingInfo.title}</b>
            <div className="shipping-address-info">
              <i className="lz lz-location-marker" />
              <span>{shippingInfo.address}</span>
              <div
                className="btn-edit"
                onClick={() => this.props.changeStep(isSender ? STEP_ADDRESS_FROM : STEP_ADDRESS, true)}
              >
                <i className="lz lz-pencil" /> {!isMobile && this.getString('tap_to_change')}
              </div>
            </div>
          </div>
        </div>
        <div className={cx('section shipping')}>
          <div className="content">
            <b>{shippingInfo.userTitle}</b>
            <ButtonSwitch
              className="pull-right"
              status={shippingInfo.active}
              toggle={() => {
                this.setState({
                  ...(isSender && { customSenderInfo: { ...customSenderInfo, active: !customSenderInfo.active } }),
                  ...(!isSender && { customReceiverInfo: { ...customReceiverInfo, active: !customReceiverInfo.active } }),
                });
              }}
            />

            {shippingInfo.active ? (
              <div style={{ marginTop: 10 }}>
                <div className="shipping-address-info">
                  <i className="fas fa-user" />
                  <input
                    value={shippingInfo.customerName}
                    onChange={this.editUserInformation(isSender, 'customerName')}
                    placeholder={this.getString('type_name_here')}
                  />
                </div>
                <div className="shipping-address-info">
                  <i className="fas fa-phone" />
                  <input
                    value={phoneNumber(shippingInfo.customerPhone)}
                    onChange={this.editUserInformation(isSender, 'customerPhone')}
                    placeholder={this.getString('type_phone_number_here')}
                  />
                </div>
              </div>
            ) : (
              <div className="shipping-address-info-empty">{shippingInfo.userEmptyContent}</div>
            )}
          </div>
        </div>
      </div>
    );
  };

  renderMerchantInfo = (merchant) => {
    if (!merchant) return null;

    return (
      <div className="section">
        <div className="content merchant-item">
          <p>{this.getString('location')}</p>
          <div className="avatar">
            <img src={merchant.get('avatar')} alt="Avatar" />
          </div>
          <div className="detail">
            <p className="title">
              <b>{merchant.get('name')}</b>
            </p>
            <p>{merchant.get('address')}</p>
          </div>
        </div>
      </div>
    );
  };

  renderOrderDetail = ({ merchant, shippingFee, extraFees, discount }) => {
    return (
      <div className="section">
        <div className="title">{this.getString('order_detail')}</div>
        <div className="content">
          <CartOrder
            mode="codebox|codebox-readonly|suggest-dishes|hide-empty|show-raw-price"
            eateryId={merchant.get('id')}
            serviceName={merchant.get('shippingService')}
            extraFees={extraFees}
            shippingFee={shippingFee}
            promotionEnable={this.props.promotionEnable}
            promotionStatus={this.getPromotionStatus()}
            discount={discount}
            keepLastDish
            showAll
          />
        </div>
      </div>
    );
  };

  // TODO: refactor
  renderOrderItem = ({
    id,
    icon,
    title,
    sub,
    price,
    priceHighlight,
    extra,
    zeroMode,
    deleted,
    border,
    separate,
    alignCenter,
    action,
    ...rest
  }) => {
    const details = iife(() => {
      if (!Array.isArray(rest.details)) rest.details = (rest.details && [rest.details]) || [];
      return rest.details;
    });
    const showExtraInsteadOfPrice = !parseInt(price) && !!extra;
    const zeroPrice = iife(() => {
      if (zeroMode === 'number') return addCurrency(0);
      if (zeroMode === 'free') return this.getString('free');
      return '';
    });
    const highlightPrice = !deleted && ((zeroMode === 'free' && !parseInt(price)) || priceHighlight);
    const showPrice = !showExtraInsteadOfPrice && (!!zeroMode || !!parseInt(price));

    return (
      <div key={id} className={cx('order-item', { border, separate, center: alignCenter })}>
        {icon}
        <div className={cx('main', { deleted })}>
          {title} {sub}
        </div>
        {showPrice && (
          <div className={cx('price', { deleted, highlight: highlightPrice })}>
            {deleted && this.getString('soldout', 'order')}
            {!deleted && (!parseInt(price) ? zeroPrice : addCurrency(price))}
          </div>
        )}
        {showExtraInsteadOfPrice && <div className={cx('extra')}>{extra}</div>}
        {details.length > 0 && (
          <div className="details">
            {details.map((detail) => (
              <div key={detail} className="detail">
                {detail}
              </div>
            ))}
          </div>
        )}
        {!deleted && !!action && action}
      </div>
    );
  };

  renderPhotoTaken = ({ title } = {}) => {
    const hasNotePhoto = this.state.notePhoto || this.props.srcNotePhoto;

    return (
      <div className="section note">
        {!!title && <div className="title">{this.getString('attach_photo')}</div>}
        {hasNotePhoto && (
          <div className="content btn-attach-photo uploaded" onClick={this.openPhotoPopup}>
            <i className="lz lz-photo" /> {this.getString('photo_attached')}
            <i className="lz lz-close btn-close" onClick={this.clearNotePhoto} />
          </div>
        )}
        {!hasNotePhoto && (
          <FileUploader className="content btn-attach-photo" onChange={this.handleFileUpload} accept="image/*">
            <i className="lz lz-plus" /> {this.getString('add_photo_optional')}
          </FileUploader>
        )}
      </div>
    );
  };

  renderPaymentMethod = () => {
    const { paymentFees, paymentCardFees } = this.state;
    if (!paymentFees) return null;

    const paymentFee = (() => {
      const fee = paymentFees.find((cFee) => cFee.method === this.props.payment.method);
      if (!fee?.paymentMethod?.fee || fee.paymentMethod.fee <= 0) return null;

      return {
        id: 'payment-fee',
        title: this.getString('convenience_fee'),
        sub: <Hint text={this.getString('the_payment_method_you_selected_is_applied_convenient_fee')} />,
        price: fee.fee,
        border: !this.context.isMobile,
      };
    })();

    const { payment, partnerConfig, paymentsSupportTokenized } = this.props;
    const onSelectPaymentMethod = partnerConfig && !partnerConfig.allowSelectOtherPaymentMethod
      ? null
      : this.props.updatePaymentInfo;
    return (
      <div className="section">
        <div className="title">{this.getString('payment_method')}</div>
        <div className="content">
          <PaymentSelector
            key={payment.method}
            defaultValue={payment}
            paymentFees={paymentFees}
            paymentCardFees={paymentCardFees}
            paymentsSupportTokenized={paymentsSupportTokenized}
            onSelect={onSelectPaymentMethod}
            onCreatePaymentCard={this.handleAddPaymentCard}
            merchant={(this.props.merchant && this.props.merchant.toObject()) || {}}
            groupTopic={this.getParams().groupTopic}
            serviceName={this.props.serviceName}
            order={getOrderLines(this.props.data)}
          />

          {paymentFee && this.renderOrderItem(paymentFee)}
        </div>
      </div>
    );
  };

  renderPaymentNotice = () => {
    return (
      <OrderPaymentNotice />
    );
  };

  sendOrder = async () => {
    const { location: { pathname } } = this.props;
    const query = this.getQuery();
    const params = this.getParams();

    const promotionStatus = this.getPromotionStatus();
    if (promotionStatus.status !== 'ACCEPTED') {
      return this.props.dispatch(
        open(GENERIC_POPUP, {
          confirmBtn: { text: this.getString('remove_code'), className: this.context.isMobile && 'btn-fw' },
          cancelBtn: { text: this.getString('go_back'), className: 'bg-transparent text-gray text-normal' },
          ...iife(() => {
            if (promotionStatus.status === 'MEMBER_HAS_NO_MAIN_DISH') {
              return {
                title: promotionStatus.title,
                content: promotionStatus.message,
                confirmBtn: undefined,
                cancelBtn: { text: this.getString('i_got_it'), type: 'primary' },
              };
            }
            if (promotionStatus.status === 'CAMPAIGN_SIMILAR_PRICE_APPLIED') {
              return {
                title: this.getString('campaign_similar_price_applied'),
                content: this.getString('campaign_similar_price_applied_desc', '', [<b key="promotion">{promotionStatus.code}</b>]),
              };
            }

            return {
              title: this.getString('cannot_apply_promo_code'),
              content: this.getString('cannot_apply_promo_code_desc', '', [<b key="promotion">{promotionStatus.code}</b>]),
            };
          }),
          onConfirm: () => {
            this.props.dispatch({ type: 'PROMOTION_CLEAR' });
          },
          actionClassName: this.context.isMobile && 'flex-column-reverse',
        }),
      );
    }

    const { senderInfo: sInfo, receiverInfo: rInfo, serviceName, payment } = this.props;
    const { customSenderInfo, customReceiverInfo, clientId } = this.state;
    const promotionCode = (this.props.promotionCode && this.props.promotionCode.toJS()) || {};
    const senderInfo = { ...sInfo };
    const receiverInfo = { ...rInfo };

    if (!receiverInfo.countryCode) {
      // prefer countryCode from reorderData
      receiverInfo.countryCode = this.props.currentUser.get('countryCode');
    }

    if (customSenderInfo.active) {
      senderInfo.customerName = customSenderInfo.customerName;
      senderInfo.customerPhone = customSenderInfo.customerPhone;
    }
    if (customReceiverInfo.active) {
      receiverInfo.customerName = customReceiverInfo.customerName;
      receiverInfo.customerPhone = customReceiverInfo.customerPhone;
      receiverInfo.isRecipientChanged = true;
      receiverInfo.countryCode = '';
    }

    let _error = undefined;

    if (serviceName === 'losend') {
      if (!senderInfo.customerName) _error = this.getString('sender_name_is_required');
      else if (!senderInfo.customerPhone) _error = this.getString('sender_phone_number_is_required');
      else if (!isValidPhoneNumber(senderInfo.customerPhone)) _error = this.getString('invalid_sender_phone_number');

      // !Preserved
      senderInfo.countryCode = '';
      receiverInfo.countryCode = '';
    }

    if (!receiverInfo.customerName) _error = this.getString('name_is_required');
    else if (!receiverInfo.customerPhone) _error = this.getString('phone_number_is_required');
    else if (!isValidPhoneNumber(receiverInfo.customerPhone)) _error = this.getString('invalid_phone_number');

    if (_error) {
      setTimeout(() => {
        window.scrollTo(0, document.body.scrollHeight);
      }, 0);
      return this.setState({ _error });
    }

    const merchant = (this.props.merchant && this.props.merchant.toObject()) || {};
    const accessToken = cookie.get('tempData');

    const onSaveLocal = async (appTransId) => {
      if (appTransId) {
        const storage = await getLoXStorage('loship');

        const order = getOrderLines(this.props.cart);
        query.status === 're-order' && query.code &&
          (await storage.setItem(`${query.code}.reorder`, JSON.stringify(order)));
        serviceName !== 'losend' && (await storage.setItem(`${appTransId}.merchant`, merchant.username));
        params.groupTopic && (await storage.setItem(`${appTransId}.groupTopic`, params.groupTopic));

        if (['lozi', 'losend'].includes(serviceName) || (query.status === 're-order' && query.code))
          await storage.setItem(`${appTransId}.redirectUrl`, `${pathname}?${qs.stringify(query)}`);

        storage.disconnect();
      }

      if (this.props.partnerConfig) {
        localStorage.setItem('latestOrderCode', this.props.latestOrder?.code || 'none');
        params.groupTopic && localStorage.setItem('latestGroupTopic', params.groupTopic);
      }
    };

    sendOrderFunc.call(this, {
      payment, clientId, senderInfo, receiverInfo, serviceName,
      note: this.refs.note.value.trim(),
      notePhoto: await this.getNotePhotoUrl(),
      promotionCode,
      buyInSameBranch: this.state.buyInSameBranch,
      isConfirmed: this.state.isConfirmed,
      accessToken,
      onSaveLocal,
      toggleIsSending: this.toggleIsSending,
      onPaylaterFailed: sendOrderFunc.bind(this, {
        isPayImmediate: true,
        payment, clientId, senderInfo, receiverInfo, serviceName,
        note: this.refs.note.value.trim(),
        notePhoto: await this.getNotePhotoUrl(),
        promotionCode,
        buyInSameBranch: this.state.buyInSameBranch,
        accessToken,
        onSaveLocal,
        toggleIsSending: this.toggleIsSending,
      }),
    });
  };

  toggleIsSending = (isSending) => {
    this.setState({ isSending });
  }

  computeRenderData = () => {
    const { extraFees, merchant, serviceName, partnerConfig, payment } = this.props;
    const { paymentFees = [] } = this.state;
    const promotionCode = (this.props.promotionCode && this.props.promotionCode.toJS()) || {};
    const paymentFee = paymentFees.find((fee) => fee.method === this.props.payment.method);

    const hasDistances = !!this.props.distances;
    const shippingFee = this.state.shippingFee || {
      actualShippingFee: 0,
      distance: 0,
      shippingFee: 0,
    };

    const promotionStatus = this.getPromotionStatus();
    const order = getOrderLines(this.props.cart);
    const totalWithoutDiscount = calculateOrderPrice({
      order,
      ship: shippingFee.shippingFee,
      extraFees,
    });
    const totalWithDiscount = calculateOrderPrice({
      order,
      ship: shippingFee.shippingFee,
      extraFees,
      discount: promotionCode,
    });
    const total = (promotionStatus.status === 'ACCEPTED' ? totalWithDiscount : totalWithoutDiscount) +
      ((paymentFee && paymentFee.fee) || 0);
    const discountValue = totalWithoutDiscount - totalWithDiscount;

    const operatingStatus = merchant && merchant.get('operatingStatus');
    const deliveryTime = !operatingStatus.isOpening && getDeliveryTime(
      moment()
        .add(operatingStatus.minutesUntilNextStatus, 'minutes')
        .add(1, 'hour'),
      true,
    );

    const brand = merchant && merchant.get('brand');
    const isBranchesAvailable = brand && (brand.size > 0 || brand.length > 0); // why i need to check both size and length T.T
    const allowSwitchBranch = iife(() => {
      if (!isBranchesAvailable) return false;
      if (partnerConfig && !partnerConfig.allowToggleBranchSwitch) return false;
      if (['lozi', 'losend'].includes(serviceName)) return false;
      return true;
    });

    const _error = this.state._error;

    const isDisabled = this.state.isSending ||
      !hasDistances ||
      !isPast(STOP_SERVING_TIMESTAMP) ||
      this.state.finalPriceStatus !== 'SUCCESS' ||
      order.length === 0;

    return {
      isDisabled,
      shippingFee,
      total,
      totalWithDiscount,
      totalWithoutDiscount,
      discountValue,
      promotionCode,
      deliveryTime,
      allowSwitchBranch,
      _error,
      payment,
    };
  };

  editUserInformation = (isSender, key) => (e) => {
    let value = e.target.value;
    if (key === 'customerPhone') value = getRawPhoneNumber(value.replace(/[^0-9+]/g, ''));

    const { customSenderInfo, customReceiverInfo } = this.state;

    this.setState({
      ...(isSender && { customSenderInfo: { ...customSenderInfo, [key]: value } }),
      ...(!isSender && { customReceiverInfo: { ...customReceiverInfo, [key]: value } }),
      _error: undefined,
    });
  };

  updateBuyInSameBranch = () => {
    this.setState((state) => ({ buyInSameBranch: !state.buyInSameBranch }));
  };

  updateIsConfirmed = () => {
    this.setState((state) => ({ isConfirmed: !state.isConfirmed }));
  }

  updateDistances = async (props, force = false) => {
    const { senderInfo, receiverInfo, distances: srcDistances } = props;

    /** Need this to bypass google map error. Backend will control distance later. */
    setTimeout(() => {
      if (!this.state.hasDistances) {
        this.props.updateDistances({ text: '0 km', value: 0 });
      }
    }, 5000);
    const distances = !force && srcDistances ? srcDistances : await getDistance(senderInfo, receiverInfo);
    this.setState({ hasDistances: !!distances }, () => props.updateDistances(distances));
  };

  updateShippingFee = (distance) => {
    const distanceValue = distance || (this.props.distances && this.props.distances.value) || 0;

    this.setState({ finalPriceStatus: 'PENDING' }, () => {
      this.props.dispatch(
        fetchShippingFees({
          ...this.getParams(),
          eateryId: this.props.eateryId,
          serviceName: this.props.serviceName,
          distance: distanceValue,
          callback: (res) => {
            if (res.status === 200) {
              const data = res.body.data;
              this.setState({ finalPriceStatus: 'SUCCESS', shippingFee: data });
            } else {
              this.setState({ finalPriceStatus: 'FAILED' });
              this.props.dispatch(
                open(CONFIRM_POPUP, {
                  title: this.getString('something_wrong'),
                  description: this.getString('fetch_shipping_fee_failed_message'),
                  confirmBtn: { text: this.getString('try_again') },
                  cancelBtn: { text: this.getString('back') },
                  onConfirm: () => this.updateShippingFee(distanceValue),
                  onCancel: () => this.props.changeStep(STEP_INFO),
                  canNotDismiss: true,
                }),
              );
            }
          },
        }),
      );
    });
  };

  updatePaymentFee = async () => {
    const { payment, partnerConfig } = this.props;
    const availablePaymentMethods = (partnerConfig && partnerConfig.availablePaymentMethods) || this.props.availablePaymentMethods;

    if (!this.props.paymentsSupportTokenized) this.props.dispatch(fetchPaymentSupportTokenized());

    const callbackFunc = ({ rawData, resolve }) => {
      const data = rawData.reduce((acc, fee) => {
        if (partnerConfig?.partnerName) {
          if (fee.method === 'cod') fee.method = partnerConfig.paymentMethod;
          if (fee.paymentMethod) {
            fee.paymentMethod.method = partnerConfig.paymentMethod;
            fee.paymentMethod.type = partnerConfig.paymentType;
          }
        }

        if (availablePaymentMethods && !availablePaymentMethods.includes(fee.method)) return acc;
        return acc.concat(fee);
      }, []);
      this.setState({ paymentFees: data });

      const paymentFee = data.find((fee) => fee.method === payment.method);
      if (!paymentFee) {
        this.props.updatePaymentInfo('cod');
        return resolve(false);
      }

      const { isBankRequired, isTokenizationEnabled, type } = paymentFee.paymentMethod || {};
      this.props.updatePaymentInfo(payment.method, {
        bank: isBankRequired ? payment.bank : undefined,
        card: payment.card,
        saveCard: !payment.card && isTokenizationEnabled,
        type,
      });
      resolve(true);
    };

    const isValidPaymentMethod = await new Promise((resolve) => {
      this.props.dispatch(
        fetchPaymentFees({
          ...this.getParams(),
          eateryId: this.props.eateryId,
          serviceName: this.props.serviceName,
          callback: (res) => {
            if (res.status === 200) {
              callbackFunc({ rawData: res.body.data, resolve });
            } else {
              callbackFunc({ rawData: [{ 'id': 0, 'method': 'cod', 'fee': 0, 'paymentMethod': { 'id': 0, 'method': 'cod', 'type': 'cod' } }], resolve });
            }
          },
        }),
      );
    });

    const isValidPaymentCard = await new Promise((resolve) => {
      this.props.dispatch(
        fetchPaymentCardFees({
          eateryId: this.props.eateryId,
          serviceName: this.props.serviceName,
          callback: (res) => {
            if (res.status === 200) {
              const data = iife(() => {
                if (partnerConfig) {
                  if (['momo', 'sacombankpay', 'viettelpay'].includes(partnerConfig.partnerName)) return [];
                }
                return res.body.data;
              });
              this.setState({ paymentCardFees: data });

              const paymentCard = payment.card && data.find((paymentCardFee) => paymentCardFee.id === payment.card.id);
              const isValid = !payment.card || !!paymentCard;

              if (paymentCard) {
                const type = paymentCard.paymentMethodFee &&
                  paymentCard.paymentMethodFee.paymentMethod &&
                  paymentCard.paymentMethodFee.paymentMethod.type;

                this.props.updatePaymentInfo(payment.method, {
                  card: paymentCard,
                  saveCard: payment.saveCard,
                  type,
                });
              }

              resolve(isValid);
            } else {
              resolve(false);
            }
          },
        }),
      );
    });

    if (!isValidPaymentMethod || !isValidPaymentCard) {
      this.props.dispatch(
        open(CONFIRM_POPUP, {
          title: this.getString(!isValidPaymentMethod ? 'payment_method_disabled' : 'card_does_not_existed'),
          content: this.getString(
            !isValidPaymentMethod ? 'payment_method_disabled_description' : 'card_does_not_existed_description',
          ),
          actionClassName: this.context.isMobile && 'flex-column-reverse',
          confirmBtn: {
            text: this.getString('select_another_payment_method'),
            className: this.context.isMobile && 'btn-fw',
          },
          cancelBtn: {
            text: this.getString('select_cod'),
            type: 'link',
            className: 'text-gray p8_8',
          },
          onConfirm: () => {
            this.props.updatePaymentInfo('cod');
            const elem = document.getElementById('btn-payment-edit');
            elem.click();
          },
          onCancel: () => this.props.updatePaymentInfo('cod'),
          canNotDismiss: true,
        }),
      );
    }
  };

  handleAddPaymentCard = (payment) => {
    const query = this.getQuery();
    const { serviceName } = this.props;
    const promotionCode = (this.props.promotionCode && this.props.promotionCode.toJS()) || {};
    const merchant = (this.props.merchant && this.props.merchant.toObject()) || {};

    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('add_payment_card'),
        content: this.getString(
          'add_payment_card_description', '',
          [addCurrency(payment.paymentMethod && payment.paymentMethod.issueFee || 0, '')],
          true,
        ),
        className: 'mw-400',
        contentClassName: 'text-left',
        actionClassName: 'flex-column-reverse',
        confirmBtn: { text: this.getString('continue'), className: 'btn-fw' },
        cancelBtn: { text: this.getString('return'), type: 'link', className: 'text-dark-gray py-8 px-8' },
        onConfirm: addCardFunc.bind(this, {
          payment,
          onSaveLocal: async (appTransId) => {
            const groupTopic = this.getParams().groupTopic;
            if (promotionCode.code && promotionCode.value && promotionCode.promotionType) {
              localStorage.setItem(
                `${merchant.username}.${groupTopic || '000000'}.lastPromotion`,
                JSON.stringify(this.props.promotionCode),
              );
            }

            if (appTransId) {
              const storage = await getLoXStorage('loship');

              const order = getOrderLines(this.props.cart);
              query.status === 're-order' && query.code &&
                (await storage.setItem(`${query.code}.reorder`, JSON.stringify(order)));
              serviceName !== 'losend' && (await storage.setItem(`${appTransId}.merchant`, merchant.username));
              groupTopic && (await storage.setItem(`${appTransId}.groupTopic`, groupTopic));
              await storage.setItem(`${appTransId}.user`, this.props.currentUser.get('id'));

              storage.disconnect();
            }
          },
        }),
      }),
    );
  };

  handleFileUpload = (data, thumb) => {
    this.setState({ notePhoto: data, notePhotoThumbnail: thumb });
  };

  clearNotePhoto = (e) => {
    e.stopPropagation();
    this.setState({ notePhoto: undefined, notePhotoThumbnail: undefined });

    if (this.props.srcNotePhoto) this.props.clearSrcNotePhoto();
  };

  openPhotoPopup = () => {
    const isBase64 = !!this.state.notePhotoThumbnail;

    this.props.dispatch(
      open(PHOTO_POPUP, {
        photo: this.state.notePhotoThumbnail || this.props.srcNotePhoto,
        isBase64,
      }),
    );
  };

  getNotePhotoUrl = async () => {
    if (this.props.srcNotePhoto) {
      const image = await LoshipImage.getImageFromUrl(this.props.srcNotePhoto);
      return image.getURL();
    }
    if (this.state.notePhoto) return this.state.notePhoto.getURL();
  };

  handleViewPromoList = () => {
    const {
      eateryId,
      location: { pathname },
    } = this.props;
    const query = this.getQuery();
    query.pPromo = eateryId;
    query.step = 'confirm-order';

    this.props.history.push(`${pathname}?${qs.stringify(query)}`);
  };

  handleClearPromo = (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.props.dispatch({ type: 'PROMOTION_CLEAR' });
  };

  getPromotionStatus = () => {
    const { extraFees, payment, cartMetadata, currentUser } = this.props;
    const order = getOrderLines(this.props.cart);

    const firstInvalidUser = iife(() => {
      if (!this.props.cart) return;
      const target = Object.entries(this.props.cart)
        .find(([key, val]) => {
          if (key ==='anonymous') return false;
          if (!val || !val.data || val.data.length === 0) return false;

          if (val.data.some((x) => !x.isExtraGroupDish)) return false;
          return true;
        });
      if (!target) return;
      const { data: { users = [] } = {} } = cartMetadata;
      return users.find((user) => user.username === target[0]);
    });
    if (firstInvalidUser) {
      if (currentUser && currentUser.get('username') === firstInvalidUser.username) {
        return {
          status: 'MEMBER_HAS_NO_MAIN_DISH',
          title: this.getString('invalid_order'),
          message: this.getString('some_locations_wont_sell_just_extra_dishes_you_need_to_select_at_least_1_main_dish_on_the_menu_before_continuing'),
        };
      }
      return {
        status: 'MEMBER_HAS_NO_MAIN_DISH',
        title: this.getString('member_havent_selected_a_main_course'),
        message: this.getString('notice_member_has_no_main_dish', '', [firstInvalidUser.name && firstInvalidUser.name.full || firstInvalidUser.username]),
      };
    }

    const promotionCode = this.props.promotionCode && this.props.promotionCode.toJS();
    if (!promotionCode) return { status: 'ACCEPTED' };
    if (!promotionCode.value) return { status: 'INVALID_CODE', code: promotionCode.code };

    if (order.some((line) => line.isAppliedFixedPricePromotionCampaign)) {
      return {
        status: 'CAMPAIGN_SIMILAR_PRICE_APPLIED',
        code: promotionCode.code,
        message: this.getString('promo_notice_campain_fixed_price'),
      };
    }

    if (promotionCode.minimumUserToApply > 1) {
      const validUsers = Object.entries(this.props.cart)
        .filter(([key, val]) => {
          if (key ==='anonymous') return false;
          if (!val || !val.data || val.data.length === 0) return false;

          if (val.data.some((x) => !x.isExtraGroupDish)) return true;
          return false;
        });
      if (this.props.sourceOrderId || validUsers.length < promotionCode.minimumUserToApply) {
        return {
          status: 'NOT_REACH_MINIMUM_MEMBER',
          code: promotionCode.code,
          message: this.getString('promo_notice_minimum_member', '', [promotionCode.minimumUserToApply], true),
        };
      }
    }

    if (promotionCode.isLimitMinimumOrderValue) {
      const shippingFee = this.state.shippingFee || {
        actualShippingFee: 0,
        distance: 0,
        shippingFee: 0,
      };

      const totalWithoutDiscount = calculateOrderPrice({
        order,
        ship: shippingFee.shippingFee,
        extraFees,
      });
      if (totalWithoutDiscount < promotionCode.minimumOrderValue) {
        return {
          status: 'NOT_REACH_MINIMUM_TOTAL',
          code: promotionCode.code,
          message: this.getString('promo_notice_minimum_total', '', [
            <b key="minimum_total">{addCurrency(promotionCode.minimumOrderValue)}</b>,
          ]),
        };
      }
    }

    if (promotionCode.isLimitPaymentType && !promotionCode.paymentTypes.includes(payment.type)) {
      const paymentTypesStr = (promotionCode.paymentTypes || [])
        .map((type) => this.getString(LIST_PAYMENT_TYPES[type]))
        .join(', ');
      return {
        status: 'UNACCEPTABLE_PAYMENT_TYPE',
        code: promotionCode.code,
        message: this.getString('promo_notice_payment_type', '', [<b key="payment_type">{paymentTypesStr}</b>]),
      };
    }

    return { status: 'ACCEPTED' };
  };

  identifyClient = () => {
    // Get clientId from partnerConfig
    const partnerConfig = this.props.partnerConfig;
    if (partnerConfig) return this.setState({ clientId: partnerConfig.clientId });

    // Only work with Lozi
    const { serviceName } = this.props;
    if (!serviceName || serviceName !== 'lozi') return;
    if (typeof navigator !== 'undefined') {
      let clientId = 5; // web Lozi desktop
      if (navigator.userAgent.match(/iPod|iPhone|iPad/) && !navigator.userAgent.match(/IEMobile/)) {
        clientId = 35; // app Lozi iOS
      } else if (navigator.userAgent.match(/Android/)) {
        clientId = 25; // app Lozi Android
      }

      this.setState({ clientId });
    }
  };

  scrollToIsConfirmedToggle = () => {
    const element = document.getElementById('toggle-is-confirmed');
    if (element) element.scrollIntoView({ behavior: 'smooth', block: 'center' });
  };
}

export default ConfirmPageBase;
