import React, { Component } from 'react';

import PropTypes from 'prop-types';
import Desktop from './confirm';
import Mobile from './confirm.mobile';

export default class OrderStepConfirm extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}
