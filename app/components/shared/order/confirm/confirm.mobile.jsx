import cx from 'classnames';
import Button from 'components/shared/button';
import AuthButton from 'components/shared/button/auth';
import ButtonSwitch from 'components/shared/button/switcher';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { addCurrency } from 'utils/format';
import ConfirmPageBase from './confirm-base';

class OrderConfirmPage extends ConfirmPageBase {
  render() {
    const { serviceName, merchant, senderInfo, receiverInfo, extraFees } = this.props;
    const { isSending } = this.state;

    const {
      isDisabled,
      shippingFee,
      total,
      discountValue,
      promotionCode,
      deliveryTime,
      allowSwitchBranch,
      payment,
      _error,
    } = this.computeRenderData();

    const isAppliedPromotionCode = promotionCode && promotionCode.code && discountValue > 0;

    return (
      <div className="order-step-confirm">
        {serviceName !== 'lozi' && this.renderMerchantInfo(merchant)}
        {serviceName === 'losend' && this.renderShippingAddress({
          title: this.getString('sender_information', 'shippingAddress'),
          shippingAddress: senderInfo,
          isSender: true,
          isMobile: true,
        })}

        {this.renderShippingAddress({
          title: this.getString(
            serviceName === 'losend' ? 'receiver_information' : 'confirm_shipping_address',
            'shippingAddress',
          ),
          shippingAddress: receiverInfo,
          isMobile: true,
        })}

        <div className="section note">
          <div className="title">{this.getString('note')}</div>
          <div className="content">
            <textarea
              className="note"
              ref="note"
              defaultValue={this.props.srcNote}
              placeholder={this.getString('building_name_floor_room_etc')}
              rows={3}
            />
          </div>
        </div>

        {serviceName === 'losend' && this.renderPhotoTaken()}

        {/* {this.renderOrderDetail({ shippingFee, extraFees, discount: discountValue, totalWithoutDiscount })} */}
        {this.renderOrderDetail({ merchant, shippingFee, extraFees, discount: discountValue })}
        {this.renderPaymentMethod()}
        {payment.method !== 'cod' && (
          <div className="section extra-option" id="toggle-is-confirmed">
            <div className="content">
              <h4 className="pull-left">{this.getString('no_order_confirm_call_needed')}</h4>
              <ButtonSwitch
                className="pull-right"
                status={this.state.isConfirmed}
                toggle={this.updateIsConfirmed}
              />
              <div className="clear" />
              <p className="content-description">{this.getString('no_order_confirm_call_needed_description')}</p>
            </div>
          </div>
        )}
        {allowSwitchBranch && (
          <div className="section extra-option">
            <div className="content">
              <h4 className="pull-left">{this.getString('allow_buy_in_same_branch')}</h4>
              <ButtonSwitch
                className="pull-right"
                status={this.state.buyInSameBranch}
                toggle={this.updateBuyInSameBranch}
              />
              <div className="clear" />
              <p className="content-description">{this.getString('allow_buy_in_same_branch_description')}</p>
            </div>
          </div>
        )}

        {!!deliveryTime && (
          <div className="pre-order-confirm-note">
            {this.getString('please_be_noticed_that', 'order')}
            &nbsp;
            {this.getString('you_are_now_pre_ordering_at_0_your_pre_order_will_be_delivered_at', 'order', [
              merchant.get('name'),
            ])}
            &nbsp;
            <b>{this.getString(deliveryTime.string, 'order', [deliveryTime.time])}</b>.
          </div>
        )}

        {!!_error && <div className="_error">{this.getString(_error)}</div>}

        <div className="section cta">
          <div className="summary">
            <b>{this.getString('total')}</b>
            <span className="highlight pull-right">
              <b>{addCurrency(total)}</b>
            </span>
          </div>
          <div className="d-flex">
            <Button
              className={cx('bg-transparent btn-promo', isAppliedPromotionCode && 'applied')}
              onClick={this.handleViewPromoList}
            >
              <i className={cx('lz', isAppliedPromotionCode ? 'lz-check-o' : 'lz-discount')} />
              <span>{this.getString(isAppliedPromotionCode ? 'applied_promotion' : 'apply_promotion')}</span>
              {isAppliedPromotionCode && <i className="lz lz-close" onClick={this.handleClearPromo} />}
            </Button>
            <AuthButton
              type="primary"
              fw
              disabled={isDisabled}
              onClick={this.sendOrder}
              onLoginSuccess={
                isDisabled
                  ? () => {}
                  : () => {
                    const isSender = serviceName === 'losend';
                    this.props.updateShippingInfo(
                      isSender ? this.props.senderInfo : this.props.receiverInfo,
                      isSender,
                      this.sendOrder,
                    );
                  }
              }
            >
              {this.getString(!isSending ? 'confirm_order' : 'sending_order')} <i className="lz lz-arrow-head-right" />
            </AuthButton>
          </div>
        </div>

        {this.renderPaymentNotice()}
      </div>
    );
  }
}

export default initComponent(OrderConfirmPage, withRouter);
