import { CONFIRM_POPUP, CUSTOM_CONTENT, GROUP_ORDER_INVITE, GROUP_ORDER_MEMBER_LIST, LOGIN, open } from 'actions/popup';
import BaseComponent from 'components/base';
import { Map } from 'immutable';
import { connect, getMQTT, publicData, subscribe } from 'lib/mqtt';
import PropTypes from 'prop-types';
import React from 'react';
import { getMerchant } from 'reducers/merchant';
import {
  cartAddItem,
  cartAddUser,
  cartGroupCreate,
  cartGroupJoin,
  cartOrderSuccess,
  getCartMetadata,
  getCartMetadataKey,
} from 'reducers/new/cart';
import { getCurrentUser } from 'reducers/user';
import { LINK_DOMAIN } from 'settings/variables';
import { isMerchantReady } from 'utils/merchant';

if (process.env.BROWSER) {
  require('assets/styles/components/_group-order.scss');
}

class GroupControllerBase extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const cartMetadata = getCartMetadata(state, params);
      if (cartMetadata) data = data.set('cartMetadata', cartMetadata);

      const merchant = getMerchant(state, params);
      if (merchant) data = data.set('merchant', merchant.get('data'));

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      const mqttStatus = getMQTT(state);
      if (mqttStatus) data = data.set('mqttStatus', mqttStatus.status);

      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  componentDidMount() {
    this.setup({ isFirstSetup: true });
  }

  componentDidUpdate(prevProps) {
    this.setup({ prevProps });
  }

  routerWillLeave = (nextLocation) => {
    const { merchantUsername, merchant, groupTopic } = this.getParams();
    const { pathname: nextPathname } = nextLocation;

    if (!groupTopic || nextPathname.includes(groupTopic)) return true;
    if (nextPathname.includes(merchantUsername) || nextPathname.includes(merchant)) return true;

    // Exception for order success -> /tai-khoan/don-hang/:code
    if (nextPathname.includes('/tai-khoan/don-hang')) return true;

    return confirm(
      this.getString('note_you_are_in_group_order', 'order') +
      ' ' +
      this.getString('if_you_leave_the_group_you_will_need_groups_url_to_return', 'order'),
    );
  };

  renderLoading = () => {
    return (
      <div className="group-control-center">
        <i className="fas fa-spinner fa-spin" /> {this.getString('fetching_group_order', 'order')}
      </div>
    );
  };

  setup = () => {
    const { cartMetadata = {}, currentUser } = this.props;
    const { data: { createdBy, success } = {} } = cartMetadata;
    const { groupTopic, merchant, merchantUsername } = this.getParams();
    const linkMerchant = `/${merchantUsername || `b/${merchant}`}`;

    if (!groupTopic) return;

    if (currentUser && !createdBy && !this.state.joined) {
      this.setState({ joined: true });
      this.props.dispatch(cartGroupJoin({
        ...this.getParams(),
        callback: ({ error }) => {
          if (error) {
            this.openPopupLoadFailed();
          }
        },
      }));
    }
    if (!this.props.currentUser) {
      return false;
    }
    if (createdBy) {
      this.setupMQTT();
    }
    if (this.props.status === 'FAILED') {
      return this.openPopupLoadFailed();
    }
    if (success) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('order_successfully_received'),
          content: this.getString('your_group_orders_are_received_thank_you_for_using_loship'),
          actionText: this.getString('Đồng ý'),
          canNotDismiss: true,
          action: () => this.props.history.push(linkMerchant),
        }),
      );
    }
  };

  setupMQTT = async () => {
    if (this.lastMqttStatus === this.props.mqttStatus) return;
    this.lastMqttStatus = this.props.mqttStatus;

    if (this.props.mqttStatus === 'INIT') return this.props.dispatch(connect());
    if (this.props.mqttStatus !== 'CONNECTED' || this.mqttHasConnected) return;

    const { merchant, currentUser, cartMetadata = {} } = this.props;
    const { data: { createdBy, users = [] } = {} } = cartMetadata;
    const { groupTopic } = this.getParams();
    const cartMetadataKey = getCartMetadataKey({ groupTopic });

    // sub channel /:id/notification to listen new users join
    this.props.dispatch(
      subscribe(
        `orders/${groupTopic}/notification`,
        {
          type: cartAddUser.SUCCESS.type,
          payload: { key: cartMetadataKey },
        },
        (userData) => {
          // collect NEW user's order
          this.props.dispatch(
            subscribe(`orders/${groupTopic}/${userData.user.id}`, {
              type: cartAddItem.SUCCESS.type,
              payload: {
                key: `${cartMetadataKey}.${userData.user.username}`,
                user: userData.user,
                cartMetadataKey,
                merchant: merchant.get('slug'),
                groupTopic,
                overwrite: true,
              },
            }),
          );
        },
      ),
    );

    // collect ALREADY user's order
    users.forEach((user) => {
      this.props.dispatch({
        type: cartAddUser.SUCCESS.type,
        payload: { key: cartMetadataKey, data: user },
      });
      this.props.dispatch(
        subscribe(`orders/${groupTopic}/${user.id}`, {
          type: cartAddItem.SUCCESS.type,
          payload: {
            key: `${cartMetadataKey}.${user.username}`,
            user,
            cartMetadataKey,
            merchant: merchant.get('slug'),
            groupTopic,
            overwrite: true,
          },
        }),
      );
    });

    // sub this channel to know when order is successfully.
    if (createdBy !== currentUser.get('id')) {
      this.props.dispatch(
        subscribe(`orders/${groupTopic}/information`, {
          type: cartOrderSuccess.type,
          payload: { key: cartMetadataKey },
        }),
      );
    }

    // pub channel /:id/notification to join
    this.props.dispatch(
      publicData(`orders/${groupTopic}/notification`, { user: this.props.currentUser.toJS() }),
    );

    this.mqttHasConnected = true;
  };

  createGroupOrder = async () => {
    const currentUser = this.props.currentUser;
    if (!currentUser) {
      return this.props.dispatch(
        open(LOGIN, {
          callback: () => {
            this.createGroupOrder();
          },
        }),
      );
    }

    const { merchant } = this.props;
    if (
      !isMerchantReady(merchant.toObject(), {
        dispatch: this.props.dispatch,
        getString: this.getString,
        history: this.props.history,
        callback: async () => {
          this.props.dispatch(
            cartGroupCreate({
              merchant: merchant.get('slug'),
              eateryId: merchant.get('id'),
              callback: ({ data, error }) => {
                if (!data || error) return;
                const { topic } = data;

                if (!topic) {
                  return this.props.dispatch(
                    open(CUSTOM_CONTENT, {
                      title: this.getString('something_went_wrong', 'eatery'),
                      content: this.getString(
                        'theres_unexpected_problem_prevent_us_to_process_your_action_sorry',
                        'eatery',
                      ),
                    }),
                  );
                }
                this.openPopupInvite(topic, true)();
              },
            }),
          );
        },
      })
    ) return;

    this.props.dispatch(
      cartGroupCreate({
        merchant: merchant.get('slug'),
        eateryId: merchant.get('id'),
        callback: ({ data, error }) => {
          if (!data || error) return;
          const { topic } = data;

          if (!topic) {
            return this.props.dispatch(
              open(CUSTOM_CONTENT, {
                title: this.getString('something_went_wrong', 'eatery'),
                content: this.getString('theres_unexpected_problem_prevent_us_to_process_your_action_sorry', 'eatery'),
              }),
            );
          }
          this.openPopupInvite(topic, true)();
        },
      }),
    );
  };

  leaveGroup = (force) => {
    const { merchant, merchantUsername } = this.getParams();

    if (force === true) return this.props.history.push(merchantUsername ? `/${merchantUsername}` : `/b/${merchant}`);
    this.props.dispatch(
      open(CONFIRM_POPUP, {
        content: () => (
          <div className="popup-group-info">
            <i className="lz lz-info-square" />
            <p>{this.getString('note_you_are_in_group_order', 'order')}</p>
            <p>{this.getString('if_you_leave_the_group_you_will_need_groups_url_to_return', 'order')}</p>
          </div>
        ),
        confirmBtn: { text: this.getString('confirm_leave_group', 'order') },
        cancelBtn: { text: this.getString('go_back'), className: 'transparent mt-8' },
        onConfirm: () => this.props.history.push(merchantUsername ? `/${merchantUsername}` : `/b/${merchant}`),
        actionClassName: 'flex-column-reverse',
      }),
    );
  };

  openPopupLogin = () => {
    return this.props.dispatch(
      open(LOGIN, {
        callback: () => {
          this.setup();
        },
      }),
    );
  }

  openPopupLoadFailed = () => {
    const { merchant, merchantUsername } = this.getParams();
    const linkMerchant = `/${merchantUsername || `b/${merchant}`}`;

    return this.props.dispatch(
      open(CUSTOM_CONTENT, {
        content: this.getString('failed_to_fetch_group_order_click_to_return_to_merchant', 'order'),
        actionText: this.getString('Đồng ý'),
        canNotDismiss: true,
        action: () => this.props.history.push(linkMerchant),
      }),
    );
  }

  openPopupInvite = (topic, isCreate) => () => {
    const {
      merchant,
      location: { pathname, search },
    } = this.props;
    const groupTopic = topic || this.getParams().groupTopic;

    const linkGroup = `${LINK_DOMAIN}/b/${merchant.get('slug')}/${groupTopic}`;
    this.props.dispatch(
      open(GROUP_ORDER_INVITE, {
        linkGroup,
        isCreate,
        onConfirm: () => this.props.history.push(`${pathname}/${groupTopic}${search}`),
      }),
    );
  };

  openPopupMemberList = () => {
    const { merchant } = this.props;
    const { groupTopic } = this.getParams();

    this.props.dispatch(open(GROUP_ORDER_MEMBER_LIST, {
      merchant: merchant.get('slug'), groupTopic,
    }));
  };
}

export default GroupControllerBase;
