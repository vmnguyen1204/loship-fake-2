import { CUSTOM_CONTENT, open } from 'actions/popup';
import { fetchEaterySuggestDishes } from 'actions/recommendation';
import cx from 'classnames';
import BaseComponent from 'components/base';
import DishItem from 'components/pages/merchant/_menu/_dish-item';
import Button from 'components/shared/button';
import CodeBox from 'components/shared/code-box';
import NumberUpDownInput from 'components/shared/number-up-down-input';
import Hint from 'components/statics/hint';
import ImageLazy from 'components/statics/img-lazy';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getMerchant } from 'reducers/merchant';
import { cartUpdateQuantity, getCart, getCartMetadata } from 'reducers/new/cart';
import { getEaterySuggestDishes } from 'reducers/recommendation';
import { getCurrentUser } from 'reducers/user';
import { calculateDishPrice, getOrderLines, getOrderLinesQuantity } from 'utils/cart';
import { addCurrency } from 'utils/format';
import { iife } from 'utils/helper';

if (process.env.BROWSER) {
  require('assets/styles/components/cart.scss');
}

class CartOrder extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      const data = getCart(state, params);
      if (!data) return;

      const cartMetadata = getCartMetadata(state, params);
      if (cartMetadata) {
        data.cartMetadata = cartMetadata;
        const { data: { merchant: cartMerchant } = {} } = cartMetadata;

        if (cartMerchant) {
          const merchant = getMerchant(state, { merchant: cartMerchant });
          if (merchant) data.merchant = merchant.toJS().data;
        }
      }

      const currentUser = getCurrentUser(state);
      if (currentUser) data.currentUser = currentUser.toJS().data;

      const suggestDishes = getEaterySuggestDishes(state, params);
      if (suggestDishes) data.suggestDishes = suggestDishes.toJS().data;

      return data;
    },
    mode: '',
  };

  static contextTypes = { isMobile: PropTypes.bool };

  static propTypes = {
    showHint: PropTypes.bool,
    showAll: PropTypes.bool,
    extraFees: PropTypes.array,
    shippingFee: PropTypes.object,
    keepLastDish: PropTypes.bool,
    mode: PropTypes.string,
    promotionStatus: PropTypes.object,
    discount: PropTypes.number,
  };

  state = {};

  componentDidMount() {
    this.updateSuggestDishes();
  }

  componentDidUpdate() {
    this.updateSuggestDishes();
  }

  renderSuggestDishes = () => {
    if (!this.props.mode.includes('suggest-dishes')) return;
    let { suggestDishes } = this.props;
    if (!suggestDishes || !suggestDishes.length) return null;

    suggestDishes = suggestDishes.slice(0, 5);
    return (
      <div className="suggest-dishes-wrapper scroll-nice">
        <div className="title">
          <b>{this.getString('better_when_order_with')}</b>
        </div>
        <div className="list-view suggest-dishes" style={{ width: 300 * suggestDishes.length }}>
          {suggestDishes.map((dish) => (
            <DishItem
              key={dish.id}
              data={dish}
              groupInfo={{
                isExtraGroupDish: dish.groupDish && dish.groupDish.isExtraGroupDish,
                groupDishId: dish.groupDish && dish.groupDish.id,
              }}
              mode="COMPACT"
              merchant={this.props.merchant}
              imageSize={80}
            />
          ))}
        </div>
      </div>
    );
  };

  renderOrderFees = () => {
    const {
      data,
      cartMetadata = {},
      currentUser = {},
      extraFees = [],
      shippingFee,
      mode,
      eateryId,
      promotionEnable,
      serviceName,
      promotionStatus,
      discount,
    } = this.props;
    const { data: { createdBy, topic: isGroupOrder } = {} } = cartMetadata;

    const quantity = getOrderLinesQuantity(data);
    const isOwner = !isGroupOrder || currentUser.id === createdBy;

    return (
      <div className="list-view">
        {iife(() => {
          if (!isOwner || quantity <= 0) return null;
          if (!shippingFee) return null;
          if (shippingFee.actualShippingFee > shippingFee.shippingFee) {
            return (
              <div className="list-item fee">
                <div>{this.getString('shipping_fee')}</div>
                <div className="price">
                  <i className="lz lz-discount" />
                  <b>{addCurrency(shippingFee.shippingFee)}</b>
                  <span className="old">{addCurrency(shippingFee.actualShippingFee)}</span>
                </div>
              </div>
            );
          }

          return (
            <div className="list-item fee">
              <div>{this.getString('shipping_fee')}</div>
              <div className="price">
                <b>{addCurrency(shippingFee.shippingFee)}</b>
              </div>
            </div>
          );
        })}
        {isOwner && quantity > 0 && extraFees.map((fee) => (
          <div key={extraFees.id} className="list-item fee">
            <div>
              {this.getString(fee.title)} <Hint text={this.getString(fee.description)} />
            </div>
            {addCurrency(fee.value)}
          </div>
        ))}
        {iife(() => {
          if (!isOwner) return null;
          if (quantity === 0) return null;
          if (!mode.includes('codebox')) return null;
          if (mode.includes('codebox-editable')) {
            return (
              <CodeBox
                className="list-item"
                eateryId={eateryId}
                promotionEnable={promotionEnable}
                serviceName={serviceName}
                discount={0}
                hideDiscount
              />
            );
          }

          if (mode.includes('codebox-readonly')) {
            if (discount > 0) {
              return (
                <CodeBox
                  className="list-item codebox"
                  eateryId={eateryId}
                  promotionEnable={promotionEnable}
                  serviceName={serviceName}
                  promotionStatus={promotionStatus}
                  discount={discount}
                />
              );
            }

            if (this.context.isMobile) return null;
            return (
              <Button type="link" className="py-8" onClick={this.handleViewPromoList}>
                <i className="lz lz-discount" />
                <span>{this.getString('apply_promotion')}</span>
              </Button>
            );
          }
        })}
        {this.props.total !== undefined && (
          <div className="list-item cta">
            <div>
              <b>{this.getString(isOwner ? 'total' : 'total_you_pay')}</b>
              <span className="estimated">
                {this.getString('estimated')}
                &nbsp;
                <Hint
                  text={this.getString('total_price_excluded_shipping_fee_and_discounts')}
                  popupWrapperClassName="layer-toaa"
                />
                &nbsp;
              </span>
            </div>
            <div className="highlight">
              <b>≈ {addCurrency(this.props.total)}</b>
            </div>
          </div>
        )}
      </div>
    );
  };

  renderOrderLines = () => {
    const { data, cartMetadata = {}, currentUser = { username: 'anonymous' } } = this.props;
    const { data: { createdBy, topic: isGroupOrder, users = [], lockedUsers = {} } = {} } = cartMetadata;

    const quantity = getOrderLinesQuantity(data);
    const isOwner = !isGroupOrder || currentUser.id === createdBy;
    const [, { data: currentUserOrder = [] } = {}] = Object.entries(data).find(([username]) => currentUser.username === username) || [];

    return (
      <div className="list-view">
        {iife(() => {
          if (currentUserOrder.length === 0) {
            if (this.props.mode.includes('hide-empty')) return null;
            return <div className="list-item empty">{this.getString('cart_empty_hint')}</div>;
          }

          const isLocked = !!lockedUsers[currentUser.username];
          return (
            <div className="list-view cart-user">
              <div className="title">
                <ImageLazy src={currentUser.avatar} size={32} mode="click" />
                <div className="px-8 flex-1">
                  <b>
                    {currentUser.username === 'anonymous'
                      ? this.getString('anonymous')
                      : (currentUser.name && currentUser.name.full) || currentUser.username}
                  </b>{' '}
                  {currentUser.username !== 'anonymous' && this.getString('you')}
                  {createdBy && createdBy === currentUser.id && <i className="lz lz-crown" />}
                </div>
              </div>
              {currentUserOrder.map((line, itemIndex) => this.renderOrderLine({
                line,
                itemIndex,
                user: currentUser,
                isEditable: true,
                isLocked: !isOwner && isLocked,
                canRemove: !this.props.keepLastDish || quantity > 1,
              }))}
            </div>
          );
        })}

        {iife(() => {
          if (/* !createdBy ||  */ users.length < 2) return null;
          if (!this.props.showAll && !this.state.showAll) {
            return (
              <div className="list-item brief-users">
                <div className="title">
                  {this.getString('view_your_friends_order', 'order')}
                  <a onClick={() => this.setState({ showAll: true })}>
                    {this.getString('view_details')} <i className="fas fa-caret-down" />
                  </a>
                </div>
                {users.map((user) => {
                  if (currentUser.id === user.id) return null;
                  return (
                    <ImageLazy key={user.username} src={user.avatar} />
                  );
                })}
              </div>
            );
          }
          return (
            <div className="list-item flex-wrap">
              {Object.entries(data).map(([username, { data: userOrder = [] }]) => {
                if (username === currentUser.username) return null;
                if (userOrder.length === 0) {
                  if (this.props.mode.includes('hide-empty')) return null;
                }

                const user = users.find((u) => u.username === username) || {};
                const isLocked = !!lockedUsers[user.username];
                return (
                  <div key={username} className="list-view cart-user">
                    <div className="title">
                      <ImageLazy src={user.avatar} size={32} mode="click" />
                      <div className="px-8 flex-1">
                        <b>{(user.name && user.name.full) || user.username}</b>
                        {createdBy === user.id && <i className="lz lz-crown" />}
                      </div>
                      <span key="status" className={cx('status', { locked: isLocked })}>
                        {this.getString(isLocked ? 'cart_done' : 'cart_doing', 'order')}
                      </span>
                    </div>
                    {userOrder.map((line, itemIndex) => this.renderOrderLine({
                      line,
                      itemIndex,
                      user,
                      isEditable: isOwner,
                      isLocked: !isOwner && isLocked,
                      canRemove: !this.props.keepLastDish || quantity > 1,
                    }))}
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    );
  };

  renderOrderLine = ({ line, itemIndex, user, isEditable, isLocked, canRemove }) => {
    const price = calculateDishPrice(line, line.customs, line.dishQuantity);
    const rawPrice = calculateDishPrice(line, line.customs, line.dishQuantity, true);

    return (
      <div key={itemIndex} className={cx('list-item order-line', line.soldOut && 'deleted')}>
        {!isEditable && <div className="quantity">x{line.dishQuantity}</div>}
        <div className="main">
          <b>{line.name}</b>
          <div className="sub">
            {line.customs
              .flatMap((custom) => custom.options || [])
              .map((option) => (
                <div key={`custom-${option.optionId}`} className="custom">
                  {option.value}
                  {option.optionQuantity > 1 ? `(x${option.optionQuantity})` : ''}
                </div>
              ))}
            {line.note && <div className="custom mt-4">&quot;{line.note}&quot;</div>}
          </div>
        </div>
        {isEditable && (
          <NumberUpDownInput
            className="action"
            value={line.dishQuantity}
            onChange={(value) => {
              if (isLocked) {
                return this.props.dispatch(
                  open(CUSTOM_CONTENT, {
                    content: this.getString(
                      'your_cart_has_been_locked_after_you_press_the_im_done_button_please_contact_the_team_leader_to_reopen_the_cart_for_you',
                      'order',
                    ),
                    actionClassName: 'bg-red-lozi',
                  }),
                );
              }
              if (!canRemove && value < 0) return;
              this.updateItemQuantity(itemIndex, user, value);
            }}
          />
        )}
        <div className="price">
          {addCurrency(price)}
          {this.props.mode.includes('show-raw-price') && price !== rawPrice && (
            <span className="old">{addCurrency(rawPrice)}</span>
          )}
        </div>
      </div>
    );
  };

  render() {
    const { cartMetadata = {}, currentUser = {}, showHint } = this.props;
    const { data: { createdBy, topic: isGroupOrder } = {} } = cartMetadata;

    return (
      <div className="order-cart-lines">
        {showHint && isGroupOrder && (
          <div className="hint">
            <i className="lz lz-question-square" />
            {this.getString(currentUser.id === createdBy ? 'cart_leader_hint' : 'cart_member_hint')}
          </div>
        )}

        {this.renderOrderLines()}
        {this.renderSuggestDishes()}
        {this.renderOrderFees()}
      </div>
    );
  }

  updateItemQuantity = (index, user, quantity) => {
    this.props.dispatch(
      cartUpdateQuantity({
        ...this.getParams(),
        itemIndex: index,
        user,
        quantity,
      }),
    );
    this.forceUpdate();
  };

  handleViewPromoList = () => {
    const {
      eateryId,
      location: { pathname },
    } = this.props;
    const query = this.getQuery();
    query.pPromo = eateryId;
    query.step = 'confirm-order';

    this.props.history.push(`${pathname}?${qs.stringify(query)}`);
  };

  updateSuggestDishes = () => {
    if (!this.props.mode.includes('suggest-dishes')) return;
    if (this.state.wasFetchedSuggestDishes) return;

    const order = getOrderLines(this.props.data);
    if (order.length === 0) return;

    this.setState({ wasFetchedSuggestDishes: true }, () => {
      this.props.dispatch(fetchEaterySuggestDishes({ ...this.getParams(), dishIds: order.map((line) => line.dishId) }));
    });
  };
}

export default initComponent(CartOrder, withRouter);
