import initComponent from 'lib/initComponent';
import React from 'react';
import { Prompt, withRouter } from 'react-router-dom';
import GroupControllerBase from './group-controller-base';
import Hint from 'components/statics/hint';

class GroupController extends GroupControllerBase {
  render() {
    const { cartMetadata = {}, status, currentUser } = this.props;
    const { data: { createdBy } = {} } = cartMetadata;
    const { groupTopic } = this.getParams();

    if (!groupTopic) {
      return (
        <div key="cart-header" className="cart-header">
          <h3 key="user-card-header">
            {this.getString('cart_of_0', 'group-order', [this.getString('yours', 'group-order')])}
          </h3>
          <div className="group-order-box">
            <a className="btn btn-sm btn-group-order" onClick={this.createGroupOrder}>
              {this.getString('order_group', 'group-order')}
            </a>
          </div>
          <div className="clearfix" />
        </div>
      );
    }

    if (!currentUser) {
      return (
        <div key="cart-header" className="cart-header">
          <h3 key="user-card-header">
            {this.getString('cart_of_0', 'group-order', [this.getString('yours', 'group-order')])}
          </h3>
          <div className="group-order-box">
            <a className="btn btn-sm btn-group-order" onClick={this.openPopupLogin}>
              {this.getString('join_group', 'group-order')}{' '}
              <Hint text={this.getString('login_to_join_group_order')} noHoverHint />
            </a>
          </div>
          <div className="clearfix" />
        </div>
      );
    }

    if (status === 'FAILED') return null;
    if (!createdBy) return this.renderLoading();

    return (
      <div>
        <div key="group-cart-header" className="cart-header">
          <h3 key="user-card-header">{this.getString('cart_of_group', 'group-order')}</h3>
        </div>
        <div key="group-cart-group-action" className="cart-group-action">
          {currentUser.get('id') === createdBy && (
            <a className="btn btn-view-group" onClick={this.openPopupMemberList}>
              {this.getString('manage_members')}
            </a>
          )}
          <a className="btn btn-sm btn-group-order" onClick={this.openPopupInvite()}>
            {this.getString('invite', 'group-order')}
          </a>
          <div className="clearfix" />
        </div>
        <Prompt message={this.routerWillLeave} />
      </div>
    );
  }
}

export default initComponent(GroupController, withRouter);
