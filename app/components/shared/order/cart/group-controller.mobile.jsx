import initComponent from 'lib/initComponent';
import React from 'react';
import { Prompt, withRouter } from 'react-router-dom';
import { getAvatarUrl } from 'utils/image';
import GroupControllerBase from './group-controller-base';

const MAX_MEMBER_SHOW = 4;

class GroupController extends GroupControllerBase {
  render() {
    const { cartMetadata = {}, currentUser } = this.props;
    const { data: { createdBy, users = [] } = {} } = cartMetadata;
    const { groupTopic } = this.getParams();

    if (!groupTopic) {
      return (
        <div className="flex-item group-control-center" onClick={this.createGroupOrder}>
          <i className="lz lz-fw lz-add-user" />
          <div className="flex-1">{this.getString('create_group_order_with_friends', 'order')}</div>
          <i className="fa fa-chevron-right" />
        </div>
      );
    }

    if (!currentUser) {
      return (
        <div className="flex-item group-control-center" onClick={this.openPopupLogin}>
          <i className="lz lz-fw lz-add-user" />
          <div className="flex-1">{this.getString('login_to_join_group_order')}</div>
        </div>
      );
    }

    if (cartMetadata.status === 'ERROR') return null;
    if (!createdBy) return this.renderLoading();
    const groupOwner = users.find((user) => user.id === createdBy) || {};

    return (
      <div className="group-control-center">
        <div className="group-title">
          {currentUser.get('id') === createdBy ? <i className="lz lz-crown" /> : <i className="far fa-grin" />}{' '}
          {this.getString('group_order_of_0', 'order', [
            (groupOwner.name && groupOwner.name.full) || groupOwner.fullname || groupOwner.username,
          ])}
          <span className="btn-leave-group" onClick={this.leaveGroup}>
            {this.getString('leave_group', 'order')}
          </span>
        </div>
        <div className="group-info">
          {this.getString('member', 'order')} {users.length}
          <div className="group-members">
            {users.slice(0, MAX_MEMBER_SHOW).map((member) => (
              <div
                key={member.username}
                className="member-avatar"
                style={{ backgroundImage: `url(${getAvatarUrl(member.avatar, 32)})` }}
              />
            ))}
            {users.length > MAX_MEMBER_SHOW && (
              <span className="member-extra">
                +{users.length - MAX_MEMBER_SHOW <= 9 ? users.length - MAX_MEMBER_SHOW : 9}
              </span>
            )}
          </div>
        </div>
        <div className="group-action">
          <a className="action" onClick={this.openPopupMemberList}>
            <i className="fas fa-list-ul" /> {this.getString('group_member_list', 'order')}
          </a>
          <a className="action" onClick={this.openPopupInvite()}>
            <i className="lz lz-add-user" /> {this.getString('invite_friends', 'order')}
          </a>
        </div>

        <Prompt message={this.routerWillLeave} />
      </div>
    );
  }
}

export default initComponent(GroupController, withRouter);
