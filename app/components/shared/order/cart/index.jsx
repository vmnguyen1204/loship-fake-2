import React, { Component } from 'react';

import PropTypes from 'prop-types';
import Desktop from './cart';
import Mobile from './cart.mobile';

export default class OrderCart extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}
