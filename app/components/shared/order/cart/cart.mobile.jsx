import Button from 'components/shared/button';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { calculateOrderPrice, getOrderLines, getOrderLinesQuantity } from 'utils/cart';
import { addCurrency } from 'utils/format';
import OrderCartBase from './cart-base';

class OrderCart extends OrderCartBase {
  render() {
    if (!this.handleShouldShowCart()) return null;
    if (this.props.mode.includes('icon')) return this.renderIcon();

    const { data, cartMetadata = {}, currentUser = {} } = this.props;
    const { data: { createdBy, topic: isGroupOrder, lockedUsers = [] } = {} } = cartMetadata;

    const isOwner = !isGroupOrder || currentUser.id === createdBy;
    const isLocked = lockedUsers[currentUser.username];
    const orders = isOwner ? data : { [currentUser.username]: data[currentUser.username] };
    const quantity = getOrderLinesQuantity(orders);
    const total = calculateOrderPrice({ order: getOrderLines(orders) });

    return (
      <div className="order-cart">
        {this.renderMQTTStatus()}
        {quantity > 0 && (
          <div className="action">
            {this.props.mode.includes('hide-cart') ? (
              <div className="cart-summary">
                <div className="total">
                  {this.getString('total')} <b>{addCurrency(total)}</b>
                </div>
              </div>
            ) : (
              <div className="cart-summary">
                <Button className="btn-cart" onClick={() => this.toggleViewOrderCartLines(true)}>
                  <i className="lz lz-cart" />
                  <span className="quantity">{String(quantity).padStart(2, 0)}</span>
                </Button>
                <div className="total">
                  <b>{addCurrency(total)}</b>
                  <br />
                  {this.getString('estimated')}
                </div>
              </div>
            )}
            {isOwner ? (
              <Button type="primary" disabled={this.props.loading} onClick={this.handleNextStep}>
                {this.getString('continue')}
              </Button>
            ) : (
              <Button type="primary" disabled={isLocked} onClick={this.handleLockCurrentUser}>
                {this.getString('im_done')}
              </Button>
            )}
          </div>
        )}

        {this.state.viewOrderCartLines && this.renderCartOrderPopup()}
      </div>
    );
  }
}

export default initComponent(OrderCart, withRouter);
