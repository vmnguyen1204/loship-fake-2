import callAPI from 'actions/helpers';
import { fetchMerchant, fetchMerchantMenu } from 'actions/merchant';
import { fetchEventById } from 'actions/newsfeed';
import { fetchUserOrderDetail } from 'actions/order';
import { CONFIRM_POPUP, CUSTOM_CONTENT, GROUP_ORDER_MEMBER_LIST, LOGIN, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Popup from 'components/shared/popup';
import { getMQTT } from 'lib/mqtt';
import PropTypes from 'prop-types';
import React from 'react';
import { getMerchant } from 'reducers/merchant';
import { API, cartClear, cartFetchLocal, cartLockUser, getCart, getCartMetadata } from 'reducers/new/cart';
import { getGlobalAddress } from 'reducers/new/metadata';
import { getOrderNotifications } from 'reducers/notification';
import { getCurrentUser } from 'reducers/user';
import { calculateOrderPrice, getOrderLines, getOrderLinesQuantity } from 'utils/cart';
import { iife } from 'utils/helper';
import { isMerchantReady } from 'utils/merchant';
import { getRoute } from 'utils/routing';
import * as cookies from 'utils/shims/cookie';
import Link from 'utils/shims/link';
import OrderCartLines from './cart-order';

if (process.env.BROWSER) {
  require('assets/styles/components/cart.scss');
}

const WHITELIST_HIDE_GLOBAL_CART = [
  '/:merchantUsername/:groupTopic?',
  '/b/:merchant/:groupTopic?',
  '/:merchantUsername/quanly',
  '/:merchantUsername/menu',
  '/:merchantUsername/cap-nhat-menu',
  '/b/:merchant/quanly',
  '/b/:merchant/menu',
  '/b/:merchant/cap-nhat-menu',
  '/payment',
  '/doi-tac',
  '/g/',
  '/m/buy',
  '/d/buy',
  '/m/tai-khoan',
  '/d/tai-khoan',
  '/tai-khoan/cap-nhat',
  '/tai-khoan/quan-ly-the',
  '/tai-khoan/gioi-thieu',
];

class OrderCartBase extends BaseComponent {
  static defaultProps = {
    actionCreator: cartFetchLocal,
    dataGetter: (state, params = {}) => {
      const data = getCart(state, params);
      if (!data) return;

      const cartMetadata = getCartMetadata(state, params);
      if (cartMetadata) {
        data.cartMetadata = cartMetadata;

        if (params.merchantUsername || params.merchant || params.eateryId) {
          const merchant = getMerchant(state, params);
          if (merchant) data.merchant = merchant.toJS().data;
        } else {
          const { data: { merchant: cartMerchant } = {} } = cartMetadata;
          const merchant = getMerchant(state, { merchant: cartMerchant });
          if (merchant) data.merchant = merchant.toJS().data;
        }
      }

      const currentUser = getCurrentUser(state);
      if (currentUser) data.currentUser = currentUser.toJS().data;

      if (params.groupTopic) {
        const mqttStatus = getMQTT(state);
        if (mqttStatus) data.mqttStatus = mqttStatus.status;
      }

      const orders = getOrderNotifications(state);
      const latestOrder = orders?.get('data')?.[0];
      if (latestOrder) data.latestOrder = latestOrder;

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data.globalAddress = globalAddress.data;

      return data;
    },
    customRender: true,
    mode: '',
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  static propTypes = { mode: PropTypes.string };

  state = { showGroupOrderHint: !cookies.get('alreadyCloseGroupOrderHint') };

  componentDidMount() {
    const { cartMetadata: { data: { merchant: cartMerchant } = {} } = {} } = this.props;
    if (cartMerchant) {
      this.props.dispatch(fetchMerchant({ merchant: cartMerchant }));
      this.props.dispatch(fetchMerchantMenu({ merchant: cartMerchant }));
    }

    this.computeGlobalCartStyle();
    if (this.props.latestOrder) {
      this.handlePartnerOrderCart();
    }
  }

  componentDidUpdate(prevProps) {
    const {
      cartMetadata: { data: { merchant: cartMerchant } = {} } = {},
      location: { pathname, search },
    } = this.props;
    const {
      cartMetadata: { data: { merchant: prevCartMerchant } = {} } = {},
      location: { pathname: prevPathname, search: prevSearch },
    } = prevProps;

    if (cartMerchant !== prevCartMerchant) {
      this.props.dispatch(fetchMerchant({ merchant: cartMerchant }));
      this.props.dispatch(fetchMerchantMenu({ merchant: cartMerchant }));
    }

    if (pathname !== prevPathname || search !== prevSearch) this.computeGlobalCartStyle();
    if (!prevProps.latestOrder && this.props.latestOrder) {
      this.handlePartnerOrderCart();
    }
  }

  renderMQTTStatus = () => {
    if (this.props.mqttStatus !== 'CONNECTING') return null;
    return <div className="mqtt-status">{this.getString('connecting', 'group-order')}</div>;
  };

  renderAction = ({ source } = {}) => {
    const { data, cartMetadata = {}, currentUser = {}, merchant = {} } = this.props;
    const { data: { createdBy, topic: isGroupOrder, lockedUsers = {} } = {} } = cartMetadata;

    const isOwner = !isGroupOrder || currentUser.id === createdBy;
    const isLocked = lockedUsers[currentUser.username];
    const orders = isOwner ? data : { [currentUser.username]: data[currentUser.username] };
    const quantity = getOrderLinesQuantity(orders);

    return (
      <div className="action">
        {isOwner ? (
          <Button type="primary" className="btn-cart-next" disabled={quantity === 0} onClick={this.handleNextStep}>
            {this.getString('continue')}
          </Button>
        ) : (
          <Button type="primary" className="btn-cart-next" disabled={isLocked} onClick={this.handleLockCurrentUser}>
            {this.getString('im_done')}
          </Button>
        )}

        {iife(() => {
          if (this.context.isMobile || source === 'popup') return null;

          if (isOwner && isGroupOrder) { return <p className="action-info">{this.getString('cart_leader_continue_hint', 'eatery')}</p>; }
          if (merchant.orderCount) {
            return (
              <p className="action-info">
                {this.getString('0_orders_successful_placed', 'eatery', [merchant.orderCount])}
              </p>
            );
          }
          return null;
        })}
      </div>
    );
  };

  renderCartOrderPopup = () => {
    const { data, cartMetadata = {}, currentUser = {}, merchant = {} } = this.props;
    const { data: { createdBy, topic: isGroupOrder } = {} } = cartMetadata;

    const isOwner = !isGroupOrder || currentUser.id === createdBy;
    const orders = isOwner ? data : { [currentUser.username]: data[currentUser.username] };
    const total = calculateOrderPrice({ order: getOrderLines(orders) });

    return (
      <Popup
        className={cx('generic-popup confirm-popup order-cart-popup', { fullscreen: this.context.isMobile })}
        handleClose={() => this.toggleViewOrderCartLines(false)}
        animationStack={2}
      >
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent')} onClick={() => this.toggleViewOrderCartLines(false)}>
            <i className="lz lz-close" />
          </Button>
          {this.getString('cart')}
        </div>
        <div className="content">
          {!this.props.mode.includes('merchant') && (
            <Link
              className="merchant-info"
              to={`/${merchant.username || `b/${merchant.slug}`}`}
              onClick={() => this.toggleViewOrderCartLines(false)}
            >
              <div className="content">
                <b>{merchant.name}</b>
                <p>{merchant.address}</p>
              </div>
              <div className="action">
                <i className="lz lz-arrow-head-right" />
              </div>
            </Link>
          )}
          <OrderCartLines total={total} toNextStep={this.handleNextStep} />
          {this.renderAction({ source: 'popup' })}
        </div>
      </Popup>
    );
  };

  renderIcon = () => {
    const { data, cartMetadata = {}, currentUser = {} } = this.props;
    const { data: { createdBy, topic: isGroupOrder } = {} } = cartMetadata;

    const isOwner = !isGroupOrder || currentUser.id === createdBy;
    const orders = isOwner ? data : { [currentUser.username]: data[currentUser.username] };
    const quantity = getOrderLinesQuantity(orders);

    return (
      <div className={cx('order-cart-icon', this.state.globalCartClassName)} style={this.state.globalCartStyle}>
        <Button className="btn-cart" onClick={() => this.toggleViewOrderCartLines(true)}>
          <i className="lz lz-cart" />
          <span className="quantity">{quantity}</span>
        </Button>
        {this.state.viewOrderCartLines && this.renderCartOrderPopup()}
      </div>
    );
  };

  handleNextStep = (step) => {
    const { cartMetadata = {}, merchant, currentUser } = this.props;
    const { data: { createdBy, users = [], lockedUsers = [], topic: isGroupOrder } = {} } = cartMetadata;

    if (!isMerchantReady(
      merchant,
      { dispatch: this.props.dispatch, getString: this.getString, history: this.props.history })
    ) return;

    if (!currentUser) {
      return this.props.dispatch(
        open(LOGIN, {
          callback: () => {
            this.handleNextStep(step);
          },
        }),
      );
    }

    if (!this.hasAtLeast1MainDish(isGroupOrder && currentUser.id !== createdBy)) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('you_havent_selected_a_main_course'),
          content: this.getString(
            'some_locations_wont_sell_just_extra_dishes_you_need_to_select_at_least_1_main_dish_on_the_menu_before_continuing',
          ),
        }),
      );
    }

    const isAllMemberLocked = users
      .filter((user) => user.username !== currentUser.username)
      .every((user) => lockedUsers && lockedUsers[user.username]);
    if (!isAllMemberLocked) {
      return this.props.dispatch(
        open(CONFIRM_POPUP, {
          content: () => (
            <div className="popup-group-info">
              <i className="lz lz-info-square" />
              <p>{this.getString('cart_continue_notice')}</p>
            </div>
          ),
          actionClassName: this.context.isMobile && 'flex-column-reverse',
          confirmBtn: {
            text: this.getString('view_group_members'),
            className: this.context.isMobile && 'btn-fw',
          },
          cancelBtn: { type: 'link', className: 'text-dark-gray py-8 px-8' },
          onConfirm: this.openPopupMemberList,
        }),
      );
    }

    if (typeof this.props.toNextStep !== 'function') {
      this.toggleViewOrderCartLines(false);
      this.props.history.push(`/${merchant.username || `b/${merchant.slug}`}?step=address`);
      return;
    }

    this.toggleViewOrderCartLines(false);
    this.props.toNextStep(step);
  };

  handleLockCurrentUser = async () => {
    const params = this.getParams();

    if (!this.hasAtLeast1MainDish(true)) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('you_havent_selected_a_main_course'),
          content: this.getString(
            'some_locations_wont_sell_just_extra_dishes_you_need_to_select_at_least_1_main_dish_on_the_menu_before_continuing',
          ),
        }),
      );
    }

    this.props.dispatch(cartLockUser({ ...this.props.params, lock: true }));
    callAPI('put', {
      url: API.LOCK_MEMBER,
      params: { groupTopic: params.groupTopic },
      content: { status: 'lock' },
    });
  };

  hasAtLeast1MainDish = (ignoreEmpty = false) => {
    const { data, currentUser } = this.props;

    const orderLines = getOrderLines({ [currentUser.username]: data[currentUser.username] });
    if (ignoreEmpty && (!Array.isArray(orderLines) || orderLines.length === 0)) return true; // Ignore empty cart
    return orderLines.some((x) => !x.isExtraGroupDish);
  }

  openPopupMemberList = () => {
    const { merchant, groupTopic } = this.props.params;

    this.props.dispatch(
      open(GROUP_ORDER_MEMBER_LIST, {
        merchant,
        groupTopic,
        showCTA: true,
        toNextStep: this.handleNextStep,
      }),
    );
  };

  handleShouldShowCart = () => {
    const { match } = (typeof window !== 'undefined' && getRoute(null, window.location.pathname)) || {};
    const { mode } = this.props;

    const { data, cartMetadata = {}, currentUser = {} } = this.props;
    const { data: { createdBy, merchant: cartMerchant, topic: isGroupOrder } = {} } = cartMetadata;
    const { merchant } = this.getParams();

    const isOwner = !isGroupOrder || currentUser.id === createdBy;
    const orders = isOwner ? data : { [currentUser.username]: data[currentUser.username] };
    const quantity = getOrderLinesQuantity(orders);

    if (['merchant'].some((m) => mode.includes(m))) {
      if (!this.context.isMobile) return true;
      if ((!merchant || merchant === cartMerchant) && quantity > 0) return true;
      return this.shouldShowCartWhenEmpty;
    }

    if (!match) return false;
    if (WHITELIST_HIDE_GLOBAL_CART.some(
      (path) => match.path === path || match.url.includes(path),
    )) return false;
    if (quantity === 0 && !this.shouldShowCartWhenEmpty) {
      this.toggleViewOrderCartLines(false);
      return false;
    } else {
      this.shouldShowCartWhenEmpty = true;
    }

    return true;
  };

  computeGlobalCartStyle = () => {
    const { match } = (typeof window !== 'undefined' && getRoute(null, window.location.pathname)) || {};
    const query = this.getQuery();

    if (!match) return this.setState({ globalCartStyle: {}, globalCartClassName: '' });
    if (match.url.includes('/su-kien')) {
      if (this.context.isMobile) {
        return this.props.dispatch(
          fetchEventById({
            ...match.params,
            globalAddress: this.props.globalAddress,
            callback: (data) => {
              if (data.type === 'shipping_service') return this.setState({ globalCartStyle: { bottom: 80 } });
            },
          }),
        );
      }
    }
    if (match.url.includes('/tai-khoan/don-hang') && match.params.order) {
      if (this.context.isMobile) {
        if (query.pCancel) return this.setState({ globalCartStyle: { display: 'none' } });
        return this.props.dispatch(
          fetchUserOrderDetail(
            {
              ...match.params,
              soft: true,
              callback: (res) => {
                const data = res.body && res.body.data;
                if (!data) return;

                const privileges = data.privileges || [];
                if (privileges.some((privilege) => privilege.includes('edit_all'))) {
                  if (data.status === 'cancel') return this.setState({ globalCartStyle: { bottom: 80 } });
                  if (data.status === 'done') return this.setState({ globalCartStyle: { bottom: 100 } });
                  return this.setState({ globalCartStyle: { bottom: 60 } });
                }
              },
            },
            this.getQuery(),
          ),
        );
      }
    }
    if (match.url.includes('/tim-kiem')) return this.setState({ globalCartClassName: 'layer-lv0' });
    if (match.url.includes('/tai-khoan/cua-hang-yeu-thich')) return this.setState({ globalCartClassName: 'layer-lv0' });

    this.setState({ globalCartStyle: {}, globalCartClassName: '' });
  };

  toggleViewOrderCartLines = (isViewing) => {
    if (this.state.viewOrderCartLines === isViewing) return;

    this.shouldShowCartWhenEmpty = isViewing;
    this.setState({ viewOrderCartLines: isViewing });

    if (this.context.isMobile) {
      if (isViewing) {
        document.body.style.overflow = 'hidden';
        document.body.style.paddingRight = 8;
      } else {
        document.body.style.overflow = '';
        document.body.style.paddingRight = '';
      }
    }
  };

  handlePartnerOrderCart = () => {
    if (!this.props.latestOrder) return;
    if (this.context.partnerName === 'none') return;

    const latestOrderCode = localStorage.getItem('latestOrderCode');
    const latestGroupTopic = localStorage.getItem('latestGroupTopic') || undefined;
    if (!latestOrderCode) return;

    localStorage.removeItem('latestOrderCode');
    localStorage.removeItem('latestGroupTopic');

    if (this.props.latestOrder.code !== latestOrderCode) {
      this.props.dispatch(cartClear({ groupTopic: latestGroupTopic }));
    }
  }
}

export default OrderCartBase;
