import GroupController from 'components/shared/order/cart/group-controller';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { calculateOrderPrice, getOrderLines } from 'utils/cart';
import * as cookies from 'utils/shims/cookie';
import OrderCartBase from './cart-base';
import OrderCartLines from './cart-order';

class OrderCart extends OrderCartBase {
  render() {
    if (!this.handleShouldShowCart()) return null;
    if (this.props.mode.includes('icon')) return this.renderIcon();

    const { data, cartMetadata = {}, currentUser = {} } = this.props;
    const { data: { createdBy, topic: isGroupOrder } = {} } = cartMetadata;

    const isOwner = !isGroupOrder || currentUser.id === createdBy;
    const orders = isOwner ? data : { [currentUser.username]: data[currentUser.username] };
    const total = calculateOrderPrice({ order: getOrderLines(orders) });

    return (
      <div className="order-cart">
        {!isGroupOrder && this.state.showGroupOrderHint && (
          <div className="hint-popup">
            <a className="close" onClick={this.closeGroupOrderHint}>
              <i className="lz lz-close" />
            </a>
            <span>{this.getString('order_together_cheaper_and_faster')}</span>
          </div>
        )}

        <div className="mc-cart" id="mc-cart">
          {this.renderMQTTStatus()}
          <GroupController />

          <OrderCartLines
            extraFees={this.props.extraFees}
            total={total}
            mode={this.props.mode}
            eateryId={this.props.eateryId}
            promotionEnable={this.props.promotionEnable}
            serviceName={this.props.serviceName}
          />
        </div>

        {this.renderAction()}
      </div>
    );
  }

  closeGroupOrderHint = () => {
    cookies.set('alreadyCloseGroupOrderHint', true, 43200);
    this.setState({ showGroupOrderHint: false });
  };
}

export default initComponent(OrderCart, withRouter);
