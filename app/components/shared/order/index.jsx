import { fetchUserOrderDetail } from 'actions/order';
import { CUSTOM_CONTENT, open } from 'actions/popup';
import { fetchShippingAddresses } from 'actions/user';
import BaseComponent from 'components/base';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import { getBlocksItemOffers, getItemById } from 'reducers/buy';
import { getMerchant } from 'reducers/merchant';
import { cartAddItem, getCart, getCartMetadata, getCartMetadataKey } from 'reducers/new/cart';
import { getCurrentCity, getGlobalAddress } from 'reducers/new/metadata';
import { getLosendConfigs } from 'reducers/order';
import { getCurrentUser, getShippingAddresses } from 'reducers/user';
import { getOrderLineData } from 'utils/cart';
import { PARTNER_CONFIGS } from 'utils/json/payment';
import * as cookies from 'utils/shims/cookie';
import localStorage from 'utils/shims/localStorage';

if (process.env.BROWSER) {
  require('assets/styles/pages/order.scss');
  require('assets/styles/pages/order-mobile.scss');
}

/** ORDER STEP */
export const STEP_SEARCH = -1;
export const STEP_INFO = 1;
export const STEP_ADDRESS = 2;
export const STEP_CONFIRM = 3;

// For Lozi-order
export const STEP_VIEW_DETAIL = 4;

// For Losend-order
export const STEP_ADDRESS_FROM = 1;

export default class OrderBase extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const cartMetadata = getCartMetadata(state, params);
      if (cartMetadata) data = data.set('cartMetadata', cartMetadata);

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      const currentCity = getCurrentCity(state);
      if (currentCity) data = data.set('currentCity', currentCity);

      const shippingAddresses = getShippingAddresses(state);
      if (shippingAddresses) data = data.set('shippingAddresses', shippingAddresses.toJS().data);

      /** Getter for Losend-order */
      const losendConfigs = getLosendConfigs(state);
      if (losendConfigs) data = data.set('losendConfigs', losendConfigs.get('data'));

      /** Getter for Lozi-order */
      const blockItemOffers = getBlocksItemOffers(state);
      if (blockItemOffers) {
        data = data.set('blockItemOffers', blockItemOffers.get('data'));
        data = data.set('merchant', new Map(blockItemOffers.getIn(['data', 'eatery'])));

        if (params.blockOfferItemId) {
          const eatery = blockItemOffers.getIn(['data', 'eatery']) || {};
          const cart = getCart(state, { merchant: eatery.slug });
          if (cart) data = data.merge(new Map(cart));
        }
      }

      params.itemId = params.itemId || params.pItem;
      const currentItem = getItemById(state, params);
      if (currentItem) {
        data = data.set('currentItem', currentItem.get('data'));
        const merchant = currentItem.getIn(['data', 'eatery']) || {};
        params.merchant = merchant.slug;
        params.merchantUsername = merchant.username;
      }

      if (params.merchant || params.merchantUsername) {
        const merchant = getMerchant(state, params);
        if (merchant) data = data.set('merchant', merchant.get('data'));
      }

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      return data;
    },
    customRender: true,
    queryParams: ['pItem'],
  };

  static contextTypes = {
    partnerName: PropTypes.string,
    isMobile: PropTypes.bool,
    setGlobalAddressAuto: PropTypes.func.isRequired,
  };

  constructor(props, context, initialState = {}) {
    super(props, context);

    this.state = {
      ...initialState,
      payment: { method: 'cod' },
    };
  }

  componentDidMount() {
    const query = this.getQuery();

    /** handle payment & partnerConfig */
    let paymentInfo = { method: 'cod' };
    if (!!localStorage && !!localStorage.getItem('lastPaymentInfo')) {
      try {
        paymentInfo = JSON.parse(localStorage.getItem('lastPaymentInfo'));
      } catch (e) {
        //
      }
    }
    if (!!query.paymentMethod && query.paymentMethod !== '') {
      paymentInfo.method = query.paymentMethod;
    }

    let partnerConfig = null;
    if (this.context.partnerName !== 'none') {
      partnerConfig = PARTNER_CONFIGS[this.context.partnerName];
      if (partnerConfig && partnerConfig.paymentMethod) paymentInfo.method = partnerConfig.paymentMethod;
    }
    this.updatePaymentInfo(
      paymentInfo.method,
      { bank: paymentInfo.bank, card: paymentInfo.card, type: paymentInfo.type },
      true,
    );

    this.handleFetchPromotionFromLocal();

    /** accessToken use in Lozi-order, it won't affect other type */
    const accessToken = cookies.get('tempData');
    if (this.props.currentUser && !this.props.shippingAddresses) {
      this.props.dispatch(fetchShippingAddresses({ accessToken }));
    }

    /** handle payment unsuccessful */
    if (query.step === 'confirm-order') this.setState({ step: STEP_CONFIRM });

    /** handle reorder */
    if (query.status === 're-order' && !!query.code) {
      this.reOrder(query.code, partnerConfig && partnerConfig.paymentMethod);
    }
  }

  componentDidUpdate() {
    if (this.props.currentUser && !this.props.shippingAddresses) {
      /** accessToken use in Lozi-order, it won't affect other type */
      const accessToken = cookies.get('tempData');
      this.props.dispatch(fetchShippingAddresses({ accessToken }));
    }

    if (this.props.merchant) {
      this.handleFetchPromotionFromLocal();
    }
  }

  updateShippingInfo = (info, isSender = false, callback = undefined) => {
    const { currentUser } = this.props;
    const targetInfo = (() => {
      if (!currentUser) return info;
      if (this.state.serviceName === 'losend' || isSender) return info;

      return {
        ...info,
        customerName: currentUser.get('fullname'),
        customerPhone: currentUser.get('phoneNumber'),
        countryCode: currentUser.get('countryCode'),
      };
    })();

    return this.setState(isSender ? { senderInfo: targetInfo } : { receiverInfo: targetInfo }, callback);
  };

  updateDistances = (distances) => {
    if (!distances) return;
    const distanceText = Math.floor(distances.value / 100) / 10 + ' km';
    distances.text = distanceText;

    this.setState((state) => ({ ...state, distances }));
  };

  updatePaymentInfo = (paymentMethod = 'cod', { bank, card, type, saveCard } = {}) => {
    this.setState((state) => ({ payment: { ...state.payment, method: paymentMethod, bank, card, type, saveCard } }));
  };

  clearSrcNotePhoto = () => {
    this.setState({ notePhoto: undefined });
  };

  /** partnerPaymentMethod will override payment method from src order.
   * has partnerPaymentMethod === open in partner's webview
   */
  reOrder = (code, partnerPaymentMethod) => {
    const { currentUser } = this.props;
    this.props.dispatch(
      fetchUserOrderDetail({
        order: code,
        soft: true,
        callback: (res) => {
          const data = res && res.body && res.body.data;
          if (!data) {
            return this.props.dispatch(
              open(CUSTOM_CONTENT, {
                title: this.getString('something_went_wrong', 'eatery'),
                content: this.getString('theres_unexpected_problem_prevent_us_to_process_your_action_sorry', 'eatery'),
              }),
            );
          }

          const storedData = (() => {
            const localOrder = localStorage.getItem(`${data.code}.reorder`);
            if (!localOrder) return null;

            localStorage.removeItem(`${data.code}.reorder`);
            return JSON.parse(localOrder);
          })();
          const { lineGroups = [], lines = [], eatery = {}, serviceName } = data;
          const cartMetadataKey = getCartMetadataKey({ groupTopic: this.getParams().groupTopic });

          lineGroups.forEach((lineGroup) => {
            const key = `${cartMetadataKey}.${lineGroup.taggedUser && lineGroup.taggedUser.username}`;
            localStorage.removeItem(key);
          });
          (storedData || lines).forEach((line) => {
            const lineGroup = lineGroups.find((l) => {
              return (l.lineIds || []).some((lineId) => lineId === line.id);
            });

            const user = (lineGroup && lineGroup.taggedUser) || {};
            const lineData = getOrderLineData({ ...line, dishQuantity: line.quantity }, user.id);
            this.props.dispatch({
              type: cartAddItem.SUCCESS.type,
              payload: {
                key: `${cartMetadataKey}.${user.username}`,
                user,
                cartMetadataKey,
                merchant: eatery.slug,
                serviceName,
                groupTopic: this.getParams().groupTopic,
                data: lineData,
              },
              lock: currentUser.get('id') !== user.id,
            });
          });

          const destination = data.routes && data.routes.destination;
          const sourceOrderData = {
            note: data.note,
            notePhoto: data.notePhoto,
            payment: (() => {
              const payment = {
                card: data.paymentCard,
                bank: data.paymentBank,
              };

              if (partnerPaymentMethod) payment.method = partnerPaymentMethod;
              else if (data.paymentMethod === 'momopayinapp') payment.method = 'momopay';
              else payment.method = data.paymentMethod;

              return payment;
            })(),
            srcServiceName: data.serviceName || 'loship',
            receiverInfo: destination && {
              address: destination.address,
              cityId: destination.cityId,
              customerName: destination.name,
              customerPhone: destination.phoneNumber,
              districtId: destination.districtId,
              lat: destination.lat,
              lng: destination.lng,
              suggestedAddress: destination.suggestedAddress,
              suggestedDistrictId: destination.suggestedDistrictId,
              countryCode: destination.countryCode,
            },
          };

          this.setState({
            step: STEP_CONFIRM,
            sourceOrderId: data.id,
            ...sourceOrderData,
          });

          this.updateDistances({ value: data.distance });
        },
        callbackFailure: () => {
          this.props.history.push(this.props.location.pathname);
        },
      }),
    );
  };

  changeStep = (step, force) => {
    const { serviceName, senderInfo = {}, receiverInfo = {} } = this.state;
    const hasSenderInfo = senderInfo.lat && senderInfo.lng && senderInfo.address;
    const hasReceiverInfo = receiverInfo.lat && receiverInfo.lng && receiverInfo.address;

    if (force) return this.setState({ step });

    let overideStep = step;

    if (serviceName === 'losend') {
      if (overideStep === STEP_ADDRESS_FROM && hasSenderInfo) overideStep = STEP_ADDRESS;
    } else if (serviceName === 'lozi') {
      if (overideStep === STEP_ADDRESS && hasReceiverInfo) overideStep = STEP_CONFIRM;
    } else if (overideStep === STEP_ADDRESS && hasReceiverInfo) overideStep = STEP_CONFIRM;

    this.setState({ step: overideStep });
  };

  computeConfirmData = () => {
    const { merchant, currentUser } = this.props;
    const {
      step,
      senderInfo,
      receiverInfo,
      sourceOrderId,
      distances,
      payment,
      note: srcNote,
      notePhoto: srcNotePhoto,
    } = this.state;

    let partnerConfig = null;
    if (this.context.partnerName !== 'none') {
      partnerConfig = PARTNER_CONFIGS[this.context.partnerName];
    }

    return {
      params: this.getParams(),
      eateryId: merchant && merchant.get('id'),
      step,
      stepLabels: this.stepLabels,
      changeStep: this.changeStep,
      senderInfo,
      receiverInfo,
      updateShippingInfo: this.updateShippingInfo,
      distances,
      updateDistances: this.updateDistances,
      merchant,
      currentUser,
      payment,
      updatePaymentInfo: this.updatePaymentInfo,
      sourceOrderId,
      srcNote,
      srcNotePhoto,
      clearSrcNotePhoto: this.clearSrcNotePhoto,
      partnerConfig,
    };
  };

  handleFetchPromotionFromLocal = () => {
    const { merchant } = this.props;
    const params = this.getParams(this.props);
    if (!merchant || !merchant.get('username')) return;

    const localPromoKey = `${merchant.get('username')}.${params.groupTopic || '000000'}.lastPromotion`;

    if (!!localStorage && !!localStorage.getItem(localPromoKey)) {
      try {
        const promotion = JSON.parse(localStorage.getItem(localPromoKey));

        // validate local promotion
        if (!promotion || !promotion.code || !promotion.value || !promotion.promotionType) return;

        this.props.dispatch({ type: 'PROMOTION_FETCH', data: promotion });
        localStorage.removeItem(localPromoKey);
      } catch (e) {
        //
      }
    }
  };
}
