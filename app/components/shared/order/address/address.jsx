import Button from 'components/shared/button';
import StepProcess from 'components/shared/step-process/order';
import initComponent from 'lib/initComponent';
import React from 'react';
import AddressPageBase from './address-base';

class OrderAddressPage extends AddressPageBase {
  renderAction = () => {
    const shippingAddress = this.state.shippingAddress;
    return (
      <div className="address-content-action">
        <Button type="link" className="text-gray" onClick={this.props.goBack}>
          <i className="lz lz-light-arrow-left" />
          <span>{this.getString(this.props.goBackText || 'go_back', 'order')}</span>
        </Button>
        {this.state.step !== 'input' && (
          <Button
            type="primary"
            disabled={!shippingAddress?.lat || !shippingAddress?.lng || !shippingAddress?.address}
            onClick={this.confirmShippingAddress}
          >
            {this.getString(this.props.confirmText || 'delivered_to_this_address', 'shippingAddress')}
          </Button>
        )}
      </div>
    );
  };

  render() {
    return (
      <div className="order-step-address">
        {this.props.step && (
          <StepProcess step={this.props.step} labels={this.props.stepLabels} />
        )}

        <div className="order-step-address-container">
          {this.renderMap()}
          {this.renderAddressPanel()}
        </div>
      </div>

    );
  }
}

export default initComponent(OrderAddressPage);
