import Button from 'components/shared/button';
import initComponent from 'lib/initComponent';
import React from 'react';
import AddressPageBase from './address-base';

class OrderAddressPage extends AddressPageBase {
  renderAction = () => {
    if (this.state.step === 'input') return null;
    const shippingAddress = this.state.shippingAddress;

    return (
      <Button
        type="primary"
        fw
        className="btn-cta"
        disabled={!shippingAddress?.lat || !shippingAddress?.lng || !shippingAddress?.address}
        onClick={this.confirmShippingAddress}
      >
        {this.getString(this.props.confirmText || 'delivered_to_this_address', 'shippingAddress')}
      </Button>
    );
  };

  render() {
    if (typeof window === 'undefined') return null;

    let resizeHeight = this.state.resizeHeight || window.innerHeight / 2;
    if (resizeHeight > window.innerHeight - 32) resizeHeight = window.innerHeight - 32;

    const mapMinHeight = window.innerHeight - resizeHeight;

    return (
      <div className="order-step-address">
        {this.renderMap({ minHeight: Math.max(mapMinHeight, 310) })}
        {this.renderAddressPanel()}
      </div>
    );
  }
}

export default initComponent(OrderAddressPage);
