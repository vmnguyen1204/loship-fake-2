import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Desktop from './address';
import Mobile from './address.mobile';

export const COORD_PRECISION = 6;

export default class OrderStepAddress extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}
