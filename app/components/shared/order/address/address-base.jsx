import cx from 'classnames';
import BaseComponent from 'components/base';
import Gmap from 'components/shared/gmap';
import PropTypes from 'prop-types';
import React from 'react';
import { Rnd as ResizeContainer } from 'react-rnd';
import { getGoogleMap } from 'services/google-map';
import { getCityId, getDistrictId } from 'utils/address';
import { getDistanceBirdFlies } from 'utils/distance';
import { roundNumber } from 'utils/format';
import { debouncer, iife } from 'utils/helper';

export const COORD_PRECISION = 6;
export const RESIZE_MIN_HEIGHT = 200;

export default class AddressFormBase extends BaseComponent {
  static defaultProps = { customRender: true };

  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
    geoLocation: PropTypes.object,
  };

  constructor(props) {
    super(props);

    const { shippingAddress, merchantAddress } = props;
    const { lat = 0, lng = 0 } = shippingAddress || {};
    const { lat: sLat, lng: sLng } = merchantAddress || {};
    const defaultCenter = { lat: sLat || 0, lng: sLng || 0 };

    this.initialState = {
      step: 'input', // input - confirm - manual
      address: '',
      center: lat && lng ? { lat, lng } : defaultCenter,
      resize: { max: false, min: false, alreadyMax: false, alreadyMin: false },
    };

    this.state = { ...this.initialState };
  }

  componentDidMount() {
    new Promise((resolve) => getGoogleMap().then((map) => {
      resolve(map);
      this.googleMap = map;
      this.fetchGeoLocation();
      this.handleSetWindowHeightManual();
    }));
  }

  componentDidUpdate(prevProps) {
    // #hackforsafari
    this.mapContainerRef?.classList.add('unset');
    setTimeout(() => { this.mapContainerRef?.classList.remove('unset'); }, 0);

    if (this.props.merchantAddress !== prevProps.merchantAddress) {
      const { lat, lng } = this.props.merchantAddress || {};

      this.setState({
        ...this.initialState,
        center: { lat: lat || 0, lng: lng || 0 },
      });
    }
  }

  renderMap = (style = {}) => {
    const { step } = this.state;
    const centerMode = (() => {
      if (step === 'confirm') return 'static';
      if (step === 'manual') return 'interactive';
      return 'hidden';
    })();

    return (
      <div className="map" ref={(ref) => {this.mapContainerRef = ref;}}>
        <Gmap
          defaultCenter={this.props.merchantAddress}
          center={{ position: this.state.center, hideInfo: step === 'input' }}
          mode={centerMode}
          onCenterChange={(center) => this.updateCenter(center)}
          markerText={this.props.markerText}
          style={{ ...style, transition: 'all 0.2s' }}
        />
        {this.context.isMobile && (
          <a className="btn-back" onClick={this.props.goBack}>
            <i className="fas fa-arrow-left" />
          </a>
        )}
      </div>
    );
  };

  renderAddressPanel = () => {
    if (typeof window === 'undefined') return null;

    const { step, address, shippingAddress } = this.state;

    let resizeHeight = this.state.resizeHeight || window.innerHeight / 2;
    if (resizeHeight > window.innerHeight) resizeHeight = window.innerHeight;

    const containerMaxHeight = resizeHeight - 120;
    const resizable = step === 'input';
    const positionY = window.innerHeight - resizeHeight;

    const [Component, props] = iife(() => {
      if (this.context.isMobile) {
        return [ResizeContainer, {
          ref: (ref) => { this.resizeContainer = ref; },
          className: cx('address-content', step, { transition: this.state.resizeTransition, resizable }),
          size: { width: '100%', height: resizeHeight },
          position: { x: 0, y: positionY },
          disableDragging: true,
          onResize: (e, direction, ref) => {
            const newHeight = parseFloat(ref.style.height);
            if (Math.abs(this.state.resizeHeight - newHeight) > 10) {
              this.setState({ resizeHeight: parseFloat(ref.style.height) });
            }
          },
          enableResizing: {
            top: resizable, right: false, bottom: false, left: false,
            topRight: false, bottomRight: false, bottomLeft: false, topLeft: false,
          },
          resizeHandleClasses: { top: resizable && 'resizer-top' },
          maxHeight: window.innerHeight,
          minHeight: !resizable ? RESIZE_MIN_HEIGHT - 20 : RESIZE_MIN_HEIGHT,
        }];
      }

      return ['div', { className: 'address-content' }];
    });

    return (
      <Component {...props} >
        <h2 className="step-title">
          {iife(() => {
            // TODO: losend / loxe
            if (step === 'confirm') return this.getString('confirm_shipping_address', 'shippingAddress');
            if (step === 'manual') return this.getString('choose_shipping_address_on_map', 'shippingAddress');
            return this.getString('choose_shipping_address', 'shippingAddress');
          })}
        </h2>

        {step === 'input' && (
          <div className="address-content-input-wrapper">
            <i className="lz lz-fw lz-marker text-red" />
            <input
              ref={this.handleRef}
              className="address-content-input"
              value={address}
              onChange={this.handleInputChange}
              onFocus={this.handleInputFocus}
              placeholder={this.getString('input_shipping_address')}
            />
            {!!address && <i className="lz lz-close" onClick={this.handleClearInput} />}
          </div>
        )}

        {step === 'input' && (
          <div
            className="scroll-container scroll-nice"
            style={{ maxHeight: containerMaxHeight }}
            onTouchStart={this.handleTouchStart}
            onTouchMove={this.handleTouchMove}
          >
            {this.renderAddressItems()}
          </div>
        )}

        {step !== 'input' && shippingAddress && (
          <div className="address-content-results">
            <div key={shippingAddress.id} className="address-content-result">
              <i className="lz lz-fw lz-marker text-red" />
              {step === 'manual' ? (
                <div className="content">
                  <h4>
                    <span>{this.state.savedAddress}</span>
                    <a onClick={this.handleInputFocus}>{this.getString('change')}</a>
                  </h4>
                  <div className="address">{this.getString('move_the_map_to_your_exact_location')}</div>
                </div>
              ) : (
                <div className="content">
                  <h4>
                    <span>
                      {iife(() => {
                        let placeName = shippingAddress.placeName;
                        if (!placeName) placeName = shippingAddress.address?.split(',')[0];
                        return placeName;
                      })}
                    </span>
                    <a onClick={this.handleInputFocus}>{this.getString('change')}</a>
                  </h4>
                  <div className="address">{shippingAddress.address}</div>
                </div>
              )}
            </div>
          </div>
        )}

        {this.renderAction()}
      </Component>
    );
  }

  renderAddressItems = () => {
    const { shippingAddresses = [] } = this.props;
    const { searchCandidates, geoLocation, isSearching, address } = this.state;

    const invalidSearching = !isSearching && !!address && (!Array.isArray(searchCandidates) || searchCandidates.length === 0);

    const MapCandidate = (
      <div className="address-content-result" onClick={this.handleSelectCandidate('manual', { address })}>
        <i className="lz lz-fw lz-map" />
        <div className="content">
          <h4>
            <span>{address}</span>
          </h4>
          <div className="address">{this.getString('choose_shipping_address_on_map')}</div>
        </div>
      </div>
    );

    const GeoLocationCandidate = (() => {
      const { hideCurrentPosition } = this.props;
      if (!geoLocation || !!hideCurrentPosition) return null;
      return (
        <div className="address-content-result separated" onClick={this.handleSelectCandidate('confirm', geoLocation)}>
          <i className="lz lz-fw lz-gps" />
          <div className="content">
            <h4>
              <span>{this.getString('current_location', 'shippingAddress')}</span>
            </h4>
            <div className="address">{geoLocation.address}</div>
          </div>
        </div>
      );
    })();

    if (isSearching) {
      return (
        <div className="address-content-results">
          {[...Array(4).keys()].map((key) => (
            <div key={key} className="address-content-result shimmer-animation">
              <div className="icon shimmer-icon" />
              <div className="content">
                <div className="shimmer-text w-40 round" />
                <div className="shimmer-text round" />
              </div>
            </div>
          ))}
        </div>
      );
    }
    if (invalidSearching) {
      return (
        <div className="address-content-results">
          {MapCandidate}
        </div>
      );
    }

    if (Array.isArray(searchCandidates) && searchCandidates.length > 0) {
      return (
        <div className="address-content-results">
          {MapCandidate}
          {searchCandidates.map((candidate) => {
            let placeName = candidate.placeName;
            if (!placeName) placeName = address?.split(',')[0];

            return (
              <div
                key={candidate.id}
                className="address-content-result"
                onClick={this.handleSelectCandidate('confirm', candidate)}
              >
                <div className="icon">
                  <i className="lz lz-fw lz-marker-o" />
                </div>
                <div className="content">
                  <h4>
                    <span>{placeName}</span>
                  </h4>
                  <div className="address">{candidate.address}</div>
                </div>
              </div>
            );
          })}
        </div>
      );
    }

    return (
      <div className="address-content-results">
        {GeoLocationCandidate}
        {shippingAddresses.map((candidate) => {
          let placeName = candidate.placeName;
          if (!placeName) placeName = candidate.address?.split(',')[0];

          return (
            <div
              key={`${candidate.lat}-${candidate.lng}`}
              className="address-content-result"
              onClick={this.handleSelectCandidate('confirm', candidate)}
            >
              <i className="lz lz-fw lz-history" />
              <div className="content">
                {placeName && (
                  <h4>
                    <span>{placeName}</span>
                  </h4>
                )}
                <div className="address">{candidate.address}</div>
              </div>
            </div>
          );
        })}
      </div>
    );
  };

  handleRef = (ref) => {
    if (this.addressInput) return;

    if (ref) this.addressInput = ref;
    if (!this.addressInput) return;

    if (this.props.autoFocusInput) this.handleInputFocus();
  }

   /** HACKING - due to some unknown reason, react-rnd Resizable component will calculate wrong transform value */
   handleSetWindowHeightManual = () => {
     clearTimeout(this.windowResizeTimeout);
     this.windowResizeTimeout = setTimeout(() => {
       if (!this.state.windowDefaultHeight) {
         this.setState((state) => ({ ...state, windowDefaultHeight: window.innerHeight }));
       }

       if (!this.state.resizeHeight) {
         this.setState((state) => ({ ...state, resizeHeight: state.windowDefaultHeight / 2 }));
         return;
       }

       if (this.state.resize.alreadyMin) {
         if (this.state.resizeHeight > RESIZE_MIN_HEIGHT) {
           this.setState((state) => ({ ...state, resizeHeight: RESIZE_MIN_HEIGHT }));
         }
         return;
       }

       if (window.innerHeight >= this.state.windowDefaultHeight || this.state.resize.alreadyMax) {
         if (this.state.resizeHeight < RESIZE_MIN_HEIGHT) {
           this.setState((state) => ({ ...state, resizeHeight: RESIZE_MIN_HEIGHT }));
         } else this.forceUpdate();
         return;
       }

       if (window.innerHeight < this.state.windowDefaultHeight || this.state.resize.alreadyMin) {
         if (this.state.resizeHeight > window.innerHeight - 32) {
           this.setState((state) => ({ ...state, resizeHeight: window.innerHeight - 32 }));
         } else this.forceUpdate();
       }
     }, 200);
   };


  handleInputChange = (e) => {
    const value = e.target.value;
    this.setState({ address: value, isSearching: true, searchCandidates: [] });

    this.deboundTimer = debouncer(
      this.deboundTimer,
      async () => {
        try {
          const predictions = value ? await this.googleMap.getAutocomplete(value) : [];

          this.setState({
            isSearching: false,
            searchCandidates: predictions.map((prediction) => ({
              id: prediction.id,
              placeName: prediction.structured_formatting && prediction.structured_formatting.main_text,
              address: prediction.description,
              placeId: prediction.place_id,
            })),
          });
        } catch (error) {
          this.setState({ isSearching: false });
        }
      },
      500,
    );
  };

  handleInputFocus = () => {
    const address = this.state.address || this.state.savedAddress || '';
    this.setState(() => ({ address, step: 'input' }), () => {
      this.handleResizeContainer(window.innerHeight);
      this.addressInput?.focus(); // TODO: some condition it's not working
    });
  };

  handleClearInput = () => {
    this.setState({ address: '', savedAddress: '', shippingAddress: undefined, searchCandidates: [] });
  };

  handleResizeContainer = (newHeight) => {
    this.setState({ resizeHeight: newHeight, resizeTransition: true });
    setTimeout(() => this.setState({ resizeTransition: false }), 300);
  };

  handleTouchStart = (e) => {
    const touch = e.touches[0];
    this.touchPoint = { x: touch.clientX, y: touch.clientY };
  };

  handleTouchMove = (e) => {
    if (!this.touchPoint) return;

    const { resize } = this.state;
    const minDistance = 50;

    if (e.changedTouches && e.changedTouches.length) {
      const touch = e.changedTouches[0];

      const deltaY = touch.clientY - this.touchPoint.y;

      if (deltaY >= minDistance && resize.min && !resize.alreadyMin) {
        this.setState(({ resize: { alreadyMin: true, alreadyMax: false } }));
        this.handleResizeContainer(window.innerHeight / 2);
      }

      if (deltaY <= -minDistance && resize.max && !resize.alreadyMax) {
        this.setState(({ resize: { alreadyMin: false, alreadyMax: true } }));
        this.handleResizeContainer(window.innerHeight - 32);
      }
    }
  };

  handleSelectCandidate = (step, item) => async () => {
    const shippingAddress = { ...item };
    const center = {
      lat: item.lat || this.state.center.lat,
      lng: item.lng || this.state.center.lng,
    };

    const data = {
      step,
      savedAddress: this.state.address,
      address: '',
      resizeTransition: true,
      resizeHeight: RESIZE_MIN_HEIGHT,
    };

    // TODO: better UX: fetch API when press Confirm, not when select
    let fullAddressInfo = {};
    try {
      if (!item.lat || !item.lng) {
        const addresses = await this.googleMap.getFullAddress({ address: shippingAddress.address });
        if (addresses[0]?.geometry) {
          shippingAddress.lat = addresses[0].geometry.location.lat();
          shippingAddress.lng = addresses[0].geometry.location.lng();
          shippingAddress.city = addresses[0].city;
          shippingAddress.district = addresses[0].district;
          shippingAddress.cityId = getCityId(addresses[0].city);
          shippingAddress.districtId = getDistrictId(addresses[0].district);
          shippingAddress.suggestedAddress = '';
          shippingAddress.suggestedDistrictId = '0';

          center.lat = addresses[0].geometry.location.lat();
          center.lng = addresses[0].geometry.location.lng();

          fullAddressInfo = { ...shippingAddress, street: `${addresses[0].streetNumber} ${addresses[0].street}` };
        }
      }
    } catch (e) {
      console.error('ERROR: handleSelectCandidate');
      data.step = 'manual';
    }

    if (data.step === 'manual') {
      data.lastValidManualCenter = shippingAddress.lat && shippingAddress.lng
        ? { lat: shippingAddress.lat, lng: shippingAddress.lng }
        : null;
    }
    this.setState((state) => ({ ...state, ...data, shippingAddress, center }), () => {
      typeof this.props.onAddressArrived === 'function' && this.props.onAddressArrived(fullAddressInfo);
    });
    setTimeout(() => this.setState({ resizeTransition: false }), 300);
  };

  updateCenter = async (center) => {
    if (this.state.step === 'confirm') return;

    const savedCenter = this.state.center;
    const roundCenter = {
      lat: roundNumber(center.lat, COORD_PRECISION),
      lng: roundNumber(center.lng, COORD_PRECISION),
    };

    if (savedCenter.lat === roundCenter.lat && savedCenter.lng === roundCenter.lng) return;
    this.setState((state) => ({ center: roundCenter, shippingAddress: { ...state.shippingAddress, ...roundCenter } }));

    if (this.state.step === 'manual') {
      const shouldUpdateAddress = iife(() => {
        if (!this.state.lastValidManualCenter) return true;
        if (getDistanceBirdFlies(
          this.state.lastValidManualCenter.lat,
          this.state.lastValidManualCenter.lng,
          roundCenter.lat, roundCenter.lng,
        ) > 0.5) return true;
      });

      try {
        if (shouldUpdateAddress) {
          const addresses = await this.googleMap.getFullAddress({
            location: { lat: roundCenter.lat, lng: roundCenter.lng },
          });
          if (addresses[0]?.geometry) {
            const placeName = addresses[0].formattedAddress?.split(',')[0];

            const address = {
              placeName,
              address: addresses[0].formattedAddress,
              lat: addresses[0].geometry.location.lat(),
              lng: addresses[0].geometry.location.lng(),
              cityId: getCityId(addresses[0].city),
              districtId: getDistrictId(addresses[0].district),
              suggestedAddress: '',
              suggestedDistrictId: '0',
            };

            this.setState((state) => ({
              ...state,
              shippingAddress: address,
              lastValidManualCenter: roundCenter,
              savedAddress: address.placeName,
            }));
          }
        }
      } catch (e) {
        console.error('ERROR: updateCenter');
      }
    }
  };

  fetchGeoLocation = async () => {
    if (!this.context.geoLocation?.latitude || !this.context.geoLocation?.longitude) return;

    try {
      const addresses = await this.googleMap.getFullAddress({
        location: { lat: this.context.geoLocation.latitude, lng: this.context.geoLocation.longitude },
      });
      if (addresses[0]?.geometry) {
        const placeName = addresses[0].formattedAddress?.split(',')[0];
        const address = {
          placeName,
          address: addresses[0].formattedAddress,
          lat: addresses[0].geometry.location.lat(),
          lng: addresses[0].geometry.location.lng(),
          cityId: getCityId(addresses[0].city),
          districtId: getDistrictId(addresses[0].district),
          suggestedAddress: '',
          suggestedDistrictId: '0',
        };

        this.setState({ geoLocation: address });
      }
    } catch (e) {
      console.error('ERROR: fetchGeoLocation');
    }
  };

  confirmShippingAddress = async () => {
    const { step, shippingAddress } = this.state;

    if (step === 'manual') {
      try {
        const addresses = await this.googleMap.getFullAddress({
          location: { lat: shippingAddress.lat, lng: shippingAddress.lng },
        });
        if (addresses[0]) {
          shippingAddress.suggestedAddress = addresses[0].formattedAddress,
          shippingAddress.suggestedDistrictId = getDistrictId(addresses[0].district);
        }
      } catch (e) {
        console.error('ERROR: confirmShippingAddress');
      }
    }

    this.props.updateShippingInfo(shippingAddress, this.props.isSender);
    typeof this.props.toNextStep === 'function' && this.props.toNextStep();
  };
}
