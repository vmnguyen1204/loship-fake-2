import BaseComponent from 'components/base';
import AppBanner from 'components/shared/banners';
import AppBannerDesktop from 'components/shared/banners/banner-footer';
import LanguageSwitcher from 'components/statics/language-switcher';
import PropTypes from 'prop-types';
import React from 'react';
import { APP_INFO, LINK_DOMAIN_LOSHIP } from 'settings/variables';
import { phoneNumber } from 'utils/format';
import { iife } from 'utils/helper';
import loziServices from 'utils/json/loziServices';
import { getRoute } from 'utils/routing';
import Link from 'utils/shims/link';

if (process.env.BROWSER) {
  require('assets/styles/components/footer.scss');
}

const WHITELIST_HIDE_FOOTER = {
  desktop: [
    '/payment',
    '/g/',
    '/m/buy',
    '/m/tai-khoan/don-hang',
    '/d/buy',
    '/d/tai-khoan/don-hang',
    '/c/:username', '/cid/:id',
    'cong-dong-loship',
    '/:merchantUsername/menu/dishes/:dishId',
    '/b/:merchant/menu/dishes/:dishId',
  ],
  mobile: [
    '/tim-kiem',
    '/tai-khoan/don-hang/:order',
    '/tai-khoan/cap-nhat',
    '/tai-khoan/quan-ly-the',
    '/tai-khoan/cua-hang-yeu-thich',
    '/c/:username', '/cid/:id',
    '/cong-dong-loship',
    '/payment',
    '/g/',
    '/m/buy',
    '/m/tai-khoan/don-hang',
    '/d/buy',
    '/d/tai-khoan/don-hang',
    '/b/:merchant/quan-ly',
    '/b/:merchant/menu',
    '/b/:merchant/cap-nhat-menu',
    '/:merchantUsername/menu',
    '/:merchantUsername/cap-nhat-menu',
    '/:merchantUsername/:groupTopic?',
    '/danh-sach',
  ],
};

const isWhitelistHideFooter = (isMobile) => {
  const { match } = (getRoute(null, globalThis.location?.pathname || globalThis.originalUrl)) || {};
  if (!match) return false;

  return WHITELIST_HIDE_FOOTER[isMobile ? 'mobile' : 'desktop'].some((path) => {
    return match.path.includes(path);
  });
};

class Footer extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  renderFooterDetail = () => {
    const service = loziServices.find((s) => s.name === APP_INFO.serviceName) ||
      loziServices.find((s) => s.name === 'loship');
    const utmSource = service.domain;
    return (
      <div className="footer">
        <div className="footer-wraper">
          <div className="col main">
            <img className="logo-loship" src="/dist/images/logo.png" alt="logo-loship" />
            <p>{this.getString('lozi_vietnam_join_stock_company')}</p>
            <p />
            <p>
              <b>{this.getString('lozi_vietnam_address_headquarter_address')}</b>
            </p>
            <p>
              {this.getString('contact_our_customer_service')}{' '}
              <a className="support-loship" href="mailto:hotroloship@lozi.vn">
                hotroloship@lozi.vn
              </a>
            </p>
            <p>
              <a style={{ color: '#5c5c5c' }} href="tel:1900638058">
                {this.getString('hotline')}
                <b>{phoneNumber('1900638058', { injectZero: false })}</b>
              </a>
            </p>
            <p>
              {this.getString('contact_cooperation_sponsorship')}{' '}
              <a className="support-loship" href="mailto:uyennhu@lozi.vn">
                uyennhu@lozi.vn
              </a>
            </p>
            <p>
              {this.getString('contact_press_inquiries')}{' '}
              <a className="support-loship" href="mailto:press@loship.vn">
                press@loship.vn
              </a>
            </p>
            <p>
              <Link to={`${LINK_DOMAIN_LOSHIP}/quy-che?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                {this.getString('loshipvn_terms_and_policies')}
              </Link>
            </p>
            <p className="special-info mt-16">
              Giấy CNĐKDN: 0313546809 – Ngày cấp: 24/11/2015, được sửa đổi lần thứ 1 ngày 01/04/2016.
            </p>
            <p className="special-info">Cơ quan cấp: Phòng Đăng ký kinh doanh – Sở kế hoạch và Đầu tư TP.HCM</p>
            <p className="special-info">
              Địa chỉ đăng ký kinh doanh: Lầu 4, Tòa nhà Mirae, 268 Tô Hiến Thành, Phường 15, Quận 10, Tp. Hồ Chí Minh,
              Việt Nam.
            </p>
            <a href="http://online.gov.vn/Home/WebDetails/43990">
              <img src="/dist/images/logo-bct.png" alt="Logo BCT" height="40px" />
            </a>
          </div>
          {iife(() => {
            if (this.context.partnerName !== 'none') return (
              <div>
                <LanguageSwitcher className="footer-language-menu" />
              </div>
            );

            return (
              <>
                <div className="col">
                  <h3>{this.getString('service')}</h3>
                  <p>
                    <Link to={`https://loship.vn/about/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('loship_food_delivery')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://lomart.vn/gioi-thieu/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('lomart_grocery_delivery')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://zat.loship.vn/gioi-thieu/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('lozat_laundry')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://loship.vn/lopoint/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('lopoint_credit_point')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://med.loship.vn/gioi-thieu/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('lomed_medicine_delivery')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://loship.vn/quy-che/losend/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('lo_send_package_delivery')}
                    </Link>
                  </p>
                  <p>
                    <Link
                      to={`https://loship.vn/gioi-thieu/lo-hoa/?utm_source=${utmSource}&utm_medium=footer`}
                      target="_blank"
                    >
                      {this.getString('lo_hoa_flower_delivery')}
                    </Link>
                  </p>
                  <p>
                    <Link
                      to={`https://loship.vn/gioi-thieu/lo-pet/?utm_source=${utmSource}&utm_medium=footer`}
                      target="_blank"
                    >
                      {this.getString('lo_pet_pet_supplies_delivery')}
                    </Link>
                  </p>
                  <p>
                    <Link
                      to={`https://losupply.vn/?utm_source=${utmSource}&utm_medium=footer`}
                      target="_blank"
                    >
                      {this.getString('lo_supply_supply_supplies_delivery')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://loship.vn/UngDungCuaHang/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('loship_merchant_application')}
                    </Link>
                  </p>
                </div>

                <div className="col">
                  <h3>{this.getString('cooperate')}</h3>
                  <p>
                    <Link to={`https://doitacloship.lozi.vn/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('open_shop_on_loship')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://dangkylomart.lozi.vn/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('open_shop_on_lomart')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://doitaclozat.lozi.vn/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('open_shop_on_lozat')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://doitaclomed.lozi.vn/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('open_shop_on_lomed')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://doitaclohoa.lozi.vn/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('open_shop_on_lohoa')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://doitaclopet.lozi.vn/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('open_shop_on_lopet')}
                    </Link>
                  </p>

                  <p>
                    <Link to={`https://hoptacvoiloship.lozi.vn/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('marketing_cooperation')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`https://loship.vn/career/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('career')}
                    </Link>
                  </p>
                </div>

                <div className="col">
                  <h3>{this.getString('loship_driver')}</h3>
                  <p>
                    <Link to={`http://chienbinhloship.com/?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('recruiting_loship_warrior')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`${LINK_DOMAIN_LOSHIP}/tuyen-dung/quy-tac-ung-xu?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('rules_of_loship_warrior')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`${LINK_DOMAIN_LOSHIP}/tuyen-dung/cau-hoi-thuong-gap?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('frequently_asked_questions')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`${LINK_DOMAIN_LOSHIP}/tuyen-dung/tram-dung-loship?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('loship_station')}
                    </Link>
                  </p>
                  <p>
                    <Link to={`${LINK_DOMAIN_LOSHIP}/tuyen-dung/bi-kip-di-don?utm_source=${utmSource}&utm_medium=footer`} target="_blank">
                      {this.getString('driver_tips')}
                    </Link>
                  </p>
                  <h3>{this.getString('media')}</h3>
                  <p>
                    <Link to="http://bit.ly/gioi-thieu-ve-loship" target="_blank">
                      {this.getString('loship_announce')}
                    </Link>
                  </p>
                  <p>
                    <Link to="http://bit.ly/allaboutlogoloship" target="_blank">
                      {this.getString('loship_download_logo')}
                    </Link>
                  </p>
                  <p>
                    <Link to="http://loship.vn/welcome" target="_blank">
                      {this.getString('Loship introduction')}
                    </Link>
                  </p>
                </div>

                <div className="col">
                  <h3>{this.getString('download_application')}</h3>
                  <p>
                    <Link
                      to={`http://bit.ly/Loship_iOS?utm_source=${utmSource}&utm_medium=footer`}
                      className="icon-app apple"
                      target="_blank"
                    />
                  </p>
                  <p>
                    <Link
                      to={`http://bit.ly/LOSHIPonAndroid?utm_source=${utmSource}&utm_medium=footer`}
                      className="icon-app android"
                      target="_blank"
                    />
                  </p>
                  <hr />
                  <h4>{this.getString('connect_with_us')}</h4>
                  <p className="item-social">
                    <Link to="https://www.facebook.com/LoshipVN/" className="social link-fb" target="_blank" />
                    <Link to="https://www.instagram.com/loshipvn/" className="social link-ins" target="_blank" />
                  </p>
                  <LanguageSwitcher className="footer-language-menu" />
                </div>
              </>
            );
          })}
        </div>
      </div>
    );
  };

  render() {
    if (isWhitelistHideFooter(this.context.isMobile)) return null;

    const { location } = this.props;
    return (
      <>
        {iife(() => {
          if (this.context.partnerName !== 'none') return null;
          if (this.context.isMobile) return <AppBanner location={location} isMobile={this.context.isMobile} />;
          return <AppBannerDesktop location={location} isMobile={this.context.isMobile} />;
        })}

        {this.renderFooterDetail()}
      </>
    );
  }
}

export default Footer;
