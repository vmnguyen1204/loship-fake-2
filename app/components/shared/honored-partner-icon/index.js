import React from 'react';
import Hint from 'components/statics/hint';

const handleClick = () => {
  window.open('https://doitacyeuthichloship.lozi.vn/');
};

export default function HonoredPartnerIcon({ clickToNavigate, getString }) {
  return (
    <Hint
      className="honored-icon"
      text={[
        getString('Cửa hàng được yêu thích trên Loship'),
        getString('Nhấn nút để xem toàn bộ cửa hàng được yêu thích nhé!'),
      ]}
      onClick={clickToNavigate && handleClick}
    >
      <img src="/dist/images/icon-honor.png" alt="Honored Icon" height="32px" />
    </Hint>
  );
}
