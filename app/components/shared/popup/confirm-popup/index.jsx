import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { CONFIRM_POPUP, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import cx from 'classnames';
import Button from 'components/shared/button';
import PropTypes from 'prop-types';

class ConfirmPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: CONFIRM_POPUP },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  close = (confirm = false) => {
    const params = (this.props.data && this.props.data.get('params')) || {};

    if (confirm === false && params.onCancel) {
      params.onCancel({ message: 'user cancel' });
    }
    if (confirm === true && params.onConfirm) {
      if (typeof params.validate === 'function') {
        const err = params.validate();
        if (err) return this.setState({ err });
      }
      params.onConfirm();
    }

    if (params.animationStack) {
      return this.ref.handleClose(() => this.props.dispatch(close(CONFIRM_POPUP)));
    }
    this.props.dispatch(close(CONFIRM_POPUP));
  };

  render() {
    const { isMobile } = this.context;
    const params = this.props.data.get('params') || {};
    const { confirmBtn = {}, cancelBtn = {}, animationStack } = params;

    const contentView = (() => {
      if (Array.isArray(params.content)) return params.content.map((str) => <p key={str}>{str}</p>);

      switch (typeof params.content) {
        case 'function':
          return params.content({ handleClose: this.close });
        case 'string':
        case 'object':
          return <p>{params.content}</p>;
        default:
          return params.content;
      }
    })();

    return (
      <Popup
        ref={this.handleRef}
        wrapperStyle={params.wrapperStyle}
        wrapperClassName={params.wrapperClassName}
        className={cx('confirm-popup', params.contentClassName, { fullscreen: params.fullscreen })}
        handleClose={params.canNotDismiss ? () => {} : this.close}
        animationStack={animationStack}
      >
        {(() => {
          if (typeof params.header === 'function') return params.header({ handleClose: this.close });
          if (params.title) {
            return (
              <div className="popup-header">
                {params.fullscreen && (
                  <Button className={cx('btn-back bg-transparent')} onClick={this.close}>
                    <i className={cx('lz', { 'lz-light-arrow-left': isMobile, 'lz-close': !isMobile })} />
                  </Button>
                )}
                {params.title}
              </div>
            );
          }
        })()}

        <div className="content">
          {params.description && <p>{params.description}</p>}
          {contentView}
        </div>

        <div className={cx('popup-footer', params.actionClassName)}>
          <Button type={cancelBtn.type} className={cancelBtn.className} onClick={() => this.close(false)}>
            {cancelBtn.text || this.getString('close')}
          </Button>
          <Button
            type={confirmBtn.type || 'primary'}
            className={cx('btn-confirm', confirmBtn.className)}
            onClick={() => this.close(true)}
          >
            {confirmBtn.text || this.getString('ok')}
          </Button>
        </div>
      </Popup>
    );
  }

  handleRef = (ref) => {
    if (ref) this.ref = ref;
    if (!this.ref) return;
  }
}

export default initComponent(ConfirmPopup);
