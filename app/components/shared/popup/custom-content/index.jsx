import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { CUSTOM_CONTENT, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import cx from 'classnames';
import Button from 'components/shared/button';

class CustomContentPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: CUSTOM_CONTENT },
  };

  close = () => {
    const { data } = this.props;
    const params = data && data.get('params');
    if (params && params.action) params.action();

    this.props.dispatch(close(CUSTOM_CONTENT));
  };

  render() {
    const params = this.props.data.get('params') || {};
    const contentView = (() => {
      if (Array.isArray(params.content)) return params.content.map((str) => <p key={str}>{str}</p>);

      switch (typeof params.content) {
        case 'function':
          return params.content();
        case 'string':
        case 'object':
          return <p>{params.content}</p>;
        default:
          return params.content;
      }
    })();

    return (
      <Popup
        wrapperClassName={params.wrapperClassName}
        className={cx('popup-custom', params.contentClassName)}
        handleClose={params.canNotDismiss ? () => {} : this.close}
      >
        {params.title && <div className="popup-header">{params.title}</div>}

        <div className="content">
          {params.description && <p>{params.description}</p>}
          {contentView}
        </div>

        <div className="popup-footer">
          <Button type={params.actionType || 'primary'} className={params.actionClassName} onClick={this.close}>
            {params.actionText || this.getString('close')}
          </Button>
        </div>
      </Popup>
    );
  }
}

export default initComponent(CustomContentPopup);
