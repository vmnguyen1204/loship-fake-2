import { CUSTOM_CONTENT, LOGIN, PROFILE_CHANGE_PASSWORD, close, open } from 'actions/popup';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Popup from 'components/shared/popup';
import PropTypes from 'prop-types';
import React from 'react';
import { changePassword } from 'actions/user';
import cx from 'classnames';
import { getCurrentUser } from 'reducers/user';
import { get as getPopup } from 'reducers/popup';
import { getRawPhoneNumber } from 'utils/format';
import initComponent from 'lib/initComponent';
import { logout } from 'actions/access-control';
import { parseErrorV2 } from 'utils/error';

class ProfileUpdateInfo extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (!data) return;

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
    params: { popup: PROFILE_CHANGE_PASSWORD },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props) {
    super(props);
    this.initialState = {
      data: {},
      error: [],
    };
    this.state = { ...this.initialState };
  }

  close = () => {
    const params = this.props.data && this.props.data.get('params');
    typeof params.callback === 'function' && params.callback();
    this.props.dispatch(close(PROFILE_CHANGE_PASSWORD));

    this.setState({ ...this.initialState, data: {}, error: [] });
  };

  render() {
    const { isMobile } = this.context;
    const { error } = this.state;

    return (
      <Popup className="profile-popup" handleClose={isMobile ? this.close : () => {}}>
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent', isMobile ? 'left' : 'right')} onClick={this.close}>
            <i className={cx('lz', isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
          </Button>

          {this.getString('change_password', 'profile')}
        </div>
        <div className="content">
          <div className="form-v2">
            <div className="form-item">
              <label htmlFor="password_current">{this.getString('current_password', 'profile')}</label>
              <input
                type="password"
                autoComplete="off"
                name="password_current"
                value={this.getField('passwordCurrent')}
                onChange={this.handleInputChange('passwordCurrent')}
              />
            </div>

            <div className="form-item">
              <label htmlFor="password">{this.getString('new_password', 'profile')}</label>
              <input
                type="password"
                name="password"
                value={this.getField('password')}
                onChange={this.handleInputChange('password')}
              />
            </div>
            <div className="form-item">
              <label htmlFor="password_confirm">{this.getString('confirm_new_password', 'profile')}</label>
              <input
                type="password"
                name="password_confirm"
                value={this.getField('passwordConfirm')}
                onChange={this.handleInputChange('passwordConfirm')}
              />
            </div>

            {error.length > 0 && (
              <div className="form-validation">
                {error.map((e) => (
                  <p key={e}>{e}</p>
                ))}
              </div>
            )}
          </div>
        </div>
        <div className="popup-footer">
          <Button type="primary" fw onClick={this.handleSubmit}>
            {this.getString('apply_changes')}
          </Button>
        </div>
      </Popup>
    );
  }

  handleSubmit = () => {
    const [validation = [], isValid] = this.getValidation();
    if (!isValid) {
      this.setState({ error: validation });
      return;
    }

    // TODO: countryCode
    const { currentUser } = this.props;
    this.props.dispatch(
      changePassword({
        countryCode: currentUser.get('countryCode'),
        user: currentUser.get('username'),
        phoneNumber: getRawPhoneNumber(currentUser.get('phoneNumber')),
        oldPassword: this.getField('passwordCurrent'),
        password: this.getField('password'),
        callback: () => {
          this.close();
          this.props.dispatch(logout());
          this.props.dispatch(
            open(CUSTOM_CONTENT, {
              title: this.getString('change_password_successfully', 'profile'),
              content: this.getString(
                'to_complete_change_password_you_need_to_login_again_with_the_new_password',
                'profile',
              ),
              action: () => this.props.dispatch(
                open(LOGIN, {
                  callback: () => {
                    this.handleSubmit();
                  },
                }),
              ),
              actionText: this.getString('agree_login_again', 'profile'),
              actionClassName: 'btn-fw',
              canNotDismiss: true,
            }),
          );
        },
        callbackFailure: (err) => {
          const error = parseErrorV2(err);
          this.setState({ error: [this.getString(error.codeEnum || error.message, 'error', [error.code])] });
        },
      }),
    );
  };

  getValidation = () => {
    const validation = [];

    if (!this.getField('passwordCurrent')) validation.push(this.getString('password_old_is_required', 'error'));

    if (!this.getField('password')) validation.push(this.getString('password_new_is_required', 'error'));
    else if (this.getField('password').length < 6) validation.push(this.getString('password_is_too_short', 'error'));
    else if (this.getField('password') !== this.getField('passwordConfirm')) validation.push(this.getString('password_not_match', 'error'));

    return [validation, validation.length === 0];
  };

  getField = (field) => {
    const { data } = this.state;

    if (data[field] !== undefined) return data[field];
    return '';
  };

  handleInputChange = (field) => (e) => {
    const value = e.target.value;
    const { data } = this.state;

    data[field] = value;

    this.setState({ data, error: [] });
  };
}

export default initComponent(ProfileUpdateInfo);
