import { PROFILE_UPDATE_INFO, close } from 'actions/popup';
import { capitalizes, isValidEmail } from 'utils/format';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import DatePicker from 'react-datepicker';
import Popup from 'components/shared/popup';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { getCurrentUser } from 'reducers/user';
import { get as getPopup } from 'reducers/popup';
import initComponent from 'lib/initComponent';
import moment from 'utils/shims/moment';
import { parseErrorV2 } from 'utils/error';
import { updateProfile } from 'actions/user';

function DatePickerInput({ value, onChange, onClick }) {
  return (
    <div className="form-item-content">
      <i className="fas fa-calendar-alt prefix" />
      <input value={value} onChange={onChange} onClick={onClick} />
    </div>
  );
}

class ProfileUpdateInfo extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (!data) return;

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
    params: { popup: PROFILE_UPDATE_INFO },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props) {
    super(props);
    this.initialState = {
      data: {},
      error: [],
    };
    this.state = { ...this.initialState };
  }

  close = () => {
    const params = this.props.data && this.props.data.get('params');
    typeof params.callback === 'function' && params.callback();
    this.props.dispatch(close(PROFILE_UPDATE_INFO));

    this.setState({ ...this.initialState, data: {}, error: [] });
  };

  render() {
    const { isMobile } = this.context;
    const { error } = this.state;

    /**
     * className visible: hotfix for Datepicker (if not visible, Datepicker won't show correctly).
     * Trade with no border-radius :(
     */
    return (
      <Popup className="profile-popup visible" handleClose={isMobile ? this.close : () => {}}>
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent', isMobile ? 'left' : 'right')} onClick={this.close}>
            <i className={cx('lz', isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
          </Button>

          {this.getString('update_personal_information', 'profile')}
        </div>
        <div className="content">
          <div className="form-v2">
            <div className="form-item required">
              <label htmlFor="fullname">{this.getString('full_name', 'profile')}</label>
              <input name="fullname" value={this.getField('fullname')} onChange={this.handleInputChange('fullname')} />
            </div>
            <div className="form-item">
              <label htmlFor="email">{this.getString('email', 'profile')}</label>
              <input name="email" value={this.getField('email')} onChange={this.handleInputChange('email')} />
            </div>
            <div className="form-item half">
              <label htmlFor="birthday">{this.getString('birthday', 'profile')}</label>
              <DatePicker
                className="f-datepicker"
                todayButton={this.getString('go_to_today')}
                selected={this.getField('birthday')}
                dateFormat="DD/MM/YYYY"
                onChange={(date) => this.handleInputChange('birthday')({ target: { value: date } })}
                customInput={<DatePickerInput />}
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
              />
            </div>
            <div className="form-item half">
              <label htmlFor="gender">{this.getString('gender', 'profile')}</label>
              <select
                name="gender"
                id="select-gender"
                value={this.getField('gender')}
                onChange={this.handleInputChange('gender')}
              >
                <option value="" disabled>
                  {this.getString('select_gender', 'profile')}
                </option>
                {['male', 'female', 'other'].map((gender) => (
                  <option key={gender} value={gender}>
                    {this.getString(gender, 'profile')}
                  </option>
                ))}
              </select>
            </div>

            <div className="form-annotate">(*) {this.getString('require_field')}</div>
            {error.length > 0 && (
              <div className="form-validation">
                {error.map((e) => (
                  <p key={e}>{e}</p>
                ))}
              </div>
            )}
          </div>
        </div>
        <div className="popup-footer">
          <Button type="primary" fw onClick={this.handleSubmit}>
            {this.getString('apply_changes')}
          </Button>
        </div>
      </Popup>
    );
  }

  handleSubmit = () => {
    const [validation = [], isValid] = this.getValidation();
    if (!isValid) {
      this.setState({ error: validation });
      return;
    }

    const birthday = this.getField('birthday', true);
    this.props.dispatch(
      updateProfile({
        user: this.props.currentUser.get('username'),
        data: {
          birthday: (typeof birthday?.isValid === 'function' && birthday.isValid() && birthday.format('DD/MM/YYYY')) || '',
          email: this.getField('email'),
          fullname: this.getField('fullname'),
          gender: this.getField('gender'),
        },
        callback: this.close,
        callbackFailure: (err) => {
          const error = parseErrorV2(err);
          this.setState({ error: [this.getString(error.codeEnum || error.message, 'error', [error.code])] });
        },
      }),
    );
  };

  getValidation = () => {
    const validation = [];

    if (!this.getField('fullname')) validation.push(this.getString('fullname_is_required', 'error'));

    const email = this.getField('email');
    if (email && !isValidEmail(email)) validation.push(this.getString('email_is_invalid', 'error'));

    return [validation, validation.length === 0];
  };

  getField = (field) => {
    const { currentUser } = this.props;
    const { data } = this.state;

    const propsField = currentUser.get('profile') && currentUser.get('profile')[field];

    if (field === 'birthday') {
      if (data[field] !== undefined) return data[field];
      if (propsField) return moment(propsField, 'DD/MM/YYYY');
      return null;
    }

    if (data[field] !== undefined) return data[field];

    if (field === 'fullname') return currentUser.get(field) || '';
    return propsField || '';
  };

  handleInputChange = (field) => (e) => {
    const value = e.target.value;
    const { data } = this.state;

    data[field] = value;
    if (field === 'fullname') data[field] = capitalizes(value);

    this.setState({ data, error: [] });
  };
}

export default initComponent(ProfileUpdateInfo);
