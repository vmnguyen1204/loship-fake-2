import { CUSTOM_CONTENT, LOGIN, PROFILE_UPDATE_PHONE, close, open } from 'actions/popup';
import {
  getPhoneNumberVerifiedCode,
  getSecretPassword,
  resendToken,
  resendTokenForFb,
  verifiedToken,
  verifiedTokenForFb,
} from 'actions/user';
import { getRawPhoneNumber, isValidPhoneNumber, phoneNumber } from 'utils/format';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Popup from 'components/shared/popup';
import PropTypes from 'prop-types';
import React from 'react';
import VerifyCodeInput from 'components/shared/token/verify-code-input';
import cx from 'classnames';
import { getCurrentUser } from 'reducers/user';
import { get as getPopup } from 'reducers/popup';
import initComponent from 'lib/initComponent';
import { logout } from 'actions/access-control';
import { parseErrorV2 } from 'utils/error';
import InputPhoneNumber from 'components/shared/input/phone-number';

class ProfileUpdatePhone extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (!data) return;

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
    params: { popup: PROFILE_UPDATE_PHONE },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props) {
    super(props);
    this.initialState = {
      data: {},
      isShowingPassword: false,
      error: [],
      step: undefined,
      alreadySent: false,
      resendCountdown: 0,
    };
    this.state = { ...this.initialState };
  }

  close = (force = false) => {
    if (this.state.step === 'verify' && force !== true) return this.setState({ step: undefined, error: [] });

    const params = this.props.data && this.props.data.get('params');
    typeof params.callback === 'function' && params.callback();
    this.props.dispatch(close(PROFILE_UPDATE_PHONE));

    this.setState({ ...this.initialState, data: {}, error: [] });
  };

  renderVerifiedStep = () => {
    const { error, resent, resendCountdown } = this.state;
    const code = this.getField('code');

    return (
      <div className="form-v2 flex flex-column align-center text-center">
        {resent ? (
          <div className="form-item">
            {this.getString('loship_is_sending_a_message_containing_the_confirmation_code_to_phone_number', 'profile')}{' '}
            <b>{this.getField('phoneNumberNew')}</b> , {this.getString('please_check_on_your_phone', 'profile')}
          </div>
        ) : (
          <div className="form-item">
            {this.getString('loship_will_send_a_message_containing_the_confirmation_code_to_phone_number', 'profile')}{' '}
            <b>{this.getField('phoneNumberNew')}</b>
            <br />
            {this.getString('enter_the_confirmation_code_in_the_box_below', 'profile')}
          </div>
        )}

        <VerifyCodeInput onCodeChange={this.handleVerifyCodeChange} />

        <Button type="primary" className="btn-verify-code" disabled={!code} onClick={this.handleVerifyToken}>
          {this.getString('confirm')}
        </Button>

        {error.length > 0 && (
          <div className="form-validation">
            {error.map((e) => (
              <p key={e}>{e}</p>
            ))}
          </div>
        )}

        <Button type="link" className="text-gray mb-16" disabled={resendCountdown > 0} onClick={this.handleResendToken}>
          <i className="lz lz-reload" />
          <span>
            {this.getString(resendCountdown > 0 ? 'request_resend_code_in_0_seconds' : 'Resend code', 'profile', [
              resendCountdown,
            ])}
          </span>
        </Button>
      </div>
    );
  };

  render() {
    const { isMobile } = this.context;
    const { isShowingPassword, error, step } = this.state;

    return (
      <Popup className="profile-popup" handleClose={isMobile ? this.close : () => {}}>
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent', isMobile ? 'left' : 'right')} onClick={this.close}>
            <i className={cx('lz', isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
          </Button>

          {this.getString('update_phone_number', 'profile')}
        </div>
        <div className="content">
          {step === 'verify' && this.renderVerifiedStep()}
          {step !== 'verify' && (
            <div className="form-v2">
              <div className="form-item">
                <b>{this.getString('notes')}</b>{' '}
                {this.getString(
                  'after_updated_your_phone_number_you_can_no_longer_use_the_old_phone_number_to_log_in',
                  'profile',
                )}
              </div>
              <div className="form-item">
                <label htmlFor="phone_number_new">{this.getString('enter_your_new_phone_number', 'profile')}</label>
                <InputPhoneNumber
                  name="phone_number_new"
                  btnClassName="btn-link bg-transparent"
                  defaultCountryCode={this.getField('countryCode')}
                  onChange={(value) => this.handleInputChange('phoneNumberNew')({ target: { value } })}
                  onChangeCountryCode={(value) => this.handleInputChange('countryCodeNew')({ target: { value } })}
                />
              </div>
              {!this.isLoginByFacebook() && (
                <div className="form-item">
                  <label htmlFor="password">{this.getString('enter_current_password', 'profile')}</label>
                  <div className="form-item-content">
                    {isShowingPassword ? (
                      <i
                        key="eye"
                        className={cx('lz lz-eye-o postfix', { animate: this.state.animateShowingPassword })}
                        onClick={this.toggleShowPassword}
                      />
                    ) : (
                      <i
                        key="eye-close"
                        className={cx('lz lz-eye-close postfix', { animate: this.state.animateShowingPassword })}
                        onClick={this.toggleShowPassword}
                      />
                    )}
                    <input
                      type={isShowingPassword ? 'text' : 'password'}
                      autoComplete="off"
                      name="password"
                      value={this.getField('password')}
                      onChange={this.handleInputChange('password')}
                      data-lpignore="true"
                    />
                  </div>
                </div>
              )}

              {error.length > 0 && (
                <div className="form-validation">
                  {error.map((e) => (
                    <p key={e}>{e}</p>
                  ))}
                </div>
              )}
            </div>
          )}
        </div>
        {step !== 'verify' && (
          <div className="popup-footer">
            <Button type="primary" fw onClick={this.handleSubmit}>
              {this.getString('confirm_change_phone_number', 'profile')}
            </Button>
          </div>
        )}
      </Popup>
    );
  }

  handleSubmit = () => {
    const [validation = [], isValid] = this.getValidation();
    if (!isValid) {
      this.setState({ error: validation });
      return;
    }

    const dispatcher = this.isLoginByFacebook() ? getPhoneNumberVerifiedCode : getSecretPassword;
    this.props.dispatch(
      dispatcher({
        user: this.props.currentUser.get('username'),
        data: this.isLoginByFacebook()
          ? {
            countryCode: this.getField('countryCodeNew'),
            phoneNumber: getRawPhoneNumber(this.getField('phoneNumberNew')),
          }
          : {
            countryCode: this.getField('countryCodeNew'),
            newPhoneNumber: getRawPhoneNumber(this.getField('phoneNumberNew')),
            password: this.getField('password'),
          },
        callback: (res) => {
          const { data } = res.body || {};
          if (!this.isLoginByFacebook() && !data.secretPassword) return this.setState({ error: [this.getString('unknown_error', 'error', [5005])] });

          this.setState({ step: 'verify', alreadySent: true, resendCountdown: 30 });
          this.handleInputChange('secretPassword')({ target: { value: data.secretPassword } });
          this.handleResendCountDown();
        },
        callbackFailure: (err) => {
          const error = parseErrorV2(err);
          this.setState({ error: [this.getString(error.codeEnum || error.message, 'error', [error.code])] });
        },
      }),
    );
  };

  handleResendCountDown = () => {
    setTimeout(() => {
      if (this.state.resendCountdown <= 0) return this.setState({ resendCountdown: 0 });
      this.setState((state) => ({ resendCountdown: state.resendCountdown - 1 }));
      this.handleResendCountDown();
    }, 1000);
  };

  handleResendToken = () => {
    const dispatcher = this.isLoginByFacebook() ? resendTokenForFb : resendToken;
    this.props.dispatch(
      dispatcher({
        user: this.props.currentUser.get('username'),
        data: {
          countryCode: this.getField('countryCodeNew'),
          phoneNumber: this.getField('phoneNumberNew'),
        },
        callback: () => {
          this.setState({ resent: true });
          setTimeout(() => this.setState({ resent: false }), 5000); // For showing message on step verify
        },
        callbackFailure: (err) => {
          const error = parseErrorV2(err);
          this.setState({ error: [this.getString(error.codeEnum || error.message, 'error', [error.code])] });
        },
      }),
    );
  };

  handleVerifyToken = () => {
    const dispatcher = this.isLoginByFacebook() ? verifiedTokenForFb : verifiedToken;
    this.props.dispatch(
      dispatcher({
        user: this.props.currentUser.get('username'),
        data: {
          countryCode: this.getField('countryCodeNew'),
          phoneNumber: this.getField('phoneNumberNew'),
          token: this.getField('code'),
          secretPassword: this.getField('secretPassword') || undefined, // Login Facebook does not have secretPassword
        },
        callback: () => {
          this.close(true);
          if (!this.isLoginByFacebook()) {
            this.props.dispatch(logout());
            this.props.dispatch(
              open(CUSTOM_CONTENT, {
                title: this.getString('change_phone_number_successfully', 'profile'),
                content: this.getString(
                  'to_complete_change_phone_number_you_need_to_login_again_with_your_new_phone_number_and_current_password',
                  'profile',
                ),
                action: () => this.props.dispatch(
                  open(LOGIN, {
                    callback: () => {
                      this.handleVerifyToken();
                    },
                  }),
                ),
                actionText: this.getString('agree_login_again', 'profile'),
                actionClassName: 'btn-fw',
                canNotDismiss: true,
              }),
            );
          }
        },
        callbackFailure: (err) => {
          const error = parseErrorV2(err);
          this.setState({ error: [this.getString(error.codeEnum || error.message, 'error', [error.code])] });
        },
      }),
    );
  };

  getValidation = () => {
    const { step } = this.state;
    const validation = [];

    if (step === 'verify') {
      //
    } else {
      if (!this.getField('phoneNumberNew')) validation.push(this.getString('phone_number_is_required', 'error'));
      else if (!isValidPhoneNumber(this.getField('phoneNumberNew'), this.getField('countryCodeNew'))) validation.push(this.getString('phone_number_is_invalid', 'error'));

      if (!this.isLoginByFacebook()) {
        if (!this.getField('password')) validation.push(this.getString('password_is_required', 'error'));
      }
    }

    return [validation, validation.length === 0];
  };

  getField = (field) => {
    const { currentUser } = this.props;
    const { data } = this.state;

    if (data[field] !== undefined) return data[field];
    if (field === 'countryCodeNew') return currentUser.get('countryCode') || '';
    return currentUser.get(field) || '';
  };

  handleInputChange = (field) => (e) => {
    const value = e.target.value;
    const { data } = this.state;

    data[field] = value;
    if (field === 'phoneNumberNew') data[field] = phoneNumber(value.replace(/\D/g, ''));

    this.setState({ data, error: [], alreadySent: false });
  };

  toggleShowPassword = () => {
    this.setState((state) => ({ isShowingPassword: !state.isShowingPassword, animateShowingPassword: true }));
  };

  handleVerifyCodeChange = (code) => {
    this.handleInputChange('code')({ target: { value: code } });
  };

  isLoginByFacebook = () => {
    const { currentUser: data } = this.props;
    return data.get('providers')?.[0] !== 'Phone'; // Fallback to Facebook for hide sensitive data when cannot detect provider
  };
}

export default initComponent(ProfileUpdatePhone);
