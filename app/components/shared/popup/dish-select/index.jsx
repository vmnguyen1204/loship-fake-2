import { warnDishSelect } from 'actions/alert';
import { generateShortLinkSharing } from 'actions/buy';
import { DISH_SELECT, SHARING_POPUP, close, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Checkbox from 'components/shared/checkbox';
import ExpandContainer from 'components/shared/expand-container';
import InputAutoGrow from 'components/shared/input/auto-grow';
import NumberUpDownInput from 'components/shared/number-up-down-input';
import Popup from 'components/shared/popup';
import ImageLazy from 'components/statics/img-lazy';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import { get as dataGetter } from 'reducers/popup';
import { LINK_LZI } from 'settings/variables';
import { calculateDishPrice } from 'utils/cart';
import { copyToClipboard } from 'utils/content';
import { addCurrency, utoa } from 'utils/format';
import { iife, produce } from 'utils/helper';

class DishSelectPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: DISH_SELECT },
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
  };

  initialState = { quantity: 1, customs: {}, note: '' };

  state = produce(this.initialState);

  close = () => {
    if (this.state.noteFocused) return;
    this.setState(produce(this.initialState));

    const params = this.props.data.get('params') || {};
    const { callback, callbackOnClose } = params;
    if (callbackOnClose) typeof callback === 'function' && callback();

    return this.ref.handleClose(() => this.props.dispatch(close(DISH_SELECT)));
  };

  renderOptions(custom) {
    const selectedOptions = (this.state.customs[custom.id] && this.state.customs[custom.id].options) || [];

    return custom.customOptions.map((option) => {
      const selectedOption = selectedOptions && selectedOptions.find((s) => s.optionId === option.id);
      const isRadio = custom.selectionType === 'single_choice';
      const isSelected = !!selectedOption;

      return (
        <Checkbox
          className="flex-item custom-option-item"
          key={option.id}
          isRadio={isRadio}
          checked={isSelected}
          onChange={() => this.handleChangeOption(custom, option, isSelected ? -1 : 1)}
        >
          <div className="name">
            {option.value}
            {option.price > 0 && <div className="price">+{addCurrency(option.price)}</div>}
          </div>
        </Checkbox>
      );
    });
  }

  render() {
    const { isMobile } = this.context;
    const { quantity, customs } = this.state;

    const params = this.props.data.get('params') || {};
    const { dish } = params;

    const customsSingleChoice = Array.isArray(dish.customs)
      ? dish.customs.filter((c) => c.selectionType === 'single_choice')
      : [];
    const customsMultipleChoice = Array.isArray(dish.customs)
      ? dish.customs.filter((c) => c.selectionType !== 'single_choice')
      : [];

    return (
      <Popup
        ref={this.updateRef}
        className={cx('dish-select', this.state.noteFocused && 'focus')}
        animationStack={1}
        handleClose={this.close}
        wrapperStyle={params.wrapperStyle}
      >
        <div className="popup-header">
          <Button type="link" className="btn-back left text-gray" onClick={this.close}>
            <i className="lz lz-close" />
          </Button>
          {this.getString('choose_dish', 'eatery')}
          {!isMobile && (
            <Button className="btn-back right bg-transparent pull-right" onClick={this.handleSharing}>
              <i className="fas fa-share-alt" />
            </Button>
          )}
        </div>

        <div className="content">
          <div className="list-item dish-item" onClick={this.handleViewItemDetail}>
            <ImageLazy
              src={dish.image}
              placeholder="/dist/images/dish-placeholder.png"
              shape="round-square"
              scrollableAncestor={null}
              size={64}
            />
            <div className="content">
              <div className="name">{dish.name}</div>
              <div className="price">
                <div>
                  <b>{addCurrency(dish.price)}</b>
                  {dish.rawPrice !== dish.price && <span className="old">{addCurrency(dish.rawPrice)}</span>}
                </div>
              </div>
              {isMobile ? (
                <ExpandContainer
                  key="description"
                  className="description"
                  content={dish.description}
                  line={2}
                  mode={isMobile ? 'FIXED' : 'VIEW_MORE'}
                />
              ) : (
                <div className="description scroll-nice">{dish.description}</div>
              )}
              {isMobile && <i className="lz lz-arrow-head-right" />}
            </div>
            {!isMobile && (
              <NumberUpDownInput
                value={quantity}
                showTrash={false}
                mode="set"
                min={1}
                onChange={(value) => this.setState({ quantity: value })}
              />
            )}
          </div>

          <div ref={(ref) => {this.dishCustomScroll = ref;}} className="dish-custom scroll-nice">
            {!isMobile && (
              <div className="flex-view">
                <div className="title">
                  {this.getString('dish_note_title')}
                </div>
                <InputAutoGrow
                  className="note" maxRows={3}
                  onChange={this.handleNoteChange}
                  placeholder={this.getString('dish_note_placeholder_desktop')}
                />
              </div>
            )}
            <div className="col">
              {isMobile && (
                <div className="flex-view p16_16">
                  <InputAutoGrow
                    className="note" maxRows={3}
                    onChange={this.handleNoteChange}
                    onFocus={this.handleNoteFocus}
                    onBlur={this.handleNoteBlur}
                    placeholder={this.getString('dish_note_placeholder')}
                  />
                </div>
              )}
              {customsSingleChoice.length > 0 && customsSingleChoice.map((custom) => {
                if (!Array.isArray(custom.customOptions) || custom.customOptions.length === 0) return null;
                return (
                  <div key={custom.id} className="flex-view">
                    <div className="title">
                      {custom.name}
                      {custom.note && <span className="note"> ({custom.note})</span>}
                    </div>
                    {this.renderOptions(custom)}
                  </div>
                );
              })}
            </div>
            {customsMultipleChoice.length > 0 && (
              <div className="col">
                {customsMultipleChoice.map((custom) => {
                  if (!Array.isArray(custom.customOptions) || custom.customOptions.length === 0) return null;
                  return (
                    <div key={custom.id} className="flex-view">
                      <div className="title">
                        {custom.name}
                        {custom.note && <span className="note"> ({custom.note})</span>}
                      </div>
                      {this.renderOptions(custom)}
                    </div>
                  );
                })}
              </div>
            )}
          </div>
        </div>

        {!this.state.noteFocused && (
          <div className="popup-footer">
            {isMobile && (
              <NumberUpDownInput
                value={quantity}
                showTrash={false}
                mode="set"
                min={1}
                onChange={(value) => this.setState({ quantity: value })}
              />
            )}
            <Button type="primary" fw={isMobile} onClick={this.handleAddToCart}>
              {this.getString('add')} +{addCurrency(calculateDishPrice(dish, customs, quantity))}
            </Button>
          </div>
        )}
      </Popup>
    );
  }

  updateRef = (ref) => {
    if (ref) this.ref = ref;
    if (!this.ref) return;

    const params = this.props.data.get('params') || {};
    params.dish && this.selectDefaultOptions(params.dish);
  };

  selectDefaultOptions = (data) => {
    const defaultCustoms = {};
    iife(() => {
      const targetCustoms = (data.customs || []).filter((custom) => custom.selectionType === 'single_choice');
      targetCustoms.forEach((custom) => {
        const options = custom.customOptions || [];
        const freeOptions = options.filter((option) => !option.price);
        const firstPaidOption = options.find((option) => !!option.price);

        let defaultOption;

        if (firstPaidOption) {
          if (freeOptions.length > 0) defaultOption = freeOptions[0];
          else defaultOption = firstPaidOption;
        } else {
          defaultOption = options[Math.floor((options.length - 1) / 2)];
        }

        if (!defaultOption) return;
        defaultCustoms[custom.id] = {
          customId: custom.id,
          options: [{ optionId: defaultOption.id, optionQuantity: 1, price: defaultOption.price }],
        };
      });
    });

    this.setState({ customs: defaultCustoms });
  };

  handleChangeOption = (custom, option, quantity) => {
    const current = produce(this.state.customs[custom.id] ? this.state.customs[custom.id].options : [], (draft) => {
      let inputQuantity = parseInt(quantity);
      if (inputQuantity < 1) inputQuantity = 1;
      if (option.limitQuantity && inputQuantity > option.limitQuantity) {
        this.props.dispatch(
          warnDishSelect(
            this.getString('you_cannot_choose_more_than_0_1', 'alert', [option.limitQuantity, `${custom.name} ${option.value}`]),
          ),
        );
        return;
      }

      const optionData = { optionId: option.id, optionQuantity: inputQuantity, price: option.price };
      const idx = draft.findIndex((c) => c.optionId === optionData.optionId);

      if (custom.selectionType === 'single_choice') return [optionData];
      if (idx > -1) {
        draft.splice(idx, 1);
      } else {
        draft.push(optionData);
      }
    });

    const currentOptionCount = current.reduce((sum, cOption) => sum + cOption.optionQuantity || 0, 0);
    if (currentOptionCount > custom.limitQuantity) {
      return this.props.dispatch(
        warnDishSelect(this.getString('you_cannot_choose_more_than_0_items', 'alert', [custom.limitQuantity])),
      );
    }

    this.setState((state) => ({
      customs: {
        ...state.customs,
        [custom.id]: {
          customId: custom.id,
          options: current,
        },
      },
    }));
  };

  handleAddToCart = () => {
    const params = this.props.data.get('params') || {};
    const { dish, merchant, groupTopic, groupInfo, handleAddToCart, callback, callbackOnClose } = params;
    if (typeof handleAddToCart !== 'function') return;

    const customs = Object.keys(this.state.customs).map((custom) => this.state.customs[custom]);
    handleAddToCart({
      data: dish,
      lineData: {
        dishId: dish.id,
        dishQuantity: this.state.quantity,
        customs,
        note: this.state.note,
        ...groupInfo,
      },
      merchant,
      groupTopic,
      callback: () => {
        if (!callbackOnClose) typeof callback === 'function' && callback();
        this.close();
      },
    });
  };

  handleViewItemDetail = () => {
    if (this.state.noteFocused) return;
    if (!this.context.isMobile) return;

    const params = this.props.data.get('params') || {};
    const { dish } = params;
    if (!dish) return;

    this.close();
    setTimeout(() => {
      const query = this.getQuery();
      query.pItem = dish.id;
      this.props.history.push(location.pathname + '?' + qs.stringify(query));
    }, 0);
  };

  handleSharing = () => {
    const params = this.props.data.get('params') || {};
    const { dish } = params;
    if (!dish) return;

    this.props.dispatch(
      generateShortLinkSharing({
        rawUrl: `https://lozi.loship.vn/items/${utoa(dish.id)}`,
        fallbackUrl: window.location.href,
        baseSharingUrl: `${LINK_LZI}/i`,
        callback: (sharingUrl) => {
          const sharingContent = this.getString('item_sharing_content', '', [dish.name]);

          copyToClipboard(`${sharingContent} ${sharingUrl}`);
          toast(this.getString('copied_link'));

          this.props.dispatch(
            open(SHARING_POPUP, {
              subject: this.getString('buy_on_loship', 'order'),
              body: sharingContent,
              url: sharingUrl,
            }),
          );
        },
      }),
    );
  };

  handleNoteChange = (value) => {
    this.setState({ note: value.trim() });
  }

  handleNoteFocus = () => {
    this.dishCustomScroll.scrollTop = 0;
    this.setState({ noteFocused: true });
  }

  handleNoteBlur = () => {
    // Prevent race-condition with handleViewItemDetail / close
    setTimeout(() => {
      this.setState({ noteFocused: false });
    }, 100);
  }
}

export default initComponent(DishSelectPopup, withRouter);
