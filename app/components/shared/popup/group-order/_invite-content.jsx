import { SHARING_POPUP, open } from 'actions/popup';

import BaseComponent from 'components/base';
import Clipboard from 'react-clipboard.js';
import React from 'react';
import cx from 'classnames';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';
import ImageLazy from 'components/statics/img-lazy';
import Button from 'components/shared/button';

class PopupGroupOrderInviteContent extends BaseComponent {
  renderContent = () => {
    const { isCreate, currentUser, shortLink } = this.props;

    if (isCreate) {
      return (
        <div className="content">
          <div className="host-avatar text-center">
            <i className="lz lz-crown" />
            <br />
            <ImageLazy src={currentUser.get('avatar')} />
          </div>
          <h4 className="text-center">
            {this.getString('congratulations_you_have_successfully_created_a_group', 'order')}
          </h4>
          <p>
            {this.getString(
              'you_are_groups_owner_share_the_link_below_for_friends_to_invite_them_to_order_with_you',
              'order',
            )}
          </p>

          <div className="shortlink">{shortLink}</div>
        </div>
      );
    }

    return (
      <div className="content">
        <p>{this.getString('invite_friends_to_orders_together_more_convenient_and_economical', 'order')}</p>
        <p>{this.getString('just_copy_the_link_below_and_send_it_to_your_friends', 'order')}</p>

        <div className="shortlink">{shortLink}</div>
      </div>
    );
  };

  render() {
    const { isCreate, shortLink } = this.props;

    return (
      <div className="popup-inner">
        {isCreate && (
          <div className="popup-header">
            <Button type="link" className="btn-back" onClick={this.handleClose}>
              <i className={cx('lz', this.props.isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
            </Button>
            <h4>{this.getString('order_with_groups', 'order')}</h4>
          </div>
        )}
        {this.renderContent()}

        <div className="content actions">
          <Clipboard className="action" component="a" data-clipboard-text={shortLink} onSuccess={this.handleCopyCode}>
            <i className="far fa-clone" />
            {this.state.copied ? this.getString('copy_success', 'order') : this.getString('copy', 'order')}
          </Clipboard>

          <a className="action" onClick={this.handleShare}>
            <i className="far fa-share-square" /> {this.getString('share', 'order')}
          </a>
        </div>

        <div className={cx('popup-footer', { short: !isCreate })}>
          {isCreate && (
            <a className="action primary" onClick={this.continueGroupOrder}>
              {this.getString('continue_order', 'order')}
            </a>
          )}
          <a className="action" onClick={this.handleClose}>
            {this.getString(isCreate ? 'leave_group' : 'close', 'order')}
          </a>
        </div>
      </div>
    );
  }

  handleCopyCode = () => {
    this.setState({ copied: true });

    if (this.copyCodeTimeout) clearTimeout(this.copyCodeTimeout);
    this.copyCodeTimeout = setTimeout(() => {
      this.setState({ copied: false });
    }, 3000);
  };

  handleShare = () => {
    const { shortLink } = this.props;

    this.props.dispatch(
      open(SHARING_POPUP, {
        url: shortLink,
        subject: this.getString('buy_on_loship', 'order'),
        body: this.getString('click_on_the_link_to_order_on_loship_with_me_0', 'order'),
      }),
    );
  };

  continueGroupOrder = () => {
    this.props.onConfirm();
    this.props.handleClose();
  };

  handleClose = () => {
    this.props.handleClose();
  };
}

export default initComponent(PopupGroupOrderInviteContent, withRouter);
