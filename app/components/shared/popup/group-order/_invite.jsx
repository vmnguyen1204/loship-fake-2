import { generateShortLinkSharing } from 'actions/buy';
import { GROUP_ORDER_INVITE, close } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { get as getPopup } from 'reducers/popup';
import { getCurrentUser } from 'reducers/user';
import { LINK_LZI } from 'settings/variables';
import PopupGroupOrderInviteContent from './_invite-content';

class PopupGroupOrderInvite extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (!data) return;

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
    params: { popup: GROUP_ORDER_INVITE },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  close = () => {
    return this.ref.handleClose(() => this.props.dispatch(close(GROUP_ORDER_INVITE)));
  };

  componentDidUpdate() {
    if (!this.props.data || !this.props.data.get('params')) return;
    if (this.state.shortLink) return;

    const params = this.props.data.get('params');
    const { linkGroup } = params;

    this.props.dispatch(
      generateShortLinkSharing({
        rawUrl: linkGroup,
        fallbackUrl: linkGroup,
        baseSharingUrl: `${LINK_LZI}/g`,
        callback: (sharingUrl) => {
          this.setState({ shortLink: sharingUrl });
        },
      }),
    );
  }

  render() {
    const { shortLink } = this.state;
    const { currentUser } = this.props;

    const params = this.props.data.get('params');
    const { isCreate, onConfirm, linkGroup } = params;

    return (
      <Popup
        ref={this.updateRef}
        className={cx('group-order-invite-popup', { fullscreen: isCreate && this.context.isMobile })}
        handleClose={this.close}
        animationStack={1}
      >
        <PopupGroupOrderInviteContent
          isMobile={this.context.isMobile}
          isCreate={isCreate}
          shortLink={shortLink || linkGroup}
          currentUser={currentUser}
          handleClose={this.close}
          onConfirm={onConfirm}
        />
      </Popup>
    );
  }

  updateRef = (ref) => {
    if (ref) this.ref = ref;
    if (!this.ref) return;
  };
}

export default initComponent(PopupGroupOrderInvite);
