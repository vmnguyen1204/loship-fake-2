import { GROUP_ORDER_MEMBER_LIST, close } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Popup from 'components/shared/popup';
import initComponent from 'lib/initComponent';
import React from 'react';
import { API, cartLockUser, getCartMetadata } from 'reducers/new/cart';
import { get as getPopup } from 'reducers/popup';
import { getCurrentUser } from 'reducers/user';
import { getAvatarUrl } from 'utils/image';
import { iife } from 'utils/helper';
import callAPI from 'actions/helpers';

class PopupGroupOrderMemberList extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (!data) return;

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      const { merchant, groupTopic } = data.getIn(['data', 'params']) || {};
      const cartMetadata = getCartMetadata(state, { merchant, groupTopic });
      if (cartMetadata) data = data.set('cartMetadata', cartMetadata);

      return data;
    },
    params: { popup: GROUP_ORDER_MEMBER_LIST },
  };

  constructor(props) {
    super(props);
    this.state = { height: 0 };
    this.onWindowResize = this.onWindowResize.bind(this);
  }

  componentDidMount() {
    this.onWindowResize();
    window.addEventListener('resize', this.onWindowResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onWindowResize);
  }

  onWindowResize() {
    this.setState({ height: window.innerHeight });
  }

  close = () => {
    this.props.dispatch(close(GROUP_ORDER_MEMBER_LIST));
  };

  render() {
    const { cartMetadata = {}, currentUser } = this.props;
    const { data: { users = [], lockedUsers = [], createdBy } = {} } = cartMetadata;

    if (!currentUser) return null;

    const params = this.props.data.get('params');
    const { showCTA } = params;
    const isHost = createdBy === currentUser.get('id');
    const isAllMemberLocked = users
      .filter((user) => user.username !== currentUser.get('username'))
      .every((user) => lockedUsers[user.username]);

    return (
      <Popup
        className="group-order-member-list-popup"
        style={{ height: this.state.height + 'px' }}
        handleClose={this.close}
      >
        <div className="popup-header">
          <span className="btn-back" onClick={this.close}>
            {this.getString('go_back')}
          </span>
          <h4>{this.getString('group_member_list', 'order')}</h4>
        </div>

        <div className="content">
          <div className="hint">
            <i className="lz lz-question-square" />
            {iife(() => {
              if (isHost) {
                return this.getString(showCTA
                  ? 'please_mark_done_with_members_are_not_ready_to_proceed_with_the_order'
                  : 'you_can_proactively_mark_done_with_your_member_to_proceed_with_the_order');
              }
              return this.getString('cart_member_hint');
            })}
          </div>

          <div className="members">
            <div className="member">
              <div
                className="member-avatar"
                style={{ backgroundImage: `url(${getAvatarUrl(currentUser.get('avatar'), 32)})` }}
              />
              <div className="name">
                <b>{currentUser.get('fullname')}</b> {this.getString('you', 'order')}
                {currentUser.get('id') === createdBy && <i className="lz lz-crown" />}
              </div>
            </div>
            {users
              .filter((user) => user.id !== currentUser.get('id'))
              .map((user) => {
                const isLocked = lockedUsers[user.username];
                return (
                  <div key={user.id} className="member">
                    <div
                      className="member-avatar"
                      style={{ backgroundImage: `url(${getAvatarUrl(user.avatar, 32)})` }}
                    />
                    <div className="name">
                      <b>{(user.name && user.name.full) || user.fullname || user.username}</b>
                      {user.id === createdBy && <i className="lz lz-crown" />}
                    </div>

                    <div
                      className={cx('status', { locked: isLocked })}
                      onClick={isHost ? this.handleLockUser(user) : () => {}}
                    >
                      {this.getString(isLocked ? 'cart_done' : 'cart_doing', 'order')}
                      {isHost && <i className="lz lz-check-mark icon-check" />}
                    </div>
                  </div>
                );
              })}
          </div>
        </div>

        {showCTA && (
          <div className="popup-footer">
            <Button type="primary" className="btn-fw" disabled={!isAllMemberLocked} onClick={this.toNextStep}>
              <span>{this.getString('place_order')}</span>
              <i className="lz lz-light-arrow-right" />
            </Button>
          </div>
        )}
      </Popup>
    );
  }

  handleLockUser = (user) => async () => {
    const { cartMetadata = {}, currentUser } = this.props;
    const { data: { createdBy, lockedUsers = {} } = {} } = cartMetadata;

    if (currentUser.get('id') !== createdBy) return;

    const params = this.props.data.get('params');
    const { merchant, groupTopic } = params;
    const currentLockStatus = lockedUsers[user.username];
    const nextLockStatus = !currentLockStatus;

    this.props.dispatch(cartLockUser({ merchant, groupTopic, user, lock: nextLockStatus }));
    callAPI('put', {
      url: API.LOCK_MEMBER_BY_ID,
      params: { groupTopic, memberId: user.id },
      content: { status: nextLockStatus ? 'lock' : 'unlock' },
    });
  };

  toNextStep = () => {
    const params = this.props.data.get('params');
    params.toNextStep();

    this.close();
  };
}

export default initComponent(PopupGroupOrderMemberList);
