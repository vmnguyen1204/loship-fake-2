import { CUSTOM_CONTENT, LOGIN, ORDER_FAILED, close, open } from 'actions/popup';
import {
  initialPhone,
  loginByFacebook,
  loginByPhone,
  loginWithData,
  openAccountFacebook,
} from 'actions/access-control';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import InputPhoneNumber from 'components/shared/input/phone-number';
import Popup from 'components/shared/popup';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { get as dataGetter } from 'reducers/popup';
import initComponent from 'lib/initComponent';
import { isValidPhoneNumber } from 'utils/format';
import { linkFacebook } from 'actions/user';
import { parseErrorV2 } from 'utils/error';
import produce from 'immer';
import { iife } from 'utils/helper';

class LoginPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: LOGIN },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  initialState = {
    screens: [],
    countryCode: '84',
    phoneNumber: '',
    password: '',
    isShowingPassword: false,
    error: null,
    facebookInfo: null,
  };

  state = produce(this.initialState, () => {});

  close = (force) => () => {
    this.ref.handleClose(() => {
      const { screens } = this.state;

      if (screens.includes('PASSWORD') && !force) return this.setState({ screens: screens.filter((s) => s !== 'PASSWORD'), error: null });

      this.props.dispatch(close(LOGIN));
      this.setState(produce(this.initialState, () => {}));
    });
  };

  renderScreenPassword = () => {
    const { error, isShowingPassword } = this.state;

    return (
      <Popup ref={this.getRef} className="login-popup login-popup-child" animationStack={3}>
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent')} onClick={this.close()}>
            <i className={cx('lz', this.context.isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
          </Button>

          {this.getString('login')}
        </div>

        <div className="content">
          <div className="form-v2">
            <div className="form-item">
              <label htmlFor="password">{this.getString('password', 'profile')}</label>
              <div className="form-item-content">
                {isShowingPassword ? (
                  <i
                    key="eye"
                    className={cx('lz lz-eye-o postfix', { animate: this.state.animateShowingPassword })}
                    onClick={this.toggleShowPassword}
                  />
                ) : (
                  <i
                    key="eye-close"
                    className={cx('lz lz-eye-close postfix', { animate: this.state.animateShowingPassword })}
                    onClick={this.toggleShowPassword}
                  />
                )}
                <input
                  type={isShowingPassword ? 'text' : 'password'}
                  autoComplete="off"
                  name="password"
                  value={this.state.password}
                  onChange={(e) => this.handleInputChange('password')(e.target.value)}
                />
              </div>
            </div>

            <div className="form-item text-center">
              <Button type="primary" fw onClick={this.handleLoginPhone}>
                {this.getString('login')}
              </Button>

              {error && <div className="_error text-center mt-16">{error}</div>}

              <Button
                type="link"
                className="my-16"
                onClick={this.openDownloadAppBanner(
                  this.getString('forget_password_download_app'),
                  this.getString('forget_password_download_app_description'),
                )}
              >
                {this.getString('forget_password')}
              </Button>
            </div>
          </div>
        </div>
      </Popup>
    );
  };

  render() {
    const params = this.props.data.get('params') || {};
    const { error, screens, facebookInfo } = this.state;
    const typeYourPhone = facebookInfo ? 'please_type_your_phone_2' : 'please_type_your_phone';
    const description = iife(() => {
      if (params.description) {
        return (
          <p>{params.description}</p>);
      }
      return (
        <p>{this.getString(typeYourPhone)}</p>);
    });

    return (
      <Popup
        ref={this.getRef} className="login-popup"
        handleClose={screens.length ? this.close() : undefined}
        animationStack={3}
        wrapperStyle={params.wrapperStyle}
      >
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent')} onClick={this.close()}>
            <i className={cx('lz', this.context.isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
          </Button>

          {facebookInfo ? (
            this.getString('login_with_facebook')
          ) : (
            <img className="logo-loship" src="/dist/images/logo-footer.png" alt="logo-loship" />
          )}
        </div>

        <div className="content">
          {facebookInfo ? (
            <p>
              {this.getString('hi')} <b>{facebookInfo.name}</b>!
            </p>
          ) : (
            <div className="title">{this.getString('welcome_to_loship')}</div>
          )}

          {description}

          <InputPhoneNumber
            onChange={this.handleInputChange('phoneNumber')}
            onChangeCountryCode={this.handleInputChange('countryCode')}
            defaultCountryCode={this.state.countryCode}
            focus
          />

          <Button type="primary" fw onClick={this.handleInitialPhone}>
            {this.getString('continue')}
          </Button>

          {error && screens.length === 0 && <div className="_error text-center mt-16">{error}</div>}

          <div className="login-facebook">
            <div className="separator" />
            <div className="bg-white text-dark-gray px-8">{this.getString('or_1')}</div>

            <Button type="link" className="btn-facebook my-16" onClick={this.handleLoginFacebook}>
              <i className="lz lz-facebook" />
              <span>{this.getString('login_with_facebook')}</span>
            </Button>

            <Button type="link" className="btn-sm text-bold" href="https://loship.vn/quy-che" target="_blank">
              {this.getString('term_privacy')}
            </Button>
          </div>
        </div>

        {screens.includes('PASSWORD') && this.renderScreenPassword()}
      </Popup>
    );
  }

  getRef = (ref) => {
    if (ref) this.ref = ref;
  };

  handleInputChange = (key) => (value) => {
    this.setState({ [key]: value, error: null });
  };

  validate = (fields) => {
    if (fields.includes('phoneNumber')) {
      if (!this.state.phoneNumber) return this.setState({ error: this.getString('phone_number_is_required') });
      if (!isValidPhoneNumber(this.state.phoneNumber)) return this.setState({ error: this.getString('phone_number_is_invalid') });
    }

    if (fields.includes('countryCode')) {
      if (!this.state.countryCode) return this.setState({ error: this.getString('country_code_is_required') });
    }

    if (fields.includes('password')) {
      if (!this.state.password) return this.setState({ error: this.getString('password_is_required') });
    }

    return true;
  };

  handleInitialPhone = () => {
    if (!this.validate(['phoneNumber', 'countryCode'])) return;

    initialPhone({
      countryCode: this.state.countryCode,
      phoneNumber: this.state.phoneNumber,
      callback: ({ status }) => {
        if (status === 409) return this.setState({ screens: ['PASSWORD'] });
        return this.openDownloadAppBanner(
          this.getString('register_account'),
          this.getString('register_account_description'),
        )();
      },
      callbackFailure: this.handleLoginFailed,
    });
  };

  handleLoginPhone = () => {
    if (!this.validate(['phoneNumber', 'countryCode', 'password'])) return;

    loginByPhone({
      countryCode: this.state.countryCode,
      phoneNumber: this.state.phoneNumber,
      password: this.state.password,
      callback: ({ data, accessToken } = {}) => {
        const { profile = {}, status } = data;
        if (!profile.phoneNumber || status !== 'active') {
          return this.openDownloadAppBanner(
            this.getString('missing_information_download_app'),
            this.getString('missing_information_download_app_description'),
          )();
        }
        this.handleLoginWithData(data, accessToken);

        if (this.state.facebookInfo) {
          const { facebookInfo } = this.state;
          this.props.dispatch(
            linkFacebook({
              accessToken,
              fbAccessToken: facebookInfo.fbAccessToken,
              callback: () => {},
              callbackFailure: () => {
                this.props.dispatch(
                  open(CUSTOM_CONTENT, {
                    title: this.getString('account_link_failed', 'error'),
                    content: this.getString('account_link_failed_desc_2', '', [facebookInfo.name]),
                  }),
                );
              },
            }),
          );
        }
      },
      callbackFailure: this.handleLoginFailed,
    });
  };

  handleLoginFacebook = () => {
    loginByFacebook({
      callback: ({ data, accessToken } = {}) => {
        const { profile = {}, verifiedPhoneNumber } = data;
        if (!profile.phoneNumber || !verifiedPhoneNumber) {
          return this.openDownloadAppBanner(
            this.getString('missing_information_download_app'),
            this.getString('missing_information_download_app_description'),
          )();
        }
        this.handleLoginWithData(data, accessToken);
      },
      callbackFailure: async ({ status, message, fbAccessToken }) => {
        if (status === 404) {
          return openAccountFacebook({
            fbAccessToken,
            callback: ({ data }) => this.setState({ facebookInfo: { ...data, fbAccessToken } }),
            callbackFailure: this.handleLoginFailed,
          });
        }

        this.handleLoginFailed({ status, message });
      },
    });
  };

  handleLoginWithData = (data, accessToken) => {
    if (!accessToken) return;

    this.fetchMissingData();
    this.close(true)();

    return this.props.dispatch(loginWithData(data, accessToken));
  };

  handleLoginFailed = (err) => {
    if (!err.message) {
      return this.setState({
        error: this.getString(
          'unknown_error_please_contact_us_at_hotroloshiplozivn_sorry_and_thank_you_for_choosing_loship_today',
          '',
          [5005],
        ),
      });
    }

    if (err.message.indexOf('{"ERROR_CODE":"REACH_LIMIT"}') > -1) {
      return this.setState({
        error: this.getString(
          'your_phone_number_is_temporary_locked_due_to_excess_of_verification_requests_please_try_using_another_phone_number_or_retry_tomorrow',
        ),
      });
    }

    const error = parseErrorV2(err);
    this.setState({ error: this.getString(error.codeEnum || error.message, 'error', [error.code]) });
  };

  toggleShowPassword = () => {
    this.setState((state) => ({ isShowingPassword: !state.isShowingPassword, animateShowingPassword: true }));
  };

  fetchMissingData = () => {
    const params = this.props.data.get('params') || {};
    if (typeof params.callback === 'function') params.callback();
  };

  openDownloadAppBanner = (title, description) => () => {
    this.props.dispatch(
      open(ORDER_FAILED, {
        title,
        error: iife(() => (
          <div style={{ maxWidth: 500 }}>
            <p>{description}</p>
            <div>
              <a href="https://goo.gl/N5Vs4V" className="app-img app-android" target="_blank" rel="noopener">
                <img src="/dist/images/app-android.png" alt="android" style={{ maxWidth: 120, marginRight: 8 }} />
              </a>
              <a href="https://goo.gl/CDFMhZ" className="app-img app-ios" target="_blank" rel="noopener">
                <img src="/dist/images/app-ios.png" alt="ios" style={{ maxWidth: 120 }} />
              </a>
            </div>
          </div>
        )),
        hideBanner: true,
        action: () => { window.location.href = '/'; },
        actionText: this.getString('return_to_homepage'),
      }),
    );
  };
}

export default initComponent(LoginPopup);
