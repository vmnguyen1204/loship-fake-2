import React from 'react';

import DishSelectPopup from 'components/shared/popup/dish-select';
import OrderFailedPopup from 'components/shared/popup/order-failed';
import LoginPopup from 'components/shared/popup/login';
import CustomContentPopup from 'components/shared/popup/custom-content';
import ConfirmPopup from 'components/shared/popup/confirm-popup';

import StopService from 'components/shared/popup/stop-service-popop';
import SelectPaymentMethodPopup from 'components/shared/payment/popup';
import SelectBankPopup from 'components/shared/payment/popup-bank';

import UpdateAvatar from 'components/shared/popup/update-avatar';
import PhotoPopup from 'components/shared/popup/photo-popup';
import GroupOrderInvitePopup from 'components/shared/popup/group-order/_invite';
import GroupOrderMemberListPopup from 'components/shared/popup/group-order/_member-list';

import SharingPopup from 'components/shared/popup/share';
import RatingPopup from 'components/shared/popup/rating';
import MerchantRatingPopup from 'components/shared/popup/rating/merchant';
import GmapPopup from 'components/shared/popup/gmap';

import ProfileUpdateInfoPopup from 'components/shared/popup/profile/update-info';
import ProfileUpdatePhonePopup from 'components/shared/popup/profile/update-phone';
import ProfileChangePasswordPopup from 'components/shared/popup/profile/change-password';
import GenericPopup from 'components/shared/popup/generic';
import MerchantThoughtfulPopup from 'components/shared/popup/merchant-thoughtful';


if (process.env.BROWSER) {
  require('assets/styles/components/popup.scss');
}

class PopupContainer extends React.Component {
  render() {
    return (
      <div>
        {/** APPLICATION LEVEL */}
        <ProfileUpdateInfoPopup />
        <ProfileUpdatePhonePopup />
        <ProfileChangePasswordPopup />

        <RatingPopup />
        <MerchantRatingPopup />

        <GmapPopup />
        <SelectPaymentMethodPopup />
        <SelectBankPopup />

        <GroupOrderInvitePopup />
        <GroupOrderMemberListPopup />
        <SharingPopup />

        <StopService />
        <LoginPopup />
        <UpdateAvatar />
        <DishSelectPopup />
        <OrderFailedPopup />
        <PhotoPopup />
        <MerchantThoughtfulPopup />

        {/** TOP LEVEL */}
        <GenericPopup />
        <ConfirmPopup />
        <CustomContentPopup />
      </div>
    );
  }
}

export default PopupContainer;
