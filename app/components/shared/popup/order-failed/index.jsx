import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { ORDER_FAILED, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import AppBanner from 'components/shared/banners';
import Button from 'components/shared/button';

class OrderFailedPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: ORDER_FAILED },
  };

  close = () => {
    this.props.dispatch(close(ORDER_FAILED));
  };

  goBack = () => {
    const params = this.props.data && this.props.data.get('params');
    const { action } = params;

    typeof action === 'function' && action();
    this.close();
  };

  render() {
    const params = this.props.data && this.props.data.get('params');
    const { title, error } = params;

    return (
      <Popup
        className="order-failed"
        handleClose={params.canNotDismiss ? () => {} : this.close}
        wrapperStyle={params.wrapperStyle}
      >
        <div className="popup-header">{title || this.getString('unable_to_place_order')}</div>

        <div className="content">
          {!error && <p>{this.getString('we_are_sorry_for_this_inconvenience')}</p>}
          {typeof error === 'function' ? (
            error()
          ) : (
            <p>{error || this.getString('please_try_order_again_after_30_seconds')}</p>
          )}
          {params.canNotDismiss && (
            <p>
              <Button type="primary" onClick={this.goBack}>
                {this.getString('choose_another')}
              </Button>
            </p>
          )}
        </div>
        {!params.hideBanner && <AppBanner isMobile={this.context.isMobile} />}
      </Popup>
    );
  }
}

export default initComponent(OrderFailedPopup);
