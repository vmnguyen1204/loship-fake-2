import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { STOP_SERVICE_POPUP, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import { STOP_SERVING_TIMESTAMP } from 'settings/variables';
import Button from 'components/shared/button';

class StopServicePopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: STOP_SERVICE_POPUP },
  };

  close = () => {
    this.props.dispatch(close(STOP_SERVICE_POPUP));
  };

  render() {
    const date = new Date(Date.parse(STOP_SERVING_TIMESTAMP));
    let hour = date.getHours();
    let minute = date.getMinutes();
    if (hour < 10) hour = `0${hour}`;
    if (minute < 10) minute = `0${minute}`;

    return (
      <Popup className="order-suspended" handleClose={this.close}>
        <div className="popup-header">{this.getString('thank_you_for_using_loship', 'overload')}</div>

        <div className="content">
          <p>
            {this.getString(
              'we_are_currently_overloaded_and_could_not_serve_all_of_your_orders_we_are_temporarily_stop_taking_new_orders_in_2_hours_in_order_to_finish_all_the_remaining_orders_we_thank_you_for_your_patience_and_please_come_back_after',
              'overload',
            )}
            <b className="text-red">
              {hour}:{minute}
            </b>
            {this.getString('4oMKj2vSrah', 'overload')}
            {date.getDate()}/{date.getMonth() + 1}.
          </p>
        </div>

        <div className="popup-footer">
          <Button className="btn-long" onClick={this.close}>
            {this.getString('ok')}
          </Button>
        </div>
      </Popup>
    );
  }
}

export default initComponent(StopServicePopup);
