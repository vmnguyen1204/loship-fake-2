import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

if (process.env.BROWSER) {
  require('assets/styles/components/popup.scss');
  require('assets/styles/components/popup-mobile.scss');
}

function PopupBackground({ onClick }) {
  return <div className="popup-bg" onClick={onClick} />;
}

const ANIMATION_STACK = [
  {},
  { mobile: 'fly-up' },
  {
    mobile: 'fly-up',
    desktop: 'fade-in',
  },
  {
    mobile: 'slide-rtl',
    speedup: 1.5,
  },
];

function calcAnimationSpeed(animationStack, animationSpeed, isMobile) {
  const animation = ANIMATION_STACK[animationStack] || {};

  if (!animationSpeed) return 0.5 / (animation.speedup || 1);
  return (isMobile ? animationSpeed : animationSpeed / 5) / (animation.speedup || 1);
}

class Popup extends React.Component {
  static propTypes = {
    children: PropTypes.any.isRequired,
    animationSpeed: PropTypes.number,
    wrapperStyle: PropTypes.object,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  static defaultProps = {
    animationSpeed: 0.5,
    wrapperStyle: {},
  };

  state = {};

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  render() {
    const { className, wrapperClassName } = this.props;
    const { animation, style } = this.getAnimation();

    return (
      <div className={cx('popup-wrapper', wrapperClassName)} style={this.getWrapperStyle()}>
        <div
          ref={this.getRef}
          className={cx('popup', className, animation, { active: this.state.animationActived })}
          style={style}
        >
          {this.props.children}
        </div>
        <PopupBackground onClick={this.props.handleClose} />
      </div>
    );
  }

  getRef = (ref) => {
    if (ref) this.ref = ref;
  };

  getAnimation = () => {
    const { animationStack = 0 } = this.props;
    const animation = ANIMATION_STACK[animationStack][this.context.isMobile ? 'mobile' : 'desktop'];
    if (!animation) return {};
    const animationSpeed = calcAnimationSpeed(animationStack, this.props.animationSpeed, this.context.isMobile);

    this.state.animationActived === undefined && setTimeout(() => this.setState({ animationActived: true }), 0);
    const style = animation ? { transition: `all ${animationSpeed}s` } : {};

    return { animation, style };
  };

  getWrapperStyle = () => {
    const style = { ...this.props.wrapperStyle };

    if (this.props.wrapperMode === 'grow' && this.ref) {
      style.height = this.ref.clientHeight;
    }

    return style;
  };

  handleKeyDown = (e) => {
    if (!e) return;
    if (e.keyCode === 27) this.props.handleClose?.();
  };

  handleClose = (callback) => {
    const handleClose = typeof callback === 'function' ? callback : this.props.handleClose || (() => {});
    if (typeof handleClose !== 'function') return console.error('No or invalid close handler provided');
    if (!this.state.animationActived) return handleClose();

    const { animationStack = 0 } = this.props;
    const animationSpeed = calcAnimationSpeed(animationStack, this.props.animationSpeed, this.context.isMobile);
    this.setState({ animationActived: false }, () => {
      setTimeout(handleClose, (animationSpeed / 2) * 1000);
    });
  };
}

export default Popup;
