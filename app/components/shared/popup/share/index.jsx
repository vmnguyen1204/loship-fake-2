import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  LineIcon,
  LineShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  PocketIcon,
  PocketShareButton,
  RedditIcon,
  RedditShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  ViberIcon,
  ViberShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from 'react-share';
import { SHARING_POPUP, close } from 'actions/popup';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Popup from 'components/shared/popup';
import PropTypes from 'prop-types';
import React from 'react';
import { get as getPopup } from 'reducers/popup';
import initComponent from 'lib/initComponent';

class PopupSharing extends BaseComponent {
  static defaultProps = {
    dataGetter: getPopup,
    params: { popup: SHARING_POPUP },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  close = () => {
    return this.ref.handleClose(() => this.props.dispatch(close(SHARING_POPUP)));
  };

  render() {
    const params = this.props.data.get('params');
    const { url, subject, body } = params;

    if (navigator.share) {
      navigator.share({ title: subject, text: body, url }).catch((error) => console.error('Error sharing', error));
      return null;
    }

    return (
      <Popup
        ref={this.getRef}
        className="invite-popup"
        handleClose={this.close}
        wrapperStyle={{ zIndex: 201 }}
        animationStack={2}
      >
        <div className="popup-header">
          <Button className="btn-back right bg-transparent" onClick={this.close}>
            <i className="lz lz-close" />
          </Button>
          {body}
          <br />
          {url}
        </div>

        <div className="content">
          <div className="share-items">
            <div className="share-item">
              <FacebookShareButton url={url} quote={body}>
                <FacebookIcon size={80} borderRadius={10} />
              </FacebookShareButton>
            </div>
            <div className="share-item">
              <TwitterShareButton url={url} title={body}>
                <TwitterIcon size={60} borderRadius={10} />
              </TwitterShareButton>
            </div>
            <div className="share-item">
              <LinkedinShareButton url={url} title={body}>
                <LinkedinIcon size={80} borderRadius={10} />
              </LinkedinShareButton>
            </div>
            <div className="share-item">
              <TelegramShareButton url={url} title={body}>
                <TelegramIcon size={60} borderRadius={10} />
              </TelegramShareButton>
            </div>
            <div className="share-item">
              <EmailShareButton url={url} subject={subject} body={body}>
                <EmailIcon size={60} borderRadius={10} />
              </EmailShareButton>
            </div>
            <div className="share-item">
              <WhatsappShareButton url={url} title={body}>
                <WhatsappIcon size={60} borderRadius={10} />
              </WhatsappShareButton>
            </div>
            <div className="share-item">
              <ViberShareButton url={url} title={body}>
                <ViberIcon size={60} borderRadius={10} />
              </ViberShareButton>
            </div>
            <div className="share-item">
              <LineShareButton url={url} title={body}>
                <LineIcon size={60} borderRadius={10} />
              </LineShareButton>
            </div>
            <div className="share-item">
              <RedditShareButton url={url} title={body}>
                <RedditIcon size={60} borderRadius={10} />
              </RedditShareButton>
            </div>
            <div className="share-item">
              <PocketShareButton url={url} title={body}>
                <PocketIcon size={60} borderRadius={10} />
              </PocketShareButton>
            </div>
          </div>
        </div>
      </Popup>
    );
  }

  getRef = (ref) => {
    if (ref) this.ref = ref;
  };
}

export default initComponent(PopupSharing);
