import { GMAP_POPUP, close } from 'actions/popup';

import BaseComponent from 'components/base';
import Gmap from 'components/shared/gmap';
import Popup from 'components/shared/popup';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { get as dataGetter } from 'reducers/popup';
import initComponent from 'lib/initComponent';

const initialState = { trackingLocation: undefined };

class GmapPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: GMAP_POPUP },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = initialState;
  }

  close = () => {
    const params = this.props.data.get('params');

    this.setState(initialState);
    this.props.dispatch(close(GMAP_POPUP));
    typeof params.handleClose === 'function' && params.handleClose();
  };

  render() {
    const { isMobile } = this.context;
    const { trackingLocation } = this.state;
    const { data } = this.props;
    const params = (data && data.get('params')) || {};

    const { centerIcon, markers, mode, popupClassName } = params;

    return (
      <Popup className={cx('gmap-popup', popupClassName)} handleClose={this.close}>
        <Gmap
          defaultCenter={{ lat: 0, lng: 0 }}
          center={{ position: trackingLocation, icon: centerIcon, hideInfo: true }}
          markers={markers}
          mode={mode}
        />
        {isMobile && (
          <a className="btn-back" onClick={this.close}>
            <i className="fas fa-arrow-left" />
          </a>
        )}
        <a className="btn-refresh" onClick={this.refreshTrackingMap}>
          <i className="fas fa-redo" />
        </a>
      </Popup>
    );
  }

  refreshTrackingMap = (props) => {
    const { data } = props || this.props;
    const params = (data && data.get('params')) || {};

    const { handleRefresh } = params;
    if (typeof handleRefresh !== 'function') return;

    handleRefresh((locations) => {
      if (!Array.isArray(locations) || !locations[0]) return;
      this.setState({ trackingLocation: locations[0] });
    });
  };
}

export default initComponent(GmapPopup);
