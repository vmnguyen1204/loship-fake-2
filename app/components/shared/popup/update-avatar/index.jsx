import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';

import { get as dataGetter } from 'reducers/popup';
import { CUSTOM_CONTENT, UPDATE_AVATAR, close, open } from 'actions/popup';
import Popup from 'components/shared/popup';
import { updateAvatar } from 'actions/merchant-manager';
import Button from 'components/shared/button';
import FileUploader from 'components/statics/uploader';

class UpdateAvatarPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: UPDATE_AVATAR },
  };

  close = () => {
    this.props.dispatch(close(UPDATE_AVATAR));
  };

  clearAvatar = () => {
    this.setState((state) => ({ ...state, avatarData: undefined, avatar: undefined }));
  };

  handleFileUpload = (data, thumb) => {
    this.setState((state) => ({ ...state, avatarData: data, avatar: thumb }));
  };

  onSubmit = async () => {
    const { avatarData } = this.state;
    const { data } = this.props;
    const params = (data && data.get('params')) || {};

    if (!params.merchantId) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('merchant_not_found'),
          description: '',
        }),
      );
    }

    if (!avatarData) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('image_not_found'),
          description: '',
        }),
      );
    }

    const avatar = {
      image: await avatarData.getURL(),
      avatar: await avatarData.getThumb(),
      callback: this.close,
    };

    this.props.dispatch(updateAvatar(params.merchantId, avatar));
  };

  render() {
    const { data } = this.props;
    const params = (data && data.get('params')) || {};

    const { avatar } = this.state;

    return (
      <Popup className="popup-update-avatar" handleClose={() => {}}>
        {params.title && <div className="popup-header">{params.title}</div>}

        <div className="content">
          {params.description && <p>{params.description}</p>}

          {!avatar ? (
            <FileUploader className="uploader" onChange={this.handleFileUpload} accept="image/*" minSize={[1000, 1000]}>
              <i className="fa fa-upload" />
              <h5>{this.getString('click_to_upload')}</h5>
            </FileUploader>
          ) : (
            <div className="avatar">
              <img src={avatar} alt="Avatar" />
              <i className="fa fa-times close" onClick={this.clearAvatar} />
            </div>
          )}
        </div>

        <div className="popup-footer">
          <Button type="feature" disabled={!avatar} onClick={this.onSubmit}>
            {(params && params.actionText) || this.getString('save_change', 'profile')}
          </Button>
        </div>
      </Popup>
    );
  }
}

export default initComponent(UpdateAvatarPopup);
