import { GENERIC_POPUP, close } from 'actions/popup';
import { debouncer, produce } from 'utils/helper';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Checkbox from 'components/shared/checkbox';
import Popup from 'components/shared/popup';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { get as dataGetter } from 'reducers/popup';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';

/**
 * !TODO: clean this shit too
 * ! this is a clone of Confirm popup due to limitation
 * ! we can only have only 1 of each popup types.
 * ! Popup should never be worked that way.
 */
class GenericPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: GENERIC_POPUP },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  initialState = { isAllowedClose: true, inverted: false, searchValue: '', isDenied: false };

  state = produce(this.initialState);

  componentDidUpdate() {
    if (!this.props.data) {
      if (this.scrollElem) {
        this.scrollElem.removeEventListener('scroll', this.detectScrolling);
        delete this.scrollElem;
      }
      return;
    }

    this.handleRef();
  }

  close = (confirm = false) => {
    if (!this.state.isAllowedClose) return;
    const params = (this.props.data && this.props.data.get('params')) || {};

    if (confirm !== true && typeof params.onCancel === 'function') {
      params.onCancel();
    }
    if (confirm === true && typeof params.onConfirm === 'function') {
      if (typeof params.validate === 'function') {
        const err = params.validate();
        if (err) return this.setState({ err });
      }
      params.onConfirm();
      if (params.preventCloseOnConfirm) return;
    }
    if (this.state.isDenied && typeof params.onDenied === 'function') {
      params.onDenied();
    }

    this.handleSearchClear();
    if (params.animationStack) {
      return this.ref.handleClose(() => this.props.dispatch(close(GENERIC_POPUP)));
    }
    this.props.dispatch(close(GENERIC_POPUP));
  };

  renderContent = (content, index) => {
    if (Array.isArray(content)) return content.map(this.renderContent);
    if (React.isValidElement(content)) return content;
    if (typeof content === 'function') {
      return content({
        handleClose: this.close,
        allowedClose: this.setAllowedClose,
        searchValue: this.state.searchValue,
      });
    }
    return <p key={index}>{content}</p>;
  };

  render() {
    const params = this.props.data.get('params') || {};

    const showFooter = !!params.confirmBtn || !!params.cancelBtn;
    const showBtnBack = !!params.fullscreen || !showFooter;
    const { confirmBtn = {}, cancelBtn = {}, deniedBtn = {}, animationStack } = params;

    return (
      <Popup
        ref={this.handleRef}
        className={cx('generic-popup confirm-popup', params.className, {
          fullscreen: params.fullscreen,
          inverted: this.state.inverted,
          search: params.mode === 'SEARCH',
        })}
        wrapperStyle={params.wrapperStyle}
        wrapperClassName={params.wrapperClassName}
        wrapperMode={params.wrapperMode}
        handleClose={params.canNotDismiss ? () => {} : this.close}
        animationStack={animationStack}
      >
        {(() => {
          if (params.mode === 'SEARCH') {
            return (
              <div className="popup-header">
                {showBtnBack && (
                  <Button className={cx('btn-back left bg-transparent')} onClick={this.close}>
                    {params.closeBtnClassName ? (
                      <i className={cx('lz', params.closeBtnClassName)} />
                    ) : (
                      <i className={cx('lz', this.context.isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
                    )}
                  </Button>
                )}
                <input
                  type="text"
                  ref={this.handleSearchRef}
                  className="search-box-input"
                  placeholder={params.searchPlaceholder}
                  onChange={this.handleSearch}
                  onKeyUp={this.handleSearchAction}
                />
                <i
                  className={cx('lz action', this.state.searchValue ? 'lz-close' : 'lz-search')}
                  onClick={this.handleSearchClear}
                />
              </div>
            );
          }
          if (typeof params.header === 'function') return params.header({ handleClose: this.close });
          if (params.title) {
            return (
              <div className="popup-header">
                {showBtnBack && (
                  <Button className={cx('btn-back bg-transparent')} onClick={this.close}>
                    {params.closeBtnClassName ? (
                      <i className={cx('lz', params.closeBtnClassName)} />
                    ) : (
                      <i className={cx('lz', this.context.isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
                    )}
                  </Button>
                )}
                {params.title}
              </div>
            );
          }
        })()}

        <div id="popup-content" className={cx('content', params.contentClassName)}>
          {params.description && <p>{params.description}</p>}
          {this.renderContent(params.content)}
        </div>

        {showFooter && (
          <div className={cx('popup-footer', params.actionClassName)}>
            {!!params.cancelBtn && (
              <Button type={cancelBtn.type} className={cancelBtn.className} onClick={() => this.close(false)}>
                {cancelBtn.text || this.getString('close')}
              </Button>
            )}
            {!!params.confirmBtn && (
              <Button
                type={confirmBtn.type || 'primary'}
                className={cx('btn-confirm', confirmBtn.className)}
                onClick={() => this.close(true)}
              >
                {confirmBtn.text || this.getString('ok')}
              </Button>
            )}
          </div>
        )}

        {typeof params.onDenied === 'function' && (
          <Checkbox
            className={cx('btn-denied', deniedBtn.className)}
            checked={this.state.isDenied}
            onChange={(checked) => this.setState({ isDenied: checked })}
          >
            {deniedBtn.text || this.getString('dont_show_again')}
          </Checkbox>
        )}
      </Popup>
    );
  }

  handleRef = (ref) => {
    if (ref) this.ref = ref;
    if (!this.ref) return;

    const params = this.props.data.get('params') || {};

    if (params.autoCloseTimeout > 0) {
      clearTimeout(this.autoCloseTimeout);
      this.autoCloseTimeout = setTimeout(() => this.close(false), params.autoCloseTimeout);
    }

    if (params.bypassTargetId) {
      if (!this.scrollElem) {
        this.scrollElem = document.getElementById('popup-content');
        this.scrollElem && this.scrollElem.addEventListener('scroll', this.detectScrolling);
        this.detectScrolling(); // do it for the first time
      }
    }

    if (typeof params.autoCloseHandler === 'function') {
      const shouldAutoClose = params.autoCloseHandler.call(this);
      if (shouldAutoClose) this.close();
    }
  };

  handleSearchRef = (ref) => {
    if (ref) this.searchRef = ref;
    if (!this.searchRef) return;

    this.searchRef.focus();
    this.searchRef.value = '';
  };

  detectScrolling = () => {
    const params = (this.props.data && this.props.data.get('params')) || {};
    if (!params.bypassTargetId) return;

    const targetElem = document.getElementById(params.bypassTargetId);
    if (!targetElem) return;

    const isBypassed = targetElem.getBoundingClientRect().bottom < 44;
    if (!isBypassed && !this.state.inverted) this.setState({ inverted: true });
    else if (isBypassed && this.state.inverted) this.setState({ inverted: false });
  };

  handleSearchAction = (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (e.keyCode === 27) return this.handleSearchClear();
    if (e.keyCode === 13) return this.handleSearch();
  };

  handleSearchClear = () => {
    this.setState({ searchValue: '' });
    this.searchRef && (this.searchRef.value = '');
  };

  handleSearch = () => {
    const params = this.props.data.get('params') || {};

    let value = this.searchRef && this.searchRef.value;
    if (typeof params.searchFormatter === 'function') {
      value = params.searchFormatter(value);
      this.searchRef && (this.searchRef.value = value);
    }

    this.searchTimeout = debouncer(this.searchTimeout, () => this.setState({ searchValue: value }), 250);
  };

  setAllowedClose = (isAllowedClose) => {
    this.setState({ isAllowedClose });
  };

  handleDeniedCheckLink = () => {
    return {
      checked: this.state.isDenied,
      onChange: (checked) => this.setState({ isDenied: checked }),
    };
  };
}

export default initComponent(GenericPopup, withRouter);
