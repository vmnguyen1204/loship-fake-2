import React from 'react';
import BaseComponent from 'components/base';

class PopupBackground extends BaseComponent {
  close = () => {
    return this.props.onClick();
  };

  render() {
    return <div className="popup-bg" onClick={this.close} />;
  }
}

export default PopupBackground;
