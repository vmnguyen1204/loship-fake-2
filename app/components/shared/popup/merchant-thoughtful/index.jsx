import { MERCHANT_THOUGHTFUL_POPUP, close } from 'actions/popup';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { get as getPopup } from 'reducers/popup';
import { getMerchantSupplied } from 'reducers/merchant';
import { fetchMerchantListItem } from 'actions/merchant';
import { withRouter } from 'react-router-dom';
import Button from 'components/shared/button';

class MerchantThoughtfulPopup extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (!data) return;

      const eateryId = data.getIn(['data', 'params'])?.eateryId;
      const MerchantSupplied = getMerchantSupplied(state, { merchantUsername: eateryId });
      if (MerchantSupplied) data = data.set('merchantsupplied', MerchantSupplied.get('data'));

      return data;
    },
    params: { popup: MERCHANT_THOUGHTFUL_POPUP },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  componentDidUpdate() {

    if (!this.props.data || !this.props.data.get('params')) return;
    if (this.props.merchantsupplied) return;
    const params = this.props.data.get('params');
    this.props.dispatch(
      fetchMerchantListItem({
        params: {
          merchantUsername: params.eateryId,
        },
      }),
    );
  }

  close = () => {
    this.props.dispatch(close(MERCHANT_THOUGHTFUL_POPUP));
  };

  render() {
    const params = this.props.data.get('params') || {};
    const { merchantsupplied } = this.props;
    const { isMobile } = this.context;

    return (
      <Popup className={cx(!isMobile?'merchant-thoughtful':'merchant-thoughtful-mobile')} handleClose={this.close}
        wrapperClassName={params.wrapperClassName}
      >
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent')} onClick={this.close}>
            <i className={cx('lz', { 'lz-light-arrow-left': isMobile, 'lz-close': !isMobile })} />
          </Button>
          {this.getString('merchant_thoughtful')}
        </div>
        <div className="content">
          <div className="bg-white losupply-merchant-cover">
            <div className="title">{this.getString('supporting_losupply_when_ordering')}</div>
            {merchantsupplied?.map((data) => {
              const ImgSupply = data.dishes.map((item) => {
                return (
                  <img key={item.id} src={item.avatar} />
                );
              });

              return (
                <div key={data.id} className="losupply-merchant">
                  <div className="losupply-merchant-list">
                    <div className="name-list-losupply">
                      <p>{data.name}</p>
                    </div>
                    <div className="img-list-losupply">
                      {ImgSupply}
                    </div>
                  </div>
                  <div className="horizantal"></div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="popup-footer">
          <div className="footer-wraper">
            <div className="title-footer">{this.getString('provided_by')}</div>
            <img src="/dist/images/logo.png" />
          </div>
        </div>
      </Popup>
    );
  }
}
export default initComponent(MerchantThoughtfulPopup, withRouter);
