import { ratingOrder } from 'actions/order';
import { RATING_POPUP, close } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import InputAutoGrow from 'components/shared/input/auto-grow';
import Popup from 'components/shared/popup';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { get as dataGetter } from 'reducers/popup';
import { BAD_RATING_LENGTH, GOOD_RATING_LENGTH } from 'settings/constants';

const ratings = ['good', 'neutral', 'bad'];
const ratingsMap = { good: 'recommended', neutral: 'neutral', bad: 'not_recommended' };
const initialState = { reviewDescription: '', rows: 3, minRows: 1, maxRows: 7, rating: 'good' };

class RatingPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: RATING_POPUP },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentDidUpdate(prevProps) {
    const prevParams = prevProps.data?.get('params') || {};
    const params = this.props.data?.get('params') || {};
    const { rating } = params;

    if (rating && !prevParams.rating) {
      this.setState({ rating: rating.rating, reviewDescription: rating.review });
    }
  }

  close = () => {
    const params = this.props.data.get('params');

    this.setState(initialState);
    this.props.dispatch(close(RATING_POPUP));
    typeof params.callback === 'function' && params.callback();
  };

  render() {
    const { isMobile } = this.context;
    const params = this.props.data.get('params');
    const { shipper, rating: ratingData, code } = params;

    const { rating, reviewDescription, sending, error } = this.state;
    const wordCount = (reviewDescription && reviewDescription.trim().split(/[\s\n,]+/).length) || 0;
    const reviewLength = (reviewDescription && reviewDescription.length) || 0;

    const counter = (() => {
      if (rating === 'bad') {
        return wordCount < BAD_RATING_LENGTH
          ? this.getString('0_words_more', 'rating', [BAD_RATING_LENGTH - wordCount])
          : this.getString('01_letters', 'rating', [reviewLength, 5000]);
      }

      if (reviewLength > 0) {
        return wordCount < GOOD_RATING_LENGTH
          ? this.getString('0_words_more', 'rating', [GOOD_RATING_LENGTH - wordCount])
          : this.getString('01_letters', 'rating', [reviewLength, 5000]);
      }

      return '';
    })();

    return (
      <Popup className="rating-popup" handleClose={this.close}>
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent', { 'text-red': isMobile })} onClick={this.close}>
            <i className={cx('lz', { 'lz-light-arrow-left': isMobile, 'lz-close': !isMobile })} />
          </Button>
          {this.getString(ratingData ? 'view_rating_shipper' : 'rate_our_shipper', 'rating')}
        </div>

        <div className="content">
          <div className="shipper-item p8_16">
            <p className="text-gray">
              {this.getString('rate_our_shipper', 'rating')}
              <span className="pull-right">{this.getString('code_0', '', [code])}</span>
            </p>
            <div className="avatar circle">
              <img src={shipper.avatar || '/dist/images/avatar.png'} alt="Avatar" />
            </div>
            <div className="detail">
              <p className="title">
                <b>{shipper.name}</b>
              </p>
            </div>
          </div>

          <div className={cx('rating-wrapper', { readonly: !!ratingData })}>
            <div className="rating">
              {!ratingData &&
              ratings.map((r) => (
                <div
                  key={r}
                  className={cx('rating-item', ratingsMap[r], { active: r === rating })}
                  onClick={() => {
                    this.setRating(r);
                    if (this.refs.txtReview) this.refs.txtReview.focus();
                  }}
                >
                  <img src={`/dist/images/${r}-face.png`} alt={r} />
                  <br />
                  <span>{this.getString(ratingsMap[r], 'rating')}</span>
                </div>
              ))}
              {!!ratingData && (
                <div className={cx('rating-item', ratingsMap[rating])}>
                  <img src={`/dist/images/${rating}-face.png`} alt={rating} />
                  <br />
                  <span>{this.getString(ratingsMap[rating], 'rating')}</span>
                </div>
              )}
            </div>
          </div>

          <div className="review-description-wrapper">
            {!ratingData ? (
              <InputAutoGrow
                className="review-description input" minRows={3} maxRows={10}
                onChange={this.handleChangeDescription}
                placeholder={this.getString('does_shipper_call_you_to_confirm_your_order_does_himher_delivery_on_time_please_rate_for_improving_loship')}
              />
            ) : (
              <div className="review-description">{this.state.reviewDescription}</div>
            )}
            {!ratingData && <div className="review-count">{counter}</div>}
            {!ratingData && error && <div className="_error">{error}</div>}
          </div>
        </div>

        {!ratingData && (
          <div className="popup-footer">
            <Button type="primary" fw disabled={sending} onClick={this.handleSubmit}>
              {this.getString(sending ? 'sending' : 'send', 'rating')}
            </Button>
          </div>
        )}
      </Popup>
    );
  }

  handleSubmit = async () => {
    if (this.state.sending) return;

    const params = this.props.data.get('params');
    const { code } = params;

    this.setState({ sending: true }, async () => {
      try {
        const { reviewDescription, rating } = this.state;
        const wordCount = (reviewDescription && reviewDescription.trim().split(/[\s\n,]+/).length) || 0;
        const reviewLength = (reviewDescription && reviewDescription.length) || 0;

        if (rating === 'bad' && reviewDescription === '') {
          throw new Error(
            this.getString(
              'we_are_sorry_for_your_bad_experience_please_write_your_review_so_we_can_improve_our_service_and_wont_let_that_happen_again',
              'rating',
            ),
          );
        }
        if (reviewLength > 0) {
          if (rating === 'bad' && wordCount < BAD_RATING_LENGTH) {
            throw new Error(
              this.getString(
                'oh_no_we_are_sorry_for_your_bad_experience_what_happened_tell_us_more_and_we_wont_let_that_happen_again_min_15_words',
                'rating',
              ),
            );
          } else if (rating !== 'bad' && wordCount < GOOD_RATING_LENGTH) {
            throw new Error(
              this.getString(
                'tell_us_more_how_was_your_food_was_the_shipper_nice_do_you_think_there_is_anything_loship_should_improve_min_3_words',
                'rating',
              ),
            );
          }
        }
        await this.props.dispatch(ratingOrder(code, rating, reviewDescription));
        this.setState({ sending: false });
        this.close();
      } catch (e) {
        this.setState({ error: this.getString(e.message, 'rating'), sending: false });
      }
    });
  };

  handleChangeDescription = (value) => {
    this.setState({
      reviewDescription: value.slice(0, 5000),
      error: '',
    });
  };

  setRating = (rating) => {
    this.setState({ rating });
  };
}

export default initComponent(RatingPopup);
