import { sendGAEvent } from 'actions/factory';
import { ratingMerchant } from 'actions/order';
import { GENERIC_POPUP, MERCHANT_RATING_POPUP, SHARING_POPUP, close, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import InputAutoGrow from 'components/shared/input/auto-grow';
import Popup from 'components/shared/popup';
import FileUploader from 'components/statics/uploader';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { toast } from 'react-toastify';
import { get as dataGetter } from 'reducers/popup';
import { BAD_RATING_LENGTH, GOOD_RATING_LENGTH } from 'settings/constants';
import { LINK_DOMAIN } from 'settings/variables';
import { copyToClipboard } from 'utils/content';
import { genShortId } from 'utils/data';
import { iife, produce } from 'utils/helper';
import * as cookie from 'utils/shims/cookie';
import Link from 'utils/shims/link';

const ratings = ['recommended', 'not_recommended'];
const ratingsMap = { recommended: 'good', not_recommended: 'bad' };

const initialState = {
  reviewDescription: '',
  rows: 1,
  minRows: 1,
  maxRows: 7,
  photos: [],
  rating: 'recommended',
  ratingSuccess: null,
};

class MerchantRatingPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: MERCHANT_RATING_POPUP },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = produce(initialState);
  }

  componentDidUpdate(prevProps) {
    const prevParams = prevProps.data?.get('params') || {};
    const params = this.props.data?.get('params') || {};
    const { rating } = params;

    if (rating && !prevParams.rating) {
      this.setState({ rating: rating.rating, reviewDescription: rating.content });
    }
  }

  close = () => {
    const params = this.props.data.get('params');

    this.setState(produce(initialState));
    this.props.dispatch(close(MERCHANT_RATING_POPUP));
    typeof params.callback === 'function' && params.callback();
  };

  renderMerchantInfo = (merchant, code) => {
    const { street, city, district } = merchant.address || {};
    const fullAddress = `${street} ${district} ${city}`;
    const merchantUrl = merchant.username ? `/${merchant.username}` : `/b/${merchant.slug}`;

    return (
      <>
        <p className="merchant-item-header">
          {this.getString('location')}
          <span className="pull-right">{this.getString('code_0', '', [code])}</span>
        </p>
        <Link to={merchantUrl} className="merchant-item" onClick={() => this.props.dispatch(close(MERCHANT_RATING_POPUP))}>
          <div className="avatar">
            <img src={merchant.avatar || '/dist/images/avatar.png'} alt="Avatar" />
          </div>
          <div className="detail">
            <p className="title">
              <b>{merchant.name}</b>
            </p>
            <p className="address">{fullAddress}</p>
          </div>
        </Link>
      </>
    );
  };

  renderRating = (rating, hasRating) => {
    return (
      <div className={cx('rating-wrapper', { readonly: hasRating })}>
        <div className="rating">
          {!hasRating &&
          ratings.map((r) => (
            <div
              key={r}
              className={cx('rating-item', r, { active: r === rating })}
              onClick={() => {
                this.setRating(r);
                if (this.refs.txtReview) this.refs.txtReview.focus();
              }}
            >
              <img src={`/dist/images/${ratingsMap[r]}-face.png`} alt={ratingsMap[r]} />
              <br />
              <span>{this.getString(r, 'rating')}</span>
            </div>
          ))}
          {hasRating && (
            <div className={cx('rating-item', rating)}>
              <img src={`/dist/images/${ratingsMap[rating]}-face.png`} alt={ratingsMap[rating]} />
              <br />
              <span>{this.getString(rating, 'rating')}</span>
            </div>
          )}
        </div>
      </div>
    );
  };

  renderPhotos = (photos, ratingData) => {
    const ratingPhotos = (ratingData && ratingData.photos) || [];
    const photosWrapperStyles = iife(() => {
      if (photos.length === 0) return {};
      if (ratingData || photos.length >= 4) return { width: photos.length * 135 };
      return { width: (photos.length + 1) * 135 };
    });

    return (
      <div className="photos">
        {!ratingData && (
          <div className="photos-wrapper" style={photosWrapperStyles}>
            {photos.length < 4 && (
              <FileUploader
                className={cx('photo-upload', { full: photos.length === 0 })}
                onChange={this.handleFileUpload}
                accept="image/*"
                thumbSize={[120, 120]}
              >
                <i className="lz lz-camera-o" />{' '}
                {this.getString(photos.length > 0 ? 'add_photo' : 'add_photo_optional')}
              </FileUploader>
            )}
            {photos.map((photo, index) => (
              <div key={photo.id} className="photo-uploaded">
                <img src={photo.thumbnail} alt="Thumbnail" />
                <i className="btn-icon bg-dark circle lz lz-close" onClick={this.removeFileUploaded(index)} />
              </div>
            ))}
          </div>
        )}
        {!!ratingData && (
          <div className="photos-wrapper" style={photosWrapperStyles}>
            {ratingPhotos.map((photo) => (
              <div key={photo} className="photo-uploaded">
                <img src={photo} alt="Uploaded" />
              </div>
            ))}
          </div>
        )}
      </div>
    );
  };

  render() {
    const { isMobile } = this.context;
    const params = this.props.data.get('params');
    const { code, merchant, rating: ratingData, orderLines, serviceName } = params;

    const { rating, reviewDescription, photos, sending, error } = this.state;
    const wordCount = (reviewDescription && reviewDescription.trim().split(/[\s\n,]+/).length) || 0;
    const reviewLength = (reviewDescription && reviewDescription.length) || 0;

    const counter = (() => {
      if (rating === 'not_recommended') {
        return wordCount < BAD_RATING_LENGTH
          ? this.getString('0_words_more', 'rating', [BAD_RATING_LENGTH - wordCount])
          : this.getString('01_letters', 'rating', [reviewLength, 5000]);
      }

      if (reviewLength > 0) {
        return wordCount < GOOD_RATING_LENGTH
          ? this.getString('0_words_more', 'rating', [GOOD_RATING_LENGTH - wordCount])
          : this.getString('01_letters', 'rating', [reviewLength, 5000]);
      }

      return '';
    })();

    return (
      <Popup className="rating-popup" handleClose={this.close}>
        <div className="popup-header">
          <Button className={cx('btn-back left bg-transparent', { 'text-red': isMobile })} onClick={this.close}>
            <i className={cx('lz', { 'lz-light-arrow-left': isMobile, 'lz-close': !isMobile })} />
          </Button>
          {this.getString(ratingData ? 'view_rating_merchant' : 'rate_this_merchant', 'rating')}
          {(!!ratingData || this.state.ratingSuccess) && rating === 'recommended' && (
            <Button
              className="btn-back right bg-transparent pull-right"
              onClick={this.handleSharing({ merchant, orderLines, serviceName, ratingData })}
            >
              <i className="fas fa-share-alt" />
            </Button>
          )}
        </div>

        <div className="content">
          {this.renderMerchantInfo(merchant, code)}
          {this.renderRating(rating, !!ratingData)}

          <div className="review-description-wrapper">
            {!ratingData ? (
              <InputAutoGrow
                className="review-description input" maxRows={10}
                onChange={this.handleChangeDescription}
                placeholder={this.getString('click_here_to_write_your_comments')}
              />
            ): (
              <div className="review-description">{this.state.reviewDescription}</div>
            )}
            {!ratingData && <div className="review-count">{counter}</div>}
            {!ratingData && error && <div className="_error">{error}</div>}
          </div>

          {this.renderPhotos(photos, ratingData)}

          {!ratingData && (
            <div className="review-tip">
              {this.getString('merchant_rating_tip', '', [], true)}
            </div>
          )}
        </div>

        {!ratingData && (
          <div className="popup-footer">
            <Button type="primary" fw disabled={sending} onClick={this.handleSubmit}>
              {this.getString(sending ? 'sending' : 'send', 'rating')}
            </Button>
          </div>
        )}
      </Popup>
    );
  }

  handleSubmit = () => {
    if (this.state.sending) return;

    const params = this.props.data.get('params');
    const { code } = params;

    this.setState({ sending: true }, async () => {
      try {
        const { reviewDescription, rating, photos } = this.state;
        const wordCount = (reviewDescription && reviewDescription.trim().split(/[\s\n,]+/).length) || 0;
        const reviewLength = reviewDescription && reviewDescription.length;

        if (rating === 'not_recommended' && reviewDescription === '') throw new Error(this.getString('minimum_is_0_word_for_review', 'rating', [BAD_RATING_LENGTH]));
        if (reviewLength > 0) {
          if (rating === 'not_recommended' && wordCount < BAD_RATING_LENGTH) throw new Error(this.getString('minimum_is_0_word_for_review', 'rating', [BAD_RATING_LENGTH]));
          else if (rating !== 'not_recommended' && wordCount < GOOD_RATING_LENGTH) throw new Error(this.getString('minimum_is_0_word_for_review', 'rating', [GOOD_RATING_LENGTH]));
        }

        const photoUrls = await Promise.all(photos.map(async (photo) => photo.data.getURL()));
        await this.props.dispatch(
          ratingMerchant({
            code,
            rating,
            content: reviewDescription,
            photos: photoUrls,
            callback: () => {
              this.setState({ sending: false, ratingSuccess: true });
              if (rating !== 'recommended') return this.close();
              this.handleShareRating();
            },
          }),
        );
      } catch (e) {
        this.setState({ error: this.getString(e.message, 'rating'), sending: false });
      }
    });
  };

  handleChangeDescription = (value) => {
    this.setState({
      reviewDescription: value.slice(0, 5000),
      error: '',
    });
  };

  setRating = (rating) => {
    this.setState({ rating });
  };

  handleFileUpload = (data, thumbnail) => {
    const { photos } = this.state;
    this.setState({ photos: [{ id: genShortId(),data, thumbnail }, ...photos] });
  };

  removeFileUploaded = (index) => () => {
    const { photos } = this.state;
    photos.splice(index, 1);

    this.setState({ photos });
  };

  handleShareRating = () => {
    const isAlreadyDeniedShareRatingMerchant = cookie.get('isAlreadyDeniedShareRatingMerchant');
    if (isAlreadyDeniedShareRatingMerchant) return;

    const { isMobile } = this.context;
    const params = this.props.data.get('params');
    const { merchant, orderLines, serviceName } = params;

    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('thank_you_sharing'),
        content: this.getString('thank_you_sharing_description'),
        confirmBtn: { text: this.getString('share_now'), className: isMobile && 'btn-fw' },
        cancelBtn: { text: this.getString('ask_me_later'), className: 'bg-transparent text-gray text-normal' },
        actionClassName: isMobile && 'flex-column-reverse',
        onConfirm: this.handleSharing({ merchant, orderLines, serviceName }),
        onCancel: this.close,
        onDenied: () => {
          cookie.set('isAlreadyDeniedShareRatingMerchant', true);
        },
      }),
    );
  };

  handleSharing = ({ merchant, orderLines, serviceName, rating }) => () => {
    const merchantUrl = LINK_DOMAIN + (merchant.username ? `/${merchant.username}` : `/b/${merchant.slug}`) + '?source=rating_merchant';
    let content = rating ? rating.content : this.state.reviewDescription;
    if (!content) {
      content = this.getString(serviceName === 'lozat' ? 'sharing_content_lozat' : 'sharing_content', '', [
        orderLines
          .concat(orderLines, orderLines)
          .map((line, index) => (index < 2 ? line.name : '...'))
          .slice(0, 3)
          .join(', '),
        merchant.name,
      ]);
    }

    copyToClipboard(`${content}\n${merchantUrl}`);
    toast(this.getString('copied_review'));
    sendGAEvent('loship_action', 'ACTION_SHARE_RATINGMERCHANT');

    this.props.dispatch(
      open(SHARING_POPUP, {
        subject: this.getString('buy_on_loship', 'order'),
        body: content,
        url: merchantUrl,
      }),
    );
  };
}

export default initComponent(MerchantRatingPopup);
