import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { PHOTO_POPUP, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import { getImageUrl } from 'utils/image';

class PhotoPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: PHOTO_POPUP },
  };

  close = () => {
    this.props.dispatch(close(PHOTO_POPUP));
  };

  render() {
    const params = this.props.data.get('params') || {};
    const { photo = '', isBase64 } = params;

    return (
      <Popup className="popup-photo" handleClose={this.close} wrapperStyle={params.wrapperStyle}>
        <div className="img-wrapper">
          <i className="lz lz-close close-btn" onClick={this.close} />
          {isBase64 && <img src={photo} alt="Main base64" />}
          {!isBase64 && (
            <a href={photo.replace('resized', 'original')} target="_blank" rel="noopener">
              <img src={getImageUrl(photo, 1200, 'o')} alt="Main" />
            </a>
          )}
        </div>
      </Popup>
    );
  }
}

export default initComponent(PhotoPopup);
