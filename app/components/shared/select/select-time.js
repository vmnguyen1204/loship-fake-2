import React from 'react';
import { getString } from 'utils/i18n';
import timeList from 'utils/json/time24';

const SelectTime = ({ onChange, value, defaultAll = true, data, typeValue = 'string', ...props }) => (
  <select onChange={onChange} value={value || (defaultAll && 'all') || 'loship'} name={props.name}>
    {defaultAll && (
      <option key="df" value={'all'}>
        {getString('-- All --')}
      </option>
    )}
    {(data || timeList).map((service) => {
      return (
        <option key={service.value} value={typeValue !== 'string' ? service.id : service.value}>
          {getString(service.text, 'contract')}
        </option>
      );
    })}
  </select>
);

export default SelectTime;
