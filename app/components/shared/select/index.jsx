import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import cx from 'classnames';

if (process.env.BROWSER) {
  require('assets/styles/components/select.scss');
}

class Select extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data || [],
      values: this.props.value || [],
      comparedId: {},
      focusedValue: -1,
      isFocused: false,
      isOpen: false,
      typed: '',
    };
  }

  onFocus = () => {
    this.setState({ isFocused: true });
  };

  onBlur = () => {
    const { data, multiple } = this.props;

    this.setState((prevState) => {
      const { values } = prevState;
      if (multiple) {
        return {
          focusedValue: -1,
          isFocused: false,
          isOpen: false,
        };
      }

      const value = values[0];
      let focusedValue = -1;

      if (value) {
        focusedValue = data.findIndex((option) => option.value === value);
      }

      return {
        focusedValue,
        isFocused: false,
        isOpen: false,
      };
    });
  };

  onClick = () => {
    this.setState((state) => ({ isOpen: !state.isOpen }));
  };

  onDeleteOption = (e) => {
    const { value } = e.currentTarget.dataset;

    this.setState((prevState) => {
      const [...values] = prevState.values;
      const index = values.indexOf(value);

      values.splice(index, 1);

      return { values };
    });
  };

  // onHoverOption = e => {
  //   const { data } = this.props;

  //   const { value } = e.currentTarget.dataset;
  //   const index = data.findIndex(option => option.get('name') === value);

  //   this.setState({
  //     focusedValue: index,
  //   });
  // };

  onClickOption = (e, item) => {
    const { multiple } = this.props;
    const { value } = e.currentTarget.dataset;
    typeof this.props.handleData === 'function' && this.props.handleData(item);

    this.setState((prevState) => {
      if (!multiple) {
        return {
          values: [value],
          comparedId: { [item.get('id')]: value },
          isOpen: false,
        };
      }

      const [...values] = prevState.values;

      const index = values.indexOf(value);

      if (index === -1) {
        values.push(value);
      } else {
        values.splice(index, 1);
      }
      return { values };
    });
  };

  stopPropagation = (e) => {
    e.stopPropagation();
  };

  renderValues = () => {
    const { multiple } = this.props;
    const { values } = this.state;

    if (multiple) {
      return values.map((value) => {
        return (
          <span key={value} onClick={this.stopPropagation} className="multiple value">
            {value}
            <span data-value={value} onClick={this.onDeleteOption} className="delete">
              <i className="fas fa-times" />
            </span>
          </span>
        );
      });
    }

    return <div className="value">{values[0]}</div>;
  };

  renderOptions = () => {
    const { isOpen, data } = this.state;
    if (!isOpen) {
      return null;
    }

    return <div className="options">{data.map(this.renderOption)}</div>;
  };

  renderOption = (option, index) => {
    const { showStatus } = this.props;
    const { values, comparedId } = this.state;

    const { name, active } = option.toJS();
    const selected = values.includes(name) && comparedId[option.get('id')] === name;

    let className = 'option';
    if (selected) className += ' selected';
    // if (index === focusedValue) className += ' focused';

    return (
      <div
        key={index}
        data-value={name}
        className={className}
        // onMouseOver={this.onHoverOption}
        onClick={(e) => this.onClickOption(e, option)}
      >
        {showStatus && <span className={`status ${active && 'active'}`} />} {name}{' '}
        <i
          className={cx('fas', { 'fa-check': selected })}
        />
      </div>
    );
  };

  render() {
    const { isOpen } = this.state;
    return (
      <div className="select" onFocus={this.onFocus} onBlur={this.onBlur}>
        <div className="selection" onClick={this.onClick}>
          {this.renderValues()}
          <span className="arrow">
            {isOpen ? <i className="fas fa-caret-up" /> : <i className="fas fa-caret-down" />}
          </span>
        </div>
        {this.renderOptions()}
      </div>
    );
  }
}

export default initComponent(Select);
