import { RATING_POPUP, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import FeatureIntroducer from 'components/statics/feature-introducer';
import Hint from 'components/statics/hint';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { clearOrderNotifications, fetchOrderNotifications, getOrderNotifications } from 'reducers/notification';
import { iife } from 'utils/helper';
import Link from 'utils/shims/link';

class OrderNotifier extends BaseComponent {
  static defaultProps = {
    dataGetter: getOrderNotifications,
    customRender: true,
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  constructor(props, context) {
    super(props, context);
    this.state = { showContent: false };
  }

  componentDidMount() {
    this.props.dispatch(fetchOrderNotifications());
  }

  renderReviewForm(notification) {
    const currentUser = this.props.currentUser;
    if (currentUser.get('id') === notification.shipper.userId) {
      return (
        <div className="rating-shipper-wrapper">
          {this.getString('you_cant_rate_your_own_delivery_order')}&nbsp;
          <Hint text={this.getString('to_maintain_fairness_you_cant_rate_your_self_order_and_delivery_order')} />
        </div>
      );
    }
    return (
      <div className="rating-shipper-wrapper">
        <p>
          {this.getString(
            'we_would_love_to_hear_from_you_in_order_for_us_to_improve_our_service_quality_please_kindly_leave_a_feedback_about_your_experience',
          )}
        </p>
        <Button
          type="feature"
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            this.ratingShipper(notification);
            return false;
          }}
        >
          <i className="fa fa-star" />
          <span>{this.getString('rate_our_shipper')}</span>
        </Button>
      </div>
    );
  }

  render() {
    if (this.context.isMobile && this.context.partnerName === 'none') return null;

    const { showContent } = this.state;
    const { count, data } = this.props;

    const offset = typeof window !== 'undefined' ? (window.innerWidth - 1180) / 2 : 0;
    const contentStyles = { right: 100 + offset + 'px' };

    return (
      <div className="nav-item">
        {showContent && <div key="notifier-backdrop" className="notifier-backdrop" onMouseDown={this.toggleContent} />}
        <div className="btn-nav-link notifier" onClick={this.toggleContent}>
          <i className="lz lz-history" />
          {count > 0 && <FeatureIntroducer />}
        </div>
        {showContent && (
          <div className="notifier-content" style={contentStyles}>
            <h3>
              {this.getString('order_history')}
              {this.context.isMobile && (
                <a className="btn-close-popup" onClick={this.toggleContent}>
                  <i className="fa fa-times" />
                </a>
              )}
            </h3>
            <div ref="content">
              <ul>
                {data.map((notification) => {
                  const avatar = (() => {
                    if (notification.serviceName === 'losend') return '/dist/images/losend-notification-item.png';
                    if (notification.serviceName === 'loxe') return '/dist/images/loxe-notification-item.png';
                    return notification.eatery.avatar;
                  })();

                  return (
                    <li key={notification.code} onClick={() => this.viewDetail(notification)}>
                      <div className={notification.isNew ? 'nt-order-item new' : 'nt-order-item'}>
                        <div className="noti-avatar">
                          <img src={avatar} alt="avatar" />
                        </div>
                        <div className="order-detail">
                          <div className="od-row">
                            <div className="od-col left">
                              <div>
                                <span className="label">{this.getString('order_code')}</span>
                                <b>#{notification.code}</b>
                              </div>
                              <div>
                                <span className="label">{this.getString('location')}</span>
                                {notification.eatery.name}
                              </div>
                            </div>
                            <div className="od-col right">
                              <div>{notification.time}</div>
                              <div>
                                <div className={cx('order-status', notification.status)}>
                                  {this.getString(notification.statusText, 'notifier')}
                                </div>
                              </div>
                            </div>
                          </div>
                          {!!notification.canReview && this.renderReviewForm(notification)}
                        </div>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </div>

            <Link to="/tai-khoan/don-hang" onClick={this.toggleContent}>{this.getString('see_all_orders')}</Link>
          </div>
        )}
      </div>
    );
  }

  toggleContent = () => {
    this.setState((state) => ({ showContent: !state.showContent }), () => {
      if (this.state.showContent) {
        this.props.dispatch(fetchOrderNotifications());
        this.addBodyClassName();
      } else {
        this.props.dispatch(clearOrderNotifications());
        this.removeBodyClassName();
      }
    });
  };

  getPrivileges = (order) => {
    const privileges = order.privileges || [];

    if (privileges.some((privilege) => privilege.includes('edit_all'))) return 'EDIT';
    return 'VIEW';
  };

  viewDetail = (order) => {
    this.toggleContent();

    const orderUrl = iife(() => {
      const shareId = order.sharing?.shareId;
      if (shareId && this.getPrivileges(order) !== 'EDIT') {
        return `/tai-khoan/don-hang/${shareId}?usp=sharing`;
      }
      return `/tai-khoan/don-hang/${order.code}`;
    });
    this.props.history.push(orderUrl);
  };

  ratingShipper = (notification) => {
    return this.props.dispatch(
      open(RATING_POPUP, {
        order: notification.id,
        code: notification.code,
        shipper: notification.shipper,
      }),
    );
  };

  addBodyClassName = () => {
    const { body } = document;
    const bodyClass = body.className.split(' ').filter((c) => c !== 'has-popup');
    bodyClass.push('has-popup');
    body.className = bodyClass.join(' ');
    const popupCount = parseInt(body.getAttribute('popup') || 0);
    body.setAttribute('popup', popupCount + 1);
  }

  removeBodyClassName = () => {
    const { body } = document;
    let popupCount = parseInt(body.getAttribute('popup') || 0);
    popupCount--;
    if (popupCount >= 0) body.setAttribute('popup', popupCount);

    if (popupCount <= 0) {
      body.className = body.className
        .split(' ')
        .filter((c) => c !== 'has-popup')
        .join(' ');
    }
  }
}

export default initComponent(OrderNotifier, withRouter);
