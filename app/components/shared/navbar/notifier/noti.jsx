import { fetchNotifications, getNotifications } from 'reducers/notification';

import BaseComponent from 'components/base';
import PropTypes from 'prop-types';
import React from 'react';
import Spinner from 'components/statics/spinner';
import { Waypoint } from 'react-waypoint';
import cx from 'classnames';
import initComponent from 'lib/initComponent';
import loziServices from 'utils/json/loziServices';
import moment from 'utils/shims/moment';
import qs from 'qs';
import { withRouter } from 'react-router-dom';

const UNSUPPORTED_ACTION_ON_WEB = ['new_lopoint_play_turn', 'send_viral_lopoint'];
const UNSUPPORTED_MESSAGE = 'you_receive_a_notification_which_is_not_supported_on_loship_web_use_loship_app_on_your_phone_instead';
const defaultImageUrl = '/dist/images/placeholder.png';

function getNotificationAvatar(notification) {
  const { action, models = {} } = notification;
  const { eatery, service: s } = models;

  if (eatery && eatery.avatar) return eatery.avatar;

  if (action === 'send_viral_ship_service') {
    if (!s) return defaultImageUrl;
    const service = loziServices.find((cService) => cService.title === cService.value) ||
    loziServices.find((cService) => cService.title === 'Loship');
    return `/dist/images/service/${service.name}.png`;
  }

  if (action === 'send_viral_lopoint') {
    return '/dist/images/service/lopoint.png';
  }

  return defaultImageUrl;
}

function getNotificationMessage(notification, getString) {
  const { eatery = {}, order = {}, message } = notification.models || {};

  if (notification.action === 'order_cancel') {
    return getString('The order #{0} at {1} has been cancelled.\nReason: {2}', '', [
      order.code,
      eatery.name,
      message && message.message,
    ]);
  }

  return message && message.message;
}
getNotificationMessage.PropTypes = {
  notification: PropTypes.object.isRequired,
  getString: PropTypes.func.isRequired,
};

function getNotificationUrl(notification) {
  if (UNSUPPORTED_ACTION_ON_WEB.some((action) => action === notification.action)) return '';

  if (notification.action === 'order_cancel') {
    const { order = {} } = notification.models;
    return `/tai-khoan/don-hang/${order.code}`;
  }

  const deeplinkUrl = notification.models.message && notification.models.message.url;
  const query = qs.parse(deeplinkUrl.split('?')[1]);

  // redirect to merchant page
  if (query.eateryId && notification.models.eatery) {
    return notification.models.eatery.username
      ? `/${notification.models.eatery.username}`
      : `/b/${notification.models.eatery.slug}`;
  }
  if (query.shipServiceId && notification.models.service) {
    const { service: s } = notification.models;
    if (s.value !== 'Loship' && s.value !== 'Lomart') return '';

    const service = s.value === 'Lomart' ? loziServices.find((cService) => cService.title === s.value) : loziServices.find((cService) => cService.title === 'Loship');
    return service.domain;
  }

  return '';
}

class Notifier extends BaseComponent {
  static defaultProps = {
    dataGetter: getNotifications,
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props, context) {
    super(props, context);
    this.state = { showContent: false };
  }

  componentDidMount() {
    this.props.dispatch(fetchNotifications());
  }

  render() {
    const { showContent } = this.state;
    const { data } = this.props;

    const offset = typeof window !== 'undefined' ? (window.innerWidth - 1180) / 2 : 0;
    const contentStyles = { right: 50 + offset + 'px' };

    return (
      <div className="nav-item">
        {showContent && <div key="notifier-backdrop" className="notifier-backdrop" onMouseDown={this.toggleContent} />}
        <div className="btn-nav-link notifier" onClick={this.toggleContent}>
          <i className="lz lz-notification" />
        </div>
        {showContent && (
          <div className="notifier-content" style={contentStyles}>
            <h3>
              {this.getString('notifications')}
              {this.context.isMobile && (
                <a className="btn-close-popup" onClick={this.toggleContent}>
                  <i className="fa fa-times" />
                </a>
              )}
            </h3>
            <div ref="content">
              <ul>
                {data.map((notification) => {
                  if (!getNotificationUrl(notification)) return null;
                  return (
                    <li key={`noti-${notification.id}`} onClick={() => this.viewDetail(notification.id)}>
                      <div className={cx('nt-notification-item')}>
                        <div className="noti-avatar">
                          <img src={getNotificationAvatar(notification)} alt="avatar" />
                        </div>
                        <div className="notification-detail">
                          <div className="od-row">
                            <div className="od-col full">
                              {UNSUPPORTED_ACTION_ON_WEB.some((action) => action === notification.action)
                                ? this.getString(UNSUPPORTED_MESSAGE)
                                : getNotificationMessage(notification, this.getString)}
                              <br />
                              <span className="time">{moment(notification.createdAt).fromNowOrNow()}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  );
                })}
                {this.props.nextUrl && (
                  <Waypoint onEnter={this.onViewPortEnter} onLeave={this.onViewPortLeave} topOffset="-40px">
                    <Spinner type="flow" />
                  </Waypoint>
                )}
              </ul>
            </div>
          </div>
        )}
      </div>
    );
  }

  onViewPortEnter = () => {
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({ enteredViewPort: true }, () => {
        this.props.dispatch(fetchNotifications(this.props.nextUrl));
      });
    }, 500);
  };

  onViewPortLeave = () => {
    if (this.timeout) clearTimeout(this.timeout);
    this.setState({ enteredViewPort: false });
  };

  viewDetail = (notificationId) => {
    const notification = this.props.data.find((n) => n.id === notificationId);
    this.toggleContent();

    const url = getNotificationUrl(notification);
    if (url.includes('http')) return window.location.replace(url);
    if (url) return this.props.history.push(url);
  };

  toggleContent = () => {
    this.setState((state) => ({ showContent: !state.showContent }), () => {
      if (this.state.showContent) {
        this.props.dispatch(fetchNotifications());
        this.addBodyClassName();
      } else {
        this.removeBodyClassName();
      }
    });
  };

  addBodyClassName = () => {
    const { body } = document;
    const bodyClass = body.className.split(' ').filter((c) => c !== 'has-popup');
    bodyClass.push('has-popup');
    body.className = bodyClass.join(' ');
    const popupCount = parseInt(body.getAttribute('popup') || 0);
    body.setAttribute('popup', popupCount + 1);
  }

  removeBodyClassName = () => {
    const { body } = document;
    let popupCount = parseInt(body.getAttribute('popup') || 0);
    popupCount--;
    if (popupCount >= 0) body.setAttribute('popup', popupCount);

    if (popupCount <= 0) {
      body.className = body.className
        .split(' ')
        .filter((c) => c !== 'has-popup')
        .join(' ');
    }
  }
}

export default initComponent(Notifier, withRouter);
