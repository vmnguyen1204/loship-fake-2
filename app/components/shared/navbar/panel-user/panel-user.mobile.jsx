import { logout } from 'actions/access-control';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import AuthButton from 'components/shared/button/auth';
import ImageLazy from 'components/statics/img-lazy';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getCurrentUser } from 'reducers/user';
import Link from 'utils/shims/link';
import OrderNotifier from '../notifier';

class UserPanel extends BaseComponent {
  static defaultProps = {
    customRender: true,
    dataGetter: (state) => {
      const currentUser = getCurrentUser(state);

      if (!currentUser || !currentUser.get('data')) return null;

      return currentUser;
    },
  };

  state = { showUserPanel: false };

  logout = () => {
    this.props.dispatch(logout());
  };

  onClick = (e) => {
    if (this.state.showUserPanel && e.target !== this.refs.userMenu) {
      e.preventDefault();
      this.setState({ showUserPanel: false });
      return false;
    }
  };

  componentDidMount() {
    window.addEventListener('click', this.onClick);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.onClick);
  }

  render() {
    const { data, partnerName } = this.props;
    if (!data) return this.renderLoginForm();
    return (
      <div className="user-nav-menu">
        <OrderNotifier currentUser={data} />
        {/* <Notifier currentUser={data} /> */}

        <div className="nav-item">
          <Button
            type="link"
            className="text-black btn-menu"
            onClick={(e) => {
              this.setState((state) => ({ showUserPanel: !state.showUserPanel }));
              e.stopPropagation();
            }}
          >
            <i className="fa fa-bars" />
          </Button>

          {this.state.showUserPanel && (
            <div className="drop-down-menu" ref="userMenu">
              <ul>
                <li>
                  <div className="user-card">
                    <Link to="/tai-khoan">
                      <ImageLazy src={data.get('avatar')} size={32} />
                      <div className="name">
                        {data.get('fullname')}
                        <div className="username">@{data.get('username')}</div>
                      </div>
                    </Link>
                  </div>
                </li>
                <li>
                  <Link to="/tai-khoan/don-hang">
                    {this.getString('orders_history', 'navbar')}
                  </Link>
                </li>
                <li>
                  <a href="https://doitacloship.lozi.vn/" target="_blank" rel="noopener" onClick={(e) => e.stopPropagation()}>
                    {this.getString('Hợp tác quán ăn')}
                  </a>
                </li>
                <li>
                  <a href="http://chienbinhloship.com/" target="_blank" rel="noopener" onClick={(e) => e.stopPropagation()}>
                    {this.getString('Hợp tác shipper')}
                  </a>
                </li>
                {partnerName === 'none' && (
                  <li>
                    <a onClick={this.logout}>{this.getString('logout')}</a>
                  </li>
                )}
              </ul>
            </div>
          )}
        </div>
      </div>
    );
  }

  renderLoginForm() {
    return (
      <div className="user-nav-menu right">
        <div className="nav-item user-panel">
          <div className="user-panel">
            <AuthButton className="btn-login">{this.getString('login')}</AuthButton>
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(UserPanel, withRouter);
