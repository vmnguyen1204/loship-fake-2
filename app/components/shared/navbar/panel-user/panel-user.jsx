import { logout } from 'actions/access-control';
import BaseComponent from 'components/base';
import AuthButton from 'components/shared/button/auth';
import ImageLazy from 'components/statics/img-lazy';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getCurrentUser } from 'reducers/user';
import Link from 'utils/shims/link';
import OrderNotifier from '../notifier';

class UserPanel extends BaseComponent {
  static defaultProps = {
    customRender: true,
    dataGetter: (state) => {
      const currentUser = getCurrentUser(state);
      if (!currentUser || !currentUser.get('data')) return null;

      return currentUser;
    },
  };

  logout = () => {
    this.props.dispatch(logout());
  };

  render() {
    const { data } = this.props;
    if (!data) return this.renderLoginForm();
    return (
      <div className="user-nav-menu">
        <OrderNotifier currentUser={data} />
        {/* <Notifier currentUser={data} /> */}

        <div className="nav-item">
          <Link
            to="/tai-khoan"
            onClick={(e) => {
              this.setState((state) => ({ showUserPanel: !state.showUserPanel }));
              e.stopPropagation();
            }}
          >
            <ImageLazy src={data.get('avatar')} size={32} />
          </Link>

          <div className="drop-down-menu">
            <ul>
              <li>
                <div className="user-card">
                  <Link to="/tai-khoan">
                    <div className="name">
                      {data.get('fullname')}
                      <div className="username">@{data.get('username')}</div>
                    </div>
                  </Link>
                </div>
              </li>
              <li>
                <Link to="/tai-khoan/don-hang">
                  {this.getString('orders_history', 'navbar')}
                </Link>
              </li>
              <li>
                <a onClick={this.logout}>{this.getString('logout')}</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }

  renderLoginForm() {
    return (
      <div className="user-nav-menu right">
        <div className="nav-item user-panel">
          <div className="user-panel">
            <AuthButton className="btn-login">{this.getString('login')}</AuthButton>
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(UserPanel, withRouter);
