import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Desktop from './panel-user';
import Mobile from './panel-user.mobile';

export default class UserPanel extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}
