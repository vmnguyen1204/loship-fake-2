import { fetchNewsFeed } from 'actions/newsfeed';
import BaseComponent from 'components/base';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import React from 'react';
import { getCurrentCity, getGlobalAddress } from 'reducers/new/metadata';
import { getNewsFeed } from 'reducers/newsfeed';
import { getCurrentUser } from 'reducers/user';
import { APP_INFO } from 'settings/variables';
import { defaultShippingAdministration } from 'utils/json/supportedCities';
import { getRoute } from 'utils/routing';
import Link from 'utils/shims/link';

const WHITELIST_HIDE_NAVBAR = {
  desktop: [
    '/payment',
    '/g/',
    '/m/buy',
    '/m/tai-khoan/don-hang',
    '/d/buy',
    '/d/tai-khoan/don-hang',
  ],
  mobile: [
    '/tai-khoan/don-hang/:order',
    '/tai-khoan/cap-nhat',
    '/tai-khoan/quan-ly-the',
    '/tai-khoan/cua-hang-yeu-thich',
    '/c/:username', '/cid/:id',
    '/cong-dong-loship',
    '/payment',
    '/g/',
    '/m/buy',
    '/m/tai-khoan/don-hang',
    '/d/buy',
    '/d/tai-khoan/don-hang',
    '/b/:merchant/quan-ly',
    '/b/:merchant/menu',
    '/b/:merchant/cap-nhat-menu',
    '/b/:merchant/:groupTopic?',
    '/:merchantUsername/menu',
    '/:merchantUsername/cap-nhat-menu',
    '/:merchantUsername/:groupTopic?',
  ],
};

export const isWhitelistHideNavbar = (isMobile) => {
  const { match } = (getRoute(null, globalThis.location?.pathname || globalThis.originalUrl)) || {};
  if (!match) return false;

  return WHITELIST_HIDE_NAVBAR[isMobile ? 'mobile' : 'desktop'].some((path) => {
    return match.path.includes(path);
  });
};

export default class NavbarBase extends BaseComponent {
  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      const currentCity = getCurrentCity(state) || defaultShippingAdministration;
      if (currentCity) data = data.set('currentCity', currentCity);

      const newsfeedGroupPromotion =
        getNewsFeed(state, { groupApi: true, force: true, globalAddress }) ||
        getNewsFeed(state, { groupApi: true, force: true });
      if (newsfeedGroupPromotion) data = data.set('newsfeedGroupPromotion', newsfeedGroupPromotion.get('extras'));

      return data;
    },
    customRender: true,
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  componentDidMount() {
    this.props.dispatch(
      fetchNewsFeed({ query: { groupApi: true, force: true, globalAddress: this.props.globalAddress } }),
    );
  }

  renderLogoText = () => {
    const logo = (() => {
      return '/dist/images/logo-footer.png';
    })();

    return (
      <Link
        className="logo"
        to="/"
        style={{ backgroundImage: `url(${logo})` }}
      />
    );
  };

  renderLogo = () => {
    const logo = (() => {
      if (APP_INFO.serviceName === 'lomart') return '/dist/images/logo-lomart.png';
      return '/dist/images/logo.png';
    })();

    return (
      <Link
        className="logo"
        to="/"
        style={{ backgroundImage: `url(${logo})` }}
      />
    );
  };
}
