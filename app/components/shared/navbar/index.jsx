import Navbar from 'components/shared/navbar/navbar';
import NavbarLomart from 'components/shared/navbar/navbar.lomart';
import React from 'react';
import { APP_INFO } from 'settings/variables';

if (process.env.BROWSER) {
  require('assets/styles/components/navbar.scss');
}

export default function() {
  if (APP_INFO.serviceName === 'lomart') return <NavbarLomart />;
  return <Navbar />;
}
