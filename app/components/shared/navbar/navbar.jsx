import { sendGAEvent } from 'actions/factory';
import UserPanel from 'components/shared/navbar/panel-user';
import Topbar from 'components/shared/topbar';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { iife } from 'utils/helper';
import { mainCitiesSlug } from 'utils/json/supportedCities';
import Link from 'utils/shims/link';
import NavbarBase, { isWhitelistHideNavbar } from './navbar-base';
import OrderNotifier from './notifier';

if (process.env.BROWSER) {
  require('assets/styles/components/navbar.scss');
}

class Navbar extends NavbarBase {
  renderPartnerNavbar = () => {
    const { pathname } = this.props.location;
    const shouldHideBackArrow = ['viettelpay'].includes(this.context.partnerName);

    return (
      <div className="navbar navbar-app">
        <div className="container">
          {(pathname === '/' || shouldHideBackArrow) ? null : (
            <a className="back-arrow" onClick={this.goBack}>
              <i className="lz lz-light-arrow-left" />
            </a>
          )}
          {this.renderLogoText()}
          {this.props.newsfeedGroupPromotion && this.props.newsfeedGroupPromotion.get('total') > 0 && (
            <Link
              className="nav-icon"
              to="danh-sach-dia-diem-an-voi-nhom"
              onClick={() => {
                sendGAEvent('internal_navigation', 'Action_Tap_Category', 'EatWithGroup');
              }}
            >
              <i className="lz lz-discount-group" />
            </Link>
          )}

          <div className="user-nav-menu">
            <OrderNotifier currentUser={this.props.currentUser} />
          </div>
        </div>
      </div>
    );
  };

  renderNavLinks = () => {
    const { currentCity } = this.props;
    const shouldShowVideoTab = iife(() => {
      if (!currentCity) return false;
      if (currentCity.type === 'city') return mainCitiesSlug.includes(currentCity.city.slug);
      return mainCitiesSlug.includes(currentCity.district.slug) || mainCitiesSlug.includes(currentCity.district.city.slug);
    });

    return [
      this.props.newsfeedGroupPromotion && this.props.newsfeedGroupPromotion.get('total') > 0 && (
        <Link
          key="an-voi-nhom"
          className="btn-nav-link"
          to={'/danh-sach-dia-diem-an-voi-nhom'}
          onClick={() => {
            sendGAEvent('internal_navigation', 'Action_Tap_Category', 'EatWithGroup');
          }}
        >
          {this.getString('group_order')}
        </Link>
      ),
      <Link
        key="an-sang"
        className="btn-nav-link"
        to={'/danh-sach-dia-diem-phuc-vu-an-sang-giao-tan-noi'}
        onClick={() => {
          sendGAEvent('internal_navigation', 'Action_Tap_Category', 'Breakfast');
        }}
      >
        {this.getString('breakfast')}
      </Link>,
      <Link
        key="an-trua"
        className="btn-nav-link"
        to={'/danh-sach-dia-diem-phuc-vu-an-trua-giao-tan-noi'}
        onClick={() => {
          sendGAEvent('internal_navigation', 'Action_Tap_Category', 'Lunch');
        }}
      >
        {this.getString('lunches')}
      </Link>,
      <Link
        key="an-toi"
        className="btn-nav-link"
        to={'/danh-sach-dia-diem-phuc-vu-an-toi-giao-tan-noi'}
        onClick={() => {
          sendGAEvent('internal_navigation', 'Action_Tap_Category', 'Dinner');
        }}
      >
        {this.getString('midnight')}
      </Link>,
      <Link
        key="an-vat"
        className="btn-nav-link"
        to={'/danh-sach-dia-diem-an-vat-giao-tan-noi'}
        onClick={() => {
          sendGAEvent('internal_navigation', 'Action_Tap_Category', 'Snack');
        }}
      >
        {this.getString('snacks')}
      </Link>,
      shouldShowVideoTab && (
        <Link
          key="video"
          className="btn-nav-link"
          to="/videos"
          onClick={() => {
            sendGAEvent('internal_navigation', 'newsfeed_video', 'navbar');
          }}
        >
          {this.getString('Video')}
        </Link>
      ),
      <Link
        key="danh-muc"
        className="btn-nav-link"
        to={'/danh-muc'}
        onClick={() => {
          sendGAEvent('internal_navigation', 'newsfeed_category', 'navbar');
        }}
      >
        {this.getString('other_categories')}
      </Link>,
    ];
  };

  render() {
    const { isMobile, partnerName } = this.context;

    if (isWhitelistHideNavbar(isMobile)) return null;
    if (partnerName !== 'none') return this.renderPartnerNavbar();
    return (
      <div className="navbar">
        <Topbar />
        <div className="container">
          {this.renderLogo()}
          {!isMobile && (
            <div className="user-nav-menu">
              {this.renderNavLinks()}
            </div>
          )}

          <div className="blank" />
          {isMobile && this.props.newsfeedGroupPromotion && this.props.newsfeedGroupPromotion.get('total') > 0 && (
            <Link className="nav-icon" to="/danh-sach-dia-diem-an-voi-nhom">
              <i className="lz lz-discount-group" />
            </Link>
          )}
          <UserPanel isMobile={isMobile} partnerName={partnerName} />
        </div>
      </div>
    );
  }

  goBack = () => {
    return this.props.history.replace('/');
  };
}

export default initComponent(Navbar, withRouter);
