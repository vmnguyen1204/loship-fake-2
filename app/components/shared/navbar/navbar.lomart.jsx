import { sendGAEvent } from 'actions/factory';
import UserPanel from 'components/shared/navbar/panel-user';
import Topbar from 'components/shared/topbar';
import initComponent from 'lib/initComponent';
import React from 'react';
import Link from 'utils/shims/link';
import NavbarBase, { isWhitelistHideNavbar } from './navbar-base';

if (process.env.BROWSER) {
  require('assets/styles/components/navbar.scss');
}

class Navbar extends NavbarBase {
  renderNavLinks = () => {
    return [
      this.props.newsfeedGroupPromotion && this.props.newsfeedGroupPromotion.get('total') > 0 && (
        <Link
          key="an-voi-nhom"
          className="btn-nav-link"
          to={'/danh-sach-cua-hang-mua-voi-nhom'}
          onClick={() => {
            sendGAEvent('internal_navigation', 'Action_Tap_Category', 'EatWithGroup');
          }}
        >
          {this.getString('group_order')}
        </Link>
      ),
      <Link
        key="sieu-thi"
        className="btn-nav-link"
        to={'/danh-sach-cua-hang-sieu-thi-cua-hang,cua-hang-bach-hoa-giao-tan-noi'}
      >
        {this.getString('groceries')}
      </Link>,
      <Link
        key="cua-hang-tien-loi"
        className="btn-nav-link"
        to={'/danh-sach-cua-hang-cua-hang-tien-loi-giao-tan-noi'}
      >
        {this.getString('convenience_stores')}
      </Link>,
      <Link
        key="rau-cu"
        className="btn-nav-link"
        to={'/danh-sach-cua-hang-trai-cay-giao-tan-noi'}
      >
        {this.getString('fruit')}
      </Link>,
      <Link key="me-va-be" className="btn-nav-link" to={'/danh-sach-cua-hang-me-va-be-giao-tan-noi'}>
        {this.getString('mom_baby_and_kids')}
      </Link>,
      <Link
        key="do-dung-gia-dinh"
        className="btn-nav-link"
        to={'/danh-sach-cua-hang-cua-hang-do-dung-gia-dinh-giao-tan-noi'}
      >
        {this.getString('households')}
      </Link>,
      <Link
        key="danh-muc"
        className="btn-nav-link"
        to={'/danh-muc'}
        onClick={() => {
          sendGAEvent('internal_navigation', 'newsfeed_category', 'navbar');
        }}
      >
        {this.getString('other_categories')}
      </Link>,
    ];
  };

  render() {
    const { isMobile, partnerName } = this.context;

    if (isWhitelistHideNavbar(isMobile)) return null;
    return (
      <div className="navbar">
        <Topbar />
        <div className="container">
          {this.renderLogo()}
          {!isMobile && (
            <div className="user-nav-menu">
              {this.renderNavLinks()}
            </div>
          )}

          <div className="blank" />
          {isMobile && this.props.newsfeedGroupPromotion && this.props.newsfeedGroupPromotion.get('total') > 0 && (
            <Link className="nav-icon" to="/danh-sach-cua-hang-mua-voi-nhom">
              <i className="lz lz-discount-group" />
            </Link>
          )}
          <UserPanel isMobile={isMobile} partnerName={partnerName} />
        </div>
      </div>
    );
  }
}

export default initComponent(Navbar);
