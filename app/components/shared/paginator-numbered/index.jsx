import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { Link, withRouter } from 'react-router-dom';
import cx from 'classnames';

if (process.env.BROWSER) {
  require('assets/styles/components/paginator.scss');
}
class NumberedPaginator extends BaseComponent {
  static defaultProps = {
    perPage: 20,
    customRender: true,
    localQuery: null,
    onPageChange: null,
  };

  getPageCount = () => {
    const total = this.props.extras && this.props.extras.get('total');
    if (!total) return 0;
    return Math.ceil(total / this.props.perPage);
  };

  render() {
    const { extras, getUnit, location, localQuery, className, currentPage: srcPage, hidePageCount, hideTotal } = this.props;

    if (!extras) return false;

    const query = !localQuery ? location.query : localQuery;
    const { total } = extras.toObject();
    const pageCount = this.getPageCount();
    const currentPage = srcPage || (query.page ? parseInt(query.page) : 1);
    return (
      <div className={`paginator ${className ? className : ''}`}>
        {!hideTotal && typeof getUnit === 'function' && (
          <div className="total">
            {this.getString('Total:', 'paginator')}&nbsp;
            {getUnit(total)}
          </div>
        )}
        {currentPage > 1 &&
          this.renderPage(currentPage - 1, currentPage, location, <i className="fa fa-arrow-left" />)}
        {!hidePageCount && pageCount > 1 && this.renderPages(pageCount)}
        {currentPage < pageCount &&
          this.renderPage(currentPage + 1, currentPage, location, <i className="fa fa-arrow-right" />)}
      </div>
    );
  }

  renderPages(pageCount) {
    const { location, localQuery, currentPage: srcPage } = this.props;
    const query = !localQuery && location.query || localQuery || {};
    const currentPage = srcPage || (query.page ? parseInt(query.page) : 1);
    const pages = [];
    const offsetPage = Math.max(currentPage - 2, 1);
    const maxVisiblePage = Math.min(currentPage + 2, pageCount);

    if (offsetPage > 1) {
      pages.push(this.renderPage(1, currentPage, location));
      if (offsetPage >2){
        pages.push(<span key="pre-dot">...</span>);
      }
    }

    for (let i = offsetPage; i <= maxVisiblePage; i++) {
      pages.push(this.renderPage(i, currentPage, location));
    }

    if (maxVisiblePage < pageCount) {
      if (maxVisiblePage < pageCount - 1) {
        pages.push(<span key="post-dot">...</span>);
      }
      pages.push(this.renderPage(pageCount, currentPage, location));
    }

    return pages;
  }

  renderPage(page, currentPage, location, text = '') {
    const { localQuery, onPageChange } = this.props;

    if (typeof onPageChange === 'function') {
      return (
        <a
          className={cx('btn', {
            active: currentPage === page,
          })}
          key={page}
          onClick={() => onPageChange(page)}
        >
          {text || page}
        </a>
      );
    }

    const query = !localQuery ? location.query : localQuery;
    const url = {
      pathname: location.pathname,
      query: { ...query, page },
    };
    return (
      <Link
        to={url}
        className={cx('btn', {
          active: currentPage === page,
        })}
        key={page}
      >
        {text || page}
      </Link>
    );
  }
}
export default initComponent(NumberedPaginator, withRouter);

