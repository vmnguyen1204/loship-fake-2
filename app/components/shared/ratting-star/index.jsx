import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { compose, onlyUpdateForPropTypes } from 'recompose';
import { createSelector } from 'reselect';

if (process.env.BROWSER) {
  require('assets/styles/components/ratting-star.scss');
}

const getFixedRating = (rating) => Math.min(rating, 5).toFixed(1);
const getCeilRating = (rating) => Math.ceil(rating);

const formatedRating = createSelector(
  [getFixedRating, getCeilRating],
  (fixed, ceiled) => {
    return { fixed, ceiled };
  },
);

const RattingStar = ({ rating, nonBuyable, getString }) => {
  const { fixed, ceiled } = formatedRating(rating);
  return (
    <div className="ratting-star">
      <div className="star">
        <div className="gray">
          <i className="lz lz-star" />
          <i className="lz lz-star" />
          <i className="lz lz-star" />
          <i className="lz lz-star" />
          <i className="lz lz-star" />
        </div>
        <div style={{ width: `${(fixed / 5) * 100}%` }} className={cx('rating', `rating${ceiled}`)}>
          <i className="lz lz-star" />
          <i className="lz lz-star" />
          <i className="lz lz-star" />
          <i className="lz lz-star" />
          <i className="lz lz-star" />
        </div>
      </div>
      {nonBuyable && (
        <span>
          ({nonBuyable} {getString('reviews')})
        </span>
      )}
    </div>
  );
};

RattingStar.propTypes = {
  rating: PropTypes.number.isRequired,
  nonBuyable: PropTypes.number,
};

export default compose(onlyUpdateForPropTypes)(RattingStar);
