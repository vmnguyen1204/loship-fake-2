import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { MENU_UPDATE_DISH, close, open } from 'actions/popup';
import Link from 'utils/shims/link';

if (process.env.BROWSER) {
  require('assets/styles/components/mobile-nav.scss');
}

class MobileNavMenuTool extends BaseComponent {
  componentDidUpdate = () => {
    const query = this.getQuery();
    if (!query.showpopup) {
      this.props.dispatch(close(MENU_UPDATE_DISH));
    }
  };

  openCreateDishPopup = () => {
    this.handleShowPopup();
    this.props.dispatch(
      open(MENU_UPDATE_DISH, {
        merchant: this.props.merchant,
        isCreate: true,
      }),
    );
  };

  handleShowPopup = () => {
    this.props.history.push(this.props.location.pathname + '?showpopup=true');
  };

  render() {
    const { merchant, originDomain } = this.props;
    const merchantUrl = `${originDomain}/b/${merchant}`;
    return (
      <div className="mobile-nav-wrapper red">
        <div className="mobile-navbar">
          <Link className="back-arrow" to={merchantUrl}>
            <i className="lz lz-arrow-head-left" />
          </Link>
          {this.getString('Quản lý menu')}
          {this.props.checkEditMenu && (
            <a onClick={this.openCreateDishPopup} className="nav-action">
              {this.getString('Thêm món')}
            </a>
          )}
        </div>
      </div>
    );
  }
}

export default initComponent(MobileNavMenuTool);
