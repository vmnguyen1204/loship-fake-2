import BasePage from 'components/pages/base';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItemLanding } from 'components/shared/newsfeed/merchant-item';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import Link from 'utils/shims/link';

if (process.env.BROWSER) {
  require('assets/styles/pages/not-found.scss');
  require('assets/styles/pages/not-found-mobile.scss');
}

class Page4xx5xx extends BasePage {
  static contextTypes = {
    isMobile: PropTypes.bool,
  }

  renderContent = () => {
    return (
      <div className="page-not-found">
        <div className="section-not-found">
          <img className="not-found-img" src="/dist/images/not-found-404.png" alt="not-found" />
          <h2>{this.getString('something_went_wrong')}</h2>
          <div className="content">
            <p className="group-1">{this.getString('sorry_loship_has_error')}</p>
            <ul className="group-2">
              <li>{this.getString('checking_the_exact_url')}</li>
              <li>{this.getString('choose_another_shop_there_are_more_than_30000_shops_await')}</li>
            </ul>
            <p className="group-1">
              <Link to="/">
                {this.getString('go_to_loshipvn_home_page')}
                <i className="lz lz-arrow-head-right" />
              </Link>
            </p>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="home-page">
        <div className="container">
          {this.renderContent()}
          <SectionNewsfeed
            params={{
              newestApi: true,
              page: 1,
              limit: 12,
              globalAddress: this.props.globalAddress,
            }}
            title={this.getString('recently_joined_merchants')}
            action={(
              <a
                key="action" href="https://doitacloship.lozi.vn/"
                className="action"
                target="_blank" rel="noopener" >
                {this.getString('cooperate_with_loship')}
              </a>
            )}
            submit={this.submit}
            mode={this.context.isMobile ? 'preload|see_all|horizontal' : 'preload|see_all'}
            MerchantItem={EateryItemLanding}
          />
          <div className="clearfix" />
        </div>
      </div>
    );
  }

  submit = () => {
    const { currentCity } = this.props;
    if (!currentCity) return;

    this.props.history.push('/danh-sach-dia-diem-moi-tham-gia-loship-giao-tan-noi');
  };
}

export default initComponent(Page4xx5xx, withRouter);
