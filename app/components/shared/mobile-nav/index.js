import cx from 'classnames';
import BaseComponent from 'components/base';
import React from 'react';
import Link from 'utils/shims/link';

if (process.env.BROWSER) {
  require('assets/styles/components/mobile-nav.scss');
}

class Navbar extends BaseComponent {
  render() {
    const hasBackButton = typeof this.props.backHandler === 'function';
    const hasAction = typeof this.props.action === 'function';
    const showLogoInsteadOfBackButton = this.props.showLogo && !hasBackButton;

    return (
      <div className={cx('mobile-nav-wrapper', this.props.className)}>
        <div className="mobile-navbar">
          {hasBackButton ? (
            <a className="back-arrow" onClick={this.props.backHandler}>
              <i className="lz lz-light-arrow-left" />
            </a>
          ) : (
            showLogoInsteadOfBackButton && <Link to="/" className="logo-loship" />
          )}

          {this.props.title || 'Loship'}

          {hasAction && (
            <a className="nav-action" onClick={this.props.action}>
              {this.props.actionText}
            </a>
          )}
        </div>
      </div>
    );
  }
}

export default Navbar;
