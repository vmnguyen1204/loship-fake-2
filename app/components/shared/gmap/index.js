import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import PropTypes from 'prop-types';
import React from 'react';
import { getGoogleMap } from 'services/google-map';

if (process.env.BROWSER) {
  require('assets/styles/components/gmap.scss');
}

export default class Gmap extends BaseComponent {
  static contextTypes = {
    geoLocation: PropTypes.object,
    getGeoLocation: PropTypes.func.isRequired,
  }

  static propTypes = {
    defaultCenter: PropTypes.object.isRequired,
    center: PropTypes.object.isRequired,
    markers: PropTypes.array,
    showMarkers: PropTypes.bool,
    mode: PropTypes.oneOf(['route', 'static', 'hidden', 'interactive']),
  };

  static defaultProps = {
    markers: [],
    showMarkers: true,
  };

  componentDidUpdate(prevProps) {
    if (this.props.mode !== 'route') {
      this.updateMarker(this.props);
    } else if (this.props.routes !== prevProps.routes) {
      this.renderRoute(this.props);
    }
  }

  renderRoute = async (props) => {
    const googleMap = await getGoogleMap();
    if (props.routes) {
      googleMap.renderRoute(this.map, props.routes);
    } else {
      googleMap.clearRoute();
    }
  };

  updateMarker = async (props) => {
    if (!this.center) return;
    const googleMap = await getGoogleMap();

    /** Update CENTER */
    const centerPosition = props.center.position || props.defaultCenter;
    if (props.showMarkers) {
      this.center.marker.setMap(this.map);
      this.map.panTo(centerPosition);
      this.center.marker.setPosition(centerPosition);
    } else this.center.marker.setMap(null);

    if (props.center.hideInfo || !props.showMarkers) this.center.infoWindow.close();
    else this.center.infoWindow.open(this.map, this.center.marker);

    /** Update OTHER MARKERS */
    this.markers.forEach((marker, index) => {
      if (props.showMarkers) marker.marker.setMap(this.map);
      else marker.marker.setMap(null);

      if (props.markers[index].hideInfo || !props.showMarkers) marker.infoWindow.close();
      else marker.infoWindow.open(this.map, marker.marker);
    });

    /** Update GEO MARKER */
    if (this.geoMarker) {
      if (props.showMarkers) this.geoMarker.marker.setMap(this.map);
      else this.geoMarker.marker.setMap(null);
    }

    /** Update MAP BOUNDS */
    if (this.markers.length === 0 || !props.showMarkers) return;

    const bounds = googleMap.createLatLngBounds();
    bounds.extend(googleMap.createLatLng(centerPosition.lat, centerPosition.lng));
    props.markers.map((marker) => bounds.extend(googleMap.createLatLng(marker.position.lat, marker.position.lng)));

    this.map.fitBounds(bounds);
  };

  updateGeoLocationMarker = async (location) => {
    const geoLocation = location || this.context.geoLocation;
    if (!geoLocation) return;

    const googleMap = await getGoogleMap();
    this.geoMarker = {
      marker: googleMap.createMarker({
        map: this.map,
        icon: {
          url: '/dist/images/marker-geo.png',
          anchor: googleMap.createPoint(0, 0),
        },
        position: { lat: geoLocation.latitude, lng: geoLocation.longitude },
      }),
    };
  }

  getMapRef = async (ref) => {
    if (!ref) return;

    const { mode, defaultCenter, center, markers } = this.props;
    if (mode === 'route') return this.renderRoute(this.props);

    const googleMap = await getGoogleMap();
    this.mapContainer = ref;
    this.map = googleMap.createGoogleMap(this.mapContainer, {
      zoom: 18,
      center: center.position || defaultCenter,
      clickableIcons: false,
      controlSize: 24,
      disableDefaultUI: true,
    });

    this.center = {
      marker: googleMap.createMarker({
        map: this.map,
        icon: {
          url: center.icon || '/dist/images/marker-01.png',
          anchor: googleMap.createPoint(38, 70),
        },
        position: center.position || defaultCenter,
      }),
      infoWindow: googleMap.createInfoWindow({ content: `<div>${center.info || this.getString('deliver_here')}</div>` }),
    };

    this.markers = [];
    markers.forEach((marker, index) => {
      this.markers[index] = {
        marker: googleMap.createMarker({
          map: this.map,
          icon: marker.icon || '/dist/images/marker-01.png',
          position: marker.position,
        }),
        infoWindow: googleMap.createInfoWindow({ content: `<div>${marker.info || this.getString('deliver_here')}</div>` }),
      };
    });

    this.updateGeoLocationMarker();

    this.updateMarker(this.props, true);
    googleMap.addListener(this.map, 'idle', () => {
      if (!this.center) return;

      if (this.props.mode === 'interactive') {
        this.center.marker.setPosition(this.map.center);

        const newCenter = googleMap.createLatLng(this.map.center.lat(), this.map.center.lng());
        this.props.onCenterChange({ lat: newCenter.lat(), lng: newCenter.lng() });
      }
    });

    googleMap.addListener(this.map, 'drag', () => {
      if (!this.center) return;

      if (this.props.mode === 'interactive') {
        this.center.marker.setPosition(this.map.center);
      }
    });
  };

  reCenter = async () => {
    const hasGeoLocation = this.context.geoLocation?.latitude && this.context.geoLocation?.longitude;
    if (!hasGeoLocation) return this.context.getGeoLocation({
      force: true,
      callback: reCenterCallback.bind(this),
    });

    return reCenterCallback.call(this, this.context.geoLocation);

    function reCenterCallback(geoLocation) {
      const centerPosition = { lat: geoLocation.latitude, lng: geoLocation.longitude };

      this.map.setZoom(18);
      this.map.panTo(centerPosition);
      this.props.onCenterChange(centerPosition);
      if (!this.geoMarker) this.updateGeoLocationMarker(geoLocation);
    }
  }

  render() {
    return (
      <>
        <div ref={this.getMapRef} className="map-container" style={this.props.style} />
        <Button className="bg-white btn-re-center" onClick={this.reCenter}>
          <i className="lz lz-gps" />
        </Button>
      </>
    );
  }
}
