import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';

class SuccessAlert extends BaseComponent {
  render() {
    const { data } = this.props;
    return (
      <div className="alert-success">
        <div className="alert-content">{this.getString(data.get('message'), 'alert')}</div>
      </div>
    );
  }
}

export default initComponent(SuccessAlert);
