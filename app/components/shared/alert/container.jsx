import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { getAll } from 'reducers/alert';
import Alert from 'components/shared/alert';

class AlertContainer extends BaseComponent {
  static defaultProps = { dataGetter: getAll };

  render() {
    const { data } = this.props;
    return (
      <ul className="list-alert">
        {data.map((alertId) => (
          <li key={alertId}>
            <Alert params={{ alert: alertId }} />
          </li>
        ))}
      </ul>
    );
  }
}

export default initComponent(AlertContainer);
