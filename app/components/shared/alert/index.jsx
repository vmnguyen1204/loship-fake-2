import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { get as dataGetter } from 'reducers/alert';
import { destroy, expire } from 'actions/alert';
import ErrorAlert from 'components/shared/alert/error';
import SuccessAlert from 'components/shared/alert/success';
import cx from 'classnames';

if (process.env.BROWSER) {
  require('assets/styles/components/alert.scss');
}
class Alert extends BaseComponent {
  static defaultProps = { dataGetter };

  componentDidMount() {
    this.setCloseTimeOut();
  }

  componentDidUpdate() {
    const { data, params } = this.props;
    if (data && data.get('status')) {
      if (data.get('status') === 'expired' && !this.state.hide) {
        this.timeout = setTimeout(() => {
          this.props.dispatch(destroy(params));
        }, 600);
      }
      this.setCloseTimeOut();
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  setCloseTimeOut = () => {
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.close();
    }, 3000);
  };

  clearCloseTimeout = () => {
    if (this.timeout) clearTimeout(this.timeout);
  };

  close = () => {
    const { data, params } = this.props;
    if (data) {
      this.props.dispatch(expire(params));
      setTimeout(() => {
        this.props.dispatch(destroy(params));
      }, 600);
    }
  };

  render() {
    const { data } = this.props;
    let alert;
    switch (data.get('type')) {
      case 'error':
        alert = <ErrorAlert data={data} />;
        break;
      case 'success':
        alert = <SuccessAlert data={data} />;
        break;
      default:
        break;
    }

    return (
      <div
        className={cx('alert', data.get('status'))}
        onMouseOver={this.clearCloseTimeout}
        onMouseOut={this.setCloseTimeOut}
      >
        <a className="btn-close-alert" onClick={this.close}>
          <i className="lz lz-close" />
        </a>
        {alert}
      </div>
    );
  }
}

export default initComponent(Alert);
