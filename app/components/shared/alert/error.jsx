import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';

class ErrorAlert extends BaseComponent {
  render() {
    const { data } = this.props;
    let err = data.get('message');
    if (Array.isArray(err) && err.length > 0) {
      err = err[0];
      if (typeof err === 'object') {
        err = err.message || 'Error occurs, please try again later';
      }
    }
    return (
      <div className="alert-error">
        <div className="alert-image" />
        <div className="alert-content">{this.getString(err, 'alert')}</div>
      </div>
    );
  }
}

export default initComponent(ErrorAlert);
