import React from 'react';
import Hint from 'components/statics/hint';
import cx from 'classnames';

export default function LoshipPartnerBadge({ className, getString, shouldCompact = false, iconFw = false }) {
  let label = getString('label_loship_partner');
  if (shouldCompact && typeof window !== 'undefined' && window.innerWidth < 400) {
    label = getString('label_loship_partner_short');
  }

  return (
    <Hint className={cx('loship-partner-icon', className)} text={[getString('hint_loship_partner')]} noHoverHint>
      <i className={cx('lz lz-star-o', iconFw && 'lz-fw')} />
      {label}
    </Hint>
  );
}
