import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

export default class Checkbox extends React.Component {
  static propTypes = {
    checked: PropTypes.bool.isRequired,
    onChange: PropTypes.func,
  };

  render() {
    const { checked, isRadio, className, title, disabled } = this.props;

    return (
      <div
        className={cx('checkbox', className, { disabled, active: checked, radio: isRadio })}
        onClick={this.handleClick}
        title={title}
      >
        {this.props.children}
        <i className={cx('lz icon-checkbox', { 'lz-check': !isRadio && !!checked })} />
      </div>
    );
  }

  handleClick = () => {
    const { checked, onChange, disabled } = this.props;
    if (disabled) return false;


    typeof onChange === 'function' && onChange(!checked);
  };
}
