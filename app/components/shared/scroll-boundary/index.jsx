import cx from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { iife } from 'utils/helper';

class ScrollBoundary extends React.Component {
  static propTypes = {
    marginTop: PropTypes.number,
    marginBottom: PropTypes.number,
    fixedMarginTop: PropTypes.number,
    fixedMarginBottom: PropTypes.number,
  };

  static defaultProps = {
    marginTop: 0,
    marginBottom: 0,
    fixedMarginTop: 0,
    fixedMarginBottom: 0,
  };

  state = {};

  componentDidMount() {
    // TODO: sticky with specific elem (support ID)
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    return (
      <div className={cx('scroll-boundary', this.props.className)}>
        <div ref={this.handleRef} className="scroll-boundary-content" style={this.state.boundaryStyle}>
          {this.props.children}
        </div>
      </div>
    );
  }

  handleRef = (ref) => {
    if (ref) {
      this.ref = ref;
      this.elem = this.ref;
      this.parentElem = this.ref.offsetParent;

      this.elemOffsetTop = this.elem.getBoundingClientRect().y - this.parentElem.getBoundingClientRect().y;
      this.handleScroll();
    }
  };

  handleScroll = () => {
    if (!this.elem || !this.parentElem) return;
    const { marginTop, marginBottom, fixedMarginTop, fixedMarginBottom } = this.props;

    const height = window.innerHeight;
    const rect = this.elem.getBoundingClientRect();
    const parentRect = this.parentElem.getBoundingClientRect();

    // Always position relative when parent not have enough space
    if (rect.height + this.elemOffsetTop >= parentRect.height) return;

    let boundaryStyle = {};
    let currentMode = this.lastMode;

    /** GET RAW MODE */
    if (this.elemOffsetTop + parentRect.y + marginTop >= fixedMarginTop) {
      // When elem not reach top of viewport
      currentMode = 'ABSOLUTE_TOP';
      boundaryStyle = { marginTop };
    } else {
      // Elem reach top of viewport
      currentMode = 'STICKY_TOP';
      boundaryStyle = { position: 'fixed', top: fixedMarginTop };
    }

    if (parentRect.height + parentRect.y <= rect.height + rect.y + marginBottom) {
      // Elem reach bottom of container
      currentMode = 'ABSOLUTE_BOTTOM';
      boundaryStyle = { position: 'absolute', bottom: marginBottom };
    }
    /** GET RAW MODE - END */

    // More case when scroll-boundary need to be scroll
    iife(() => {
      const scrollDirection = parentRect.y - this.lastScrollPosition > 0 ? 'up' : 'down';
      this.lastScrollPosition = parentRect.y;
      const delta = rect.height - height;

      if (scrollDirection === 'up') {
        if (currentMode === 'STICKY_TOP' && rect.y + fixedMarginTop < 0) {
          // Absolute top for enable scroll to top of elem (currently it sticky bottom because rect.y < 0)
          currentMode = 'ABSOLUTE_TOP';
          boundaryStyle = { position: 'absolute', top: -(parentRect.y + delta) + fixedMarginBottom };
        }
        if (currentMode === 'ABSOLUTE_BOTTOM' && rect.y + fixedMarginTop > 0) {
          // When at bottom of container, switch to sticky top if continue scroll up
          currentMode = 'STICKY_TOP';
          boundaryStyle = { position: 'fixed', top: fixedMarginTop };
        }
        return;
      }

      if (currentMode === 'STICKY_TOP') {
        if (rect.y + delta > fixedMarginTop) {
          // Absolute for enable scroll to bottom of elem
          currentMode = 'ABSOLUTE_TOP';
          boundaryStyle = { position: 'absolute', top: -parentRect.y };
        } else if (delta > fixedMarginBottom) {
          // Already scroll to bottom of elem -> sticky bottom
          currentMode = 'STICKY_BOTTOM';
          boundaryStyle = { position: 'fixed', bottom: fixedMarginBottom };
        }
      }
    });

    // Only update when change mode, better performance
    if (currentMode !== this.lastMode) {
      this.setState({ boundaryStyle });
      this.lastMode = currentMode;
    }
  };
}

export default ScrollBoundary;
