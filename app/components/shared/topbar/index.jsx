import { APP_INFO } from 'settings/variables';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import InputPhoneNumber from 'components/shared/input/phone-number';
import LanguageSwitcher from 'components/statics/language-switcher';
import React from 'react';
import initComponent from 'lib/initComponent';
import { sendGAEvent } from 'actions/factory';
import { sendUseAppInvitation } from 'actions/home';
import { isValidPhoneNumber } from 'utils/format';
import { GENERIC_POPUP, open } from 'actions/popup';

export class Topbar extends BaseComponent {
  state = { countryCode: '84' };

  render() {
    const { countryCode } = this.state;

    return (
      <div className="topbar-wrapper">
        <div className="container">
          <div className="topbar">
            <LanguageSwitcher />
            <span className="mr-8">
              {this.getString('download_loship_app')}
              <a href="https://goo.gl/CDFMhZ" target="_blank" rel="noopener" className="btn-platform">
                {this.getString('iOS')}
              </a>
              <a href="https://goo.gl/N5Vs4V" target="_blank" rel="noopener" className="btn-platform">
                {this.getString('Android')}
              </a>
            </span>

            <span className="mr-8">{this.getString('or')}</span>

            <InputPhoneNumber
              className="mr-8"
              btnClassName="bg-transparent text-black"
              defaultCountryCode={countryCode}
              onChange={this.handlePhoneNumberInput}
              onChangeCountryCode={this.handleChangeCountryCode}
              mode="caret"
              placeholder={this.getString('type_phone_number_here')}
            />

            <Button
              type="link"
              className="btn-send"
              onClick={this.handleSendUseAppInvitation}
              disabled={this.state.isSendingInvitation}
            >
              {this.getString('send_link')}
            </Button>

            <div className="dropdown-wrapper">
              <span>{this.getString('become_a_loship_partner')}</span>
              <div className="dropdown-menu centered">
                <ul>
                  <li className="dropdown-item">
                    <a href="https://doitacloship.lozi.vn/" target="_blank" rel="noopener">
                      {this.getString('for_eatery_owners')}
                    </a>
                  </li>
                  <li className="dropdown-item">
                    <a href="http://chienbinhloship.com/" target="_blank" rel="noopener">
                      {this.getString('as_a_loship_driver')}
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <a
              href={`https://lomart.vn/?utm_source=${APP_INFO.serviceName}&utm_medium=topbar`}
              target="_blank"
              rel="noopener"
              className="text-black"
            >
              {this.getString('looking_for_groceries_try_lomart')}
            </a>
          </div>
        </div>
      </div>
    );
  }

  handleChangeCountryCode = (countryCode) => {
    this.setState({ countryCode });
  };

  handlePhoneNumberInput = (phoneNumber) => {
    this.setState({ phoneNumber });
  };

  validate = (fields) => {
    if (fields.includes('phoneNumber')) {
      if (!this.state.phoneNumber) return this.setState({ error: this.getString('phone_number_is_required') });
      if (!isValidPhoneNumber(this.state.phoneNumber)) return this.setState({ error: this.getString('phone_number_is_invalid') });
    }

    if (fields.includes('countryCode')) {
      if (!this.state.countryCode) return this.setState({ error: this.getString('country_code_is_required') });
    }

    return true;
  };

  handleSendUseAppInvitation = () => {
    if (!this.validate(['phoneNumber', 'countryCode'])) return;

    const { countryCode, phoneNumber } = this.state;
    sendGAEvent('loship_action', 'ACTION_SEND_DOWNLOADSMS');

    this.setState({ isSendingInvitation: true }, () => {
      this.props.dispatch(
        sendUseAppInvitation({
          countryCode,
          phoneNumber,
          callback: () => {
            this.setState({ isSendingInvitation: false });
            this.props.dispatch(
              open(GENERIC_POPUP, {
                className: 'mw-400',
                content: (
                  <div>
                    <i className="lz lz-check-o text-green" style={{ fontSize: 60 }} />
                    <h3 className="mt-16">{this.getString('send_sms_invitation_success')}</h3>
                  </div>
                ),
                autoCloseTimeout: 3000,
              }),
            );
          },
          callbackFailure: () => this.setState({ isSendingInvitation: false }),
        }),
      );
    });
  };
}

export default initComponent(Topbar);
