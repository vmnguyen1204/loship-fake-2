import React from 'react';
import Hint from 'components/statics/hint';
import cx from 'classnames';

export default function merchantthoughtfulpartner({ openpopup, className, getString, iconFw = false }) {
  const labelmerchantlosupply = getString('merchant_thoughtful');

  return (
    <Hint noClickHint={true} onClick={openpopup} className={cx('loship-partner-icon thoughtful', className)}>
      <i className={cx('lz lz-star-s', iconFw && 'lz-fw')} />
      {labelmerchantlosupply}
    </Hint>
  );
}
