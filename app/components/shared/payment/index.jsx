import { SELECT_PAYMENT_METHOD, open } from 'actions/popup';

import BaseComponent from 'components/base';
import { LIST_PAYMENT_TEXT } from 'utils/json/payment';
import PropTypes from 'prop-types';
import React from 'react';
import { formatCard } from 'utils/format';
import initComponent from 'lib/initComponent';

if (process.env.BROWSER) {
  require('assets/styles/components/payment.scss');
}

class PaymentSelector extends BaseComponent {
  static defaultProps = { paymentFees: [] };

  static contextTypes = { isMobile: PropTypes.bool };

  state = {};

  renderPaymentIcon = (paymentMethod) => {
    if (paymentMethod === 'cod') return <img src="/dist/images/payment-cod.png" alt="Payment cod" />;
    if (['zalopayapp', 'momopay', 'momopayinapp', 'sacombankpay'].some(
      (method) => paymentMethod.includes(method),
    )) return <img src="/dist/images/payment-wallet.png" alt="Payment wallet" />;
    if (paymentMethod.includes('fullpayment')) return <img src="/dist/images/payment-atm.png" alt="Payment atm" />;
    if (paymentMethod.includes('CC')) return <img src="/dist/images/payment-card.png" alt="Payment card" />;

    return <img src="/dist/images/payment-card.png" alt="Payment online" />;
  };

  renderBtnEdit = () => {
    if (typeof this.props.onSelect !== 'function') return null;
    if (this.context.isMobile) return <i id="btn-payment-edit" className="lz lz-pencil" onClick={this.openPopup} />;
    return (
      <a id="btn-payment-edit" onClick={this.openPopup}>
        [{this.getString('tap_to_change_payment_method')}]
      </a>
    );
  };

  render() {
    const { defaultValue = { method: 'cod' } } = this.props;

    return (
      <div key={this.props.key} className="payment-method">
        <div className="payment-method-item">
          {this.renderPaymentIcon(defaultValue.method)}
          <div className="title">{this.getString(LIST_PAYMENT_TEXT[defaultValue.method] || 'payment_online')}</div>
          {this.renderBtnEdit()}
        </div>

        {(() => {
          if (defaultValue.method.includes('fullpayment') && !!defaultValue.bank) {
            return (
              <div className="payment-method-item extra">
                <img src={defaultValue.bank.image} alt={defaultValue.bank.name} />
                {defaultValue.bank.name} ({defaultValue.bank.code})
              </div>
            );
          }

          if (defaultValue.method.includes('CC')) {
            if (defaultValue.card) {
              return (
                <div className="payment-method-item extra">
                  <img src={defaultValue.card.image} alt="Card number" />
                  {formatCard(defaultValue.card.prefixNumber, defaultValue.card.postfixNumber)}
                </div>
              );
            }
            if (defaultValue.saveCard) {
              return (
                <div className="payment-method-item extra">
                  <i className="lz lz-check text-green" />
                  {this.getString('save_card_agree')}
                </div>
              );
            }
          }
        })()}
      </div>
    );
  }

  openPopup = () => {
    return this.props.dispatch(
      open(SELECT_PAYMENT_METHOD, {
        defaultValue: this.props.defaultValue,
        paymentFees: this.props.paymentFees,
        paymentCardFees: this.props.paymentCardFees,
        paymentsSupportTokenized: this.props.paymentsSupportTokenized,
        onSelectPaymentMethod: (...args) => {
          this.props.onSelect(...args);
          this.forceUpdate(); // TODO: investigate why we need it for re-render
        },
        onCreatePaymentCard: this.props.onCreatePaymentCard,
      }),
    );
  };
}

export default initComponent(PaymentSelector);
