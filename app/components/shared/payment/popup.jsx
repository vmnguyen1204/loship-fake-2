import { SELECT_BANK, SELECT_PAYMENT_METHOD, close, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Checkbox from 'components/shared/checkbox';
import Popup from 'components/shared/popup';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { get as getPopup } from 'reducers/popup';
import { getCurrentUser } from 'reducers/user';
import { formatCard } from 'utils/format';
import { LIST_PAYMENT_TEXT } from 'utils/json/payment';

class SelectPaymentMethodPopup extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (!data) return;

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
    params: { popup: SELECT_PAYMENT_METHOD },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  state = {
    saveCard: true,
    isBankRequired: false,
    isTokenizationEnabled: false,
  };

  close = () => {
    this.setState({ saveCard: true, isBankRequired: false, isTokenizationEnabled: false });
    this.ref.handleClose(() => this.props.dispatch(close(SELECT_PAYMENT_METHOD)));

    const params = this.props.data && this.props.data.get('params');
    typeof params.onClose === 'function' && params.onClose();
  };

  componentDidUpdate() {
    this.ensurePaymentMethod();
  }

  render() {
    const { isMobile } = this.context;

    const params = this.props.data && this.props.data.get('params');
    const { paymentFees, paymentCardFees = [], paymentsSupportTokenized, disabledPaymentMethods = [] } = params;

    const hasAsterisks = paymentFees.some((option) => option.fee > 0);

    return (
      <Popup ref={this.getRef}
        className="select-payment" wrapperClassName={params.wrapperClassName}
        handleClose={this.close} animationStack={3}
      >
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent', { 'text-red': isMobile })} onClick={this.close}>
            <i className={cx('lz', { 'lz-light-arrow-left': isMobile, 'lz-close': !isMobile })} />
          </Button>
          {this.getString('payment_method_popup_header')}
        </div>

        <div className="content scroll-nice">
          {paymentsSupportTokenized && paymentsSupportTokenized.size > 0 && (
            <div className="bg-white my-16">
              <div className="title">{this.getString('add_card')}</div>

              <div className="list-view list-payment-method">
                {paymentsSupportTokenized.map((payment) => (
                  <div key={payment.id} className="list-item card" onClick={this.handleAddPaymentCard(payment)}>
                    <i className="lz lz-plus-r" />
                    <span className="pl-16">
                      {this.getString('add')} {this.getString(LIST_PAYMENT_TEXT[payment.method])}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          )}

          {paymentCardFees.length > 0 && (
            <div className="bg-white mb-16">
              <div className="title">{this.getString('use_saved_card')}</div>

              <div className="list-view list-payment-method">
                {paymentCardFees.map((paymentCardFee) => (
                  <Checkbox
                    key={paymentCardFee.id}
                    className="list-item card"
                    isRadio
                    {...this.getPaymentCardFeeCheckLink(paymentCardFee)}
                  >
                    <span>
                      <img src={paymentCardFee.image} alt="card" />
                      {formatCard(paymentCardFee.prefixNumber, paymentCardFee.postfixNumber)}
                    </span>
                  </Checkbox>
                ))}
              </div>
            </div>
          )}

          <div className="bg-white mb-16">
            <div className="title">{this.getString('select_payment_method')}</div>
            {hasAsterisks && (
              <p className="note">
                {this.getString('(*)')}&nbsp;
                {this.getString('convenience_fee_applied')}
              </p>
            )}

            <div className="list-view list-payment-method">
              {paymentFees.map((paymentFee) => (
                <Checkbox
                  key={paymentFee.method}
                  className="list-item"
                  isRadio
                  disabled={disabledPaymentMethods.includes(paymentFee.method)}
                  {...this.getPaymentFeeCheckLink(paymentFee)}
                >
                  {this.getString(LIST_PAYMENT_TEXT[paymentFee.method])}
                  {!!paymentFee.fee && ' (*)'}
                </Checkbox>
              ))}
            </div>
          </div>

          {this.state.isTokenizationEnabled && (
            <div id="scrollElem" className="bg-white my-16">
              <Checkbox className="checkbox-saved" {...this.getSaveCardCheckLink()}>
                {this.getString('save_card')}
              </Checkbox>
              <div className="note">{this.getString('save_card_description')}</div>
            </div>
          )}
        </div>

        <div className="popup-footer">
          <Button
            type="primary"
            fw
            onClick={this.selectPaymentMethod}
            disabled={!params.defaultValue && !this.state.payment}
          >
            {this.getString('select_this_payment_method')}
          </Button>
        </div>
      </Popup>
    );
  }

  getRef = (ref) => {
    if (ref) this.ref = ref;
  };

  ensurePaymentMethod = () => {
    const params = this.props.data && this.props.data.get('params');
    if (!params) return;

    const payment = this.state.payment || params.defaultValue || {};
    const { paymentFees = [] } = params;

    const paymentFee = (!payment.card && paymentFees.find((fee) => fee.method === payment.method)) || {};
    const { isBankRequired, isTokenizationEnabled } = paymentFee.paymentMethod || {};
    if (isBankRequired !== this.state.isBankRequired || isTokenizationEnabled !== this.state.isTokenizationEnabled) {
      this.setState({ isBankRequired, isTokenizationEnabled });
    }
  };

  getPaymentCardFeeCheckLink = (paymentCardFee) => {
    if (!paymentCardFee) return {};
    const params = (this.props.data && this.props.data.get('params')) || {};
    const payment = this.state.payment || params.defaultValue || {};

    const method = (paymentCardFee.paymentMethodFee && paymentCardFee.paymentMethodFee.method) || 'cod';
    const card = {
      id: paymentCardFee.id,
      type: paymentCardFee.type,
      image: paymentCardFee.image,
      prefixNumber: paymentCardFee.prefixNumber,
      postfixNumber: paymentCardFee.postfixNumber,
    };
    const type = paymentCardFee.paymentMethodFee &&
      paymentCardFee.paymentMethodFee.paymentMethod &&
      paymentCardFee.paymentMethodFee.paymentMethod.type;

    return {
      checked: payment.card && payment.card.id === paymentCardFee.id,
      onChange: () => this.setState({ payment: { method, card, type }, isBankRequired: false, isTokenizationEnabled: false }),
    };
  };

  getPaymentFeeCheckLink = (paymentFee) => {
    if (!paymentFee) return {};
    const params = (this.props.data && this.props.data.get('params')) || {};
    const payment = this.state.payment || params.defaultValue || {};
    const { isBankRequired, isTokenizationEnabled, type } = paymentFee.paymentMethod || {};

    return {
      checked: !payment.card && payment.method === paymentFee.method,
      onChange: () => this.setState({ payment: { method: paymentFee.method, type }, isBankRequired, isTokenizationEnabled }, () => {
        if (isTokenizationEnabled) {
          const elem = document.getElementById('scrollElem');
          elem.scrollIntoView({ behavior: 'smooth', block: 'start' });
        }
      }),
    };
  };

  getSaveCardCheckLink = () => {
    return {
      checked: this.state.saveCard,
      onChange: (checked) => this.setState({ saveCard: checked }),
    };
  };

  selectPaymentMethod = () => {
    const params = (this.props.data && this.props.data.get('params')) || {};
    const payment = this.state.payment || params.defaultValue || {};

    const { isBankRequired, isTokenizationEnabled, saveCard } = this.state;

    if (isBankRequired) {
      return this.props.dispatch(
        open(SELECT_BANK, {
          title: this.getString(LIST_PAYMENT_TEXT[payment.method]),
          paymentMethod: payment.method,
          wrapperClassName: params.wrapperClassName,
          onSelectBank: (bank) => {
            if (!bank) return;
            params.onSelectPaymentMethod(payment.method, { bank, type: payment.type });
            this.close();
          },
        }),
      );
    }

    params.onSelectPaymentMethod(payment.method, {
      card: payment.card,
      type: payment.type,
      saveCard: isTokenizationEnabled && saveCard,
    });
    this.close();
  };

  handleAddPaymentCard = (payment) => () => {
    const params = (this.props.data && this.props.data.get('params')) || {};
    typeof params.onCreatePaymentCard === 'function' && params.onCreatePaymentCard(payment);
  };
}

export default initComponent(SelectPaymentMethodPopup, withRouter);
