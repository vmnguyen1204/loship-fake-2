import { CONFIRM_POPUP, SELECT_BANK, close, open } from 'actions/popup';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Checkbox from 'components/shared/checkbox';
import Popup from 'components/shared/popup';
import PropTypes from 'prop-types';
import { REGEX_STRING } from 'utils/data';
import React from 'react';
import { Waypoint } from 'react-waypoint';
import cx from 'classnames';
import { get as dataGetter } from 'reducers/popup';
import { fetchPaymentBank } from 'actions/merchant';
import initComponent from 'lib/initComponent';
import { normalize } from 'utils/format';

class SelectBankPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: SELECT_BANK },
  };

  static contextTypes = { isMobile: PropTypes.bool };

  close = (sendData = false) => {
    const params = (this.props.data && this.props.data.get('params')) || {};
    if (sendData && typeof params.onSelectBank === 'function') params.onSelectBank(this.state.selectedBank);

    this.setState({ banks: undefined, selectedBank: undefined, page: undefined });
    this.ref.handleClose(() => this.props.dispatch(close(SELECT_BANK)));
  };

  componentDidUpdate() {
    if (!this.state.banks) this.fetchBanks();
  }

  render() {
    const { isMobile } = this.context;
    const data = this.getResults();
    const { page = 1 } = this.state;

    const params = (this.props.data && this.props.data.get('params')) || {};

    return (
      <Popup ref={this.getRef}
        className="select-payment" wrapperClassName={params.wrapperClassName}
        handleClose={this.close} animationStack={3}
      >
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent', { 'text-red': isMobile })} onClick={this.close}>
            <i className={cx('lz', { 'lz-light-arrow-left': isMobile, 'lz-close': !isMobile })} />
          </Button>
          {params.title}
        </div>

        <div className="content">
          <div className="search-box">
            <input
              ref={this.getInputRef}
              onChange={this.handleInputChange}
              placeholder={this.getString('input_country_name_or_code')}
            />
            <i className="lz lz-search btn-search" />
          </div>

          <div className="list-view list-bank">
            {data.slice(0, page * 16).map((bank) => (
              <Checkbox key={bank.code} className="list-item bank" {...this.getCheckboxCheckedLink(bank)}>
                <span>
                  <img src={bank.image} alt="Bank" />
                  {bank.name} ({bank.code})
                </span>
              </Checkbox>
            ))}

            {page * 16 < data.length && (
              <Waypoint onEnter={() => this.setState({ page: page + 1 })} topOffset="-100px" />
            )}
            {!data.length && <div className="list-item">{this.getString('no_result')}</div>}
          </div>
        </div>

        <div className="popup-footer">
          <Button type="primary" fw onClick={this.selectBank}>
            {this.getString('select_this_bank')}
          </Button>
        </div>
      </Popup>
    );
  }

  getRef = (ref) => {
    if (ref) this.ref = ref;
  };

  getInputRef = (ref) => {
    if (!ref) return;
    this.input = ref;
    this.input.focus();
  };

  fetchBanks = () => {
    const params = (this.props.data && this.props.data.get('params')) || {};

    fetchPaymentBank({
      paymentMethod: params.paymentMethod,
      callback: (res) => {
        if (res.status === 200) {
          const data = res.body.data;
          return this.setState({ banks: data });
        }

        this.props.dispatch(
          open(CONFIRM_POPUP, {
            title: this.getString('something_wrong'),
            description: this.getString('fetch_shipping_fee_failed_message'),
            confirmBtn: { text: this.getString('try_again') },
            cancelBtn: { text: this.getString('back') },
            onConfirm: this.fetchBanks,
            onCancel: this.close,
            canNotDismiss: true,
          }),
        );
      },
    });
  };

  selectBank = () => {
    if (!this.state.selectedBank) return;
    return this.close(true);
  };

  handleInputChange = (e) => {
    const value = e.target.value;
    this.setState({ value });
  };

  getCheckboxCheckedLink = (bank) => {
    if (!bank) return {};
    const currentValue = this.state.selectedBank && this.state.selectedBank.code;

    return {
      checked: currentValue === bank.code,
      onChange: () => this.setState({ selectedBank: bank }),
    };
  };

  getResults = () => {
    if (!this.state.banks) return [];

    const searchTerm = normalize(this.input && this.input.value);
    if (!searchTerm) return this.state.banks || [];

    return this.state.banks.filter((bank) => {
      if (normalize(bank.name.toLowerCase()).match(REGEX_STRING(searchTerm))) return true;
      if (bank.code.toLowerCase().match(REGEX_STRING(searchTerm))) return true;
      return false;
    });
  };
}

export default initComponent(SelectBankPopup);
