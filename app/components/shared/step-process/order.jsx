import React from 'react';
import BaseComponent from 'components/base';
import classnames from 'classnames';

class StepProcess extends BaseComponent {
  renderStepIcon = (step, label, first = false) => {
    const currentStep = this.props.step;
    return (
      <div
        className={classnames('step', {
          done: currentStep > step,
          current: currentStep === step,
        })}
      >
        {!first && <i className={`lz lz-light-arrow-right ${currentStep >= step ? 'done' : ''}`} />}
        {currentStep > step ? <i className="lz lz-check" /> : <span className="step-number">{step}</span>}
        <span className="label">{label}</span>
      </div>
    );
  };

  render() {
    const labels = this.props.labels || [
      this.getString('choose_dish', 'eatery'),
      this.getString('delivery_information'),
      this.getString('order_preview'),
    ];
    return (
      <div className="step-process">
        {this.renderStepIcon(1, labels[0], true)}
        {this.renderStepIcon(2, labels[1])}
        {this.renderStepIcon(3, labels[2])}
      </div>
    );
  }
}

export default StepProcess;
