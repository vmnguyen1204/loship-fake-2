import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { getCurrentUser } from 'reducers/user';
import { hasRole } from 'utils/access';

class ActionButton extends BaseComponent {
  static defaultProps = { dataGetter: getCurrentUser };

  render() {
    const { data, roles, children, owner, isValid } = this.props;
    if (
      hasRole(data, roles) ||
        (!!owner && data.get('username') === owner) ||
        (typeof isValid === 'function' && isValid())
    ) {
      return children;
    }
    return null;
  }
}

export default initComponent(ActionButton);
