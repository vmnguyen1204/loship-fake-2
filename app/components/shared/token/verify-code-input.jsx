import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import cx from 'classnames';
import PropTypes from 'prop-types';

if (process.env.BROWSER) {
  require('assets/styles/components/verify-code-box.scss');
}

class VerifyCodeInput extends BaseComponent {
  static propTypes = { onCodeChange: PropTypes.func.isRequired };

  render() {
    const { heading, className } = this.props;

    return (
      <div className={cx('verify-code-box', className)}>
        {heading}

        <form onSubmit={(e) => e.preventDefault()}>
          <input
            className="verify-code"
            type="text"
            name="code_1"
            maxLength="1"
            onKeyPress={this.handleInput}
            onKeyDown={this.handleControlKey}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            ref={(ref) => { this.code_1 = ref; }}
          />
          <input
            className="verify-code"
            type="text"
            name="code_2"
            maxLength="1"
            onKeyPress={this.handleInput}
            onKeyDown={this.handleControlKey}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            ref={(ref) => { this.code_2 = ref; }}
          />
          <input
            className="verify-code"
            type="text"
            name="code_3"
            maxLength="1"
            onKeyPress={this.handleInput}
            onKeyDown={this.handleControlKey}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            ref={(ref) => { this.code_3 = ref; }}
          />
          <input
            className="verify-code"
            type="text"
            name="code_4"
            maxLength="1"
            onKeyPress={this.handleInput}
            onKeyDown={this.handleControlKey}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            ref={(ref) => { this.code_4 = ref; }}
          />
        </form>
      </div>
    );
  }

  handleChange = () => {
    if (!this.code_1.value || !this.code_2.value || !this.code_3.value || !this.code_4.value) {
      this.handleFocus(true);
      this.props.onCodeChange(undefined);
      return false;
    }

    const code = `${this.code_1.value}${this.code_2.value}${this.code_3.value}${this.code_4.value}`;
    this.props.onCodeChange(code);
    return code;
  };

  handleInput = (e) => {
    if (e.charCode < 48 || e.charCode > 57) {
      e.preventDefault();
    }
  };

  handleFocus = () => {
    if (!this.code_1.value) this.code_1.focus();
    else if (!this.code_2.value) this.code_2.focus();
    else if (!this.code_3.value) this.code_3.focus();
    else this.code_4.focus();
  };

  handleRemoveCode = () => {
    if (this.code_4.value) {
      this.code_4.value = '';
      this.code_3.focus();
    } else if (this.code_3.value) {
      this.code_3.value = '';
      this.code_2.focus();
    } else if (this.code_2.value) {
      this.code_2.value = '';
      this.code_1.focus();
    } else {
      this.code_1.value = '';
      this.code_1.focus();
    }

    this.props.onCodeChange(undefined);
  };

  handleControlKey = (e) => {
    if (e.key === 'Backspace') {
      this.handleRemoveCode();
      return;
    }

    if (e.key === 'Enter') this.handleChange();
  };
}

export default initComponent(VerifyCodeInput);
