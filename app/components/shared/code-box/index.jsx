import callAPI from 'actions/helpers';
import { LOGIN, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import React from 'react';
import { getPromotionCode } from 'reducers/code';
import { getCurrentUsername } from 'reducers/metadata';
import { getGlobalAddress } from 'reducers/new/metadata';
import { getUser } from 'reducers/user';
import { addCurrency } from 'utils/format';
import { debouncer } from 'utils/helper';

if (process.env.BROWSER) {
  require('assets/styles/components/code-box.scss');
}

const getCurrentUser = (state) => {
  const username = getCurrentUsername(state);
  return getUser(state, { user: username });
};

class CodeBox extends BaseComponent {
  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();

      const promotionCode = getPromotionCode(state);
      if (promotionCode) data = data.merge(promotionCode);

      const user = getCurrentUser(state);
      if (user) data = data.set('user', user);

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      return data;
    },
    customRender: true,
  };

  componentDidUpdate(prevProps) {
    if (prevProps.data && !this.props.data) this.updateRef();
  }

  renderCodeDisabled = () => {
    return (
      <div className="code-box-content">
        <i className="lz lz-star" />
        <div className="code">
          {this.getString(
            this.props.serviceName === 'losend'
              ? 'this_order_has_already_been_applied_the_one_price_shipping_promotion'
              : 'promotion_is_not_available_for_this_order',
          )}
        </div>
      </div>
    );
  };

  renderCodeValid = () => {
    const { data, promotionStatus = {} } = this.props;
    const isPromotionAccepted = !promotionStatus.status || promotionStatus.status === 'ACCEPTED';
    if (!data) return null;

    return (
      <div className={cx('code-box-content', isPromotionAccepted && 'text-green')}>
        <i className="lz lz-check-o" />
        <div className="code-main">
          <div className="code">{data.get('code')}</div>
          <i className="lz lz-close" onClick={this.handleClearCode} />
        </div>
        {!this.props.hideDiscount && (
          <div className="pull-right">-{addCurrency(isPromotionAccepted ? this.props.discount : 0)}</div>
        )}
        {!isPromotionAccepted && <div className="notice">{promotionStatus.message}</div>}
      </div>
    );
  };

  renderCodeResult = () => {
    const { data } = this.props;
    const { isChecking } = this.state;

    if (isChecking) return <i className="lz lz-hourglass pending" />;
    if (!data || !data.get('code')) return false;
    if (!data.get('value')) return <i className="lz lz-close-bold error" />;

    return <i className="lz lz-check success" />;
  };

  render() {
    const { promotionStatus = {} } = this.props;
    const isPromotionAccepted = !promotionStatus.status || promotionStatus.status === 'ACCEPTED';

    return (
      <div className={cx('code-box', this.props.className, !isPromotionAccepted && 'bg-error')}>
        {(() => {
          if (this.props.promotionEnable === false) return this.renderCodeDisabled();
          if (this.props.data && this.props.data.get('value')) return this.renderCodeValid();
          return (
            <div className="code-box-content">
              <span>{this.getString('promo_code')}:</span>
              <input
                type="text"
                name="code"
                ref={this.updateRef}
                autoComplete="off"
                onClick={this.handleClick}
                onChange={this.handleSubmit}
                onFocus={this.handleFocus}
              />
              {this.renderCodeResult()}
            </div>
          );
        })()}
      </div>
    );
  }

  updateRef = (ref) => {
    if (ref) this.ref = ref;
    if (!this.ref) return;

    const { data } = this.props;
    this.ref.value = (data && data.get('code')) || '';
    if (this.props.focus) this.ref.focus();
  };

  handleSubmit = (e) => {
    const code = e.target.value.toUpperCase();
    const globalAddress = this.props.globalAddress;

    if (!code) return this.handleClearCode();
    this.ref.value = code;

    this.setState({ isChecking: true });
    this.timeout = debouncer(
      this.timeout,
      async () => {
        const { eateryId } = this.props;
        const res = await callAPI('post', {
          url: '/promotions',
          content: {
            code,
            eateryId,
            cityId: globalAddress?.cityId,
          },
        });

        if (res.status !== 200) this.handleUpdatePromotion({ code });
        else {
          const data = res.body && res.body.data;
          const promoData = Array.isArray(data) ? data[0] : data;
          this.handleUpdatePromotion(promoData);
        }

        this.setState({ isChecking: false });
      },
      500,
    );
    return false;
  };

  handleFocus = () => {
    if (!this.props.user) {
      this.props.dispatch(open(LOGIN));
    }
  };

  handleClick = () => {
    if (!this.ref) return;
    this.ref.select();
  };

  handleClearCode = () => {
    this.props.dispatch({ type: 'PROMOTION_CLEAR' });
  };

  handleUpdatePromotion = (promotion) => {
    this.props.dispatch({ type: 'PROMOTION_FETCH', data: promotion });
  };
}

export default initComponent(CodeBox);
