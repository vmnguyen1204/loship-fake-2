import { sendGAEvent } from 'actions/factory';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { getCurrentUser } from 'reducers/user';
import { COOKIE_04 } from 'settings/constants';
import { LINK_DOMAIN } from 'settings/variables';
import { iife } from 'utils/helper';
import * as cookie from 'utils/shims/cookie';

if (process.env.BROWSER) {
  require('assets/styles/components/banner.scss');
}

const WHITELIST_HIDE_STICKY_BANNER = [
  '/m/buy',
  '/m/tai-khoan/don-hang',
  '/d/buy',
  '/d/tai-khoan/don-hang',
  '/g/',
  '/payment',
];
const isWhitelistHideStickyBanner = (pathname) => {
  return WHITELIST_HIDE_STICKY_BANNER.some((path) => {
    if (pathname.indexOf(path) >= 0) return true;
    return false;
  });
};

class StickyAppBanner extends BaseComponent {
  static contextTypes = {
    partnerName: PropTypes.string,
  };

  static defaultProps = {
    dataGetter: getCurrentUser,
    customRender: true,
  }

  state = {
    bannerMode: 'NORMAL', // CLOSE | NORMAL | VIEWER
    closeBanner: false,
    platform: null,
  };

  componentDidMount() {
    this.checkDevicePlatform();
    this.checkBannerMode();

    window.addEventListener('blur', this.cancelRedirect);
  }

  componentDidUpdate() {
    this.checkBannerMode();
  }

  render() {
    const { location } = this.props;
    const { platform, bannerMode } = this.state;

    if (!['VIEWER'].includes(bannerMode)) {
      if (this.context.partnerName !== 'none') return null;
      if (isWhitelistHideStickyBanner(location.pathname)) return null;
      if (!platform || !bannerMode || bannerMode === 'CLOSE') return null;
    }

    return (
      <div className="banner-sticky">
        <div className="viewer-title">
          {this.getString('banner_sticky_viewer')}
        </div>
        <a className="close" onClick={this.handleCloseAppBanner}>
          <i className="lz lz-close-bold" />
        </a>
        <Button type="primary" fw onClick={this.openAppStore}>
          <img src="/dist/images/ic-loship-white.png" alt="Logo Loship" />
          <span>{this.getString('continue_on_loship')}</span>
          <i className="fas fa-chevron-right pull-right mr-8" />
        </Button>
      </div>
    );
  }

  checkDevicePlatform = () => {
    if (!!window && !!navigator) {
      let platform;
      if (navigator.userAgent.match(/iPod|iPhone|iPad/) && !navigator.userAgent.match(/IEMobile/)) {
        platform = 'ios';
      }
      if (navigator.userAgent.match(/Android/)) {
        platform = 'android';
      }

      if (platform) {
        this.setState({ platform });
      }
    }
  };

  checkBannerMode = () => {
    const { bannerMode } = this.state;
    const isAlreadyCloseAppBanner = bannerMode === 'CLOSE' || cookie.get(COOKIE_04);
    const isViewer = bannerMode === 'VIEWER' || !!cookie.get('order-viewer');

    const mode = iife(() => {
      if (this.context.partnerName !== 'none') return 'CLOSE';
      if (isViewer) return 'VIEWER';
      if (isAlreadyCloseAppBanner) return 'CLOSE';
      return 'NORMAL';
    });

    if (mode === this.state.bannerMode) return;
    this.setState({ bannerMode: mode });
    if (mode === 'VIEWER') {
      // remove immediately because next reload should not show viewer sticky banner)
      setTimeout(() => cookie.expire('order-viewer'), 2000);
    }
  };

  handleCloseAppBanner = () => {
    this.setState({ bannerMode: 'CLOSE' });
  };

  openAppStore = () => {
    const { platform } = this.state;
    const { metadata = {} } = this.props;

    if (this.state.bannerMode === 'VIEWER') {
      const { pathname, search } = this.props.location;
      sendGAEvent('loship_action', 'ACTION_DOWNLOAD_APP', LINK_DOMAIN + `${pathname}${search}`);
    }

    if (platform === 'ios' && metadata.iosUrl) {
      window.location = metadata.iosUrl;
    } else if (platform === 'android' && metadata.androidUrl) {
      window.location = metadata.androidUrl;
    }

    this.timout = setTimeout(() => {
      if (!document.hidden) {
        window.location = 'https://loship.vn/use-app';
      }
    }, 2000);
  };

  cancelRedirect = () => {
    if (this.timout) clearTimeout(this.timout);
  }
}

export default initComponent(StickyAppBanner);
