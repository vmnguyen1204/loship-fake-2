import BaseComponent from 'components/base';
import React from 'react';

if (process.env.BROWSER) {
  require('assets/styles/components/banner.scss');
}

class AppBanner extends BaseComponent {
  state = { platform: null };

  componentDidMount() {
    this.checkDevicePlatform();
  }

  render() {
    const { isMobile } = this.props;
    const { platform } = this.state;

    return (
      <div className="app-banner">
        <div className="app-banner-wrapper">
          <div className="hand-phone" />
          <div className="promo-text">
            <h2>{this.getString('order_faster')}</h2>
            <p>
              {this.getString('with_app')}{' '}
              <span>
                <img src="/dist/images/loship-txt.png" alt="loship" />
              </span>{' '}
              {isMobile ? this.getString('on_mobile') : this.getString('on_android_and_ios')}
            </p>
          </div>

          <div className="promo-btns">
            {(!platform || platform === 'ios') && (
              <a href="https://goo.gl/CDFMhZ" className="app-img app-ios" target="_blank" rel="noopener">
                <img src="/dist/images/app-ios.png" alt="ios" />
              </a>
            )}
            {(!platform || platform === 'android') && (
              <a href="https://goo.gl/N5Vs4V" className="app-img app-android" target="_blank" rel="noopener">
                <img src="/dist/images/app-android.png" alt="android" />
              </a>
            )}
          </div>
        </div>
      </div>
    );
  }

  checkDevicePlatform = () => {
    if (!!window && !!navigator) {
      let platform;
      if (navigator.userAgent.match(/iPod|iPhone|iPad/) && !navigator.userAgent.match(/IEMobile/)) {
        platform = 'ios';
      }
      if (navigator.userAgent.match(/Android/)) {
        platform = 'android';
      }

      if (platform) {
        this.setState({ platform });
      }
    }
  };
}

export default AppBanner;
