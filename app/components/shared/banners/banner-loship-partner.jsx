import BaseComponent from 'components/base';
import React from 'react';

if (process.env.BROWSER) {
  require('assets/styles/components/banner.scss');
}

class LoshipPartnerBanner extends BaseComponent {
  render() {
    return (
      <div className="merchant-manager-banner">
        <img className="promo-image" src="/dist/images/banners/do-an-ship.jpg" alt="Partner banner" />
        <div className="promo-text">
          <h1>{this.getString('digitalize_your_shop_and_let_customers_buy_from_far_away_why_not')}</h1>
          <p>
            {this.getString(
              'take_loads_of_orders_without_worrying_how_to_deliver_them_to_the_customers_for_loship_is_here',
            )}
          </p>
          <a className="btn cta-button" href="https://doitacloship.lozi.vn/" target="_blank" rel="noopener">
            {this.getString('become_a_loship_partner')}
          </a>
        </div>
      </div>
    );
  }
}

export default LoshipPartnerBanner;
