import { GENERIC_POPUP, open } from 'actions/popup';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import InputPhoneNumber from 'components/shared/input/phone-number';
import React from 'react';
import initComponent from 'lib/initComponent';
import { isValidPhoneNumber } from 'utils/format';
import { sendGAEvent } from 'actions/factory';
import { sendUseAppInvitation } from 'actions/home';

if (process.env.BROWSER) {
  require('assets/styles/components/banner.scss');
}

class AppBannerFooter extends BaseComponent {
  state = {
    platform: null,
    countryCode: '84',
  };

  componentDidMount() {
    this.checkDevicePlatform();
  }

  render() {
    const { isMobile } = this.props;
    const { platform, countryCode } = this.state;

    return (
      <div className="banner-footer">
        <div className="app-banner-wrapper">
          {!isMobile && (
            <div className="download-app-sms">
              <h3>{this.getString('please_type_your_phone_3')}</h3>
              <div className="app-input">
                <InputPhoneNumber
                  className="mr-8"
                  btnClassName="bg-transparent"
                  defaultCountryCode={countryCode}
                  onChange={this.handlePhoneNumberInput}
                  onChangeCountryCode={this.handleChangeCountryCode}
                  mode="flag|caret"
                  placeholder={this.getString('type_phone_number_here')}
                />

                <Button
                  type="primary"
                  className="btn-send"
                  onClick={this.handleSendUseAppInvitation}
                  disabled={this.state.isSendingInvitation}
                >
                  {this.getString('send')}
                </Button>
              </div>

              {this.state.error && <div className="text-error">{this.state.error}</div>}

              <div className="app-privacy">
                {this.getString('send_sms_invitation_privacy', '', [
                  <a key="term_of_use" href="https://loship.vn/quy-che/" target="_blank" rel="noopener">
                    {this.getString('term_of_use')}
                  </a>,
                ])}
              </div>
            </div>
          )}
          <div className="download-app">
            <h3>{this.getString('download_loship_now')}</h3>
            <div className="app-list">
              {(!platform || platform === 'ios') && (
                <a href="https://goo.gl/CDFMhZ" className="app-img app-ios" target="_blank" rel="noopener">
                  <img src="/dist/images/app-ios.png" alt="ios" />
                </a>
              )}
              {(!platform || platform === 'android') && (
                <a href="https://goo.gl/N5Vs4V" className="app-img app-android" target="_blank" rel="noopener">
                  <img src="/dist/images/app-android.png" alt="android" />
                </a>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }

  checkDevicePlatform = () => {
    if (!!window && !!navigator) {
      let platform;
      if (navigator.userAgent.match(/iPod|iPhone|iPad/) && !navigator.userAgent.match(/IEMobile/)) {
        platform = 'ios';
      }
      if (navigator.userAgent.match(/Android/)) {
        platform = 'android';
      }

      if (platform) {
        this.setState({ platform });
      }
    }
  };

  handleChangeCountryCode = (countryCode) => {
    this.setState({ countryCode, error: null });
  };

  handlePhoneNumberInput = (phoneNumber) => {
    this.setState({ phoneNumber, error: null });
  };

  validate = (fields) => {
    if (fields.includes('phoneNumber')) {
      if (!this.state.phoneNumber) return this.setState({ error: this.getString('phone_number_is_required') });
      if (!isValidPhoneNumber(this.state.phoneNumber)) return this.setState({ error: this.getString('phone_number_is_invalid') });
    }

    if (fields.includes('countryCode')) {
      if (!this.state.countryCode) return this.setState({ error: this.getString('country_code_is_required') });
    }

    return true;
  };

  handleSendUseAppInvitation = () => {
    if (!this.validate(['phoneNumber', 'countryCode'])) return;

    const { countryCode, phoneNumber } = this.state;
    sendGAEvent('loship_action', 'ACTION_SEND_DOWNLOADSMS');

    this.setState({ isSendingInvitation: true }, () => {
      this.props.dispatch(
        sendUseAppInvitation({
          countryCode,
          phoneNumber,
          callback: () => {
            this.setState({ isSendingInvitation: false });
            this.props.dispatch(
              open(GENERIC_POPUP, {
                className: 'mw-400',
                content: (
                  <div>
                    <i className="lz lz-check-o text-green" style={{ fontSize: 60 }} />
                    <h3 className="mt-16">{this.getString('send_sms_invitation_success')}</h3>
                  </div>
                ),
                autoCloseTimeout: 3000,
              }),
            );
          },
          callbackFailure: () => this.setState({ isSendingInvitation: false }),
        }),
      );
    });
  };
}

export default initComponent(AppBannerFooter);
