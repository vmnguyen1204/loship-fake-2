import cx from 'classnames';
import DishItem from 'components/pages/merchant/_menu/_dish-item';
import Button from 'components/shared/button';
import ExpandContainer from 'components/shared/expand-container';
import ImageLazy from 'components/statics/img-lazy';
import Spinner from 'components/statics/spinner';
import React from 'react';
import { toast } from 'react-toastify';
import { LINK_DOMAIN } from 'settings/variables';
import { copyToClipboard } from 'utils/content';
import { getDistanceBirdFlies } from 'utils/distance';
import { addCurrency } from 'utils/format';
import { iife } from 'utils/helper';
import { getString } from 'utils/i18n';
import { getImageUrl } from 'utils/image';
import moment from 'utils/shims/moment';
import { getNextOperatingStatus } from 'utils/time';
import LoshipPartnerBadge from '../loship-partner-badge';

export function BirdFliesDistance({ className, merchant, globalAddress, iconFw }) {
  if (!merchant || !globalAddress) return null;

  const [mLat, mLng] = [merchant.lat, merchant.lng || merchant.long];
  const [lat, lng] = [globalAddress.lat, globalAddress.lng];
  if (!mLat || !mLng || !lat || !lng) return null;

  const distance = getDistanceBirdFlies(lat, lng, mLat, mLng);
  if (distance === 0) return null;

  return (
    <span className={className}>
      <i className={cx('lz lz-marker-o', iconFw && 'lz-fw')} />
      {distance}km
    </span>
  );
}

export function GmapDistance({ className, merchant, iconFw }) {
  if (!merchant || !merchant.distance) return null;
  const distance = iife(() => {
    if (merchant.distance > 0 && merchant.distance < 100) return 0.1;
    return Math.round(merchant.distance / 100) / 10;
  });
  if (distance === 0) return null;

  return (
    <span className={className} >
      <i className={cx('lz lz-marker-o', iconFw && 'lz-fw')} />
      {distance}km
    </span>
  );
}

export function EateryItemLanding({ merchant, onClick, isLoading, scrollableAncestor, partnerName }) {
  const imgProps = { shape: 'square', mode: 'static', size: 200, type: 'f', scrollableAncestor };
  if (!merchant) {
    return (
      <div className="list-item eatery-item-landing shimmer-animation">
        <ImageLazy
          alt="Avatar"
          className="figure"
          placeholder={getImageUrl('/dist/images/dish-placeholder.png')}
          {...imgProps}
        />
        <div className="content">
          <div className="metadata shimmer-text" />
          <div className="shimmer-text w-40" />
        </div>
      </div>
    );
  }

  const promotion = (() => {
    if (merchant.groupPromotion) return merchant.groupPromotion;
    if (merchant.promotionCampaign && merchant.promotionCampaign.type === 'fixed_price') return merchant.promotionCampaign;
    if (merchant.promotions && merchant.promotions[0]) return merchant.promotions[0];

    if (merchant.freeShippingMilestone === undefined) return partnerName !== 'none' ? undefined : {};
    const freeship = iife(() => {
      if (merchant.freeShippingMilestone <= 0) return 0;
      return Math.max(Math.floor(merchant.freeShippingMilestone / 100) / 10, 0.1);
    });
    if (freeship > 0) return { freeship };
    return null;
  })();
  const merchantUrl = merchant.username ? `/${merchant.username}` : `/b/${merchant.slug}`;

  return (
    <a href={merchantUrl} className="list-item eatery-item-landing" onClick={onClick}>
      <ImageLazy
        src={merchant.avatar}
        alt="Avatar"
        className="figure"
        placeholder="/dist/images/dish-placeholder.png"
        {...imgProps}
      />
      <div className="content">
        <div className="metadata merchant-name">
          <b>{merchant.name}</b>
        </div>
        <div className="merchant-tag">
          {merchant.isSponsored && (
            <span className="sponsored">{getString('sponsored')}</span>
          )}
          <GmapDistance className="distance" merchant={merchant} />
        </div>

        {iife(() => {
          if (promotion) {
            return (
              <span className="promotion">
                <i className="lz lz-discount" />
                {(() => {
                  if (promotion.minimumUserToApply >= 2) {
                    return getString('promo_off_group_nano', '', [
                      promotion.minimumUserToApply,
                      promotion.promotionType === 'percent' ? `${promotion.value}%` : addCurrency(promotion.value, 'đ'),
                    ]);
                  }
                  if (promotion.type === 'fixed_price') return getString('promo_fixed_price', '', [addCurrency(promotion.value)]);
                  if (promotion.value) {
                    return getString('promo_off_nano', '', [
                      promotion.promotionType === 'percent' ? `${promotion.value}%` : addCurrency(promotion.value, 'đ'),
                    ]);
                  }
                  if (promotion.freeship) return `${getString('free_shipping')} ${promotion.freeship}km`;
                  return getString('free_shipping');
                })()}
              </span>
            );
          }
        })}
        {merchant.isPurchasedSupplyItems && (
          <div className="merchant-thoughtful">
            <i className="lz lz-star-s" />
          </div>
        )}
      </div>
      {isLoading && <Spinner />}
    </a>
  );
}

export function EateryItem({ isMobile, merchant, onClick, isLoading, scrollableAncestor, handleExtraActions, partnerName }) {
  const imgProps = isMobile
    ? { shape: 'round-square', size: 200, type: 'f', scrollableAncestor }
    : { shape: 'square', size: 200, type: 'f', scrollableAncestor };

  if (!merchant) {
    return (
      <div className="list-item eatery-item shimmer-animation">
        <ImageLazy
          alt="Avatar"
          className="figure"
          placeholder={getImageUrl('/dist/images/dish-placeholder.png')}
          {...imgProps}
        />
        <div className="content">
          <div className="metadata shimmer-text" />
          <div className="shimmer-text w-40" />
        </div>
      </div>
    );
  }

  const promotion = (() => {
    if (merchant.groupPromotion) return merchant.groupPromotion;
    if (merchant.promotionCampaign && merchant.promotionCampaign.type === 'fixed_price') return merchant.promotionCampaign;
    if (merchant.promotions && merchant.promotions[0]) return merchant.promotions[0];

    if (merchant.freeShippingMilestone === undefined) return partnerName !== 'none' ? undefined : {};
    const freeship = iife(() => {
      if (merchant.freeShippingMilestone <= 0) return 0;
      return Math.max(Math.floor(merchant.freeShippingMilestone / 100) / 10, 0.1);
    });
    if (freeship > 0) return { freeship };
    return null;
  })();
  const merchantUrl = merchant.username ? `/${merchant.username}` : `/b/${merchant.slug}`;
  const recommendedRatio = ~~(merchant.recommendedRatio * 1000) / 10;
  const { isOpening, openingStatus } = getNextOperatingStatus(
    getString, merchant.operatingStatus,
  );
  const hasAction = typeof handleExtraActions === 'function';

  const OperatingStatus = iife(() => {
    if (merchant.closed) {
      return <div className="text-red">{getString('permanently_closed')}</div>;
    }
    if (!merchant.isActive) {
      return <div className="text-dark-gray">{getString('temporary_closed')}</div>;
    }
    if (!merchant.isCheckedIn) {
      return <div className="text-dark-gray">{getString('temporary_not_orders')}</div>;
    }
    if (!isOpening) {
      return <div className="text-dark-gray">{openingStatus}</div>;
    }
    return null;
  });

  return (
    <a href={merchantUrl} className="list-item eatery-item" onClick={onClick}>
      <ImageLazy
        src={merchant.avatar}
        alt="Avatar"
        className="figure"
        placeholder="/dist/images/dish-placeholder.png"
        {...imgProps}
      />
      <div className="content">
        <div className={cx('metadata merchant-name', hasAction && 'action')}>
          <b>{merchant.name}</b>
          {hasAction && (
            <Button type="link" className="btn-action text-black" onClick={handleExtraActions}>
              <i className="fas fa-ellipsis-v" />
            </Button>
          )}
        </div>
        {iife(() => {
          return (
            <span className="merchant-tag">
              {merchant.isSponsored && (
                <span className="sponsored">{getString('sponsored')}</span>
              )}
              <GmapDistance key="distance" className="distance" merchant={merchant} iconFw={isMobile} />
              {merchant.recommendedEnable && recommendedRatio > 0 && (
                <span key="recommended" className="distance">
                  <i className={cx('lz lz-like', isMobile && 'lz-fw')} /> {recommendedRatio}%
                </span>
              )}
            </span>
          );
        })}
        {isMobile && merchant.isLoshipPartner && (
          <div className="metadata">
            <LoshipPartnerBadge getString={getString} iconFw />
          </div>
        )}
        {merchant.isPurchasedSupplyItems && (
          isMobile ? (
            <div className="metadata">
              <div className="hint-icon loship-partner-icon">
                <i className="lz lz-star-s" />
                <div>{getString('merchant_thoughtful')}</div>
              </div>
            </div>
          ) : (
            <div className="merchant-thoughtful">
              <i className="lz lz-star-s" />
            </div>
          )
        )}
        {OperatingStatus}

        {iife(() => {
          if (OperatingStatus) return null;
          if (promotion) {
            return (
              <div className="d-flex">
                <span className="promotion">
                  <i className={cx('lz lz-discount', isMobile && 'lz-fw')} />
                  {(() => {
                    if (promotion.minimumUserToApply >= 2) {
                      return getString('promo_off_group_nano', '', [
                        promotion.minimumUserToApply,
                        promotion.promotionType === 'percent' ? `${promotion.value}%` : addCurrency(promotion.value, 'đ'),
                      ]);
                    }
                    if (promotion.type === 'fixed_price') return getString('promo_fixed_price', '', [addCurrency(promotion.value)]);
                    if (promotion.value) {
                      return getString('promo_off_nano', '', [
                        promotion.promotionType === 'percent' ? `${promotion.value}%` : addCurrency(promotion.value, 'đ'),
                      ]);
                    }
                    if (promotion.freeship) return `${getString('free_shipping')} ${promotion.freeship}km`;
                    return getString('free_shipping');
                  })()}
                </span>
              </div>
            );
          }
        })}
      </div>
      {isLoading && <Spinner />}
    </a>
  );
}

export function EateryChainItem({ isMobile, merchant, onClick, className, scrollableAncestor, mode = '' }) {
  if (!merchant) {
    return <EateryItem isMobile={isMobile} />;
  }

  const imgProps = isMobile
    ? { shape: 'round-square', size: 200, type: 'f', scrollableAncestor }
    : { shape: 'square', size: 200, type: 'f', scrollableAncestor };
  const eateryChain = merchant.eateryChain;
  const eateryChainUrl = eateryChain.username ? `/c/${eateryChain.username}` : `/cid/${eateryChain.id}`;

  return (
    <a href={eateryChainUrl} className={cx('list-item eatery-item', className)} onClick={onClick}>
      <ImageLazy
        src={eateryChain.avatar}
        alt="Avatar"
        className="figure"
        placeholder="/dist/images/dish-placeholder.png"
        {...imgProps}
      />
      <div className="content">
        <div className="metadata">
          <b>{eateryChain.name}</b>
        </div>
        {!mode.includes('heading') && (
          <div className="metadata text-dark-gray">{getString('eatery_chain')}</div>
        )}

        {isMobile && (
          <div className="metadata eatery-chain">
            <i className="lz lz-fw lz-store" />
            {getString('0_merchants', 'search', [eateryChain.count.eatery])}
          </div>
        )}
        {mode.includes('heading') && (
          <div className="metadata merchant-info">
            <span className="main">
              <i className="lz lz-link-x" />
              {`loship.vn${eateryChainUrl}`}
            </span>
            <Button
              className="btn-sm"
              onClick={() => {
                copyToClipboard(LINK_DOMAIN + eateryChainUrl);
                toast(getString('copied'));
              }}
            >
              <span>{getString('copy')}</span>
            </Button>
          </div>
        )}
      </div>
    </a>
  );
}

export function EateryDishItem({ merchant, onClick, isLoading }) {
  const recommendedRatio = ~~(merchant.recommendedRatio * 1000) / 10;
  const { isOpening, openingStatus } = getNextOperatingStatus(
    getString, merchant.operatingStatus,
  );

  return (
    <div className="list-item eatery-dish-item">
      <div className="content">
        <div className="metadata">
          <b className="name">{merchant.name}</b>
          <Button type="link" className="btn-switch-to-merchant text-black" onClick={onClick}>
            <i className="lz lz-arrow-head-right" />
          </Button>
        </div>
        {iife(() => {
          if (merchant.closed) {
            return <div className="text-red">{getString('permanently_closed')}</div>;
          }
          if (!merchant.isActive) {
            return <div className="text-dark-gray">{getString('temporary_closed')}</div>;
          }
          if (!merchant.isCheckedIn) {
            return <div className="text-dark-gray">{getString('temporary_not_orders')}</div>;
          }
          if (!isOpening) {
            return <div className="text-dark-gray">{openingStatus}</div>;
          }
          return (
            <div className="info">
              <GmapDistance className="distance" merchant={merchant} />
              {merchant.recommendedEnable > 0 && (
                <span className="distance">
                  <i className={'lz lz-like'} /> {recommendedRatio}%
                </span>
              )}
              {merchant.isLoshipPartner && (
                <LoshipPartnerBadge getString={getString} />
              )}
            </div>
          );
        })}
      </div>
      <div>
        {merchant.dishes.map((dish) => (
          <DishItem
            key={dish.id}
            data={dish}
            groupInfo={{
              isExtraGroupDish: dish.groupDish && dish.groupDish.isExtraGroupDish,
              groupDishId: dish.groupDish && dish.groupDish.id,
            }}
            merchant={merchant}
          />
        ))}
      </div>
      {isLoading && <Spinner />}
    </div>
  );
}

export function EateryChainEateryItem({ merchant, onClick, isLoading }) {
  if (!merchant) {
    return (
      <div className="list-item eatery-chain-eatery-item shimmer-animation">
        <div className="content">
          <div className="metadata shimmer-text" />
          <div className="shimmer-text w-40" />
        </div>
      </div>
    );
  }

  const recommendedRatio = ~~(merchant.recommendedRatio * 1000) / 10;
  const { isOpening, openingStatus } = getNextOperatingStatus(
    getString, merchant.operatingStatus,
  );
  const merchantUrl = merchant.username ? `/${merchant.username}` : `/b/${merchant.slug}`;

  return (
    <a href={merchantUrl} className="list-item eatery-chain-eatery-item" onClick={onClick}>
      <div className="content">
        <div className="metadata">
          <b className="name">{merchant.name}</b>
        </div>
        {iife(() => {
          if (merchant.closed) {
            return <div className="text-red">{getString('permanently_closed')}</div>;
          }
          if (!merchant.isActive) {
            return <div className="text-dark-gray">{getString('temporary_closed')}</div>;
          }
          if (!merchant.isCheckedIn) {
            return <div className="text-dark-gray">{getString('temporary_not_orders')}</div>;
          }
          if (!isOpening) {
            return <div className="text-dark-gray">{openingStatus}</div>;
          }
          return (
            <div className="info">
              <GmapDistance className="distance" merchant={merchant} />
              {merchant.recommendedEnable > 0 && (
                <span className="distance">
                  <i className={'lz lz-like'} /> {recommendedRatio}%
                </span>
              )}
              {merchant.isLoshipPartner && (
                <LoshipPartnerBadge getString={getString} />
              )}
            </div>
          );
        })}
      </div>
      {isLoading && <Spinner />}
    </a>
  );
}

export function EateryRatingItem({
  merchant: rating, onClick, onViewDetail, scrollableAncestor,
  handleLike, handleSharing, handleOpenPhotoPopup,
}) {
  const imgProps = { shape: 'circle', size: 40, scrollableAncestor };
  const ratingPhotoSize = 120;

  if (!rating) {
    return (
      <div className="list-item eatery-rating-item shimmer-animation">
        <div className="content">
          <ImageLazy {...imgProps} />
          <div className="metadata">
            <div className="name shimmer-text" />
            <div className="eatery-name shimmer-text w-40" />
          </div>
        </div>
        <div className="content rating-item">
          <div className="shimmer-text w-40" />
        </div>
        <div className="action">
          <div className="shimmer-text w-20" />
          <div className="shimmer-text w-40" />
        </div>
      </div>
    );
  }

  const {
    createdAt, createdBy = {}, eatery = {},
    photos = [], count = {}, liked,
  } = rating;

  const timeStr = iife(() => {
    const now = moment();
    const createdAtPlus7Day = moment(createdAt).add(7, 'days');
    const createdAtMoment = moment(createdAt);

    if (createdAtPlus7Day.isAfter(now)) return createdAtMoment.fromNowOrNow();
    if (createdAtMoment.format('YYYY') === now.format('YYYY')) return createdAtMoment.format('DD MMMM, HH:mm');
    return createdAtMoment.format('DD MMMM YYYY, HH:mm');
  });

  return (
    <div className="list-item eatery-rating-item">
      <div className="content" onClick={onViewDetail}>
        <ImageLazy src={createdBy.avatar} {...imgProps} />
        <div className="metadata">
          <span className="name">
            <b>{createdBy.name && createdBy.name.short || getString('anonymous')}</b>
          </span>
          {` ${getString('comment')} `}
          <span className="eatery-name" onClick={onClick}>
            <b>{eatery.name}</b>
          </span>
          <div className="text-dark-gray">{timeStr}</div>
        </div>
      </div>
      <div className="content rating-item">
        {rating.content && (
          <ExpandContainer
            key="rating-content"
            className="rating-content"
            content={rating.content}
            line={3}
            mode="ARROW"
            arrow={(isExpanded) => !isExpanded && (
              <a className="view-more">
                {getString('view_more')}
                <i className={cx('fas fa-caret-down')} />
              </a>
            )}
          />
        )}

        {photos.length > 0 && (
          <div className="rating-photos-list-wrapper">
            <div className="rating-photos-list" style={{ width: photos.length * ratingPhotoSize + (photos.length - 1) * 15 }}>
              {photos.map((photo) => (
                <ImageLazy
                  key={photo}
                  src={photo}
                  shape="round-square"
                  size={ratingPhotoSize * 2}
                  onClick={() => handleOpenPhotoPopup(photo)}
                />
              ))}
            </div>
          </div>
        )}
      </div>
      <div className="action">
        <Button
          type="link"
          className={cx('btn-like text-dark-gray', liked && 'active')}
          onClick={handleLike}>
          <ImageLazy key={liked} src={`/dist/images/${liked ? 'icon-smile.png' : 'icon-smile-o.png'}`} />
          <span>{count.like} {getString('useful')}</span>
        </Button>
        <Button
          type="link"
          className={cx('text-dark-gray')}
          onClick={handleSharing}>
          <i className="lz lz-share" />
          <span>{getString('share')}</span>
        </Button>
      </div>
    </div>
  );
}
