import { sendGAEvent } from 'actions/factory';
import { fetchMerchant } from 'actions/merchant';
import { fetchNewsFeed } from 'actions/newsfeed';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Pagination from 'components/pages/search/_pagination';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { Waypoint } from 'react-waypoint';
import { getNewsFeed } from 'reducers/newsfeed';
import { debouncer, iife } from 'utils/helper';
import { EateryChainItem, EateryDishItem } from './merchant-item';

if (process.env.BROWSER) {
  require('assets/styles/components/_newsfeed.scss');
}

class Section extends BaseComponent {
  static defaultProps = {
    dataGetter: getNewsFeed,
    customRender: true,
    mode: '', // preload|scroll-nice|search-dish|search-page|horizontal|see_all|pagination|load_more
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  state = {
    loading: undefined,
    page: undefined,
  };

  componentDidMount() {
    this.props.mode.includes('preload') && this.load();
  }

  componentDidUpdate(prevProps) {
    const params = this.getParams();
    const prevParams = this.getParams(prevProps);

    if (JSON.stringify(params) !== JSON.stringify(prevParams)) {
      if (this.props.mode.includes('preload')) this.load(true);
      else this.setState({ shouldHardReload: true });
    }
  }

  renderTitle = () => {
    const { title, action, mode } = this.props;
    const showSeeAll = mode.includes('horizontal') && mode.includes('see_all');

    if (!title) return null;

    return (
      <div className={cx('title', action && 'with-action')}>
        <h2>{title}</h2>
        {action}
        {showSeeAll && (
          <a onClick={this.seeAll}>{this.getString('see_all')}</a>
        )}
      </div>
    );
  };

  render() {
    const { isMobile } = this.context;
    const { data, className, params, extras, status, mode, MerchantItem, renderNotFound, renderLoading } = this.props;
    const { shouldHardReload } = this.state;
    const loadMore = extras && extras.get('loadMore');
    const scrollableAncestor = this.props.scrollableAncestor !== undefined ? this.props.scrollableAncestor : 'window';

    const listViewStyle = iife(() => {
      const baseSize = (isMobile ? 132 : 170) + 15;
      if (!mode.includes('horizontal')) return {};
      if (!data || !data.size) return { width: baseSize * 6 };
      return { width: baseSize * data.size + (mode.includes('see_all') ? baseSize : 0) };
    });

    if (!data || !data.size) {
      if (mode.includes('search') && !params.q) return null;
      if (!['PENDING', 'INIT', undefined].includes(status)) {
        if (typeof renderNotFound === 'function') return renderNotFound();
        if (shouldHardReload) {
          return (
            <Waypoint onEnter={() => this.load(true)} onLeave={this.cancelLoad} scrollableAncestor={scrollableAncestor} />
          );
        }
        return null;
      }

      if (typeof renderLoading === 'function') return renderLoading();
      return (
        <div className={cx('section-newsfeed', className, {
          horizontal: mode.includes('horizontal'),
          dish: mode.includes('search-dish'),
          'eatery-ratings': mode.includes('eatery-ratings'),
        })}>
          {!mode.includes('preload') && (
            <Waypoint onEnter={() => this.load()} onLeave={this.cancelLoad} scrollableAncestor={scrollableAncestor} />
          )}
          {this.renderTitle()}
          <div className={cx('content', mode.includes('scroll-nice') && 'scroll-nice')}>
            <div className="list-view" style={listViewStyle}>
              {[...Array(6).keys()].map((key) => (
                <MerchantItem key={key} isMobile={isMobile} />
              ))}
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className={cx('section-newsfeed', className, {
        horizontal: mode.includes('horizontal'),
        dish: mode.includes('search-dish'),
        'eatery-ratings': mode.includes('eatery-ratings'),
      })}>
        {shouldHardReload && (
          <Waypoint onEnter={() => this.load(true)} onLeave={this.cancelLoad} scrollableAncestor={scrollableAncestor} />
        )}
        {this.renderTitle()}
        <div className={cx('content', mode.includes('scroll-nice') && 'scroll-nice')}>
          <div className="list-view" style={listViewStyle}>
            {data.toArray().flatMap((merchant) => {
              if (mode.includes('search-dish') && Array.isArray(merchant.dishes) && merchant.dishes.length > 0) {
                return (
                  <EateryDishItem
                    key={merchant.id}
                    isMobile={isMobile}
                    merchant={merchant}
                    onClick={this.openMerchant(merchant)}
                    isLoading={this.state.loading === `merchant.${merchant.id}`}
                    scrollableAncestor={scrollableAncestor}
                  />
                );
              }

              if (merchant.eateryChain) {
                return [
                  <MerchantItem
                    key={merchant.id}
                    isMobile={isMobile}
                    merchant={merchant}
                    onClick={this.openMerchant(merchant)}
                    isLoading={this.state.loading === `merchant.${merchant.id}`}
                    scrollableAncestor={scrollableAncestor}
                    partnerName={this.context.partnerName}
                  />,
                  <EateryChainItem
                    key={`${merchant.id}.chain`}
                    isMobile={isMobile}
                    merchant={merchant}
                    onClick={this.openFranchised(merchant)}
                    scrollableAncestor={scrollableAncestor}
                  />,
                ];
              }

              return (
                <MerchantItem
                  key={merchant.id}
                  isMobile={isMobile}
                  merchant={merchant}
                  onClick={this.openMerchant(merchant)}
                  isLoading={this.state.loading === `merchant.${merchant.id}`}
                  scrollableAncestor={scrollableAncestor}
                  partnerName={this.context.partnerName}
                />
              );
            })}
            {mode.includes('see_all') && mode.includes('horizontal') && (
              <div className="list-item view-all" onClick={this.seeAll}>
                <div className="content">
                  {this.getString('see_all')} {this.props.title}
                  <i className="lz lz-light-arrow-right" />
                </div>
              </div>
            )}
          </div>
          {mode.includes('see_all') && !mode.includes('horizontal') && (
            <div className="btn-view-all" onClick={this.seeAll}>
              {this.getString('see_all')} <i className="lz lz-arrow-head-right" />
            </div>
          )}
          {mode.includes('pagination') && (
            <Pagination
              changePage={this.changePage}
              currentPage={extras.get('page')}
              itemPerPage={extras.get('limit')}
              totalItems={extras.get('total')}
              getString={this.getString}
            />
          )}
          {mode.includes('load_more') && loadMore && (
            <Waypoint onEnter={this.loadMore} onLeave={this.cancelLoadMore} scrollableAncestor={scrollableAncestor}>
              <div className="list-view" style={listViewStyle}>
                <MerchantItem isMobile={isMobile} />
              </div>
            </Waypoint>
          )}
        </div>
      </div>
    );
  }

  load = (force) => {
    const query = this.getParams();

    this.loadDebounce = debouncer(
      this.loadDebounce,
      () => {
        if (!this.props.data || force) {
          if (this.props.location.pathname === '/tim-kiem' && !query.q && !query.chainId) return;
          this.props.dispatch(
            fetchNewsFeed({
              query,
              force: force || query.force,
              callback: this.props.onLoaded,
            }),
          );
          this.setState({ shouldHardReload: undefined });
        }
      },
      0,
    );
  };

  cancelLoad = () => {
    if (this.loadDebounce) clearTimeout(this.loadDebounce);
  };

  loadMore = () => {
    if (this.loadMoreDebounce) clearTimeout(this.loadMoreDebounce);
    this.loadMoreDebounce = debouncer(
      this.loadMoreDebounce,
      () => {
        this.setState((state) => ({ page: (state.page || 1) + 1 }), () => {
          this.props.dispatch(
            fetchNewsFeed({
              query: { ...this.getParams(), page: this.state.page },
              loadMore: true,
            }),
          );
        });
      },
      0,
    );
  };

  cancelLoadMore = () => {
    if (this.loadMoreDebounce) clearTimeout(this.loadMoreDebounce);
  };


  openMerchant = (merchant) => (e) => {
    /** Handle right-click and ctrl+click
     ** Definitely need another handy way to achieve this :(
     */
    if (e.ctrlKey || e.metaKey) return;
    if (typeof e.preventDefault === 'function') {
      e.preventDefault();
      e.stopPropagation();
    }

    const callback = typeof e === 'function' && e;

    if (this.state.loading) return;
    if (!merchant) return;

    this.setState({ loading: `merchant.${merchant.id}` });
    this.props.dispatch(
      fetchMerchant({
        merchant: merchant.slug,
        callback: () => {
          const merchantUrl = merchant.username ? `/${merchant.username}` : `/b/${merchant.slug}`;
          this.props.history.push(merchantUrl);
          typeof callback === 'function' && callback();
          typeof this.props.onMerchantLoaded === 'function' && this.props.onMerchantLoaded();
        },
      }),
    );

    this.sendGATracking('merchant');
  };

  openFranchised = (merchant) => (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (this.state.loading) return;
    if (!merchant || !merchant.eateryChain) return;

    const eateryChainUrl = merchant.eateryChain.username
      ? `/c/${merchant.eateryChain.username}`
      : `/cid/${merchant.eateryChain.id}`;
    return this.props.history.push(eateryChainUrl);
  };

  seeAll = () => {
    const { submit, params } = this.props;
    submit(params);
    this.sendGATracking('see_all');
  };

  changePage = (page) => {
    const { submit } = this.props;
    submit({ page });
  }

  sendGATracking = (trackingLabel) => {
    if (!this.props.trackingAction) return;
    const trackingCategory = this.props.trackingCategory || 'loship_section';

    sendGAEvent(trackingCategory, this.props.trackingAction, this.props.trackingLabel || trackingLabel);
  };
}

export default initComponent(Section, withRouter);
