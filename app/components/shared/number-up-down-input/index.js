import BaseComponent from 'components/base';
import { MAX_ITEM_PER_DISH } from 'settings/variables';
import PropTypes from 'prop-types';
import React from 'react';
import initComponent from 'lib/initComponent';
import { warnDishSelect } from 'actions/alert';
import cx from 'classnames';

if (process.env.BROWSER) {
  require('assets/styles/components/number-input-up-down.scss');
}

class NumberUpDownInput extends BaseComponent {
  static propTypes = {
    max: PropTypes.number,
    min: PropTypes.number,
    onChange: PropTypes.func,
    value: PropTypes.number.isRequired,
  };

  static defaultProps = { min: 0 };

  reachLimit = () => {
    this.props.dispatch(
      warnDishSelect(this.getString('you_cannot_choose_more_than_0_items', 'alert', [MAX_ITEM_PER_DISH])),
    );
  };

  preOnChange = () => {
    if (!this.props.preOnChange) return true;
    return this.preOnChange.preOnChange();
  };

  onChange = (e, value) => {
    e.stopPropagation();

    if (!this.preOnChange()) return;
    if (!this.props.onChange) return;

    let newValue = parseInt(this.props.value) + value;
    if (this.props.max && newValue > this.props.max) {
      newValue = this.props.max;
    }
    if (this.props.min && newValue < this.props.min) {
      newValue = this.props.min;
    }

    if (newValue !== this.props.value) this.props.onChange(this.props.mode === 'set' ? newValue : value);
  };

  render() {
    return (
      <div className={cx('number-input-up-down', this.props.className)}>
        {this.props.showTrash ? (
          <a className={cx({ trash: this.props.value <= 1 })} onClick={(e) => this.onChange(e, -1)}>
            <i className={cx('lz', { 'lz-trash': this.props.value <= 1, 'lz-minus': this.props.value > 1 })} />
          </a>
        ) : (
          <a className={cx({ disabled: this.props.value <= this.props.min })} onClick={(e) => this.onChange(e, -1)}>
            <i className="lz lz-minus" />
          </a>
        )}
        <input
          type="number"
          value={this.props.value || this.props.min}
          readOnly
          onChange={(e) => {
            if (e.target !== undefined) {
              this.onChange(e, e.target.value);
            } else {
              this.onChange(e, this.props.min);
            }
          }}
        />
        <a onClick={(e) => this.onChange(e, 1)}>
          <i className="lz lz-plus" />
        </a>
      </div>
    );
  }
}

export default initComponent(NumberUpDownInput);
