import PropTypes from 'prop-types';
import React from 'react';
import Slider from 'react-slick';
import cx from 'classnames';
import { getImageUrl } from 'utils/image';

if (process.env.BROWSER) {
  require('assets/styles/components/_carousel.scss');
}

class Carousel extends React.Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    const { isMobile } = this.context;
    const { data, currentSlide, className, slideClassName } = this.props;

    const slidesToShow = this.props.slidesToShow || (isMobile ? 1 : 3);
    const PrevArrow = ({ currentSlide: _, slideCount: __, ...rest }) => (
      <button type="button" className="slick-arrow slick-prev" {...rest}>
        <i className="fas fa-arrow-left" />
      </button>
    );
    const NextArrow = ({ currentSlide: _, slideCount: __, ...rest }) => (
      <button type="button" className="slick-arrow slick-next" {...rest}>
        <i className="fas fa-arrow-right" />
      </button>
    );

    const settings = {
      centerMode: this.props.centerMode !== undefined ? this.props.centerMode : isMobile,
      draggable: false,
      dots: this.props.dot !== undefined ? this.props.dot : data.length > slidesToShow,
      dotsClass: 'slick-dots carousel-paging-bar',
      infinite: data.length > slidesToShow,
      centerPadding: '20px',
      slidesToShow,
      speed: 500,
      autoplay: this.props.autoplay !== undefined ? this.props.autoplay : data.length > slidesToShow,
      autoplaySpeed: 5000,
      variableWidth: data.length < slidesToShow,
      initialSlide: (currentSlide && data.findIndex((slide) => slide.key === currentSlide)) || 0,
      prevArrow: <PrevArrow />,
      nextArrow: <NextArrow />,
    };

    let imgWidth = isMobile ? window.innerWidth - 48 : this.props.imgWidth || Math.min(window.innerWidth, 1180) / 3;
    imgWidth = Math.floor(imgWidth);
    const imgContentHeight = this.props.ratio ? imgWidth / this.props.ratio : (imgWidth / 16) * 9;

    return (
      <Slider key={data.length} className={cx('container', 'carousel', className)} {...settings}>
        {data.map((item) => (
          <div key={item.key} className={slideClassName} onClick={item.onClick} style={{ width: imgWidth }}>
            <div
              style={{
                backgroundImage: `url(${getImageUrl(item.image, imgWidth, 'o')})`,
                height: imgContentHeight,
              }}
            >
              {!isMobile && this.props.photoZoom && (
                <a
                  className="carousel-slide-link"
                  href={getImageUrl(item.image.replace('resized', 'original'), imgWidth, 'o')}
                  target="_blank"
                  rel="noopener"
                />
              )}
            </div>
          </div>
        ))}
      </Slider>
    );
  }
}

export default Carousel;
