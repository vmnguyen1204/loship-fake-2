import cx from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

class InputAutoGrow extends React.Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    minRows: PropTypes.number,
    maxRows: PropTypes.number,
  }

  static defaultProps = {
    minRows: 1,
    maxRows: 5,
  }

  state = {
    rows: 1,
    value: '',
  }

  render() {
    return (
      <textarea
        ref={this.getRef}
        className={cx('input-auto-grow scroll-nice', this.props.className)}
        rows={this.state.rows}
        value={this.state.value}
        onChange={this.handleChange}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        placeholder={this.props.placeholder}
      />
    );
  }

  getRef = (ref) => {
    if (ref) this.ref = ref;
    if (!this.ref) return;

    if (this.props.initialValue) this.ref.value = this.props.initialValue;
    if (this.props.focus) this.ref.focus();

    if (this.state.rows < this.props.minRows) {
      this.setState({ rows: this.props.minRows });
    }
  }

  handleChange = (e) => {
    const textareaLineHeight = 24;
    const { minRows, maxRows } = this.props;

    const previousRows = e.target.rows;
    e.target.rows = minRows; // reset number of rows in textarea

    const currentRows = ~~(e.target.scrollHeight / textareaLineHeight);
    if (currentRows === previousRows) {
      e.target.rows = currentRows;
    }
    if (currentRows >= maxRows) {
      e.target.rows = maxRows;
      e.target.scrollTop = e.target.scrollHeight;
    }

    this.setState({
      value: e.target.value,
      rows: currentRows < maxRows ? currentRows : maxRows,
    });

    typeof this.props.onChange === 'function' && this.props.onChange(e.target.value);
  }

  handleFocus = () => {
    if (this.props.selectOnFocus) this.ref.select();
    typeof this.props.onFocus === 'function' && this.props.onFocus();
  }

  handleBlur = () => {
    typeof this.props.onBlur === 'function' && this.props.onBlur();
  }
}

export default InputAutoGrow;
