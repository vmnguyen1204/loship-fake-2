import { GENERIC_POPUP, open } from 'actions/popup';
import { normalize, phoneNumber } from 'utils/format';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import PropTypes from 'prop-types';
import { REGEX_STRING } from 'utils/data';
import React from 'react';
import cx from 'classnames';
import { debouncer } from 'utils/helper';
import initComponent from 'lib/initComponent';
import { getConfigs } from 'reducers/order';
import { Waypoint } from 'react-waypoint';
import Spinner from 'components/statics/spinner';

class ChooseCountryCode extends React.Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    handleClose: PropTypes.func,
  };

  state = {};

  render() {
    const data = this.getResults();
    const { page = 1 } = this.state;

    return (
      <div className="country-code-content">
        <div className="search-box">
          <input
            ref={this.setInput}
            onKeyUp={this.handleKeyUp}
            placeholder={this.props.getString('input_country_name_or_code')}
          />
          <i className="lz lz-search btn-search" />
        </div>

        <div className="list-view">
          {data.slice(0, page * 16).map((country) => (
            <div key={country.id} className="list-item" onClick={this.handleSelect(country.countryCode)}>
              <img src={country.image} alt="Country" />
              <span>{country.name}</span>
              <big>
                <b>+{country.countryCode}</b>
              </big>
            </div>
          ))}

          {page * 16 < data.length && (
            <Waypoint onEnter={() => this.setState({ page: page + 1 })} topOffset="-100px">
              <div className="list-item justify-center">
                <Spinner type="flow" />
              </div>
            </Waypoint>
          )}
          {!data.length && <div className="list-item">{this.props.getString('no_result')}</div>}
        </div>
      </div>
    );
  }

  setInput = (ref) => {
    if (!ref) return;

    this.input = ref;
    this.input.focus();
  };

  handleKeyUp = () => {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.forceUpdate();
    }, 500);
  };

  handleSelect = (countryCode) => () => {
    this.props.onChange(countryCode);
    this.props.handleClose && this.props.handleClose();
  };

  getResults = () => {
    const searchTerm = normalize(this.input && this.input.value);
    if (!searchTerm) return this.props.data;

    return this.props.data.filter((country) => {
      if (normalize(country.name.toLowerCase()).match(REGEX_STRING(searchTerm))) return true;
      if (country.countryCode.match(REGEX_STRING(searchTerm))) return true;
      return false;
    });
  };
}

class InputPhoneNumber extends BaseComponent {
  static defaultProps = {
    dataGetter: getConfigs,
    customRender: true,
    mode: 'flag',
  };

  static propTypes = {
    onChange: PropTypes.func.isRequired,
    onChangeCountryCode: PropTypes.func.isRequired,
  };

  state = {};

  renderCurrentCode = () => {
    const { data, mode } = this.props;
    if (!data) return;

    const { countryCode } = this.state;
    const currentCountry = data.get('countries').find((country) => country.countryCode === countryCode);
    if (!currentCountry) return null;

    return [
      mode.includes('flag') && <img key="img" src={currentCountry.image} alt="Country" />,
      `+${countryCode}`,
      mode.includes('caret') && <i key="caret" className="lz lz-triangle-down" />,
    ].filter((x) => !!x);
  };

  render() {
    const { data, readOnly, name, type, className, btnClassName, placeholder } = this.props;
    if (!data) return null;

    return (
      <div className={cx('input-phone-number', className)}>
        <Button className={cx('text-blue btn-border', btnClassName)} onClick={this.openCountryCodePopup}>
          {this.renderCurrentCode()}
        </Button>
        <input
          type={type}
          name={name}
          ref={this.setInput}
          readOnly={readOnly}
          onChange={this.handleChange}
          placeholder={placeholder}
        />
      </div>
    );
  }

  setInput = (ref) => {
    if (!ref) return;

    this.input = ref;
    if (this.props.defaultValue) this.input.value = phoneNumber(this.props.defaultValue, { autoFormat: false });
    if (this.props.focus) this.input.focus();
    if (this.props.defaultCountryCode) this.setState({ countryCode: this.props.defaultCountryCode });
    else {
      this.setState({ countryCode: '84' });
      this.handleChangeContryCode('84');
    }
  };

  handleChange = (e) => {
    const value = e.target.value.replace(/\D/g, '');
    this.input.value = value;

    // TODO: fix auto format cause issues on some device using Windows Telex keyboard
    // this.input.value = phoneNumber(value, { autoFormat: false });

    this.timeout = debouncer(
      this.timeout,
      () => {
        this.props.onChange(value);
      },
      250,
    );
  };

  handleChangeContryCode = (countryCode) => {
    this.setState({ countryCode });
    this.props.onChangeCountryCode(countryCode);
  };

  openCountryCodePopup = () => {
    const { data } = this.props;
    if (!data) return;

    const supportedCountries = data.get('countries') || [];
    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('choose_country_name_code'),
        className: 'input-country-code-popup',
        closeBtnClassName: 'lz-close',
        content: (cProps) => (
          <ChooseCountryCode
            {...cProps}
            data={supportedCountries}
            onChange={this.handleChangeContryCode}
            getString={this.getString}
          />
        ),
        animationStack: 1,
      }),
    );
  };
}

export default initComponent(InputPhoneNumber);
