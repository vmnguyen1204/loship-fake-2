import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { getCurrentUser } from 'reducers/user';
import { hasRole } from 'utils/access';

class AuthByRole extends BaseComponent {
  static defaultProps = { dataGetter: getCurrentUser, customRender: true };

  render() {
    const { children, data, roles } = this.props;

    if (!children || !data) return false;

    if (!hasRole(data, roles)) return false;

    return Array.isArray(children) ? <div>{children}</div> : children;
  }
}

export default initComponent(AuthByRole);
