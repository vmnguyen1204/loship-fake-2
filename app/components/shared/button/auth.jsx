import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { getCurrentUser } from 'reducers/user';
import { LOGIN, open } from 'actions/popup';
import Button from 'components/shared/button';

class AuthButton extends BaseComponent {
  static defaultProps = {
    dataGetter: getCurrentUser,
    customRender: true,
  };

  onFail = () => {
    this.props.dispatch(
      open(LOGIN, { callback: this.props.onLoginSuccess }),
    );
  };

  render() {
    const { onClick, onFail, data, ...rest } = this.props;
    const handleClick = !data ? onFail || this.onFail : onClick;

    // Remove unused properties
    delete rest.dataGetter;
    delete rest.customRender;
    delete rest.status;
    delete rest.dispatch;
    delete rest.onLoginSuccess;
    return <Button onClick={handleClick} {...rest} />;
  }
}

export default initComponent(AuthButton);
