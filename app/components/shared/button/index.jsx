import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Link from 'utils/shims/link';

class Button extends React.Component {
  static propTypes = {
    type: PropTypes.oneOf(['primary', 'feature', 'optional', 'link']),
  };

  static defaultProps = {
    onClick: () => {},
    type: 'optional',
  };

  render() {
    const { type, htmlType, to, href, disabled, className, fw, onClick, children, ...rest } = this.props;
    if (htmlType) rest.type = htmlType;

    let typeClassName = [];
    if (!disabled) {
      type === 'primary' && (typeClassName = ['bg-red', 'text-white']);
      type === 'feature' && (typeClassName = ['bg-blue', 'text-white']);
      type === 'optional' && (typeClassName = ['bg-light-gray', 'text-black']);
      type === 'link' && (typeClassName = ['bg-transparent', 'text-blue']);
    } else {
      typeClassName = ['bg-gray', 'text-white'];
      type === 'link' && (typeClassName = ['bg-transparent', 'text-gray']);
    }
    if (className && className.includes('text-')) typeClassName.splice(1, 1);
    if (className && className.includes('bg-')) typeClassName.splice(0, 1);

    const handleClick = disabled ? () => {} : onClick;
    const elemClassName = cx('btn', typeClassName, className, { 'btn-link': type === 'link', 'btn-fw': fw });

    if (to || href) {
      return (
        <Link className={elemClassName} to={to} href={href} {...rest}>
          {children}
        </Link>
      );
    }

    return (
      <span className={elemClassName} onClick={handleClick} {...rest}>
        {children}
      </span>
    );
  }
}

export default Button;
