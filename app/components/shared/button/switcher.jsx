import React from 'react';
import BaseComponent from 'components/base';
import cx from 'classnames';

class ButtonSwitch extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { className, toggle, status } = this.props;

    return (
      <div
        className={cx('btn-switch', className, { active: !!status, disactive: !status })}
        onClick={!!toggle && typeof toggle === 'function' && toggle}
      >
        <div className="bullet" />
      </div>
    );
  }
}

export default ButtonSwitch;
