import React, { Component } from 'react';
import { getString } from 'utils/i18n';
import qs from 'qs';
import PropTypes from 'prop-types';

// This component defines common methods of Component connecting to Store
// Ex: fetchMissingData, renderLoading,...

function setLoadingComponent(comp) {
  this.loadingComponent = comp || <span>{this.getString('loading')}</span>;
}

function injectRender() {
  const _render = this.render;
  this.render = () => {
    const { status, renderEmpty } = this.props;

    if (!(renderEmpty || status)) return false;

    if (status === 'ERROR') return <span />;

    if (status === 'PENDING') return this.loadingComponent;

    return _render && _render.call(this);
  };
}

function injectDidMount() {
  const _didMount = this.componentDidMount;
  this.componentDidMount = () => {
    this.fetchMissing();
    return _didMount && _didMount.call(this);
  };
}

function injectDidUpdate() {
  const _didUpdate = this.componentDidUpdate;
  this.componentDidUpdate = (prevProps, prevState) => {
    this.fetchMissing();
    return _didUpdate && _didUpdate.call(this, prevProps, prevState);
  };
}

export default class BaseComponent extends Component {
  static propTypes = {
    actionCreator: PropTypes.func,
    dataGetter: PropTypes.func,
  };

  constructor(props, context = {}) {
    super(props, { ...context, customRender: true });

    this.state = {};

    if (typeof props.dataGetter === 'function') {
      // Avoid injectRender with customRenderOption
      if (!context.customRender && !props.customRender) injectRender.call(this);
      setLoadingComponent.call(this, context.loadingComponent);
    }

    if (props.actionCreator) {
      if (typeof props.actionCreator === 'function') {
        injectDidMount.call(this);
        injectDidUpdate.call(this);
      } else console.error(this.constructor.name, 'actionCreator is required as a function');
    }
  }

  fetchMissing = () => {
    if (this.props.status) return;

    const { dispatch, actionCreator } = this.props;

    dispatch(actionCreator(this.getParams()));
  };

  getString = (...args) => {
    return getString(...args);
  };

  callAction = (fn, props) => {
    const { dispatch } = props || this.props;
    return dispatch && dispatch(fn);
  };

  getParams = (props) => {
    const { params, queryParams, match, location } = props || this.props;

    let resParams = { ...params };
    if (match && match.params) resParams = { ...resParams, ...match.params };
    if (Array.isArray(queryParams) && location && location.search) {
      const query = qs.parse(location.search, { ignoreQueryPrefix: true });
      queryParams.forEach((p) => {
        if (query[p]) resParams[p] = query[p];
      });
    }

    return resParams;
  };

  getQuery = (props) => {
    const { location } = props || this.props;
    const search = (location && location.search) || '';
    return qs.parse(search, { ignoreQueryPrefix: true });
  };
}
