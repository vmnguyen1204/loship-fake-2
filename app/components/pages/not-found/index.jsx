import BasePage from 'components/pages/base';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItemLanding } from 'components/shared/newsfeed/merchant-item';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import * as SEOTYPE from 'settings/seo-types';
import NotFoundPage from './not-found';

if (process.env.BROWSER) {
  require('assets/styles/pages/not-found.scss');
  require('assets/styles/pages/not-found-mobile.scss');
}

class NotFound extends BasePage {
  static contextTypes = {
    isMobile: PropTypes.bool,
  }

  constructor(props, context) {
    super(props, {
      ...context,
      seo: { type: SEOTYPE.NOT_FOUND },
    });
  }

  render() {
    const params = this.getParams();
    const isLziNotFoundPage = params.merchantUsername === 'items' || params.merchantUsername === 'i';

    return (
      <div className="home-page">
        <div className="container">
          <NotFoundPage isLziNotFoundPage={isLziNotFoundPage} />
          {!isLziNotFoundPage && (
            <SectionNewsfeed
              params={{
                newestApi: true,
                page: 1,
                limit: 12,
                globalAddress: this.props.globalAddress,
              }}
              title={this.getString('recently_joined_merchants')}
              action={(
                <a
                  key="action" href="https://doitacloship.lozi.vn/"
                  className="action"
                  target="_blank" rel="noopener" >
                  {this.getString('cooperate_with_loship')}
                </a>
              )}
              submit={this.submit}
              mode={this.context.isMobile ? 'preload|see_all|horizontal' : 'preload|see_all'}
              MerchantItem={EateryItemLanding}
            />
          )}
          <div className="clearfix" />
        </div>
      </div>
    );
  }

  submit = () => {
    this.props.history.push('/danh-sach-dia-diem-moi-tham-gia-loship-giao-tan-noi');
  };
}

export default initComponent(NotFound, withRouter);
