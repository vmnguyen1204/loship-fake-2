import BaseComponent from 'components/base';
import React from 'react';
import Link from 'utils/shims/link';

class NotFoundPage extends BaseComponent {
  render() {
    const isLziNotFoundPage = this.props.isLziNotFoundPage;

    return (
      <div className="page-not-found">
        <div className="section-not-found">
          <img className="not-found-img" src="/dist/images/not-found-404.png" alt="not-found" />
          <h2>{this.getString('error_404')}</h2>
          {isLziNotFoundPage ? (
            <div className="content">
              <p className="group-1">{this.getString('sorry_loship_cannot_find_the_item_that_you_request')}</p>
              <ul className="group-2">
                <li>{this.getString('choose_another_shop_there_are_more_than_30000_shops_await')}</li>
              </ul>
              <p className="group-1">
                <a href="/">
                  {this.getString('go_to_loshipvn_home_page')}
                  <i className="lz lz-arrow-head-right" />
                </a>
              </p>
            </div>
          ) : (
            <div className="content">
              <p className="group-1">{this.getString('sorry_loship_cannot_find_the_shop_that_you_request_try')}</p>
              <ul className="group-2">
                <li>{this.getString('checking_the_exact_url')}</li>
                <li>{this.getString('choose_another_shop_there_are_more_than_30000_shops_await')}</li>
              </ul>
              <p className="group-1">
                <Link to="/">
                  {this.getString('go_to_loshipvn_home_page')}
                  <i className="lz lz-arrow-head-right" />
                </Link>
              </p>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default NotFoundPage;
