import OrderBase, { STEP_ADDRESS, STEP_ADDRESS_FROM, STEP_CONFIRM } from 'components/shared/order';

import MobileNavbar from 'components/shared/mobile-nav';
import React from 'react';
import StepAddress from 'components/shared/order/address';
import StepConfirm from 'components/shared/order/confirm';
import { fetchLosendConfigs } from 'actions/order';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';

class SendPage extends OrderBase {
  constructor(props, context) {
    super(props, context, {
      step: STEP_ADDRESS_FROM,
      serviceName: 'losend',
    });

    this.stepLabels = [
      this.getString('sender_information'),
      this.getString('receiver_information'),
      this.getString('completed'),
    ];
  }

  componentDidMount() {
    super.componentDidMount();

    const query = this.getQuery();
    if (query && query.status === 'success' && query.code) return this.props.history.push(`/tai-khoan/don-hang/${query.code}`);

    /** FETCH SHIP PRICE */
    if (!this.props.losendConfigs) {
      this.props.dispatch(
        fetchLosendConfigs({
          callback: (res) => {
            if (res.status === 200) {
              const data = res.body.data;
              this.setState({
                shippingFee: data.shippingFee,
                currentShippingFee: data.currentShippingFee,
                minimumShippingFee: data.minimumShippingFee,
                extraFees: data.extraFees,
                promotionEnable: data.promotionEnable,
                isLoaded: true,
              });
            }
          },
        }),
      );
    }
  }

  renderStep = (step) => {
    const { senderInfo, receiverInfo } = this.state;

    const losendConfigs = {
      shippingFee: this.state.shippingFee,
      currentShippingFee: this.state.currentShippingFee,
      minimumShippingFee: this.state.minimumShippingFee,
      extraFees: this.state.extraFees,
      promotionEnable: this.state.promotionEnable,
    };

    if (step === STEP_ADDRESS_FROM) {
      return (
        <StepAddress
          step={step}
          stepLabels={this.stepLabels}
          toNextStep={() => this.changeStep(STEP_ADDRESS)}
          goBack={() => this.props.history.push('/')}
          confirmText={this.getString('this_is_pickup_address', 'shippingAddress')}
          markerText={this.getString('pickup_here', 'shippingAddress')}
          serviceName="losend"
          // address + distance
          shippingAddress={senderInfo}
          merchantAddress={receiverInfo}
          shippingAddresses={this.props.shippingAddresses}
          updateShippingInfo={this.updateShippingInfo}
          isSender
        />
      );
    }

    if (step === STEP_ADDRESS) {
      return (
        <StepAddress
          step={step}
          stepLabels={this.stepLabels}
          toNextStep={() => this.changeStep(STEP_CONFIRM)}
          goBack={() => this.changeStep(STEP_ADDRESS_FROM, true)}
          serviceName="losend"
          // address + distance
          shippingAddress={receiverInfo}
          merchantAddress={senderInfo}
          shippingAddresses={this.props.shippingAddresses}
          updateShippingInfo={this.updateShippingInfo}
        />
      );
    }

    return <StepConfirm {...this.computeConfirmData()} {...losendConfigs} serviceName="losend" />;
  };

  render() {
    const { step } = this.state;

    if (this.context.isMobile) {
      return (
        <div>
          {this.shouldShowNavbar() && (
            <MobileNavbar title={this.getNavbarTitle()} backHandler={this.handleBackNavbar} />
          )}
          <div className="order-page">{this.renderStep(step)}</div>
        </div>
      );
    }

    return (
      <div className="container">
        <div className="order-page" style={{ minHeight: '600px' }}>
          {this.renderStep(step)}
        </div>
      </div>
    );
  }

  shouldShowNavbar = () => {
    const { step } = this.state;
    if (step === STEP_ADDRESS_FROM || step === STEP_ADDRESS) return false;
    return true;
  };

  handleBackNavbar = () => {
    const { step } = this.state;
    if (step === STEP_ADDRESS_FROM) return this.props.history.push('/');
    if (step === STEP_ADDRESS) return this.changeStep(STEP_ADDRESS_FROM, true);
    if (step === STEP_CONFIRM) return this.changeStep(STEP_ADDRESS, true);
  };

  getNavbarTitle = () => {
    const { step } = this.state;

    if (step === STEP_ADDRESS_FROM) return this.getString('enter_sender_address');
    if (step === STEP_ADDRESS) return this.getString('enter_recipient_address');
    return this.getString('order_information');
  };
}

export default initComponent(SendPage, withRouter);
