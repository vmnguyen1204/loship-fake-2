import { getFacebookAccessToken } from 'actions/access-control';
import {
  CONFIRM_POPUP, CUSTOM_CONTENT,
  PROFILE_CHANGE_PASSWORD, PROFILE_UPDATE_INFO, PROFILE_UPDATE_PHONE, open,
} from 'actions/popup';
import { linkFacebook, unlinkFacebook, updateAvatar } from 'actions/user';
import BaseComponent from 'components/base';
import ImageLazy from 'components/statics/img-lazy';
import FileUploader from 'components/statics/uploader';
import qs from 'qs';
import React from 'react';
import { toast } from 'react-toastify';
import { getCurrentUser } from 'reducers/user';
import { parseErrorV2 } from 'utils/error';
import { phoneNumber } from 'utils/format';
import moment from 'utils/shims/moment';

class ProfileUpdatePageBase extends BaseComponent {
  static defaultProps = {
    dataGetter: getCurrentUser,
    customRender: true,
  };

  renderProfile = () => {
    const { data } = this.props;
    if (!data) return null;

    const { gender, birthday: bDay, email, phoneNumber: phone } = data.get('profile') || {};
    const fbAccountInfo = data.get('fbAccountInfo');
    const birthday = moment(bDay, 'DD/MM/YYYY');

    return (
      <div>
        <div className="section section-avatar">
          <ImageLazy className="avatar" src={data.get('avatar')} size={80} mode="click" />
          <FileUploader
            className="btn bg-light-gray text-black btn-camera"
            onChange={this.handleFileUpload}
            accept="image/*"
          >
            <i className="lz lz-camera" />
          </FileUploader>
        </div>

        <div className="section">
          <div className="profile-item">
            <b>{this.getString('personal_information', 'profile')}</b>
            <a className="pull-right" onClick={this.openUserInfoPopup}>
              {this.getString('tap_to_change')}
            </a>
          </div>

          <div className="profile-item">
            <i className="lz lz-person" />
            {data.get('fullname')}
            {gender && ', ' + this.getString(gender, 'profile')}
          </div>
          <div className="profile-item" title={email}>
            <i className="lz lz-email" />
            {email || <span className="text-gray">{this.getString('not_updated', 'profile')}</span>}
          </div>
          <div className="profile-item">
            <i className="fas fa-calendar-alt" />
            {(birthday.isValid() && birthday.format('DD/MM/YYYY')) || (
              <span className="text-gray">{this.getString('not_updated', 'profile')}</span>
            )}
          </div>
        </div>
        <div className="section">
          <div className="profile-item">
            <b>{this.getString('contact_phone_number', 'profile')}</b>
            <a className="pull-right" onClick={this.openPhoneNumberPopup}>
              {this.getString('tap_to_change')}
            </a>
          </div>
          <div className="profile-item">
            <i className="lz lz-phone" />
            {phoneNumber(phone) || <span className="text-gray">{this.getString('not_updated', 'profile')}</span>}
          </div>
        </div>
        {!this.isLoginByFacebook() && (
          <div className="section">
            <div className="profile-item">
              <div className="action-item" onClick={this.openChangePasswordPopup}>
                <i className="lz lz-lock" />
                {this.getString('change_password', 'profile')}
                <i className="fa fa-chevron-right pull-right" />
              </div>
            </div>
          </div>
        )}

        <div className="section">
          <div className="profile-item">
            <b>{this.getString('facebook_account', 'profile')}</b>
          </div>
          <div className="profile-item">
            {fbAccountInfo ? (
              <div className="fb-info-item">
                <ImageLazy src={fbAccountInfo.avatar} size={80} />
                <div className="name">{fbAccountInfo.name}</div>
                {!this.isLoginByFacebook() && (
                  <a className="pull-right" onClick={this.handleUnlinkFacebook}>
                    {this.getString('remove_link', 'profile')}
                  </a>
                )}
              </div>
            ) : (
              <div className="social-item" onClick={this.handleLinkFacebook}>
                <i className="fab fa-facebook pull-left" />
                {this.getString('link_facebook_account', 'profile')}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  handleFileUpload = async (data) => {
    const avatarUrl = await data.getURL();
    this.props.dispatch(updateAvatar({ image: avatarUrl, user: this.props.data.get('username') }));
  };

  openUserInfoPopup = () => {
    const { location: { pathname } } = this.props;
    const query = this.getQuery();

    this.props.history.push(`${pathname}?${qs.stringify({ ...query, o: 1 })}`);
    this.props.dispatch(
      open(PROFILE_UPDATE_INFO, { callback: () => this.props.history.push(`${pathname}?${qs.stringify({ ...query, o: undefined })}`) }),
    );
  };

  openPhoneNumberPopup = () => {
    const {
      data,
      location: { pathname },
    } = this.props;
    const query = this.getQuery();

    const nextUpdatePhoneNumber = moment(data.get('nextUpdatePhoneNumber'));
    if (nextUpdatePhoneNumber.isAfter(moment())) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('cannot_change_phone_number', 'profile'),
          content: [
            this.getString(
              'you_changed_the_phone_number_not_too_long_ago_for_safety_reasons_loship_will_restrict_the_change_of_phone_numbers_many_times_in_a_short_time',
              'profile',
            ),
            this.getString('next_change_0', 'profile', [nextUpdatePhoneNumber.format('HH:mm, DD/MM/YYYY')]),
          ],
          actionText: this.getString('i_got_it'),
          actionClassName: 'btn-fw',
          contentClassName: 'mw-400',
        }),
      );
    }

    this.props.history.push(`${pathname}?${qs.stringify({ ...query, o: 1 })}`);
    this.props.dispatch(
      open(PROFILE_UPDATE_PHONE, { callback: () => this.props.history.push(`${pathname}?${qs.stringify({ ...query, o: undefined })}`) }),
    );
  };

  openChangePasswordPopup = () => {
    const { location: { pathname } } = this.props;
    const query = this.getQuery();

    this.props.history.push(`${pathname}?${qs.stringify({ ...query, o: 1 })}`);
    this.props.dispatch(
      open(PROFILE_CHANGE_PASSWORD, {
        callback: () => {
          this.props.history.push(`${pathname}?${qs.stringify({ ...query, o: undefined })}`);
        },
      }),
    );
  };

  handleLinkFacebook = async () => {
    return getFacebookAccessToken({
      callback: (accessToken) => {
        // Press ignore on Facebook popup
        if (!accessToken) return;
        this.props.dispatch(
          linkFacebook({
            user: this.props.data.get('username'),
            fbAccessToken: accessToken,
            callback: () => toast(this.getString('successfully_link_facebook_account', 'profile')),
            callbackFailure: (err) => {
              const error = parseErrorV2(err);
              this.props.dispatch(
                open(CUSTOM_CONTENT, {
                  title: this.getString('account_link_failed', 'error'),
                  content: this.getString(error.codeEnum || error.message, 'error', [error.code]),
                  actionText: this.getString('go_back'),
                  actionClassName: 'btn-fw',
                }),
              );
            },
          }),
        );
      },
    });
  };

  handleUnlinkFacebook = () => {
    const confirmUnlink = () => {
      this.props.dispatch(
        unlinkFacebook({
          user: this.props.data.get('username'),
          callback: () => toast(this.getString('successfully_unlink_facebook_account', 'profile')),
          callbackFailure: (err) => {
            const error = parseErrorV2(err);
            this.props.dispatch(
              open(CUSTOM_CONTENT, {
                title: this.getString('account_unlink_failed', 'error'),
                content: this.getString(error.codeEnum || error.message, 'error', [error.code]),
                actionText: this.getString('go_back'),
                actionClassName: 'btn-fw',
              }),
            );
          },
        }),
      );
    };

    this.props.dispatch(
      open(CONFIRM_POPUP, {
        title: this.getString('remove_facebook_account', 'profile'),
        content: this.getString(
          'are_you_sure_you_want_to_unlink_this_facebook_account_after_unlink_you_will_no_longer_be_able_to_log_into_this_facebook_account',
          'profile',
        ),
        actionClassName: 'flex-column-reverse',
        confirmBtn: { text: this.getString('yes_i_want_to_unlink', 'profile') },
        cancelBtn: { text: this.getString('abort', 'profile'), className: 'bg-transparent text-gray' },
        onConfirm: confirmUnlink,
      }),
    );
  };

  isLoginByFacebook = () => {
    const { data } = this.props;
    return data.get('providers')?.[0] !== 'Phone'; // Fallback to Facebook for hide sensitive data when cannot detect provider
  };
}

export default ProfileUpdatePageBase;
