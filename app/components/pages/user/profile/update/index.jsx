import PropTypes from 'prop-types';
import React from 'react';
import Desktop from './update';
import Mobile from './update.mobile';

class ProfileUpdatePage extends React.Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}

export default ProfileUpdatePage;
