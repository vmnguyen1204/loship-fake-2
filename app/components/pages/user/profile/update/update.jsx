import React from 'react';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router';
import ProfileUpdatePageBase from './update-base';

class ProfileUpdatePage extends ProfileUpdatePageBase {
  render() {
    return <div className="profile-panel user-profile-update">{this.renderProfile()}</div>;
  }
}

export default initComponent(ProfileUpdatePage, withRouter);
