import { PROFILE_UPDATE_INFO, close } from 'actions/popup';

import MobileNav from 'components/shared/mobile-nav';
import React from 'react';
import initComponent from 'lib/initComponent';
import ProfileUpdatePageBase from './update-base';

class ProfileUpdatePage extends ProfileUpdatePageBase {
  componentDidUpdate() {
    const query = this.getQuery();
    if (!query.o) {
      this.props.dispatch(close(PROFILE_UPDATE_INFO));
    }
  }

  render() {
    return (
      <div className="user-profile-update">
        <MobileNav hasBackButton title={this.getNavbarTitle()} backHandler={this.handleBack} />
        {this.renderProfile()}
      </div>
    );
  }

  getNavbarTitle = () => {
    return this.getString('update_profile', 'profile');
  };

  handleBack = () => {
    this.props.history.push('/tai-khoan');
  };
}

export default initComponent(ProfileUpdatePage);
