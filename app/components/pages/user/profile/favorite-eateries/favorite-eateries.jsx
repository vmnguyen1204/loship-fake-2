import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import ProfileFavoriteEateriesPageBase from './favorite-eateries-base';

class ProfileFavoriteEateriesPage extends ProfileFavoriteEateriesPageBase {
  render() {
    return <div className="user-favorite-eateries">{this.renderFavoriteEateries()}</div>;
  }
}

export default initComponent(ProfileFavoriteEateriesPage, withRouter);
