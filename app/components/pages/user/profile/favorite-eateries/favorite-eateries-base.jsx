import { deleteLikeEatery } from 'actions/merchant';
import { CONFIRM_POPUP, GENERIC_POPUP, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItem } from 'components/shared/newsfeed/merchant-item';
import Spinner from 'components/statics/spinner';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { getGlobalAddress } from 'reducers/new/metadata';
import { getKey, parseQuery } from 'reducers/newsfeed';

if (process.env.BROWSER) {
  require('assets/styles/pages/eatery-favorite.scss');
  require('assets/styles/components/_newsfeed.scss');
}

class ProfileFavoriteEateriesPage extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
    globalAddress: PropTypes.object,
  }

  static defaultProps = {
    dataGetter: (state) => {
      const data = {};

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data.globalAddress = globalAddress.data;

      return data;
    },
    customRender: true,
  }

  renderLoading = () => {
    return (
      <div className="user-favorite-eateries-empty">
        <Spinner type="wave">
          <span className="ml-8">{this.getString('loading')}</span>
        </Spinner>
      </div>
    );
  }

  renderEmpty = () => {
    return (
      <div className="user-favorite-eateries-empty">
        <img src="/dist/images/favorite-eateries-empty.png" alt="Favorite eateries empty" />

        <p>
          <b>{this.getString('no_favorite_eateries')}</b>
        </p>
        <p>{this.getString('no_favorite_eateries_description')}</p>

        <div className={cx(this.context.isMobile ? 'section-cta' : 'mt-16 miw-320')}>
          <Button type="primary" to="/" className="btn-fw">{this.getString('explore_loship')}</Button>
        </div>
      </div>
    );
  }

  renderFavoriteEateries = () => {
    const query = this.getQuery();

    return (
      <SectionNewsfeed
        params={{
          ...query,
          limit: this.context.isMobile ? 8 : 12,
          favoriteEateriesApi: true,
          force: true,
        }}
        submit={this.submit}
        mode={this.context.isMobile ? 'preload|load_more' : 'preload|pagination'}
        MerchantItem={({ merchant, isMobile, ...rest }) => (
          <EateryItem
            key={merchant.id}
            isMobile={isMobile}
            merchant={merchant}
            handleExtraActions={this.handleEateryActions(merchant)}
            {...rest}
          />
        )}
        renderNotFound={this.renderEmpty}
        renderLoading={this.renderLoading}
      />
    );
  }

  submit = (query) => {
    const { pathname } = this.props.location;

    const thisQuery = this.getQuery();
    const nextQuery = (() => {
      if (query.q && thisQuery.q !== query.q) return { ...thisQuery, ...query, page: undefined };
      return { ...thisQuery, ...query };
    })();

    if ((thisQuery.page || nextQuery.page) && parseInt(thisQuery.page) !== nextQuery.page) {
      return this.props.history.push(`${pathname}?${qs.stringify(nextQuery)}`);
    }

    const elem = document.getElementById('screen');
    elem.scrollTop = 0;
  }

  handleEateryActions = (merchant) => (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('view_merchant_info'),
        content: (props) => (
          <div className="section-newsfeed">
            <div className="content">
              <div className="list-view">
                <EateryItem
                  params={{}}
                  merchant={merchant}
                  isMobile={true}
                  scrollableAncestor={null}
                  onClick={(evt) => {
                    evt.stopPropagation();
                    evt.preventDefault();
                  }}
                />
                <div className="list-item action" onClick={this.handleRemoveFromFavorite(merchant, props.handleClose)}>
                  <i className="lz lz-fw lz-trash" />
                  {this.getString('remove_from_favorite')}
                </div>
              </div>
            </div>
          </div>
        ),
        className: 'favorite-eateries-action-popup mobile',
        closeBtnClassName: 'lz-close',
        animationStack: 1,
      }),
    );
  }

  handleRemoveFromFavorite = (merchant, callback) => () => {
    const { isMobile } = this.context;
    this.props.dispatch(
      open(CONFIRM_POPUP, {
        content: this.getString('remove_from_favorite_description', '', [<b key="name">{merchant.name}</b>]),
        actionClassName: isMobile && 'flex-column-reverse',
        confirmBtn: { text: this.getString('ok'), className: isMobile && 'btn-fw' },
        cancelBtn: {
          text: this.getString('back'),
          type: 'link',
          className: 'bg-transparent text-gray px-8 py-8',
        },
        onConfirm: () => {
          this.props.dispatch(deleteLikeEatery({
            eateryId: merchant.id,
            callback,
            currentKey: getKey(parseQuery({
              limit: this.context.isMobile ? 8 : 12,
              favoriteEateriesApi: true,
              globalAddress: this.props.globalAddress,
              force: true,
            })),
          }));
        },
      }),
    );
  }
}

export default ProfileFavoriteEateriesPage;
