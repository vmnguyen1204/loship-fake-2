import MobileNav from 'components/shared/mobile-nav';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import ProfileFavoriteEateriesPageBase from './favorite-eateries-base';

class ProfileFavoriteEateriesPage extends ProfileFavoriteEateriesPageBase {
  render() {
    return (
      <div className="user-favorite-eateries">
        <MobileNav hasBackButton title={this.getString('favorite_merchants')} backHandler={this.handleBack} />
        {this.renderFavoriteEateries()}
      </div>
    );
  }

  handleBack = () => {
    this.props.history.push('/tai-khoan');
  };
}

export default initComponent(ProfileFavoriteEateriesPage, withRouter);
