import PropTypes from 'prop-types';
import React from 'react';
import Desktop from './favorite-eateries';
import Mobile from './favorite-eateries.mobile';

class ProfileFavoriteEateriesPage extends React.Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}

export default ProfileFavoriteEateriesPage;
