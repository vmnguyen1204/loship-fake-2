import BasePage from 'components/pages/base';
import OrderListPage from 'components/pages/user/profile/orders';
import ProfileUpdatePage from 'components/pages/user/profile/update';
import OrderDebt from 'components/shared/order/payment/debt-order';
import ImageLazy from 'components/statics/img-lazy';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { getConfigs } from 'reducers/order';
import { getCurrentUser } from 'reducers/user';
import { APP_INFO, LINK_DOMAIN_LOSHIP } from 'settings/variables';
import { addCurrency, phoneNumber } from 'utils/format';
import { getRoute } from 'utils/routing';
import Link from 'utils/shims/link';

if (process.env.BROWSER) {
  require('assets/styles/pages/user-profile.scss');
  require('assets/styles/pages/user-profile-mobile.scss');
}

class UserProfile extends BasePage {
  static defaultProps = {
    dataGetter: (state) => {
      let data = getCurrentUser(state);
      if (!data) return;

      const config = getConfigs(state);
      if (config) data = data.set('config', config.get('data'));

      return data;
    },
    customRender: true,
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  componentDidMount() {
    this.handleRedirect();
  }

  componentDidUpdate() {
    this.handleRedirect();
  }

  renderNavigation = () => {
    const { isMobile, partnerName } = this.context;
    const { data, config } = this.props;
    if (!data) return null;

    if (isMobile) {
      return (
        <div className="profile-navigation-panel">
          <div className="profile-card">
            <ImageLazy src={data.get('avatar')} size={80} mode="click" />
            <div className="content">
              <h4>{data.get('fullname')}</h4>
              <div>{phoneNumber(data.get('phoneNumber'))}</div>
            </div>
          </div>

          <OrderDebt />

          {config && config.get('referralEnable') && partnerName === 'none' && (
            <Link to="/tai-khoan/gioi-thieu" className="profile-navigation separate">
              <ImageLazy
                src="/dist/images/referral-icon.png"
                shape="square"
                placeholder="/dist/images/dish-placeholder.png"
              />
              <div className="content">
                {this.getString('share_loship_with_friend', 'profile')}
                <br />
                <span className="text-dark-gray">{this.getString('earn_x', '', [addCurrency(10000)])}</span>
              </div>
              <i className="fa fa-chevron-right pull-right" />
            </Link>
          )}
          {APP_INFO.serviceName === 'loship' && partnerName === 'none' && (
            <Link to="/cong-dong-loship" className="profile-navigation">
              <i className="lz lz-fw lz-smile" />
              <div className="content">{this.getString('loship_community', 'profile')}</div>
              <i className="fa fa-chevron-right pull-right" />
            </Link>
          )}
          {partnerName === 'none' && (
            <Link to="/tai-khoan/cua-hang-yeu-thich" className="profile-navigation">
              <i className="lz lz-fw lz-heart-o" />
              <div className="content">{this.getString('favorite_merchants', 'profile')}</div>
              <i className="fa fa-chevron-right pull-right" />
            </Link>
          )}
          {partnerName === 'none' && (
            <Link to="/tai-khoan/cap-nhat" className="profile-navigation">
              <i className="lz lz-fw lz-pencil" />
              <div className="content">{this.getString('update_profile', 'profile')}</div>
              <i className="fa fa-chevron-right pull-right" />
            </Link>
          )}
          {partnerName === 'none' && (
            <Link to="/tai-khoan/quan-ly-the" className="profile-navigation">
              <i className="lz lz-fw lz-card" />
              <div className="content">{this.getString('update_payment_card', 'profile')}</div>
              <i className="fa fa-chevron-right pull-right" />
            </Link>
          )}
          {partnerName === 'none' && (
            <a
              href={`${LINK_DOMAIN_LOSHIP}/cskh/de-xuat-cua-hang?cpn=${data.get('phoneNumber') || ''}&cc=${data.get('countryCode') || ''}`}
              className="profile-navigation"
            >
              <i className="lz lz-fw lz-comment" />
              <div className="content">
                {this.getString('suggest_merchant_for_loship', 'profile')}
                <i className="lz lz-like" />
              </div>
              <i className="fa fa-chevron-right pull-right" />
            </a>
          )}
          {partnerName === 'none' && (
            <a
              href={`${LINK_DOMAIN_LOSHIP}/cskh/de-xuat-tinh-nang?cpn=${data.get('phoneNumber') || ''}&cc=${data.get('countryCode') || ''}`}
              className="profile-navigation"
            >
              <i className="lz lz-fw lz-comment" />
              <div className="content">
                {this.getString('suggest_feature_for_loship', 'profile')}
                <i className="lz lz-like" />
              </div>
              <i className="fa fa-chevron-right pull-right" />
            </a>
          )}
        </div>
      );
    }

    return (
      <div className="profile-navigation-panel">
        <ProfileUpdatePage />

        {config && config.get('referralEnable') && (
          <Link to="/tai-khoan/gioi-thieu" className="profile-navigation separate module">
            <ImageLazy
              src="/dist/images/referral-icon.png"
              shape="square"
              placeholder="/dist/images/dish-placeholder.png"
            />
            <div className="content">
              {this.getString('share_loship_with_friend', 'profile')}
              <br />
              <span className="text-dark-gray">{this.getString('earn_x', '', [addCurrency(10000)])}</span>
            </div>
            <i className="fa fa-chevron-right pull-right" />
          </Link>
        )}

        <div className="module">
          {APP_INFO.serviceName === 'loship' && (
            <Link to="/cong-dong-loship" className="profile-navigation">
              <i className="lz lz-fw lz-smile" />
              <div className="content">{this.getString('loship_community', 'profile')}</div>
              <i className="fa fa-chevron-right pull-right" />
            </Link>
          )}
          <a
            href={`${LINK_DOMAIN_LOSHIP}/cskh/de-xuat-cua-hang?cpn=${data.get('phoneNumber') || ''}&cc=${data.get('countryCode') || ''}`}
            className="profile-navigation"
          >
            <i className="lz lz-fw lz-comment" />
            <div className="content">
              {this.getString('suggest_merchant_for_loship', 'profile')}
              <i className="lz lz-like" />
            </div>
            <i className="fa fa-chevron-right pull-right" />
          </a>
          <a
            href={`${LINK_DOMAIN_LOSHIP}/cskh/de-xuat-tinh-nang?cpn=${data.get('phoneNumber') || ''}&cc=${data.get('countryCode') || ''}`}
            className="profile-navigation"
          >
            <i className="lz lz-fw lz-comment" />
            <div className="content">
              {this.getString('suggest_feature_for_loship', 'profile')}
              <i className="lz lz-like" />
            </div>
            <i className="fa fa-chevron-right pull-right" />
          </a>
        </div>
      </div>
    );
  };

  render() {
    const { isMobile, partnerName } = this.context;
    const { location: { pathname } } = this.props;
    const { match } = getRoute(null, pathname);

    if (isMobile) {
      if (match && match.params.order) return this.props.children;
      if ([
        '/tai-khoan/cap-nhat', '/tai-khoan/quan-ly-the',
        '/tai-khoan/cua-hang-yeu-thich',
      ].includes(pathname)) return this.props.children;
    }

    return (
      <div className="user-profile-page">
        <div className="container">
          {this.renderNavigation()}
          <div className="profile-content-panel">
            {this.props.data && partnerName === 'none' &&(
              <ul className="tab-navigation">
                <li>
                  <Link
                    to="/tai-khoan/don-hang"
                    className={pathname.includes('/tai-khoan/don-hang') ? 'active' : ''}
                  >
                    {this.getString('your_order')}
                  </Link>
                </li>
                {!isMobile && (
                  <li>
                    <Link
                      to="/tai-khoan/cua-hang-yeu-thich"
                      className={pathname.includes('/tai-khoan/cua-hang-yeu-thich') ? 'active' : ''}
                    >
                      {this.getString('favorite_merchants')}
                    </Link>
                  </li>
                )}
                {!isMobile && (
                  <li>
                    <Link
                      to="/tai-khoan/quan-ly-the"
                      className={pathname.includes('/tai-khoan/quan-ly-the') ? 'active' : ''}
                    >
                      {this.getString('update_payment_card')}
                    </Link>
                  </li>
                )}
              </ul>
            )}

            {(() => {
              if (['/tai-khoan/don-hang', '/tai-khoan/quan-ly-the', '/tai-khoan/cua-hang-yeu-thich'].some(
                (path) => pathname.includes(path),
              )) return this.props.children;
              return <OrderListPage />;
            })()}
          </div>
        </div>
      </div>
    );
  }

  handleRedirect = () => {
    const { pathname, search } = this.props.location;
    if (pathname === '/tai-khoan') {
      return this.props.history.replace('/tai-khoan/don-hang');
    }

    if (!this.props.data) {
      if (search.includes('usp=sharing')) return;
      return this.props.history.push('/');
    }
  };
}

export default initComponent(UserProfile);
