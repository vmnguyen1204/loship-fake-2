import React from 'react';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';
import ProfilePaymentPageBase from './payment-base';

class ProfilePaymentPage extends ProfilePaymentPageBase {
  render() {
    return (
      <div className="user-payment-card">
        {this.renderPaymentAddCard()}
        {this.renderPaymentCards()}
        {this.renderPaymentNotice()}
      </div>
    );
  }
}

export default initComponent(ProfilePaymentPage, withRouter);
