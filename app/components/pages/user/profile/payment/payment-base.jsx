import { fetchPaymentSupportTokenized } from 'actions/order';
import { CONFIRM_POPUP, GENERIC_POPUP, open } from 'actions/popup';
import { deletePaymentCard, fetchPaymentCards } from 'actions/user';
import BaseComponent from 'components/base';
import { addCardFunc } from 'components/shared/order/payment/add-card';
import OrderPaymentNotice from 'components/shared/order/payment/notice';
import PropTypes from 'prop-types';
import React from 'react';
import { getPaymentSupportTokenized } from 'reducers/order';
import { getCurrentUser, getPaymentCards } from 'reducers/user';
import { addCurrency, formatCard } from 'utils/format';
import { LIST_PAYMENT_TEXT } from 'utils/json/payment';

class ProfilePaymentPageBase extends BaseComponent {
  static defaultProps = {
    dataGetter: (state) => {
      let data = getCurrentUser(state);

      const paymentCards = getPaymentCards(state);
      if (paymentCards) data = data.set('paymentCards', paymentCards.get('data'));

      const paymentsSupportTokenized = getPaymentSupportTokenized(state);
      if (paymentsSupportTokenized) data = data.set('paymentsSupportTokenized', paymentsSupportTokenized.get('data'));

      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  componentDidMount() {
    if (!this.props.paymentCards) this.props.dispatch(fetchPaymentCards());
    this.props.dispatch(fetchPaymentSupportTokenized());
  }

  renderEmpty = () => {
    return (
      <div className="payment-empty">
        <img src="/dist/images/payment-empty.png" alt="Payment empty" />

        <p>
          <b>{this.getString('payment_empty_header')}</b>
        </p>
        <p>{this.getString('payment_empty_message')}</p>
      </div>
    );
  };

  renderPaymentCards = () => {
    const { paymentCards } = this.props;
    if (!paymentCards || paymentCards.size === 0) return this.renderEmpty();

    return (
      <div className="list-view list-payment-card">
        <div className="title">{this.getString('saved_card')}</div>
        {paymentCards.map((card) => (
          <div key={card.id} className="list-item">
            <div className="figure">
              <img src={card.image} alt="Card" />
            </div>
            <div className="content">
              <p>{formatCard(card.prefixNumber, card.postfixNumber)}</p>
            </div>
            {this.context.isMobile ? (
              <div className="action" onClick={this.showCardDetail(card)}>
                <i className="fas fa-ellipsis-v" />
              </div>
            ) : (
              <div className="action" onClick={this.removeCard(card.id)}>
                <i className="fas fa-trash-alt" />
              </div>
            )}
          </div>
        ))}
      </div>
    );
  };

  renderPaymentAddCard = () => {
    const { paymentsSupportTokenized } = this.props;
    if (!paymentsSupportTokenized || paymentsSupportTokenized.size === 0) return null;

    return (
      <div className="list-view list-payment-card">
        <div className="title">{this.getString('add_card')}</div>
        {paymentsSupportTokenized.map((payment) => (
          <div key={payment.id} className="list-item add-card" onClick={this.handleAddPaymentCard(payment)}>
            <i className="lz lz-plus-r" />
            <span className="pl-16">
              {this.getString('add')} {this.getString(LIST_PAYMENT_TEXT[payment.method])}
            </span>
          </div>
        ))}
      </div>
    );
  };

  renderPaymentNotice = () => {
    return (
      <OrderPaymentNotice />
    );
  };

  showCardDetail = (card) => () => {
    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('payment_card_info'),
        content: (props) => (
          <div className="user-payment-card">
            <div className="list-view list-payment-card">
              <div key={card.id} className="list-item">
                <div className="figure">
                  <img src={card.image} alt="Card" />
                </div>
                <div className="content">
                  <p>{formatCard(card.prefixNumber, card.postfixNumber)}</p>
                </div>
              </div>
              <div className="list-item border-top" onClick={this.removeCard(card.id, props.handleClose)}>
                {this.getString('payment_card_remove')}
              </div>
            </div>
          </div>
        ),
        closeBtnClassName: 'lz-close',
        animationStack: 1,
      }),
    );
  };

  removeCard = (cardId, callback) => () => {
    if (!cardId) return;

    const { isMobile } = this.context;
    this.props.dispatch(
      open(CONFIRM_POPUP, {
        title: this.getString('payment_card_remove_confirm'),
        content: this.getString('payment_card_remove_description'),
        actionClassName: isMobile && 'flex-column-reverse',
        confirmBtn: { text: this.getString('payment_card_remove_confirm_txt'), className: isMobile && 'btn-fw' },
        cancelBtn: {
          text: this.getString('back'),
          type: 'link',
          className: 'text-gray p8_8',
        },
        onConfirm: () => {
          this.props.dispatch(deletePaymentCard({ cardId, callback }));
        },
      }),
    );
  };

  handleAddPaymentCard = (payment) => () => {
    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('add_payment_card'),
        content: this.getString(
          'add_payment_card_description', '',
          [addCurrency(payment.paymentMethod && payment.paymentMethod.issueFee || 0, '')],
          true,
        ),
        className: 'mw-400',
        contentClassName: 'text-left',
        actionClassName: 'flex-column-reverse',
        onConfirm: addCardFunc.bind(this, { payment }),
        confirmBtn: { text: this.getString('continue'), className: 'btn-fw' },
        cancelBtn: { text: this.getString('return'), type: 'link', className: 'text-dark-gray py-8 px-8' },
      }),
    );
  };
}

export default ProfilePaymentPageBase;
