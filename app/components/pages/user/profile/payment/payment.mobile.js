import React from 'react';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';
import MobileNav from 'components/shared/mobile-nav';
import ProfilePaymentPageBase from './payment-base';

class ProfilePaymentPage extends ProfilePaymentPageBase {
  render() {
    return (
      <div className="user-payment-card">
        <MobileNav hasBackButton title={this.getString('update_payment_card')} backHandler={this.handleBack} />
        {this.renderPaymentAddCard()}
        {this.renderPaymentCards()}
        {this.renderPaymentNotice()}
      </div>
    );
  }

  handleBack = () => {
    this.props.history.push('/tai-khoan');
  };
}

export default initComponent(ProfilePaymentPage, withRouter);
