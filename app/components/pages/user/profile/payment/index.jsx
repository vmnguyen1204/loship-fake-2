import PropTypes from 'prop-types';
import React from 'react';
import Desktop from './payment';
import Mobile from './payment.mobile';

class ProfilePaymentPage extends React.Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}

export default ProfilePaymentPage;
