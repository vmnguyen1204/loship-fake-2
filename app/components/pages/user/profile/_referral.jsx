import { GENERIC_POPUP, ORDER_FAILED, SHARING_POPUP, open } from 'actions/popup';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import ImageLazy from 'components/statics/img-lazy';
import React from 'react';
import { addCurrency } from 'utils/format';
import { copyToClipboard } from 'utils/content';
import { fetchReferees, fetchReferralCode } from 'actions/user';
import { getConfigs } from 'reducers/order';
import { getReferees, getReferralCode } from 'reducers/user';
import initComponent from 'lib/initComponent';
import { toast } from 'react-toastify';
import { iife } from 'utils/helper';

class ReferralPopupComp extends BaseComponent {
  static defaultProps = {
    actionCreator: fetchReferralCode,
    dataGetter: (state) => {
      let data = getReferralCode(state);
      if (!data) return;

      const config = getConfigs(state);
      if (config) data = data.set('config', config.get('data'));

      const referees = getReferees(state);
      if (referees) data = data.set('referees', referees.get('data'));

      return data;
    },
  };

  componentDidMount() {
    this.props.dispatch(fetchReferees());
  }

  renderReferees = () => {
    const { referees } = this.props;

    return (
      <div className="referees list-view">
        <div className="title">{this.getString('invited_friends')}</div>
        {!referees || referees.size === 0 && (
          <div className="list-item empty">
            <img src="/dist/images/promo-not-found.png" alt="Promo not found" />
            <div className="content">{this.getString('invited_friends_notice', '', [addCurrency(10000)])}</div>
          </div>
        )}
        {referees && referees.map((referee) => {
          const { createdBy = {}, referrerPromotionValue } = referee;
          return (
            <div key={referee.id} className="list-item">
              <ImageLazy className="figure" src={createdBy.avatar} />
              <div className="content">
                <h3>{createdBy.name && createdBy.name.full}</h3>
                <span className="text-dark-gray">
                  {this.getString('you_earn_x', '', [addCurrency(referrerPromotionValue)])}
                </span>
              </div>
            </div>
          );
        })}
      </div>
    );
  };

  render() {
    const { data } = this.props;

    return (
      <div className="share-referral-popup-content">
        <ImageLazy
          src="/dist/images/referral-img-1.png"
          placeholder="/dist/images/dish-placeholder.png"
          shape="square"
          size={320}
        />
        <div className="bg-white p16_16">
          <h3>{this.getString('share_with_friend_earn_x', '', [addCurrency(10000)])}</h3>

          <p>{this.getString('step_to_earn_referral_reward')}</p>
          <ol>
            <li>{this.getString('referral_step_1')}</li>
            <li>{this.getString('referral_step_2', '', [addCurrency(10000)])}</li>
            <li>{this.getString('referral_step_3', '', [addCurrency(10000)])}</li>
          </ol>

          <p>{this.getString('your_referral_code')}</p>
          <div className="referral-code">
            <input value={data.get('code')} readOnly />
            <Button onClick={this.handleCopyCode}>
              <i className="far fa-clone" />
              <span>{this.getString('copy')}</span>
            </Button>
          </div>

          <Button type="primary" fw className="mt-16" onClick={this.handleOpenInvitePopup}>
            {this.getString('invite_your_friend')}
          </Button>
        </div>

        <div className="coupon-container">
          <div className="coupon-banner">
            <div className="content">
              <h3>{this.getString('redeem_now')}</h3>
              <p>{this.getString('so_many_gift_wait_you')}</p>
            </div>
            <img src="/dist/images/referral-coupon.png" alt="coupon" />
            <Button type="primary" className="btn-sm" onClick={this.openDownloadAppBanner}>
              {this.getString('redeem')}
            </Button>
          </div>
        </div>

        {this.renderReferees()}
      </div>
    );
  }

  handleCopyCode = () => {
    const { data, config } = this.props;
    const sharingContent = this.getString('enter_code_x_for_y_off', '', [
      data.get('code'),
      addCurrency(config.get('referralRefereePromotionValue')),
    ]);
    const sharingUrl = 'https://loship.vn/use-app';

    copyToClipboard(`${sharingContent} ${sharingUrl}`);
    toast(this.getString('copied_content'));
  };

  handleOpenInvitePopup = () => {
    const { data, config } = this.props;
    const sharingContent = this.getString('enter_code_x_for_y_off', '', [
      data.get('code'),
      addCurrency(config.get('referralRefereePromotionValue')),
    ]);
    const sharingUrl = 'https://loship.vn/use-app';

    copyToClipboard(`${sharingContent} ${sharingUrl}`);
    toast(this.getString('copied_content'));

    this.props.dispatch(
      open(SHARING_POPUP, {
        url: sharingUrl,
        subject: this.getString('share_referral_code'),
        body: sharingContent,
      }),
    );
  };

  openDownloadAppBanner = () => {
    this.props.dispatch(
      open(ORDER_FAILED, {
        wrapperStyle: { zIndex: 201 },
        title: this.getString('only_available_on_mobile_app'),
        error: iife(() => (
          <div style={{ maxWidth: 500 }}>
            <p>{this.getString('the_feature_you_are_requesting_is_only_available_on_loship_app')}</p>
            <div>
              <a href="https://goo.gl/N5Vs4V" className="app-img app-android" target="_blank" rel="noopener">
                <img src="/dist/images/app-android.png" alt="android" style={{ maxWidth: 120, marginRight: 8 }} />
              </a>
              <a href="https://goo.gl/CDFMhZ" className="app-img app-ios" target="_blank" rel="noopener">
                <img src="/dist/images/app-ios.png" alt="ios" style={{ maxWidth: 120 }} />
              </a>
            </div>
          </div>
        )),
        hideBanner: true,
        action: () => {
          window.location.href = '/';
        },
        actionText: this.getString('return_to_homepage'),
      }),
    );
  };
}
const ReferralPopup = initComponent(ReferralPopupComp);

export function ReferralPopupHandler({ callback }) {
  if (!this || !this.props) return;

  this.props.dispatch(
    open(GENERIC_POPUP, {
      title: this.getString('share_referral_code'),
      content: (popupProps) => <ReferralPopup {...popupProps} />,
      className: 'share-referral-popup',
      animationStack: 3,
      onCancel: () => {
        typeof callback === 'function' && callback();
        this.props.history.push('/tai-khoan/don-hang');
      },
    }),
  );
}
