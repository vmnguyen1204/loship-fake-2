import { CUSTOM_CONTENT, ORDER_FAILED, open } from 'actions/popup';
import { fetchMoreOrdersHistory, fetchOrdersHistory } from 'actions/order';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Link from 'utils/shims/link';
import PropTypes from 'prop-types';
import React from 'react';
import Spinner from 'components/statics/spinner';
import cx from 'classnames';
import { fetchMerchant } from 'actions/merchant';
import { getCurrentUserOrders } from 'reducers/metadata';
import { getRoute } from 'utils/routing';
import { getStatus } from 'reducers/notification';
import initComponent from 'lib/initComponent';
import loziServices, { servicesAvailable } from 'utils/json/loziServices';
import moment from 'utils/shims/moment';

import { withRouter } from 'react-router-dom';
import { isMerchantReady } from 'utils/merchant';
import { iife } from 'utils/helper';

if (process.env.BROWSER) {
  require('assets/styles/pages/order-list.scss');
}

class OrderListPage extends BaseComponent {
  static defaultProps = {
    dataGetter: (state) => {
      const data = getCurrentUserOrders(state);
      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  componentDidMount() {
    this.props.dispatch(fetchOrdersHistory({ force: true }));
    if (!this.props.location.pathname.includes('/tai-khoan')) {
      this.props.history.replace(`/tai-khoan${this.props.location.pathname}`);
    }
  }

  componentDidUpdate() {
    if (!this.state.isLoading) return;
    this.setState({ isLoading: false });
  }

  loadMore = () => {
    const extras = this.props.extras;
    const nextUrl = extras && extras.get('nextUrl');

    if (nextUrl) {
      this.setState(
        (state) => ({ ...state, isLoading: true }),
        () => {
          this.props.dispatch(fetchMoreOrdersHistory(nextUrl, this.getParams()));
        },
      );
    }
  };

  renderDetailPage() {
    if (!this.props.children) return null;
    return this.props.children;
  }

  renderLoadingPage() {
    return (
      <div className="page-order-list">
        <Spinner type="wave">
          <span className="ml-8">{this.getString('loading')}</span>
        </Spinner>
      </div>
    );
  }

  render() {
    const { data: orders, extras, status: loadingStatus } = this.props;

    const { match } = getRoute(null, this.props.location.pathname);
    if (match && match.params.order) return this.renderDetailPage();

    if (!orders) {
      if (loadingStatus === 'ERROR') return <div />;
      return this.renderLoadingPage();
    }

    return (
      <div className="page-order-list">
        <ul className="order-list">
          {orders.toArray().map((order) => {
            const statusText = getStatus(order);
            const avatar = (() => {
              if (order.serviceName === 'losend') return '/dist/images/losend-order-item.png';
              if (order.serviceName === 'loxe') return '/dist/images/loxe-order-item.png';
              return order.eatery.avatar;
            })();
            const status = order.status;
            const canReOrder = status === 'done' || status === 'cancel';
            const service = loziServices.find((s) => s.name === order.serviceName) ||
              loziServices.find((s) => s.name === 'loship');

            const orderUrl = iife(() => {
              const shareId = order.sharing?.shareId;
              if (shareId && this.getPrivileges(order) !== 'EDIT') {
                return `/tai-khoan/don-hang/${shareId}?usp=sharing`;
              }
              return `/tai-khoan/don-hang/${order.code}`;
            });

            return (
              <li key={order.code}>
                <div className="order-item">
                  <div className="avatar">
                    <img src={avatar} alt="Avatar" />
                  </div>
                  <div className="content">
                    <div className="col">
                      <p>
                        <label>{this.getString('order_code')}</label>
                        <b>{`#${order.code}`}</b>
                      </p>
                      {['loxe', 'losend'].includes(order.serviceName) ? (
                        <p>
                          <label>{this.getString('service')}:</label>
                          {service.title}
                        </p>
                      ) : (
                        <p>
                          <label>{this.getString('location')}</label>
                          <Link className="link-merchant" to={`/${order.eatery.username}`}>
                            {order.eatery.name}
                          </Link>
                        </p>
                      )}
                    </div>
                    <div className="col created-at">
                      <p>
                        {moment(order.createdAt).format('DD/MM/YYYY')} <br />
                        <br />
                      </p>
                      <p>
                        <span className={`order-status ${statusText}`}>{this.getString(statusText, 'notifier')}</span>
                      </p>
                    </div>
                    <div className="col actions">
                      <Button to={orderUrl}>{this.getString('view_details')}</Button>

                      {canReOrder && (
                        <Button
                          type="feature"
                          className={cx({ disabled: order.serviceName !== 'loship' && order.serviceName !== 'lomart' })}
                          onClick={this.reOrder(order.code)}
                        >
                          <i className="fas fa-redo" />
                          <span>{this.getString('re_order')}</span>
                        </Button>
                      )}
                    </div>
                  </div>
                </div>
              </li>
            );
          })}
        </ul>
        {extras.get('nextUrl') && (
          <Button disabled={this.state.isLoading} onClick={this.loadMore}>
            {this.state.isLoading && <i className="fas fa-spinner fa-spin" />}
            <span>{this.getString('view_more')}</span>
          </Button>
        )}
      </div>
    );
  }

  reOrder = (code) => () => {
    const { data } = this.props;

    if (!data) return;

    const sourceOrder = data.toArray().find((order) => order.code === code);

    if (!sourceOrder) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('something_went_wrong', 'eatery'),
          content: this.getString('theres_unexpected_problem_prevent_us_to_process_your_action_sorry', 'eatery'),
        }),
      );
    }

    const serviceName = sourceOrder.serviceName;
    const isAvailable = servicesAvailable.includes(serviceName);
    if (!isAvailable) {
      return this.props.dispatch(
        open(ORDER_FAILED, {
          title: this.getString('only_available_on_mobile_app'),
          error: iife(() => (
            <div>
              <p>
                {this.getString(
                  'the_service_you_are_requesting_is_only_available_on_loship_app_download_app_on_your_phone_to_use_the_service',
                )}
              </p>
              <div className="promo-btns" style={{ marginBottom: 32, marginTop: 8 }}>
                <a href="https://goo.gl/N5Vs4V" className="app-img app-android" target="_blank" rel="noopener">
                  <img src="/dist/images/app-android.png" alt="android" style={{ maxWidth: 120, marginRight: 8 }} />
                </a>
                <a href="https://goo.gl/CDFMhZ" className="app-img app-ios" target="_blank" rel="noopener">
                  <img src="/dist/images/app-ios.png" alt="ios" style={{ maxWidth: 120 }} />
                </a>
              </div>
            </div>
          )),
          hideBanner: true,
        }),
      );
    }

    const { username, slug } = sourceOrder.eatery || {};
    const redirectUrl = serviceName === 'losend' ? '/send' : (username && `/${username}`) || `/b/${slug}`;

    if (serviceName === 'losend') return this.props.history.push(redirectUrl + `?status=re-order&code=${sourceOrder.code}`);

    this.props.dispatch(
      fetchMerchant({
        merchantUsername: username,
        merchant: slug,
        callback: (resData) => {
          if (
            !isMerchantReady(
              { ...resData, shippingService: resData.shippingService && resData.shippingService.name },
              {
                dispatch: this.props.dispatch,
                getString: this.getString,
                history: this.props.history,
                callback: () => {
                  this.props.history.push(redirectUrl + `?status=re-order&code=${sourceOrder.code}`);
                },
              },
            )
          ) return;

          this.props.history.push(redirectUrl + `?status=re-order&code=${sourceOrder.code}`);
        },
      }),
    );
  };

  getPrivileges = (order) => {
    const privileges = order.privileges || [];

    if (privileges.some((privilege) => privilege.includes('edit_all'))) return 'EDIT';
    return 'VIEW';
  };

}

export default initComponent(OrderListPage, withRouter);
