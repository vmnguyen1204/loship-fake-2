import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getRoute } from 'utils/routing';
import EateryRatingsPageBase from './_base';

class EateryRatingsPage extends EateryRatingsPageBase {
  render() {
    const { location: { pathname } } = this.props;
    const { match } = getRoute(null, pathname);
    if (match?.params.ratingId) {
      return (
        <div className="eatery-ratings-page">
          <div className="container">
            <h2>{this.getString('loship_community')}</h2>
            {this.props.children}
          </div>
        </div>
      );
    }

    if (!this.props.currentCity) {
      return this.renderUnsupportedCity();
    }

    return (
      <div className="eatery-ratings-page">
        <div className="container">
          <h2>{this.getString('loship_community')}</h2>
          {this.renderContent()}
        </div>
      </div>
    );
  }
}

export default initComponent(EateryRatingsPage, withRouter);
