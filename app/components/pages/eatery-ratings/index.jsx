import PropTypes from 'prop-types';
import React from 'react';
import Desktop from './_desktop';
import Mobile from './_mobile';

class EateryRatingsPage extends React.Component {
    static contextTypes = { isMobile: PropTypes.bool };

    render() {
      if (this.context.isMobile) return <Mobile {...this.props} />;
      return <Desktop {...this.props} />;
    }
}

export default EateryRatingsPage;
