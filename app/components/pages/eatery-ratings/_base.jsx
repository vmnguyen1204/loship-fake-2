import { sendGAEvent } from 'actions/factory';
import BaseComponent from 'components/base';
import SearchGlobalAddress, { LoshipNotAvailableComponent } from 'components/pages/search/_globalAddress';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryRatingItem } from 'components/shared/newsfeed/merchant-item';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { getCurrentCity, getGlobalAddress } from 'reducers/new/metadata';
import { getCurrentUser } from 'reducers/user';
import { handleLike, handleOpenPhotoPopup, handleSharing, openMerchant } from './detail/_content';

if (process.env.BROWSER) {
  require('assets/styles/pages/eatery-ratings.scss');
}

class EateryRatingsPageBase extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  }

  static defaultProps = {
    dataGetter: (state) => {
      const data = {};

      const currentUser = getCurrentUser(state);
      if (currentUser) data.currentUser = currentUser.get('data');

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data.globalAddress = globalAddress.data;

      const currentCity = getCurrentCity(state);
      if (currentCity) data.currentCity = currentCity;

      return data;
    },
    customRender: true,
  }

  componentDidMount() {
    sendGAEvent('loship_action', 'ACTION_VIEW_LOSHIPCOMMUNITY');
  }

  renderContent = () => {
    const query = {
      ...this.getQuery(),
      limit: this.context.isMobile ? 8 : 24,
      eateryRatingsApi: true,
      globalAddress: this.props.globalAddress,
    };

    return (
      <SectionNewsfeed
        params={query}
        mode={'preload|load_more|eatery-ratings'}
        submit={this.submit}
        MerchantItem={({ merchant: rating, isMobile }) => (
          <EateryRatingItem
            key={rating && rating.id}
            merchant={rating}
            isMobile={isMobile}
            onClick={openMerchant.call(this, rating && rating.eatery)}
            onViewDetail={this.viewRatingDetail(rating)}
            handleLike={handleLike.call(this, rating)}
            handleSharing={handleSharing.call(this, rating)}
            handleOpenPhotoPopup={handleOpenPhotoPopup.call(this, rating)}
          />
        )}
      />
    );
  }

  renderUnsupportedCity = () => {
    return (
      <div className="eatery-ratings-page">
        <SearchGlobalAddress />
        <LoshipNotAvailableComponent globalAddress={this.props.globalAddress} />
      </div>
    );
  }

  submit = (query) => {
    const { pathname } = this.props.location;

    const thisQuery = this.getQuery();
    const nextQuery = (() => {
      if (query.q && thisQuery.q !== query.q) return { ...thisQuery, ...query, page: undefined };
      return { ...thisQuery, ...query };
    })();

    if ((thisQuery.page || nextQuery.page) && parseInt(thisQuery.page) !== nextQuery.page) {
      return this.props.history.push(`${pathname}?${qs.stringify(nextQuery)}`);
    }

    const elem = document.getElementById('screen');
    elem.scrollTop = 0;
  };

  viewRatingDetail = (rating) => () => {
    if (!rating) return;
    this.props.dispatch({
      type: 'EATERY_RATING_FETCH',
      key: `${rating.id}.eateryRating`,
      data: JSON.parse(JSON.stringify(rating)),
    });
    this.props.history.push(`/cong-dong-loship/${rating.id}`);
  }
}

export default EateryRatingsPageBase;
