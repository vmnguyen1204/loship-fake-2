import MobileNav from 'components/shared/mobile-nav';
import initComponent from 'lib/initComponent';
import React from 'react';
import { getRoute } from 'utils/routing';
import EateryRatingsPageBase from './_base';

class EateryRatingsPage extends EateryRatingsPageBase {
  render() {
    const { location: { pathname } } = this.props;
    const { match } = getRoute(null, pathname);
    const shouldHideBackArrow = ['viettelpay'].includes(this.context.partnerName);

    if (match?.params.ratingId) {
      return (
        <div className="eatery-ratings-page">
          <MobileNav title={this.getString('loship_community')} backHandler={shouldHideBackArrow ? null : this.handleBack} />
          {this.props.children}
        </div>
      );
    }

    if (!this.props.currentCity) {
      return (
        <div className="eatery-ratings-page">
          <MobileNav title={this.getString('loship_community')} backHandler={shouldHideBackArrow ? null : this.handleBack} />
          {this.renderUnsupportedCity()}
        </div>
      );
    }
    return (
      <div className="eatery-ratings-page">
        <MobileNav title={this.getString('loship_community')} backHandler={shouldHideBackArrow ? null : this.handleBack} />
        {this.renderContent()}
      </div>
    );
  }

  handleBack = () => {
    this.props.history.goBack();
  };
}

export default initComponent(EateryRatingsPage);
