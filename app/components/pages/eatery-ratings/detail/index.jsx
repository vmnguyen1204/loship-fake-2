import { fetchEateryRatingById } from 'actions/newsfeed';
import BaseComponent from 'components/base';
import React from 'react';
import Content from './_content';

class EateryRatingDetailPage extends BaseComponent {
  static needs = [fetchEateryRatingById]

  render() {
    return <Content {...this.props} />;
  }
}

export default EateryRatingDetailPage;
