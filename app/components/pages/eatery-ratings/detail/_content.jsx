import { deleteLikeEateryRating, fetchEateryRatingById, updateLikeEateryRating } from 'actions/newsfeed';
import { CUSTOM_CONTENT, LOGIN, SHARING_POPUP, open } from 'actions/popup';
import BasePage from 'components/pages/base';
import Carousel from 'components/shared/carousel';
import { EateryRatingItem } from 'components/shared/newsfeed/merchant-item';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { toast } from 'react-toastify';
import { getGlobalAddress } from 'reducers/new/metadata';
import { getEateryRatingById, getKey, parseQuery } from 'reducers/newsfeed';
import { getCurrentUser } from 'reducers/user';
import * as SEOTYPE from 'settings/seo-types';
import { LINK_DOMAIN_LOSHIP } from 'settings/variables';
import { copyToClipboard } from 'utils/content';
import { produce } from 'utils/helper';

class EateryRatingDetailPage extends BasePage {
  static contextTypes = {
    isMobile: PropTypes.bool,
  }

  static defaultProps = {
    actionCreator: fetchEateryRatingById,
    dataGetter: (state, params) => {
      let data = {};

      const rating = getEateryRatingById(state, params);
      if (rating) data = { ...data, ...rating.toJS() };

      const currentUser = getCurrentUser(state);
      if (currentUser) data.currentUser = currentUser.get('data');

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data.globalAddress = globalAddress.data;


      return data;
    },
    customRender: true,
    getMetadata: (props, seoOptions) => {
      return produce(seoOptions, (draft) => {
        draft.type = SEOTYPE.EATERY_RATING;
        draft.data = props.data;
      });
    },
  }

  render() {
    const rating = this.props.data;

    return (
      <div className="section-newsfeed eatery-ratings">
        <div className="content">
          <div className="list-view">
            <EateryRatingItem
              merchant={rating}
              isMobile={this.context.isMobile}
              onClick={openMerchant.call(this, rating && rating.eatery)}
              handleLike={handleLike.call(this, rating)}
              handleSharing={handleSharing.call(this, rating)}
              handleOpenPhotoPopup={handleOpenPhotoPopup.call(this, rating)}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(EateryRatingDetailPage);

export function openMerchant(merchant) {
  if (!this || !this.props || !merchant) return;
  return (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.props.history.push(`/${merchant.username || `b/${merchant.slug}`}`);
  };
}

export function handleLike(rating) {
  if (!this || !this.props || !rating) return;
  return () => {
    if (this.state.isLikePending) return;
    if (!this.props.currentUser) {
      this.props.dispatch(open(LOGIN));
    }

    const isLiked = !rating.liked;
    const handler = isLiked ? updateLikeEateryRating : deleteLikeEateryRating;

    this.setState({ isLikePending: true }, () => {
      this.props.dispatch(handler({
        ratingId: rating.id,
        currentKey: getKey(parseQuery({
          ...this.getQuery(),
          limit: this.context.isMobile ? 8 : 24,
          eateryRatingsApi: true,
          globalAddress: this.props.globalAddress,
        })),
        callback: () => this.setState({ isLikePending: false }),
        callbackFailure: () => this.setState({ isLikePending: false }),
      }));
    });
  };
}

export function handleSharing(rating) {
  if (!this || !this.props || !rating) return;
  return () => {
    const sharingUrl = LINK_DOMAIN_LOSHIP + `/danh-gia/${rating.id}`;

    copyToClipboard(sharingUrl);
    toast(this.getString('copied_link'));

    this.props.dispatch(
      open(SHARING_POPUP, {
        subject: this.getString('buy_on_loship', 'order'),
        body: sharingUrl,
        url: sharingUrl,
      }),
    );
  };
}

export function handleOpenPhotoPopup(rating) {
  if (!this || !this.props || !rating) return;
  return (photo) => {
    const { photos = [] } = rating;
    this.props.dispatch(
      open(CUSTOM_CONTENT, {
        contentClassName: 'mc-photos-popup slider',
        content: () => (
          <Carousel
            className="mc-photos-slider"
            data={photos.map((cPhoto) => ({ key: cPhoto, image: cPhoto }))}
            currentSlide={photo}
            slideClassName="mc-photo-item"
            slidesToShow={1}
            dot={false}
            centerMode={false}
            autoplay={false}
            imgWidth={Math.min(window.innerWidth - 64, 800)}
            ratio={4 / 3}
            photoZoom
          />
        ),
        wrapperStyle: { zIndex: 201 },
      }),
    );
  };
}
