import React from 'react';

class PartnerPage extends React.Component {
  render() {
    if (!this.props.children) return null;
    return this.props.children;
  }
}

export default PartnerPage;
