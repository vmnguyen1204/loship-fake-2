import BasePage from 'components/pages/base';
import initComponent from 'lib/initComponent';
import React from 'react';
import { getPartnerConfig } from 'reducers/metadata';
import { copyToClipboard } from 'utils/content';

if (process.env.BROWSER) {
  require('assets/styles/pages/gateway.scss');
}

class GatewayAuthPage extends BasePage {
  static defaultProps = {
    dataGetter: getPartnerConfig,
    customRender: true,
  };

  state = { isAuthorized: true };

  async componentDidMount() {
    const { data } = this.props;
    if (!data || !['momo', 'sacombankpay', 'viettelpay'].includes(data.get('partner')) || !data.get('signature')) {
      return this.setState({ isAuthorized: false });
    }
    return this.props.history.push('/');
  }

  render() {
    if (this.state.isAuthorized) return null;

    return (
      <div className="container">
        <div className="gateway-err">
          <div className="alone-message">
            <h3>
              {this.getString('Lỗi truy cập')}
              <span onClick={this.handleViewCurrentUrl}>.</span>
            </h3>
            <p>{this.getString('Xin lỗi quý khách, đã có lỗi xảy ra dẫn đến việc quý khách không thể đặt hàng được.')}</p>
            <p>
              {this.getString('Vui lòng liên hệ email ')}
              <a href="mailto:hotroloship@lozi.vn" title={this.getString('Liên hệ Loship')} target="_blank" rel="noopener">
              hotroloship@lozi.vn
              </a>
              {this.getString(' để được hỗ trợ.')}
            </p>
            <p>
              {this.getString('Loship biết quý khách có nhiều lựa chọn. Cảm ơn quý khách đã chọn Loship ngày hôm nay.')}
            </p>
          </div>
        </div>
      </div>
    );
  }
  handleViewCurrentUrl = () => {
    const currentUrl = window.location.href;
    copyToClipboard(currentUrl);
  }
}

export default initComponent(GatewayAuthPage);
