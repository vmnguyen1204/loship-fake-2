import BasePage from 'components/pages/base';
import initComponent from 'lib/initComponent';
import React from 'react';
import GatewayAuthOrderPageContent from './_content';

if (process.env.BROWSER) {
  require('assets/styles/pages/order.scss');
  require('assets/styles/pages/order-mobile.scss');
  require('assets/styles/pages/order-history.scss');
  require('assets/styles/pages/order-history-mobile.scss');
}

class GatewayAuthOrderPage extends BasePage {
  render() {
    return <GatewayAuthOrderPageContent {...this.props} params={this.getParams()} />;
  }
}

export default initComponent(GatewayAuthOrderPage);
