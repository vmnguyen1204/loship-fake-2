import cx from 'classnames';
import MobileNavbar from 'components/shared/mobile-nav';
import OrderDetailBase from 'components/shared/order/detail/detail-base';
import OrderDebt from 'components/shared/order/payment/debt-order';
import initComponent from 'lib/initComponent';
import React from 'react';
import { getProcessStep } from 'utils/cart';

class OrderDetailComponent extends OrderDetailBase {
  render() {
    const { data, status: loadingStatus, location } = this.props;
    const { step } = this.state;
    const params = this.getParams();

    if (!data && !loadingStatus) return null;
    if (loadingStatus === 'PENDING') return this.renderLoadingPage();
    if (loadingStatus !== 'SUCCESS') return this.renderErrorPage();

    const status = data.get('status');
    const processStep = getProcessStep(status);
    const hideNavbar = location.pathname.includes('/m/tai-khoan/don-hang');
    const isOnPartnerWebview = !!params.partnerName || this.context.partnerName !== 'none';

    if (step === 'detail') {
      return (
        <div className="order-page">
          <div className="order-step-detail detail">
            <MobileNavbar
              className={cx({ red: isOnPartnerWebview })}
              backHandler={this.changeStep('overview')}
              title={this.getString('order_detail_0', '', [data.get('code')])}
            />
            <OrderDebt />
            {this.renderOrderInfo()}
          </div>
        </div>
      );
    }

    return (
      <div className="order-page">
        <div className={cx('order-step-detail', { cancel: processStep === -1, done: processStep === 3 })}>
          {!hideNavbar && (
            <MobileNavbar
              className={cx({ red: isOnPartnerWebview })}
              backHandler={!isOnPartnerWebview ? () => this.props.history.push('/tai-khoan/don-hang') : undefined}
              showLogo={isOnPartnerWebview}
              title={
                !isOnPartnerWebview ? (
                  this.getString('order_0', '', [data.get('code')])
                ) : (
                  <div className="information-code">
                    <span className="label">{this.getString('order_code')}</span>
                    <b>#{data.get('code')}</b>
                  </div>
                )
              }
              actionText={this.getString('cancel_order', 'order')}
              action={data.get('isCancellable') && this.cancelOrder}
            />
          )}

          {this.renderShippingProcessComplete(processStep)}
          <OrderDebt />
          {this.renderShippingInfo()}
          {this.renderPaymentUnsuccessful()}
          {this.renderShippingProcess(processStep)}

          {this.renderShipperInfo(data.get('shipper'), processStep)}

          {this.renderRefundInfo()}
          {this.renderContactInfo()}
        </div>
      </div>
    );
  }
}

export default initComponent(OrderDetailComponent);
