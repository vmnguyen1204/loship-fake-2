import { LINK_API_MANAGER, LINK_AUTH } from 'settings/variables';

import BaseComponent from 'components/base';
import { API as MerchantManagerAPI } from 'actions/merchant-manager';
import React from 'react';
import Spinner from 'components/statics/spinner';
import callAPI from 'actions/helpers';
import Finish from './steps/_finish';
import CreateMenu from './steps/_create-menu';

const STEPS = {
  WAIT: Symbol('WAIT'),
  CREATE_USERNAME: Symbol('CREATE_USERNAME'),
  CREATE_MENU: Symbol('CREATE_MENU'),
  FINISH: Symbol('FINISH'),
};

function parseMerchantData(data) {
  const result = {
    id: data.id,
    avatar: data.avatar,
    name: data.name,
    address: data.address.full,
    cityId: data.cityId,
    slug: data.slug,
    lat: data.latitude,
    lng: data.longitude,
    username: data.username,
    isManager: data.isManager,
  };
  return result;
}

class PartnerTrialPageContent extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isSubmitting: false,
      error: '',
      merchantData: null,
      step: STEPS.WAIT,
    };
  }

  componentDidMount() {
    this.initMerchantInfo();
  }

  fallback = () => {
    // if (this.fallbackTimeOut) clearTimeout(this.fallbackTimeOut);
    // this.fallbackTimeOut = setTimeout(() => {
    //   window.location = LINK_DOMAIN_LOSHIP + '/?utm_source=partner-trial&utm_medium=' + statusCode;
    // }, 1000);
  };

  initMerchantInfo = async () => {
    const { eaterySlug } = this.props;

    // 1. FETCH MERCHANT DATA
    if (!this.state.merchantData) {
      const resMerchantInfo = await callAPI('get', {
        url: LINK_AUTH + '/eateries/slug:${eaterySlug}',
        params: { eaterySlug },
      });

      if (resMerchantInfo.status !== 200) {
        return this.setState(
          { error: 'Địa điểm không tồn tại' },
          this.fallback.bind(this, resMerchantInfo.status),
        );
      }

      const merchantData = parseMerchantData(resMerchantInfo.body.data);

      // 2. VALIDATE MERCHANT
      // Rule 1: createdAt within 1 hour
      // Rule 2: has no manager
      // const isAllowedToContinue = true;

      // if (!isAllowedToContinue) {
      //   // Should redirect to merchant page as if nothing happened!
      //   return this.setState(
      //     {
      //       error: 'Bạn không có quyền truy cập địa điểm này',
      //     },
      //     this.fallback.bind(this, 403),
      //   );
      // }

      // 3. CLAIM TRIAL OWNER IF NOT OWNED
      if (!merchantData.isManager) {
        const resClaimOwner = await callAPI('post', {
          url: LINK_AUTH + '/eateries/${eateryId}/claimTrialOwner',
          params: { eateryId: merchantData.id },
        });

        if (resClaimOwner.status !== 200) {
          // Should redirect to merchant page as if nothing happened!
          return this.setState({ error: 'Lỗi 400. Vui lòng liên hệ bộ phận hỗ trợ qua email hotroloship@lozi.vn' });
        }
      }

      // FINISH INITIALIZING
      this.setState({
        merchantData,
        isLoading: false,
        step: STEPS.CREATE_MENU,
      });
    }
  };

  handleUpdateMerchantAvatar = (payload = {}) => {
    const merchantId = this.state.merchantData.id;
    if (!merchantId) return false;
    return callAPI('put', {
      url: MerchantManagerAPI.UPDATE_AVATAR,
      params: { merchant: merchantId },
      content: { image: payload.uploadedImageName },
    });
  };

  handleCreateMenu = (payload = {}) => {
    return this.setState(
      { isSubmitting: true },
      async () => {
        await this._createMenu(payload);
        typeof payload.callback === 'function' && payload.callback();
      },
    );
  };

  _createMenu = async (payload = {}) => {
    try {
      const eateryId = this.state.merchantData.id;
      const { groupDishName, dishName, dishPrice, dishImage } = payload;

      // CREATE THE FIRST GROUPDISH
      const resCreateGroupDish = await callAPI('post', {
        url: LINK_API_MANAGER + '/eateries/${eateryId}/menu/groups',
        params: { eateryId },
        content: {
          name: groupDishName,
          active: true,
        },
      });

      const groupDishData = resCreateGroupDish.body.data;
      const groupDishId = groupDishData.id;

      // SET GROUPDISH ACTIVE
      await callAPI('put', {
        url: LINK_API_MANAGER + '/eateries/${eateryId}/menu/groups/${groupDishId}',
        params: { eateryId, groupDishId },
        content: {
          ...groupDishData,
          active: true,
        },
      });

      // CREATE THE FIRST DISH
      const resCreateDish = await callAPI('post', {
        url: LINK_API_MANAGER + '/eateries/${eateryId}/menu/dishes',
        params: { eateryId },
        content: {
          name: dishName,
          price: parseInt(dishPrice),
          customs: [],
          image: dishImage,
          groupDishId,
          active: true,
        },
      });

      const dishData = { ...resCreateDish.body.data };
      delete dishData.image;
      const dishId = dishData.id;

      // SET DISH ACTIVE
      await callAPI('put', {
        url: LINK_API_MANAGER + '/eateries/${eateryId}/menu/dishes/${dishId}',
        params: { eateryId, dishId },
        content: {
          ...dishData,
          groupDishId,
          active: true,
        },
      });

      this.setState({ step: STEPS.FINISH });
    } catch (e) {
      console.error(e);
      this.setState({
        isSubmitting: false,
        error: 'Lỗi 4001. Vui lòng liên hệ bộ phận hỗ trợ qua email hotroloship@lozi.vn',
      });
    }
  };

  render() {
    if (this.state.error) return <div className="partner-trial-content">{this.state.error}</div>;
    if (this.state.isLoading) {
      return (
        <div className="partner-trial-content">
          <Spinner type="wave">
            <span className="ml-8">{this.getString('loading')}</span>
          </Spinner>
        </div>
      );
    }
    const { merchantData, step, isSubmitting } = this.state;

    return (
      <div className="partner-trial-content">
        <h1 className="text-center">Mở cửa hàng trên Loship trong 2 phút</h1>
        {step === STEPS.CREATE_MENU && (
          <CreateMenu
            handleCreateMenu={this.handleCreateMenu}
            handleUpdateMerchantAvatar={this.handleUpdateMerchantAvatar}
            merchantName={merchantData.name}
            merchantCityId={merchantData.cityId}
            isSubmitting={isSubmitting}
          />
        )}
        {step === STEPS.FINISH && <Finish merchantData={merchantData} source={this.props.source} />}
      </div>
    );
  }
}

export default PartnerTrialPageContent;
