import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { addCommas, removeCommas } from 'utils/format';
import { MIN_MAIN_DISH_PRICE } from 'utils//menu-tool';
import Button from 'components/shared/button';
import FileUploader from 'components/statics/uploader';

const ACCOUNT_PHONE = {
  1: {
    name: 'Trần Minh Sơn',
    phoneNumber: '0839.334.264',
  },
  50: {
    name: 'Nhật Phương',
    phoneNumber: '0944.428.880',
  },
  32: {
    name: 'Nhật Phương',
    phoneNumber: '0944.428.880',
  },
  59: {
    name: 'Nhật Phương',
    phoneNumber: '0944.428.880',
  },
};

class CreateMenu extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      dishImage: null,
      dishThumbnail: null,
      avatar: null,
      avatarThumbnail: null,
    };
  }

  render() {
    return (
      <div className="create-menu">
        <div className="_row">
          <div className="_col-50">
            <div className="form">
              <h3>Bước 1: Chọn ảnh đại diện cho cửa hàng</h3>
              <p>Giống như bảng hiệu, ảnh đại diện là &quot;bộ mặt&quot; cửa cửa hàng. Hãy chọn ảnh thật đẹp nhé!</p>

              {!this.state.avatarThumbnail && (
                <FileUploader
                  className="btn-upload-wrapper"
                  onChange={this.handleSelectAvatar}
                  accept="image/*"
                  type="avatar"
                >
                  <Button>Chọn ảnh từ máy tính</Button>
                </FileUploader>
              )}
              {!!this.state.avatarThumbnail && (
                <div className="thumbnail" style={{ backgroundImage: `url(${this.state.avatarThumbnail})` }}>
                  <a className="btn-clear-dish-image" onClick={this.clearAvatar}>
                    <i className="fa fa-times" />
                  </a>
                </div>
              )}
            </div>
            <div className="form">
              <h3>Bước 2: Thêm món đầu tiên cho menu của bạn</h3>
              <p>
                <b>{this.props.merchantName}</b> sẽ sẵn sàng để nhận đơn đặt hàng sau khi thêm món đầu tiên lên menu.
              </p>
              <div className="form-group">
                <label>Tên danh mục</label>
                <div className="input">
                  <input type="text" ref="groupdish" placeholder="Món mặn / Món tráng miệng / Bánh / ..." />
                </div>
              </div>

              <div className="form-group with-upper-separator">
                <label>Ảnh món</label>
                <div className="input">
                  {!this.state.dishThumbnail && (
                    <FileUploader
                      className="btn-upload-wrapper"
                      onChange={this.handleSelectDishImage}
                      accept="image/*"
                      type="dish"
                    >
                      <Button>Chọn ảnh từ máy tính</Button>
                    </FileUploader>
                  )}
                  {!!this.state.dishThumbnail && (
                    <div className="thumbnail" style={{ backgroundImage: `url(${this.state.dishThumbnail})` }}>
                      <a className="btn-clear-dish-image" onClick={this.clearDishImage}>
                        <i className="fa fa-times" />
                      </a>
                    </div>
                  )}
                </div>
              </div>

              <div className="form-group">
                <label>Tên món</label>
                <div className="input">
                  <input type="text" ref="dishname" placeholder="Trà sữa trân châu / Bánh bột lọc (1kg) / ..." />
                </div>
              </div>
              <div className="form-group">
                <label>Giá bán (VND)</label>
                <div className="input">
                  <input
                    type="text"
                    ref="dishprice"
                    onChange={(e) => {
                      this.refs.dishprice.value = addCommas(removeCommas(e.target.value));
                    }}
                  />
                </div>
              </div>
            </div>
            {!!this.state.error && <div className="form-message">{this.state.error}</div>}
            <div className="form-action">
              <Button type="primary" onClick={this.createMenu} disabled={this.state.isSending}>
                <span>Bắt đầu bán hàng trên Loship ngay</span>
                <i className="fa fa-arrow-right" />
              </Button>
            </div>
          </div>
          <div className="_col-50 text-center">
            <img
              src="/dist/images/partner-demo-with-info.jpg"
              alt="hiển thị menu trên app Loship"
              className="demo-image"
            />
          </div>
        </div>
        <div className="_row">
          <div className="_col-100">
            <div className="section-help">
              <h4>Bạn cần trợ giúp?</h4>
              <ul>
                <li>
                  Xem{' '}
                  <a href="https://www.youtube.com/watch?v=3KM6voUnCh0" target="_blank" rel="noopener">
                    Video: Hướng dẫn mở cửa hàng trên Loship và tự quản lý menu
                  </a>
                  .
                </li>
                <li>
                  hoặc gọi hotline trợ giúp: {ACCOUNT_PHONE[this.props.merchantCityId].phoneNumber} (
                  {ACCOUNT_PHONE[this.props.merchantCityId].name})
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleSelectDishImage = (data, thumb) => {
    this.setState({ dishImage: data, dishThumbnail: thumb });
  };

  handleSelectAvatar = (data, thumb) => {
    this.setState({ avatar: data, avatarThumbnail: thumb });
  };

  clearDishImage = () => this.setState({
    dishImage: null,
    dishThumbnail: null,
  });

  clearAvatar = () => this.setState({
    avatar: null,
    avatarThumbnail: null,
  });

  createMenu = () => {
    const { groupdish, dishname, dishprice } = this.refs;
    const groupDishName = groupdish.value.trim();
    const dishName = dishname.value.trim();
    const dishPrice = dishprice.value.trim();
    if (!groupDishName || !dishName || !dishPrice) return this.setState({ error: 'Bạn cần điền đầy đủ thông tin để tạo menu.' });
    if (removeCommas(dishPrice) < MIN_MAIN_DISH_PRICE) return this.setState({ error: 'Giá bán không được thấp hơn ' + addCommas(MIN_MAIN_DISH_PRICE) + 'đ.' });
    const { dishImage, avatarThumbnail } = this.state;
    if (!dishImage) return this.setState({ error: 'Hãy chọn hình ảnh cho sản phẩm của bạn nhé.' });
    if (!avatarThumbnail) return this.setState({ error: 'Hãy chọn ảnh đại diện cho cửa hàng của bạn.' });

    this.setState({ isSending: true }, async () => {
      await this.props.handleUpdateMerchantAvatar({ uploadedImageName: await this.state.avatar.getURL() });

      const dishImageURL = await this.state.dishImage.getURL();

      this.props.handleCreateMenu({
        groupDishName,
        dishName,
        dishPrice: removeCommas(dishPrice),
        dishImage: dishImageURL,
        callback: () => this.setState({ isSending: false }),
      });
    });
  };
}

CreateMenu.propTypes = {
  handleUpdateMerchantAvatar: PropTypes.func.isRequired,
  handleCreateMenu: PropTypes.func.isRequired,
  merchantName: PropTypes.string.isRequired,
  merchantCityId: PropTypes.number,
};

CreateMenu.defaultProps = { merchantCityId: 50 };

export default CreateMenu;
