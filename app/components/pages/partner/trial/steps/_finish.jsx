import React, { PureComponent } from 'react';
import Button from 'components/shared/button';

class Finish extends PureComponent {
  render() {
    const merchantData = this.props.merchantData;
    const source = this.props.source;
    let merchantLink = '/b/' + merchantData.slug + '?utm_source=partner-trial&utm_medium=finish';
    if (source === 'lomart') merchantLink = 'https://lomart.vn' + merchantLink;
    return (
      <div className="finish">
        <h2>Cửa hàng đã sẵn sàng!</h2>
        <p>
          <b>{merchantData.name}</b> đã sẵn sàng để bán trên Loship!
        </p>
        <p>
          <Button type="feature" href={merchantLink}>
            <span>Xem cửa hàng của bạn ngay</span> <i className="fa fa-chevron-right" />
          </Button>
        </p>
        <p>
          Xem thêm{' '}
          <a href="https://www.youtube.com/watch?v=3KM6voUnCh0" target="_blank" rel="noopener">
            {' '}
            Video: Hướng dẫn quản lý menu cho cửa hàng trên Loship
          </a>
        </p>
      </div>
    );
  }
}

Finish.propTypes = {};

export default Finish;
