import React, { PureComponent } from 'react';

class CreateUsername extends PureComponent {
  render() {
    return (
      <div className="create-username">
        <h2>Chúc mừng bạn tạo địa điểm thành công</h2>
        <p>Tạo username nhé:</p>
        <div>
          <input type="text" ref="username" />
          <button type="button" onClick={this.createUsername}>Tạo username</button>
        </div>
      </div>
    );
  }

  createUsername = () => {
    this.props.handleCreateUsername(this.refs.username.value);
  };
}

CreateUsername.propTypes = {};

export default CreateUsername;
