import React from 'react';

import BasePage from 'components/pages/base';
import * as SEOTYPE from 'settings/seo-types';
import AuthButton from 'components/shared/button/auth';
import UserPanel from 'components/shared/navbar/panel-user';
import PartnerTrialContent from './_content';

if (process.env.BROWSER) {
  require('assets/styles/pages/partner-trial.scss');
}

const getServiceMembershipPlan = (serviceName = 'loship') => {
  switch (serviceName) {
    case 'lozat':
      return {
        commissionValue: '30%',
        joinFee: 0,
      };
    case 'loship':
    case 'lomart':
    case 'lomed':
    case 'lopet':
    case 'lohoa':
    default:
      return {
        commissionValue: '25%',
        joinFee: 0,
      };
  }
};

class PartnerTrialPage extends BasePage {
  constructor(props, context) {
    super(props, {
      ...context,
      seo: { type: SEOTYPE.PARTNER_TRIAL },
    });
    this.state = { showContent: false };
  }

  showContent = () => this.setState({ showContent: true });

  render() {
    const query = this.getQuery();
    if (!query.slug) return false;

    const emailAddress = `mailto:hotroloship@lozi.vn?subject=[LIÊN HỆ HỢP TÁC] Cần trợ giúp mở cửa hàng&body=Tôi cần trợ giúp mở cửa hàng Loship (Link Lozi: https://lozi.vn/b/${query.slug})`;
    const membershipPlan = getServiceMembershipPlan(query.source);

    return (
      <div className="page partner-trial">
        <div className="navbar">
          <div className="container">
            <div className="logo" />
            <div className="blank" />
            <UserPanel />
          </div>
        </div>
        {!this.state.showContent && (
          <div className="partner-trial-content">
            <h1 className="text-center">Mở cửa hàng trên Loship trong 2 phút</h1>
            <div className="_row">
              <div className="_col-50">
                <h2>Lợi ích của Loship</h2>
                <ul>
                  <li>Có ứng dụng dành cho chủ cửa hàng nhận và theo dõi đơn hàng.</li>
                  <li>Công cụ tạo và quản lý menu của cửa hàng trên Loship.</li>
                  <li>Shipper của Loship sẽ trả tiền ngay khi đến mua hàng.</li>
                  <li>Miễn phí ship hoàn toàn cho khách hàng của bạn.</li>
                </ul>
                <h2>Điều kiện mở cửa hàng trên Loship</h2>
                <p>Để bắt đầu bán hàng được trên Loship, bạn cần tạo menu cho cửa hàng.</p>
                <p>
                  Bằng việc tiếp tục tạo menu, bạn đồng ý rằng sẽ chiết khấu cho Loship trên mỗi đơn hàng thành công là{' '}
                  {membershipPlan.commissionValue} và các đơn hàng sẽ áp dụng chế độ Thu tiền sau (*)
                </p>
                <p>
                  Thu tiền sau: Bạn không thu tiền tài xế Loship đến mua, mà tiền hàng được cộng trực tiếp vào tài khoản
                  của cửa hàng, Loship sẽ chuyển lại tiền hàng này cho bạn qua ngân hàng.
                </p>

                <div className="text-center">
                  <AuthButton type="feature" onClick={this.showContent} onLoginSuccess={this.showContent}>
                    Tôi đồng ý tham gia, tiếp tục tạo menu!
                  </AuthButton>
                </div>
              </div>
              <div className="_col-50 text-center">
                <img src="/dist/images/partner-demo.jpg" alt="hiển thị menu trên app Loship" className="demo-image" />
              </div>
            </div>
          </div>
        )}
        {this.state.showContent && <PartnerTrialContent eaterySlug={query.slug} source={query.source} />}
        <div className="footer-help">
          Mọi vấn đề hay thắc mắc, quý đối tác vui lòng liên hệ bộ phận CSKH qua email:{' '}
          <a href={emailAddress} title="Gửi email cho CSKH">
            hotroloship@lozi.vn
          </a>
        </div>
      </div>
    );
  }
}

export default PartnerTrialPage;
