import React from 'react';
import initComponent from 'lib/initComponent';
import BasePage from 'components/pages/base';
import { fetchMoreVideo, fetchTopVideo, fetchVideo } from 'actions/discover';
import { getTopVideoMerchant, getVideoMerchant } from 'reducers/discover';
import { Map } from 'immutable';
import { Waypoint } from 'react-waypoint';
import { withRouter } from 'react-router-dom';
import VideoItem from 'components/pages/discover/_item-video';
import TrendingVideoItem from 'components/pages/discover/_item-trending-video';
import { sendGAEvent } from 'actions/factory';
import { getGlobalAddress } from 'reducers/new/metadata';

if (process.env.BROWSER) {
  require('assets/styles/pages/discover.scss');
  require('assets/styles/pages/discover-mobile.scss');
}

// TODO: globalAddress Discover not working
class Discover extends BasePage {
  constructor(props) {
    super(props);
    this.state = { data: {}, checkTrending: false, beforePlayYT: '', afterPlayYT: '' };
  }

  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      const listVideos = getVideoMerchant(state);
      if (!listVideos) {
        return data;
      }
      data = data.merge(listVideos);
      const topData = getTopVideoMerchant(state);
      if (topData) {
        data = data.set('topData', topData);
      }
      return data;
    },
    customRender: true,
  };

  componentDidMount() {
    this.handleCoreData();
  }

  componentDidUpdate() {
    if (this.state.checkTrending) {
      const elmnt = document.getElementById('trendingVideo');
      elmnt.scrollIntoView();
    }
    this.handleCoreData();
  }

  handlePauseVideo = (videoId, typeVideo) => {
    if (this.state.afterPlayYT === '') {
      this.setState({ afterPlayYT: videoId, type: typeVideo });
    } else {
      this.setState((state) => ({
        beforePlayYT: { id: state.afterPlayYT, type: state.type },
        afterPlayYT: videoId,
        type: typeVideo,
      }));
    }
  };

  handlePlayVideo = (videoId, typeVideo) => {
    this.handlePauseVideo(videoId, typeVideo);
    this.setState((state) => ({ data: { ...state.data, [videoId]: true }, checkTrending: false }));
    sendGAEvent('loship_performance', 'video', 'play');
  };

  handlePlayVideoTrending = (videoId, typeVideo) => {
    const result = this.props.topData
      .get('data')
      .toArray()
      .find((item) => item.id === videoId);
    this.handlePauseVideo(videoId, typeVideo);
    this.setState((state) => ({ data: { ...state.data, dataTrending: result, [videoId]: true }, checkTrending: true }));
    sendGAEvent('loship_performance', 'video', 'play_trending');
  };

  handleCoreData = () => {
    const { globalAddress } = this.props;
    if (!globalAddress || !globalAddress.cityId || !globalAddress.lat || !globalAddress.lng || !globalAddress.address) return;

    const city = {
      cityId: globalAddress.cityId,
      districtId: globalAddress.districtId,
    };

    if (!this.props.data) {
      this.props.dispatch(
        fetchVideo({
          limit: 10,
          ...city,
        }),
      );
    }

    if (!this.props.topData?.get('data')) {
      this.props.dispatch(
        fetchTopVideo({
          limit: 6,
          ...city,
        }),
      );
    }
  }

  renderAppBanner = () => {
    return (
      <div className="download-app-panel">
        <div className="download-app-card">
          <a href="https://lozi.vn/d/dich-vu-quang-cao/" target="_blank" rel="noopener">
            <img className="download-app-img" src="/dist/images/download-app.jpg" alt="download-app" />
          </a>
          <div className="download-app-content">
            <p className="title">{this.getString('loship_the_most_favorite_app_for_selling_your_items')}</p>
            <p className="download">{this.getString('free_download_now')}</p>
            <div className="promo-btns">
              <a
                href="http://bit.ly/LOSHIPoniOS"
                className="app-img app-ios"
                target="_blank"
                rel="noopener"
                onClick={() => {
                  sendGAEvent('loship_performance', 'app_ios_download', 'newsfeed_video');
                }}
              >
                <img src="/dist/images/app-ios.png" alt="ios" />
              </a>
              <a
                href="http://bit.ly/LOSHIPonAndroid"
                className="app-img app-android"
                target="_blank"
                rel="noopener"
                onClick={() => {
                  sendGAEvent('loship_performance', 'app_android_download', 'newsfeed_video');
                }}
              >
                <img src="/dist/images/app-android.png" alt="android" />
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  };

  renderTrending = (topVideos) => {
    if (!topVideos || typeof topVideos.get !== 'function' || !topVideos.get('data')) return <div />;
    return (
      <div className="trending-panel">
        <h3>{this.getString('trending_videos')}</h3>
        <div className="trending-scroll">
          {topVideos
            .get('data')
            .toArray()
            .map((vid) => {
              return (
                <TrendingVideoItem
                  beforePlayYT={this.state.beforePlayYT}
                  data={vid}
                  onPlay={this.handlePlayVideoTrending}
                  key={vid.id}
                />
              );
            })}
        </div>
      </div>
    );
  };


  _handleWaypointEnter = () => {
    const extras = this.props.extras;
    const nextUrl = extras && extras.get('nextUrl');
    if (nextUrl) {
      this.setState({}, () => {
        this.props.dispatch(fetchMoreVideo(nextUrl));
      });
    }
  };

  _handleWaypointLeave = () => {};

  renderContent = (listVideo) => {
    if (!listVideo || typeof listVideo.toArray !== 'function') return <div />;

    let iframeSize = { width: '480', height: '480' };
    if (this.context.isMobile) iframeSize = { width: '100%', height: '286' };

    const trendingVideo = this.state.data.dataTrending;

    return (
      <div>
        <h3>{this.getString('latest_video')}</h3>
        {trendingVideo && (
          <VideoItem
            beforePlayYT={this.state.beforePlayYT}
            data={trendingVideo}
            isPlaying={this.state.data[trendingVideo.id]}
            getString={this.getString}
            iframeSize={iframeSize}
          />
        )}

        {listVideo.toArray().map((vid) => {
          return (
            <VideoItem
              beforePlayYT={this.state.beforePlayYT}
              data={vid}
              isPlaying={this.state.data[vid.id]}
              onPlay={this.handlePlayVideo}
              getString={this.getString}
              iframeSize={iframeSize}
              key={vid.id}
            />
          );
        })}
        <Waypoint onEnter={this._handleWaypointEnter} onLeave={this._handleWaypointLeave} scrollableAncestor="window" />
      </div>
    );
  };

  render() {
    const { data: listVideo, topData: topVideos } = this.props;
    return (
      <div className="page-discover">
        <div className="container">
          {this.renderAppBanner()}
          <div className="detail-content detail-panel">{this.renderContent(listVideo)}</div>
          {this.renderTrending(topVideos)}
        </div>
      </div>
    );
  }
}

export default initComponent(Discover, withRouter);
