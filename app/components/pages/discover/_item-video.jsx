import React, { Component } from 'react';
import YouTube from 'react-youtube';
import { getImageUrl } from 'utils/image';
import Button from 'components/shared/button';

class VideoItem extends Component {
  componentDidUpdate() {
    if (this.props.beforePlayYT.type === 'youtube' && this.props.beforePlayYT.id !== '') {
      document
        .getElementById('playYT-' + this.props.beforePlayYT.id)
        .contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
    }
  }


  render() {
    const { data, getString, iframeSize, onPlay, isPlaying } = this.props;
    const merchantLink = data.eatery.username ? `/${data.eatery.username}` : `/b/${data.eatery.slug}`;
    const options = {
      height: iframeSize.height,
      width: iframeSize.width,
      playerVars: { autoplay: true },
    };
    return (
      <div className="section">
        <div className="block-1">
          <p className="title">{data.name}</p>
        </div>
        <div className="detail-img">
          {isPlaying ? (
            this.autoPlayVideo(data, options)
          ) : (
            <a
              onClick={() => onPlay(data.id, data.sources[0].type)}
              style={{ backgroundImage: `url(${getImageUrl(data.thumbnail, 480)})` }}
            >
              <div className="dark-bg">
                <div name="linkvideo" className="btn-active" />
              </div>
            </a>
          )}
        </div>
        <div className="loship-eatery">
          <div className="eatery-avatar" style={{ backgroundImage: `url(${data.eatery.avatar})` }} />
          <div className="eatery-info">
            <p className="name">{data.eatery.name}</p>
            <p className="address">{data.eatery.address && data.eatery.address.full}</p>
          </div>
          <div className="eatery-action">
            <Button type="primary" to={`${merchantLink}?utm_source=loship_discover_page&utm_medium=video_item`}>
              <span>{getString('see_menu_now')}</span>
              <i className="fa fa-arrow-right" />
            </Button>
          </div>
        </div>
      </div>
    );
  }

  onStateChange = () => {}

  autoPlayVideo = (data, options) => {
    return (
      <div>
        {data.sources[0].type === 'youtube' ? (
          <YouTube
            id={'playYT-' + data.id}
            videoId={data.videoId}
            onStateChange={() => this.changeState}
            opts={options}
          />
        ) : (
          <iframe title="Video" src={data.sources[0].url} width={options.width} height={options.height} />
        )}
      </div>
    );
  };
}

export default VideoItem;
