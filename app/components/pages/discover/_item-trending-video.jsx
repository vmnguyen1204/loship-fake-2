import React from 'react';
import { getImageUrl } from 'utils/image';
import { compose, lifecycle } from 'recompose';

function TrendingVideoItem({ data, onPlay }) {
  return (
    <div className="trending-card" id="trendingVideo">
      <a
        style={{ backgroundImage: `url(${getImageUrl(data.thumbnail, 240)})` }}
        onClick={() => onPlay(data.id, data.sources[0].type)}
      >
        <div className="dark-bg">
          <div className="btn-active" />
        </div>
      </a>
      <p>{data.name}</p>
    </div>
  );
}

const lifecycleHandler = {};

export default compose(lifecycle(lifecycleHandler))(TrendingVideoItem);
