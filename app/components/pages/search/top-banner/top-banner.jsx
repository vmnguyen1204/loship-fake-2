import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { debouncer, iife } from 'utils/helper';
import { getClientLocale } from 'utils/i18n';

if (process.env.BROWSER) {
  require('assets/styles/components/_top-banner.scss');
}

class TopBanner extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  state = { value: '' };

  render() {
    const type = this.getPageType();
    const shouldRenderTitle = type === 'loship' || (type === 'search-loship' && !this.context.isMobile);

    const currentLocale = getClientLocale();
    const imgTitle = iife(() => {
      if (this.context.isMobile) {
        if (currentLocale === 'vi_VN') return '/dist/images/home-banner-mobile.jpg';
        return '/dist/images/home-banner-mobile-en.jpg';
      }

      if (currentLocale === 'vi_VN') return '/dist/images/home-banner.jpg';
      return '/dist/images/home-banner-en.jpg';
    });

    return (
      <div className={cx('top-banner', type)} style={shouldRenderTitle ? { backgroundImage: `url(${imgTitle})` } : {}}>
        <div className="wrapper">
          <div className="search-box">
            <Button type="link" className="btn-search" onClick={this.handleSearch}>
              <i className="lz lz-search" />
            </Button>
            <input
              type="text"
              ref={this.setInput}
              className="search-box-input"
              placeholder={this.getString('find_your_favorite_brands_of_foodsmilktea_for_loship_to_deliver')}
              value={this.state.value}
              onChange={this.handleInputChange}
              onFocus={this.handleFocus}
              onKeyUp={this.handleActionKey}
            />

            {this.context.isMobile && this.state.value && <i className="lz lz-close" onClick={this.handleClear} />}
          </div>
        </div>
      </div>
    );
  }

  setInput = (ref) => {
    if (!ref) return;

    this.input = ref;
    if (this.props.location.pathname === '/tim-kiem') this.input.focus();

    const query = this.getQuery();
    if (query.q) {
      this.input.value = query.q;
      this.setState({ value: query.q });
    }
  };

  getPageType = () => {
    if (this.context.partnerName !== 'none') return 'partner';

    const isSearch = !['/su-kien/:eventId', '/'].includes(
      this.props.match.path,
    );
    if (isSearch) return 'search-loship';
    return 'loship';
  }

  handleInputChange = (e) => {
    this.setState({ value: e.target.value }, () => {
      if (['loship'].includes(this.getPageType())) return;
      const { value } = this.state;
      if (value.trim().length < 2) return;

      this.debounceTimer = debouncer(this.debounceTimer, this.handleSearch, 500);
    });
  };

  handleActionKey = (e) => {
    if (e.keyCode === 27) return this.handleClear();
    if (e.keyCode === 13) return this.handleSearch();
  };

  handleSearch = () => {
    this.props.submit({ q: this.state.value });
  };

  handleFocus = () => {
    if (this.context.partnerName !== 'none' && this.props.location.pathname !== '/tim-kiem') {
      this.props.history.push('/tim-kiem');
    }
  };

  handleClear = () => {
    this.setState({ value: '' });
  };
}

export default initComponent(TopBanner, withRouter);
