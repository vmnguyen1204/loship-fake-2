import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getCurrentCity } from 'reducers/new/metadata';
import { debouncer, iife } from 'utils/helper';

class TopBanner extends BaseComponent {
  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();

      const currentCity = getCurrentCity(state);
      if (currentCity) data = data.set('currentCity', currentCity);

      return data;
    },
    customRender: true,
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  state = { value: '' };

  render() {
    const { currentCity } = this.props;

    const type = this.getPageType();
    const shouldRenderTitle = type === 'lomart' || (type === 'search-lomart' && !this.context.isMobile);

    const imgTitle = '/dist/images/lomart/search-bg.jpg';
    const cityName = iife(() => {
      if (!currentCity) return '';
      if (currentCity.type === 'city') return currentCity.city.name;
      return currentCity.district.name;
    });

    return (
      <div className={cx('top-banner', type)} style={shouldRenderTitle ? { backgroundImage: `url(${imgTitle})` } : {}}>
        <div className="wrapper">
          {shouldRenderTitle && (
            <h1>{this.getString('loshipvn_bring_your_orders_to_your_house_at_0', '', [cityName])}</h1>
          )}
          <div className="search-box">
            <Button type="link" className="btn-search" onClick={this.handleSearch}>
              <i className="lz lz-search" />
            </Button>
            <input
              type="text"
              ref={this.setInput}
              className="search-box-input"
              placeholder={this.getString('find_your_food_beverages_sweets_for_loship_to_deliver')}
              value={this.state.value}
              onChange={this.handleInputChange}
              onFocus={this.handleFocus}
              onKeyUp={this.handleActionKey}
            />

            {this.context.isMobile && this.state.value && <i className="lz lz-close" onClick={this.handleClear} />}
          </div>
        </div>
      </div>
    );
  }

  setInput = (ref) => {
    if (!ref) return;

    this.input = ref;
    if (this.props.location.pathname === '/tim-kiem') this.input.focus();

    const query = this.getQuery();
    if (query.q) {
      this.input.value = query.q;
      this.setState({ value: query.q });
    }
  };

  getPageType = () => {
    if (this.context.partnerName !== 'none') return 'partner';

    const isSearch = !['/su-kien/:eventId', '/'].includes(
      this.props.match.path,
    );
    if (isSearch) return 'search-lomart';
    return 'lomart';
  }

  handleInputChange = (e) => {
    this.setState({ value: e.target.value }, () => {
      if (['lomart'].includes(this.getPageType())) return;
      const { value } = this.state;
      if (value.trim().length < 2) return;

      this.debounceTimer = debouncer(this.debounceTimer, this.handleSearch, 500);
    });
  };

  handleActionKey = (e) => {
    if (e.keyCode === 27) return this.handleClear();
    if (e.keyCode === 13) return this.handleSearch();
  };

  handleSearch = () => {
    this.props.submit({ q: this.state.value });
  };

  handleFocus = () => {
    if (this.context.partnerName !== 'none' && this.props.location.pathname !== '/tim-kiem') {
      this.props.history.push('/tim-kiem');
    }
  };

  handleClear = () => {
    this.setState({ value: '' });
  };
}

export default initComponent(TopBanner, withRouter);
