import TopBanner from 'components/pages/search/top-banner/top-banner';
import TopBannerLomart from 'components/pages/search/top-banner/top-banner.lomart';
import React from 'react';
import { APP_INFO } from 'settings/variables';

export default function(props) {
  if (APP_INFO.serviceName === 'lomart') return <TopBannerLomart {...props} />;
  return <TopBanner {...props} />;
}
