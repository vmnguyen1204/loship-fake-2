import cx from 'classnames';
import BasePage from 'components/pages/base';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { getGlobalAddress } from 'reducers/new/metadata';
import { iife } from 'utils/helper';
import localStorage from 'utils/shims/localStorage';

const MAX_COUNT = 999;

export const LIMIT_DESKTOP = 24;
export const LIMIT_MOBILE = 8;

export const sortFilterConfig = {
  sorts: [
    { label: 'sort_nearby', value: 'nearby', icon: 'gps' },
    { label: 'sort_mostReviewed', value: 'most-reviewed', icon: 'like' },
  ],
  filters: [
    { label: 'filter_opening', labelShort: 'filter_opening_short', value: 'opening', icon: 'history' },
    { label: 'filter_partner', labelShort: 'filter_partner_short', value: 'partner', icon: 'star-o' },
  ],
};

export default class SearchPageBase extends BasePage {
  static defaultProps = {
    dataGetter: (state) => {
      const data = {};

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data.globalAddress = globalAddress.data;

      return data;
    },
    customRender: true,
  };

  static contextTypes = {
    isMobile: PropTypes.bool,
  };

  state = { stats: {} };

  componentDidMount() {
    const query = this.getQuery();

    const currentTab = (() => {
      if (['merchant', 'dish'].includes(query.md)) return query.md;
      if (this.context.isMobile && ['merchant', 'dish'].includes(localStorage.getItem('search-tab'))) {
        return localStorage.getItem('search-tab');
      }
      return 'merchant';
    })();
    const currentSearchFilter = iife(() => {
      try {
        const data = !query.sorts && !query.filters
          ? JSON.parse(localStorage.getItem('search-filter'))
          : { filters: (query.filters || '').split(','), sorts: (query.sorts || '').split(',') };
        data.filters = data.filters.filter((filter) => sortFilterConfig.filters.some((f) => f.value === filter));
        data.sorts = data.sorts.filter((sort) => sortFilterConfig.sorts.some((f) => f.value === sort));

        return data;
      } catch (e) {
        const defaultSearchFilter = { filters: ['opening'], sorts: ['nearby'] };
        return defaultSearchFilter;
      }
    });

    this.setState({ currentTab, currentSearchFilter }, this.acquireStats);
  }

  renderSearchTitle = (query) => {
    const title = (() => {
      if (query.newestApi) return this.getString('new_location');
      if (query.featureChainApi) return this.getString('popular_brands');
      if (query.merchanteateries) return this.getString('merchant_thoughtful');
      if (query.promotionApi) return this.getString('promoting_locations');
      if (query.recentApi) return this.getString('recently_ordered');
      if (query.nearbyApi) return this.getString('nearby_location_search');
      if (query.groupApi) return this.getString('more_fun_to_order_together');
      if (query.searchSuggestEateriesApi) return this.getString('similar_merchant');
      if (query.food) return this.getString('treats_for_groups');
      if (query.snacks) return this.getString('snacks_finger_food');
      if (query.breakfast) return this.getString('food_for_breakfast');
      if (query.lunch) return this.getString('food_for_lunch');
      if (query.midnight) return this.getString('food_for_midnight');
    })();

    if (!title) return null;
    return <h1 className="container">{title}</h1>;
  };

  renderTabs = () => {
    if (!this.context.isMobile) return null;

    const { currentTab, stats = {} } = this.state;
    return (
      <div className="search-tabs">
        <div
          className={cx('search-tab-item', { active: currentTab === 'merchant' })}
          onClick={this.changeTab('merchant')}
        >
          {this.getString('merchant', 'search')}
          {stats.eateries && ` (${stats.eateries.total > MAX_COUNT ? '999+' : stats.eateries.total})`}
        </div>
        <div className={cx('search-tab-item', { active: currentTab === 'dish' })} onClick={this.changeTab('dish')}>
          {this.getString('qpBeBWHLmz', 'search')}
          {stats.dishes && ` (${stats.dishes.total > MAX_COUNT ? '999+' : stats.dishes.total})`}
        </div>
      </div>
    );
  };

  renderNotFound = () => {
    return (
      <div className="not-found">
        <img src="/dist/images/loship-404.png" alt="Not found" />
        <h3>{this.getString('sorry_loship_has_no_results_you_are_looking_for', 'search')}</h3>
        <p>{this.getString('check_spelling_and_try_again_or_search_with_different_keywords', 'search')}</p>
      </div>
    );
  };

  submit = (query) => {
    const { pathname } = this.props.location;

    const thisQuery = this.getQuery();
    const nextQuery = (() => {
      if (query.q && thisQuery.q !== query.q) return { ...thisQuery, ...query, page: undefined };
      return { ...thisQuery, ...query };
    })();

    if (thisQuery.q !== nextQuery.q) {
      if (pathname === '/tim-kiem') this.props.history.replace(`/tim-kiem?${qs.stringify(nextQuery)}`);
      else this.props.history.push(`/tim-kiem?${qs.stringify(nextQuery)}`);
    } else if ((thisQuery.page || nextQuery.page) && parseInt(thisQuery.page) !== nextQuery.page) {
      return this.props.history.push(`${pathname}?${qs.stringify(nextQuery)}`);
    }

    this.acquireStats();
    const elem = document.getElementById('screen');
    elem.scrollTop = 0;
  };

  changeTab = (tab) => () => {
    if (tab === this.state.tab) return;

    this.setState({ currentTab: tab });
    localStorage.setItem('search-tab', tab);

    const { location: { pathname } } = this.props;
    const query = this.getQuery();
    delete query.page;
    this.props.history.push(`${pathname}?${qs.stringify(query)}`);
  };

  handleChangeSearchFilter = (searchFilter) => {
    this.setState({ currentSearchFilter: searchFilter }, this.acquireStats);
    localStorage.setItem('search-filter', JSON.stringify(searchFilter));

    const { location: { pathname } } = this.props;
    const query = this.getQuery();
    query.sorts = searchFilter.sorts.join(',');
    query.filters = searchFilter.filters.join(',');
    delete query.page;

    this.props.history.push(`${pathname}?${qs.stringify(query)}`);
  };

  toNextStep = () => {
    const { cartMetadata = {} } = this.props;
    const { data: { merchant } = {} } = cartMetadata;
    if (!merchant) return;

    this.props.history.push(`/b/${merchant}?step=confirm-order`);
  };
}
