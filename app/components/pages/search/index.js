import { fetchCategories, fetchCoreCategories } from 'actions/category';
import BaseComponent from 'components/base';
import React from 'react';
import { APP_INFO } from 'settings/variables';
import Search from './search';
import SearchLomart from './search.lomart';

export default class SearchPage extends BaseComponent {
  static needs = [fetchCategories, fetchCoreCategories];

  render() {
    if (APP_INFO.serviceName === 'lomart') return <SearchLomart {...this.props} />;
    return <Search {...this.props} />;
  }
}
