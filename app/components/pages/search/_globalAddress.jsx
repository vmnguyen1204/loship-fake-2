import { CONFIRM_POPUP, GENERIC_POPUP, open } from 'actions/popup';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import StepAddress from 'components/shared/order/address';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getCurrentCity, getGlobalAddress } from 'reducers/new/metadata';
import { getShippingAddresses } from 'reducers/user';
import { getString } from 'utils/i18n';

function handleOpenAddressPopup({ globalAddress, updateShippingInfo }) {
  this.props.dispatch(
    open(CONFIRM_POPUP, {
      header: null,
      content: (popupProps) => (
        <StepAddress
          goBack={popupProps.handleClose}
          // address + distance
          merchantAddress={globalAddress}
          shippingAddresses={this.props.shippingAddresses}
          updateShippingInfo={(address) => {
            updateShippingInfo(address);
            popupProps.handleClose();
          }}
          // config
          autoFocusInput={true}
        />
      ),
      wrapperClassName: 'layer-toaa order-page',
      contentClassName: 'search-global-address-popup order-step-address',
      actionClassName: 'd-none',
      closeBtnClassName: 'lz-close',
      animationStack: 3,
    }),
  );
}

class SearchGlobalAddress extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
    setGlobalAddress: PropTypes.func.isRequired,
  }

  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();

      const currentCity = getCurrentCity(state);
      if (currentCity) data = data.set('currentCity', currentCity);

      const shippingAddresses = getShippingAddresses(state);
      if (shippingAddresses) data = data.set('shippingAddresses', shippingAddresses.toJS().data);

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      return data;
    },
    customRender: true,
  }

  render() {
    const address = this.props.globalAddress?.address;

    return (
      <div className="search-global-address">
        <div
          className="search-global-address-content container"
          onClick={address
            ? handleOpenAddressPopup.bind(this, {
              globalAddress: this.props.globalAddress,
              updateShippingInfo: this.context.setGlobalAddress,
              isMobile: this.context.isMobile,
            })
            : undefined}
        >
          {this.getString('global_address_select')}

          {address ? (
            <div className="address">
              <i className="lz lz-fw lz-marker" />
              <div className="content">{address}</div>
              <i className="lz lz-arrow-head-right" />
            </div>
          ) : (
            <div className="address shimmer-animation">
              <i className="lz lz-fw lz-marker" />
              <div className="content shimmer-text" />
              <i className="lz lz-arrow-head-right" />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default initComponent(SearchGlobalAddress, withRouter);

export function LoshipNotAvailableComponent({ globalAddress }) {
  const isLoading = globalAddress?.status === 'LOADING';

  if (isLoading) {
    return (
      <div key="component-loading" className="section-loship-not-available">
        <img src="/dist/images/loading-address.png" />

        <h3>{getString('global_address_loading_title')}</h3>
        <p>{getString('global_address_loading')}</p>
      </div>
    );
  }

  return (
    <div key="component-404" className="section-loship-not-available">
      <img src="/dist/images/loship-404-home.png" />

      <h3>{getString('global_address_not_available_title')}</h3>
      <p>{getString('global_address_not_available')}</p>
    </div>
  );
}

export function globalAddressRequireHandler({ geoLocationStatus, saveClosedHandler }) {
  return new Promise((resolve) => {
    this.props.dispatch(
      open(GENERIC_POPUP, {
        header: ({ handleClose }) => {
          saveClosedHandler(handleClose);
          if (this.partnerName !== 'none' || !this.isMobile) return null;
          return (
            <div className="header">
              <div className="address-content-results">
                <div className="address-content-result address-request-gps">
                  <i className="lz lz-fw lz-gps" />
                  <div className="content">
                    <div className="address">{this.getString('global_address_notice_enable_location')}</div>
                    <Button
                      type="primary" className="btn-sm"
                      onClick={handleFetchGeoLocation.bind(this, resolve)}
                    >
                      {this.getString(geoLocationStatus.state === 'denied' ? 'enable_location' : 'allow_location')}
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          );
        },
        content: ({ handleClose }) => (
          <div className="address-content">
            <h2>{this.getString('choose_shipping_address', 'shippingAddress')}</h2>

            <div className="address-content-input-wrapper">
              <i className="lz lz-fw lz-marker text-red" />
              <input
                className="address-content-input"
                onFocus={handleOpenAddressPopup.bind(this, {
                  globalAddress: this.props.globalAddress,
                  updateShippingInfo: (address) => this.setGlobalAddress(address, handleClose),
                  isMobile: this.isMobile,
                })}
                placeholder={this.getString('input_shipping_address')}
              />
            </div>

            <div className="scroll-container scroll-nice">
              {this.props.shippingAddresses?.length > 0 ? (
                <div className="address-content-results">
                  {this.props.shippingAddresses.map((address) => {
                    let placeName = address.placeName;
                    if (!placeName) placeName = address.address?.split(',')[0];

                    return (
                      <div
                        key={`${address.lat}-${address.lng}`}
                        className="address-content-result"
                        onClick={() => this.setGlobalAddress(address, handleClose)}
                      >
                        <i className="lz lz-fw lz-history" />
                        <div className="content">
                          {placeName && <h4>{placeName}</h4>}
                          <div className="address">{address.address}</div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              ) : (
                <div className="text-center">
                  <img src="/dist/images/loship-no-data.png" />
                </div>
              )}
            </div>
          </div>
        ),
        footer: null,
        wrapperClassName: 'layer-lv4',
        className: 'request-global-address-popup',
        animationStack: 1,
        canNotDismiss: true,
      }),
    );
  });

  function handleFetchGeoLocation(resolve) {
    this.setState({ isFetchingGeoLocation: true }, () => {
      this.getGeoLocation({
        force: true,
        callback: (location) => {
          if (location) {
            resolve(location);
            this.setState({ isFetchingGeoLocation: true });
          }
        },
      });
    });
  }
}
