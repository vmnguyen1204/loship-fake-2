import { fetchNewsFeedStats } from 'actions/newsfeed';
import SearchFilter from 'components/pages/search/_filter';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItem } from 'components/shared/newsfeed/merchant-item';
import initComponent from 'lib/initComponent';
import qs from 'qs';
import React from 'react';
import Helmet from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { getCategories, getCategory } from 'reducers/category';
import { getCoreCategories } from 'reducers/category-core';
import { getCartMetadata } from 'reducers/new/cart';
import { getCurrentCity, getGlobalAddress } from 'reducers/new/metadata';
import { getCurrentUser } from 'reducers/user';
import { getDistrictBySlug } from 'utils/data';
import { dessertDishes, drinkDishes, drinkThemes, foodCuisines, foodDishes, foodThemes } from 'utils/json/categories';
import { head } from 'utils/seo';
import SearchPageBase, { LIMIT_DESKTOP, LIMIT_MOBILE } from './search-base';
import TopBanner from './top-banner';
import SearchGlobalAddress, { LoshipNotAvailableComponent } from './_globalAddress';

class SearchPage extends SearchPageBase {
  static mapStateToProps = (state, props) => {
    const currentCity = getCurrentCity(state);
    const globalAddress = getGlobalAddress(state);
    const categories = getCategories(state);
    const coreCategories = getCoreCategories(state);

    const query = composeQuery(props);
    const currentUser = getCurrentUser(state);
    const cartMetadata = getCartMetadata(state, props.match.params);

    return {
      seo: {
        city: currentCity,
        district: query.districts && query.districts[0] ? getDistrictBySlug(query.districts[0]) : null,
        theme: query.themeSlugs && query.themeSlugs[0] ? getCategory(state, { category: query.themeSlugs[0] }) : null,
        food: query.dishGroupSlugs && query.dishGroupSlugs[0] ? getCategory(state, { category: query.dishGroupSlugs[0] }) : null,
      },
      cartMetadata,
      currentUser: currentUser && currentUser.get('data'),
      categories: categories && categories.get('data'),
      coreCategories: coreCategories && coreCategories.get('data'),
      globalAddress: globalAddress?.data,
      currentCity,
    };
  };

  render() {
    const { currentSearchFilter } = this.state;

    const query = composeQuery(this.props, {
      state: this.state,
      context: this.context,
    });
    const metadata = composeMetadata(query, this.props.seo, { getString: this.getString });

    if (!this.props.currentCity) {
      return (
        <div className="search-page">
          <Helmet {...metadata} />
          <TopBanner submit={this.submit} />
          <SearchGlobalAddress />
          <LoshipNotAvailableComponent globalAddress={this.props.globalAddress} />
        </div>
      );
    }

    return (
      <div className="search-page">
        <Helmet {...metadata} />
        <TopBanner submit={this.submit} />
        {!this.context.isMobile && <SearchGlobalAddress />}

        {this.renderSearchTitle(query)}
        {!!query.q && this.renderTabs()}
        {!!query.shouldShowSortFilter && (
          <SearchFilter data={currentSearchFilter} handleChange={this.handleChangeSearchFilter} />
        )}
        {this.context.isMobile && <SearchGlobalAddress />}

        <div className="container">
          <SectionNewsfeed
            params={query}
            mode={this.context.isMobile
              ? `search-page|preload|load_more${this.state.currentTab === 'dish' && '|search-dish'}`
              : 'search-page|preload|pagination'}
            submit={this.submit}
            MerchantItem={EateryItem}
            renderNotFound={this.renderNotFound}
          />
        </div>

      </div>
    );
  }

  acquireStats = () => {
    const query = this.getQuery();
    if (query.q) {
      this.props.dispatch(
        fetchNewsFeedStats({
          query: { q: query.q, ...this.state.currentSearchFilter },
          callback: (data) => {
            this.setState({ stats: data });
          },
        }),
      );
    }
  };
}

export default initComponent(SearchPage, withRouter);

export const searchGroupHardCode = {
  'do-an': {
    name: 'Đồ ăn',
    filter: {
      theme: foodThemes,
      food: foodDishes,
      cuisine: foodCuisines,
    },
  },
  'thuc-uong': {
    name: 'Thức uống',
    filter: {
      theme: drinkThemes,
      food: drinkDishes,
    },
  },
  'trang-mieng': {
    name: 'Tráng miệng',
    filter: { food: dessertDishes },
  },
};

export function composeQuery(props, { state = {}, context = {} } = {}) {
  const { pathname, search } = props.location;
  const { params } = props.match;

  const query = qs.parse(search, { ignoreQueryPrefix: true });
  const { page = query.page || 1, q = query.q || '' } = state;

  let composedQuery = {
    limit: context.isMobile ? LIMIT_MOBILE : LIMIT_DESKTOP,
    page,
    q,
    cityId: query.cityId, districtId: query.districtId,
    superCategoryId: query.superCategoryId,
    globalAddress: props.globalAddress,
  };

  if (state.currentTab === 'dish' && pathname.includes('/tim-kiem')) {
    composedQuery.searchDishesApi = true;
    return composedQuery;
  }

  if (pathname.includes('danh-sach-dia-diem-giao-tan-noi')) composedQuery.shouldShowSortFilter = true;
  if (pathname.includes('danh-sach-dia-diem-moi-tham-gia-loship-giao-tan-noi')) composedQuery.newestApi = true;
  if (pathname.includes('danh-sach-dia-diem-co-khuyen-mai-giao-tan-noi')) composedQuery.promotionApi = true;
  if (pathname.includes('danh-sach-dia-diem-thuong-hieu-quen-thuoc-giao-tan-noi')) composedQuery.featureChainApi = true;
  if (pathname.includes('danh-sach-dia-diem-cua-hang-chu-dao-giao-tan-noi')) composedQuery.merchanteateries = true;
  if (pathname.includes('danh-sach-dia-diem-vua-duoc-dat-giao-tan-noi')) composedQuery.recentApi = true;
  if (pathname.includes('danh-sach-dia-diem-an-voi-nhom')) composedQuery.groupApi = true;
  if (pathname.includes('danh-sach-dia-diem-tuong-tu-gan-day')) {
    composedQuery.searchSuggestEateriesApi = true;
    composedQuery.ignoreCity = true;
    composedQuery.eateryId = params.eateryId;
  }

  if (pathname.includes('danh-sach-dia-diem-an-vat-giao-tan-noi')) {
    composedQuery.snacks = true;
    composedQuery.dishGroupSlugs = ['an-vat-via-he'];
  }
  if (pathname.includes('danh-sach-sieu-thi-bach-hoa')) {
    composedQuery.groceries = true;
    composedQuery.dishGroupSlugs = ['sieu-thi-cua-hang', 'cua-hang-bach-hoa'];
  }
  if (pathname.includes('dia-diem-quanh-day')) {
    composedQuery.nearbyApi = true;
  }

  const districts = [],
        themeSlugs = [],
        dishGroupSlugs = [];

  if (params.theme) {
    themeSlugs.push(...params.theme.split(',').filter((c) => !!c));
  }
  if (params.dishGroup) {
    const searchGroup = searchGroupHardCode[params.dishGroup];
    if (searchGroup) {
      if (searchGroup.filter.food) dishGroupSlugs.push(...searchGroup.filter.food.map((f) => f.id));
      if (searchGroup.filter.theme) dishGroupSlugs.push(...searchGroup.filter.theme.map((f) => f.id));
      if (searchGroup.filter.cuisine) themeSlugs.push(...searchGroup.filter.cuisine.map((f) => f.id));
    } else dishGroupSlugs.push(...params.dishGroup.split(',').filter((c) => !!c));
  }

  if (params.district) {
    districts.push(
      ...params.district
        .split(',')
        .map(getDistrictBySlug)
        .filter((district) => !!district)
        .map((district) => district.slug),
    );
  }

  const customQuery = {
    ...(dishGroupSlugs.indexOf('an-sang') > -1 && { breakfast: true }),
    ...(dishGroupSlugs.indexOf('an-trua') > -1 && { lunch: true }),
    ...(dishGroupSlugs.indexOf('an-toi') > -1 && { midnight: true }),
    ...(dishGroupSlugs.indexOf('an-voi-nhom') > -1 && { food: true }),
  };

  composedQuery = { ...composedQuery, ...customQuery, districts, themeSlugs };
  composedQuery.dishGroupSlugs = (dishGroupSlugs.length > 0 && dishGroupSlugs) || composedQuery.dishGroupSlugs || [];
  composedQuery.shouldShowSortFilter = composedQuery.shouldShowSortFilter ||
    !!composedQuery.q ||
    composedQuery.dishGroupSlugs.length > 0;

  if (composedQuery.shouldShowSortFilter) {
    const { sorts = [], filters = [] } = state.currentSearchFilter || {};
    if (Array.isArray(filters) && filters.length > 0) composedQuery.filters = filters;
    if (Array.isArray(sorts) && sorts.length > 0) composedQuery.sorts = sorts;
  }

  return composedQuery;
}

export function composeMetadata(query, seo, { getString }) {
  const metadata = {};
  const city = (seo.city && seo.city.name) || '';

  if (query.newestApi) {
    metadata.title = getString(`Tổng hợp các địa điểm mới tham gia loship giao tận nơi tại ${city}`);
    metadata.description = getString(
      `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`,
    );
    return head(metadata);
  }
  if (query.featureChainApi) {
    metadata.title = getString(`Tổng hợp các địa điểm thương hiệu quen thuộc giao tận nơi tại ${city}`);
    metadata.description = getString(
      `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`,
    );
    return head(metadata);
  }
  if (query.merchanteateries) {
    metadata.title = getString(`Tổng hợp các địa điểm cửa hàng chu đáo ${city}`);
    metadata.description = getString(
      `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`,
    );
    return head(metadata);
  }
  if (query.promotionApi) {
    metadata.title = getString(`Tổng hợp các địa điểm đang có khuyến mãi giao tận nơi tại ${city}`);
    metadata.description = getString(
      `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`,
    );
    return head(metadata);
  }
  if (query.recentApi) {
    metadata.title = getString(`Tổng hợp các địa điểm vừa được đặt giao tận nơi tại ${city}`);
    metadata.description = getString(
      `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`,
    );
    return head(metadata);
  }

  if (query.food) {
    metadata.title = getString(`Tổng hợp các địa điểm ăn no với nhóm giao tận nơi tại ${city}`);
    metadata.description = getString(
      `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`,
    );
    return head(metadata);
  }

  if (query.midnight) {
    metadata.title = `Tổng hợp các địa điểm ăn khuya giao tận nơi tại ${city}`;
    metadata.description = `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`;
    return head(metadata);
  }

  if (query.breakfast) {
    metadata.title = `Tổng hợp các địa điểm ăn sáng giao tận nơi tại ${city}`;
    metadata.description = `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`;
    return head(metadata);
  }

  if (query.snacks) {
    metadata.title = `Tổng hợp các địa điểm ăn vặt giao tận nơi tại ${city}`;
    metadata.description = `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`;
    return head(metadata);
  }

  if (query.groceries) {
    metadata.title = `Tổng hợp siêu thị bách hóa giao tận nơi tại ${city}`;
    metadata.description = `Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ tại ${city} với Loship`;
    return head(metadata);
  }

  const district = seo.district && seo.district.name;
  const theme = seo.theme && seo.theme.getIn(['data', 'value']);
  const food = seo.food && seo.food.getIn(['data', 'value']);

  metadata.title = getString('all_locations');
  metadata.description = getString('discover_all_delivery_locations');

  if (theme) {
    metadata.title += getString(` phong cách ${theme}`);
    metadata.description += getString(` phong cách ${theme}`);
  }

  if (food) {
    metadata.title += getString(` phục vụ món ${food}`);
    metadata.description += getString(` phục vụ món ${food}`);
  }

  if (district) {
    metadata.title += ` ở ${district}`;
    metadata.description += getString(' ở ') + district;
  }

  metadata.title += getString(' giao tận nơi ') + city;
  metadata.description += getString(` ${city} của Loship.vn - Đặt món và giao ngay chỉ trong 1 giờ`);
  return head(metadata);
}
