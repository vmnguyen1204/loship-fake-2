import React from 'react';

const TOTAL_PAGE_BREAKPOINTS = 7;
const MIN_CURRENT_PAGE_SHOW_DOTS_BEFORE = 4;

export default function Pagination({ currentPage, itemPerPage, totalItems, changePage, getString }) {
  const totalsPage = Math.ceil(totalItems / itemPerPage);
  if (totalsPage <= 1) return null;

  if (totalsPage < TOTAL_PAGE_BREAKPOINTS) {
    const pages = Array.from(Array(totalsPage).keys());
    return (
      <div className="pagination">
        <ul>
          {pages.map((p) => (
            <li onClick={() => changePage(p + 1)} key={p} className={currentPage === p + 1 ? 'current' : ''}>
              <a>{p + 1}</a>
            </li>
          ))}
        </ul>
      </div>
    );
  }

  const items = [];

  if (currentPage > 1) {
    items.push(
      <li key="prev" onClick={() => changePage(currentPage - 1)}>
        <a>{getString('previous_page')}</a>
      </li>,
    );
  }

  if (currentPage > 1) {
    items.push(
      <li key={1} onClick={() => changePage(1)}>
        <a>1</a>
      </li>,
    );
  }

  if (currentPage >= MIN_CURRENT_PAGE_SHOW_DOTS_BEFORE) {
    items.push(
      <li key="...1" className="spread">
        ...
      </li>,
    );
  }

  if (currentPage - 1 > 1) {
    items.push(
      <li key={currentPage - 1} onClick={() => changePage(currentPage - 1)}>
        <a>{currentPage - 1}</a>
      </li>,
    );
  }
  items.push(
    <li key={currentPage} className="current" onClick={() => changePage(currentPage)}>
      <a>{currentPage}</a>
    </li>,
  );

  if (currentPage + 1 < totalsPage) {
    items.push(
      <li key={currentPage + 1} onClick={() => changePage(currentPage + 1)}>
        <a>{currentPage + 1}</a>
      </li>,
    );
  }

  if (currentPage + 1 < totalsPage - 1) {
    items.push(
      <li key="...2" className="spread">
        ...
      </li>,
    );
  }
  if (currentPage < totalsPage) {
    items.push(
      <li key={totalsPage} onClick={() => changePage(totalsPage)}>
        <a>{totalsPage}</a>
      </li>,
    );
  }
  if (currentPage < totalsPage) {
    items.push(
      <li key="next" onClick={() => changePage(currentPage + 1)}>
        <a>{getString('next_page')}</a>
      </li>,
    );
  }

  return (
    <div className="pagination">
      <ul>{items}</ul>
    </div>
  );
}
