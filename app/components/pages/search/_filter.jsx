import { GENERIC_POPUP, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import { sortFilterConfig } from 'components/pages/search/search-base';
import Button from 'components/shared/button';
import Checkbox from 'components/shared/checkbox';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';

class SearchFilterPopup extends React.Component {
  state = { data: {} };

  componentDidMount() {
    const { data: { sorts = [], filters = [] } = {} } = this.props;

    const currentData = {};
    sorts.concat(filters).forEach((item) => {
      currentData[item] = true;
    });
    this.setState({ data: currentData });
  }

  render() {
    const { data } = this.state;

    return (
      <div className="search-filter-content">
        <div className="flex-view">
          {sortFilterConfig.filters.map((item) => (
            <Checkbox
              key={item.label}
              className="flex-item filter-item"
              checked={!!data[item.value]}
              onChange={this.handleCheck('filter', item.value)}
            >
              <span>
                <i className={cx('lz', `lz-${item.icon}`)} />
                {this.props.getString(item.label)}
              </span>
            </Checkbox>
          ))}
        </div>
        <div className="flex-view">
          {sortFilterConfig.sorts.map((item) => (
            <Checkbox
              key={item.label}
              className="flex-item filter-item"
              checked={!!data[item.value]}
              onChange={this.handleCheck('sort', item.value)}
              isRadio
            >
              <span>
                <i className={cx('lz', `lz-${item.icon}`)} />
                {this.props.getString(item.label)}
              </span>
            </Checkbox>
          ))}
        </div>
        <div className="section-cta">
          <Button type="primary" fw onClick={this.handleChange}>
            {this.props.getString('finish')}
          </Button>
        </div>
      </div>
    );
  }

  handleChange = () => {
    const { data } = this.state;

    const sortFilterData = {
      sorts: sortFilterConfig.sorts.filter((item) => !!data[item.value]).map((item) => item.value),
      filters: sortFilterConfig.filters.filter((item) => !!data[item.value]).map((item) => item.value),
    };

    typeof this.props.handleChange === 'function' && this.props.handleChange(sortFilterData);
    typeof this.props.handleClose === 'function' && this.props.handleClose();
  };

  handleCheck = (type, value) => (checked) => {
    const { data } = this.state;

    if (type === 'filter') data[value] = checked;
    else if (type === 'sort') {
      data[value] = true;
      sortFilterConfig.sorts.forEach((item) => {
        if (item.value !== value) data[item.value] = false;
      });
    }

    this.setState({ data });
  };
}

class SearchFilter extends BaseComponent {
  render() {
    const { data: { sorts = [], filters = [] } = {} } = this.props;

    let sortFilterData = sorts.map((value) => {
      const config = sortFilterConfig.sorts.find((sort) => sort.value === value);
      return config && this.getString(config.labelShort || config.label);
    });
    sortFilterData = sortFilterData.concat(
      filters.map((value) => {
        const config = sortFilterConfig.filters.find((filter) => filter.value === value);
        return config && this.getString(config.labelShort || config.label);
      }),
    );

    return (
      <div className="search-filter container">
        <div className="search-filter-content">
          <div className="text-trimmed">
            <b>{this.getString('filter')}: </b>
            {sortFilterData.join(' - ')}
          </div>
          <Button className="btn-filter" onClick={this.handleOpenSearchFilterPopup}>
            <i className="lz lz-filter" />
          </Button>
        </div>
      </div>
    );
  }

  handleOpenSearchFilterPopup = () => {
    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('choose_filter'),
        content: (popupProps) => (
          <SearchFilterPopup
            {...popupProps}
            data={this.props.data}
            handleChange={this.props.handleChange}
            getString={this.getString}
          />
        ),
        wrapperClassName: 'layer-toaa',
        className: 'search-filter-popup',
        closeBtnClassName: 'lz-close',
        animationStack: 1,
      }),
    );
  };
}

export default initComponent(SearchFilter, withRouter);
