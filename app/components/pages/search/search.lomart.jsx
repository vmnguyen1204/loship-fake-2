import { fetchNewsFeedStats } from 'actions/newsfeed';
import SearchFilter from 'components/pages/search/_filter';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItem } from 'components/shared/newsfeed/merchant-item';
import initComponent from 'lib/initComponent';
import qs from 'qs';
import React from 'react';
import Helmet from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { getCategories, getCategory } from 'reducers/category';
import { getCoreCategories } from 'reducers/category-core';
import { getCartMetadata } from 'reducers/new/cart';
import { getCurrentCity, getGlobalAddress } from 'reducers/new/metadata';
import { getCurrentUser } from 'reducers/user';
import { getDistrictBySlug } from 'utils/data';
import { head } from 'utils/seo';
import SearchPageBase, { LIMIT_DESKTOP, LIMIT_MOBILE } from './search-base';
import TopBanner from './top-banner';
import SearchGlobalAddress, { LoshipNotAvailableComponent } from './_globalAddress';

class SearchPage extends SearchPageBase {
  static mapStateToProps = (state, props) => {
    const currentCity = getCurrentCity(state);
    const globalAddress = getGlobalAddress(state);
    const categories = getCategories(state);
    const coreCategories = getCoreCategories(state);

    const query = composeQuery(props);
    const currentUser = getCurrentUser(state);
    const cartMetadata = getCartMetadata(state, props.match.params);

    return {
      seo: {
        city: currentCity,
        district: query.districts && query.districts[0] ? getDistrictBySlug(query.districts[0]) : null,
        theme: query.themeSlugs && query.themeSlugs[0] ? getCategory(state, { category: query.themeSlugs[0] }) : null,
        food: query.dishGroupSlugs && query.dishGroupSlugs[0] ? getCategory(state, { category: query.dishGroupSlugs[0] }) : null,
      },
      cartMetadata,
      currentUser: currentUser && currentUser.get('data'),
      categories: categories && categories.get('data'),
      coreCategories: coreCategories && coreCategories.get('data'),
      globalAddress: globalAddress?.data,
      currentCity,
    };
  };

  render() {
    const { currentSearchFilter } = this.state;

    const query = composeQuery(this.props, {
      state: this.state,
      context: this.context,
    });
    const metadata = composeMetadata(query, this.props.seo, { getString: this.getString });

    if (!this.props.currentCity) {
      return (
        <div className="search-page">
          <Helmet {...metadata} />
          <TopBanner submit={this.submit} />
          <SearchGlobalAddress />
          <LoshipNotAvailableComponent globalAddress={this.props.globalAddress} />
        </div>
      );
    }

    return (
      <div className="search-page">
        <Helmet {...metadata} />
        <TopBanner submit={this.submit} />
        {!this.context.isMobile && <SearchGlobalAddress />}

        {this.renderSearchTitle(query)}
        {!!query.q && this.renderTabs()}
        {!!query.shouldShowSortFilter && (
          <SearchFilter data={currentSearchFilter} handleChange={this.handleChangeSearchFilter} />
        )}
        {this.context.isMobile && <SearchGlobalAddress />}

        <div className="container">
          <SectionNewsfeed
            params={query}
            mode={this.context.isMobile
              ? `search-page|preload|load_more${this.state.currentTab === 'dish' && '|search-dish'}`
              : 'search-page|preload|pagination'}
            submit={this.submit}
            MerchantItem={EateryItem}
            renderNotFound={this.renderNotFound}
          />
        </div>

      </div>
    );
  }

  acquireStats = () => {
    const query = this.getQuery();
    if (query.q) {
      this.props.dispatch(
        fetchNewsFeedStats({
          query: { q: query.q, ...this.state.currentSearchFilter },
          callback: (data) => {
            this.setState({ stats: data });
          },
        }),
      );
    }
  };
}

export default initComponent(SearchPage, withRouter);

export const searchGroupHardCode = {
  'thit-va-trung': {
    name: 'Thịt & trứng',
    filter: {
      theme: [
        {
          id: 1086,
          slug: 'cua-hang-thit-trung',
        },
      ],
    },
  },
  'hai-san-tuoi': {
    name: 'Hải sản tươi',
    filter: {
      theme: [
        {
          id: 1087,
          slug: 'cua-hang-hai-san-tuoi',
        },
      ],
    },
  },
  'rau-cu': {
    name: 'Rau củ',
    filter: {
      theme: [
        {
          id: 1088,
          slug: 'cua-hang-rau-cu',
        },
      ],
    },
  },
  'gao-va-mi': {
    name: 'Gạo & mì',
    filter: {
      theme: [
        {
          id: 1092,
          slug: 'cua-hang-gao-mi',
        },
      ],
    },
  },
  'gia-vi': {
    name: 'Gia vị',
    filter: {
      theme: [
        {
          id: 1093,
          slug: 'cua-hang-gia-vi',
        },
      ],
    },
  },
  'tap-hoa': {
    name: 'Tạp hóa',
    filter: {
      theme: [
        {
          id: 1085,
          slug: 'tiem-tap-hoa',
        },
      ],
    },
  },
};

export function composeQuery(props, { state = {}, context = {} } = {}) {
  const { pathname, search } = props.location;
  const { params } = props.match;

  const query = qs.parse(search, { ignoreQueryPrefix: true });
  const { page = query.page || 1, q = query.q || '' } = state;

  let composedQuery = {
    limit: context.isMobile ? LIMIT_MOBILE : LIMIT_DESKTOP,
    page,
    q,
    cityId: query.cityId, districtId: query.districtId,
    superCategoryId: query.superCategoryId,
    globalAddress: props.globalAddress,
  };

  if (state.currentTab === 'dish' && pathname.includes('/tim-kiem')) {
    composedQuery.searchDishesApi = true;
    return composedQuery;
  }

  if (pathname.includes('danh-sach-cua-hang-giao-tan-noi')) composedQuery.shouldShowSortFilter = true;
  if (pathname.includes('danh-sach-cua-hang-moi-tham-gia-lomart-giao-tan-noi')) composedQuery.newestApi = true;
  if (pathname.includes('danh-sach-cua-hang-co-khuyen-mai-giao-tan-noi')) composedQuery.promotionApi = true;
  if (pathname.includes('danh-sach-cua-hang-vua-duoc-dat-giao-tan-noi')) composedQuery.recentApi = true;
  if (pathname.includes('danh-sach-cua-hang-duoc-dat-nhieu-giao-tan-noi')) composedQuery.popular = true;
  if (pathname.includes('danh-sach-cua-hang-mua-voi-nhom')) composedQuery.groupApi = true;
  if (pathname.includes('danh-sach-cua-hang-tuong-tu-gan-day')) {
    composedQuery.searchSuggestEateriesApi = true;
    composedQuery.ignoreCity = true;
    composedQuery.eateryId = params.eateryId;
  }

  if (pathname.includes('danh-sach-cua-hang-bach-hoa-va-sieu-thi')) {
    composedQuery.groceries = true;
    composedQuery.dishGroupSlugs = ['sieu-thi-cua-hang', 'cua-hang-bach-hoa'];
  }
  if (pathname.includes('cua-hang-quanh-day')) {
    composedQuery.nearbyApi = true;
  }

  const districts = [],
        themeSlugs = [],
        dishGroupSlugs = [];

  if (params.theme) {
    themeSlugs.push(...params.theme.split(',').filter((c) => !!c));
  }
  if (params.dishGroup) {
    const searchGroup = searchGroupHardCode[params.dishGroup];
    if (searchGroup) {
      if (searchGroup.filter.food) dishGroupSlugs.push(...searchGroup.filter.food.map((f) => f.id));
      if (searchGroup.filter.theme) dishGroupSlugs.push(...searchGroup.filter.theme.map((f) => f.id));
      if (searchGroup.filter.cuisine) themeSlugs.push(...searchGroup.filter.cuisine.map((f) => f.id));
    } else dishGroupSlugs.push(...params.dishGroup.split(',').filter((c) => !!c));
  }

  if (params.district) {
    districts.push(
      ...params.district
        .split(',')
        .map(getDistrictBySlug)
        .filter((district) => !!district)
        .map((district) => district.slug),
    );
  }

  const customQuery = {};

  composedQuery = { ...composedQuery, ...customQuery, districts, themeSlugs };
  composedQuery.dishGroupSlugs = (dishGroupSlugs.length > 0 && dishGroupSlugs) || composedQuery.dishGroupSlugs || [];
  composedQuery.shouldShowSortFilter = composedQuery.shouldShowSortFilter ||
    !!composedQuery.q ||
    composedQuery.dishGroupSlugs.length > 0;

  if (composedQuery.shouldShowSortFilter) {
    const { sorts = [], filters = [] } = state.currentSearchFilter || {};
    if (Array.isArray(filters) && filters.length > 0) composedQuery.filters = filters.join(',');
    if (Array.isArray(sorts) && sorts.length > 0) composedQuery.sorts = sorts.join(',');
  }

  return composedQuery;
}

export function composeMetadata(query, seo, { getString }) {
  const metadata = {};
  const city = (seo.city && seo.city.name) || '';

  if (query.newestApi) {
    metadata.title = getString(`Tổng hợp các cửa hàng mới tham gia lomart giao tận nơi tại ${city}`);
    metadata.description = getString(`Đặt mua rau thịt cá và được giao tận nơi chỉ trong 1 giờ tại ${city} với Lomart`);
    return head(metadata);
  }
  if (query.promotionApi) {
    metadata.title = getString(`Tổng hợp các cửa hàng đang có khuyến mãi giao tận nơi tại ${city}`);
    metadata.description = getString(`Đặt mua rau thịt cá và được giao tận nơi chỉ trong 1 giờ tại ${city} với Lomart`);
    return head(metadata);
  }
  if (query.recentApi) {
    metadata.title = getString(`Tổng hợp các cửa hàng vừa được đặt giao tận nơi tại ${city}`);
    metadata.description = getString(`Đặt mua rau thịt cá và được giao tận nơi chỉ trong 1 giờ tại ${city} với Lomart`);
    return head(metadata);
  }

  if (query.groceries) {
    metadata.title = `Tổng hợp các cửa hàng bách hóa và siêu thị tại ${city}`;
    metadata.description = `Đặt mua rau thịt cá và được giao tận nơi chỉ trong 1 giờ tại ${city} với Lomart`;
    return head(metadata);
  }

  const district = seo.district && seo.district.name;
  const theme = seo.theme && seo.theme.getIn(['data', 'value']);
  const food = seo.food && seo.food.getIn(['data', 'value']);

  metadata.title = getString('all_locations');
  metadata.description = getString('discover_all_delivery_locations');

  if (theme) {
    metadata.title += getString(` phong cách ${theme}`);
    metadata.description += getString(` phong cách ${theme}`);
  }

  if (food) {
    metadata.title += getString(` ${food}`);
    metadata.description += getString(` ${food}`);
  }

  if (district) {
    metadata.title += ` ở ${district}`;
    metadata.description += getString(' ở ') + district;
  }

  metadata.title += getString(' giao tận nơi ') + city;
  metadata.description += getString(` ${city} của Lomart.vn - Đặt món và giao ngay chỉ trong 1 giờ`);
  return head(metadata);
}
