import { sendGAEvent } from 'actions/factory';
import { GENERIC_POPUP, ORDER_FAILED, open } from 'actions/popup';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import ExpandContainer from 'components/shared/expand-container';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItem } from 'components/shared/newsfeed/merchant-item';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getGlobalAddress } from 'reducers/new/metadata';
import { APP_INFO } from 'settings/variables';
import { iife } from 'utils/helper';
import { getImageUrl } from 'utils/image';
import loziServices, { servicesAvailable } from 'utils/json/loziServices';

class HomeBannerComp extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
  };

  static defaultProps = {
    dataGetter: (state) => {
      const data = {};

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data.globalAddress = globalAddress.data;

      return data;
    },
    customRender: true,
  }

  render() {
    const { data } = this.props;

    const imgWidth = Math.min(window.innerWidth, 600);
    const imgContentHeight = (imgWidth / 16) * 9;
    const showServiceBtn = data.get('type') === 'shipping_service';

    const query = {
      ...this.getQuery(),
      limit: 8,
      globalAddress: this.props.globalAddress,
      eventId: data.get('id'),
    };

    return (
      <div className="banner-popup-content mobile">
        <div
          className="banner-image"
          style={{
            backgroundImage: `url(${getImageUrl(data.get('image'), imgWidth, 'o')})`,
            height: imgContentHeight + 'px',
          }}
        />

        <ExpandContainer
          title={data.get('name')}
          content={data.get('description')}
          line={0}
          className="banner-content"
          style={{ marginBottom: showServiceBtn ? 96 : 8 }}
        />

        {showServiceBtn ? (
          <div className="section-cta">
            <Button type="primary" fw onClick={this.goToHomepage}>
              {this.getString('order_now')}
            </Button>
          </div>
        ) : (
          <SectionNewsfeed
            params={query}
            mode="'preload|load_more"
            title={this.getString('applicable_stores')}
            MerchantItem={(props) => (
              <EateryItem {...props} isMobile={true} />
            )}
            scrollableAncestor={null}
            trackingAction="Loship_Action.Click.Banner.Eatery"
            trackingLabel={data.get('id')}
            onMerchantLoaded={() => this.props.handleClose(true)}
          />
        )}
      </div>
    );
  }

  goToHomepage = () => {
    const { data } = this.props;
    sendGAEvent('internal_navigation', 'Loship_Action.Click.Banner.Service', data.get('id'));

    if (data.get('superCategoryId') === APP_INFO.shipServiceId) {
      this.props.history.push('/');
      return this.props.handleClose();
    }

    const targetService = loziServices.find((s) => s.superCategoryId === data.get('superCategoryId'));
    const isAvailable = targetService && servicesAvailable.includes(targetService.name);

    if (isAvailable) window.location.href = targetService.domain;
    else {
      this.props.dispatch(
        open(ORDER_FAILED, {
          title: this.getString('only_available_on_mobile_app'),
          error: iife(() => (
            <div>
              <p>
                {this.getString(
                  'the_service_you_are_requesting_is_only_available_on_loship_app_download_app_on_your_phone_to_use_the_service',
                )}
              </p>
              <div className="promo-btns">
                <a href="https://goo.gl/N5Vs4V" className="app-img app-android" target="_blank" rel="noopener">
                  <img src="/dist/images/app-android.png" alt="android" style={{ maxWidth: 120, marginRight: 8 }} />
                </a>
                <a href="https://goo.gl/CDFMhZ" className="app-img app-ios" target="_blank" rel="noopener">
                  <img src="/dist/images/app-ios.png" alt="ios" style={{ maxWidth: 120 }} />
                </a>
              </div>
            </div>
          )),
          hideBanner: true,
          action: () => { window.location.href = '/'; },
          actionText: this.getString('return_to_homepage'),
          wrapperStyle: { zIndex: 201 },
        }),
      );
    }
  };
}
const HomeBanner = initComponent(HomeBannerComp, withRouter);

export function HomeBannerPopupHandler({ callback }) {
  if (!this || !this.props) return;
  const { event } = this.props;
  if (!event) return;

  this.props.dispatch(
    open(GENERIC_POPUP, {
      title: event.get('name'),
      content: (cProps) => <HomeBanner {...cProps} data={event} />,
      className: 'banner-popup',
      contentClassName: 'scroll-nice',
      animationStack: 3,
      onCancel: () => {
        typeof callback === 'function' && callback();
        this.props.history.push('/');
      },
    }),
  );
}
