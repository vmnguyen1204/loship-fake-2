import { sendGAEvent } from 'actions/factory';
import { fetchBanners, searchSuggestDishes } from 'actions/newsfeed';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import ImageLazy from 'components/statics/img-lazy';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import React from 'react';
import { getCategories } from 'reducers/category';
import { getCoreCategories } from 'reducers/category-core';
import { getCurrentCity, getGlobalAddress } from 'reducers/new/metadata';
import { getBanners, getSuggestDishes } from 'reducers/newsfeed';
import { getOrderNotifications } from 'reducers/notification';
import { APP_INFO } from 'settings/variables';
import { addCurrency } from 'utils/format';
import { iife } from 'utils/helper';
import { getString } from 'utils/i18n';
import Link from 'utils/shims/link';

class HomePageBase extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  };

  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();

      const categories = getCategories(state);
      if (categories) data = data.set('categories', categories.get('data'));

      const coreCategories = getCoreCategories(state);
      if (coreCategories) data = data.set('coreCategories', coreCategories.get('data'));

      const banners = getBanners(state);
      if (banners) data = data.set('banners', banners.get('data'));

      const suggestDishes = getSuggestDishes(state);
      if (suggestDishes) data = data.set('suggestDish', suggestDishes.getIn(['data', 0]));

      const currentCity = getCurrentCity(state);
      if (currentCity) data = data.set('currentCity', currentCity);

      const orders = getOrderNotifications(state);
      const latestOrder = orders && orders.get('data') && orders.get('data')[0];
      if (latestOrder) data = data.set('latestOrder', latestOrder);

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      return data;
    },
    customRender: true,
  }

  state = {};

  componentDidMount() {
    this.props.dispatch(searchSuggestDishes());
    this.handleCoreData();
  }

  componentDidUpdate() {
    this.handleCoreData();
  }

  renderSectionCategories = () => {
    const { isMobile } = this.context;
    if (!this.props.categories) return null;

    const categories = this.props.categories.slice(0, 10);
    const listStyle = isMobile ? { width: categories.size * 115 } : {};

    const baseRoute = iife(() => {
      if (APP_INFO.serviceName === 'lomart') return '/danh-sach-cua-hang-';
      return '/danh-sach-dia-diem-phuc-vu-';
    });

    return (
      <div className={cx('section-newsfeed', isMobile && 'horizontal')}>
        <div className="title">
          <h2>{this.getString('select_by_category')}</h2>
          {isMobile && <Link to={'/danh-muc'}>{this.getString('see_all')}</Link>}
        </div>
        <div className="content">
          <div className="list-view" style={listStyle}>
            {categories.map((category) => {
              return (
                <Link
                  key={category.id}
                  to={`${baseRoute}${category.slug}-giao-tan-noi`}
                  className="list-item category-item"
                  title={category.value}
                  onClick={() => this.sendGAEvent(category.id, 'Action_Tap_Category')}
                >
                  <ImageLazy
                    className="figure"
                    src={(isMobile && category.imageMobile) || category.imageWeb}
                    placeholder="/dist/images/dish-placeholder.png"
                    shape="square"
                    size={100}
                    type="f"
                  />
                  <div className="content">
                    <div className="metadata">
                      <b>{category.value}</b>
                    </div>
                  </div>
                </Link>
              );
            })}
          </div>
          {!isMobile && (
            <Link to={'/danh-muc'} className="btn-view-all">
              {this.getString('see_all')} <i className="lz lz-arrow-head-right" />
            </Link>
          )}
        </div>
      </div>
    );
  }

  renderLatestOrder = () => {
    if (this.context.partnerName === 'none') return null;

    const { latestOrder } = this.props;
    if (!latestOrder) return null;

    const lineStr = latestOrder.lines
      ? latestOrder.lines
        .map((line) => !line.isDeleted && line.name)
        .filter(Boolean)
        .join(', ')
      : [];
    const canReOrder = latestOrder.statusText === 'done' || latestOrder.statusText === 'cancelled';

    return (
      <div className="section-newsfeed home-latest-order">
        <div className="title">
          <h2>{this.getString('your_order')}</h2>
          <Link to={`/tai-khoan/don-hang/${latestOrder.code}`}>
            {this.getString('view_details')}
          </Link>
        </div>
        <div className="content">
          <div className="list-view">
            <div className="list-item eatery-item">
              <ImageLazy
                src={latestOrder.eatery && latestOrder.eatery.avatar}
                alt="Avatar"
                className="figure"
                placeholder="/dist/images/dish-placeholder.png"
                shape='round-square'
                mode='static'
                size={320}
              />
              <div className="content">
                <div className="metadata">
                  <b>{latestOrder.eatery && latestOrder.eatery.name}</b>
                </div>
                {lineStr && <div className="text-trimmed">{lineStr}</div>}
                <div className="metadata">
                  {this.getString('total')} <b>{addCurrency(latestOrder.totalUserFee)}</b>
                </div>
                <div className="status">
                  <span className={`order-status ${latestOrder.statusText}`}>
                    {this.getString(latestOrder.statusText, 'notifier')}
                  </span>
                  {canReOrder && (
                    <Button
                      type="feature"
                      className="btn-sm btn-re-order"
                      to={`/${latestOrder.eatery.username}?status=re-order&code=${latestOrder.code}`}
                    >
                      <i className="fas fa-redo" />
                      <span>{this.getString('re_order')}</span>
                    </Button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  sendGAEvent = (label, action) => {
    return sendGAEvent('internal_navigation', action, label);
  };

  showBannerDetail = (banner) => () => {
    this.sendGAEvent(banner.id, 'Loship_Action.Click.Banner');
    this.props.history.push(`/su-kien/${banner.objId}`);
  };

  handleCoreData = () => {
    const { globalAddress } = this.props;
    if (!globalAddress || !globalAddress.cityId || !globalAddress.lat || !globalAddress.lng || !globalAddress.address) return;

    this.props.dispatch(fetchBanners());
  }
}

export default HomePageBase;

// limit is only for desktop. mobile always 12
export const SECTION_CONFIG = {
  loship_50: [
    {
      title: getString('recently_joined_merchants'),
      action: (
        <a
          key="action" href="https://doitacloship.lozi.vn/"
          className="action"
          target="_blank" rel="noopener" >
          {getString('cooperate_with_loship')}
        </a>
      ),
      params: { newestApi: true },
      mode: 'preload|see_all',
    },
    {
      title: getString('hot_deals'),
      params: { promotionApi: true },
      mode: 'see_all',
    },
    {
      title: getString('popular_brands'),
      params: { featureChainApi: true },
      mode: 'see_all',
    },
    {
      title: getString('merchant_thoughtful'),
      params: { merchanteateries: true },
      mode: 'see_all',
    },
    {
      title: getString('trending_locations'),
      params: { recentApi: true },
      mode: 'see_all',
    },
    {
      title: getString('breakfast'),
      params: { breakfast: true, dishGroupIds: [1167], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('lunches'),
      params: { lunch: true, dishGroupIds: [1168], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('midnight'),
      params: { midnight: true, dishGroupIds: [1169], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('milk_tea'),
      params: { dishGroupIds: [24], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('snacks'),
      params: { snacks: true, dishGroupIds: [3], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('more_fun_to_order_together'),
      params: { groupApi: true, dishGroupIds: [1164] },
      mode: 'see_all',
    },
    {
      title: getString('birthday_cakes'),
      params: { dishGroupIds: [1606], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('low_carb'),
      params: { dishGroupIds: [1134], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('vegetarian'),
      params: { dishGroupIds: [62], limit: 6 },
      mode: 'see_all',
    },
  ],
  lomart_50: [
    {
      title: getString('new_locations'),
      action: (
        <a
          key="action"
          href="https://dangkylomart.lozi.vn/?utm_source=lomart.vn&utm_medium=home_section"
          className="action"
          target="_blank" rel="noopener" >
          {getString('cooperate_with_loship')}
        </a>
      ),
      params: { newestApi: true },
      mode: 'preload|see_all',
    },
    {
      title: getString('hot_deals'),
      params: { promotionApi: true, limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('vegetables'),
      params: { dishGroupIds: [1088], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('fruits'),
      params: { dishGroupIds: [1042], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('rice'),
      params: { dishGroupIds: [1092], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('meat_poultry_and_eggs'),
      params: { dishGroupIds: [1086], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('fish_seafood_and_frozen_food'),
      params: { dishGroupIds: [1087], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('milk_and_dairy_products'),
      params: { dishGroupIds: [1554], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('grocery_stores_and_supermarkets'),
      params: { dishGroupIds: [1094, 20], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('convenience_stores'),
      params: { dishGroupIds: [1104], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('spices_and_seasonings'),
      params: { dishGroupIds: [1093], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('drinks_and_beverages'),
      params: { dishGroupIds: [1510, 1141], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('sweets_and_snacks'),
      params: { dishGroupIds: [1097], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('stationaries'),
      params: { dishGroupIds: [1142], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('households'),
      params: { dishGroupIds: [1099], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('electronic_products'),
      params: { dishGroupIds: [1051], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('mom_baby_and_kids'),
      params: { dishGroupIds: [1046], limit: 6 },
      mode: 'see_all',
    },
  ],
  lomart_59: [
    {
      title: getString('new_locations'),
      action: (
        <a
          key="action"
          href="https://dangkylomart.lozi.vn/?utm_source=lomart.vn&utm_medium=home_section"
          className="action"
          target="_blank" rel="noopener" >
          {getString('cooperate_with_loship')}
        </a>
      ),
      params: { newestApi: true },
      mode: 'preload|see_all',
    },
    {
      title: getString('hot_deals'),
      params: { promotionApi: true, limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('fruits'),
      params: { dishGroupIds: [1042], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('fish_seafood'),
      params: { dishGroupIds: [1087], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('milk_and_dairy_products'),
      params: { dishGroupIds: [1554], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('grocery_stores_and_supermarkets'),
      params: { dishGroupIds: [1094, 20], limit: 6 },
      mode: 'see_all',
    },
    {
      title: getString('convenience_stores'),
      params: { dishGroupIds: [1104], limit: 6 },
      mode: 'see_all',
    },
  ],
};
