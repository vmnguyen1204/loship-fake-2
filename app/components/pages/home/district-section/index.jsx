import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItemLanding } from 'components/shared/newsfeed/merchant-item';
import PropTypes from 'prop-types';
import React from 'react';
import { compose, withStateHandlers } from 'recompose';
import { getCityByIdOrNameOrSlug, getDistrictInCity } from 'utils/data';
import { iife } from 'utils/helper';

// const SectionDistrictTitleComponent = ({ updateDistrict, showDropdown, toogleDropdown, params }) => {
//   const districts = getDistrictInCity(params.city);
//   const dist = getDistrictById(params.district || districts[0]);

//   return (
//     <h2>
//       <i className="lz lz-location-marker" />
//       KHU VỰC
//       <div className="dropdown" onClick={toogleDropdown}>
//         {dist && <span className="label">{dist.name}</span>}
//         <i className="lz lz-triangle-down" />
//         {showDropdown && <div className="dropdown-backdrop" />}
//         {showDropdown && (
//           <ul className="selection">
//             {districts.map((d, k) => (
//               <li
//                 key={k}
//                 onClick={() => {
//                   updateDistrict(d.slug);
//                 }}
//               >
//                 <span>{d.name}</span>
//               </li>
//             ))}
//           </ul>
//         )}
//       </div>
//     </h2>
//   );
// };

// const SectionDistrictTitle = compose(
//   withStateHandlers(
//     () => ({ showDropdown: false }),
//     { toogleDropdown: (state) => () => ({ showDropdown: !state.showDropdown }) },
//   ),
// )(SectionDistrictTitleComponent);

const DistrictSection = (
  { submit, params, district: propDistrict, updateDistrict, getString, cityId },
  { isMobile },
) => {
  const city = getCityByIdOrNameOrSlug(cityId);
  const cityName = iife(() => {
    if (!city) return '';
    if (city.type === 'city') return city.city.name;
    return city.district.name;
  });

  const districts = city.type === 'city' ? getDistrictInCity(cityId) : [];
  const district = propDistrict || (districts[0] && districts[0].slug);

  if (isMobile) {
    return (
      <SectionNewsfeed
        // CustomTitle={
        //   <SectionDistrictTitle
        //     params={{
        //       ...params,
        //       districts: district && [district],
        //       hideEmpty: false,
        //     }}
        //     updateDistrict={updateDistrict}
        //   />
        // }
        title="Khu vực"
        params={{
          ...params,
          districts: district && [district],
          hideEmpty: false,
        }}
        mode={isMobile ? 'see_all|horizontal' : 'see_all'}
        submit={submit}
        MerchantItem={EateryItemLanding}
      />
    );
  }

  return (
    <div className="section-newsfeed district-section">
      <div className="title">
        <h2>{getString('area')}</h2>
      </div>
      <div className="wrapper">
        <div className="district-list">
          <h4>{cityName}</h4>
          <ul className="scroll-nice">
            {districts.map((d) => (
              <li key={d.slug} className={district === d.slug ? 'active' : ''} onClick={() => updateDistrict(d.slug)}>
                {d.name}
              </li>
            ))}
          </ul>
        </div>
        <SectionNewsfeed
          params={{
            ...params,
            limit: 10,
            districts: district && [district],
          }}
          mode={isMobile ? 'see_all|horizontal' : 'see_all'}
          submit={submit}
          MerchantItem={EateryItemLanding}
        />
      </div>
    </div>
  );
};

DistrictSection.contextTypes = { isMobile: PropTypes.bool };

export default compose(
  withStateHandlers(
    () => ({ district: null }),
    { updateDistrict: () => (value) => ({ district: value }) },
  ),
)(DistrictSection);
