import TopBanner from 'components/pages/search/top-banner';
import SearchGlobalAddress, { LoshipNotAvailableComponent } from 'components/pages/search/_globalAddress';
import Carousel from 'components/shared/carousel';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItemLanding } from 'components/shared/newsfeed/merchant-item';
import OrderDebt from 'components/shared/order/payment/debt-order';
import TroThanhChienBinhLoship from 'components/statics/chienbinhloship';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { APP_INFO } from 'settings/variables';
import { iife } from 'utils/helper';
import { DEFAULT_CITY_ID } from 'utils/json/supportedCities';
import DistrictSection from './district-section';
import HomePageBase, { SECTION_CONFIG } from './home-base';
import LomartBanner from './lomart-banner';

const LOW_CARB_ID = 1134;
const VEGAN_ID = 62;
const BIRTHDAY_CAKE_ID = 1606;

class HomePage extends HomePageBase {
  submit = (query, force = false) => {
    if (query.q || force) {
      return this.props.history.push(`/tim-kiem?q=${query.q}`);
    }

    const { categories, coreCategories } = this.props;
    const dishGroups = query.dishGroupIds
      ?.map((id) => {
        return categories.find((category) => category.id === id) ||
          coreCategories.find((category) => category.id === id);
      })
      .filter(Boolean)
      .map((category) => category.slug);

    const queryMapping = {
      newestApi: '-moi-tham-gia-loship',
      featureChainApi: '-thuong-hieu-quen-thuoc',
      merchanteateries: '-cua-hang-chu-dao',
      promotionApi: '-co-khuyen-mai',
      recentApi: '-vua-duoc-dat',
      food: '-phuc-vu-an-voi-nhom',
      breakfast: '-phuc-vu-an-sang',
      lunch: '-phuc-vu-an-trua,com-trua-van-phong',
      midnight: '-phuc-vu-an-toi',
      snacks: '-an-vat',
      theme: query.themes && `-phong-cach-${query.themes.join(',')}`,
      dishGroupIds: dishGroups && `-phuc-vu-${dishGroups.join(',')}`,
    };

    const gaMapping = {
      promotionApi: 'Promo',
      recentApi: 'Recent',
      food: 'EatWithGroup',
      breakfast: 'Breakfast',
      lunch: 'Lunch',
      midnight: 'Dinner',
      snacks: 'Snack',
    };

    if (query.groceries) {
      return this.props.history.push('/danh-sach-sieu-thi-bach-hoa');
    }

    if (query.nearbyApi) {
      return this.props.history.push('/danh-sach-dia-diem-quanh-day');
    }

    if (query.groupApi) {
      return this.props.history.push('/danh-sach-dia-diem-an-voi-nhom');
    }

    if (query.dishGroupIds) {
      if (query.dishGroupIds[0] === LOW_CARB_ID) this.sendGAEvent('LowCarb', 'Action_Tap_Section');
      if (query.dishGroupIds[0] === VEGAN_ID) this.sendGAEvent('Vegan', 'Action_Tap_Section');
      if (query.dishGroupIds[0] === BIRTHDAY_CAKE_ID) this.sendGAEvent('BirthdayCake', 'Action_Tap_Section');
    }

    const gaEvent = Object.keys(gaMapping).find((p) => !!query[p]);
    if (gaEvent) this.sendGAEvent(gaEvent);

    const param = Object.keys(queryMapping).find((p) => !!query[p]);
    let queryString = `/danh-sach-dia-diem${queryMapping[param] || ''}-giao-tan-noi`;
    if (query.districts && query.districts.length > 0) {
      queryString += '-o-' + query.districts.join(',');
    }

    return this.props.history.push(queryString);
  };

  render() {
    const { isMobile, partnerName } = this.context;
    const { banners, suggestDish, currentCity } = this.props;

    if (!this.props.currentCity) {
      return (
        <div className="home-page">
          <TopBanner submit={(query) => this.submit(query, true)} getString={this.getString} />
          <SearchGlobalAddress />
          <LoshipNotAvailableComponent globalAddress={this.props.globalAddress} />
        </div>
      );
    }

    const cityId = currentCity.type === 'city' ? currentCity.city.id : currentCity.district.city.id;
    const cityKey = currentCity.type === 'city'
      ? `${currentCity.city.id}`
      : `${currentCity.district.city.id}_${currentCity.district.id}`;

    const sectionData = SECTION_CONFIG[`${APP_INFO.serviceName}_${cityKey}`] ||
      SECTION_CONFIG[`${APP_INFO.serviceName}_${DEFAULT_CITY_ID}`] ||
      SECTION_CONFIG.loship_50;

    return (
      <div className="home-page">
        <TopBanner submit={(query) => this.submit(query, true)} getString={this.getString} />
        <SearchGlobalAddress />

        {this.context.isMobile && this.renderLatestOrder()}

        {isMobile && <OrderDebt />}

        {banners && banners.size > 0 && (
          <Carousel
            className="banner-list"
            slideClassName="banner-item"
            data={banners
              .toArray()
              .map((banner) => ({ key: banner.id, image: banner.image, onClick: this.showBannerDetail(banner) }))}
          />
        )}

        <div className="container">
          {this.renderSectionCategories()}

          {suggestDish && (
            <SectionNewsfeed
              title={this.getString('nearby_location_has_dish', '', [suggestDish.name])}
              params={{
                page: 1,
                limit: 6,
                searchSuggestEateriesByDishId: true,
                dishId: suggestDish.id,
                globalAddress: this.props.globalAddress,
              }}
              mode={isMobile ? 'horizontal' : ''}
              trackingAction="section_x"
              submit={this.submit}
              MerchantItem={EateryItemLanding}
            />
          )}

          <SectionNewsfeed
            title={this.getString('nearby_location')}
            params={{
              nearbyApi: true,
              page: 1,
              limit: 12,
              globalAddress: this.props.globalAddress,
            }}
            mode={isMobile ? 'see_all|horizontal' : 'see_all'}
            submit={this.submit}
            MerchantItem={EateryItemLanding}
          />

          {sectionData.map((section) => (
            <SectionNewsfeed
              key={section.title}
              params={{
                ...section.params,
                page: 1,
                limit: iife(() => {
                  if (!this.context.isMobile && section.params.limit) return section.params.limit;
                  return 12;
                }),
                globalAddress: this.props.globalAddress,
              }}
              title={section.title} action={section.action}
              submit={this.submit}
              mode={isMobile ? `${section.mode}|horizontal` : section.mode}
              MerchantItem={EateryItemLanding}
            />
          ))}

          {partnerName === 'none' && <LomartBanner />}

          {partnerName === 'none' && (
            <SectionNewsfeed
              params={{
                page: 1,
                limit: 6,
                dishGroupIds: [1094, 20],
                globalAddress: this.props.globalAddress,
              }}
              title={this.getString('groceries')}
              mode={isMobile ? 'see_all|horizontal' : 'see_all'}
              submit={this.submit}
              MerchantItem={EateryItemLanding}
            />
          )}

          {partnerName === 'none' && currentCity.type !== 'district' && (
            <DistrictSection
              params={{
                page: 1,
                limit: 12,
                globalAddress: this.props.globalAddress,
              }}
              mode="see_all"
              submit={this.submit}
              getString={this.getString}
              cityId={cityId}
            />
          )}

          {partnerName === 'none' && <TroThanhChienBinhLoship />}
        </div>
      </div>
    );
  }
}

export default initComponent(HomePage, withRouter);
