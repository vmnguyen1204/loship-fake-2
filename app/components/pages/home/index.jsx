import { fetchEventById } from 'actions/newsfeed';
import BasePage from 'components/pages/base';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import React from 'react';
import { getCurrentCity } from 'reducers/new/metadata';
import { getEventById } from 'reducers/newsfeed';
import * as SEOTYPE from 'settings/seo-types';
import { APP_INFO } from 'settings/variables';
import { produce } from 'utils/helper';
import Home from './home';
import HomeLomart from './home.lomart';

if (process.env.BROWSER) {
  require('assets/styles/pages/home.scss');
  require('assets/styles/pages/home-mobile.scss');
}

class HomePage extends BasePage {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const currentCity = getCurrentCity(state);
      if (currentCity) data = data.set('currentCity', currentCity);

      const currentEvent = getEventById(state, params);
      if (currentEvent) data = data.set('currentEvent', currentEvent.get('data'));

      return data;
    },
    customRender: true,
    getMetadata: (props, seoOptions) => {
      return produce(seoOptions, (draft) => {
        draft.type = (() => {
          if (props.currentEvent) return SEOTYPE.EVENT;
          return SEOTYPE.HOMEPAGE;
        })();
        draft.currentEvent = props.currentEvent;
      });
    },
  };

  static needs = [fetchEventById];

  render() {
    if (APP_INFO.serviceName === 'lomart') return <HomeLomart />;
    return <Home />;
  }
}

export default initComponent(HomePage);
