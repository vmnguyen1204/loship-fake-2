import React from 'react';
import MerchantItem from './item-merchant';

function MerchantList({ merchantList, handleSelectMerchantItem, selectedMerchant }) {
  if (!merchantList || merchantList.length === 0) return false;
  return (
    <ul className="merchant-list">
      {merchantList.map((mc) => (
        <MerchantItem {...mc} onClick={handleSelectMerchantItem} selectedMerchant={selectedMerchant} key={mc.id} />
      ))}
    </ul>
  );
}

export default MerchantList;
