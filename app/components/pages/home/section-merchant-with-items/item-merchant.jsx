import React from 'react';
import cx from 'classnames';

function MerchantItem({ name, slug, onClick, selectedMerchant } = {}) {
  return (
    <li className={cx({ active: selectedMerchant === slug })} onClick={() => onClick(slug)}>
      {name}
    </li>
  );
}
export default MerchantItem;
