const merchantList = {
  1: [
    // {
    //   id: 100,
    //   name: 'Siêu thị Big C',
    //   username: 'bigcthegardent',
    //   slug: 'big-c-the-gardent-quan-cau-giay-ha-noi-1529640736511570625',
    //   featuredItemIds: [3036603, 3036017, 3036268, 3037335, 3036399, 3036347, 3037399, 3036841, 3036969, 3035800, 3037328, 3036186, 3036152, 3035633, 3035700, 3035770, 3036074, 3035940, 3035911, 3036039],
    // },
    {
      id: 101,
      name: 'Fivimart',
      username: 'fivimartnguyenvancu1',
      slug: 'fivimart-nguyen-van-cu-1-quan-long-bien-ha-noi-1530001741251629554',
      featuredItemIds: [3149939, 3150025, 3149982, 3150020, 3149985, 3150179, 3148715, 3148689, 3148446, 3148416, 3151004, 3148383, 3149684, 3149044, 3149155, 3151025, 3149154, 3148907, 3149480, 3149055],
    },
    {
      id: 102,
      name: 'Circle K',
      username: 'circlektongthattung',
      slug: 'circle-k-tong-that-tung-quan-dong-da-ha-noi-1523592628965162523',
      featuredItemIds: [3476019, 3475969, 3475962, 3475925, 3475911, 3475895, 3475893, 3475873, 3476001, 3476007, 3475994, 3476077, 3476036, 3475949, 3475936, 3476046, 3476049, 3476156, 3476123, 3475955],
    },
    {
      id: 103,
      name: 'BakerLand',
      username: 'bakerlandbichcau',
      slug: 'bakerland-bich-cau-quan-dong-da-ha-noi-1533284535651603097',
      featuredItemIds: [3592272, 3591934, 3593513, 3592893, 3593229, 3593580, 3593060, 3593507, 3594774, 3594446, 3594724, 3594789, 3594411, 3594346, 3594919, 3594937, 3594988, 3594995, 3594992, 3595089, 3595084, 3595016],
    },
    {
      id: 104,
      name: 'Luôn Tươi Sạch',
      username: 'luontuoisachtranthaitong',
      slug: 'luon-tuoi-sach-tran-thai-tong-quan-cau-giay-ha-noi-1530763099959359147',
      featuredItemIds: [3163470, 3163466, 3163443, 3163438, 3163452, 3163449, 3163398, 3163473, 3163394, 3163392, 3163400, 3163404, 3163461, 3163456, 3163416, 3163420, 3163391, 3163426, 3163421, 3163472],
    },
    {
      id: 105,
      name: 'Bác Tôm',
      username: 'bactomhamnghi',
      slug: 'bac-tom-ham-nghi-quan-nam-tu-liem-ha-noi-1530011022086490560',
      featuredItemIds: [3106828, 3106812, 3106934, 3106945, 3107010, 3107006, 3106988, 3107014, 3107016, 3106902, 3106888, 3106894, 3106867, 3106861, 3106868, 3106872, 3106865, 3106957, 3106956, 3106964, 3106972],
    },
    {
      id: 106,
      name: 'Michi Mart',
      username: 'michimartphotranphu',
      slug: 'michi-mart-pho-tran-phu-quan-ba-dinh-ha-noi-1531322731120277606',
      featuredItemIds: [3162992, 3163000, 3162998, 3162994, 3162987, 3163009, 3163014, 3163007, 3163011, 3163002, 3162974, 3162972, 3162973, 3162982, 3162969, 3163039, 3163041, 3163050, 3163046, 3163052, 3163053, 3163027, 3163028, 3163025],
    },
    {
      id: 8,
      name: 'Vinamilk',
      username: 'vinamilkgiacmosuavietminhkhaihaibatrunglh10022',
      slug: 'vinamilk-giac-mo-sua-viet-minh-khai-hai-ba-trung-lh10022-quan-hai-ba-trung-ha-no-1584331610690588588',
      featuredItemIds: [36823203,36822939,36822937,36823000,36822998,36822992,36823037,36823030,36823054,36822911,36822924,36822919,36822918,36822904,36823068,36823071,36823173,36823122,36823167,36823187],
    },
  ],
  50: [
    {
      id: 1,
      name: 'Siêu thị Big C',
      username: 'sieuthibigcmiendongtohienthanh',
      slug: 'sieu-thi-big-c-mien-dong-to-hien-thanh-quan-10-ho-chi-minh-1527673128954004931',
      featuredItemIds: [421925, 423792, 424717, 426162, 426289, 426179, 426524, 421084, 424641, 419896, 421082, 421086, 418301, 425691, 418288, 418294, 424216, 467865, 467752, 421961],
    },
    {
      id: 2,
      name: 'Bách Hoá Xanh',
      username: 'bachhoaxanhcuulong',
      slug: 'bach-hoa-xanh-cuu-long-quan-10-ho-chi-minh-1529639290096418013',
      featuredItemIds: [35913697, 35913518, 35913488, 35913605, 35915660, 35915666, 35913905, 35915364, 35914791, 35914125, 35915110, 35914667, 35915167, 35914325, 35914864, 35914188, 35914978, 35915258, 35913419],
    },
    {
      id: 9,
      name: 'Vinamilk',
      username: 'vinamilkgiacmosuavietnguyenthinhoqtanbinhkp40051',
      slug: 'vinamilk-giac-mo-sua-viet-nguyen-thi-nho-q-tan-binh-kp40051-quan-tan-binh-ho-chi-1584431696200325025',
      featuredItemIds: [36893994,36894104,36894086,36893973,36893967,36894201,36894381,36894150,36894134,36894122,36893960,36893949,36894237,36894216,36894277,36894317,36894253,36894400,36894420,36894454],
    },
    {
      id: 3,
      name: 'TH True Mart',
      username: 'thtruemartsuvanhanh',
      slug: 'th-true-mart-su-van-hanh-quan-10-ho-chi-minh-1530609146019232146',
      featuredItemIds: [2176309, 2176279, 2176277, 2176273, 2176287, 2176297, 2176294, 2176286, 2176296, 2176310, 2176311, 2176303, 2176302, 2176272, 2176263, 2176278, 2176280, 2176285, 2176307, 2176290],
    },
    {
      id: 4,
      name: 'Mẹ Quê',
      username: 'mequethucphamdacsananvat',
      slug: 'me-que-thuc-pham-dac-san-an-vat',
      featuredItemIds: [466826, 466828, 466840, 466834, 466891, 466855, 466908, 466925, 466940, 466938, 466977, 466955, 466950, 466949, 466821, 466837, 467025, 467035, 467034, 467013],
    },
    {
      id: 5,
      name: 'ALA Mall',
      username: 'alamallsieuthihangngoainhap',
      slug: 'ala-mall-sieu-thi-hang-ngoai-nhap',
      featuredItemIds: [436279, 436208, 436275, 436231, 436227, 436252, 436197, 436207, 436192, 436234, 436212, 436257, 436191, 436181, 436186, 436179, 436188, 436255, 436193, 436269],
    },
    {
      id: 6,
      name: 'Tèobokki Store',
      username: 'teobokkistorehuynhthienloc',
      slug: 'teobokki-store-huynh-thien-loc',
      featuredItemIds: [466483, 466476, 466463, 466464, 466482, 466474, 466586, 466598, 466579, 466571, 466567, 466462, 466628, 466621, 466549, 466520, 466497, 466517, 466551, 466527],
    },
    {
      id: 7,
      name: 'Gạo Me-Kong',
      username: 'cuahanggaomekong',
      slug: 'cua-hang-gao-me-kong',
      featuredItemIds: [457695, 457698, 457700, 457707, 457711, 457705, 457703, 457704, 457712, 457715, 457714, 457713, 457702, 457696, 457706, 457709, 457697, 457708, 457710, 457699],
    },
    {
      id: 8,
      name: 'Cô Ba Trái Cây',
      username: 'cobatraicaynoidiavanhapkhau',
      slug: 'co-ba-trai-cay-noi-dia-va-nhap-khau',
      featuredItemIds: [440450, 440437, 440465, 440441, 440439, 440454, 440449, 440451, 440464, 440457, 440460, 440435, 440459, 440444, 440440, 440445, 440452, 440458, 440438, 440443],
    },
  ],
};

export default merchantList;
