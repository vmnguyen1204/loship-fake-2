import { fetchMerchant, fetchMerchantMenu } from 'actions/merchant';
import { CONFIRM_POPUP, open } from 'actions/popup';
import BaseComponent from 'components/base';
import DishItem from 'components/pages/merchant/_menu/_dish-item';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getMerchantMenu } from 'reducers/menu';

function dishDataGenerator({ group, dish }) {
  return {
    group,
    dish: {
      id: dish.id,
      name: dish.name,
      price: dish.price,
      image: dish.image,
      customs: dish.customs,
      groupId: null,
      groupDishId: group.id,
      isExtraGroupDish: group.isExtraGroupDish,
    },
  };
}

class MerchanProductItemList extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
  }

  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getMerchantMenu(state, params);
      if (!data) return;
      if (!data.get('data')) return data;

      const featuredItems = [];
      const menuObj = data.toJS();
      menuObj.data.group.forEach((gr) => {
        gr.dishes.forEach((dish) => {
          if (Array.isArray(params.itemIds) && params.itemIds.indexOf(dish.id) > -1) {
            featuredItems.push(dishDataGenerator({ group: gr, dish }));
          }
        });
      });
      data = data.set('products', featuredItems);

      return data;
    },
    customRender: true,
  };

  componentDidMount() {
    const params = this.props.params;
    this.props.dispatch(fetchMerchantMenu(params));
    this.props.dispatch(fetchMerchant(params));
  }

  componentDidUpdate(prevProps) {
    const params = this.props.params;
    const prevParams = prevProps.params;

    if (params.merchant !== prevParams.merchant) {
      this.setState({ ignoreGoToMerchant: false });
      this.props.dispatch(fetchMerchantMenu(params));
      this.props.dispatch(fetchMerchant(params));
    }
  }

  render() {
    const { products } = this.props;
    const params = this.getParams();

    if (!params.merchant) return null;
    if (!products) return <span>{this.getString('no_products_found')}</span>;

    return (
      <div className="product-list">
        {products.map(({ dish, group }) => (
          <DishItem
            key={dish.id}
            data={dish}
            groupInfo={{
              isExtraGroupDish: group.isExtraGroupDish,
              groupDishId: group.id,
            }}
            mode="COMPACT"
            merchant={this.props.merchant}
            ignoreMerchantReady
            callback={this.handleCallback}
            imageSize={this.context.isMobile ? 240 : 120}
          />
        ))}
      </div>
    );
  }

  handleCallback = () => {
    if (this.state.ignoreGoToMerchant) return;
    const { merchant } = this.props;

    this.props.dispatch(
      open(CONFIRM_POPUP, {
        title: this.getString('item_added'),
        content: () => {
          return (
            <div className="text-left">
              <p>
                {this.getString('do_you_want_to_continue_shopping_at')}
                &nbsp;
                <b>{merchant.name}</b>?
              </p>
              <p>{this.getString('click_go_to_store_to_see_all_products_listed_here')}</p>
            </div>
          );
        },
        confirmText: this.getString('go_to_store'),
        cancelText: this.getString('go_back'),
        onConfirm: () => {
          this.props.history.push(`/${merchant.username || `b/${merchant.slug}`}`);
        },
        onCancel: () => {
          this.setState({ ignoreGoToMerchant: true });
        },
      }),
    );
  };
}

export default initComponent(MerchanProductItemList, withRouter);
