import React from 'react';
import { compose, withStateHandlers } from 'recompose';
import Link from 'utils/shims/link';
import MerchantProductItemList from './list-product';
import MerchantList from './list-merchant';
import merchantList from './merchants';

function MerchantWithItemsSection({ getString, cityId, selectedMerchant, handleSelectMerchantItem } = {}) {
  const merchantListByCity = merchantList[cityId];
  const selectedMerchantByCity = selectedMerchant[cityId];

  if (!merchantListByCity || merchantListByCity.length === 0) return false;
  const currentMerchant = merchantListByCity.find((mc) => mc.slug === selectedMerchantByCity) || merchantListByCity[0];
  const featuredItemIds = currentMerchant.featuredItemIds;

  return (
    <div className="merchant-with-items-section">
      <h2>
        <i className="lz lz-heart" />
        {getString('most_favored_products')}
      </h2>
      <MerchantList
        merchantList={merchantListByCity}
        handleSelectMerchantItem={handleSelectMerchantItem}
        selectedMerchant={selectedMerchantByCity || currentMerchant.slug}
      />
      <MerchantProductItemList
        params={{ merchant: selectedMerchantByCity, itemIds: featuredItemIds }}
        merchant={currentMerchant}
      />
      <p className="text-center mt-16">
        <Link to={`/${currentMerchant.username}`} className="btn-view-all-result">
          {getString('see_all_products_at_0', 'default', [currentMerchant.name])}
          &nbsp;
          <i className="lz lz-arrow-head-right" />
        </Link>
      </p>
    </div>
  );
}

MerchantWithItemsSection.defaultProps = { cityId: 50 };

export default compose(
  withStateHandlers(
    () => ({
      selectedMerchant: {
        1: 'big-c-the-gardent-quan-cau-giay-ha-noi-1529640736511570625',
        50: 'sieu-thi-big-c-mien-dong-to-hien-thanh-quan-10-ho-chi-minh-1527673128954004931',
      },
    }),
    { handleSelectMerchantItem: (state, props) => (slug) => ({ selectedMerchant: { ...state.selectedMerchant, [props.cityId]: slug } }) },
  ),
  // lifecycle(lifecycleHandler)
)(MerchantWithItemsSection);
