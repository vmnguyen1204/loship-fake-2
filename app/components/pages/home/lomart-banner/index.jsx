import React from 'react';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';

if (process.env.BROWSER) {
  require('assets/styles/components/banner.scss');
}

class LomartBanner extends BaseComponent {
  render() {
    return (
      <div className="lomart-banner">
        <div className="bg">
          <div className="bg-shade" />
          <div className="bg-image" />
        </div>
        <img src="/dist/images/lomart/basket.png" alt="lomart-basket" />
        <div className="content">
          <h3>{this.getString('new_buy_groceries_in_lomart')}</h3>
          <p>
            {this.getString(
              'too_busy_to_go_shopping_for_dinner_tonight_you_want_to_buy_a_gift_for_a_loved_one_but_dont_have_time',
            )}
          </p>
          <p>
            {this.getString(
              'dont_worry_lomart_will_help_you_buy_and_deliver_at_home_all_things_from_flower_shops_to_groceries',
            )}
          </p>
        </div>
        <div>
          <Button
            type="primary"
            className="btn-banner"
            href="https://lomart.vn/?utm_source=loship&utm_medium=home_banner"
            target="_blank"
          >
            {this.getString('try_lomart_now')}
          </Button>
        </div>
      </div>
    );
  }
}

export default LomartBanner;
