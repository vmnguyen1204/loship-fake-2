import Navbar from 'components/shared/mobile-nav';
import initComponent from 'lib/initComponent';
import React from 'react';
import EateryChainPageBase from './_base';

class EateryChainPage extends EateryChainPageBase {
  render() {
    const { status } = this.props;
    const shouldHideBackArrow = ['viettelpay'].includes(this.context.partnerName);

    if (status === 'PENDING') return this.renderLoading();
    if (status !== 'SUCCESS') return null;

    return (
      <div className="eatery-chain-page">
        <Navbar title={this.getString('eatery_chain')} backHandler={shouldHideBackArrow ? null : this.handleBack} />

        <div className="container">
          {this.renderHeading()}
          {this.renderContent()}
        </div>

      </div>
    );
  }

  handleBack = () => {
    this.props.history.goBack();
  };
}

export default initComponent(EateryChainPage);
