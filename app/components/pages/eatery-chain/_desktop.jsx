import initComponent from 'lib/initComponent';
import React from 'react';
import EateryChainPageBase from './_base';

class EateryChainPage extends EateryChainPageBase {
  render() {
    const { status } = this.props;

    if (status === 'PENDING') return this.renderLoading();
    if (status !== 'SUCCESS') return null;

    return (
      <div className="eatery-chain-page">
        <div className="container mobile">
          <h2>{this.getString('eatery_chain')}</h2>
          {this.renderHeading()}
          {this.renderContent()}
        </div>

      </div>
    );
  }
}

export default initComponent(EateryChainPage);
