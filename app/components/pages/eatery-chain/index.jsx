import BaseComponent from 'components/base';
import PropTypes from 'prop-types';
import React from 'react';
import { fetchEateryChain } from 'reducers/new/eatery';
import Desktop from './_desktop';
import Mobile from './_mobile';

class EateryChainPage extends BaseComponent {
  static contextTypes = { isMobile: PropTypes.bool };

  static needs = [fetchEateryChain];

  render() {
    if (this.context.isMobile) return <Mobile {...this.props} />;
    return <Desktop {...this.props} />;
  }
}

export default EateryChainPage;
