import { sendGAEvent } from 'actions/factory';
import cx from 'classnames';
import BasePage from 'components/pages/base';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryChainEateryItem, EateryChainItem } from 'components/shared/newsfeed/merchant-item';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { toast } from 'react-toastify';
import { fetchEateryChain, getEateryChain } from 'reducers/new/eatery';
import { getGlobalAddress } from 'reducers/new/metadata';
import * as SEOTYPE from 'settings/seo-types';
import { produce } from 'utils/helper';

if (process.env.BROWSER) {
  require('assets/styles/pages/eatery-chain.scss');
  require('assets/styles/components/_newsfeed.scss');
}

class EateryChainPageBase extends BasePage {
  static contextTypes = {
    isMobile: PropTypes.bool,
    partnerName: PropTypes.string,
  }

  static defaultProps = {
    dataGetter: (state, params) => {
      let data = {};

      const eateryChain = getEateryChain(state, params);
      if (eateryChain) data = { ...eateryChain };

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data.globalAddress = globalAddress.data;

      return data;
    },
    customRender: true,
    getMetadata: (props, seoOptions) => {
      return produce(seoOptions, (draft) => {
        draft.type = SEOTYPE.EATERY_CHAIN;
        draft.eateryChain = props.data;
      });
    },
  }

  componentDidMount() {
    this.props.dispatch(fetchEateryChain(this.getParams()));
    this.handleNotFound();
    if (this.props.data) this.sendGAEvent();
  }

  componentDidUpdate(prevProps) {
    this.handleNotFound();
    if (this.props.data && !prevProps.data) this.sendGAEvent();
  }

  renderLoading = () => {
    return null;
  }

  renderHeading = () => {
    const { data } = this.props;

    return (
      <div className={cx('section-newsfeed', this.context.isMobile && 'p0_0')}>
        <div className="content">
          <div className="list-view">
            <EateryChainItem
              className="eatery-chain-heading"
              merchant={{ eateryChain: data }}
              isMobile={true}
              mode="heading"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
              }}
            />
          </div>
        </div>
      </div>
    );
  }

  renderContent = () => {
    const { data } = this.props;

    const query = {
      ...this.getQuery(),
      limit: this.context.isMobile ? 8 : 16,
      chainId: data.id,
      globalAddress: this.props.globalAddress,
    };

    return (
      <SectionNewsfeed
        params={query}
        className="p0_0"
        mode="preload|load_more"
        submit={this.submit}
        MerchantItem={EateryChainEateryItem}
      />
    );
  }

  handleNotFound = () => {
    if (this.props.status === 'ERROR') {
      this.props.history.replace('/');
      toast(this.getString('not_found_eatery_chain'));
    }
  }

  submit = (query) => {
    const { pathname } = this.props.location;

    const thisQuery = this.getQuery();
    const nextQuery = (() => {
      if (query.q && thisQuery.q !== query.q) return { ...thisQuery, ...query, page: undefined };
      return { ...thisQuery, ...query };
    })();

    if ((thisQuery.page || nextQuery.page) && parseInt(thisQuery.page) !== nextQuery.page) {
      return this.props.history.push(`${pathname}?${qs.stringify(nextQuery)}`);
    }

    const elem = document.getElementById('screen');
    elem.scrollTop = 0;
  };

  sendGAEvent = () => {
    sendGAEvent('loship_action', 'ACTION_VIEW_EATERYCHAIN', this.props.data.id);
  }
}

export default EateryChainPageBase;
