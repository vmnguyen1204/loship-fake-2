import BaseComponent from 'components/base';
import NumberUpDownInput from 'components/shared/number-up-down-input';
import React from 'react';
import initComponent from 'lib/initComponent';
import { getImageUrl } from 'utils/image';
import OrderCart from 'components/shared/order/cart';

class StepInfo extends BaseComponent {
  render() {
    const { data: dish } = this.props;
    if (!dish) return null;

    return (
      <div className="step-info">
        <div className="detail">
          <div className="figure">
            <img
              src={getImageUrl(dish.image, 480, undefined, undefined, '/dist/images/dish-placeholder.png')}
              srcSet={`${getImageUrl(
                dish.image,
                480,
                undefined,
                undefined,
                '/dist/images/dish-placeholder.png',
              )} 1x, ${getImageUrl(dish.image, 960, undefined, undefined, '/dist/images/dish-placeholder.png')} 2x`}
              alt="Dish"
            />
          </div>

          <h3>{dish.name}</h3>
          <b>
            <p>{this.getString('quantity', 'order')}</p>
          </b>
          <NumberUpDownInput
            className="fw"
            value={dish.dishQuantity}
            min={1}
            max={10}
            onChange={this.props.handleChangeQuantity}
          />
        </div>

        <OrderCart mode="merchant|hide-cart" toNextStep={this.props.toNextStep} />
        {/* <div className="action">
          <div className="price">
            {this.getString('total')} <b>{addCurrency(dish.price * dish.dishQuantity)}</b>
          </div>
          <Button type="primary" onClick={this.props.toNextStep}>
            <span>{this.getString('continue')}</span> <i className="lz lz-arrow-head-right" />
          </Button>
        </div> */}
      </div>
    );
  }
}

export default initComponent(StepInfo);
