import { fetchBlockItemOfferByIds } from 'actions/buy';
import { fetchMerchantShipPrice } from 'actions/merchant';
import Footer from 'components/shared/footer';
import OrderBase, { STEP_ADDRESS, STEP_CONFIRM, STEP_INFO, STEP_VIEW_DETAIL } from 'components/shared/order';
import StepAddress from 'components/shared/order/address';
import StepConfirm from 'components/shared/order/confirm';
import OrderDetail from 'components/shared/order/detail';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { cartAddItem, cartUpdateQuantity } from 'reducers/new/cart';
import { getDistrictId } from 'utils/address';
import { getOrderLine } from 'utils/cart';
import { getCityByIdOrNameOrSlug } from 'utils/data';
import loziServices from 'utils/json/loziServices';
import * as cookie from 'utils/shims/cookie';
import StepInfo from './info';

if (process.env.BROWSER) {
  require('assets/styles/pages/buy.scss');
}

const AVAILABLE_PAYMENT_METHODS = ['cod'];

class BuyPage extends OrderBase {
  constructor(props, context) {
    super(props, context, {
      step: STEP_INFO,
      serviceName: 'lozi',
    });
  }

  componentDidMount() {
    super.componentDidMount();

    // VALIDATE BLOCK/OFFER ITEM
    const { blockOfferItemId = '' } = this.getParams();

    const parsedParams = blockOfferItemId.split(':');
    const blockId = (parsedParams[0] === 'block-id' && parsedParams[1]) || undefined;
    const offerItemId = (parsedParams[0] === 'offer-item-id' && parsedParams[1]) || undefined;
    if (!blockId && !offerItemId) return this.setClientLoaded();

    // VALIDATE USER INFO
    if (!this.props.currentUser) return this.setClientLoaded();

    // ORDER SUCCESS -> RENDER ORDER DETAIL
    const { status } = this.getQuery();
    if (status === 'success') return this.setClientLoaded({ step: STEP_VIEW_DETAIL });

    const service = loziServices.find((s) => s.name === 'lozi');
    const additionalHeaders = { 'X-Loship-Service': service.appApiHeader };
    const accessToken = cookie.get('tempData');
    this.props.dispatch(
      fetchBlockItemOfferByIds({
        blockId,
        offerItemId,
        callback: async (res) => {
          const { dishes = [], eatery } = (res && res.body && res.body.data && res.body.data) || {};
          const dish = dishes[0] && { ...dishes[0], dishId: dishes[0].id, dishQuantity: 1 };
          if (eatery && !!eatery.slug && dish) {
            this.props.dispatch(
              cartAddItem({ data: getOrderLine(dish), merchant: eatery.slug, serviceName: 'lozi', overwrite: true }),
            );

            /** FETCH SHIP PRICE */
            const shipPrice = await fetchMerchantShipPrice({ merchant: eatery.slug, additionalHeaders });
            if (shipPrice) {
              this.setState({
                shippingFee: shipPrice.shippingFee,
                currentShippingFee: shipPrice.currentShippingFee,
                minimumShippingFee: shipPrice.minimumShippingFee,
                extraFees: shipPrice.extraFees,
                promotionEnable: shipPrice.promotionEnable,
                isLoaded: true,
              });
            } else {
              this.setState({
                isLoaded: true,
                errorCode: -1043,
              });
            }
          }
          this.setClientLoaded();
        },
        callbackFailure: (error) => {
          console.error(error);

          if (error.message.includes('not enough infomation')) {
            this.setState({ errorType: 'not enough information', errorCode: -1044, isLoaded: true });
          }
          this.setClientLoaded();
        },
        accessToken,
      }),
    );
  }

  componentDidUpdate(prevProps) {
    super.componentDidUpdate(prevProps);

    if (this.props.blockItemOffers) {
      if (!this.state.senderInfo) {
        const eatery = this.props.blockItemOffers.get('eatery');
        const city = getCityByIdOrNameOrSlug(eatery.address?.city);

        const senderInfo = {
          customerName: eatery.name,
          customerPhone: eatery.phone,
          lat: eatery.lat,
          lng: eatery.lng,
          address: eatery.address && eatery.address.full,
          districtId: getDistrictId(eatery.address && eatery.address.district),
          suggestedAddress: eatery.address && eatery.address.full,
          suggestedDistrictId: getDistrictId(eatery.address && eatery.address.district),
          cityId: city?.id || 50,
        };
        this.updateShippingInfo(senderInfo, true);
      }

      if (!this.state.receiverInfo) {
        const receiverInfo = this.props.shippingAddresses && this.props.shippingAddresses[0];
        receiverInfo && this.updateShippingInfo(receiverInfo);
      }
    }
  }

  renderErrorPageBackup = (isAuthorized = false, errorCode = undefined) => {
    const notEnoughInformation = this.state.errorType === 'not enough information';

    return (
      <div>
        <div className="step-unauthorized">
          <div className="page-merchant page-merchant-manager">
            <div className="alone-message">
              <h3>{this.getString('buy_with_lozi')}</h3>

              {notEnoughInformation && (
                <p>{this.getString('sorry_the_seller_did_not_provided_enough_information_order_cancelled')}</p>
              )}
              {!notEnoughInformation && !isAuthorized && (
                <p>{this.getString('sorry_you_are_not_authorized_to_access_this_page')}</p>
              )}
              {!notEnoughInformation && isAuthorized && (
                <p>{this.getString('sorry_your_requested_order_is_not_exist')}</p>
              )}

              {!!errorCode && <p>{this.getString('error_code_0', '', [errorCode])}</p>}
              <p>
                {this.getString('if_you_think_this_is_a_bug_please_contact_us_via')}
                <a href="mailto:hotroloship@lozi.vn" title={this.getString('Contact us')}>
                  hotroloship@lozi.vn
                </a>
                {this.getString('for_assistance')}
              </p>
              <p>{this.getString('thank_you_for_choosing_lozi')}</p>
            </div>
          </div>
        </div>

        <Footer location={this.props.location} />
      </div>
    );
  };

  renderErrorPage = () => {
    return (
      <div>
        <div className="step-unauthorized">
          <div className="page-merchant page-merchant-manager">
            <div className="alone-message">
              <p>{this.getString('temporary_error_message_for_testing')}</p>
            </div>
          </div>
        </div>
      </div>
    );
  };

  renderStep = (step) => {
    const { data, merchant, currentUser } = this.props;
    const { senderInfo, receiverInfo, isLoaded, errorCode } = this.state;

    if (step === STEP_VIEW_DETAIL) {
      return (
        <OrderDetail
          params={{ order: this.getQuery().code, accessToken: cookie.get('tempData'), serviceName: 'lozi' }}
          location={this.props.location}
          history={this.props.history}
        />
      );
    }

    const dishesData = data && currentUser && (data[currentUser.get('username')] || {});
    const dishes = dishesData && Array.isArray(dishesData.data) && dishesData.data;
    const hasDishes = Array.isArray(dishes) && dishes.length > 0;

    const loziConfig = {
      shippingFee: this.state.shippingFee,
      currentShippingFee: this.state.currentShippingFee,
      minimumShippingFee: this.state.minimumShippingFee,
      extraFees: this.state.extraFees,
      promotionEnable: this.state.promotionEnable,
      availablePaymentMethods: AVAILABLE_PAYMENT_METHODS,
    };

    if (!(data && currentUser && (hasDishes || step === STEP_CONFIRM))) return this.renderErrorPage(!!currentUser);
    if (isLoaded && !!errorCode) return this.renderErrorPage(!!currentUser, errorCode);
    if (step === STEP_ADDRESS) {
      return (
        <StepAddress
          step={step}
          toNextStep={() => this.changeStep(STEP_CONFIRM)}
          goBack={() => this.changeStep(STEP_INFO, true)}
          serviceName="lozi"
          // address + distance
          shippingAddress={receiverInfo}
          merchantAddress={senderInfo}
          shippingAddresses={this.props.shippingAddresses}
          updateShippingInfo={this.updateShippingInfo}
          // config
        />
      );
    }

    if (step === STEP_CONFIRM) {
      return <StepConfirm {...this.computeConfirmData()} {...loziConfig} serviceName="lozi" />;
    }

    return (
      <StepInfo
        data={dishes[0]}
        merchant={merchant}
        handleChangeQuantity={this.handleChangeQuantity}
        toNextStep={() => this.changeStep(STEP_ADDRESS)}
      />
    );
  };

  render() {
    if (!this.state.isClientLoaded) return false;

    const { step } = this.state;
    return <div className="order-page order-page-buy">{this.renderStep(step)}</div>;
  }

  setClientLoaded = (extras = {}) => {
    this.setState({ isClientLoaded: true, ...extras });
  };

  handleChangeQuantity = (value) => {
    this.props.dispatch(
      cartUpdateQuantity({
        merchant: this.props.merchant.get('slug'),
        itemIndex: 0,
        user: this.props.currentUser.toObject(),
        quantity: value,
      }),
    );
    this.forceUpdate();
  };
}

export default initComponent(BuyPage, withRouter);
