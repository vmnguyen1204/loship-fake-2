/* eslint-disable */

import BaseComponent from 'components/base';
import StickyAppBanner from 'components/shared/banners/banner-sticky';
import PropTypes from 'prop-types';
import React from 'react';
import Helmet from 'react-helmet';
import { getRoute } from 'utils/routing';
import * as seo from 'utils/seo';

function getShouldRenderStickyBanner(match) {
  if (!match || !match.params) return true;

  const { match: realMatch } = (typeof window !== 'undefined' && getRoute(null, window.location.pathname)) || {};
  if (!realMatch || !realMatch.params) return true;
  if (!realMatch.params.order) return true;

  if (match.params.order === realMatch.params.order) return true;
  return false;
}

// This component defines common methods of Component in Route
// Ex: Track GA for page access, Update metadata for SEO,...
export default class BasePage extends BaseComponent {
  constructor(props, context = {}) {
    super(props, context);

    this.seoOptions = { ...context.seo, location: props.location };
    this.injectRender();
  }

  static contextTypes = { isMobile: PropTypes.bool };

  // Override this method to pass custom data props
  getMetadata = (props, seoOptions) => {
    return seoOptions;
  };

  injectRender = () => {
    const _render = this.render;
    this.render = () => {
      const metadata = this.updateMetadata(this.props);
      if (!metadata) return _render && _render.call(this);
      const shouldRenderStickyBanner = this.context.isMobile && getShouldRenderStickyBanner(this.props.match);
      return (
        <div>
          <Helmet {...seo.head(metadata)} />
          {_render && _render.call(this)}
          {shouldRenderStickyBanner ? <StickyAppBanner location={this.props.location} metadata={this.metadata} /> : false}
        </div>
      );
    };
  };

  updateMetadata = (props) => {
    this.seoOptions = (props.getMetadata && props.getMetadata(props, this.seoOptions)) || this.getMetadata(props, this.seoOptions);
    this.metadata = seo.metadata(this.seoOptions, this.metadata);
    return this.metadata;
  };
}
