import React from 'react';
import { getPartnerConfig } from 'reducers/metadata';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';

class MomoGatewayContent extends BaseComponent {
  static defaultProps = { dataGetter: getPartnerConfig };

  state = { isAuthorized: true };

  componentDidMount() {
    const { data } = this.props;
    if (!data || data.get('partner') !== 'momo' || !data.get('signature')) {
      return this.setState({ isAuthorized: false });
    }

    return this.props.history.push('/');
  }

  render() {
    if (this.state.isAuthorized) return false;
    return (
      <div className="gateway-err">
        <div className="alone-message">
          <h3>{this.getString('Đặt hàng Loship cùng MoMo')}</h3>
          <p>{this.getString('Xin lỗi quý khách, đã có lỗi xảy ra dẫn đến việc quý khách không thể đặt hàng được.')}</p>
          <p>
            {this.getString('Vui lòng liên hệ email ')}
            <a href="mailto:hotroloship@lozi.vn" title={this.getString('Liên hệ Loship')}>
              hotroloship@lozi.vn
            </a>
            {this.getString(' để được hỗ trợ.')}
          </p>
          <p>
            {this.getString('Loship biết quý khách có nhiều lựa chọn. Cảm ơn quý khách đã chọn Loship ngày hôm nay.')}
          </p>
        </div>
      </div>
    );
  }
}

export default initComponent(MomoGatewayContent);
