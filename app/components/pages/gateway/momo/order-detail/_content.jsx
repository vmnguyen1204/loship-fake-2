import { LIST_PAYMENT_STATUS, LIST_PAYMENT_TEXT } from 'utils/json/payment';

import Footer from 'components/shared/footer';
import Hint from 'components/statics/hint';
import MobileNavbar from 'components/shared/mobile-nav';
import OrderDetailBase from 'components/shared/order/detail/detail-base';
import React from 'react';
import StepProcess from 'components/shared/step-process/order-detail';
import { addCurrency, phoneNumber } from 'utils/format';
import { getProcessStep } from 'utils/cart';
import initComponent from 'lib/initComponent';

// TODO: outdate
class OrderDetailComponent extends OrderDetailBase {
  renderSummary = ({ total, paymentMethod, paymentStatus }) => {
    return (
      <div className="section cta">
        <div className="summary">
          <b>{this.getString('total')}</b>
          <br />
          <span className="highlight">
            <b>{addCurrency(total)}</b>
          </span>
        </div>

        <div className="summary text-right text-light">
          <b>{this.getString(LIST_PAYMENT_TEXT[paymentMethod])}</b>
          <br />
          {paymentStatus !== 'refunded' && (
            <span className={`price bold payment-${paymentStatus}`}>
              <b>{this.getString(LIST_PAYMENT_STATUS[paymentStatus])}</b>
            </span>
          )}
          {paymentStatus === 'refunded' && (
            <Hint
              text={[
                this.getString(
                  ['fullpayment', 'CC'].includes(paymentMethod)
                    ? 'Loship will refund to your bank account / card within 48 hours, after order cancelling.'
                    : 'Loship will refund to your MoMo Wallet within 30 minutes after order cancelling.',
                  'order',
                ),
                this.getString('in_case_it_takes_longer_than_noted_please_contact_us_for_assistant', 'order'),
                <span key="email">
                  {this.getString('email', 'order')}
                  <a href="mailto:hotroloship@lozi.vn">hotroloship@lozi.vn</a>
                </span>,
                <span key="hotline">
                  {this.getString('hotline', 'order')}
                  <a href="tel:1900638058">{phoneNumber('1900638058', { injectZero: false })}</a>
                </span>,
                this.getString('thank_you_and_sorry_for_this_inconvenience_we_hope_to_serve_you_next_time', 'order'),
              ]}
            >
              <span className={`price bold payment-${paymentStatus}`}>
                <b>{this.getString(LIST_PAYMENT_STATUS[paymentStatus])}</b>&nbsp;
              </span>
              <div className="question">?</div>
            </Hint>
          )}
        </div>
      </div>
    );
  };

  renderRefundStatus = () => {
    const { data } = this.props;
    const paymentMethod = data.get('paymentMethod');

    return (
      <div className="refund-note">
        <h4>{this.getString('payment_refunding_notice', 'order')}</h4>
        <p>
          {this.getString(
            ['fullpayment', 'CC'].includes(paymentMethod)
              ? 'Loship will refund to your bank account / card within 48 hours, after order cancelling.'
              : 'Loship will refund to your MoMo Wallet within 30 minutes after order cancelling.',
            'order',
          )}
        </p>
        <p>{this.getString('in_case_it_takes_longer_than_noted_please_contact_us_for_assistant', 'order')}</p>
        <p>
          {this.getString('email', 'order')}
          <a href="mailto:hotroloship@lozi.vn">hotroloship@lozi.vn</a>
        </p>
        <p>
          {this.getString('hotline', 'order')}
          <a href="tel:1900638058">{phoneNumber('1900638058', { injectZero: false })}</a>
        </p>
      </div>
    );
  };

  render() {
    const { data, status: loadingStatus } = this.props;

    if (!data && !loadingStatus) return null;
    if (loadingStatus === 'PENDING') return this.renderLoadingPage();
    if (loadingStatus !== 'SUCCESS') return this.renderErrorPage(true);

    const status = data.get('status');
    const processStep = getProcessStep(status);
    const { title: cancelNoteTitle, message: cancelNoteMessage } = this.getCancelNote();
    const paymentStatus = data.get('paymentStatus');

    return (
      <div>
        <div className="order-page">
          <div className="order-step-detail detail">
            <MobileNavbar
              className="red"
              title={(
                <div className="information-code">
                  <span className="label">{this.getString('order_code')}</span>
                  <b>#{data.get('code')}</b>
                </div>
              )}
            />
            <div className="scroll-container">
              <div className="detail">
                {processStep === -1 ? (
                  <div className="cancel-note">
                    <h4>{cancelNoteTitle}</h4>
                    <p>{cancelNoteMessage}</p>
                  </div>
                ) : (
                  <StepProcess step={processStep} />
                )}
                {paymentStatus === 'refunded' && this.renderRefundStatus()}
                {processStep >= 2 && this.renderShipperInfo(data.get('shipper'))}
                {this.renderShippingInfo()}
                {(!!data.get('note') || !!data.get('notePhoto')) && (
                  <div className="section note">
                    <div className="content">
                      <p>
                        {this.getString('note')} <span>{data.get('note') || '--'}</span>
                      </p>
                      {!!data.get('notePhoto') && (
                        <div className="btn-attach-photo uploaded" onClick={this.openPhotoPopup}>
                          <i className="lz lz-photo" /> {this.getString('photo_attached')}
                        </div>
                      )}
                    </div>
                  </div>
                )}
                {this.renderOrderDetail({
                  lines: data.get('lines'),
                  lineGroups: data.get('lineGroups'),
                  shipPrice: data.get('shippingFee'),
                  distance: data.get('routes') && data.get('routes').distance,
                  extraFees: data.get('extraFees'),
                  paymentFee: data.get('paymentFee'),
                  promotion: data.get('promotion'),
                  discount: data.get('discount'),
                })}
                {this.renderSummary({
                  total: data.get('totalUserFee'),
                  paymentMethod: data.get('paymentMethod'),
                  paymentStatus: data.get('paymentStatus'),
                })}

                <div className="hotline-notice">
                  {this.getString('if_you_are_not_happy_with_our_service_please_contact_us_at_email')}
                  <b>
                    <a>hotroloship@lozi.vn</a>
                  </b>
                  {this.getString('for_support')}
                </div>
              </div>
              <Footer location={this.props.location} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(OrderDetailComponent);
