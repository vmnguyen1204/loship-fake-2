import React from 'react';
import BasePage from 'components/pages/base';
import initComponent from 'lib/initComponent';
import OrderDetail from './_content';

if (process.env.BROWSER) {
  require('assets/styles/pages/order.scss');
  require('assets/styles/pages/order-mobile.scss');
  require('assets/styles/pages/order-history.scss');
  require('assets/styles/pages/order-history-mobile.scss');
}

class MoMoOrderDetailPage extends BasePage {
  render() {
    return <OrderDetail {...this.props} params={{ ...this.getParams(), partnerName: 'momo' }} />;
  }
}

export default initComponent(MoMoOrderDetailPage);
