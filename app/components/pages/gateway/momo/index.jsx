import BasePage from 'components/pages/base';
import React from 'react';
import MomoGatewayContent from './_content';

class MomoGateway extends BasePage {
  render() {
    return <MomoGatewayContent history={this.props.history} getString={this.getString} />;
  }
}

export default MomoGateway;
