import React from 'react';
import BasePage from 'components/pages/base';
import initComponent from 'lib/initComponent';

if (process.env.BROWSER) {
  require('assets/styles/pages/gateway.scss');
}

class GatewayPage extends BasePage {
  render() {
    return (
      <div>
        <div className="container">{this.props.children}</div>
      </div>
    );
  }
}

export default initComponent(GatewayPage);
