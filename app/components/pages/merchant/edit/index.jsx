import BaseComponent from 'components/base';
import SelectCityDistrict from './cities-districts';
import StepAddress from 'components/shared/order/address';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { getCurrentUser } from 'reducers/user';
import { LINK_AUTH, LINK_DOMAIN, ROLES_CONTENT } from 'settings/variables';
import { hasRole } from 'utils/access';
import { produce } from 'utils/helper';
import { getDefaultMatrix, getOperatingTimes } from 'utils/time';
import MerchantImage from './avatar/_merchant-avatar';
import MerchantCategories from './category';
import OperatingTimeTable from './operate_time';
import { fetchCategoriesBySuperCategoryCityIdDistrictId } from 'actions/category';
import Button from 'components/shared/button';
import { Helmet } from 'react-helmet';
import callAPI from 'actions/helpers';
import { phoneNumber as formatPhoneNumber, getRawPhoneNumber } from 'utils/format';

if (process.env.BROWSER) {
  require('assets/styles/pages/create-merchant.scss');
}

const initialState = {
  data: {
    address: {},
    image: null,
    name: '',
    phoneNumber: '',
    operatingTime: getDefaultMatrix(),
  },
};

class MerchantEditPage extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();
      const currentUser = getCurrentUser(state, params);
      if (currentUser) {
        data = data.set('hasRoleContent', hasRole(currentUser.get('data'), ROLES_CONTENT));
      }
      return data;
    },
    customRender: true,
  };

  state = produce(initialState);

  componentDidMount() {
    this.handleRedirect();
  }

  componentDidUpdate() {
    const { data } = this.state;
    const id = data.address?.cityId;
    const superCategoryId = data.superCategoryId;
    if (id && superCategoryId) {
      this.props.dispatch(
        fetchCategoriesBySuperCategoryCityIdDistrictId({
          superCategoryId,
          cityId: id,
        }),
      );
    }
    this.handleRedirect();
  }

  render() {
    const { data, slug, isCreateSuccess, isCreating } = this.state;
    const { hasRoleContent } = this.props;
    if (!hasRoleContent) return null;
    return (
      <div className='page-create-merchant'>
        <Helmet title={this.getString('loship_create_merchant_page')} />
        <h1>{this.getString('loship_create_merchant')}</h1>
        <div className="form-content">
          <div className="merchant-avatar">
            <MerchantImage
              image={data.image}
              setImage={(image) => this.handleChange('image', image)}
            />
          </div>
          <div className="form">
            <div className="merchant-info">
              <div className="merchant-name">
                <label>{this.getString('merchant_name')}</label>
                <input
                  value={data.name}
                  placeholder={this.getString('input_merchant_name')}
                  onChange={(e) => this.handleChange('name', e.target.value)}
                  name='name'
                  autoComplete="off"
                />
              </div>

              <div className="phone-number">
                <label>{this.getString('merchant_phone_number')}</label>
                <input
                  value={data.phoneNumber}
                  placeholder="0939.115.xxx"
                  onChange={(e) => this.handleChange('phoneNumber', e.target.value)}
                  autoComplete="off"
                />
              </div>
            </div>

            <div className="merchant-address">
              <label>{this.getString('merchant_address')}</label>
              <StepAddress
                shippingAddress={{ lat: 10.7715684, lng: 106.6509818 }}
                onAddressArrived={(shippingAddress) => this.handleChange('address', shippingAddress)}
                hideCurrentPosition={true}
              />
            </div>

            <SelectCityDistrict
              onCityArrived={(city) => {
                this.handleChange('city', city);
              }}
              onDistrictArrived={(district) => {
                this.handleChange('district', district);
              }}
              address={data.address}
            />

            <MerchantCategories
              onServiceArrived={(superCategoryId) => {
                this.handleChange('superCategoryId', superCategoryId);
              }}
              onCategoriesArrived={(categories) =>
                this.handleChange('categories', categories)}
              cityId={data.address.cityId}
            />

            <div className="operate-time">
              <div className="title">{this.getString('operating_time')}</div>
              <OperatingTimeTable
                data={data.operatingTime}
                updateOperatingTime={this.handleChange.bind(this, 'operatingTime')}
                updateInactiveWeekdays={this.handleChange.bind(this, 'inactiveWeekdays')}
                onSelect={this.handleChange.bind(this, 'open24', false)}
                unchecked={data.inactiveWeekdays}
              />
            </div>

            <div className="form-footer">
              <Button type="feature" onClick={this.handleCreateMerchant}>
                {!isCreateSuccess ? this.getString('create_merchant')
                  : <a href={LINK_DOMAIN + `/b/${slug}/menu`}>{this.getString('go_to_menu_management')}</a>}
              </Button>
              {isCreating && <i className="status fas fa-spinner fa-spin" />}
              {isCreateSuccess && <i className="status fas fa-check" />}
              {<div className="form-message">{this.renderMessage()}</div>}
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleRedirect = () => {
    const { hasRoleContent } = this.props;
    if (!hasRoleContent) return this.props.history.replace('/');
  };

  handleChange = (field, value) => {
    const data = produce(this.state.data, (draft) => {

      if (field === 'city' || field === 'district') {
        draft.address[`${field}Id`] = value.id;
        draft.address[field] = value.name;
      }
      else if (field === 'phoneNumber') draft[field] = formatPhoneNumber(value.replace(/\D/g, ''));

      else draft[field] = value;
    });

    this.setState({ data });
  };

  checkValid = () => {
    const { data } = this.state;

    let isValid = true;
    let errMessage = '';

    if (!data.categories?.length) {
      errMessage = this.getString('need_select_merchant_categories');
      isValid = false;
    }
    if (!data.superCategoryId) {
      errMessage = this.getString('ship_service_not_empty');
      isValid = false;
    }
    if (!data.address?.districtId) {
      errMessage = this.getString('district_not_empty');
      isValid = false;
    }
    if (!data.address?.cityId) {
      errMessage = this.getString('city_province_not_empty');
      isValid = false;
    }
    if (!data.address?.placeName) {
      errMessage = this.getString('address_not_empty');
      isValid = false;
    }
    if (!data.phoneNumber) {
      errMessage = this.getString('phone_number_not_empty');
      isValid = false;
    }
    if (!data.name) {
      errMessage = this.getString('merchant_name_not_empty');
      isValid = false;
    }
    if (!isValid) {
      this.setState({ message: errMessage });
      return false;
    }

    return true;
  }

  handleCreateMerchant = () => {
    if (!this.checkValid()) return;
    const { data } = this.state;
    const { address = {}, categories, name, phoneNumber, operatingTime, image } = data;
    this.setState({ isCreating: true },
      async () => {
        const res = await callAPI('post', {
          url: LINK_AUTH + '/eateries',
          content: {
            categories,
            name,
            address: {
              city: address.city,
              district: address.district,
              street: address.placeName,
            },
            phoneNumber: getRawPhoneNumber(phoneNumber),
            cityId: address.cityId,
            districtId: address.districtId,
            latitude: address.lat,
            longitude: address.lng,
            capacityInt: 0,
            closed: false,
            operatingTime: getOperatingTimes(operatingTime),
          },
        });
        if (!res || res.status !== 200) return alert('create_merchant_failed');
        const { id, slug } = res.body.data;
        if (image) {
          const imageURL = await image.getURL();
          await callAPI('put', {
            url: LINK_AUTH + `/eateries/${id}/avatar`,
            content: { image: imageURL },
          });
        }
        this.setState({ isCreating: false, isCreateSuccess: true, message: 'OK', slug });
      },
    );
  }

  renderMessage = () => {
    const { message } = this.state;

    if (message !== 'OK') return <span>{message}</span>;

    if (message === 'OK') return <span className="success-message">{this.getString('create_merchant_success')}</span>;
  }
}

export default initComponent(MerchantEditPage);
