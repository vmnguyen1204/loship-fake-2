import BaseComponent from 'components/base';
import Checkbox from 'components/shared/checkbox';
import Select from 'components/shared/select/select-time';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { iife } from 'utils/helper';
import timeList from 'utils/json/time24';
import weekdays from 'utils/json/weekday';
import moment from 'utils/shims/moment';
import { getOriginMatrix, getTimeFormatFromValue, getTimeValueInWeek, isOpenAllWeek, reduceMatrix } from 'utils/time';

const TimeRange = ({ valueLinkFrom, valueLinkTo }) => {
  const parsedTimeList = timeList.map((timeItem) => ({ value: timeItem.name, text: timeItem.value }));
  return (
    <div className="time-range">
      <Select data={parsedTimeList} {...valueLinkFrom} />
      <span className="connector" />
      <Select data={parsedTimeList} {...valueLinkTo} />
    </div>
  );
};
TimeRange.propTypes = {
  valueLinkFrom: PropTypes.func.isRequired,
  valueLinkTo: PropTypes.func.isRequired,
  placeholderFrom: PropTypes.string,
  placeholderTo: PropTypes.string,
};

const MINUTES_IN_DAY = 24 * 60;

class OperatingTimeTable extends BaseComponent {
  static propTypes = {
    updateOperatingTime: PropTypes.func.isRequired,
    updateInactiveWeekdays: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
  };

  render() {
    const { data } = this.props;
    const reducedData = reduceMatrix(data);

    return (
      <div className="operating-time">
        <Checkbox isRadio={true} checked={true} className="reverse d-inline-flex">
          {this.getString('fixed_operating_time')}
        </Checkbox>

        {isOpenAllWeek(data) && !this.state.isShowFullWeek ? (
          <table className="operating-time-table">
            <tbody>
              {iife(() => {
                const currentData = reducedData[0] || [];
                const hasFirst = currentData.length >= 1;
                const hasSecond = currentData.length >= 2;

                return (
                  <tr>
                    <th scope="row" colSpan={1} className="with-action">
                      {this.getString('all_week')}
                      <br />
                      <a onClick={this.toggleShowMore}>{this.getString('plus_show_more')}</a>
                    </th>
                    {hasFirst && (
                      <td className="operating-time-item">
                        <TimeRange
                          valueLinkFrom={this.valueLinkAllWeek(0, 0)}
                          valueLinkTo={this.valueLinkAllWeek(0, 1)}
                          titleFrom={this.getString('open_time')}
                          titleTo={this.getString('close_time')}
                        />
                        <div className="btn-close" onClick={this.removeOperatingTime(0, 0, true)}>
                          <i className="lz lz-close" />
                        </div>
                      </td>
                    )}
                    {hasSecond && (
                      <td className="operating-time-item">
                        <TimeRange
                          valueLinkFrom={this.valueLinkAllWeek(1, 0)}
                          valueLinkTo={this.valueLinkAllWeek(1, 1)}
                          titleFrom={this.getString('open_time')}
                          titleTo={this.getString('close_time')}
                        />
                        <div className="btn-close" onClick={this.removeOperatingTime(0, 1, true)}>
                          <i className="lz lz-close" />
                        </div>
                      </td>
                    )}

                    {(!hasFirst || !hasSecond) && (
                      <td className="operating-time action" colSpan={hasFirst ? 1 : 2}>
                        <a onClick={this.addOperatingTime(0, true)}>
                          + {this.getString('add_hours_of_service')}
                        </a>
                      </td>
                    )}
                  </tr>
                );
              })}
            </tbody>
          </table>
        ) : (
          <table className="operating-time-table">
            <tbody>
              <tr>
                <th className="with-action">
                  <a onClick={this.toggleShowMore}>{this.getString('minus_show_less')}</a>
                </th>
              </tr>
              {weekdays.map(({ name, slug: day }, index) => {
                const currentData = reducedData[index] || [];

                const hasFirst = currentData.length >= 1;
                const hasSecond = currentData.length >= 2;

                return (
                  <tr key={`operating-time-${day}`}>
                    <th scope="row" colSpan={1}>
                      <Checkbox className="reverse d-inline-flex" {...this.checkedLink(index)}>
                        {this.getString(name, 'eateryForm')}
                      </Checkbox>
                    </th>

                    {hasFirst && (
                      <td className="operating-time-item">
                        <TimeRange
                          valueLinkFrom={this.valueLink(index, 0, 0)}
                          valueLinkTo={this.valueLink(index, 0, 1)}
                          titleFrom={this.getString('open_time')}
                          titleTo={this.getString('close_time')}
                        />
                        <div className="btn-close" onClick={this.removeOperatingTime(index, 0)}>
                          <i className="lz lz-close" />
                        </div>
                      </td>
                    )}
                    {hasSecond && (
                      <td className="operating-time-item">
                        <TimeRange
                          valueLinkFrom={this.valueLink(index, 1, 0)}
                          valueLinkTo={this.valueLink(index, 1, 1)}
                          titleFrom={this.getString('open_time')}
                          titleTo={this.getString('close_time')}
                        />
                        <div className="btn-close" onClick={this.removeOperatingTime(index, 1)}>
                          <i className="lz lz-close" />
                        </div>
                      </td>
                    )}

                    {(!hasFirst || !hasSecond) && (
                      <td className="operating-time action" colSpan={hasFirst ? 1 : 2}>
                        <a onClick={this.addOperatingTime(index)}>
                        + {this.getString('add_hours_of_service', 'eateryForm')}
                        </a>
                      </td>
                    )}
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
      </div>
    );
  }

  valueLink = (dayIndex, index, orderIndex) => {
    const { data } = this.props;
    const reducedData = reduceMatrix(data);
    const currentData = reducedData[dayIndex];
    let time = (currentData && currentData[index]) || [0, 0];
    const value = getTimeFormatFromValue(time[orderIndex], 'H:mm');

    return {
      value: orderIndex === 1 && value === '0:00' ? '24:00' : value,
      onChange: (e) => {
        const value = e.target.value;
        time = time.slice(0, orderIndex).concat(
          getTimeValueInWeek(moment(`${dayIndex} ${value}`, 'e H:mm')),
          time.slice(orderIndex + 1),
        );
        currentData[index] = time;

        this.props.updateOperatingTime(getOriginMatrix(reducedData, false));
      },
    };
  };

  valueLinkAllWeek = (index, orderIndex) => {
    const { data } = this.props;
    const reducedData = reduceMatrix(data);
    const currentData = reducedData[0];
    let time = (currentData && currentData[index]) || [0, 0];
    const value = getTimeFormatFromValue(time[orderIndex], 'H:mm');

    return {
      value: orderIndex === 1 && value === '0:00' ? '24:00' : value,
      onChange: (e) => {
        const value = e.target.value;
        time = time.slice(0, orderIndex).concat(
          getTimeValueInWeek(moment(`${0} ${value}`, 'e H:mm')),
          time.slice(orderIndex + 1),
        );
        currentData[index] = time;

        weekdays.forEach((_, index) => {
          reducedData[index] = currentData.map((item) => {
            return [item[0] + index * MINUTES_IN_DAY, item[1] + index * MINUTES_IN_DAY];
          });
        });

        this.props.updateOperatingTime(getOriginMatrix(reducedData, false));
      },
    };
  };

  checkedLink = (dayIndex) => {
    const unchecked = this.props.unchecked || {};
    const { data } = this.props;
    const reducedData = reduceMatrix(data);

    return {
      checked: !Array.isArray(reducedData[dayIndex]) || reducedData[dayIndex].length === 0 ? false : !unchecked[dayIndex],
      onChange: (value) => {
        this.props.updateInactiveWeekdays({ ...unchecked, [dayIndex]: !value });

        if (!unchecked[dayIndex] && (!Array.isArray(reducedData[dayIndex]) || reducedData[dayIndex].length === 0)) {
          this.addOperatingTime(dayIndex)();
        }
      },
    };
  };

  addOperatingTime = (dayIndex, isAllWeek = false) => () => {
    const { data } = this.props;
    const reducedData = reduceMatrix(data);

    if (!isAllWeek) {
      let currentData = reducedData[dayIndex];
      if (!Array.isArray(currentData)) currentData = [];
      if (currentData.length >= 2) return;

      const value = getTimeValueInWeek(moment(`${dayIndex} 0:00`, 'e h:mm'));
      currentData.push([value, value]);
      reducedData[dayIndex] = currentData;
    } else {
      let currentData = reducedData[0];
      if (!Array.isArray(currentData)) currentData = [];
      if (currentData.length >= 2) return;

      const value = getTimeValueInWeek(moment(`${0} 0:00`, 'e h:mm'));
      currentData.push([value, value]);

      weekdays.forEach((_, index) => {
        reducedData[index] = currentData.map((item) => {
          return [item[0] + index * MINUTES_IN_DAY, item[1] + index * MINUTES_IN_DAY];
        });
      });
    }

    this.props.updateOperatingTime(getOriginMatrix(reducedData, false));
  };

  removeOperatingTime = (dayIndex, index, isAllWeek = false) => () => {
    const { data } = this.props;
    const reducedData = reduceMatrix(data);

    if (!isAllWeek) {
      const currentData = reducedData[dayIndex];
      if (currentData.length < index) return;
      currentData.splice(index, 1);
    } else {
      const currentData = reducedData[0];
      if (currentData.length < index) return;
      currentData.splice(index, 1);

      weekdays.forEach((_, index) => {
        reducedData[index] = currentData.map((item) => {
          return [item[0] + index * MINUTES_IN_DAY, item[1] + index * MINUTES_IN_DAY];
        });
      });
    }

    this.props.updateOperatingTime(getOriginMatrix(reducedData, false));
  };

  toggleShowMore = () => {
    const nextStatus = !this.state.isShowFullWeek;

    if (nextStatus === false) {
      const { data } = this.props;
      const reducedData = reduceMatrix(data);
      const currentData = reducedData[0];

      weekdays.forEach((_, index) => {
        reducedData[index] = currentData.map((item) => {
          return [item[0] + index * MINUTES_IN_DAY, item[1] + index * MINUTES_IN_DAY];
        });
      });

      this.props.updateOperatingTime(getOriginMatrix(reducedData, false));
    }

    this.setState({ isShowFullWeek: nextStatus });
  };
}

export default initComponent(OperatingTimeTable);
