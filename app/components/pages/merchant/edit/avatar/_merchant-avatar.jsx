import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import FileUploader from 'components/statics/uploader';

class MerchantImage extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      imageThumbnail: props.preloadImage,
      imageThumbnailUrl: props.preloadImage,
    };
  }

  static defaultProps = {
    customRender: true,
    isAllowedToCopyDishImage: false,
  };

  clearImage = () => {
    this.setState({
      imageThumbnail: null,
      imageThumbnailUrl: null,
    });
    this.props.setImage(null);
  };

  handleFileUpload = (data, thumb) => {
    this.props.setImage(data);
    this.setState({ imageThumbnail: thumb });
  };

  renderThumbImage = ({ imageThumbnail }) => {
    return (
      <div className="dish-image-input">
        <div>
          <img src={imageThumbnail} alt="Thumbnail" />
        </div>
        <a className="btn-clear-dish-image" onClick={this.clearImage}>
          <i className="fa fa-times" />
        </a>
      </div>
    );
  };

  renderUpload = () => {
    return (
      <div className="upload-wrapper">
        <div>
          <FileUploader className="btn-upload-wrapper" onChange={this.handleFileUpload} accept="image/*" type="dish">
            <Button type="optional">{this.getString('add_avatar')}</Button>
          </FileUploader>
        </div>
      </div>
    );
  };

  render() {
    const { imageThumbnail, imageThumbnailUrl } = this.state;


    if (imageThumbnail || imageThumbnailUrl) {
      return this.renderThumbImage({ imageThumbnail: imageThumbnailUrl || imageThumbnail });
    }

    return this.renderUpload();
  }
}

export default initComponent(MerchantImage);
