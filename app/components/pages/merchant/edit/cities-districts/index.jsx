import BaseComponent from 'components/base';
import { JSON } from 'globalthis/implementation';
import initComponent from 'lib/initComponent';
import React from 'react';
import { slugify } from 'utils/format';
import cities from 'utils/json/cities';
import districts from 'utils/json/districts';

class SelectCityDistrict extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
      districts: [],
    };
  }

  async componentDidUpdate(prevProps) {
    const { address } = this.props;
    if (JSON.stringify(prevProps.address) !== JSON.stringify(this.props.address)) {
      await this.setState({
        city: { id: address.cityId, name: address.city },
        district: { id: address.districtId, name: address.district },
        districts: districts.filter((district) => district.cityId === address.cityId),
      });
    }
  }

  onFocusCityInput = () => {
    this.setState({
      isFocusCityInput: true,
      city: { id: undefined, name: '' },
      district: { id: undefined, name: '' },
    }, async () => {
      await this.props.onCityArrived({ id: undefined, name: '' });
      await this.props.onDistrictArrived({ id: undefined, name: '' });
    });
  };

  onFocusDistrictInput = () => {
    this.setState({
      isFocusDistrictInput: true,
      district: { id: undefined, name: '' },
    });
  };

  clickOutSide = () => {
    setTimeout(() => {
      this.setState({ isFocusCityInput: false, isFocusDistrictInput: false });
    }, 200);
  }

  selectCities = ({ id, name }) => {
    if (!id || !name) return;
    const districtsByCities = districts.filter((district) => district.cityId === id);
    return this.setState({
      city: { id, name },
      districts: districtsByCities,
    }, () => this.props.onCityArrived({ id, name }),
    );
  }

  selectDistricts = ({ id, name }) => {
    if (!id || !name) return;
    return this.setState({ district: { id, name } },
      () => this.props.onDistrictArrived({ id, name }),
    );

  }

  sortBigFourCity = () => {
    const rawArr = [];
    cities.filter((item) => {
      if (item.slug === 'ha-noi' || item.slug === 'ho-chi-minh' || item.slug === 'can-tho' || item.slug === 'da-nang') {
        rawArr.unshift(item);
      } else rawArr.push(item);
    });
    return rawArr;
  }

  handleSearch = (e) => {
    const result = e.target.value;
    const { name } = e.target;
    if (name === 'cities') {
      let filterResult = [...cities];
      filterResult = cities.filter((item) => slugify(item.name).indexOf(slugify(result)) !== -1);
      this.setState({
        filterResult,
        isSearchCity: true,
        city: { id: undefined, name: result },
      });
    }

    if (name === 'districts') {
      const data = this.state.districts;
      let filterDistricts = [...data];
      filterDistricts = data.filter((item) => slugify(item.name).indexOf(slugify(result)) !== -1);
      this.setState({
        filterDistricts,
        isSearchDistrict: true,
        district: { id: undefined, name: result },
      });
    }
  }

  renderInputCities = () => {
    const { isFocusCityInput, filterResult, isSearchCity, cities: cits } = this.state;
    let data = this.sortBigFourCity();
    if (isSearchCity) data = [...filterResult];
    const newCities = data.filter((cit) => cits.findIndex((item) => item.id === cit.id) === -1);
    return [
      <div className="cities-input" key={1}>
        <label>Tỉnh/Thành Phố *</label>
        <input
          className="input-search-cities"
          onChange={this.handleSearch}
          type="text"
          onFocus={this.onFocusCityInput}
          onBlur={this.clickOutSide}
          placeholder={this.getString('select_city_province')}
          name="cities"
          value={this.state.city?.name}
          autoComplete="off"
        />
        {isFocusCityInput && (
          <div className="list-cities" key={2}>
            {newCities?.map((item) => {
              const { id, name } = item;
              return (
                <div className="item-city" onClick={() => this.selectCities({ id, name })} key={id}>
                  {item.name}
                </div>
              );
            })}
          </div>
        )}
      </div>,
    ];
  }

  renderInputDistricts = () => {
    const { isFocusDistrictInput, city, filterDistricts, isSearchDistrict, districts: dstr } = this.state;
    const disabled = !!city?.id;
    let data = dstr;
    if (isSearchDistrict) data = [...filterDistricts];
    return [
      <div className="districts-input" key={1}>
        <label>Quận/Huyện *</label>
        <input
          className="input-search-districts"
          onChange={this.handleSearch}
          type="text"
          onFocus={this.onFocusDistrictInput}
          onBlur={this.clickOutSide}
          placeholder={this.getString('select_district')}
          name="districts"
          value={this.state.district?.name}
          disabled={!disabled}
          autoComplete="off"
        />
        {isFocusDistrictInput && (
          <div className="list-districts" key={2}>
            {data?.map((item) => {
              const { id, name } = item;
              return (
                <div className="item-district" onClick={() => this.selectDistricts({ id, name })} key={id}>
                  {item.name}
                </div>
              );
            })}
          </div>
        )}
      </div>,
    ];
  }

  render() {
    return (
      <div className="cities-districts">
        {this.renderInputCities()}
        {this.renderInputDistricts()}
      </div>
    );
  }
}

export default initComponent(SelectCityDistrict);
