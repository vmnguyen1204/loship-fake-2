import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import React from 'react';
import CategoryConfig from 'utils/json/loshipServices';
import AddCategoriesContent from './_content';
import { slugify } from 'utils/format';

class MerchantCategories extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      showCategory : false,
      services: [],
    };
  }

  handleFocus = () => {
    this.setState({ isShowServices: true });
  }

  handleBlur = () => {
    setTimeout(() => {
      this.setState({ showCategory: false, isShowServices: false });
    }, 200);
  }

  selectService = (item) => {
    this.setState({ service: item, isShowServices: false });
    this.props.onServiceArrived(item.superCategoryId);
  }

  renderSelectedService = () => {
    const { service } = this.state;
    return (
      <div className="selected-service">
        {service.title}
        {<i onClick={() => this.setState({ service: null, isShowServices: true })} className="fa fa-times" />}
      </div>
    );
  }
  handleSearch = (e) => {
    const results = e.target.value;
    let filterResult = [...CategoryConfig];
    filterResult = CategoryConfig.filter((item) => slugify(item.name).indexOf(slugify(results)) !== -1);
    this.setState({ filterResult, isSearch: true });
  }

  callBackCategories = () => {
    let { categories } = this.state;
    const arrCategories = categories?.map((item) => item.value);
    categories = [...arrCategories];
    this.props.onCategoriesArrived(categories);
  }

  renderServices = () => {
    const { isSearch, filterResult, services: svr } = this.state;
    let data = CategoryConfig;
    if (isSearch) data = [...filterResult];
    const newServices = data.filter((cat) => svr.findIndex((item) => item.id === cat.id) === -1);
    return (
      <div className="list-services">
        {(newServices || []).map((item) => {
          return (
            <div className="item-service" onClick={() => this.selectService(item)} key={item.id}>
              {item.title}
            </div>
          );
        })}
      </div>
    );
  }
  render() {
    const { service, isShowServices } = this.state;
    const { cityId } = this.props;
    const superCategoryId = service?.superCategoryId;

    return (
      <div className="services-categories">
        <div className="services">
          <label>{this.getString('services_label')}</label>
          {!this.state.service ? (
            <input
              onChange={this.handleSearch}
              onFocus={this.handleFocus}
              onBlur={this.handleBlur}
              type="text" name="category"
              placeholder={this.getString('select_service')}
              ref="input"
            />
          ) : (
            this.renderSelectedService()
          )}
          {isShowServices && this.renderServices()}
        </div>
        <div className="categories">
          <label>{this.getString('categories_label')}</label>
          <AddCategoriesContent
            onDataArrived={(categories) => {
              this.setState({ categories }, () => this.callBackCategories());
            }}
            service={service}
            params={{ superCategoryId, cityId }}
          />
        </div>
      </div>
    );
  }
}

export default initComponent(MerchantCategories);
