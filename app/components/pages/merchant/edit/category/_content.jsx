import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import { slugify } from 'utils/format';
import { CUSTOM_CONTENT, open } from 'actions/popup';
import { getCategoriesBySuperCategoryIdCityIdDistrictId } from 'reducers/category';
import ImageLazy from 'components/statics/img-lazy';

class AddCategoriesContent extends BaseComponent {

  constructor(props) {
    super(props);
    this.state = {
      categories: [],
    };
  }

  static defaultProps = {
    dataGetter: (state, params) => {
      const data = {};
      const categories = getCategoriesBySuperCategoryIdCityIdDistrictId(state, params)?.toJS().data;
      if (!categories) return;
      data.categories = categories;
      return data;
    },
    customRender: true,
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(this.props.service) !== JSON.stringify(prevProps.service)) {
      this.setState({ categories: [] },
        () => this.props.onDataArrived(this.state.categories),
      );
    }
  }

  onFocus = () => {
    this.setState({ isFocus: true });
  };

  clickOutSide = () => {
    setTimeout(() => {
      this.setState({ isFocus: false });
    }, 300);
  }

  selectCategories = ({ id, value }) => {
    if (!id || !value) return;
    const { categories } = this.state;
    if (categories.length >= 3) return this.alertLimitPopup(true);
    categories.push({ id, value });
    this.setState({ categories }, () => {
      this.refs.input.value = '';
      this.props.onDataArrived(categories);
    });
  }

  renderCategoriesSelected = () => {
    return this.state.categories?.map((item) => {
      return (
        <ul key={item.id}>
          <li className="item-selected" onClick={() => {this.removeItem(item.id);}}>
            {item.value}
            <i className="fa fa-times" />
          </li>
        </ul>
      );
    });
  }

  removeItem = (id) => {
    const { categories } = this.state;
    if (categories.length === 1) return this.alertLimitPopup();
    const newCategories = categories?.filter((item) => item.id !== id);
    return this.setState({ categories: newCategories }, () => this.props.onDataArrived(newCategories));
  }

  alertLimitPopup = (isMaximum) => {
    return this.props.dispatch(
      open(CUSTOM_CONTENT, {
        content: isMaximum ? this.getString('select_maximum_3_category') : this.getString('your_merchant_must_have_a_little_type'),
      }),
    );
  };

  handleSearch = (e) => {
    const results = e.target.value;
    const { categories } = this.props;
    if (!categories) return;
    let filterResult = [...categories];
    filterResult = categories.filter((item) => slugify(item.value).indexOf(slugify(results)) !== -1);
    this.setState({ filterResult, isSearch: true });
  }

  renderCategories = () => {
    const { isFocus, isSearch, filterResult, categories: cats } = this.state;
    let { categories, service } = this.props;
    if (isSearch) categories = [...filterResult];

    const newCategories = categories?.filter((cat) => cats.findIndex((item) => item.id === cat.id) === -1);
    const hasService = !!service?.id;
    return [
      <div className="categories-manage" key={1}>
        <input
          className="input-search-categories"
          onChange={this.handleSearch}
          type="text"
          onFocus={this.onFocus}
          onBlur={this.clickOutSide}
          placeholder={this.getString('click_get_to_select_categories')}
          ref="input"
          disabled={!hasService}
        />
        {isFocus && (
          <div className="list-categories scroll-nice" key={3}>
            {newCategories?.map((item) => {
              const { id, value } = item;
              return (
                <div className="item-category" onClick={() => this.selectCategories({ id, value })} key={id}>
                  <ImageLazy
                    src={item.imageWeb}
                    placeholder="/dist/images/dish-placeholder.png"
                    size={10}
                    shape="round-square"
                  />
                  {item.value}
                </div>
              );
            })}
          </div>
        )}
      </div>,
    ];
  }

  render() {
    return (
      <div className="submenu-categories">
        {this.renderCategories()}
        <div className="list-selected">{this.renderCategoriesSelected()}</div>
      </div>
    );
  }
}

export default initComponent(AddCategoriesContent);
