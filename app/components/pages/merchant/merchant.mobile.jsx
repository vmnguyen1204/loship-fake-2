import { STEP_ADDRESS, STEP_CONFIRM, STEP_INFO, STEP_SEARCH } from 'components/shared/order';

import Footer from 'components/shared/footer';
import GroupController from 'components/shared/order/cart/group-controller.mobile';
import LoshipPartnerBanner from 'components/shared/banners/banner-loship-partner';
import MobileNavbar from 'components/shared/mobile-nav';
import OrderCart from 'components/shared/order/cart';
import React from 'react';
import StepAddress from 'components/shared/order/address';
import StepConfirm from 'components/shared/order/confirm';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItemLanding } from 'components/shared/newsfeed/merchant-item';
import { isMerchantReady } from 'utils/merchant';
import MerchantSearch from './_search';
import { MerchantPromo } from './_promo';
import MerchantPageBase from './merchant-base';
import MerchantMenu from './_menu';
import MerchantHeader from './_header';

class MerchantPage extends MerchantPageBase {
  renderStep = (step) => {
    const { cartMetadata = {}, currentUser, merchant } = this.props;
    const { data: { lockedUser = [] } = {} } = cartMetadata;
    const isLocked = currentUser && lockedUser[currentUser.get('username')];

    const { senderInfo, receiverInfo } = this.state;

    const closed = merchant.get('closed');
    const isActive = merchant.get('isActive');
    const isCheckedIn = merchant.get('isCheckedIn');

    const eateryId = merchant.get('id');
    const isLoshipPartner = typeof merchant.get('isLoshipPartner') !== 'undefined' ? !!merchant.get('isLoshipPartner') : null;

    const params = this.getParams();

    const serviceName = this.context && this.context.partnerName !== 'none' ? this.context.partnerName : this.state.srcServiceName; // prefer partner, then reorder's srcServiceName

    const merchantConfig = {
      shippingFee: this.state.shippingFee,
      currentShippingFee: this.state.currentShippingFee,
      minimumShippingFee: this.state.minimumShippingFee,
      extraFees: this.state.extraFees,
      promotionEnable: this.state.promotionEnable,
    };
    const isReady = isMerchantReady(merchant.toObject());

    if (step === STEP_ADDRESS) {
      return (
        <StepAddress
          params={params}
          eateryId={eateryId}
          step={step}
          toNextStep={() => this.changeStep(STEP_CONFIRM)}
          goBack={() => this.changeStep(STEP_INFO, true)}
          goBackText={this.getString('back_to_menu')}
          serviceName={serviceName}
          // address + distance
          shippingAddress={receiverInfo}
          merchantAddress={senderInfo}
          shippingAddresses={this.props.shippingAddresses}
          updateShippingInfo={this.updateShippingInfo}
        />
      );
    }

    if (step === STEP_CONFIRM) {
      return <StepConfirm {...this.computeConfirmData()} {...merchantConfig} serviceName={serviceName} />;
    }

    if (step === STEP_SEARCH) {
      return (
        <MerchantSearch
          closed={closed}
          isActive={isActive}
          isCheckedIn={isCheckedIn}
          isLocked={isLocked}
          shippingService={merchant.get('shippingService')}
          params={{
            groupTopic: this.getParams().groupTopic,
            merchant: merchant.get('slug'),
          }}
          goBack={() => this.changeStep(STEP_INFO, true)}
        />
      );
    }

    return (
      <div itemScope itemType="http://schema.org/FoodEstablishment" className="page-merchant">
        <MerchantHeader
          params={{
            groupTopic: this.getParams().groupTopic,
            merchant: merchant.get('slug'),
          }}
          data={merchant}
          extraFees={this.state.extraFees}
          onSearch={() => this.changeStep(STEP_SEARCH, true)}
        />

        {!isReady && (
          <SectionNewsfeed
            title={this.getString('suggest_similar_merchant_near_you')}
            params={{
              page: 1,
              limit: 6,
              hideEmpty: true,
              searchSuggestEateriesApi: true,
              eateryId: merchant.get('id'),
              globalAddress: this.props.globalAddress,
            }}
            history={this.props.history}
            location={this.props.location}
            changePage={(page) => this.submit({ page })}
            submit={this.submitSimilarMerchant}
            mode="preload|horizontal|see_all"
            MerchantItem={EateryItemLanding}
          />
        )}

        {this.isAllowedToSeeManager() && isLoshipPartner === false && <LoshipPartnerBanner />}

        <MerchantPromo
          params={{ eateryId: merchant.get('id') }}
          eateryId={merchant.get('id')}
          merchant={merchant}
          freeShippingMilestone={merchant.get('freeShippingMilestone')}
        />
        <GroupController merchant={merchant} eateryId={merchant.get('id')} />

        <MerchantMenu
          closed={closed}
          isActive={isActive}
          isCheckedIn={isCheckedIn}
          isLocked={isLocked}
          shippingService={merchant.get('shippingService')}
          params={{
            ...this.getParams(),
            merchant: merchant.get('slug'),
          }}
          merchantName={merchant.get('name')}
        />

        <OrderCart
          params={{
            groupTopic: this.getParams().groupTopic,
            merchant: merchant.get('slug'),
          }}
          mode="merchant"
          toNextStep={() => this.changeStep(STEP_ADDRESS)}
          {...merchantConfig}
        />
        <div className="clearfix" />

        <Footer location={this.props.location} />
      </div>
    );
  };

  render() {
    if (!this.props.merchant || !this.state.isLoaded) return this.renderLoading();

    const { step } = this.state;
    return (
      <div>
        {step !== STEP_INFO && step !== STEP_ADDRESS && step !== STEP_SEARCH && (
          <MobileNavbar {...this.getNavBarProps()} />
        )}

        <div id="mc-container" className="order-page">
          {this.renderStep(step)}
        </div>
      </div>
    );
  }

  getNavBarProps = () => {
    const { step } = this.state;
    const shouldHideBackArrow = ['viettelpay'].includes(this.context.partnerName);

    if (step === STEP_ADDRESS) {
      return {
        title: this.getString('delivery_information'),
        backHandler: shouldHideBackArrow ? null : () => this.changeStep(STEP_INFO, true),
      };
    }

    if (step === STEP_CONFIRM) {
      return {
        title: this.getString('order_information'),
        backHandler: shouldHideBackArrow ? null : () => this.changeStep(STEP_ADDRESS, true),
      };
    }
  };
}

export default initComponent(MerchantPage, withRouter);
