import { fetchItemById } from 'actions/buy';
import { fetchMerchant } from 'actions/merchant';
import BasePage from 'components/pages/base';
import NotFoundPage from 'components/pages/not-found';
import produce from 'immer';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getItemById } from 'reducers/buy';
import { getMerchant } from 'reducers/merchant';
import { getCurrentUser } from 'reducers/user';
import * as SEOTYPE from 'settings/seo-types';
import loziServices from 'utils/json/loziServices';
import Desktop from './merchant';
import Mobile from './merchant.mobile';

class Merchant extends BasePage {
  static contextTypes = {
    partnerName: PropTypes.string,
    isMobile: PropTypes.bool,
  };

  static needs = [fetchMerchant, fetchItemById];

  static defaultProps = {
    // actionCreator: fetchMerchant,
    dataGetter: (state, params) => {
      let data = new Map();

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      params.itemId = params.itemId || params.pItem;
      const currentItem = getItemById(state, params);
      if (currentItem) {
        data = data.set('currentItem', currentItem.get('data'));
        const merchant = currentItem.getIn(['data', 'eatery']);
        if (merchant) data = data.merge(getMerchant(state, { merchant: merchant.slug, merchantUsername: merchant.username }));
      } else {
        data = data.merge(getMerchant(state, params));
      }

      return data;
    },
    getMetadata: (props, seoOptions) => {
      return produce(seoOptions, (draft) => {
        draft.type = (() => {
          if (props.currentItem) return SEOTYPE.ITEM;
          if (props.match.params.groupTopic) return SEOTYPE.GROUP_ORDER;
          return SEOTYPE.MERCHANT;
        })();
        draft.item = props.currentItem;
        draft.merchant = props.data;
        draft.groupTopic = props.match.params.groupTopic;
      });
    },
    customRender: true,
    queryParams: ['pItem'],
  };

  componentDidMount() {
    const elem = document.getElementById('screen');
    elem.scrollTop = 0;

    const params = this.getParams();
    if (!this.props.currentUser && params.eateryId) {
      return this.props.history.replace('/');
    }

    if (this.props.currentItem) {
      const merchant = this.props.currentItem.get('eatery') || {};
      params.merchantUsername = merchant.username;
      params.merchant = merchant.slug;
    }

    if (!this.props.data) {
      if (this.context.partnerName !== 'none') {
        const service = loziServices.find((s) => s.name === this.context.partnerName);
        if (service) params.additionalHeaders = { 'X-Loship-Service': service.appApiHeader };
      }

      this.props.dispatch(
        fetchMerchant({
          ...params,
          callback: (data) => {
            if (params.eateryId) this.props.history.push(data.username ? `/${data.username}` : `/b/${data.slug}`);
          },
        }),
      );
    }
  }

  render() {
    const { status } = this.props;
    if (status === 'ERROR') return <NotFoundPage />;
    if (this.context.isMobile) return <Mobile />;
    return (
      <div>
        <div className="container">
          <Desktop />
          <div className="clearfix" />
        </div>
      </div>
    );
  }
}

export default initComponent(Merchant, withRouter);
