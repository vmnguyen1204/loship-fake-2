import {
  checkPromotion,
  fetchCampaignById,
  fetchCampaignListByEateryId,
  fetchPromotionByCode,
  fetchPromotionListByEateryId,
} from 'actions/merchant';
import { GENERIC_POPUP, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Popup from 'components/shared/popup';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  getCampaignById,
  getCampaignListByEateryId,
  getMerchant,
  getPromotionByCode,
  getPromotionListByEateryId,
  getSearchedPromotion,
} from 'reducers/merchant';
import { copyToClipboard } from 'utils/content';
import { addCurrency, phoneNumber } from 'utils/format';
import { iife } from 'utils/helper';
import { LIST_PAYMENT_TYPES } from 'utils/json/payment';
import { isLoshipClients } from 'utils/merchant';
import moment from 'utils/shims/moment';

class MerchantPromoComp extends BaseComponent {
  render() {
    const { merchant, freeShippingMilestone } = this.props;
    if (!merchant) return null;

    const promotionCampaigns = merchant.get('promotionCampaigns') || [];
    const promotions = merchant.get('promotions') || [];
    const count = merchant.get('count');
    const freeship = iife(() => {
      if (!freeShippingMilestone || freeShippingMilestone <= 0) return 0;
      return Math.max(Math.floor(freeShippingMilestone / 100) / 10, 0.1);
    });

    const promotionCount = count.promotion + count.promotionCampaign;
    const nonPromotionCount = iife(() => {
      let nonPromocount = 0;
      if (freeship > 0) nonPromocount++;

      return nonPromocount;
    });
    if (promotionCount === 0 && nonPromotionCount === 0) return null;

    return (
      <div className="card mc-promo">
        <div className="card-heading">
          {this.getString('promotions')}
          {promotionCount > 0 && (
            <a className="view-all" onClick={this.handleViewPromoList}>
              {this.getString('view_more')}
              {' '}
              (
              {promotionCount}
              )
            </a>
          )}
        </div>
        <div className="card-content">
          {freeship > 0 && (
            <div className="card-item flex-item">
              <i className="lz lz-fw lz-marker-o" />
              <div className="flex-1 text-trimmed">
                {this.getString('free_shipping_under_0', '', [freeship])}
              </div>
            </div>
          )}
          {promotionCampaigns.map((promotion) => {
            const { id, type, value } = promotion;
            if (!['fixed_price'].includes(type)) return null;
            return (
              <div
                key={id}
                className="card-item flex-item mc-promo-item"
                onClick={this.handleViewPromoDetail(promotion)}
              >
                <i className="lz lz-fw lz-discount" />
                <div className="flex-1 text-trimmed">
                  {this.getString('promo_fixed_price', '', [addCurrency(Math.ceil(value / 1000) * 1000)])}
                </div>
                <i className="fa fa-chevron-right" />
              </div>
            );
          })}
          {promotions.map((promotion) => {
            const {
              id,
              code,
              value,
              promotionType,
              isLimitMinimumOrderValue,
              minimumOrderValue,
              isLimitPaymentType,
              paymentTypes = [],
              minimumUserToApply = 1,
            } = promotion;

            const promotionStr = (() => {
              let strPostfix = '';
              return [
                this.getString('promo_off_short', '', [promotionType === 'percent' ? value + '%' : addCurrency(value)]),
                ...(() => {
                  if (minimumUserToApply >= 2) { return [this.getString('promo_min_member_short' + strPostfix, '', [minimumUserToApply])]; }
                  return [
                    (() => {
                      if (!isLimitMinimumOrderValue) return null;
                      const str = this.getString('promo_min_total_short' + strPostfix, '', [
                        addCurrency(minimumOrderValue),
                      ]);
                      strPostfix = '_2nd';
                      return str;
                    })(),
                    (() => {
                      if (!isLimitPaymentType) return null;
                      const strVar = paymentTypes
                        .map((type) => this.getString(LIST_PAYMENT_TYPES[type] + '_short'))
                        .join(', ');
                      const str = this.getString('promo_payment_type_short' + strPostfix, '', [strVar]);
                      strPostfix = '_2nd';
                      return str;
                    })(),
                  ];
                })(),

                this.getString('promo_use_code', '', [code]),
              ].join('');
            })();

            return (
              <div
                key={id}
                className="card-item flex-item mc-promo-item"
                onClick={this.handleViewPromoDetail(promotion)}
              >
                <i className={cx('lz lz-fw', minimumUserToApply < 2 ? 'lz-discount' : 'lz-discount-group')} />
                <div className="flex-1 text-trimmed" title={promotionStr}>
                  {promotionStr}
                </div>
                <i className="fa fa-chevron-right" />
              </div>
            );
          })}
        </div>
      </div>
    );
  }

  handleViewPromoList = () => {
    const {
      eateryId,
      location: { pathname },
    } = this.props;
    const query = this.getQuery();
    query.pPromo = eateryId;

    this.props.history.push(`${pathname}?${qs.stringify(query)}`);
  };

  handleViewPromoDetail = (promotion) => () => {
    if (!promotion) return;

    const { location: { pathname } } = this.props;
    const query = this.getQuery();
    if (promotion.type === 'fixed_price') query.pPromo = `${promotion.id}_campaign`;
    else query.pPromo = promotion.code;

    this.props.history.push(`${pathname}?${qs.stringify(query)}`);
  };
}

class MerchantPromoPopupComp extends BaseComponent {
  static contextTypes = { isMobile: PropTypes.bool };

  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      if (params.eateryId) {
        const merchant = getMerchant(state, params);
        if (merchant) data = data.set('merchant', merchant.get('data'));

        const promotionCampaigns = getCampaignListByEateryId(state, params);
        if (promotionCampaigns) data = data.set('promotionCampaigns', promotionCampaigns.get('data'));

        const promotions = getPromotionListByEateryId(state, params);
        if (promotions) data = data.set('promotions', promotions.get('data'));
      }

      if (params.promotionCode) {
        const promotion = getPromotionByCode(state, params);
        if (promotion) data = data.set('promotion', promotion.get('data'));
      }

      if (params.campaignId) {
        const promotionCampaign = getCampaignById(state, params);
        if (promotionCampaign) data = data.set('promotionCampaign', promotionCampaign.get('data'));
      }

      const searchedPromotion = getSearchedPromotion(state);
      if (searchedPromotion) data = data.set('searchedPromotion', searchedPromotion.get('data'));

      return data;
    },
    customRender: true,
  };

  componentDidMount() {
    const params = this.getParams();
    if (params.promotionCode) this.props.dispatch(fetchPromotionByCode(params));
    if (params.campaignId) this.props.dispatch(fetchCampaignById(params));
    if (params.eateryId) {
      this.props.dispatch(fetchCampaignListByEateryId(params));
      this.props.dispatch(fetchPromotionListByEateryId(params));
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.searchValue !== prevProps.searchValue) {
      this.props.dispatch(
        checkPromotion({
          promotionCode: this.props.searchValue.toUpperCase(),
          eateryId: this.props.merchant && this.props.merchant.get('id'),
          mode: 'SEARCH',
        }),
      );
    }
  }

  renderPromotionList = () => {
    const { merchant, mode, promotionCampaigns, promotions } = this.props;
    const freeship = iife(() => {
      const freeShippingMilestone = merchant.get('freeShippingMilestone');
      if (!freeShippingMilestone || freeShippingMilestone <= 0) return 0;
      return Math.max(Math.floor(freeShippingMilestone / 100) / 10, 0.1);
    });

    const PromotionCampaignList = (list) => {
      if (!list) return null;
      return list.map((promotion) => {
        const { id, type, value } = promotion.toJS();
        if (!['fixed_price'].includes(type)) return null;
        return (
          <div key={id} className="card-item flex-item promotion-item">
            <i className="lz lz-fw lz-discount" />
            <div className="flex-1">
              <h4>{this.getString('promo_fixed_price', '', [addCurrency(Math.ceil(value / 1000) * 1000)])}</h4>
              <div className="action">
                <Button type="link" onClick={this.handleViewPromoDetail(promotion)}>
                  {this.getString('view_condition', 'order')}
                </Button>
              </div>
            </div>
          </div>
        );
      });
    };
    const PromotionList = (list) => {
      if (!list) return null;
      return list.map((promotion) => {
        const {
          id,
          code,
          value,
          maxDiscount,
          promotionType,
          isLimitMinimumOrderValue,
          minimumOrderValue,
          isLimitPaymentType,
          paymentTypes = [],
          minimumUserToApply = 1,
        } = promotion.toJS();

        const promotionStr = (() => {
          let strPostfix = '';
          return [
            this.getString('promo_off', '', [
              promotionType === 'percent' ? value + '%' : addCurrency(value),
              promotionType === 'percent' ? this.getString('discount_x_maximum', '', [addCurrency(maxDiscount)]) : '',
            ]),
            ...(() => {
              if (minimumUserToApply >= 2) { return [this.getString('promo_min_member' + strPostfix, '', [minimumUserToApply])]; }
              return [
                (() => {
                  if (!isLimitMinimumOrderValue) return null;
                  const str = this.getString('promo_min_total' + strPostfix, '', [addCurrency(minimumOrderValue)]);
                  strPostfix = '_2nd';
                  return str;
                })(),
                (() => {
                  if (!isLimitPaymentType) return null;
                  const strVar = paymentTypes
                    .map((type) => this.getString(LIST_PAYMENT_TYPES[type] + '_short'))
                    .join(', ');
                  const str = this.getString('promo_payment_type' + strPostfix, '', [strVar]);
                  strPostfix = '_2nd';
                  return str;
                })(),
              ];
            })(),
          ].join('');
        })();

        return (
          <div key={`promotion-${id}`} className="card-item flex-item promotion-item">
            <i className={cx('lz lz-fw', minimumUserToApply < 2 ? 'lz-discount' : 'lz-discount-group')} />
            <div className="flex-1">
              <h4>{promotionStr}</h4>
              <small>{code}</small>
              <div className="action">
                <Button type="link" onClick={this.handleViewPromoDetail(promotion)}>
                  {this.getString('view_condition', 'order')}
                </Button>
                <Button className="btn-sm" onClick={this.handleApplyPromotion(promotion.toJS())}>
                  {this.getString('use')}
                </Button>
              </div>
            </div>
          </div>
        );
      });
    };

    if (this.props.searchValue) {
      if (this.props.searchedPromotion) { return <div className="section">{PromotionList([this.props.searchedPromotion])}</div>; }
      return (
        <div className="section not-found">
          <img src="/dist/images/promo-not-found.png" alt="No promo" />

          <h3 className="text-dark-gray">{this.getString('promotion_not_found')}</h3>
          <p className="text-dark-gray">{this.getString('promotion_not_found_message')}</p>
        </div>
      );
    }

    if (mode === 'SEARCH') {
      return promotions && promotions.size > 0 ? (
        <div className="section">
          <h4 className="mb-16">{this.getString('promotion_eatery')}</h4>

          {PromotionCampaignList(promotionCampaigns)}
          {PromotionList(promotions)}
        </div>
      ) : null;
    }

    return (
      <div className="section">
        {mode !== 'SEARCH' &&
          (freeship > 0 && (
            <div className="card-item flex-item promotion-item">
              <i className="lz lz-fw lz-marker-o" />
              <h4>
                {this.getString('free_shipping_under_0', '', [freeship])}
              </h4>
            </div>
          ))}
        {mode !== 'SEARCH' && PromotionCampaignList(promotionCampaigns)}

        {mode === 'SEARCH' && <h4 className="mb-16">{this.getString('promotion_eatery')}</h4>}
        {!!promotions && PromotionList(promotions)}
      </div>
    );
  };

  renderPromotionDetail = () => {
    const promotion = this.state.currentPromotion || this.props.promotion || this.props.promotionCampaign;
    if (!promotion) return null;

    const {
      code,
      value,
      maxDiscount,
      type,
      promotionType,
      isLimitMinimumOrderValue,
      minimumOrderValue,
      isLimitPaymentType,
      paymentTypes = [],
      minimumUserToApply = 1,
      groupDishes = [],
      isFirstTimeOrder,
      isFirstTimeCode,
      perUserActiveTimes,
      clientIds,
      activeFrom,
      activeTo,
      dailyActiveFrom,
      dailyActiveTo,
    } = promotion.toJS();

    const activeTimesStr = this.getString(
      perUserActiveTimes === 1 ? 'once' : (perUserActiveTimes === 2 && 'twice') || '{0} times',
      '',
      [`${perUserActiveTimes}`.padStart(2, '0')],
    );
    const paymentTypeStr = paymentTypes.map((paymentType) => this.getString(LIST_PAYMENT_TYPES[paymentType] + '_short')).join(', ');

    const promotionStr = (() => {
      if (type === 'fixed_price') { return this.getString('promo_fixed_price', '', [addCurrency(Math.ceil(value / 1000) * 1000)]); }

      let strPostfix = '';
      return [
        this.getString('promo_off', '', [
          promotionType === 'percent' ? value + '%' : addCurrency(value),
          promotionType === 'percent' ? this.getString('discount_x_maximum', '', [addCurrency(maxDiscount)]) : '',
        ]),
        ...(() => {
          if (minimumUserToApply >= 2) { return [this.getString('promo_min_member' + strPostfix, '', [minimumUserToApply])]; }
          return [
            (() => {
              if (!isLimitMinimumOrderValue) return null;
              const str = this.getString('promo_min_total' + strPostfix, '', [addCurrency(minimumOrderValue)]);
              strPostfix = '_2nd';
              return str;
            })(),
            (() => {
              if (!isLimitPaymentType) return null;

              const str = this.getString('promo_payment_type' + strPostfix, '', [paymentTypeStr]);
              strPostfix = '_2nd';
              return str;
            })(),
          ];
        })(),
      ].join('');
    })();

    return (
      <Popup
        className="generic-popup confirm-popup mc-promo-popup"
        handleClose={this.handleCloseViewPromoDetail}
        animationStack={3}
      >
        <div className="popup-header">
          <Button className={cx('btn-back bg-transparent')} onClick={this.handleCloseViewPromoDetail}>
            <i className={cx('lz', this.context.isMobile ? 'lz-light-arrow-left' : 'lz-close')} />
          </Button>
          {this.getString('promotion_detail')}
        </div>
        <div className={cx('content', type !== 'fixed_price' && 'action')}>
          <div className="mc-promo-popup-content">
            <div className="section">
              <img src="/dist/images/promo-img.png" alt="Promo" />
              {code && (
                <input
                  ref={this.setCodeRef}
                  onClick={this.handleCopyCode(code)}
                  type="text"
                  className="code"
                  value={code}
                  readOnly
                />
              )}

              <h3 className={cx('promotion-info', type === 'fixed_price' && 'text-center')}>{promotionStr}</h3>
            </div>
            <div className="section">
              <h4>{this.getString('promotion_duration')}</h4>
              <div>
                {moment(activeFrom).format('D/M/YYYY')}
                {' '}
                -
                {' '}
                {moment(activeTo).format('D/M/YYYY')}
                {(dailyActiveFrom > 0 || dailyActiveTo < 86399) &&
                  ', ' +
                    this.getString('promotion_duration_info', '', [
                      moment()
                        .startOf('day')
                        .add(dailyActiveFrom, 'seconds')
                        .format('HH:mm'),
                      moment()
                        .startOf('day')
                        .add(dailyActiveTo, 'seconds')
                        .format('HH:mm'),
                    ])}
              </div>

              {type === 'fixed_price' ? (
                <h4 className="promotion-condition">{this.getString('campaign_conditions')}</h4>
              ) : (
                <h4 className="promotion-condition">{this.getString('promotion_conditions')}</h4>
              )}

              {type === 'fixed_price' ? (
                <ul className="group">
                  <div>{this.getString('campaign_group_dish_applied')}</div>
                  {groupDishes.map((groupDish) => (
                    <li key={groupDish.id}>{groupDish.name}</li>
                  ))}
                  <div>{this.getString('not_allow_other_promotion')}</div>
                </ul>
              ) : (
                <ul>
                  <li>
                    {this.getString('promo_off', '', [
                      promotionType === 'percent' ? value + '%' : addCurrency(value),
                      promotionType === 'percent'
                        ? this.getString('discount_x_maximum', '', [addCurrency(maxDiscount)]) : '',
                    ])}
                  </li>
                  {minimumUserToApply >= 2 && (
                    <li>{this.getString('applied_for_min_member', '', [minimumUserToApply])}</li>
                  )}
                  {isLimitMinimumOrderValue && (
                    <li>{this.getString('applied_for_min_total', '', [addCurrency(minimumOrderValue)])}</li>
                  )}
                  {isLimitPaymentType && <li>{this.getString('applied_for_payment_types', '', [paymentTypeStr])}</li>}
                  {isFirstTimeOrder && <li>{this.getString('applied_for_first_time_order_only')}</li>}
                  {isFirstTimeCode && <li>{this.getString('applied_for_first_time_using_a_promotion_code_only')}</li>}
                  {perUserActiveTimes && perUserActiveTimes !== -1 && (
                    <li>{this.getString('applied_x_times_per_customer', '', [activeTimesStr])}</li>
                  )}
                  {isLoshipClients(clientIds) && (
                    <li>
                      {this.getString('applied_for_loship_app')}
                      {' '}
                      <a
                        target="_blank"
                        rel="noopener"
                        href="https://itunes.apple.com/vn/app/loship-ship-%C4%91%E1%BB%93-%C4%83n-r%E1%BA%A5t-nhanh/id1348206645"
                      >
                        iOS
                      </a>
                      {' | '}
                      <a target="_blank" rel="noopener" href="https://play.google.com/store/apps/details?id=lozi.loship_user">
                        Android
                      </a>
                    </li>
                  )}
                </ul>
              )}

              <h4 className="contact-info">{this.getString('contact_loship_cs')}</h4>
              <div>
                {this.getString('order_failed_contact_info', '', [
                  <b key="phone">{phoneNumber('1900638058', { injectZero: false })}</b>,
                  <b key="email">hotroloship@lozi.vn</b>,
                  <b key="facebook">facebook.com/LoshipVN</b>,
                ])}
              </div>
            </div>

            {type !== 'fixed_price' && (
              <div className="section-cta">
                <Button type="primary" fw onClick={this.handleApplyPromotion(promotion.toJS())}>
                  {this.getString('use_now')}
                </Button>
              </div>
            )}
          </div>
        </div>
      </Popup>
    );
  };

  render() {
    const isViewingPromoDetail = (() => {
      if (this.props.isViewingPromoDetail) return true;
      if (this.state.currentPromotion) return true;
      return false;
    })();

    return (
      <div className="mc-promo-popup-content">
        {isViewingPromoDetail ? this.renderPromotionDetail() : this.renderPromotionList()}
      </div>
    );
  }

  setCodeRef = (ref) => {
    if (ref) this.codeRef = ref;
  };

  handleCopyCode = (code) => () => {
    copyToClipboard(code);
    toast(this.getString('copied_promotion'));
    if (this.codeRef) this.codeRef.select();
  };

  handleApplyPromotion = (promotion) => () => {
    this.props.dispatch({ type: 'PROMOTION_FETCH', data: promotion });
    this.handleCloseViewPromoDetail();
  };

  handleViewPromoDetail = (promotion) => () => {
    const viewPromoDetailCallback = (res = {}) => {
      const { body: { data } = {} } = res;
      this.setState({ currentPromotion: data ? new Map(data) : promotion });
    };
    typeof this.props.allowedClose === 'function' && this.props.allowedClose(false);

    if (promotion.get('missingData')) {
      if (promotion.get('type') === 'fixed_price') {
        return this.props.dispatch(
          fetchCampaignById({ campaignId: promotion.get('id'), callback: viewPromoDetailCallback }),
        );
      }
      if (promotion.get('code')) {
        return this.props.dispatch(
          fetchPromotionByCode({ promotionCode: promotion.get('code'), callback: viewPromoDetailCallback }),
        );
      }
    }

    return viewPromoDetailCallback();
  };

  handleCloseViewPromoDetail = () => {
    typeof this.props.allowedClose === 'function' && this.props.allowedClose(true);

    if (this.state.currentPromotion) return this.setState({ currentPromotion: null });
    typeof this.props.handleClose === 'function' && this.props.handleClose();
  };
}
const MerchantPromoPopup = initComponent(MerchantPromoPopupComp);

export const MerchantPromo = initComponent(MerchantPromoComp, withRouter);

export function MerchantPromoPopupHandler({ isViewingPromoDetail, callback }) {
  if (!this || !this.props) return;

  const query = this.getQuery();
  const popupParams = isViewingPromoDetail
    ? {
      promotionCode: !query.pPromo.includes('_campaign') && query.pPromo,
      campaignId: query.pPromo.includes('_campaign') && query.pPromo.split('_')[0],
    }
    : { eateryId: query.pPromo };

  this.props.dispatch(
    open(GENERIC_POPUP, {
      title: this.getString('promotion_eatery'),
      content: (popupProps) => (
        <MerchantPromoPopup
          params={popupParams}
          isViewingPromoDetail={isViewingPromoDetail}
          mode={query.step === 'confirm-order' && 'SEARCH'}
          {...popupProps}
        />
      ),
      className: 'mc-promo-popup',
      animationStack: 3,
      mode: query.step === 'confirm-order' && 'SEARCH',
      searchPlaceholder: this.getString('type_promotion_here'),
      searchFormatter: (value) => value.toUpperCase(),
      onCancel: () => {
        delete query.pPromo;
        if (query.step === 'confirm-order') delete query.step;
        this.props.history.push(this.props.location.pathname + '?' + qs.stringify(query));

        typeof callback === 'function' && callback();
        this.props.dispatch({ type: 'MERCHANT_PROMOTION_SEARCH_CLEAR' });
      },
    }),
  );
}
