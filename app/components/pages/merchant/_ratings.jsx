import { CUSTOM_CONTENT, open } from 'actions/popup';
import { getMerchant, getMerchantRatings } from 'reducers/merchant';

import BaseComponent from 'components/base';
import Carousel from 'components/shared/carousel';
import ExpandContainer from 'components/shared/expand-container';
import ImageLazy from 'components/statics/img-lazy';
import React from 'react';
import Spinner from 'components/statics/spinner';
import { Waypoint } from 'react-waypoint';
import cx from 'classnames';
import { fetchMerchantRatings } from 'actions/merchant';
import initComponent from 'lib/initComponent';
import moment from 'utils/shims/moment';

class MerchantRatings extends BaseComponent {
  static defaultProps = {
    actionCreator: fetchMerchantRatings,
    dataGetter: (state, params) => {
      let data = getMerchantRatings(state, params);
      if (!data) return null;

      const merchant = getMerchant(state, params);
      if (merchant) data = data.set('merchant', merchant.get('data'));

      return data;
    },
    customRender: true,
  };

  renderEmpty = () => {
    return (
      <div className="merchant-rating-item text-center">
        <img src="/dist/images/not-found-404.png" alt="not-found" />
        <p className="text-gray mt-16">{this.getString('merchant_has_no_ratings')}</p>
      </div>
    );
  };

  renderShimmer = () => {
    return (
      <div className="merchant-rating-item shimmer-animation">
        <div className="rating-header">
          <ImageLazy placeholder="/dist/images/placeholder.png" size={80} />
          <div className="rating-header-title ">
            <div className="shimmer-text w-40" />
            <div className="shimmer-text w-20" />
          </div>
        </div>
        <div className="shimmer-text" />
        <div className="shimmer-text" />
        <div className="shimmer-text w-40" />
      </div>
    );
  };

  renderHeader = () => {
    const { merchant } = this.props;
    if (!merchant) return null;

    const recommendedEnable = merchant.get('recommendedEnable');
    const recommendedRatio = ~~(merchant.get('recommendedRatio') * 1000) / 10;

    const countComment = merchant.get('comment') - merchant.get('buyable');

    return (
      <div className="merchant-rating-header">
        <h1 itemProp="name" className="name" title={merchant.get('name')}>
          {merchant.get('name')}
        </h1>
        {recommendedEnable && (
          <div>
            <i className="lz lz-like" /> {this.getString('recommended_0', '', [recommendedRatio])}
            <span className="ml-4 text-dark-gray">
              {countComment > 0 && this.getString('see_0_reviews', 'default', [countComment])}
            </span>
          </div>
        )}
      </div>
    );
  };

  render() {
    const { data, status, extras } = this.props;
    const loadMore = extras && extras.get('nextUrl');

    if (!data || data.size === 0) {
      if (!status || status === 'PENDING') return <div className="merchant-ratings-list">{this.renderShimmer()}</div>;
      return <div className="merchant-ratings-list">{this.renderEmpty()}</div>;
    }

    return (
      <div className="merchant-ratings-list">
        {this.renderHeader()}
        {data.map((rating) => {
          const ratingStatus = rating.rating;
          const photos = rating.photos || [];

          return (
            <div key={rating.id} className="merchant-rating-item">
              <div className="rating-header">
                <ImageLazy src={rating.createdBy && rating.createdBy.avatar} size={80} />
                <div className="rating-header-title">
                  {rating.createdBy && (rating.createdBy.name.short || rating.createdBy.name.full)}
                  <br />
                  <div className={cx('rating')}>
                    <div className={cx('rating-item', ratingStatus)}>
                      <span>{this.getString(ratingStatus, 'rating')}</span>
                    </div>
                  </div>
                </div>

                <div className="rating-header-badge">
                  <div className="text-gray">
                    {this.getString('purchased_on')}{' '}
                    {moment(rating.order && rating.order.createdAt).format('DD/MM/YYYY')}
                  </div>
                </div>
              </div>

              {rating.content && (
                <ExpandContainer
                  key="rating-content"
                  className="rating-content"
                  content={rating.content}
                  line={0}
                  // mode="VIEW_MORE"
                />
              )}

              {photos.length > 0 && (
                <div className="rating-photos-list">
                  {photos.map((photo) => (
                    <ImageLazy
                      key={photo}
                      src={photo}
                      placeholder="/dist/images/placeholder.png"
                      shape="round-square"
                      size={80}
                      onClick={this.handleOpenPhotoPopup(photo, photos)}
                      scrollableAncestor={null}
                    />
                  ))}
                </div>
              )}
            </div>
          );
        })}

        {loadMore && (
          <Waypoint onEnter={this.handleLoadMore}>
            <Spinner type="flow" />
          </Waypoint>
        )}
      </div>
    );
  }

  handleLoadMore = () => {
    const { params, extras = {} } = this.props;
    if (extras.get('nextUrl')) {
      this.props.dispatch(fetchMerchantRatings({ ...params, nextUrl: extras.get('nextUrl') }));
    }
  };

  handleOpenPhotoPopup = (photo, photos) => () => {
    this.props.dispatch(
      open(CUSTOM_CONTENT, {
        contentClassName: 'mc-photos-popup slider',
        content: () => (
          <Carousel
            className="mc-photos-slider"
            data={photos.map((cPhoto) => ({ key: cPhoto, image: cPhoto }))}
            currentSlide={photo}
            slideClassName="mc-photo-item"
            slidesToShow={1}
            dot={false}
            centerMode={false}
            autoplay={false}
            imgWidth={Math.min(window.innerWidth - 64, 800)}
            ratio={4 / 3}
            photoZoom
          />
        ),
        wrapperStyle: { zIndex: 201 },
      }),
    );
  };
}

export default initComponent(MerchantRatings);
