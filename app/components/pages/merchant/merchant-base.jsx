import { sendGAEvent } from 'actions/factory';
import { fetchMerchant } from 'actions/merchant';
import { GENERIC_POPUP, STOP_SERVICE_POPUP, open } from 'actions/popup';
import MerchantRatings from 'components/pages/merchant/_ratings';
import OrderBase, { STEP_ADDRESS, STEP_INFO } from 'components/shared/order';
import qs from 'qs';
import React from 'react';
import { APP_INFO, ROLES_ACCOUNT, ROLES_CONTENT, STOP_SERVING_TIMESTAMP } from 'settings/variables';
import { hasRole } from 'utils/access';
import { getDistrictId } from 'utils/address';
import { mergeArray } from 'utils/content';
import { getCityByIdOrNameOrSlug } from 'utils/data';
import { DEFAULT_CITY_ID } from 'utils/json/supportedCities';
import { isPast } from 'utils/new-time';

if (process.env.BROWSER) {
  require('assets/styles/pages/merchant.scss');
  require('assets/styles/pages/merchant-mobile.scss');
}

class MerchantPageBase extends OrderBase {
  constructor(props, context) {
    super(props, context, { step: STEP_INFO });
    this.lastMQTTStatus = null;
  }

  componentDidMount() {
    this.attachListeners();
    this.setupMerchantPage(this.props, true);
    this.handleMerchantRatingsViewer(this.props, true);

    /**
     * !TODO: block on action level, not UI
     */
    if (!isPast(STOP_SERVING_TIMESTAMP)) this.props.dispatch(open(STOP_SERVICE_POPUP));

    super.componentDidMount();
  }

  componentDidUpdate(prevProps) {
    this.setupMerchantPage(this.props);
    this.handleMerchantRatingsViewer(prevProps);

    const prevQuery = this.getQuery(prevProps);
    const query = this.getQuery();
    if (prevQuery.step === 'search' && query.step !== 'search') this.changeStep(STEP_INFO, true);

    super.componentDidUpdate(prevProps);
  }

  componentWillUnmount() {
    this.detachListeners();
  }

  attachListeners = () => {
    this.boundKeyUpListener = this.handleKeyUp;
    window.addEventListener('keyup', this.boundKeyUpListener);
  };

  detachListeners = () => {
    window.removeEventListener('keyup', this.boundKeyUpListener);
    this.boundKeyUpListener = null;
  };

  renderLoading = () => {
    return <span />;
  };

  setupMerchantPage = (props, isFirstSetup = false) => {
    if (!props.merchant) return;

    const params = this.getParams();
    const query = this.getQuery(props);
    const username = props.merchant.get('username');
    const slug = props.merchant.get('slug');

    if (params.shareId && props.currentItem) {
      query.pItem = props.currentItem.get('id');
      return props.history.replace(
        `/${username || `b/${slug}`}${params.groupTopic ? '/' + params.groupTopic : ''}?${qs.stringify(query)}`,
      );
    }

    if (username && username !== params.merchantUsername) {
      return props.history.replace(
        `/${username}${params.groupTopic ? '/' + params.groupTopic : ''}?${qs.stringify(query)}`,
      );
    }
    if (isFirstSetup && query.source === 'qr_code') {
      sendGAEvent('loship_action', 'ACTION_OPEN_EATERY', 'S:qr_code');
      delete query.source;
      return props.history.replace(
        `/${username}${params.groupTopic ? '/' + params.groupTopic : ''}?${qs.stringify(query)}`,
      );
    }
    if (props.merchant.get('missingData')) {
      this.props.dispatch(fetchMerchant(params));
    }
    if (
      !this.state.fetchedDistance &&
      !props.merchant.get('distance') &&
      this.props.globalAddress?.lat &&
      this.props.globalAddress?.lng
    ) {
      this.setState({ fetchedDistance: true }, () => {
        this.props.dispatch(fetchMerchant({ ...params, globalAddress: this.props.globalAddress, force: true }));
      });
    }

    this.saveShippingInfo(props);
    this.handleLoadData(props);
  };

  saveShippingInfo = (props) => {
    if (!props.merchant) return;

    if (!this.state.senderInfo) {
      const eatery = props.merchant.toJS();
      const city = getCityByIdOrNameOrSlug(eatery.city);

      const senderInfo = {
        customerName: eatery.name,
        customerPhone: '',
        lat: eatery.lat,
        lng: eatery.lng,
        address: eatery.address,
        districtId: getDistrictId(eatery.district),
        suggestedAddress: eatery.address,
        suggestedDistrictId: getDistrictId(eatery.district),
        cityId: (city && city.id) || DEFAULT_CITY_ID,
      };
      this.updateShippingInfo(senderInfo, true);
    }

    if (!this.state.receiverInfo &&
      this.props.globalAddress?.lat && this.props.globalAddress?.lng && this.props.globalAddress?.address) {
      const receiverInfo = this.props.globalAddress;
      receiverInfo &&
        this.updateShippingInfo(receiverInfo, false, () => {
          const query = this.getQuery(props);
          if (query.step === 'address') this.changeStep(STEP_ADDRESS);
        });
    }
  };

  isAllowedToEditMenu = () => {
    const { currentUser } = this.props;

    if (currentUser && hasRole(currentUser, mergeArray(ROLES_ACCOUNT, ROLES_CONTENT))) return true;

    return false;
  };

  isAllowedToSeeManager = () => {
    const { currentUser, merchant } = this.props;

    if (merchant.get('isManager')) return true;

    if (currentUser && hasRole(currentUser, mergeArray(ROLES_ACCOUNT, ROLES_CONTENT))) return true;

    return false;
  };

  isAllowedToRequestMenuChange = () => {
    return this.isAllowedToSeeManager();
  };

  handleKeyUp = (e) => {
    const { step } = this.state;
    if (step !== STEP_INFO) return;

    // console.log('EVENT', e.key, e.ctrlKey);
    if (e.key === 'f' && e.ctrlKey) e.preventDefault();
  };

  handleLoadData = (props) => {
    const { isLoaded } = this.state;
    if (props.merchant && !isLoaded) {
      const { merchant } = props;

      const data = (typeof merchant.toJS === 'function' && merchant.toJS()) || merchant;
      this.setState({
        shippingFee: data.shippingFee,
        currentShippingFee: data.currentShippingFee,
        minimumShippingFee: data.minimumShippingFee,
        extraFees: data.extraFees,
        promotionEnable: data.promotionEnable,
        isLoaded: true,
      });
    }
  };

  submitSimilarMerchant = () => {
    const { merchant } = this.props;

    if (APP_INFO.serviceName === 'lomart') return this.props.history.push(`/danh-sach-cua-hang-tuong-tu-gan-day/${merchant.get('id')}`);
    return this.props.history.push(`/danh-sach-dia-diem-tuong-tu-gan-day/${merchant.get('id')}`);
  };

  handleMerchantRatingsViewer = (props, isFirstSetup) => {
    const { match } = this.props;
    const { match: prevMatch } = props;
    const params = this.getParams();

    if (match.url.includes('/danh-gia') && (isFirstSetup || match.url !== prevMatch.url)) {
      sendGAEvent('loship_action', 'ACTION_VIEW_RATINGMERCHANT');
      this.props.dispatch(
        open(GENERIC_POPUP, {
          title: this.getString('merchant_ratings'),
          className: 'merchant-ratings-popup',
          content: (cProps) => <MerchantRatings params={match.params} {...cProps} />,
          animationStack: 3,
          onCancel: () => {
            const linkMerchant = `/${params.merchantUsername || `b/${params.merchant}`}${
              params.groupTopic ? `/${params.groupTopic}` : ''
            }`;
            this.props.history.push(linkMerchant);
          },
        }),
      );
    }
  };
}

export default MerchantPageBase;
