import cx from 'classnames';
import BaseComponent from 'components/base';
import DishGroup from 'components/pages/merchant/_menu/_dish-group';
import Button from 'components/shared/button';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { getMerchantMenu } from 'reducers/menu';
import { getMerchant } from 'reducers/merchant';
import { getCurrentUsername } from 'reducers/metadata';
import { getCart } from 'reducers/new/cart';
import { getOrderLinesData } from 'utils/cart';
import { REGEX_STRING } from 'utils/data';

class MerchantSearch extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getMerchantMenu(state, params);
      if (!data || data.get('status') !== 'SUCCESS') return data;

      const merchant = getMerchant(state, params);
      if (merchant) data = data.set('merchant', merchant.get('data'));

      const currentUserName = getCurrentUsername(state);
      const cart = getCart(state, params);
      const currentUserCart = cart?.data?.[currentUserName];
      const lines = getOrderLinesData({ [currentUserName]: currentUserCart });

      const group = data.getIn(['data', 'group']);
      const newGroup = group.map((groupDish, key) => {
        const dishes = groupDish.dishes.map((dish) => {
          const selectedCount = lines
            .filter((item) => dish.id === item.dishId)
            .reduce((sum, item) => sum + item.dishQuantity, 0);
          return { ...dish, selectedCount };
        });
        return { ...groupDish, dishes, key };
      });

      data = data.setIn(['data', 'group'], newGroup);
      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  static propTypes = { goBack: PropTypes.func.isRequired };

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      searchResults: [],
      isSearching: false,
    };
  }

  renderEmpty = () => {
    return (
      <div className="mc-search-not-found">
        <img src="/dist/images/not-found-404.png" alt="not-found" />
        <h3>{this.getString('no_matching_results_were_found', 'eatery')}</h3>
        <p>{this.getString('check_for_spelling_and_try_again_or_try_another_name', 'eatery')}</p>

        <Button className="bg-transparent text-blue" onClick={this.props.goBack}>
          {this.getString('go_back_to_merchant_page', 'eatery')}
        </Button>
      </div>
    );
  };

  render() {
    const { searchTerm, searchResults, isSearching } = this.state;
    const data = isSearching
      ? this.parseSearchResults(searchResults)
      : (this.props.data && this.props.data.get('group')) || [];
    const hasResults = data.length > 0;

    const content = hasResults ? (
      <div className="flex-view mt-8 py-16">
        {!isSearching && <div className="title p16_16 text-uppercase">{this.getString('category', 'eatery')}</div>}
        {data.map((group) => (
          <DishGroup
            key={group.key + '_search'}
            index={group.key + '_search'}
            data={group}
            getString={this.getString}
            groupHeadline={(
              <div
                key={group.id}
                className="flex-item justify-between center flex-wrap px-16 py-16"
                onClick={this.goToMenuContent(group.key)}
              >
                <b>{group.name}</b>
                <div>
                  <span className="text-dark-gray">
                    {this.getString(group.dishes.length > 1 ? '0_dishes' : '0_dish', 'order', [group.dishes.length])}
                  </span>
                  <i className="lz lz-arrow-head-right ml-16" />
                </div>
              </div>
            )}
            hideDishes={!isSearching}
            merchant={this.props.merchant}
            isLocked={this.props.isLocked}
          />
        ))}
      </div>
    ) : (
      this.renderEmpty()
    );

    return (
      <div className="mc-search">
        <div className={cx('mc-search-nav flex-item justify-between center')}>
          <i className="lz lz-light-arrow-left" onClick={this.props.goBack} />
          <input
            ref={this.setInputRef}
            placeholder={this.getString('search_inside_merchant', 'eatery')}
            value={searchTerm}
            onChange={this.handleChangeSearchTerm}
            onKeyUp={this.handleSearch()}
          />
          <i
            className={cx('lz action', { 'lz-search': !searchTerm, 'lz-close': !!searchTerm })}
            onClick={this.handleClearSearch}
          />
        </div>
        {content}
      </div>
    );
  }

  setInputRef = (ref) => {
    this.input = ref;
    if (this.input) this.input.focus();
  }

  handleChangeSearchTerm = (e) => {
    const value = e.target.value;
    this.setState({ searchTerm: value });
  };

  handleSearch = (debounce = 500) => () => {
    if (this.timeout) clearTimeout(this.timeout);
    if (debounce === 0) return this.handleSearchCallback();

    this.timeout = setTimeout(() => this.handleSearchCallback(), debounce);
  };

  handleSearchCallback = () => {
    const { searchTerm } = this.state;
    const data = (this.props.data && this.props.data.get('group')) || [];

    const result = data.reduce((res, group) => {
      const dishIds = group.dishes
        .filter((dish) => {
          if (dish.name.toLowerCase().match(REGEX_STRING(searchTerm))) return true;
          if (dish.description.toLowerCase().match(REGEX_STRING(searchTerm))) return true;
          return false;
        })
        .map((dish) => dish.id);

      if (dishIds.length === 0) return res;
      return res.concat([[group.id, dishIds]]);
    }, []);

    this.setState({ searchResults: result, isSearching: !!searchTerm });
  };

  parseSearchResults = (searchResults) => {
    const srcData = (this.props.data && this.props.data.get('group')) || [];

    return searchResults.reduce((res, [groupId, dishIds]) => {
      const group = srcData.find((g) => g.id === groupId);
      if (!group) return res;

      const dishes = group.dishes.filter((d) => dishIds.includes(d.id));
      return res.concat({ ...group, dishes });
    }, []);
  };

  handleClearSearch = () => {
    this.setState({
      searchTerm: '',
      searchResults: [],
      isSearching: false,
    });
  };

  goToMenuContent = (key) => () => {
    this.props.goBack();

    setTimeout(() => {
      const menuContent = document.getElementById('dGroup' + key);
      if (!menuContent) return;
      menuContent.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }, 200);
  };
}

export default initComponent(MerchantSearch);
