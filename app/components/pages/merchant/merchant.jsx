import { STEP_ADDRESS, STEP_CONFIRM, STEP_INFO } from 'components/shared/order';

import LoshipPartnerBanner from 'components/shared/banners/banner-loship-partner';
import OrderCart from 'components/shared/order/cart';
import React from 'react';
import ScrollBoundary from 'components/shared/scroll-boundary';
import StepAddress from 'components/shared/order/address';
import StepConfirm from 'components/shared/order/confirm';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';
import SectionNewsfeed from 'components/shared/newsfeed';
import { EateryItemLanding } from 'components/shared/newsfeed/merchant-item';
import { isMerchantReady } from 'utils/merchant';
import { MerchantPromo } from './_promo';
import MerchantPageBase from './merchant-base';
import MerchantMenu from './_menu';
import MerchantHeader from './_header';

class MerchantPage extends MerchantPageBase {
  renderStep = (step) => {
    const { cartMetadata = {}, currentUser, merchant } = this.props;
    const { data: { lockedUser = [] } = {} } = cartMetadata;
    const isLocked = currentUser && lockedUser[currentUser.get('username')];

    const { senderInfo, receiverInfo } = this.state;

    const closed = merchant.get('closed');
    const isActive = merchant.get('isActive');
    const isCheckedIn = merchant.get('isCheckedIn');

    const eateryId = merchant.get('id');
    const isLoshipPartner = typeof merchant.get('isLoshipPartner') !== 'undefined' ? !!merchant.get('isLoshipPartner') : null;

    const params = this.getParams();

    let roles = (currentUser && currentUser.get('roles')) || [];
    if (roles.toArray) roles = roles.toArray();

    const menuWarning = roles.length && !merchant.get('username');

    const serviceName = this.context && this.context.partnerName !== 'none' ? this.context.partnerName : this.state.srcServiceName; // prefer partner, then reorder's srcServiceName

    const merchantConfig = {
      shippingFee: this.state.shippingFee,
      currentShippingFee: this.state.currentShippingFee,
      minimumShippingFee: this.state.minimumShippingFee,
      extraFees: this.state.extraFees,
      promotionEnable: this.state.promotionEnable,
    };
    const isReady = isMerchantReady(merchant.toObject());

    if (step === STEP_ADDRESS) {
      return (
        <StepAddress
          params={params}
          eateryId={eateryId}
          step={step}
          toNextStep={() => this.changeStep(STEP_CONFIRM)}
          goBack={() => this.changeStep(STEP_INFO, true)}
          goBackText={this.getString('back_to_menu')}
          serviceName={serviceName}
          // address + distance
          shippingAddress={receiverInfo}
          merchantAddress={senderInfo}
          shippingAddresses={this.props.shippingAddresses}
          updateShippingInfo={this.updateShippingInfo}
        />
      );
    }

    if (step === STEP_CONFIRM) {
      return <StepConfirm {...this.computeConfirmData()} {...merchantConfig} serviceName={serviceName} />;
    }

    return (
      <div itemScope itemType="http://schema.org/FoodEstablishment" className="page-merchant">
        {this.isAllowedToSeeManager() && isLoshipPartner === false && <LoshipPartnerBanner />}

        <div className="col-left">
          <MerchantHeader
            params={{
              groupTopic: this.getParams().groupTopic,
              merchant: merchant.get('slug'),
            }}
            data={merchant}
            extraFees={this.state.extraFees}
            isAllowedToEditMenu={this.isAllowedToEditMenu()}
            isAllowedToSeeManager={this.isAllowedToSeeManager()}
            isAllowedToRequestMenuChange={this.isAllowedToRequestMenuChange()}
          />

          {!isReady && (
            <SectionNewsfeed
              title={this.getString('suggest_similar_merchant_near_you')}
              params={{
                page: 1,
                limit: 6,
                hideEmpty: true,
                searchSuggestEateriesApi: true,
                eateryId: merchant.get('id'),
                globalAddress: this.props.globalAddress,
              }}
              history={this.props.history}
              location={this.props.location}
              changePage={(page) => this.submit({ page })}
              submit={this.submitSimilarMerchant}
              mode="preload|horizontal|see_all|scroll-nice"
              MerchantItem={EateryItemLanding}
            />
          )}

          <MerchantMenu
            closed={closed}
            isActive={isActive}
            isCheckedIn={isCheckedIn}
            isLocked={isLocked}
            shippingService={merchant.get('shippingService')}
            isWarning={menuWarning}
            params={{
              ...this.getParams(),
              merchant: merchant.get('slug'),
            }}
            merchantName={merchant.get('name')}
          />
        </div>

        <div className="col-right">
          <MerchantPromo
            params={{ eateryId: merchant.get('id') }}
            eateryId={merchant.get('id')}
            merchant={merchant}
            freeShippingMilestone={merchant.get('freeShippingMilestone')}
          />
          <ScrollBoundary marginBottom={15} fixedMarginTop={10} fixedMarginBottom={10} className="order-scroll-wrapper">
            <OrderCart
              params={{
                groupTopic: this.getParams().groupTopic,
                merchant: merchant.get('slug'),
              }}
              mode="merchant|codebox|codebox-editable"
              eateryId={merchant.get('id')}
              serviceName={merchant.get('shippingService')}
              toNextStep={() => this.changeStep(STEP_ADDRESS)}
              {...merchantConfig}
            />
          </ScrollBoundary>
        </div>
      </div>
    );
  };

  render() {
    if (!this.props.merchant || !this.state.isLoaded) return this.renderLoading();

    const { step } = this.state;
    return <div className="order-page">{this.renderStep(step)}</div>;
  }

  // handleSearch = e => {
  //   if (e.keyCode === 114 || (e.ctrlKey && e.keyCode === 70)) {
  //     e.preventDefault();

  //     alert('SEARCH!!');
  //   }
  // };
}

export default initComponent(MerchantPage, withRouter);
