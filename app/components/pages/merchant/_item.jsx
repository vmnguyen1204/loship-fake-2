import { generateShortLinkSharing } from 'actions/buy';
import { sendGAEvent } from 'actions/factory';
import { fetchMerchantMenu } from 'actions/merchant';
import { DISH_SELECT, GENERIC_POPUP, SHARING_POPUP, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import { handleAddToCart } from 'components/pages/merchant/_menu/_dish-item';
import Button from 'components/shared/button';
import ExpandContainer from 'components/shared/expand-container';
import ImageLazy from 'components/statics/img-lazy';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import qs from 'qs';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getItemById } from 'reducers/buy';
import { getMerchantMenu } from 'reducers/menu';
import { getMerchant } from 'reducers/merchant';
import { getCurrentUser } from 'reducers/user';
import { LINK_LZI } from 'settings/variables';
import { copyToClipboard } from 'utils/content';
import { addCurrency, utoa } from 'utils/format';
import { getRoute } from 'utils/routing';


class MerchantItemMenuComp extends BaseComponent {
  static defaultProps = {
    actionCreator: fetchMerchantMenu,
    dataGetter: getMerchantMenu,
  };

  render() {
    const { merchant } = this.props;
    const data = (this.props.data && this.props.data.toJS()) || {};
    const dishes = (data.group &&
        data.group.reduce((res, g) => {
          if (!g.dishes) return res;
          return res.concat(g.dishes);
        }, [])) ||
      [];

    return (
      <div>
        <div className="merchant-info">
          <div className="merchant-info-title">{this.getString('other_item_from_merchant')}</div>
          <ImageLazy src={merchant.get('avatar')} size={64} />
          <div className="detail">
            <p className="title">
              <b>{merchant.get('name')}</b>
            </p>
            <p>{this.getString('x_items', '', [dishes.length])}</p>
            <p>{merchant.get('address') && merchant.get('address').full}</p>
          </div>
          <Button className="action" onClick={this.props.switchToMerchant}>
            {this.getString('view_merchant_page')}
          </Button>
        </div>

        {dishes.length > 0 && (
          <div className="merchant-item-list-wrapper">
            <div className="merchant-item-list" style={{ width: 116 * Math.min(8, dishes.length) }}>
              {dishes.slice(0, 8).map((dish) => (
                <div
                  key={dish.id}
                  className="merchant-item-detail"
                  onClick={() => this.props.handleViewItemDetail(new Map(dish))}
                >
                  <ImageLazy
                    src={dish.image}
                    placeholder="/dist/images/dish-placeholder.png"
                    size={128}
                    shape="round-square"
                    scrollableAncestor={null}
                  />
                  <div className="name">
                    <ExpandContainer content={dish.name} line={2} mode="FIXED" />
                    <div className="price">{addCurrency(dish.price)}</div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  }
}
const MerchantItemMenu = initComponent(MerchantItemMenuComp, withRouter);

class MerchantItemComp extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getItemById(state, params);
      if (!data) return;

      let merchantFromItem = data.getIn(['data', 'eatery']) || {};
      if (typeof merchantFromItem.toJS === 'function') merchantFromItem = merchantFromItem.toJS();
      params.merchant = merchantFromItem.slug;
      params.merchantUsername = merchantFromItem.username;

      if (params.merchant || params.merchantUsername) {
        const merchant = getMerchant(state, params);
        if (merchant) data = data.set('merchant', merchant.get('data'));
      }

      const currentUser = getCurrentUser(state, params);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
  };

  componentDidMount() {
    sendGAEvent('loship_action', 'ACTION_VIEW_ITEM');
  }

  render() {
    const { data, merchant } = this.props;
    let merchantAddress = merchant.get('address') || '';
    if (merchantAddress.full) merchantAddress = merchantAddress.full;

    return (
      <div className="merchant-item">
        <ImageLazy
          id="item-detail-img"
          src={data.get('image')}
          placeholder="/dist/images/dish-placeholder.png"
          size={480}
          shape="square"
          preload
        />
        <div className="name">
          {data.get('name')}
          <div className="price" itemProp="offers" itemScope itemType="http://schema.org/Offer">
            <meta itemProp="priceCurrency" content="VNĐ" />
            <meta itemProp="price" content={data.get('price')} />
            <div>
              <b>{addCurrency(data.get('price'))}</b>
              {data.get('rawPrice') !== data.get('price') && (
                <span className="old">{addCurrency(data.get('rawPrice'))}</span>
              )}
            </div>
          </div>

          <div className="address">{merchantAddress}</div>
        </div>

        <div className="p16_16 bg-white">
          <Button type="primary" fw onClick={this.handleDishSelect}>
            {this.getString('buy_now')}
          </Button>
        </div>

        {data.get('description') && data.get('description').trim() && (
          <ExpandContainer
            className="description"
            title={this.getString('item_information')}
            content={data.get('description').trim()}
            line={4}
            mode="ARROW"
            arrow={(isExpanded) => (
              <span className={cx('btn-expand', !isExpanded && 'has-shadow')}>
                {this.getString(isExpanded ? 'view_less' : 'view_more')}
                <i className={cx('fas', isExpanded ? 'fa-chevron-up' : 'fa-chevron-down')} />
              </span>
            )}
          />
        )}

        <MerchantItemMenu
          params={{ merchant: merchant.get('slug') }}
          merchant={merchant}
          switchToMerchant={this.switchToMerchant}
          handleViewItemDetail={this.handleViewOtherItem}
        />
      </div>
    );
  }

  handleDishSelect = () => {
    const { data, merchant } = this.props;
    this.props.dispatch(
      open(DISH_SELECT, {
        merchant: merchant.get('slug'),
        groupTopic: this.getParams().groupTopic,
        dish: data.toJS(),
        handleAddToCart: handleAddToCart.bind(this),
      }),
    );
  };

  switchToMerchant = () => {
    const { merchant } = this.props;

    const merchantUrl = merchant.get('username') ? `/${merchant.get('username')}` : `/b/${merchant.get('slug')}`;
    this.props.history.push(merchantUrl);
    typeof this.props.handleClose === 'function' && this.props.handleClose();
  };

  handleViewOtherItem = (item) => {
    if (!item) return;
    if (item.get('id') === this.props.data.get('id')) return;

    typeof this.props.handleViewOtherItem === 'function' && this.props.handleViewOtherItem(item);
  };
}
const MerchantItem = initComponent(MerchantItemComp, withRouter);
export default MerchantItem;

export function MerchantItemPopupHandler({ isMobile, callback }) {
  if (!this || !this.props) return;

  const { location: { pathname }, history, currentItem, merchant } = this.props;
  const query = this.getQuery();
  const { match } = (typeof window !== 'undefined' && getRoute(null, window.location.pathname)) || {};
  const groupTopic = match && match.params.groupTopic;

  const handleSharing = (item) => {
    this.props.dispatch(
      generateShortLinkSharing({
        rawUrl: `https://lozi.loship.vn/items/${utoa(item.get('id'))}`,
        fallbackUrl: window.location.href,
        baseSharingUrl: `${LINK_LZI}/i`,
        callback: (sharingUrl) => {
          const sharingContent = this.getString('item_sharing_content', '', [item.get('name')]);

          copyToClipboard(`${sharingContent} ${sharingUrl}`);
          toast(this.getString('copied_link'));

          this.props.dispatch(
            open(SHARING_POPUP, {
              subject: this.getString('buy_on_loship', 'order'),
              body: sharingContent,
              url: sharingUrl,
            }),
          );
        },
      }),
    );
  };

  const handleViewItemDetail = (item) => {
    this.props.dispatch(
      open(GENERIC_POPUP, {
        wrapperStyle: { zIndex: 100 },
        header: (popupProps) => (
          <div className="popup-header">
            <Button className={cx('btn-back left bg-transparent')} onClick={popupProps.handleClose}>
              <i className={cx('lz', { 'lz-light-arrow-left': isMobile, 'lz-close': !isMobile })} />
            </Button>
            {item.get('name') || this.getString('item_detail')}
            <Button className="btn-back right bg-transparent pull-right" onClick={() => handleSharing(item)}>
              <i className="fas fa-share-alt" />
            </Button>
          </div>
        ),
        className: 'merchant-item-popup',
        content: (popupProps) => (
          <MerchantItem params={{ itemId: item.get('id') }} {...popupProps} handleViewOtherItem={handleViewOtherItem} />
        ),
        // animationStack: 3,
        bypassTargetId: 'item-detail-img',
        onCancel: () => {
          typeof callback === 'function' && callback();

          delete query.pItem;
          this.props.history.push(this.props.location.pathname + '?' + qs.stringify(query));
        },
        autoCloseHandler: () => {
          const cQuery = this.getQuery();
          if (!cQuery.pItem) return true;
        },
      }),
    );
  };


  function handleViewOtherItem(item) {
    query.pItem = item.get('id');
    history.replace(`${pathname}?${qs.stringify(query)}`);

    return handleViewItemDetail(item);
  }

  if (isMobile) return handleViewItemDetail(currentItem);
  this.props.dispatch(
    open(DISH_SELECT, {
      merchant: merchant.get('slug'),
      groupTopic,
      dish: currentItem.toJS(),
      handleAddToCart: handleAddToCart.bind(this),
      callbackOnClose: true,
      callback: () => {
        delete query.pItem;
        this.props.history.push(this.props.location.pathname + '?' + qs.stringify(query));
        typeof callback === 'function' && callback();
      },
    }),
  );
}
