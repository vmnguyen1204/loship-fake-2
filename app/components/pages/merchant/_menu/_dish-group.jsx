import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Hint from 'components/statics/hint';
import Button from 'components/shared/button';
import DishItem from './_dish-item';

class DishGroup extends PureComponent {
  static propTypes = {
    displayItemCount: PropTypes.number,
    data: PropTypes.object.isRequired,
  }

  static defaultProps = { displayItemCount: 15 };

  constructor(props) {
    super(props);
    this.state = { isExpanded: false };
  }

  renderDish = (data, key, groupInfo) => {
    return (
      <DishItem
        key={key}
        data={{ ...data, ...groupInfo }}
        groupInfo={groupInfo}
        merchant={this.props.merchant}
        isLocked={this.props.isLocked}
        suggestEateriesUrl={this.props.suggestEateriesUrl}
      />
    );
  };

  render() {
    const { data, index: key, displayItemCount, getString } = this.props;
    const size = data.dishes ? data.dishes.length : 0;
    const isExpanded = this.state.isExpanded;

    const headline = (() => {
      if (!this.props.groupHeadline) {
        return (
          <div className="title">
            <h3 itemProp="name" id={`dGroup${key}`}>
              {data.name}
              {data.isPinned && (
                <>
                  <span className="ml-4" />
                  <div className="featured">Nổi bật</div>
                </>
              )}
              {!!data.isExtraGroupDish && (
                <small>
                  {getString('side_dishes_or_desserts')}
                  <Hint
                    className="dish-group-hint"
                    text={getString(
                      'all_food_in_this_group_cannot_be_ordered_alone_your_order_must_have_at_least_01_main_course',
                    )}
                  />
                </small>
              )}
            </h3>
          </div>
        );
      }
      if (!this.props.hideDishes) {
        return (
          <div className="title">
            <h3 itemProp="name" id={`dGroup${key}`}>
              {data.name}
            </h3>
          </div>
        );
      }

      return this.props.groupHeadline;
    })();

    return (
      <div
        className="mc-submenu"
        ref={`menuContent${key}`}
        itemProp="hasMenuSection"
        itemScope
        itemType="http://schema.org/MenuSection"
      >
        {headline}
        {!this.props.hideDishes && (
          <ul className="food-list">
            {data.dishes.slice(0, isExpanded ? size : displayItemCount).map((d, k) => this.renderDish(d, k, {
              isExtraGroupDish: data.isExtraGroupDish,
              groupDishId: data.id,
            }))}
            {size > displayItemCount && !isExpanded && (
              <div className="px-8">
                <Button fw className="bg-transparent outline-blue text-blue" onClick={this.expandItemList}>
                  {getString('see_more_0_1', 'default', [data.name, size - displayItemCount])}
                </Button>
              </div>
            )}
          </ul>
        )}
      </div>
    );
  }

  expandItemList = () => this.setState({ isExpanded: true });
}

export default DishGroup;
