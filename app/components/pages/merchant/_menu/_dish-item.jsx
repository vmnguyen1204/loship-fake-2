import { CONFIRM_POPUP, CUSTOM_CONTENT, DISH_SELECT, LOGIN, open } from 'actions/popup';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import ExpandContainer from 'components/shared/expand-container';
import ImageLazy from 'components/statics/img-lazy';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import qs from 'qs';
import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { cartAddItem, getCartMetadata } from 'reducers/new/cart';
import { getCurrentUser } from 'reducers/user';
import { getOrderLine, getOrderLineData } from 'utils/cart';
import { addCurrency } from 'utils/format';
import { isMerchantReady } from 'utils/merchant';

class AddToCartEffect extends PureComponent {
  state = { animated: false }

  componentDidMount() {
    this.setState({ animated: true });
  }

  render() {
    const { data } = this.props;

    return (
      <span ref={(ref) => { this.effect = ref; }} className="animate-add-to-cart" style={this.addAnimation()}>
        {data}
      </span>
    );
  }

  addAnimation = () => {
    const { animated } = this.state;
    if (!animated) return;

    const headerElement = document.getElementById('mc-cart');
    if (!headerElement) return { display: 'none' };

    const animPosition = headerElement.getBoundingClientRect();
    const currentPosition = this.effect.getBoundingClientRect();

    const [transformX, transformY] = [
      animPosition.left - currentPosition.left + 24,
      animPosition.top - currentPosition.top + 20,
    ];

    return {
      transform: `translate(${transformX}px, ${transformY}px)`,
      opacity: 0.8,
    };
  };
}

class DishItem extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const cartMetadata = getCartMetadata(state, params);
      if (cartMetadata) data = data.set('cartMetadata', cartMetadata);

      const currentUser = getCurrentUser(state, params);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  state = { animatedItems: [], nextKey: 1 };

  render() {
    const { data, mode } = this.props;
    const { isMobile } = this.context;

    const query = this.getQuery();
    const itemName = (query && query.showDishId === 'true' && `[ID: ${data.id}] ${data.name}`) || data.name;

    return (
      <div
        className="list-item dish-item"
        itemProp="hasMenuItem"
        itemScope
        itemType="http://schema.org/MenuItem"
        onClick={this.handleDishSelect}
      >
        <meta itemProp="suitableForDiet" content="http://schema.org/GlutenFreeDiet" />
        <ImageLazy
          src={data.image}
          placeholder="/dist/images/dish-placeholder.png"
          mode={!isMobile && mode !== 'COMPACT' ? 'hover' : undefined}
          shape="round-square"
          size={this.props.imageSize || 200}
          type="o"
        />
        <div className="content">
          <div className="name" itemProp="name">
            {itemName}
          </div>
          <div className="price" itemProp="offers" itemScope itemType="http://schema.org/Offer">
            <meta itemProp="priceCurrency" content="VNĐ" />
            <meta itemProp="price" content={data.price} />
            <div>
              <b>{addCurrency(data.price)}</b>
              {data.rawPrice !== data.price && <span className="old">{addCurrency(data.rawPrice)}</span>}
            </div>
            {mode !== 'COMPACT' && data.selectedCount > 0 && (
              <div className="selected-count">
                {this.getString(isMobile ? 'x{0}' : 'ordered_count_0', 'eatery', [data.selectedCount])}
              </div>
            )}
          </div>
          {mode !== 'COMPACT' && data.description && (
            <ExpandContainer
              className={cx('description')}
              content={data.description}
              line={2}
              mode={isMobile ? 'FIXED' : 'VIEW_MORE'}
              padding={0}
            />
          )}
        </div>

        {mode !== 'COMPACT' && !isMobile && (
          <Button type="primary" className="btn-add-to-cart">
            <i className="lz lz-plus" />
          </Button>
        )}
        {mode === 'COMPACT' && (
          <Button type="primary" className="btn-add-to-cart">
            + {this.getString('add')}
          </Button>
        )}

        {Object.values(this.state.animatedItems)}
      </div>
    );
  }

  handleDishSelect = (e) => {
    e.stopPropagation();
    e.preventDefault();

    const { isMobile } = this.context;
    const { data, mode, groupInfo, location } = this.props;
    let merchant = this.props.merchant;
    if (typeof merchant.toJS === 'function') merchant = merchant.toJS();

    const classList = `${e.target.classList} ${e.target.parentElement ? e.target.parentElement.classList : ''}`;

    // handle click image
    if (classList.includes('img-lazy')) {
      this.props.dispatch({
        type: 'FETCH_ITEM_BY_ID',
        key: `${data.id}.item`,
        data: { ...data, eatery: merchant },
      });

      const query = this.getQuery();
      query.pItem = data.id;
      return this.props.history.push(location.pathname + '?' + qs.stringify(query));
    }

    if ((!isMobile || mode === 'COMPACT') && !classList.includes('btn-add-to-cart')) return;
    if ((!isMobile || mode === 'COMPACT') && (!data.customs || !data.customs.length)) {
      return this.handleAddToCart({
        merchant: merchant.slug,
        groupTopic: this.getParams().groupTopic,
        data: { ...data, ...groupInfo, dishQuantity: 1 },
        callback: this.handleCallback,
      });
    }

    this.props.dispatch(
      open(DISH_SELECT, {
        merchant: merchant.slug,
        groupTopic: this.getParams().groupTopic,
        dish: data,
        groupInfo,
        handleAddToCart: this.handleAddToCart,
        callback: this.handleCallback,
      }),
    );
  };

  handleCallback = () => {
    if (this.props.callback) return setTimeout(() => this.props.callback(), 0);

    if (this.context.isMobile) return;
    const { animatedItems, nextKey } = this.state;

    const item = React.createElement(AddToCartEffect, { data: 1, key: nextKey });
    animatedItems[nextKey] = item;
    this.setState({ animatedItems, nextKey: nextKey + 1, triggerUpdate: false });

    setTimeout(() => {
      animatedItems[nextKey] = undefined;
      this.setState({ animatedItems, triggerUpdate: true });
    }, 700);
  };

  handleAddToCart = ({ data, lineData, merchant, groupTopic, callback }) => {
    return handleAddToCart.call(this, { data, lineData, merchant, groupTopic, callback });
  };
}

export default initComponent(DishItem, withRouter);

export function handleAddToCart({ data, lineData, merchant, groupTopic, callback }) {
  if (!this || !this.props) return;

  const { cartMetadata = {}, currentUser, match: { params = {} } = {} } = this.props;
  const { data: { merchant: currentMerchant, topic: currentGroupTopic, lockedUsers = {} } = {} } = cartMetadata;

  if (params.groupTopic && !currentUser) {
    this.props.dispatch(
      open(LOGIN, {
        description : this.getString('cart_require_login_hint'),
        wrapperStyle: { zIndex: 201 },
      }),
    );
    return false;
  }

  if (currentUser && lockedUsers[currentUser.get('username')]) {
    this.props.dispatch(
      open(CUSTOM_CONTENT, {
        content: this.getString(
          'your_cart_has_been_locked_after_you_press_the_im_done_button_please_contact_the_team_leader_to_reopen_the_cart_for_you',
        ),
      }),
    );
    return false;
  }

  let merchantObj = this.props.merchant;
  if (typeof merchantObj.toJS === 'function') merchantObj = merchantObj.toJS();

  if (currentMerchant && currentMerchant !== merchantObj.slug) {
    if (currentGroupTopic) {
      this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('cannot_add_to_cart', 'order'),
          content: this.getString(
            'you_are_in_group_order_of_another_merchant_so_you_cannot_add_items_from_0',
            'order',
            [this.props.merchant.get('name')],
          ),
          actionText: this.getString('i_got_it'),
        }),
      );
      return false;
    }

    const { isMobile } = this.context;
    this.props.dispatch(
      open(CONFIRM_POPUP, {
        title: this.getString('do_you_want_to_switch_to_new_merchant'),
        content: this.getString(
          'if_you_agree_to_switch_merchant_your_current_shopping_cart_will_be_cleaned_so_you_can_add_the_items_you_want',
        ),
        confirmBtn: { text: this.getString('agree_continue'), className: isMobile && 'btn-fw' },
        cancelBtn: { text: this.getString('go_back'), className: 'bg-transparent text-gray text-normal' },
        actionClassName: isMobile && 'flex-column-reverse',
        onConfirm: () => {
          this.props.dispatch(
            cartAddItem({
              data: getOrderLineData(getOrderLine(data, lineData)),
              merchant,
              groupTopic,
              serviceName: merchantObj.shippingService,
            }),
          );
          typeof callback === 'function' && callback();
        },
      }),
    );
    return false;
  }

  if (
    !this.props.ignoreMerchantReady &&
    !isMerchantReady(merchantObj, {
      callback: () => {
        this.props.dispatch(
          cartAddItem({
            data: getOrderLineData(getOrderLine(data, lineData)),
            merchant,
            groupTopic,
            serviceName: merchantObj.shippingService,
          }),
        );
        typeof callback === 'function' && callback();
      },
      dispatch: this.props.dispatch,
      getString: this.getString,
      history: this.props.history,
      suggestEateriesUrl: this.props.suggestEateriesUrl,
    })
  ) return false;

  this.props.dispatch(
    cartAddItem({
      data: getOrderLineData(getOrderLine(data, lineData)),
      merchant,
      groupTopic,
      serviceName: merchantObj.shippingService,
    }),
  );
  typeof callback === 'function' && callback();
  return true;
}
