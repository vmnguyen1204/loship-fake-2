import { fetchMerchantMenu } from 'actions/merchant';
import BaseComponent from 'components/base';
import ScrollBoundary from 'components/shared/scroll-boundary';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getMerchantMenu } from 'reducers/menu';
import { getMerchant } from 'reducers/merchant';
import { getCurrentUsername } from 'reducers/metadata';
import { getCart } from 'reducers/new/cart';
import { getSuggestEateries } from 'reducers/newsfeed';
import { APP_INFO, LINK_DOMAIN_MCSTATS } from 'settings/variables';
import { getOrderLinesData } from 'utils/cart';
import DishGroup from './_dish-group';

class MerchantMenu extends BaseComponent {
  static defaultProps = {
    actionCreator: fetchMerchantMenu,
    dataGetter: (state, params) => {
      let data = getMerchantMenu(state, params);
      if (!data || data.get('status') !== 'SUCCESS') return data;

      const merchant = getMerchant(state, params);
      if (merchant) data = data.set('merchant', merchant.get('data'));

      const currentUserName = getCurrentUsername(state);
      const cart = getCart(state, params);
      const currentUserCart = cart?.data?.[currentUserName];
      const lines = getOrderLinesData({ [currentUserName]: currentUserCart });

      const group = data.getIn(['data', 'group']);
      const newGroup = group.map((groupDish, key) => {
        const dishes = groupDish.dishes.map((dish) => {
          const selectedCount = lines
            .filter((item) => dish.id === item.dishId)
            .reduce((sum, item) => sum + item.dishQuantity, 0);
          return { ...dish, selectedCount };
        });
        return { ...groupDish, dishes, key };
      });

      data = data.setIn(['data', 'group'], newGroup);

      const suggestEateries = getSuggestEateries(state, {
        hideEmpty: true,
        page: 1,
        limit: 6,
        eateryId: merchant.getIn(['data', 'id']),
        searchSuggestEateriesApi: true,
      });
      if (suggestEateries) {
        data = data.set(
          'suggestEateriesUrl',
          APP_INFO.serviceName === 'lomart'
            ? `/danh-sach-cua-hang-tuong-tu-gan-day/${merchant.getIn(['data', 'id'])}`
            : `/danh-sach-dia-diem-tuong-tu-gan-day/${merchant.getIn(['data', 'id'])}`,
        );
      }

      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  renderInvalid = () => {
    const { merchantName } = this.props;
    return (
      <div className="menu-manager-warning">
        <h2>{this.getString('this_merchant_is_not_ready_for_order', 'menuManage')}</h2>
        <p className="text-center">
          <i>{this.getString('only_lozi_staffs_can_see_this_notice', 'menuManage')}</i>
        </p>
        <p>
          {this.getString(
            'a_merchant_on_loship_must_have_a_username_in_order_to_create_short_link_for_it_like_loshipvntencuahang',
            'menuManage',
          )}
        </p>
        <p>
          {this.getString('to_create_a_username_for_0', 'menuManage', [merchantName])}
          <a href={`${LINK_DOMAIN_MCSTATS}/b/${this.getParams().merchant}`} target="_blank" rel="noopener">
            {this.getString('click_here', 'menuManage')}
          </a>
        </p>
        <p className="text-center">
          <img src="/dist/images/update-username.png" alt="username" />
        </p>
      </div>
    );
  };

  renderNotReady = () => {
    return (
      <div className="mc-menu empty">
        <div className="empty-menu-content">
          <h3>{this.getString('this_place_has_not_supported_ordering_on_loship_yet')}</h3>
          <p>
            {this.getString('if_you_think_this_is_a_mistake_please_let_us_know_by_contacting')}
            <a href="mailto:hotroloship@lozi.vn">hotroloship@lozi.vn</a>
            {this.getString('thanks_for_your_help')}
          </p>
        </div>
      </div>
    );
  };

  render() {
    const { data, isWarning } = this.props;

    if (isWarning) return this.renderInvalid();
    if (!data) return <div className="mc-menu" />;
    if (data.get('group') && data.get('group').length === 0) return this.renderNotReady();

    return (
      <div className="mc-menu" itemProp="hasMenu" itemScope itemType="http://schema.org/Menu">
        <meta itemProp="inLanguage" content="Vietnamese" />

        {!this.context.isMobile && (
          <ScrollBoundary marginBottom={15} fixedMarginTop={10} fixedMarginBottom={10} className="submenu-heading-list">
            <div className="panel">
              {data.get('group').map((item, key) => {
                return (
                  <a className="menu-heading" key={item.id} onClick={() => this.goToMenuContent(key)}>
                    {item.name}
                  </a>
                );
              })}
            </div>
          </ScrollBoundary>
        )}
        <div className="submenu-content-list">
          {data.get('group').map((item, key) => {
            return (
              <DishGroup
                key={item.id}
                index={key}
                data={item}
                getString={this.getString}
                merchant={this.props.merchant}
                isLocked={this.props.isLocked}
                suggestEateriesUrl={this.props.suggestEateriesUrl}
              />
            );
          })}
        </div>
      </div>
    );
  }

  goToMenuContent = (key) => {
    const menuContent = document.getElementById('dGroup' + key);
    menuContent.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };
}

export default initComponent(MerchantMenu, withRouter);
