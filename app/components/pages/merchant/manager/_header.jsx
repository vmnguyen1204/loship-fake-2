import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { getImageUrl } from 'utils/image';
import AuthByRole from 'components/shared/button/auth-by-role';
import { ROLES_CONTENT } from 'settings/variables';

if (process.env.BROWSER) {
  require('assets/styles/pages/merchant-manager.scss');
}

class MerchantHeader extends BaseComponent {
  render() {
    const { data, originDomain } = this.props;
    const name = data.get('name');
    const hasContract = data.get('commissionValue');

    return (
      <div className="mc-header">
        <div>
          <a
            href={originDomain}
            className="mc-avatar"
            style={{ backgroundImage: `url('${getImageUrl(data.get('avatar'), 240)}')` }}
          />
          <div className="mc-info">
            <ul className="breadcrumbs" title={name}>
              <li>
                <a href={originDomain}>{name}</a>
              </li>
              <li>
                <i className="fa fa-angle-right" />
              </li>
              <li>{this.getString('menu_management')}</li>
            </ul>
            <p>
              {this.getString(
                'menu_management_tools_help_the_ownersadministrators_to_easily_optimize_and_update_the_menu_of_0_that_appears_on_loship',
                '',
                [name],
              )}
            </p>
            <p>
              {this.getString(
                'you_can_easily_create_new_dishes_update_the_new_price_for_old_dishes_or_turning_on_and_off_that_dishes_you_dont_want_to_appear_every_changes_will_be_immediately_update_for_the_customers_to_order',
              )}
            </p>

            {!hasContract && (
              <AuthByRole roles={ROLES_CONTENT}>
                <div className="mc-warning">
                  <p>
                    {this.getString(
                      'Lưu ý: địa điểm này không phải là đối tác Loship. Vui lòng kiểm tra kỹ khi tạo menu.',
                    )}
                  </p>
                </div>
              </AuthByRole>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(MerchantHeader);
