import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';

import {
  approveActiveMenuRequest,
  createActiveRequest,
  fetchActiveRequest,
  rejectActiveMenuRequest,
} from 'actions/merchant-manager';

class InactiveWarning extends BaseComponent {
  static defaultProps = {
    actionCreator: fetchActiveRequest,
    dataGetter: (state, props) => {
      return state.menuActiveRequest.get(props.merchant);
    },
  };

  createActiveMenuRequest = () => {
    this.props.dispatch(createActiveRequest(this.getParams()));
  };

  approveActiveMenuRequest = () => {
    this.props.dispatch(approveActiveMenuRequest(this.getParams()));
  };

  rejectActiveMenuRequest = () => {
    this.props.dispatch(rejectActiveMenuRequest(this.getParams()));
  };

  renderRejectMessage = () => {
    return (
      <div className="mc-menu-inactive-warning">
        {this.getString('your_request_has_been_denied') + ' '}
        <button type="button" className="btn btn-active" onClick={this.createActiveMenuRequest}>
          {this.getString('request_activation')}
        </button>
      </div>
    );
  };

  renderAdminAction = () => {
    return (
      <div className="mc-menu-inactive-warning">
        <button type="button" className="btn btn-active" onClick={this.approveActiveMenuRequest}>
          {this.getString('request_accepted')}
        </button>
        <button type="button" className="btn btn-active" onClick={this.rejectActiveMenuRequest}>
          {this.getString('request_denied')}
        </button>
      </div>
    );
  };

  renderUserAction = () => {
    return (
      <div className="mc-menu-inactive-warning">
        <button type="button" className="btn btn-active" onClick={this.createActiveMenuRequest}>
          {this.getString('request_activation')}
        </button>
      </div>
    );
  };

  renderUserData = () => {
    return (
      <div className="mc-menu-inactive-warning">
        <p>{this.getString('your_request_has_been_received')}</p>
        <p>{this.getString('call_09x_for_customer_services')}</p>
      </div>
    );
  };

  render() {
    const { data, hasRole } = this.props;
    if (!data) {
      return this.renderUserAction();
    }

    if (!hasRole) {
      if (data.get('status') === 'reject') return this.renderRejectMessage();
      return this.renderUserData();
    }
    if (data.get('status') === 'accept') {
      return null;
    }
    return this.renderAdminAction();
  }
}

export default initComponent(InactiveWarning);
