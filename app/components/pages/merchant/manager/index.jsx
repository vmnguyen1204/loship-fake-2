import React from 'react';
import { withRouter } from 'react-router-dom';
import BasePage from 'components/pages/base';
import initComponent from 'lib/initComponent';
import { checkCanEditMenu, fetchMerchant } from 'actions/merchant-manager';
import { getMerchant } from 'reducers/merchant-manager';
import { getCurrentUser } from 'reducers/user';
import * as SEOTYPE from 'settings/seo-types';
import { Map } from 'immutable';
import {
  LINK_DOMAIN,
  LINK_DOMAIN_LOZI,
  LIST_MERCHANT_LOSUPPLY,
  ROLES_ADMIN,
  ROLES_CONTENT,
} from 'settings/variables';
import { hasRole } from 'utils/access';
import { CUSTOM_CONTENT, MENU_ADD_CATEGORIES, UPDATE_AVATAR, open } from 'actions/popup';
import { getMerchantLoship } from 'reducers/merchant';
import { fetchMerchantLoshipById } from 'actions/merchant';
import MobileMerchantManager from 'components/pages/merchant/manager-new';
import PropTypes from 'prop-types';
import CloneMenuDishPopup from './popup/dish-clone';
import RemoveMenuDishPopup from './popup/dish-remove';
import UpdateMenuDishPopup from './popup/dish';
import SearchDishImagePopup from './popup/dish-image';
import UpdateMenuDishCustomsPopup from './popup/dish-customs';
import UpdateMenuDishCustomPopup from './popup/dish-custom';
import CloneMenuDishCustomPopup from './popup/custom-clone';
import UpdateMenuDishCustomOptionPopup from './popup/dish-custom-option';
import UpdateMenuGroupPopup from './popup/group';
import UpdateMenuIndustryPopup from './popup/industry';
import UpdateMenuSurgePricePopup from './popup/menu-surge-price';
import UpdateFranchisedPopup from './popup/menu-update-franchised';
import MenuWareHousingPopup from './popup/warehouse-popup';
import ListStockHisory from './popup/dish-stock-history';
import LosupplyDishItem from 'components/pages/merchant/manager/item-losupply';

import MerchantMenu from './_menu';
import MerchantHeader from './_header';
import { getRoute } from 'utils/routing';
import AddCategoriesPopup from './popup/add-categories';
import cx from 'classnames';

if (process.env.BROWSER) {
  require('assets/styles/pages/merchant-manager.scss');
}

class MerchantManager extends BasePage {
  static contextTypes = { isMobile: PropTypes.bool };
  static childContextTypes = { isAdmin: PropTypes.bool };

  constructor(props, context) {
    super(props, {
      ...context,
      seo: {
        type: SEOTYPE.MERCHANT_MANAGER,
        merchant: props.data,
      },
    });
  }

  static defaultProps = {
    // actionCreator: fetchMerchant,
    dataGetter: (state, params) => {
      let data = new Map();
      const merchant = getMerchant(state, params);
      if (merchant) {
        data = data.merge(merchant);

        const merchantLoship = getMerchantLoship(state, { ...params, merchant: merchant.getIn(['data', 'id']) });
        if (merchantLoship) data = data.set('merchantLoship', merchantLoship.get('data'));
      }
      const currentUser = getCurrentUser(state);
      if (currentUser) {
        data = data.set('currentUser', currentUser);
        data = data.set('hasRoleContent', hasRole(currentUser.get('data'), ROLES_CONTENT));
      }

      return data;
    },
  };

  componentDidMount() {
    this.checkValidMerchant(this.props);
    const { merchant, merchantUsername } = this.getParams();

    if (!this.props.data) {
      this.props.dispatch(
        fetchMerchant(
          { merchant, merchantUsername, isAdmin: this.isAdmin() },
        ),
      );
    }

    if (this.props.data) {
      this.props.dispatch(
        fetchMerchantLoshipById({ merchant: this.props.data.get('id') }),
      );

      checkCanEditMenu({
        merchant: this.props.data.get('id'),
        callback: () => {
          this.setState({ canEditMenu: true }, () => this.checkValidMerchant(this.props));
        },
        isAdmin: this.isAdmin(),
      });
    }
  }

  componentDidUpdate(prevProps) {
    this.checkValidMerchant(this.props);

    if (!prevProps.data && this.props.data) {
      this.props.dispatch(
        fetchMerchantLoshipById({ merchant: this.props.data.get('id') }),
      );

      if (!this.state.canEditMenu) {
        checkCanEditMenu({
          merchant: this.props.data.get('id'),
          callback: () => {
            this.setState({ canEditMenu: true }, () => this.checkValidMerchant(this.props));
          },
          isAdmin: this.isAdmin(),
        });
      }
    }
  }

  checkValidMerchant = (props) => {
    if (!props.data || !this.state.canEditMenu || this.state.isInvalid) return;

    const { avatar, lat, lng, street, slug, id } = props.data.toObject();
    if (!lat || !lng || !street) {
      this.setState({ isInvalid: true });
      return props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('missing_proper_address', 'menuManage'),
          content: (
            <div className="menu-manager-warning">
              <p>
                {this.getString('a_merchant_must_have_an_address_and_geo_location_before_having_a_menu', 'menuManage')}
              </p>
              <p>
                {this.getString('reload_this_page_after_updating_or', 'menuManage')}
                <a onClick={() => window.location.reload()}>{this.getString('click_here', 'menuManage')}</a>
              </p>
              <img src="/dist/images/update-lat-long.png" alt="lat-long-desc" />
            </div>
          ),
          actionText: this.getString('click_to_update_address', 'menuManage'),
          canNotDismiss: true,
          action: () => {
            window.open(`${LINK_DOMAIN_LOZI}/b/${slug}/edit`);
          },
        }),
      );
    }

    if (!avatar) {
      this.setState({ isInvalid: true });
      return props.dispatch(
        open(UPDATE_AVATAR, {
          title: this.getString('missing_avatar', 'menuManage'),
          description: this.getString(
            'a_merchant_must_have_an_avatar_picture_for_better_display_on_loship_before_having_a_menu',
            'menuManage',
          ),
          merchantId: id,
        }),
      );
    }
  };

  openCategoriesPopup = () => {
    if (!this.props.data) return;
    const { merchantLoship } = this.props;
    const categories = merchantLoship?.toJS().categories;
    const { address, cityId, name, id, shippingService } = this.props.data?.toJS();
    const superCategoryId = shippingService.id;
    return this.props.dispatch(
      open(MENU_ADD_CATEGORIES, {
        title: this.getString('select_categories_of_merchant'),
        defaultCategories: categories,
        eateryId: id,
        name,
        address,
        cityId,
        superCategoryId,
      }),
    );
  }

  getMetadata = (props, seoOptions) => {
    const seo = { ...seoOptions };
    seo.merchant = props.data;
    return seo;
  };

  isAdmin = () => {
    const { currentUser } = this.props;
    if (currentUser && currentUser.get('data') && hasRole(currentUser.get('data'), ROLES_ADMIN)) return true;
    return false;
  };

  getChildContext() {
    return { isAdmin: this.isAdmin() };
  }

  isAllowToManage = () => {
    // TODO: GET RID OF THIS BULLSHIT FUNCTION
    // HOW ABOUT A NEW MERCHANT, NO MENU CREATED YET?
    if (!this.props.merchantLoship) return false;
    return true;
  };

  isValidListMerchantLosupply = (domain) => LIST_MERCHANT_LOSUPPLY.indexOf(domain.split('/').reverse()[0]) > -1;

  render() {
    if (this.context.isMobile) return <MobileMerchantManager />;

    const { merchantUsername: username, merchant: slug } = this.getParams();
    const { redirect_url: redirectUrl } = this.getQuery();
    const originDomain = redirectUrl || `${LINK_DOMAIN}/${username || `b/${slug}`}`;

    if (!this.state.canEditMenu) {
      return (
        <div>
          <div className="container">
            <div className="page-merchant page-merchant-manager">
              <div className="alone-message">
                <h3>{this.getString('request_for_a_menu_update')}</h3>
                <p>{this.getString('sorry_you_are_not_authorized_to_access_this_page')}</p>
                <p>
                  {this.getString('in_case_you_merchant_owner_need_to_update_your_menu_please_contact_us_via')}
                  <a href="mailto:leduy@lozi.vn" title={this.getString('Contact us to request a menu update')}>
                    leduy@lozi.vn
                  </a>
                  {this.getString('for_assistance')}
                </p>
                <p>{this.getString('thank_you_for_choosing_loship')}</p>
                <p>
                  <a href={originDomain}>
                    <i className="fa fa-chevron-left" />
                    &nbsp;
                    {this.getString('go_back')}
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      );
    }

    if (!this.props.data) return false;

    const eaterySlug = this.props.data.get('slug');
    const { merchantLoship } = this.props;
    const categories = merchantLoship?.toJS().categories;
    const isHasCategory = categories?.length > 0;
    const { hasRoleContent } = this.props;
    const isValidListMerchantLosupply = this.isValidListMerchantLosupply(originDomain);

    const { location: { pathname } } = this.props;
    const { params } = getRoute(null, pathname).match;
    const { merchantUsername, merchant } = params;
    const eatery = merchantUsername ? merchantUsername : `b/${merchant}`;
    if (params.dishId) return (
      <LosupplyDishItem
        params={{ merchant: eaterySlug, dishId: params.dishId, eatery }}
        merchant={this.props.data}
        originDomain={originDomain}
      />
    );

    return (
      <div>
        <div className="container">
          <div className="page-merchant page-merchant-manager">
            <MerchantHeader data={this.props.data} originDomain={originDomain} />
            {/* {isInactive && (
              <InactiveMenuWarning params={{merchant: eaterySlug, eateryId }} hasRole={this.isAdmin()}/>
            )} */}
            <div className="categories-wrapper">
              <div className="categories-content">
                <h3>{this.getString('Categories')}</h3>
                {!isHasCategory ? <span className="categories-warning">{this.getString('not_merchant_has_not_yet_created_a_category')}</span>
                  : (
                    <div className="categories">
                      {categories?.map((category) => {
                        return (
                          <span className="category" key={category.id}>{category.value}</span>
                        );
                      })}
                    </div>
                  )}
              </div>
              <div className="btn-update-categories">
                <a onClick={this.openCategoriesPopup}>
                  {isHasCategory ? (
                    <span>
                      <i className="fa fa-pencil-alt" />
                      {this.getString('edit_categories')}
                    </span>
                  )
                    : this.getString('create_categories')}
                </a>
              </div>
            </div>
            <div className="wrap-merchant-menu">
              <div className={cx('menu-notice', this.state.isLoadMore && 'load-more')}>
                <div className="notice-content">
                  <b>{this.getString('Thông báo:')} </b>
                  {this.getString('Từ ngày 06/12/2018 giá món chính tối thiểu khi tạo menu món mới là 12,000 đồng')}

                  <br />
                  <br />
                  <b>{this.getString('image_requirement')}</b>

                  <p>{this.getString('Ảnh có kích thước tối thiểu 800 x 800 pixel')}</p>
                  <p>
                    {this.getString(
                      'Ảnh đồ ăn/thức uống quán chụp sắp xếp bắt mắt, ngay giữa hình. Không lấy ảnh trên mạng hoặc ảnh có logo các dịch vụ tương tự',
                    )}
                  </p>
                  <p>{this.getString('Ảnh không kèm giá món, số điện thoại và link web cửa hàng')}</p>
                  <p>{this.getString('Độ phân giải rõ nét, không bị ngoè hoặc vỡ hạt')}</p>
                </div>
                {!this.state.isLoadMore && <a onClick={() => this.setState({ isLoadMore: true })}>Xem thêm</a>}
              </div>

              <MerchantMenu
                closed={this.props.data.get('closed')}
                params={{
                  merchant: eaterySlug,
                  isAdmin: this.isAdmin(),
                  isMenuLosupply: isValidListMerchantLosupply && hasRoleContent,
                }}
                merchant={this.props.data}
                originDomain={originDomain}
                categories={categories}
                openCategoriesPopup={this.openCategoriesPopup}
              />
            </div>
            <UpdateMenuDishPopup merchant={this.props.data} isAdmin={this.isAdmin()} />
            <SearchDishImagePopup />
            <UpdateMenuDishCustomsPopup />
            <UpdateMenuDishCustomPopup />
            <CloneMenuDishCustomPopup />
            <UpdateMenuDishCustomOptionPopup />
            <CloneMenuDishPopup />
            <RemoveMenuDishPopup />
            <UpdateMenuGroupPopup />
            <UpdateMenuIndustryPopup />
            <UpdateMenuSurgePricePopup />
            <UpdateFranchisedPopup />
            <MenuWareHousingPopup />
            <ListStockHisory />
            <AddCategoriesPopup />
          </div>
          <div className="clearfix" />
        </div>
      </div>
    );
  }
}

export default withRouter(initComponent(MerchantManager));
