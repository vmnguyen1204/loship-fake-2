import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { CONFIRM_POPUP, MENU_CLONE_DISH, close, open } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import CloneMenuDishPopupContent from './_content';

class CloneMenuDishPopup extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = { couldEscape: true };
  }

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_CLONE_DISH },
  };

  close = (checkEditing = false) => {
    if (checkEditing === true && this.state.isEditing) {
      return this.props.dispatch(
        open(CONFIRM_POPUP, {
          title: this.getString('your_data_havent_save_yet'),
          content: this.getString('are_you_sure_you_want_to_leave_any_unsaved_changes_will_be_lost'),
          onConfirm: () => this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_CLONE_DISH))),
        }),
      );
    }

    this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_CLONE_DISH)));
  };

  render() {
    const params = this.props.data.get('params');
    return (
      <Popup className="menu-dish-group" handleClose={() => this.close(true)}>
        <div className="heading">
          <h3>{`Tạo món mới từ "${params.dish.get('name')}"`}</h3>
        </div>
        <CloneMenuDishPopupContent
          handleClose={this.close}
          params={{
            groupDishId: params.groupDishId,
            isExtraGroupDish: params.isExtraGroupDish,
            merchant: params.merchant,
            merchantId: params.merchantId,
          }}
          hasRoleContent={params.hasRoleContent}
          data={params.dish}
          groups={params.groups}
          toggleEditing={this.toggleEditing}
          toggleCouldEscape={this.toggleCouldEscape}
        />
      </Popup>
    );
  }

  toggleEditing = (isEditing = false) => {
    this.setState({ isEditing });
  };

  toggleCouldEscape = (could) => {
    this.setState({ couldEscape: could });
  };
}

export default initComponent(CloneMenuDishPopup);
