import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { copyDish } from 'actions/merchant-manager';
import { Map } from 'immutable';
import DishImage from 'components/pages/merchant/manager/popup/dish/_dish-image';
import { addCommas, removeCommas, slugify } from 'utils/format';
import { MIN_MAIN_DISH_PRICE } from 'utils//menu-tool';
import Button from 'components/shared/button';
import Select from 'components/shared/select';
import UNITMENU from 'utils/json/unit_menu';
import PropTypes from 'prop-types';

const mapPropsToState = (props) => {
  const {
    name,
    price,
    description,
    image,
    active,
    additionalFee,
    createdAt,
    createdBy,
    updatedAt,
    updatedBy,
    unit,
    unitQuantity,
  } = props.data.toJS();
  const priceInt = parseInt(price, 10);
  const additionalFeeInt = parseInt(additionalFee, 10);

  return {
    data: new Map({
      name,
      price: addCommas(price),
      description,
      image,
      active,
      additionalFee: addCommas(additionalFee),
      additionalFeePercent: addCommas(Math.ceil((additionalFeeInt / priceInt) * 100).toString()),
      createdAt,
      createdBy,
      updatedAt,
      updatedBy,
      unitMenus: UNITMENU,
      unit: unit || '',
      unitQuantity: unitQuantity || 0,
      groupDishId: props.params.groupDishId,
    }),
  };
};

class CloneMenuDishPopup extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = {
      ...mapPropsToState(props),
      image: null,
      error: '',
    };
  }

  componentDidMount() {
    if (this.props.data) this.setState(mapPropsToState(this.props));

    if (this.refs.txtDishName) {
      this.refs.txtDishName.focus();
      this.refs.txtDishName.select();
    }
    window.addEventListener('keydown', this.handleKeyDown);
    document.addEventListener('click', this.handleClickOutside);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
    document.removeEventListener('click', this.handleClickOutside);
  }

  handleKeyDown = (e) => {
    if (this.state.noteFocused && !e.ctrlKey) return;
    if (e.key === 'Tab' && e.target.name === 'unit') {
      let { data } = this.state;
      data = data.set('showUnitMenus', false);
      this.setState({ data });
    }

    if (e.key === 'Enter' && !e.shiftKey) this.handleCreateDish();
  };

  handleClickOutside = (e) => {
    if (!this.node.contains(e.target)) {
      let { data } = this.state;
      data = data.set('showUnitMenus', false);
      this.setState({ data });
    }
  };

  handleCreateDish = async () => {
    let dishData = this.props.data;
    const isExtraGroupDish = this.getParams().isExtraGroupDish;
    const { image, groupDishId } = this.state;

    const { txtDishName, txtPrice, txtDescription, txtAdditionalFeePortion, txtUnitQuantity, txtUnit } = this.refs;
    const name = txtDishName.value.trim();
    const description = txtDescription.value.trim();
    const additionalFeePortion = txtAdditionalFeePortion.value.trim();
    const price = removeCommas(txtPrice.value.trim());
    const unitQuantity = parseFloat(txtUnitQuantity.value);
    const unit = txtUnit.value.trim();
    const group = groupDishId;
    if (!name) return this.setState({ error: this.getString('the_name_cannot_be_empty') });
    dishData = dishData.set('name', name);

    dishData = dishData.set('description', description);

    if (!price) return this.setState({ error: this.getString('the_price_cannot_be_empty') });
    if (price % 1000 !== 0 && !this.props.hasRoleContent) {
      return this.setState({ error: this.getString('not_created_price_should_be_rounded') });
    } dishData = dishData.set('price', price);

    if (!isExtraGroupDish) {
      const additionalFee = (removeCommas(price) * removeCommas(additionalFeePortion)) / 100;
      if (removeCommas(price) + removeCommas(additionalFee) < MIN_MAIN_DISH_PRICE) {
        return this.setState({ error: this.getString('Món chính không được bán giá thấp hơn ' + addCommas(MIN_MAIN_DISH_PRICE) + 'đ') });
      }
    }

    if (!additionalFeePortion) return this.setState({ error: this.getString('the_surge_price_cannot_be_empty') });
    dishData = dishData.set('additionalFeePortion', additionalFeePortion);

    if (image && image.hasDish()) {
      dishData = dishData.set('copyImageFromDish', this.state.image.getDish());
    } else if (image && image.hasImage()) {
      dishData = dishData.set('image', await this.state.image.getURL());
    }

    if (unit) {
      dishData = dishData.set('unit', unit);

      if (unitQuantity === 0) {
        return this.setState({ error: this.getString('Số lượng phải lớn hơn 0') });
      } dishData = dishData.set('unitQuantity', unitQuantity);
    }

    await new Promise((resolve) => this.props.dispatch(
      copyDish({
        merchant: this.getParams().merchant,
        merchantId: this.getParams().merchantId,
        groupDishId: group ? parseInt(group) : this.getParams().groupDishId,
        dishId: dishData.get('id'),
        dishData: dishData.delete('id'),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    this.props.handleClose();
  };

  handleInputChange = (field) => (e) => {
    const rawValue = e.target.value;
    let { data } = this.state;

    if (field === 'price' || field === 'additionalFee') {
      data = data.set(field, addCommas(removeCommas(rawValue)));
    } else {
      data = data.set(field, rawValue);
    }

    if (field === 'price' || field === 'additionalFeePercent') {
      const price = parseInt(removeCommas(data.get('price')), 10);
      const additionalFeePercent = parseInt(removeCommas(data.get('additionalFeePercent')), 10) / 100;
      if (!isNaN(price) && !isNaN(additionalFeePercent)) {
        data = data.set(
          'additionalFee',
          addCommas((Math.ceil(((price * additionalFeePercent) / 1000).toFixed(3)) * 1000).toString()),
        );
      } else {
        data = data.set('additionalFee', '0');
      }
    }
    if (field === 'additionalFee') {
      const price = parseInt(removeCommas(data.get('price')), 10);
      const additionalFee = parseInt(removeCommas(data.get('additionalFee')), 10);
      if (!isNaN(price) && !isNaN(additionalFee)) {
        data = data.set('additionalFeePercent', addCommas(Math.ceil((additionalFee / price) * 100).toString()));
      } else {
        data = data.set('additionalFeePercent', '0');
      }
    }

    if (field === 'unit') {
      data = data.set('unit', rawValue.toLowerCase()).set('shouldFilterUnit', true);
    }

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data, data));
  };

  handleGroupDishChange = (group) => {
    this.setState({ groupDishId: group.get('id') });
  };

  setImage = (image) => {
    this.setState({ image }, () => this.props.toggleEditing(this.isEditing(this.props.data, this.state.data)));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      parseInt(srcData.get('price')) !== parseInt(removeCommas(data.get('price'))) ||
      parseInt(srcData.get('additionalFee')) !== parseInt(removeCommas(data.get('additionalFee'))) ||
      srcData.get('description') !== data.get('description') ||
      !!this.state.image ||
      parseInt(this.getParams().groupDishId) !== parseInt(data.get('groupDishId'))
    );
  };

  handleShow = () => {
    let { data } = this.state;
    data = data.set('showUnitMenus', true).set('shouldFilterUnit', false);
    this.setState({ data }, () => this.refs.txtUnit.focus());
  };

  handleUnitMenu = (unit) => {
    let { data } = this.state;
    data = data.set('unit', unit.name);
    data = data.set('showUnitMenus', false);
    this.setState({ data });
  };

  filterData = (unit) => {
    const data = this.state.data;
    if (!data.get('shouldFilterUnit')) return true;

    const txtUnitName = this.refs.txtUnit ? this.refs.txtUnit.value.trim() : '';
    if (txtUnitName.length > 0) {
      const unitName = slugify(unit.name);
      const regex = new RegExp(slugify(data.get('unit')), 'gi');
      return unitName.search(regex) > -1;
    }
    return true;
  };

  render() {
    const data = this.state.data;
    const hasRoleContent = this.props.hasRoleContent;
    const groups = this.props.groups;
    const group = groups.toJS().find((item) => {
      return item.id === data.get('groupDishId');
    });

    return (
      <div className="content">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <div className="form-group">
            <label>
              {this.getString('dish_name')} <span className="required">*</span>
            </label>
            <div className="input">
              <input ref="txtDishName" value={data.get('name')} onChange={this.handleInputChange('name')} />
            </div>
          </div>

          <div className="form-group" style={{ width: '50%' }}>
            <label>{this.getString('unit_name')}</label>
            <div
              className="input wrapper-unit-menu"
              ref={(node) => {
                this.node = node;
              }}
            >
              <input
                ref="txtUnit"
                name="unit"
                onFocus={this.handleShow}
                onChange={this.handleInputChange('unit')}
                value={data.get('unit')}
              />

              {data.get('showUnitMenus') && (
                <div className="unitMenus scroll-nice">
                  {data
                    .get('unitMenus')
                    .filter(this.filterData)
                    .map((unit) => (
                      <div key={unit.id} className="item-unit-menu" onClick={() => this.handleUnitMenu(unit)}>
                        {unit.name}
                      </div>
                    ))}
                </div>
              )}
            </div>
          </div>

          <div className="form-group" style={{ width: '50%' }}>
            <label style={{ paddingLeft: 24 }}>{this.getString('unit_quantity')}</label>
            <div className="input">
              <input
                ref="txtUnitQuantity"
                value={data.get('unitQuantity')}
                onChange={this.handleInputChange('unitQuantity')}
              />
            </div>
          </div>

          <div className="form-group">
            <label>
              {this.getString('6vDaz_GcUI')} <span className="required">*</span>
            </label>
            <div className="input">
              <input ref="txtPrice" value={data.get('price')} onChange={this.handleInputChange('price')} />
            </div>
          </div>
          <div className="form-group">
            <label>{this.getString('surge_price_percent')}</label>
            <div className="input">
              <input
                type="number"
                ref="txtAdditionalFeePortion"
                value={data.get('additionalFeePercent')}
                onChange={this.handleInputChange('additionalFeePercent')}
              />
            </div>
          </div>
          <div className="form-group">
            <label>{this.getString('category')}</label>
            <div className="input">
              <Select data={groups} value={[group.name]} handleData={this.handleGroupDishChange} showStatus />
            </div>
          </div>

          <div className="form-group">
            <label>{this.getString('notes_if_any')}</label>
            <div className="input">
              <textarea
                ref="txtDescription"
                rows="3"
                value={data.get('description')}
                onChange={this.handleInputChange('description')}
                onFocus={() => this.setState({ noteFocused: true })}
                onBlur={() => this.setState({ noteFocused: false })}
              />
            </div>
          </div>

          <div className="form-group">
            <label>
              {this.getString('picture')} <span className="required">*</span>
            </label>
            <div className="input">
              <DishImage
                preloadImage={data.get('image')}
                image={this.state.image}
                setImage={this.setImage}
                keyword={data.get('name')}
                isAllowedToCopyDishImage={hasRoleContent}
                toggleCouldEscape={this.props.toggleCouldEscape}
              />
            </div>
          </div>
          {!!this.state.error && <div className="form-message text-right">{this.state.error}</div>}
          <div className="form-action">
            <Button onClick={this.props.handleClose}>{this.getString('abort')}</Button>
            <Button htmlType="submit" type="feature" onClick={this.handleCreateDish}>
              {this.getString('add')}
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(CloneMenuDishPopup);
