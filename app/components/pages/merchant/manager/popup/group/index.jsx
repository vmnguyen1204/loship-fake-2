import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { CONFIRM_POPUP, MENU_UPDATE_GROUP, close, open } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import UpdateMenuGroupPopupContent from './_content';

class UpdateMenuGroupPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: MENU_UPDATE_GROUP },
  };

  close = (checkEditing = false) => {
    if (checkEditing === true && this.state.isEditing) {
      return this.props.dispatch(
        open(CONFIRM_POPUP, {
          title: this.getString('your_data_havent_save_yet'),
          content: this.getString('are_you_sure_you_want_to_leave_any_unsaved_changes_will_be_lost'),
          onConfirm: () => this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_UPDATE_GROUP))),
        }),
      );
    }

    this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_UPDATE_GROUP)));
  };

  render() {
    const params = this.props.data.get('params');
    return (
      <Popup className="menu-dish-group" handleClose={() => this.close(true)}>
        <div className="heading">
          <h3>{this.getString(params.isCreate ? 'Thêm danh mục menu' : 'Chỉnh sửa danh mục menu')}</h3>
        </div>
        <UpdateMenuGroupPopupContent
          handleClose={this.close}
          isCreate={params.isCreate}
          data={params.group}
          params={{ merchant: params.merchant, industry: params.industry }}
          toggleEditing={this.toggleEditing}
        />
      </Popup>
    );
  }

  toggleEditing = (isEditing = false) => {
    this.setState({ isEditing });
  };
}

export default initComponent(UpdateMenuGroupPopup);
