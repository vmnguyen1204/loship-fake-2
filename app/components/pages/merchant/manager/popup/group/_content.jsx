import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import cx from 'classnames';
import { createMenuGroup, deleteMenuGroup, updateMenuGroup } from 'actions/merchant-manager';
import Hint from 'components/statics/hint';
import { Map } from 'immutable';

import AuthByRole from 'components/shared/button/auth-by-role';
import { LINK_DOMAIN_LOZI, LIST_MERCHANT_LOSUPPLY, ROLES_CONTENT } from 'settings/variables';
import { MIN_MAIN_DISH_PRICE, isGroupDishHasInvalidDish } from 'utils//menu-tool';
import moment from 'utils/shims/moment';
import { addCommas, slugify } from 'utils/format';
import Button from 'components/shared/button';
import { CONFIRM_POPUP, open } from 'actions/popup';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { getMenuIndustrySearch } from 'reducers/menu-manager';

if (process.env.BROWSER) {
  require('assets/styles/components/_checkbox.scss');
}

const defaultData = new Map({
  name: '',
  active: false,
  isPinned: false,
});

const mapPropsToState = (props) => {
  if (!props.data) return { data: defaultData };

  const {
    name = '',
    active,
    isExtraGroupDish,
    createdBy,
    createdAt,
    updatedBy,
    updatedAt,
    dishes,
    isPinned,
    isShownSupplyItemsForUser,
  } = props.data.toJS();
  const { industry } = props.params;
  return {
    data: new Map({ name, active, isExtraGroupDish, createdBy, createdAt, updatedBy, updatedAt, dishes, isPinned, isShownSupplyItemsForUser }),
    industryName: industry?.get('name'),
    selectedIndustry: industry?.toJS(),
  };
};

class UpdateMenuGroupPopup extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: defaultData,
      error: '',
      industryName: '',
    };
  }

  static defaultProps = {
    dataGetter: (state, params) => {
      const data = getMenuIndustrySearch(state, params);
      const industryList = data?.toJS()?.data?.industry || [];
      return { industryList };
    },
    customRender: true,
  };
  static contextTypes = { isAdmin: PropTypes.bool };

  componentDidMount() {
    if (this.props.data) this.setState(mapPropsToState(this.props));
    if (this.refs.txtName) this.refs.txtName.focus();
    window.addEventListener('keydown', this.handleKeyDown);
    document.addEventListener('click', this.handleClickOutside);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
    document.removeEventListener('click', this.handleClickOutside);
  }

  handleKeyDown = (e) => {
    const isCreate = this.props.isCreate;
    if (e.key === 'Enter' && !e.shiftKey) isCreate ? this.handleCreate(e) : this.handleUpdate(e);
  };

  handleCreate = async () => {
    const { industry } = this.getParams();
    await new Promise((resolve) => this.props.dispatch(
      createMenuGroup({
        merchant: this.getParams().merchant,
        groupData: new Map({
          name: this.refs.txtName.value.trim(),
          isExtraGroupDish: this.state.data.get('isExtraGroupDish'),
          industryId: industry?.get('id'),
        }),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    this.props.handleClose();
  };

  handleUpdate = async (e) => {
    e.preventDefault();
    const invalidDish = isGroupDishHasInvalidDish(this.props.data.get('dishes'), MIN_MAIN_DISH_PRICE);
    const isExtraGroupDish = this.state.data.get('isExtraGroupDish');

    if (!isExtraGroupDish && invalidDish !== null) {
      return this.setState({
        error: this.getString(
          `Không thể đổi menu này thành menu món chính, do có món "${
            invalidDish.name
          }" có giá bán thấp hơn mức quy định dành cho món chính (${addCommas(
            MIN_MAIN_DISH_PRICE,
          )}đ). Anh chị vui lòng cập nhật lại giá bán trước.`,
        ),
      });
    }

    const { selectedIndustry } = this.state;

    await new Promise((resolve) => this.props.dispatch(
      updateMenuGroup({
        merchant: this.getParams().merchant,
        groupDishId: this.props.data.get('id'),
        groupData: new Map({
          ...this.state.data.toJS(),
          industryId: selectedIndustry?.id,
        }),
        callback: resolve,
        isAdmin: this.context.isAdmin,
        eateryId: this.props.data.get('eateryId'),
        previousShownSupplyItems: this.props.data.get('isShownSupplyItemsForUser'),
      }),
    ));
    this.props.handleClose();
  };

  handleDeleteGroupDish = () => {
    this.props.dispatch(
      open(CONFIRM_POPUP, {
        content: () => {
          return (
            <div className="text-center">
              <p>
                {this.getString('are_you_sure_you_want_to_delete')}
                <b>{this.props.data.get('name')} ?</b>
              </p>
            </div>
          );
        },
        confirmText: this.getString('delete'),
        onConfirm: async () => {
          await new Promise((resolve) => {
            this.props.dispatch(
              deleteMenuGroup({
                merchant: this.getParams().merchant,
                groupDishId: this.props.data.get('id'),
                callback: resolve,
                isAdmin: this.context.isAdmin,
                industryId: this.getParams().industry?.get('id'),
              }),
            );
          });
        },
      }),
      this.props.dispatch(this.props.handleClose),
    );
  };

  handleDelete = async () => {
    const data = this.props.data;
    if (data.get('dishes').size > 0) {
      return this.setState({
        error: this.getString(
          `Chỉ có thể xóa danh mục "${data.get('name')}" khi đã di chuyển toàn bộ món ăn sang danh mục khác.`,
        ),
      });
    }
    // need handle
  };

  handleChange = (field, value) => {
    let { data } = this.state;
    data = data.set(field, value);
    if (field === 'name') data = data.set(field, value.toUpperCase());
    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultData, data));
  };

  handleCheckboxChange = (field) => {
    let { data } = this.state;
    data = data.set(field, !data.get(field));

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultData, data));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      srcData.get('isExtraGroupDish') !== data.get('isExtraGroupDish') ||
      srcData.get('isShownSupplyItemsForUser') !== data.get('isShownSupplyItemsForUser')

    );
  };

  toggleEditorInfo = () => {
    this.setState((state) => ({ ...state, showDetail: !state.showDetail }));
  };

  renderEditorInfo = () => {
    const { data } = this.state;
    const { createdAt, createdBy, updatedAt, updatedBy } = (data && data.toObject()) || {};

    // VALIDATE INPUT DATA
    if (!createdBy || !updatedBy) return null;

    return (
      <div className="history">
        <span>
          {this.getString('created_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + createdBy.username}>
            @{createdBy.username}
          </a>
          {this.getString('at')}
          {moment(createdAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <br />
        <span>
          {this.getString('updated_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + updatedBy.username}>
            @{updatedBy.username}
          </a>
          {this.getString('at')}
          {moment(updatedAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <AuthByRole key="delete" roles={ROLES_CONTENT}>
          <div className="delete-group" style={{ marginTop: 3 }}>
            <a onClick={this.handleDeleteGroupDish}>
              <i className="fas fa-trash" /> {this.getString('delete')}
            </a>
          </div>
        </AuthByRole>
      </div>
    );
  };

  handleClickOutside = (e) => {
    if (!this.nodeIndustry || !this.nodeIndustry.contains(e.target)) {
      this.setState({ showIndustryList: false });
    }
  }

  handleShowIndustryList = () => {
    this.setState({ showIndustryList: true, shouldFilterIndustry: false });
  }

  handleOnChange = (e) => {
    this.setState({
      showIndustryList: true,
      industryName: e.target.value.toUpperCase(),
      selectedIndustry: undefined,
    });
  }

  getFilteredIndustries = (industries) => {
    if (!this.state.showIndustryList) return industries;
    const txtIndustry = this.refs.txtIndustry ? this.refs.txtIndustry.value.trim() : '';
    const industriesFiltered = industries.filter((industry) => {
      if (txtIndustry.length) {
        const industryName = slugify(industry.name);
        const regex = new RegExp(slugify(this.state.industryName), 'gi');
        return industryName.search(regex) > -1;
      }
    });
    return industriesFiltered;
  }

  handleChooseIndustry = (industry) => {
    this.setState({
      showIndustryList: false,
      industryName: industry.name,
      selectedIndustry: { ...industry },
    });
  }

  render() {
    const { isCreate, industryList } = this.props;
    const { data, error, showIndustryList, industryName } = this.state;
    const merchantUsername = this.props.match?.params?.merchantUsername;
    const isLosupply = LIST_MERCHANT_LOSUPPLY.includes(merchantUsername);
    const { industry } = this.getParams();
    return (
      <div className="content">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          {!isCreate && [
            <div key="active" className="form-group">
              <label>{this.getString('is_available', 'menuManage')}</label>
              <div className="input">
                <div>
                  <a
                    title={this.getString('press_to_0_this_dish', '', [!data.get('active') ? 'bật' : 'tắt'])}
                    onClick={() => this.handleCheckboxChange('active')}
                  >
                    <i
                      className={cx('availability-status', { active: data.get('active') })}
                    />
                  </a>
                </div>
                <div className="input-note">
                  {this.getString('if_on_0_will_appear_on_the_menu_for_the_customers_to_order', '', [data.get('name')])}
                </div>
              </div>
            </div>,
            <div key="pin" className="form-group">
              <label>{this.getString('top_pin', 'menuManage')}</label>
              <div className="input">
                <div>
                  <a
                    title={this.getString('press_to_0_this_dish', '', [!data.get('isPinned') ? 'bật' : 'tắt'])}
                    onClick={() => this.handleCheckboxChange('isPinned')}
                  >
                    <i
                      className={cx('availability-status', { active: data.get('isPinned') })}
                    />
                  </a>
                </div>
              </div>
            </div>,
          ]}
          <div className="form-group">
            <label>{this.getString('Tên danh mục')}</label>
            <div className="input">
              <input ref="txtName" value={data.get('name')} onChange={(e) => this.handleChange('name', e.target.value)} />
            </div>
          </div>
          <div className="form-group">
            <label>
              {this.getString('Menu món phụ ')}
              <Hint
                text={[
                  this.getString('Tất cả món ăn trong menu này sẽ được xem là món phụ.'),
                  this.getString('Đơn hàng chỉ toàn món phụ, thì không được phép đặt hàng.'),
                ]}
              />
            </label>
            <div className="input">
              <a onClick={() => this.handleCheckboxChange('isExtraGroupDish')}>
                <i
                  className={cx('fas fa-check', 'extra-dishes', { active: data.get('isExtraGroupDish') })}
                />
              </a>
            </div>
          </div>
          {!isCreate && isLosupply && (
            <AuthByRole roles={ROLES_CONTENT}>
              <div className="form-group">
                <label>
                  {this.getString('NGUYÊN LIỆU HIỂN THỊ VỚI NGƯỜI DÙNG')}
                </label>
                <div className="input">
                  <a onClick={() => this.handleCheckboxChange('isShownSupplyItemsForUser')}>
                    <i
                      className={cx('fas fa-check', 'extra-dishes', { active: data.get('isShownSupplyItemsForUser') })}
                    />
                  </a>
                </div>
              </div>
            </AuthByRole>
          )}
          {industry && (
            <div className="form-group">
              <label>{this.getString('Ngành')}</label>
              {isCreate && (
                <div className="input">
                  <input value={industry?.get('name')} disabled={true} />
                </div>
              )}
              {!isCreate && (
                <div
                  className="input wrapper-industry-menu"
                  ref={(nodeIndustry) => {
                    this.nodeIndustry = nodeIndustry;
                  }}
                >
                  <input
                    ref="txtIndustry"
                    type="text"
                    name="industryName"
                    value={industryName}
                    onFocus={this.handleShowIndustryList}
                    onChange={this.handleOnChange}
                    autoComplete="off"
                  />
                  {showIndustryList && industryList && (
                    <div className="industry-menu scroll-nice">
                      {this.getFilteredIndustries(industryList)
                        .map((itemIndustry) => {
                          return (
                            <div key={itemIndustry.id} className="item-industry-menu" onClick={() => this.handleChooseIndustry(itemIndustry)}>
                              {itemIndustry.name}
                            </div>
                          );
                        })}
                    </div>
                  )}
                </div>
              )}
            </div>
          )}
          {!!error && <div className="form-message">{error}</div>}
          <div className="form-action">
            {!isCreate && <AuthByRole roles={ROLES_CONTENT}>{this.renderEditorInfo()}</AuthByRole>}

            <Button onClick={this.props.handleClose}>{this.getString('abort')}</Button>
            {isCreate && (
              <Button htmlType="submit" type="feature" onClick={this.handleCreate}>
                {this.getString('add')}
              </Button>
            )}
            {/* (!isCreate) && (<a className="btn btn-delete pull-left" onClick={this.handleDelete}>Xóa</a>) */}
            {!isCreate && (
              <Button htmlType="submit" type="feature" onClick={this.handleUpdate}>
                {this.getString('save')}
              </Button>
            )}
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(UpdateMenuGroupPopup, withRouter);
