import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import moment from 'utils/shims/moment';
import { fetchNoteStock } from 'actions/merchant-manager';
import PropTypes from 'prop-types';
import { getNote } from 'reducers/menu-manager';
import NumberedPaginator from 'components/shared/paginator-numbered';
import { Map } from 'immutable';
import { addCommas, addCurrency } from 'utils/format';

if (process.env.BROWSER) {
  require('assets/styles/components/stock-history.scss');
}

class ListStockHisory extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };
  static defaultProps = {
    dataGetter: getNote,
    customRender: true,
  }

  componentDidMount() {
    const { params, merchant, dispatch } = this.props;
    const { isAdmin } = this.context;
    dispatch(fetchNoteStock({ dishId: params.dishId, merchant, isAdmin, callback: this.callback }));
  }

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      perPage: 20,
      extras: {},
    };
  }

  callback = (data) => {
    this.setState({ extras: data });
  }

  handlePageChange = (page) => {
    const { params, merchant, dispatch } = this.props;
    const { isAdmin } = this.context;
    const { perPage } = this.state;
    const query = {};
    const offset = (page - 1) * perPage;
    query.offset = offset;
    query.limit = perPage;

    const data = { dishId: params.dishId, page, merchant, isAdmin, force: true, query };

    this.setState({ currentPage: page }, () => {
      dispatch(fetchNoteStock({ ...data }));
    });
  };

  render() {
    const { data } = this.props;

    if (!data) return null;
    const notes = data.getIn(['group', 'notes']);
    return (
      <div className="detail-history-stock">
        <NumberedPaginator
          onPageChange={this.handlePageChange}
          currentPage={this.state.currentPage}
          extras={Map(this.props.pagination || this.state.extras)}
        />
        <div className="wrapper-table scroll-nice">
          <table>
            <thead>
              <tr>
                <th className="right">ID</th>
                <th className="right">Ngày</th>
                <th className="right">Số lượng</th>
                <th className="right">Tiền nhập hàng</th>
                <th>Nhà cung cấp</th>
                <th>Ghi chú</th>
                <th>Người thực hiện</th>
              </tr>
            </thead>
            <tbody>
              {notes && notes.map((item) => {
                return (
                  <tr key={item.id} className="history-item">
                    <td className="stock-id right">{item.id}</td>
                    <td className="created-at right">{moment(item.createdAt).format('DD/MM')}</td>
                    <td className="stock-count right">{addCommas(item.amount)}</td>
                    <td className="stock-count right">{item.price ? addCurrency(item.price) : '--'}</td>
                    <td className="stock-count">{item.dishVendor?.name || '--'}</td>
                    <td className="note">{item.note}</td>
                    <td className="created-by">{item.createdBy.fullName}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default initComponent(ListStockHisory);
