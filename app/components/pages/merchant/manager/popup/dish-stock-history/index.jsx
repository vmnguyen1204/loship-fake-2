import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { MENU_STOCK_DISH_HISTORY, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import PropTypes from 'prop-types';
import ListStockHistory from 'components/pages/merchant/manager/popup/dish-stock-history/_list-stock_history';

class MenuStockDishHistory extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_STOCK_DISH_HISTORY },
  };

  close = () => {
    this.props.dispatch(close(MENU_STOCK_DISH_HISTORY));
  };

  render() {
    const { data } = this.props;
    if (!data) return null;
    const params = data.get('params') || {};
    const lastStockAddedAt = data.getIn(['params', 'lastStockAddedAt']);
    const dishId = data.getIn(['params', 'dishId']);
    const merchantId = data.getIn(['params', 'merchant']);

    return (
      <Popup className="menu-stock-history-popup" handleClose={() => this.close(true)}>
        <div className='content'>
          <div className="stock-dish-name">{this.getString('Lịch sử nhập kho')}: {params.dishName}</div>
          <div className='list-stock-history'>
            <ListStockHistory merchant={merchantId} params={{ dishId }} timeStockIn={lastStockAddedAt} />
          </div>
        </div>
      </Popup>
    );
  }
}

export default initComponent(MenuStockDishHistory);
