import { acquireDiffInfo, fetchOldData, generateDiffEditor } from 'utils/merchant';

import Button from 'components/shared/button';
import React from 'react';
import { cloneDeep } from 'lodash';
import initComponent from 'lib/initComponent';
import Checkbox from 'components/shared/checkbox';
import { genShortId } from 'utils/data';
import UpdateFranchisedContent from '../menu-update-franchised/_content-base';

function generateSrcMenu(src, newData = {}) {
  const menu = cloneDeep(src);

  function generateSrcCustom(custom, newCustomOptions = []) {
    let newArr = [];
    if (newData.type === 'customOption' && newData.customName === custom.name) newArr = [newData];
    else if (newData.type === 'custom' && newData.name === custom.name) newArr = newCustomOptions;

    if (newArr.length > 0) {
      const customOptions = custom.customOptions || [];
      newArr.forEach((newC) => {
        const cIndex = customOptions.findIndex((c) => c.name === newC.oldName || c.name === newC.name);
        const newCustomOption = { ...customOptions[cIndex], ...newC };
        if (cIndex < 0) newCustomOption.id = genShortId();
        /**
         * index >= 0 -> splice
         * !isDelete -> push
         */
        if (cIndex >= 0) customOptions.splice(cIndex, 1);
        if (!newCustomOption.isDelete) customOptions.push(newCustomOption);
      });
      return { ...custom, customOptions };
    }
    return custom;
  }
  function generateSrcDish(dish) {
    if (newData.type === 'custom') {
      const customs = dish.customs || [];
      const cIndex = customs.findIndex((c) => c.name === newData.oldName || c.name === newData.name);
      const newCustom = { ...customs[cIndex], ...newData };
      if (cIndex < 0) newCustom.id = genShortId();
      /**
       * index >= 0 -> splice
       * !isDelete -> push
       */
      const newCustomOptions = newCustom.customOptions;
      delete newCustom.customOptions;
      if (cIndex >= 0) customs.splice(cIndex, 1);
      if (!newCustom.isDelete) customs.push(newCustom);
      return { ...dish, customs: customs.map((custom) => generateSrcCustom(custom, newCustomOptions)) };
    }
    return { ...dish, customs: (dish.customs || []).map(generateSrcCustom) };
  }
  function generateSrcGroup(group) {
    return { ...group, dishes: (group.dishes || []).map(generateSrcDish) };
  }

  return menu.map(generateSrcGroup);
}

class CustomCloneContent extends UpdateFranchisedContent {
  componentDidMount() {
    this.getSrcMenu();
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (e) => {
    if (e.key === 'Enter' && !e.shiftKey) this.handleSyncMenu();
  };

  render() {
    if (!this.state.srcMenu) return null;

    const { isPerforming } = this.props;
    const { progress, diffCount, diffResolved } = this.state;
    const [srcMenu, destMenu] = acquireDiffInfo(this.state.srcMenu, this.props.menu);

    return (
      <div className="update-franchised-content">
        <div className="action text-left">
          <Checkbox className="reverse" {...this.allMenuCheckLink()}>
            Đồng bộ toàn menu
          </Checkbox>
        </div>
        <div className="diff-editor">
          <div className="diff-line diff-title">
            <div className="diff-line-item">Menu hiện tại</div>
            <div className="diff-line-item new">Menu sau khi đồng bộ</div>
          </div>
          {generateDiffEditor(srcMenu, destMenu, {
            onToggleAcceptDiff: this.onToggleAcceptDiff,
            onToggleAcceptGroup: this.onToggleAcceptGroup,
            onExpandCustom: this.onExpandCustom,
            acceptedDiff: this.state.acceptedDiff,
            expandedCustom: this.state.expandedCustom,
          })}
        </div>
        <div className="diff-progress">
          {isPerforming && (
            <div className="diff-progress-label">
              {diffResolved} / {diffCount}
            </div>
          )}
          <div className="diff-progress-resolved" style={{ width: `${progress}%` }} />
        </div>
        <div className="action">
          {!isPerforming && <Button onClick={this.props.handleClose}>{this.getString('close')}</Button>}
          {!isPerforming && (
            <Button type="feature" onClick={this.handleSyncMenu}>
              {this.getString('execute')}
            </Button>
          )}
          {isPerforming && <Button onClick={this.handleCancel}>{this.getString('EWVVPSD4D')}</Button>}
        </div>
      </div>
    );
  }

  getSrcMenu = () => {
    const { merchant, srcMenu: menu, newData: tmp } = this.props;
    const oldData = fetchOldData(merchant);
    const oldItem = oldData.find((item) => item.id === tmp.id && item.type === tmp.type);
    const newData = { ...tmp, oldName: oldItem && oldItem.name };
    delete newData.id;

    const srcMenu = generateSrcMenu(menu, newData);
    this.setState({ srcMenu });
  };

  allMenuCheckLink = () => {
    const { isCheckedAll } = this.state;
    const [_, destMenu] = acquireDiffInfo(this.state.srcMenu, this.props.menu);

    return {
      checked: isCheckedAll,
      onChange: (checked) => {
        this.setState({ isCheckedAll: checked }, () => {
          destMenu.forEach((group) => setTimeout(() => this.onToggleAcceptGroup(`group-${group.id}`, checked), 0));
        });
      },
    };
  };
}

export default initComponent(CustomCloneContent);
