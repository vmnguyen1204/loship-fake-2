import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
// import CreateDishDetail from './_detail-create';
import { MENU_CLONE_DISH_CUSTOM, close } from 'actions/popup';
import { get as getPopup } from 'reducers/popup';
import { generateCompactMenu } from 'utils/merchant';
import { getMerchantMenu } from 'reducers/menu-manager';
import CloneMenuCustomPopupContent from './_content';

class CustomClonePopup extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (!data) return;

      const menuParams = data.getIn(['data', 'params']) || {};
      const menu = getMerchantMenu(state, menuParams);
      if (menu) data = data.set('menu', menu.get('data'));

      return data;
    },
    params: { popup: MENU_CLONE_DISH_CUSTOM },
  };

  close = () => {
    this.props.dispatch(close(MENU_CLONE_DISH_CUSTOM));
  };

  render() {
    const { menu } = this.props;
    if (!menu) return null;

    const params = this.props.data.get('params');
    const newData = params.newData || {};
    return (
      <Popup className="update-franchised-popup" handleClose={() => this.close(true)}>
        <div className="heading">
          <h3>{this.getString('Cập nhật menu')}</h3>
        </div>

        <CloneMenuCustomPopupContent
          menu={generateCompactMenu(menu.toJS().group)}
          srcMenu={generateCompactMenu(menu.toJS().group, params.merchant)}
          merchant={params.merchant}
          triggerPerformAction={this.performAction}
          isPerforming={this.state.isPerforming}
          handleClose={this.close}
          newData={newData}
        />
      </Popup>
    );
  }

  performAction = (isPerforming, callback) => {
    this.setState({ isPerforming }, callback);
  };
}

export default initComponent(CustomClonePopup);
