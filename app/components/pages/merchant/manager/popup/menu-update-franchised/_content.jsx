import { CUSTOM_CONTENT, open } from 'actions/popup';
import { acquireDiffInfo, generateDiffEditor } from 'utils/merchant';

import Button from 'components/shared/button';
import React from 'react';
import initComponent from 'lib/initComponent';
import localStorage from 'utils/shims/localStorage';
import Checkbox from 'components/shared/checkbox';
import Hint from 'components/statics/hint';
import UpdateFranchisedContent from './_content-base';

class FormContent extends UpdateFranchisedContent {
  componentDidMount() {
    this.getSrcMenu();
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (e) => {
    if (e.key === 'Enter' && !e.shiftKey) this.handleSyncMenu();
  };

  render() {
    if (!this.state.srcMenu) return null;

    const { isPerforming } = this.props;
    const { progress, diffCount, diffResolved } = this.state;
    const [srcMenu, destMenu] = acquireDiffInfo(this.state.srcMenu, this.props.menu);
    const keepOldData = this.props.behavior && this.props.behavior.includes('keep_old_menu');

    return (
      <div className="update-franchised-content">
        <div className="diff-editor">
          <div className="diff-line diff-title">
            <div className="diff-line-item">Menu hiện tại</div>
            <div className="diff-line-item new">Menu sau khi đồng bộ</div>
          </div>
          {generateDiffEditor(srcMenu, destMenu, {
            onExpandCustom: this.onExpandCustom,
            expandedCustom: this.state.expandedCustom,
          })}
        </div>
        <div className="diff-progress">
          {isPerforming && (
            <div className="diff-progress-label">
              {diffResolved} / {diffCount}
            </div>
          )}
          <div className="diff-progress-resolved" style={{ width: `${progress}%` }} />
        </div>
        <div className="action">
          <div className="pull-left">
            <Checkbox {...this.deleteModeCheckLink()} disabled={keepOldData}>
              Xóa menu cũ{' '}
              <Hint
                text="Nếu chọn tùy chọn này, toàn bộ những item có thay đổi sẽ được xóa sau khi đồng bộ hoàn tất."
              />
            </Checkbox>
          </div>
          {!isPerforming && <Button onClick={this.props.handleClose}>{this.getString('close')}</Button>}
          {!isPerforming && (
            <Button type="feature" onClick={this.handleSyncMenu}>
              {this.getString('execute')}
            </Button>
          )}
          {isPerforming && <Button onClick={this.handleCancel}>{this.getString('EWVVPSD4D')}</Button>}
        </div>
      </div>
    );
  }

  getSrcMenu = () => {
    try {
      const code = localStorage.getItem('lastMenuSynchronizationCode');
      const data = JSON.parse(localStorage.getItem(code));
      this.setState({ code, srcMenu: data });
    } catch (e) {
      console.error('ERROR: ', e);
      this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: 'Không thể đồng bộ menu',
          content: 'Lên slack #tech liên hệ @ryenguyen7411 ngay!',
        }),
      );
    }
  };

  deleteModeCheckLink = () => {
    const { isDeleteMode } = this.state;
    return {
      checked: isDeleteMode,
      onChange: (checked) => this.setState({ isDeleteMode: checked }),
    };
  };
}

export default initComponent(FormContent);
