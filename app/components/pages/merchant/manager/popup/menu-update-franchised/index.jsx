import { CUSTOM_CONTENT, MENU_UPDATE_FRANCHISED, close, open } from 'actions/popup';
import { genCode } from 'utils/data';

import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import Popup from 'components/shared/popup';
import React from 'react';
import { get as dataGetter } from 'reducers/popup';
import initComponent from 'lib/initComponent';
import localStorage from 'utils/shims/localStorage';
import { MIN_MAIN_DISH_PRICE, isGroupDishHasInvalidDish } from 'utils//menu-tool';
import { cleanUpOldData, generateCompactMenu } from 'utils/merchant';
import FormContent from './_content';

const CODE_LEN = 16;

class CopyCodeContent extends React.Component {
  state = {}

  componentDidMount() {
    this.copyCode('copyCodeText');
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }


  render() {
    const { code } = this.props;
    const { isCopied } = this.state;

    return (
      <div className="copy-code">
        {this.props.getString('Chuyển sang menu cần cập nhật và dán đoạn code: ')}
        <a className="btn-copy-code" onClick={() => this.copyCode('copyCodeText')}>
          <b>{code}</b>
        </a>
        <textarea id="copyCodeText" value={code} readOnly />
        {this.props.getString(' vào ô Nhập mã.')}
        <br />

        <small className="text-right">
          {this.props.getString('* Tip: Nhấn vào code để copy mã nhanh.')}
          <br />
          {isCopied && <div className="green">{this.props.getString('Đã copy')}</div>}
        </small>
      </div>
    );
  }

  copyCode = (elementId) => {
    const copyCodeText = document.getElementById(elementId);
    copyCodeText.select();
    document.execCommand('Copy');

    this.setState({ isCopied: true }, () => {
      this.timeout = setTimeout(() => {
        this.setState({ isCopied: false });
      }, 5000);
    });
  };
}

class UpdateFranchised extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: MENU_UPDATE_FRANCHISED },
  };

  constructor(props) {
    super(props);
    this.initialState = { isPerforming: false, step: 'PRE', code: undefined, showCode: false, error: undefined };
    this.state = { ...this.initialState };
  }

  close = () => {
    this.props.dispatch(close(MENU_UPDATE_FRANCHISED));
    this.setState(this.initialState);
  };

  componentDidUpdate(prevProps) {
    const prevParams = prevProps.data?.get('params') || {};
    const params = this.props.data?.get('params') || {};
    if (!prevParams.code && params.code) this.handleParseDataFromCode(params.code);
  }

  render() {
    const params = this.props.data.get('params');
    const { step, error } = this.state;

    return (
      <Popup className="update-franchised-popup" handleClose={this.state.isPerforming ? () => {} : this.close}>
        <div className="heading">
          <h3>{this.getString('Đồng bộ menu')}</h3>
        </div>
        {step === 'PRE' && (
          <div className="step-pre">
            <p>
              {this.getString(
                'Đây là tool hỗ trợ đồng bộ nhanh menu giữa nhiều cửa hàng. Vui lòng chọn 1 hành động để tiếp tục',
              )}
            </p>
            <p>
              <Button type="feature" onClick={this.generateSyncData}>
                Đánh dấu menu hiện tại là menu nguồn
              </Button>
            </p>
            <p>
              <b>{this.getString('Hoặc')}</b>
            </p>
            <p>
              <Button type="feature" onClick={this.showCodeInput}>
                Cập nhật menu với cửa hàng khác
              </Button>
            </p>

            {this.state.showCode && (
              <p className="input">
                <input
                  ref="input"
                  onKeyUp={this.handleKeyUp}
                  onPaste={this.handlePaste}
                  placeholder={this.getString('Nhập mã và nhấn enter...')}
                />
                <Button
                  type="feature"
                  onClick={() => this.handleParseDataFromCode(this.refs.input && this.refs.input.value)}
                >
                  {this.getString('Thực hiện')}
                </Button>
              </p>
            )}
            {error && <p className="text-error">{error}</p>}
          </div>
        )}

        {step === 'SYNC' && (
          <FormContent
            menu={generateCompactMenu(params.groups)}
            merchant={params.merchant}
            triggerPerformAction={this.performAction}
            isPerforming={this.state.isPerforming}
            handleClose={this.close}
            behavior={params.behavior}
          />
        )}
      </Popup>
    );
  }

  performAction = (isPerforming, callback) => {
    this.setState({ isPerforming }, callback);
  };

  switchToSyncStep = () => {
    this.setState({ step: 'SYNC' });
  };

  menuValidation = async () => {
    return new Promise((resolve) => {
      const params = this.props.data.get('params');
      if (!Array.isArray(params.groups) || params.groups.length === 0) resolve(false);

      for (const group of params.groups) {
        if (!group.isExtraGroupDish && isGroupDishHasInvalidDish(group.dishes, MIN_MAIN_DISH_PRICE)) {
          resolve(false);
          break;
        }
      }

      resolve(true);
    });
  };

  generateSyncData = async () => {
    const isValidMenu = await this.menuValidation();
    if (!isValidMenu) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('Cannot copy menu'),
          content: this.getString(
            'This menu is invalid. It either has main dish with price < 12k, or empty. Please check and try again later.',
          ),
        }),
      );
    }

    const params = this.props.data.get('params');
    const data = generateCompactMenu(params.groups, params.merchant);
    cleanUpOldData(params.merchant);

    const code = genCode(CODE_LEN);
    const lastCode = localStorage.getItem('lastMenuSynchronizationCode');
    localStorage.removeItem(lastCode);
    localStorage.setItem(code, JSON.stringify(data));
    localStorage.setItem('lastMenuSynchronizationCode', code);

    this.props.dispatch(
      open(CUSTOM_CONTENT, {
        content: () => (
          <CopyCodeContent code={code} getString={this.getString} />
        ),
      }),
    );
    this.close();
  };

  showCodeInput = () => {
    this.setState({ showCode: true }, () => {
      this.refs.input && this.refs.input.focus();
    });
  };

  handleKeyUp = (e) => {
    if (e.keyCode === 13) {
      this.handleParseDataFromCode(e.target.value);
    }
  };

  handlePaste = (e) => {
    const code = (e.clipboardData || window.clipboardData).getData('text');
    this.handleParseDataFromCode(code);
  };

  handleParseDataFromCode = (input) => {
    if (!input) return;

    const savedCode = localStorage.getItem('lastMenuSynchronizationCode');
    if (savedCode !== input) {
      return this.setState({ error: this.getString('Mã không hợp lệ, vui lòng kiểm tra lại.') });
    }

    this.switchToSyncStep();
  };
}

export default initComponent(UpdateFranchised);
