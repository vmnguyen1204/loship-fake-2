import { CONFIRM_POPUP, CUSTOM_CONTENT, open } from 'actions/popup';
import {
  createDish,
  createDishCustom,
  createDishCustomOption,
  createMenuGroup,
  deleteDish,
  deleteMenuGroup,
  fetchMerchantMenu,
  updateDish,
  updateDishCustom,
  updateDishCustomOption,
  updateMenuGroup,
} from 'actions/merchant-manager';

import BaseComponent from 'components/base';
import { Map } from 'immutable';
import React from 'react';
import { acquireDiffCount, acquireDiffInfo } from 'utils/merchant';
import { capitalizes } from 'utils/format';

import { parseError } from 'utils/error';
import { genShortId } from 'utils/data';
import * as LoshipImage from 'utils/LoshipImage';
import PropTypes from 'prop-types';

function getAcceptedChildren(menu, isChecked, itemId, type = 'group') {
  const res = {};
  menu.some((g) => {
    if (type === 'group' && g.id.toString() !== itemId) return false;
    let founded = false;
    g.dishes
      .flatMap((dish) => dish.customs || [])
      .forEach((custom) => {
        if (type === 'custom' && custom.id.toString() !== itemId) return;

        const id = `custom-${custom.id}`;
        res[id] = isChecked;
        founded = true;

        (custom.customOptions || []).forEach((c) => {
          const cId = `customOption-${c.id}`;
          res[cId] = isChecked;
        });
      });

    if (type === 'group' && g.id.toString() === itemId) return true;
    return founded; // prevent waste calc
  });

  return res;
}

class UpdateFranchisedContent extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.initialState = {
      error: [],
      isCompleted: false,
      acceptedDiff: {},
      diffResolved: 0,
      progress: 0,
      isDeleteMode: true,
      isCheckedAll: true,
      expandedCustom: {},
    };
    this.state = this.initialState;
  }

  handleCancel = () => {
    this.props.dispatch(
      open(CONFIRM_POPUP, {
        title: this.getString('stop_sync_menu'),
        content: this.getString('stop_sync_menu_description'),
        onConfirm: () => this.props.triggerPerformAction(false),
      }),
    );
  };

  handleSyncMenu = () => {
    const [_, destMenu] = acquireDiffInfo(this.state.srcMenu, this.props.menu);
    const diffCount = acquireDiffCount(destMenu);
    /**
     * 1. Loop through destMenu
     *    - POST -> create group
     *    - PUT -> update group
     *    - !srcMap -> DELETE -> delete group -> done
     *    2. Loop through group (POST, PUT)
     *      - parent POST -> create dish
     *      - POST -> create dish
     *      - PUT -> update dish
     *      - !srcMap -> DELETE -> delete dish -> done
     *      3. Loop through dish (POST, PUT)
     *        - parent POST -> create custom
     *        - POST -> create custom
     *        - PUT -> update custom
     *        - !srcMap -> DELETE -> delete custom -> done
     *        4. Loop through custom (POST, PUT)
     *          - parent POST -> create custom option -> done
     *          - POST -> create custom option -> done
     *          - PUT -> update custom option -> done
     *          - !srcMap -> delete custom option -> done
     */
    this.props.triggerPerformAction(true, () => {
      this.setState(
        { isCompleted: false, error: [], errorMessages: {}, diffResolved: 0, diffCount, progress: 0 },
        async () => {
          await this.handleSyncGroups(destMenu);

          this.props.triggerPerformAction(false, () => {
            const { error = [], errorMessages = {} } = this.state;
            this.props.dispatch(
              open(CUSTOM_CONTENT, {
                title: this.getString(error.length === 0 ? 'Đồng bộ menu thành công' : 'Đồng bộ đã hoàn tất, nhưng...'),
                content: error.length > 0 && [
                  this.getString('Có 1 vài món/tùy chọn không đồng bộ được'),
                  ...error.map((e) => (
                    <div key={e.id}>
                      <b>{e.item}</b>
                      {errorMessages[e.id] && ` | ${this.getString(errorMessages[e.id])}`}
                      {e.stack && (
                        <div>
                          <small style={{ fontStyle: 'italic', color: '#9c9c9c' }}>({e.stack})</small>
                        </div>
                      )}
                    </div>
                  )),
                  this.getString('Bạn hãy kiểm tra lại hoặc lên slack #tech liên hệ @ryenguyen7411 ngay!'),
                ],
                action: error.length === 0 && this.props.handleClose,
              }),
            );
            this.props.dispatch(fetchMerchantMenu({ merchant: this.props.merchant, isAdmin: this.context.isAdmin }));
          });
        },
      );
    });
  };

  handleSyncGroups = async (groups) => {
    for (const group of groups) {
      let groupDishId = group.id;
      if (this.isDiffAccepted(group.id, 'group')) {
        if (!group.srcMap && !['POST', 'PUT'].includes(group.method)) {
          if (!this.state.isDeleteMode && !group.active) continue;
          if (this.props.behavior && this.props.behavior.includes('keep_old_menu')) continue;
          // console.log('DELETE group', group.name, group.id);
          // return;
          const handler = this.state.isDeleteMode ? deleteMenuGroup : updateMenuGroup;
          await new Promise((resolve) => {
            this.props.dispatch(
              handler({
                groupDishId: group.id,
                groupData: new Map({ ...group, active: false }),
                merchant: this.props.merchant,
                refetchMenu: false,
                callback: () => this.resolveAndReportProgress(group, 'group', { resolve }),
                callbackFailure: (error) => this.resolveAndReportProgress(group, 'group', { resolve, error }),
                isAdmin: this.context.isAdmin,
              }),
            );
          });
          continue;
        }

        if (['POST', 'PUT'].includes(group.method)) {
          groupDishId = await new Promise((resolve) => {
            const { name, active, isPinned, isExtraGroupDish } = group.newData || group;
            // const groupDishId = group.id || 'POST-id';
            // console.log(`${group.method} group`, group.name, groupDishId);
            // return resolve(groupDishId);
            const handler = group.method === 'POST' ? createMenuGroup : updateMenuGroup;
            this.props.dispatch(
              handler({
                groupDishId: group.id,
                groupData: new Map({ name, active, isPinned, isExtraGroupDish }),
                merchant: this.props.merchant,
                refetchMenu: false,
                callback: (res) => this.resolveAndReportProgress(group, 'group', {
                  resolve,
                  resolveData: res.body.data && res.body.data.id,
                }),
                callbackFailure: (error) => this.resolveAndReportProgress(group, 'group', { resolve, error }),
                isAdmin: this.context.isAdmin,
              }),
            );
          });
        }
      }

      if (!Array.isArray(group.dishes) || group.dishes.length === 0) continue;
      await this.handleSyncDishes(
        group.dishes.map((d) => ({ ...d, method: group.method === 'POST' ? 'POST' : d.method })),
        { group: { name: group.name, id: groupDishId } },
      );
    }
  };

  handleSyncDishes = async (dishes, extraData = {}) => {
    for (const dish of dishes) {
      if (!extraData.group || !extraData.group.id) {
        console.error('ERROR in handleSyncDishes: no groupId provided');
        continue;
      }

      let dishId = dish.id;
      if (this.isDiffAccepted(dish.id, 'dish')) {
        if (!dish.srcMap && !['POST', 'PUT'].includes(dish.method)) {
          if (!this.state.isDeleteMode && !dish.active) continue;
          if (this.props.behavior && this.props.behavior.includes('keep_old_menu')) continue;
          // console.log('DELETE dish', dish.name, dish.id);
          // return;
          const handler = this.state.isDeleteMode ? deleteDish : updateDish;
          await new Promise((resolve) => {
            this.props.dispatch(
              handler({
                groupDishId: extraData.group.id,
                dishId: dish.id,
                dishData: new Map({ ...dish, active: false }),
                merchant: this.props.merchant,
                callback: () => this.resolveAndReportProgress(dish, 'dish', { resolve, extraData }),
                callbackFailure: (error) => this.resolveAndReportProgress(dish, 'dish', { resolve, extraData, error }),
                isAdmin: this.context.isAdmin,
              }),
            );
          });
          continue;
        }

        if (['POST', 'PUT'].includes(dish.method)) {
          // eslint-disable-next-line no-async-promise-executor
          dishId = await new Promise(async (resolve) => {
            const { name, active, description, price, additionalFee, unit, unitQuantity } = dish.newData || dish;
            // const dishId = dish.id || 'POST-id';
            // console.log(`${dish.method} dish`, dish.name, dishId);
            // return resolve(dishId);
            const handler = dish.method === 'POST' ? createDish : updateDish;
            let image = dish.image;
            if (this.props.behavior && this.props.behavior.includes('fetch_image')) {
              try {
                const imageData = await LoshipImage.getImageFromUrl(image);
                image = await imageData.getURL();
              } catch (e) {
                console.error(`ERROR: cannot fetch image from ${dish.image}`, e);
                return resolve();
              }
            }

            this.props.dispatch(
              handler({
                groupDishId: extraData.group.id,
                dishId: dish.id,
                dishData: new Map({
                  ...extraData,
                  name,
                  active,
                  description,
                  price,
                  additionalFee,
                  image,
                  copyImageFromDish: dish.srcMap,
                  unit, unitQuantity,
                }),
                merchant: this.props.merchant,
                callback: (res) => this.resolveAndReportProgress(dish, 'dish', {
                  resolve,
                  resolveData: res.body.data && res.body.data.id,
                  extraData,
                }),
                callbackFailure: (error) => this.resolveAndReportProgress(dish, 'dish', { resolve, extraData, error }),
                isAdmin: this.context.isAdmin,
              }),
            );
          });
        }
      }

      if (!Array.isArray(dish.customs) || dish.customs.length === 0 || !dishId) continue;
      await this.handleSyncCustoms(
        dish.customs.map((c) => ({ ...c, method: dish.method === 'POST' ? 'POST' : c.method })),
        { ...extraData, dish: { name: dish.name, id: dishId } },
      );
    }
  };

  handleSyncCustoms = async (customs, extraData = {}) => {
    for (const custom of customs) {
      if (!extraData.group || !extraData.group.id || !extraData.dish || !extraData.dish.id) {
        console.error('ERROR in handleSyncCustoms: no groupDishId/dishId provided');
        continue;
      }

      let customId = custom.id;
      if (this.isDiffAccepted(custom.id, 'custom')) {
        if (!custom.srcMap && !['POST', 'PUT'].includes(custom.method)) {
          if (!this.state.isDeleteMode && !custom.active) continue;
          if (this.props.behavior && this.props.behavior.includes('keep_old_menu')) continue;
          // console.log('DELETE custom', custom.name, custom.id);
          // return;
          await new Promise((resolve) => {
            this.props.dispatch(
              updateDishCustom({
                groupDishId: extraData.group.id,
                dishId: extraData.dish.id,
                customId: custom.id,
                customData: new Map({ ...custom, active: false }),
                merchant: this.props.merchant,
                callback: () => this.resolveAndReportProgress(custom, 'custom', { resolve, extraData }),
                callbackFailure: (error) => this.resolveAndReportProgress(custom, 'custom', { resolve, extraData, error }),
                isAdmin: this.context.isAdmin,
              }),
            );
          });
          continue;
        }

        if (['POST', 'PUT'].includes(custom.method)) {
          customId = await new Promise((resolve) => {
            const { name, active, selectionType, limitQuantity, note } = custom.newData || custom;
            // const customId = custom.id || 'POST-id';
            // console.log(`${custom.method} custom`, custom.name, customId);
            // return resolve(customId);
            const handler = custom.method === 'POST' ? createDishCustom : updateDishCustom;
            this.props.dispatch(
              handler({
                groupDishId: extraData.group.id,
                dishId: extraData.dish.id,
                customId: custom.id,
                customData: new Map({ ...extraData, name, active, selectionType, limitQuantity, note }),
                merchant: this.props.merchant,
                callback: (res) => this.resolveAndReportProgress(custom, 'custom', {
                  resolve,
                  resolveData: res.body.data && res.body.data.id,
                  extraData,
                }),
                callbackFailure: (error) => this.resolveAndReportProgress(custom, 'custom', { resolve, extraData, error }),
                isAdmin: this.context.isAdmin,
              }),
            );
          });
        }
      }

      if (!Array.isArray(custom.customOptions) || custom.customOptions.length === 0) continue;
      await this.handleSyncCustomOptions(
        custom.customOptions.map((c) => ({ ...c, method: custom.method === 'POST' ? 'POST' : c.method })),
        { ...extraData, custom: { name: custom.name, id: customId } },
      );
    }
  };

  handleSyncCustomOptions = async (customOptions, extraData = {}) => {
    for (const customOption of customOptions) {
      if (
        !extraData.group ||
        !extraData.group.id ||
        !extraData.dish ||
        !extraData.dish.id ||
        !extraData.custom ||
        !extraData.custom.id
      ) {
        console.error('ERROR in handleSyncCustomOptions: no groupDishId/dishId/customId provided');
        continue;
      }

      if (this.isDiffAccepted(customOption.id, 'customOption')) {
        if (!customOption.srcMap && !['POST', 'PUT'].includes(customOption.method)) {
          if (!this.state.isDeleteMode && !customOption.active) continue;
          if (this.props.behavior && this.props.behavior.includes('keep_old_menu')) continue;
          // console.log('DELETE custom option', customOption.name, customOption.id);
          // return;
          await new Promise((resolve) => {
            this.props.dispatch(
              updateDishCustomOption({
                groupDishId: extraData.group.id,
                dishId: extraData.dish.id,
                customId: extraData.custom.id,
                customOptionId: customOption.id,
                customOptionData: new Map({ ...customOption, value: customOption.name, active: false }),
                merchant: this.props.merchant,
                callback: () => this.resolveAndReportProgress(customOption, 'customOption', { resolve, extraData }),
                callbackFailure: (error) => this.resolveAndReportProgress(customOption, 'customOption', {
                  resolve,
                  extraData,
                  error,
                }),
                isAdmin: this.context.isAdmin,
              }),
            );
          });
          continue;
        }

        if (['POST', 'PUT'].includes(customOption.method)) {
          const { name: value, active, price, limitQuantity } = customOption.newData || customOption;
          // const customOptionId = customOption.id || 'POST-id';
          // console.log(`${customOption.method} custom option`, customOption.name, customOptionId);
          // return;
          const handler = customOption.method === 'POST' ? createDishCustomOption : updateDishCustomOption;
          await new Promise((resolve) => {
            this.props.dispatch(
              handler({
                groupDishId: extraData.group.id,
                dishId: extraData.dish.id,
                customId: extraData.custom.id,
                customOptionId: customOption.id,
                customOptionData: new Map({ ...extraData, value, active, price, limitQuantity }),
                merchant: this.props.merchant,
                callback: () => this.resolveAndReportProgress(customOption, 'customOption', { resolve, extraData }),
                callbackFailure: (error) => this.resolveAndReportProgress(customOption, 'customOption', {
                  resolve,
                  extraData,
                  error,
                }),
                isAdmin: this.context.isAdmin,
              }),
            );
          });
        }
      }
    }
  };

  logErrorMessage = (data, type, err, extraData) => {
    const { error = [], errorMessages = {} } = this.state;

    const errorId = genShortId();
    const errorItem = this.getString(`${capitalizes(type)}: {0}`, 'menuManage', [data.name]);
    const errorStack = (() => {
      const stack = [];
      if (extraData.group) stack.push(this.getString('group_0', 'menuManage', [extraData.group.name]));
      if (extraData.dish) stack.push(this.getString('dish_0', 'menuManage', [extraData.dish.name]));
      if (extraData.custom) stack.push(this.getString('custom_0', 'menuManage', [extraData.custom.name]));
      return stack.join(', ');
    })();
    error.push({ id: errorId, item: errorItem, stack: errorStack });

    const errorMessage = parseError(err.message);
    if (!errorMessage.includes('unknown_error')) errorMessages[errorId] = errorMessage;

    this.setState({ error, errorMessages });
  };

  onToggleAcceptGroup = (lineId, checked) => {
    const [_, destMenu] = acquireDiffInfo(this.state.srcMenu, this.props.menu);

    const { acceptedDiff } = this.state;
    acceptedDiff[lineId] = checked;

    const groupId = lineId.split('-').pop();
    const acceptedChildren = getAcceptedChildren(destMenu, checked, groupId);

    this.setState({ acceptedDiff: { ...acceptedDiff, ...acceptedChildren } });
  };

  onToggleAcceptDiff = (lineId, checked) => {
    let { acceptedDiff } = this.state;
    acceptedDiff[lineId] = checked;

    const [type, itemId] = lineId.split('-');
    if (type === 'custom') {
      const [_, destMenu] = acquireDiffInfo(this.state.srcMenu, this.props.menu);
      const acceptedChildren = getAcceptedChildren(destMenu, checked, itemId, 'custom');
      acceptedDiff = { ...acceptedDiff, ...acceptedChildren };
    }

    this.setState({ acceptedDiff });
  };

  isDiffAccepted = (id, type) => {
    if (!this.props.isPerforming) return false;

    const { acceptedDiff } = this.state;
    const diffItem = Object.entries(acceptedDiff).find((entries) => {
      const [itemType, itemId] = entries[0].split('-');
      return id.toString() === itemId.toString() && type === itemType;
    });

    return !diffItem || diffItem[1] !== false;
  };

  onExpandCustom = (id) => {
    const { expandedCustom } = this.state;

    expandedCustom[id] = !expandedCustom[id];
    this.setState({ expandedCustom });
  };

  resolveAndReportProgress = (data, type, { resolve, resolveData, error, extraData }) => {
    const diffResolved = this.state.diffResolved + 1;
    const diffCount = this.state.diffCount;

    const progress = Math.floor((diffResolved / diffCount) * 1000) / 10;
    this.setState(() => ({ progress, diffResolved }), () => {
      if (error) this.logErrorMessage(data, type, error, extraData);
      resolve(resolveData);
    });
  };
}

export default UpdateFranchisedContent;
