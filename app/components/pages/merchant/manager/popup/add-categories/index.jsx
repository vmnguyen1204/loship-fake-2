import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { get as dataGetter } from 'reducers/popup';
import { CUSTOM_CONTENT, MENU_ADD_CATEGORIES, close, open } from 'actions/popup';
import AddCategoriesContent from './_content';
import Button from 'components/shared/button';
import callAPI from 'actions/helpers';
import { LINK_AUTH } from 'settings/variables';
import { fetchCategoriesBySuperCategoryCityIdDistrictId } from 'actions/category';

class AddCategoriesPopup extends BaseComponent {

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_ADD_CATEGORIES },
  }

  close = () => {
    this.props.dispatch(close(MENU_ADD_CATEGORIES));
  };

  componentDidUpdate(prevProps) {
    const params = prevProps.data?.get('params');
    if (!params) return;
    const superCategoryId = params?.superCategoryId;
    const cityId = params?.cityId;
    this.props.dispatch(fetchCategoriesBySuperCategoryCityIdDistrictId({ superCategoryId, cityId }));
  }

  handleUpdateCategories = () => {
    const params = this.props.data.get('params');
    const { address, eateryId, name } = params;
    const dataCategories = this.state.data;
    const arrCategory = dataCategories?.map((item) => item.value);
    if (!arrCategory || !arrCategory.length)
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          content: this.getString('will_need_had_to_select_a_new_type'),
        }),
      );

    this.setState({ isUpdating: true },
      async () => {
        const res = await callAPI('put', {
          url: LINK_AUTH + `/eateries/${eateryId}`,
          content: {
            categories: arrCategory,
            name,
            address,
          },
        });
        if (!res || res.status !== 200) return alert('theres_unexpected_problem_prevent_us_to_process_your_action_sorry');
        setTimeout(() => {
          this.setState({ isUpdating: false, isUpdateSuccess: true });
          return window.location.reload();
        }, 500);
      },
    );
  }

  render() {
    const params = this.props.data.get('params');
    const superCategoryId = params.superCategoryId;
    const cityId = params.cityId;
    return (
      <Popup className="menu-categories-popup" handleClose={this.close}>
        {params.title && <div className="popup-header">{params.title}</div>}
        <div className="content">
          <div className="menu-manager-warning">
            <p>
              {this.getString('need_to_select_correct_type_litle_3_type')}
            </p>
          </div>
          <AddCategoriesContent
            defaultCategories={params.defaultCategories}
            onDataArrived={(category) => {
              this.setState({ data: category });
            }}
            data={this.state.data}
            params={{ superCategoryId, cityId }}
          />
        </div>

        <div className="popup-footer text-right">
          {this.state.isUpdating && <i className="status fas fa-spinner fa-spin" />}
          {this.state.isUpdateSuccess && <i className="status fas fa-check" />}
          <Button type="feature" onClick={this.handleUpdateCategories}>
            {this.getString('apply_changes')}
          </Button>
        </div>
      </Popup>
    );
  }
}

export default initComponent(AddCategoriesPopup);
