import { LINK_DOMAIN_LOZI, ROLES_CONTENT } from 'settings/variables';
import { Map } from 'immutable';
import { createDishCustom, updateDishCustom } from 'actions/merchant-manager';

import AuthByRole from 'components/shared/button/auth-by-role';
import BaseComponent from 'components/base';
import React from 'react';
import cx from 'classnames';
import initComponent from 'lib/initComponent';
import moment from 'utils/shims/moment';
import Button from 'components/shared/button';
import PropTypes from 'prop-types';

const defaultDataObject = new Map({
  name: '',
  price: 0,
  note: '',
  limitQuantity: 1,
  active: false,
});

const mapPropsToState = (props) => {
  if (!props.data || props.isCreate) return { data: defaultDataObject.set('selectionType', props.selectionType || 'single_choice') };

  const {
    id,
    name,
    price,
    note,
    limitQuantity,
    active,
    selectionType,
    createdBy,
    createdAt,
    updatedBy,
    updatedAt,
  } = props.data.toJS();

  return {
    data: new Map({
      id,
      name,
      oldName: name,
      price,
      note,
      limitQuantity,
      active,
      selectionType,
      createdBy,
      createdAt,
      updatedBy,
      updatedAt,
    }),
  };
};

class DishCustomContent extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = { data: new Map(defaultDataObject) };
  }

  componentDidMount() {
    this.setState(mapPropsToState(this.props));

    if (this.refs.txtCustomName) this.refs.txtCustomName.focus();
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (e) => {
    const isCreate = this.props.isCreate;
    if (e.key === 'Enter' && !e.shiftKey) isCreate ? this.handleCreateDishCustom(e) : this.handleUpdateDishCustom(e);
  };

  handleClose = () => {
    return this.props.handleClose();
  };

  handleCreateDishCustom = async (e) => {
    e.preventDefault();
    await new Promise((resolve) => this.props.dispatch(
      createDishCustom({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.dish.get('id'),
        customData: this.state.data,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    return this.handleClose();
  };

  handleUpdateDishCustom = async (e) => {
    e.preventDefault();
    await new Promise((resolve) => this.props.dispatch(
      updateDishCustom({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.dish.get('id'),
        customId: this.state.data.get('id'),
        customData: this.state.data,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    return this.handleClose();
  };

  handleInputChange = (field, e) => {
    let { data } = this.state;
    data = data.set(field, e.target.value);

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultDataObject, data));
  };

  handleCheckboxChange = (newValue) => {
    let { data } = this.state;
    data = data.set('active', newValue);

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultDataObject, data));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      srcData.get('note') !== data.get('note') ||
      srcData.get('limitQuantity') !== data.get('limitQuantity')
    );
  };

  toggleEditorInfo = () => {
    this.setState((state) => ({ ...state, showDetail: !state.showDetail }));
  };

  renderEditorInfo = () => {
    const { data } = this.state;
    const { createdAt, createdBy, updatedAt, updatedBy } = (data && data.toObject()) || {};

    // VALIDATE INPUT DATA
    if (!createdBy || !updatedBy) return null;

    return (
      <div className="history">
        <span>
          {this.getString('created_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + createdBy.username}>
            @{createdBy.username}
          </a>
          {this.getString('at')}
          {moment(createdAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <br />
        <span>
          {this.getString('updated_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + updatedBy.username}>
            @{updatedBy.username}
          </a>
          {this.getString('at')}
          {moment(updatedAt).format('HH:mm, DD-MM-YYYY')}
        </span>
      </div>
    );
  };

  render() {
    const data = this.state.data;
    const isSingleChoice = data.get('selectionType') === 'single_choice';
    const isCreate = this.props.isCreate;
    return (
      <div className="content">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          {isSingleChoice && (
            <div className="form-text">
              {this.getString(
                'Mục tùy chọn đơn nghĩa là khách chỉ được chọn 01 trong nhiều lựa chọn có sẵn. Ví dụ: chọn mức đường, mức đá của đồ uống, size món ăn,...',
              )}
            </div>
          )}
          {!isSingleChoice && (
            <div className="form-text">
              {this.getString(
                'Mục tùy chọn đa nghĩa là khách có quyền chọn nhiều mục cùng lúc. Ví dụ: các loại topping trà sữa, đồ ăn thêm,...',
              )}
            </div>
          )}
          {!isCreate && (
            <div className="form-group">
              <label>{this.getString('available')}</label>
              <div className="input">
                <div>
                  <a
                    title={this.getString('press_to_0_this_dish', '', [!data.get('active') ? 'bật' : 'tắt'])}
                    onClick={() => this.handleCheckboxChange(!data.get('active'))}
                  >
                    <i
                      className={cx('availability-status', { active: data.get('active') })}
                    />
                  </a>
                </div>
                <div className="input-note">
                  {this.getString(`Nếu bật, ${data.get('name')} sẽ xuất hiện trên menu để khách hàng chọn.`)}
                </div>
              </div>
            </div>
          )}
          <div className="form-group">
            <label>
              {this.getString('option_name')} <span className="required">*</span>
            </label>
            <div className="input">
              <input ref="txtCustomName" value={data.get('name')} onChange={(e) => this.handleInputChange('name', e)} />
            </div>
          </div>
          <div className="form-group">
            <label>{this.getString('note')}</label>
            <div className="input">
              <input value={data.get('note')} onChange={(e) => this.handleInputChange('note', e)} />
            </div>
          </div>
          {!isSingleChoice && (
            <div className="form-group">
              <label>
                {this.getString('ylARvvkqdz')} <span className="required">*</span>
              </label>
              <div className="input">
                <input
                  type="number"
                  min={1}
                  max={10}
                  ref="txtCustomLimitQuantity"
                  value={data.get('limitQuantity')}
                  onChange={(e) => this.handleInputChange('limitQuantity', e)}
                />
                <div className="input-note">{this.getString('maximum_choices_for_customers')}</div>
              </div>
            </div>
          )}

          <div className="form-action">
            {!isCreate && <AuthByRole roles={ROLES_CONTENT}>{this.renderEditorInfo()}</AuthByRole>}

            <Button onClick={this.props.handleClose}>{this.getString('abort')}</Button>
            {!isCreate && (
              <Button htmlType="submit" type="feature" onClick={this.handleUpdateDishCustom}>
                {this.getString('save')}
              </Button>
            )}
            {isCreate && (
              <Button htmlType="submit" type="feature" onClick={this.handleCreateDishCustom}>
                {this.getString('add')}
              </Button>
            )}
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(DishCustomContent);
