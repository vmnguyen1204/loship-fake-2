import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
// import CreateDishDetail from './_detail-create';
import { CONFIRM_POPUP, MENU_UPDATE_DISH_CUSTOM, close, open } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import DishCustomContent from './_content';

class UpdateMenuDishCustomPopup extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      customs: [],
    };
  }

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_UPDATE_DISH_CUSTOM },
  };

  close = (checkEditing = false) => {
    const params = this.props.data.get('params');
    const onClose = params.onClose;

    if (checkEditing === true && this.state.isEditing) {
      return this.props.dispatch(
        open(CONFIRM_POPUP, {
          title: this.getString('your_data_havent_save_yet'),
          content: this.getString('are_you_sure_you_want_to_leave_any_unsaved_changes_will_be_lost'),
          onConfirm: () => this.setState({ isEditing: false }, () => {
            this.props.dispatch(close(MENU_UPDATE_DISH_CUSTOM));
            typeof onClose === 'function' && onClose();
          }),
        }),
      );
    }

    this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_UPDATE_DISH_CUSTOM)));
    typeof onClose === 'function' && onClose();
  };

  render() {
    const params = this.props.data.get('params');
    const isCreate = params.isCreate;
    return (
      <Popup className="menu-dish-custom-update" handleClose={() => this.close(true)}>
        <div className="heading">
          <h3>
            {this.getString(isCreate ? 'Thêm mục tùy chọn' : 'Sửa mục tùy chọn')}{' '}
            {params.selectionType === 'single_choice' ? 'đơn' : 'đa'}
          </h3>
        </div>
        <DishCustomContent
          params={{
            merchant: params.merchant,
            groupDishId: params.groupDishId,
          }}
          isCreate={isCreate}
          data={params.custom}
          dish={params.dish}
          selectionType={params.selectionType}
          handleClose={this.close}
          toggleEditing={this.toggleEditing}
        />
      </Popup>
    );
  }

  toggleEditing = (isEditing = false) => {
    this.setState({ isEditing });
  };
}

export default initComponent(UpdateMenuDishCustomPopup);
