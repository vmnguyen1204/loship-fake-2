import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import cx from 'classnames';
import {
  fetchLoshipDishImages,
  fetchLoziBlockImages,
  fetchMoreLoshipDishImages,
  fetchMoreLoziBlockImages,
} from './actions';

const TABS = {
  LOSHIP: 'Loship',
  LOZI: 'Lozi',
};

class SearchDishImagePopupContent extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      keyword: '',
      data: null,
      loziData: null,
      nextUrl: null,
      loziNextUrl: null,
      isLoading: false,
      isLoadingMore: false,
      currentTab: TABS.LOSHIP,
    };
  }

  componentDidMount() {
    if (!this.state.keyword) {
      this.setState({ keyword: this.props.keyword }, () => {
        this.searchImages(this.state.keyword);
      });
    }
  }

  handleInputChange = (e) => {
    const value = e.target.value;

    if (this.state.keyword === value) return;

    this.setState(
      { keyword: value },
      () => {
        if (this.timeout) clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          this.searchImages(this.state.keyword);
        }, 500);
      },
    );
  };

  handleSelectDish = (ev) => {
    const { id, image } = ev.target.dataset;
    this.props.handleSelectDish({ id, image });
    this.props.handleClose();
  };

  setActiveTab = (tabKey) => {
    this.setState(
      { currentTab: TABS[tabKey] },
      () => {
        if (
          (this.state.currentTab === TABS.LOSHIP && !this.state.data) ||
          (this.state.currentTab === TABS.LOZI && !this.state.dataLozi)
        ) this.searchImages(this.state.keyword);
      },
    );
  };

  searchImages = (searchKeyword) => {
    const keyword = searchKeyword.trim();
    if (!keyword) return;
    this.setState({ isLoading: true }, async () => {
      switch (this.state.currentTab) {
        case TABS.LOSHIP: {
          const resLoship = await fetchLoshipDishImages(keyword);
          this.setState({
            data: resLoship.data,
            nextUrl: resLoship.nextUrl,
            isLoading: false,
          });
          break;
        }
        case TABS.LOZI: {
          const resLozi = await fetchLoziBlockImages(keyword);

          this.setState({
            dataLozi: resLozi.data,
            nextUrlLozi: resLozi.nextUrl,
            isLoading: false,
          });
          break;
        }
        default:
          break;
      }
    });
  };

  loadMore = () => {
    let nextUrl;
    switch (this.state.currentTab) {
      case TABS.LOSHIP: {
        nextUrl = this.state.nextUrl;
        this.setState(
          {
            nextUrl: null,
            isLoadingMore: true,
          },
          async () => {
            const resLoship = await fetchMoreLoshipDishImages(nextUrl);
            this.setState((state) => ({
              data: state.data.concat(resLoship.data),
              nextUrl: resLoship.nextUrl,
              isLoadingMore: false,
            }));
          },
        );
        break;
      }
      case TABS.LOZI: {
        nextUrl = this.state.nextUrlLozi;
        this.setState(
          {
            nextUrl: null,
            isLoadingMore: true,
          },
          async () => {
            const resLozi = await fetchMoreLoziBlockImages(nextUrl);

            this.setState((state) => ({
              dataLozi: state.dataLozi.concat(resLozi.data),
              nextUrlLozi: resLozi.nextUrl,
              isLoadingMore: false,
            }));
          },
        );
        break;
      }
      default:
        break;
    }
  };

  render() {
    const { keyword, currentTab } = this.state;
    return (
      <div className="content">
        <div className="search-bar">
          <div className="form inline">
            <div className="form-group">
              <label>{this.getString('input_your_dish')}</label>
              <div className="input">
                <input type="text" onChange={this.handleInputChange} value={keyword} />
              </div>
            </div>
            <a className="btn btn-search">{this.getString('find_images')}</a>
          </div>
        </div>
        <div className="tab-bar">
          <ul>
            {Object.keys(TABS).map((tabKey) => (
              <li
                key={tabKey}
                onClick={() => this.setActiveTab(tabKey)}
                className={cx({ active: this.state.currentTab === TABS[tabKey] })}
              >
                {this.getString('image_from')} {TABS[tabKey]}
              </li>
            ))}
          </ul>
        </div>
        {this.renderResultTab(currentTab)}
      </div>
    );
  }

  renderResultTab = (currentTab) => {
    const { isLoading, isLoadingMore } = this.state;
    let data, nextUrl;
    switch (currentTab) {
      case TABS.LOSHIP:
        data = this.state.data;
        nextUrl = this.state.nextUrl;
        break;
      case TABS.LOZI:
        data = this.state.dataLozi;
        nextUrl = this.state.nextUrlLozi;
        break;
      default:
        return false;
    }
    return (
      <div className="search-results">
        {isLoading && (
          <span>
            <i className="fas fa-hourglass-half" /> {this.getString('uploading')}
          </span>
        )}
        {!isLoading && data && (
          <div className="result-list">
            <ul>
              {data.map((dish) => {
                return (
                  <li key={dish.id}>
                    <div>
                      <img onClick={this.handleSelectDish} src={dish.image} alt="Dish" data-image={dish.image} data-id={dish.id} />
                    </div>
                  </li>
                );
              })}
            </ul>
            {!isLoadingMore && nextUrl && (
              <div className="text-center">
                <button type="button" className="btn load-more" onClick={this.loadMore}>
                  {this.getString('view_more')}
                </button>
              </div>
            )}
            {isLoadingMore && (
              <div className="text-center">
                <i className="fa fa-hourglass-half" /> {this.getString('uploading')}
              </div>
            )}
          </div>
        )}
      </div>
    );
  };
}

export default initComponent(SearchDishImagePopupContent);
