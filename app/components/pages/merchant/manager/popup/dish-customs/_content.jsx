import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { addCurrency, removeCommas } from 'utils/format';
import { updateDishCustom, updateDishCustomOption } from 'actions/merchant-manager';
import { getDish } from 'reducers/menu-manager';
import { MENU_CLONE_DISH_CUSTOM, MENU_UPDATE_DISH_CUSTOM, MENU_UPDATE_DISH_CUSTOM_OPTION, open } from 'actions/popup';
import Button from 'components/shared/button';
import { mergeWithOldData } from 'utils/merchant';
import PropTypes from 'prop-types';

// TODO: ADD ACTIVE TOGGLE LOGIC

class DishCustomsContent extends BaseComponent {
  static defaultProps = { dataGetter: getDish };
  static contextTypes = { isAdmin: PropTypes.bool };

  openUpdateDishCustomPopup = (custom, selectionType) => {
    typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(false);
    this.props.dispatch(
      open(MENU_UPDATE_DISH_CUSTOM, {
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        isCreate: !custom,
        selectionType,
        custom,
        dish: this.props.data,
        onClose: () => typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(true),
      }),
    );
  };

  openUpdateDishCustomOptionPopup = (customOption, custom) => {
    typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(false);
    this.props.dispatch(
      open(MENU_UPDATE_DISH_CUSTOM_OPTION, {
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        isCreate: !customOption,
        custom,
        customOption,
        dish: this.props.data,
        onClose: () => typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(true),
      }),
    );
  };

  // ACTIVATION

  handleActiveDishCustom = async (custom) => {
    if (!confirm(this.getString('are_you_sure_you_want_to_delete_0', '', [custom.get('name')]))) return;

    await new Promise((resolve) => this.props.dispatch(
      updateDishCustom({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.data.get('id'),
        customId: custom.get('id'),
        customData: custom.set('active', !custom.get('active')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    this.openCloneDishCustomPopup(custom, true);
  };

  handleActiveDishCustomOption = async (customOption, custom) => {
    if (
      customOption.get('active') &&
      !confirm(this.getString('are_you_sure_you_want_to_delete_0', '', [customOption.get('value')]))
    ) return;
    await new Promise((resolve) => this.props.dispatch(
      updateDishCustomOption({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.data.get('id'),
        customId: custom.get('id'),
        customOptionId: customOption.get('id'),
        customOptionData: customOption.set('active', !customOption.get('active')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    this.openCloneDishCustomOptionPopup(customOption, custom, true);
  };

  openCloneDishCustomPopup = (custom, isDelete) => {
    const customOptions = (custom.toJS().customOptions || []).map((c) => mergeWithOldData(
      {
        isDelete,
        id: c.id,
        active: isDelete ? false : c.active, // DELETE will send active: false. API will not send custom option inactive
        name: c.value,
        price: removeCommas(c.price),
        limitQuantity: c.limitQuantity,
        customName: custom.get('name'),
      },
      this.getParams().merchant,
    ));
    this.props.dispatch(
      open(MENU_CLONE_DISH_CUSTOM, {
        merchant: this.getParams().merchant,
        newData: mergeWithOldData(
          {
            type: 'custom',
            isDelete,
            id: custom.get('id'),
            active: isDelete ? false : custom.get('active'), // DELETE will send active: false. API will not send custom inactive
            name: custom.get('name'),
            selectionType: custom.get('selectionType'),
            limitQuantity: custom.get('limitQuantity'),
            note: custom.get('note'),
            customOptions,
          },
          this.getParams().merchant,
        ),
      }),
    );
  };

  openCloneDishCustomOptionPopup = (customOption, custom, isDelete) => {
    this.props.dispatch(
      open(MENU_CLONE_DISH_CUSTOM, {
        merchant: this.getParams().merchant,
        newData: mergeWithOldData(
          {
            type: 'customOption',
            isDelete,
            id: customOption.get('id'),
            active: isDelete ? false : customOption.get('active'), // DELETE will send active: false. API will not send custom option inactive
            name: customOption.get('value'),
            price: removeCommas(customOption.get('price')),
            limitQuantity: customOption.get('limitQuantity'),
            customName: custom.get('name'),
          },
          this.getParams().merchant,
        ),
      }),
    );
  };

  render() {
    return (
      <div className="content">
        <div className="form">
          <div className="dish-custom">
            <div className="section">
              <h3>
                {this.getString('order_adjustments') + ' '}
                <small>{this.getString('eg_sugar_levels_ice_levels_size')}</small>
              </h3>
              {this.renderCustomList('single_choice')}
            </div>
            <div className="section">
              <h3>
                {this.getString('multiple_picks') + ' '}
                <small>{this.getString('eg_toppings_extras')}</small>
              </h3>
              {this.renderCustomList('multi_choice')}
            </div>
          </div>
          <div className="form-action">
            <Button onClick={this.props.handleClose}>{this.getString('close')}</Button>
          </div>
        </div>
      </div>
    );
  }

  // CUSTOM

  renderCustomList(selectionType) {
    const { data } = this.props;
    return (
      <table className="menu-dish-custom-list">
        <thead>
          <tr>
            <th>{this.getString('option_info')}</th>
            <th />
          </tr>
        </thead>
        <tbody>
          <tr className="custom-item" key="custom.create">
            <td colSpan={2}>
              <a onClick={() => this.openUpdateDishCustomPopup(null, selectionType)}>
                <i className="fa fa-plus" />
                &nbsp; {this.getString('Thêm mục tùy chọn')} {selectionType === 'single_choice' ? 'đơn' : 'đa'}
              </a>
            </td>
          </tr>
          {data.get('customs') &&
            data
              .get('customs')
              .filter((c) => c.get('selectionType') === selectionType)
              .filter((c) => !!c.get('active'))
              .map((custom, key) => {
                return this.renderCustom(custom, key);
              })}
        </tbody>
      </table>
    );
  }

  renderCustom(custom, key) {
    const view = [
      <tr className="custom-item" key={key}>
        <td className="name">
          {custom.get('name')}
          {!!custom.get('note') && <span className="note">&nbsp;({custom.get('note')})</span>}
        </td>
        <td className="actions">
          {/* <a><i className="fa fa-clone"/></a> */}
          <a onClick={() => this.handleActiveDishCustom(custom)}>
            <i className="fa fa-trash" />
          </a>
          <a onClick={() => this.openCloneDishCustomPopup(custom)}>
            <i className="fa fa-sync" title={this.getString('Đồng bộ tùy chọn')} />
          </a>
          <a onClick={() => this.openUpdateDishCustomPopup(custom, custom.get('selectionType'))}>
            <i className="fas fa-pencil-alt" />
          </a>
        </td>
      </tr>,
      <tr className="custom-option-item" key={`${key}.option.create`}>
        <td colSpan={2}>
          <a className="create-custom-option-item" onClick={() => this.openUpdateDishCustomOptionPopup(null, custom)}>
            <i className="fa fa-plus" />
            &nbsp;{this.getString('additional')}
          </a>
        </td>
      </tr>,
    ];
    const customOptions = custom.get('customOptions');
    if (customOptions) {
      customOptions.forEach((co) => {
        const coView = (
          <tr className="custom-option-item" key={`${key}.${co.get('id')}`}>
            <td className="name">
              {co.get('value')}
              {co.get('price') > 0 && <span className="price">&nbsp;(+{addCurrency(co.get('price'))})</span>}
            </td>
            <td className="actions">
              <a onClick={() => this.handleActiveDishCustomOption(co, custom)}>
                <i className="fa fa-trash" />
              </a>
              <a
                onClick={() => this.openCloneDishCustomOptionPopup(co, custom)}
                title={this.getString('Đồng bộ tùy chọn')}
              >
                <i className="fa fa-sync" />
              </a>
              <a onClick={() => this.openUpdateDishCustomOptionPopup(co, custom)}>
                <i className="fas fa-pencil-alt" />
              </a>
            </td>
          </tr>
        );
        view.push(coView);
      });
    }

    return view;
  }
}

export default initComponent(DishCustomsContent);
