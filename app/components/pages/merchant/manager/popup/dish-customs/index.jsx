import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
// import CreateDishDetail from './_detail-create';
import { MENU_UPDATE_DISH_CUSTOMS, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import DishCustomsContent from './_content';

class UpdateMenuDishCustomsPopup extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      customs: [],
      couldEscape: true,
    };
  }

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_UPDATE_DISH_CUSTOMS },
  };

  close = () => {
    if (!this.state.couldEscape) return;
    this.props.dispatch(close(MENU_UPDATE_DISH_CUSTOMS));
  };

  render() {
    const params = this.props.data.get('params');
    return (
      <Popup className="menu-dish-customs" handleClose={this.close}>
        <div className="heading">
          <h3>{this.getString(`Quản lý mục tùy chọn cho "${params.dishName}"`)}</h3>
        </div>
        <DishCustomsContent
          params={{
            merchant: params.merchant,
            groupDishId: params.groupDishId,
            dishId: params.dishId,
          }}
          handleClose={this.close}
          toggleCouldEscape={this.toggleCouldEscape}
        />
      </Popup>
    );
  }

  toggleCouldEscape = (could) => {
    this.setState({ couldEscape: could });
  };
}

export default initComponent(UpdateMenuDishCustomsPopup);
