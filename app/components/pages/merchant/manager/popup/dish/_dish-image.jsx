import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import * as LoshipImage from 'utils/LoshipImage';
import { MENU_SEARCH_DISH_IMAGE, open } from 'actions/popup';
import { getImageUrl } from 'utils/image';
import Button from 'components/shared/button';
import FileUploader from 'components/statics/uploader';

class DishImage extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      imageThumbnail: props.preloadImage,
      imageThumbnailUrl: props.preloadImage,
      fetchingSelectedDish: false,
    };
  }

  static defaultProps = {
    customRender: true,
    isAllowedToCopyDishImage: false,
  };

  clearImage = () => {
    this.setState({
      imageThumbnail: null,
      imageThumbnailUrl: null,
      fetchingSelectedDish: false,
    });
    this.props.setImage(null);
  };

  handleFileUpload = (data, thumb) => {
    this.props.setImage(data);
    this.setState({ imageThumbnail: thumb });
  };

  handleSelectDish = async (item) => {
    this.setState(
      { fetchingSelectedDish: true },
      async () => {
        const { id, image: imageThumbnail } = item;
        let image = null;
        if (id) {
          image = await LoshipImage.getImageFromDish(id, imageThumbnail);
        } else {
          image = await LoshipImage.getImageFromUrl(getImageUrl(imageThumbnail, 1600));
        }
        this.props.setImage(image);
        if (image.hasDish()) {
          this.setState({
            imageThumbnail: null,
            imageThumbnailUrl: image.getDishImageUrl(),
            fetchingSelectedDish: false,
          });
        } else {
          image
            .getThumb()
            .then((data) => {
              this.setState({
                imageThumbnail: data,
                imageThumbnailUrl: null,
                fetchingSelectedDish: false,
              });
            })
            .catch((e) => {
              console.error(e);
              alert(this.getString('error_choosing_picture'));
              this.setState({ fetchingSelectedDish: false });
            });
        }
      },
    );
  };

  openSearchImagePopup = () => {
    typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(false);
    this.props.dispatch(
      open(MENU_SEARCH_DISH_IMAGE, {
        keyword: this.props.keyword,
        handleSelectDish: this.handleSelectDish,
        onClose: () => typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(true),
      }),
    );
  };

  renderThumbImage = ({ imageThumbnail }) => {
    return (
      <div className="dish-image-input">
        <div>
          <img src={imageThumbnail} alt="Thumbnail" />
        </div>
        <a className="btn-clear-dish-image" onClick={this.clearImage}>
          <i className="fa fa-times" />
        </a>
      </div>
    );
  };

  renderUpload = () => {
    // const isAllowedToCopyDishImage = this.props.isAllowedToCopyDishImage;
    // temporary hide search image button because system will break
    // const isAllowedToCopyDishImage = false;
    return (
      <div className="upload-wrapper">
        <div>
          <FileUploader className="btn-upload-wrapper" onChange={this.handleFileUpload} accept="image/*" type="dish">
            <Button type="feature">{this.getString('upload')}</Button>
          </FileUploader>

          {/* isAllowedToCopyDishImage && (
            <button
              disabled={keyword === ''}
              title={this.getString(keyword === '' ? 'Type in your favorite dishes to search' : '            ')}
              className="btn btn-search"
              onClick={this.openSearchImagePopup}
            >
              {this.getString('find_photos_on_loship')}
            </button>
          ) */}
        </div>
      </div>
    );
  };

  renderFetchingSelectedDish = () => {
    return (
      <span>
        {this.getString('processing')} <a onClick={this.clearImage}>{this.getString('HLzeei24a9')}</a>
      </span>
    );
  };

  render() {
    const { keyword } = this.props;
    const { imageThumbnail, imageThumbnailUrl, fetchingSelectedDish } = this.state;

    if (fetchingSelectedDish) return this.renderFetchingSelectedDish();

    if (imageThumbnail || imageThumbnailUrl) {
      return this.renderThumbImage({ imageThumbnail: imageThumbnailUrl || imageThumbnail });
    }

    return this.renderUpload({ keyword });
  }
}

export default initComponent(DishImage);
