import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { CONFIRM_POPUP, MENU_UPDATE_DISH, close, open } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import CreateDishDetail from './_detail-create';
import UpdateDishDetail from './_detail-update';

const MIN_DESCRIPTION_LENGTH_LOMART = 20;
class UpdateMenuDishPopup extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      customs: [],
      couldEscape: true,
    };
  }

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_UPDATE_DISH },
  };

  close = (checkEditing = false) => {
    if (!this.state.couldEscape) return;

    if (checkEditing === true && this.state.isEditing) {
      return this.props.dispatch(
        open(CONFIRM_POPUP, {
          title: this.getString('your_data_havent_save_yet'),
          content: this.getString('are_you_sure_you_want_to_leave_any_unsaved_changes_will_be_lost'),
          onConfirm: () => this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_UPDATE_DISH))),
        }),
      );
    }

    this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_UPDATE_DISH)));
  };

  render() {
    const params = this.props.data.get('params');
    const isCreate = params.isCreate;
    const { merchant, isAdmin } = this.props;
    return (
      <Popup className="menu-dish-update" handleClose={() => this.close(true)}>
        <div className="heading">
          <h3>{this.getString(isCreate ? 'Thêm món ăn' : 'Chỉnh sửa món ăn')}</h3>
        </div>
        {isCreate && (
          <CreateDishDetail
            params={{
              merchant: params.merchant,
              groupDishId: params.groupDishId,
              isExtraGroupDish: params.isExtraGroupDish,
            }}
            merchant={merchant}
            minLengthLomart={MIN_DESCRIPTION_LENGTH_LOMART}
            isAdmin={isAdmin}
            hasRoleContent={params.hasRoleContent}
            handleClose={this.close}
            toggleEditing={this.toggleEditing}
            toggleCouldEscape={this.toggleCouldEscape}
          />
        )}
        {!isCreate && (
          <UpdateDishDetail
            params={{
              merchant: params.merchant,
              groupDishId: params.groupDishId,
              isExtraGroupDish: params.isExtraGroupDish,
            }}
            hasRoleContent={params.hasRoleContent}
            merchant={merchant}
            minLengthLomart={MIN_DESCRIPTION_LENGTH_LOMART}
            isAdmin={isAdmin}
            data={params.dish}
            handleClose={this.close}
            toggleEditing={this.toggleEditing}
            toggleCouldEscape={this.toggleCouldEscape}
          />
        )}
      </Popup>
    );
  }

  toggleEditing = (isEditing = false) => {
    this.setState({ isEditing });
  };

  toggleCouldEscape = (could) => {
    this.setState({ couldEscape: could });
  };
}

export default initComponent(UpdateMenuDishPopup);
