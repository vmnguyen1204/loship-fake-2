import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { createDish } from 'actions/merchant-manager';
import { Map } from 'immutable';
import * as cookies from 'utils/shims/cookie';
import { addCommas, removeCommas, slugify, ucFirstAllWords } from 'utils/format';
import { MIN_MAIN_DISH_PRICE } from 'utils//menu-tool';
import Button from 'components/shared/button';
import UNITMENU from 'utils/json/unit_menu';
import DishImage from './_dish-image';
import PropTypes from 'prop-types';

let defaultData = new Map({
  name: '',
  price: '0',
  additionalFee: '0',
  additionalFeePercent: cookies.get('additionalFeePercent') || '10',
  description: '',
  image: '',
  active: false,
  unit: '',
  unitQuantity: 0,
  unitMenus: UNITMENU,
});

class CreateDishDetail extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = {
      data: defaultData,
      image: null,
      error: '',
    };
  }

  componentDidMount() {
    if (this.refs.txtDishName) this.refs.txtDishName.focus();
    window.addEventListener('keydown', this.handleKeyDown);
    document.addEventListener('click', this.handleClickOutside);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
    document.removeEventListener('click', this.handleClickOutside);
  }

  handleKeyDown = (e) => {
    if (this.state.noteFocused && !e.ctrlKey) return;
    if (e.key === 'Tab' && e.target.name === 'unit') {
      let { data } = this.state;
      data = data.set('showUnitMenus', false);
      this.setState({ data });
    }

    if (e.key === 'Enter' && !e.shiftKey) this.handleCreateDish();
  };

  handleClickOutside = (e) => {
    if (!this.node.contains(e.target)) {
      let { data } = this.state;
      data = data.set('showUnitMenus', false);
      this.setState({ data });
    }
  };

  createDish = async (payload) => {
    let dishData = payload;
    if (!this.props.hasRoleContent) {
      dishData = dishData.set('additionalFee', 0);
    }
    // Cast value to its real type, ex: remove commas in price
    dishData = dishData.set('price', parseInt(removeCommas(dishData.get('price'))));
    dishData = dishData.set('additionalFee', parseInt(removeCommas(dishData.get('additionalFee'))));
    dishData = dishData.set('additionalFeePercent', parseInt(removeCommas(dishData.get('additionalFeePercent'))));

    await new Promise((resolve) => this.props.dispatch(
      createDish({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishData,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    cookies.set('additionalFeePercent', this.state.data.get('additionalFeePercent'));
    defaultData = defaultData.set('additionalFeePercent', this.state.data.get('additionalFeePercent'));

    return this.props.handleClose();
  };

  handleCreateDish = async () => {
    const { data, image } = this.state;
    let dish = data;
    const { merchant, isAdmin, minLengthLomart } = this.props;
    const isExtraGroupDish = this.getParams().isExtraGroupDish;

    let errorMessage = '';

    const { name, price, additionalFee, description, unitQuantity, unit } = dish.toObject();

    if (!image) {
      errorMessage = 'Bạn chưa chọn ảnh món.';
    }

    if (!isExtraGroupDish && removeCommas(price) + removeCommas(additionalFee) < MIN_MAIN_DISH_PRICE) errorMessage = 'Món chính không được bán giá thấp hơn ' + addCommas(MIN_MAIN_DISH_PRICE) + 'đ';

    if (!price) errorMessage = 'Giá bán là bắt buộc nhập.';
    if (removeCommas(price) % 1000 !== 0 && !this.props.hasRoleContent) errorMessage = 'Giá bán cần được làm tròn đến phần nghìn. VD: 10.000, 15.000, 100.000,...';

    if (!name) errorMessage = 'Tên món là bắt buộc nhập.';

    if (merchant && merchant.getIn(['shippingService', 'name']) === 'lomart') {
      if (!isAdmin && description.length < minLengthLomart) {
        errorMessage = 'Ghi chú tối thiêu phải đạt 20 kí tự';
      }
    }

    if (unit) {
      if (unitQuantity === 0) errorMessage = 'Số lượng phải lớn hơn 0';
    } else {
      dish = dish.delete('unit');
      dish = dish.delete('unitQuantity');
    }

    if (errorMessage) return this.setState({ error: errorMessage });

    if (image && image.hasDish()) {
      return this.createDish(dish.set('copyImageFromDish', this.state.image.getDish()));
    } if (image && image.hasImage()) {
      return this.createDish(dish.set('image', await this.state.image.getURL()));
    }
    return this.createDish(dish);
  };

  handleInputChange = (field) => (e) => {
    const rawValue = e.target.value;
    let { data } = this.state;

    if (field === 'price' || field === 'additionalFee') {
      data = data.set(field, addCommas(removeCommas(rawValue)));
    } else {
      data = data.set(field, rawValue);
    }

    if (field === 'name') {
      data = data.set(field, ucFirstAllWords(rawValue));
    }

    if (field === 'price' || field === 'additionalFeePercent') {
      const price = parseInt(removeCommas(data.get('price')), 10);
      const additionalFeePercent = parseInt(removeCommas(data.get('additionalFeePercent')), 10) / 100;
      if (!isNaN(price) && !isNaN(additionalFeePercent)) {
        data = data.set(
          'additionalFee',
          addCommas((Math.ceil(((price * additionalFeePercent) / 1000).toFixed(3)) * 1000).toString()),
        );
      } else {
        data = data.set('additionalFee', '0');
      }
    }
    if (field === 'additionalFee') {
      const price = parseInt(removeCommas(data.get('price')), 10);
      const additionalFee = parseInt(removeCommas(data.get('additionalFee')), 10);
      if (!isNaN(price) && !isNaN(additionalFee)) {
        data = data.set('additionalFeePercent', addCommas(Math.ceil((additionalFee / price) * 100).toString()));
      } else {
        data = data.set('additionalFeePercent', '0');
      }
    }

    if (field === 'unit') {
      data = data.set('unit', rawValue.toLowerCase()).set('shouldFilterUnit', true);
    }

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(defaultData, data));
  };

  setImage = (image) => {
    this.setState({ image }, () => this.props.toggleEditing(this.isEditing(defaultData, this.state.data)));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      parseInt(srcData.get('price')) !== parseInt(removeCommas(data.get('price'))) ||
      parseInt(srcData.get('additionalFee')) !== parseInt(removeCommas(data.get('additionalFee'))) ||
      srcData.get('description') !== data.get('description') ||
      !!this.state.image
    );
  };

  handleShow = () => {
    let { data } = this.state;
    data = data.set('showUnitMenus', true).set('shouldFilterUnit', false);
    this.setState({ data }, () => this.refs.txtUnit.focus());
  };

  handleUnitMenu = (unit) => {
    let { data } = this.state;
    data = data.set('unit', unit.name);
    data = data.set('showUnitMenus', false);
    this.setState({ data });
  };

  filterData = (unit) => {
    const data = this.state.data;
    if (!data.get('shouldFilterUnit')) return true;

    const txtUnitName = this.refs.txtUnit ? this.refs.txtUnit.value.trim() : '';
    if (txtUnitName.length > 0) {
      const unitName = slugify(unit.name);
      const regex = new RegExp(slugify(data.get('unit')), 'gi');
      return unitName.search(regex) > -1;
    }
    return true;
  };

  render() {
    const data = this.state.data;
    const hasRoleContent = this.props.hasRoleContent;
    return (
      <div className="content">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <div className="form-group">
            <label>
              {this.getString('dish_name')} <span className="required">*</span>
            </label>
            <div className="input">
              <input ref="txtDishName" value={data.get('name')} onChange={this.handleInputChange('name')} />
            </div>
          </div>

          <div className="form-group" style={{ width: '50%' }}>
            <label>{this.getString('unit_name')}</label>
            <div
              className="input wrapper-unit-menu"
              ref={(node) => {
                this.node = node;
              }}
            >
              <input
                ref="txtUnit"
                name="unit"
                onFocus={this.handleShow}
                onChange={this.handleInputChange('unit')}
                value={data.get('unit')}
              />

              {data.get('showUnitMenus') && (
                <div className="unitMenus scroll-nice">
                  {data
                    .get('unitMenus')
                    .filter(this.filterData)
                    .map((unit) => (
                      <div key={unit.id} className="item-unit-menu" onClick={() => this.handleUnitMenu(unit)}>
                        {unit.name}
                      </div>
                    ))}
                </div>
              )}
            </div>
          </div>

          <div className="form-group" style={{ width: '50%' }}>
            <label style={{ paddingLeft: 24 }}>{this.getString('unit_quantity')}</label>
            <div className="input">
              <input value={data.get('unitQuantity')} onChange={this.handleInputChange('unitQuantity')} />
            </div>
          </div>

          <div className="form-group">
            <label>
              {this.getString('6vDaz_GcUI')} <span className="required">*</span>
            </label>
            <div className="input">
              <input value={data.get('price')} onChange={this.handleInputChange('price')} />
            </div>
          </div>
          {hasRoleContent && (
            <div className="form-group">
              <label>{this.getString('surge_price_percent')}</label>
              <div className="input">
                <input
                  type="number"
                  value={data.get('additionalFeePercent')}
                  onChange={this.handleInputChange('additionalFeePercent')}
                />
              </div>
            </div>
          )}
          {hasRoleContent && (
            <div className="form-group">
              <label>{this.getString('surge_price')}</label>
              <div className="input">
                <input value={data.get('additionalFee')} onChange={this.handleInputChange('additionalFee')} />
              </div>
            </div>
          )}

          <div className="form-group">
            <label>{this.getString('notes_if_any')}</label>
            <div className="input">
              <textarea
                rows="3"
                value={data.get('description')}
                onChange={this.handleInputChange('description')}
                onFocus={() => this.setState({ noteFocused: true })}
                onBlur={() => this.setState({ noteFocused: false })}
              />
            </div>
          </div>

          <div className="form-group">
            <label>
              {this.getString('picture')} <span className="required">*</span>
            </label>
            <div className="input">
              <DishImage
                image={this.state.image}
                setImage={this.setImage}
                keyword={data.get('name')}
                isAllowedToCopyDishImage={hasRoleContent}
                toggleCouldEscape={this.props.toggleCouldEscape}
              />
            </div>
          </div>
          {!!this.state.error && <div className="form-message text-right">{this.state.error}</div>}
          <div className="form-action">
            <Button onClick={this.props.handleClose}>{this.getString('abort')}</Button>
            <Button htmlType="submit" type="feature" onClick={this.handleCreateDish}>
              {this.getString('add_dish')}
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(CreateDishDetail);
