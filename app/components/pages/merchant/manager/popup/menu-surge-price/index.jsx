import { MENU_UPDATE_SURGE_PRICE, close } from 'actions/popup';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import initComponent from 'lib/initComponent';
import React from 'react';
import { get as dataGetter } from 'reducers/popup';
import UpdateMenuSurgePriceContent from './_content';

class UpdateMenuSurgePrice extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: MENU_UPDATE_SURGE_PRICE },
  };

  close = () => {
    this.props.dispatch(close(MENU_UPDATE_SURGE_PRICE));
  };

  render() {
    const params = this.props.data.get('params');
    return (
      <Popup className="menu-update-surge-price" handleClose={this.close}>
        <div className="heading">
          <h3>{this.getString('Điều chỉnh giá tăng thêm')}</h3>
        </div>
        <UpdateMenuSurgePriceContent
          handleClose={this.close}
          isCreate={params.isCreate}
          data={params.groups}
          params={{ merchant: params.merchant }}
        />
      </Popup>
    );
  }
}

export default initComponent(UpdateMenuSurgePrice);
