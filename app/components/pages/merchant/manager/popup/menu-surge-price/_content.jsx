import { addCommas, removeCommas } from 'utils/format';

import BaseComponent from 'components/base';
import Checkbox from 'components/shared/checkbox';
import { Map } from 'immutable';
import React from 'react';
import initComponent from 'lib/initComponent';
import { updateDish } from 'actions/merchant-manager';
import cx from 'classnames';
import { MIN_MAIN_DISH_PRICE } from 'utils//menu-tool';
import * as cookies from 'utils/shims/cookie';
import Button from 'components/shared/button';
import PropTypes from 'prop-types';

if (process.env.BROWSER) {
  require('assets/styles/components/_checkbox.scss');
}

const defaultData = new Map({
  surgePricePercent: cookies.get('surgePricePercent') || '10',
  surgePriceMinimum: cookies.get('surgePriceMinimum') || addCommas('10000'),
});

class UpdateMenuSurgePrice extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };
  static defaultProps = { customRender: true };

  constructor(props) {
    super(props);
    this.state = {
      data: defaultData,
      error: '',
      isNotSelected: {},
      isResolved: {},
    };
  }

  componentDidMount() {
    if (!this.refs.txtSurgePrice) return;
    this.refs.txtSurgePrice.focus();
    this.refs.txtSurgePrice.select();
  }

  handleUpdate = (e) => {
    e.preventDefault();
    const affectedDishes = this.getAffectedDishes();
    const isResolved = {};
    const { data } = this.state;

    const surgePriceMinimum = removeCommas(data.get('surgePriceMinimum'));
    const surgePricePercent = removeCommas(data.get('surgePricePercent')) / 100;

    cookies.set('surgePriceMinimum', surgePriceMinimum);
    cookies.set('surgePricePercent', surgePricePercent);

    this.setState({ isHandling: true, isResolved }, async () => {
      for (const dish of affectedDishes) {
        if (!this.state.isHandling) break;
        const surgePrice = Math.max(
          Math.ceil((dish.get('price') * surgePricePercent) / 1000) * 1000,
          surgePriceMinimum,
        );

        // Prevent update main dish price under minimum price
        if (!dish.get('isExtra') && dish.get('price') + surgePrice < MIN_MAIN_DISH_PRICE) {
          isResolved[dish.get('id')] = false;
          this.setState({ isResolved });
          continue;
        }

        await new Promise((resolve) => {
          setTimeout(() => {
            this.props.dispatch(
              updateDish({
                dishId: dish.get('id'),
                merchant: this.getParams().merchant,
                groupDishId: dish.get('groupDishId'),
                dishData: dish.set('additionalFee', surgePrice),
                callback: () => {
                  isResolved[dish.get('id')] = true;
                  this.setState({ isResolved });
                  resolve();
                },
                isAdmin: this.context.isAdmin,
              }),
            );
          }, 200);
        });
      }

      this.setState({ isHandling: false });
    });
  };

  handleCancel = () => {
    this.setState({ isHandling: false });
  };

  handleChange = (field, value) => {
    let { data } = this.state;

    if (field === 'surgePricePercent' || field === 'surgePriceMinimum') {
      if (isNaN(removeCommas(value))) return;
    }

    data = data.set(field, addCommas(removeCommas(value)));
    this.setState({ data });
  };

  getAffectedDishes = () => {
    const { data: groups } = this.props;

    return groups
      .map((group) => {
        return (
          group.get('dishes') &&
          group
            .get('dishes')
            .map((dish) => dish.set('groupDishId', group.get('id')).set('isExtra', group.get('isExtraGroupDish')))
        );
      })
      .filter((c) => !!c)
      .reduce((res, groupDishes) => res.concat(groupDishes.toArray()), [])
      .filter((dish) => !this.state.isNotSelected[dish.get('id')]);
  };

  getGroupCheckedLink = (group) => {
    const { isNotSelected } = this.state;
    const isCheckedAll = !group.get('dishes').some((dish) => isNotSelected[dish.get('id')]);

    return {
      checked: isCheckedAll,
      onChange: (checked) => {
        group.get('dishes').forEach((dish) => {
          isNotSelected[dish.get('id')] = !checked;
        });

        this.setState({ isNotSelected });
      },
    };
  };

  getAllCheckedLink = () => {
    const { isNotSelected } = this.state;
    const groups = this.props.data;
    const isCheckedAll = !groups.some((group) => {
      return group.get('dishes') && group.get('dishes').some((dish) => isNotSelected[dish.get('id')]);
    });

    return {
      checked: isCheckedAll,
      onChange: (checked) => {
        groups.forEach((group) => {
          group.get('dishes') &&
            group.get('dishes').forEach((dish) => {
              isNotSelected[dish.get('id')] = !checked;
            });
        });

        this.setState({ isNotSelected });
      },
    };
  };

  render() {
    const error = this.state.error;
    const { data, isNotSelected, isResolved, isHandling } = this.state;
    const { data: groups } = this.props;
    const isSingleGroup = groups && groups.size === 1;

    return (
      <div className="content">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <div className="form-group">
            <label>{this.getString('surge_price_percent')}</label>
            <div className="input">
              <input
                ref="txtSurgePrice"
                value={data.get('surgePricePercent')}
                onChange={(e) => this.handleChange('surgePricePercent', e.target.value)}
                readOnly={this.state.isHandling}
              />
            </div>
          </div>

          <div className="form-group">
            <label>{this.getString('surge_price_minimum')}</label>
            <div className="input">
              <input
                value={data.get('surgePriceMinimum')}
                onChange={(e) => this.handleChange('surgePriceMinimum', e.target.value)}
                readOnly={this.state.isHandling}
              />
            </div>
          </div>

          <div className="group-list">
            <label>{this.getString('list_of_affected_dish')}</label>
            <br />
            {!isSingleGroup && (
              <div className="group-item">
                <div className="group-item-header all">
                  <Checkbox className="reverse" {...this.getAllCheckedLink()}>
                    {this.getString('select_all_dishes')}
                  </Checkbox>
                </div>
              </div>
            )}
            {groups.map((group) => (group.get('dishes') ? (
              <div key={group.get('id')} className="group-item">
                <div className="group-item-header">
                  <Checkbox className="reverse" {...this.getGroupCheckedLink(group)}>
                    {group.get('name')}
                  </Checkbox>
                </div>
                <div className="group-item-dishes">
                  {group.get('dishes').map((dish) => {
                    const surgePriceMinimum = removeCommas(data.get('surgePriceMinimum'));
                    const surgePricePercent = removeCommas(data.get('surgePricePercent')) / 100;

                    const surgePrice = Math.max(
                      Math.ceil((dish.get('price') * surgePricePercent) / 1000) * 1000,
                      surgePriceMinimum,
                    );

                    const isNotAcceptedPrice = !group.get('isExtraGroupDish') && dish.get('price') + surgePrice < MIN_MAIN_DISH_PRICE;

                    return (
                      <div key={`${group.get('id')}-${dish.get('id')}`} className="group-item-dish">
                        <div className="name">
                          <Checkbox
                            className="reverse"
                            checked={!isNotSelected[dish.get('id')]}
                            onChange={() => {
                              isNotSelected[dish.get('id')] = !isNotSelected[dish.get('id')];
                              this.setState({ isNotSelected });
                            }}
                          >
                            {dish.get('name')}
                          </Checkbox>

                          {!isNotSelected[dish.get('id')] ? (
                            <span className="pull-right">
                              <span
                                className={cx('price', { isNotSelected: isNotSelected[dish.get('id')] })}
                              >
                                {addCommas(dish.get('price'))}
                              </span>
                              <span>
                                <i className="fas fa-long-arrow-alt-right" />
                                <span className={`price-new${isNotAcceptedPrice ? '-error' : ''}`}>
                                  {addCommas(dish.get('price') + surgePrice)}
                                </span>

                                {isHandling &&
                                    !isNotSelected[dish.get('id')] &&
                                    isResolved[dish.get('id')] === undefined && (
                                  <i className="status fas fa-spinner fa-spin" />
                                )}
                                {isResolved[dish.get('id')] === true && <i className="status fas fa-check" />}
                                {isResolved[dish.get('id')] === false && <i className="status fas fa-times" />}
                              </span>
                            </span>
                          ) : (
                            <span className="price pull-right">{addCommas(dish.get('price'))}</span>
                          )}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            ) : null))}
          </div>

          {!!error && <div className="form-message">{error}</div>}
          <div className="form-action">
            {!this.state.isHandling && <Button onClick={this.props.handleClose}>{this.getString('close')}</Button>}
            {!this.state.isHandling && (
              <Button htmlType="submit" type="feature" onClick={this.handleUpdate}>
                {this.getString('save')}
              </Button>
            )}
            {this.state.isHandling && <Button onClick={this.handleCancel}>{this.getString('abort')}</Button>}
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(UpdateMenuSurgePrice);
