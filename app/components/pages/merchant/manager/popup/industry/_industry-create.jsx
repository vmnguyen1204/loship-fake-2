import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import cx from 'classnames';
import { createMenuIndustry } from 'actions/merchant-manager';
import { Map } from 'immutable';

import AuthByRole from 'components/shared/button/auth-by-role';
import { LINK_DOMAIN_LOZI, ROLES_CONTENT } from 'settings/variables';
import moment from 'utils/shims/moment';
import Button from 'components/shared/button';
import { withRouter } from 'react-router-dom';
import IndustryImage from './_industry-image';
import PropTypes from 'prop-types';

if (process.env.BROWSER) {
  require('assets/styles/components/_checkbox.scss');
}

const ORDER_INDUSTRY = [...Array(10)].map((_, index) => index + 1);

const defaultData = new Map({
  name: '',
  active: false,
  image: '',
  order: 1,
});

class IndustryCreateContent extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: defaultData,
      error: '',
    };
  }

  static defaultProps = { customRender: true };
  static contextTypes = { isAdmin: PropTypes.bool };

  isValidIndustry = () => {
    const { data } = this.state;
    let isValid = true;
    let error = this.state;
    const name = data.get('name');
    const image = data.get('image');

    if (!image) {
      error = 'Bắt buộc chọn hình ảnh';
      isValid = false;
    }

    if (!name) {
      error = 'Bắt buộc nhập tên ngành';
      isValid = false;
    }

    if (!isValid) {
      this.setState({
        error,
      });
      return false;
    }
    return true;
  }

  handleCreate = async () => {
    const isValidIndustry = this.isValidIndustry();

    if (!isValidIndustry) return;
    const { data } = this.state;
    const image = data.get('image');
    const imageURL = await image.getURL();
    await new Promise((resolve) => this.props.dispatch(
      createMenuIndustry({
        merchant: this.getParams().merchant,
        industryData: new Map({
          name: data.get('name'),
          active: data.get('active'),
          isPublished: data.get('active'),
          order: data.get('order'),
          image: imageURL,
        }),
        callback: resolve,
        isAdmin: this.context.isAdmin,
        refetchMenu: false,
      }),
    ));
    this.props.handleClose();
  };

  handleChange = (field, value) => {
    let { data } = this.state;
    data = data.set(field, value);
    if (field === 'name') data = data.set(field, value.toUpperCase());
    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultData, data));
  };

  handleCheckboxChange = (field) => {
    let { data } = this.state;
    data = data.set(field, !data.get(field));

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultData, data));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      srcData.get('isExtraGroupDish') !== data.get('isExtraGroupDish') ||
      srcData.get('isShownSupplyItemsForUser') !== data.get('isShownSupplyItemsForUser')
    );
  };

  toggleEditorInfo = () => {
    this.setState((state) => ({ ...state, showDetail: !state.showDetail }));
  };

  renderEditorInfo = () => {
    const { data } = this.state;
    const { createdAt, createdBy, updatedAt, updatedBy } = (data && data.toObject()) || {};

    // VALIDATE INPUT DATA
    if (!createdBy || !updatedBy) return null;

    return (
      <div className="history">
        <span>
          {this.getString('created_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + createdBy.username}>
            @{createdBy.username}
          </a>
          {this.getString('at')}
          {moment(createdAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <br />
        <span>
          {this.getString('updated_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + updatedBy.username}>
            @{updatedBy.username}
          </a>
          {this.getString('at')}
          {moment(updatedAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <AuthByRole key="delete" roles={ROLES_CONTENT}>
          <div className="delete-group" style={{ marginTop: 3 }}>
            <a onClick={this.handleDeleteGroupDish}>
              <i className="fas fa-trash" /> {this.getString('delete')}
            </a>
          </div>
        </AuthByRole>
      </div>
    );
  };

  render() {
    const { data, error } = this.state;
    return (
      <div className="content">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <div className="form-group">
            <label>
              {this.getString('Tên ngành')}<span className="required">*</span>
            </label>
            <div className="input">
              <input ref="txtName" value={data.get('name')} onChange={(e) => this.handleChange('name', e.target.value)} />
            </div>
          </div>
          <div key="active" className="form-group">
            <label>{this.getString('is_available', 'menuManage')}</label>
            <div className="input">
              <div>
                <a
                  title={this.getString('press_to_0_this_dish', '', [!data.get('active') ? 'bật' : 'tắt'])}
                  onClick={() => this.handleCheckboxChange('active')}
                >
                  <i
                    className={cx('availability-status', { active: data.get('active') })}
                  />
                </a>
              </div>
            </div>
          </div>
          <div key="order" className="form-group">
            <label>{this.getString('Vị trí')}</label>
            <div className="input">
              <div>
                <select
                  name="order"
                  id="select-order"
                  className="order-industry"
                  value={data.get('order')}
                  onChange={(e) => this.handleChange('order', parseInt(e.target.value))}
                >
                  {ORDER_INDUSTRY.map((order) => (
                    <option key={order} value={order}>
                      {order}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
          <div key="image" className="form-group">
            <label>
              {this.getString('picture')} <span className="required">*</span>
            </label>
            <div className="input">
              <IndustryImage
                image={data.get('image')}
                setImage={(image) => this.handleChange('image', image)}
              />
            </div>
          </div>
          {!!error && <div className="form-message">{error}</div>}
          <div className="form-action">
            <Button onClick={this.props.handleClose}>{this.getString('abort')}</Button>
            <Button htmlType="submit" type="feature" onClick={this.handleCreate}>
              {this.getString('add')}
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(IndustryCreateContent, withRouter);
