import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { CONFIRM_POPUP, MENU_INDUSTRY, close, open } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import IndustryCreateContent from './_industry-create';
import IndustryUpdateContent from './_industry-update';

class UpdateMenuIndustryPopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: MENU_INDUSTRY },
  };

  close = (checkEditing = false) => {
    if (checkEditing === true && this.state.isEditing) {
      return this.props.dispatch(
        open(CONFIRM_POPUP, {
          title: this.getString('your_data_havent_save_yet'),
          content: this.getString('are_you_sure_you_want_to_leave_any_unsaved_changes_will_be_lost'),
          onConfirm: () => this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_INDUSTRY))),
        }),
      );
    }

    this.setState({ isEditing: false }, () => this.props.dispatch(close(MENU_INDUSTRY)));
  };

  render() {
    const params = this.props.data.get('params');
    const { isCreate } = params;
    return (
      <Popup className="menu-dish-group" handleClose={() => this.close(true)}>
        <div className="heading">
          <h3>{this.getString(params.isCreate ? 'Thêm ngành menu' : 'Chỉnh sửa ngành menu')}</h3>
        </div>
        {isCreate && <IndustryCreateContent
          handleClose={this.close}
          isCreate={params.isCreate}
          params={{ merchant: params.merchant }}
          toggleEditing={this.toggleEditing}
        />}
        {!isCreate && <IndustryUpdateContent
          handleClose={this.close}
          data={params.industry}
          params={{ merchant: params.merchant }}
          toggleEditing={this.toggleEditing}
        />}
      </Popup>
    );
  }

  toggleEditing = (isEditing = false) => {
    this.setState({ isEditing });
  };
}

export default initComponent(UpdateMenuIndustryPopup);
