import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import cx from 'classnames';
import { deleteMenuIndustry, updateMenuIndustry } from 'actions/merchant-manager';
import { Map } from 'immutable';

import AuthByRole from 'components/shared/button/auth-by-role';
import { LINK_DOMAIN_LOZI, ROLES_CONTENT } from 'settings/variables';
import moment from 'utils/shims/moment';
import Button from 'components/shared/button';
import { CONFIRM_POPUP, open } from 'actions/popup';
import { withRouter } from 'react-router-dom';
import IndustryImage from './_industry-image';
import PropTypes from 'prop-types';

if (process.env.BROWSER) {
  require('assets/styles/components/_checkbox.scss');
}

const ORDER_INDUSTRY = [...Array(10)].map((_, index) => index + 1);

const defaultData = new Map({
  name: '',
  active: false,
  image: '',
  order: 1,
});

const mapPropsToState = (props) => {
  if (!props.data) return { data: defaultData };

  const {
    name = '',
    isActive,
    image,
    id,
    order = 1,
    createdAt,
    createdBy,
    updatedAt,
    updatedBy,
  } = props.data.toJS();
  return { data: new Map({ id, name, image, isActive, order, createdAt, createdBy, updatedAt, updatedBy }) };
};

class IndustryUpdateContent extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      ...mapPropsToState(props),
      error: '',
      image: null,
      isChangeImage: false,
    };
  }

  static defaultProps = { customRender: true };
  static contextTypes = { isAdmin: PropTypes.bool };

  componentDidMount() {
    if (this.props.data) this.setState(mapPropsToState(this.props));
  }

  isValidIndustry = () => {
    const { data, image, isChangeImage } = this.state;
    let isValid = true;
    let error = this.state;
    const name = data.get('name');
    const imagePrev = data.get('image');

    if ((isChangeImage && !image) || (!isChangeImage && !imagePrev)) {
      error = 'Bắt buộc chọn ảnh';
      isValid = false;
    }

    if (!name) {
      error = 'Bắt buộc nhập tên ngành';
      isValid = false;
    }

    if (!isValid) {
      this.setState({
        error,
      });
      return false;
    }
    return true;
  }

  handleUpdate = async (e) => {
    e.preventDefault();
    const isValidIndustry = this.isValidIndustry();

    if (!isValidIndustry) return;
    const { data, image, isChangeImage } = this.state;
    const content = {
      id: data.get('id'),
      name: data.get('name'),
      active: !!data.get('isActive'),
      isPublished: !!data.get('isActive'),
      order: data.get('order'),
    };

    if (isChangeImage && image) {
      content.image = await image.getURL();
    }

    await new Promise((resolve) => this.props.dispatch(
      updateMenuIndustry({
        merchant: this.getParams().merchant,
        callback: resolve,
        isAdmin: this.context.isAdmin,
        content,
        id: data.get('id'),
      }),
    ));
    this.props.handleClose();
  };

  handleDeleteIndustry = () => {
    this.props.dispatch(
      open(CONFIRM_POPUP, {
        content: () => {
          return (
            <div className="text-center">
              <p>
                {this.getString('are_you_sure_you_want_to_delete')}
                <b>{this.state.data.get('name')} ?</b>
              </p>
            </div>
          );
        },
        confirmText: this.getString('delete'),
        onConfirm: async () => {
          await new Promise((resolve) => {
            const { data } = this.state;
            this.props.dispatch(
              deleteMenuIndustry({
                merchant: this.getParams().merchant,
                id: data.get('id'),
                callback: resolve,
                isAdmin: this.context.isAdmin,
                refetchMenu: false,
              }),
            );
          });
        },
      }),
      this.props.dispatch(this.props.handleClose),
    );
  };

  handleDelete = async () => {
    const data = this.props.data;
    if (data.get('dishes').size > 0) {
      return this.setState({
        error: this.getString(
          `Chỉ có thể xóa danh mục "${data.get('name')}" khi đã di chuyển toàn bộ món ăn sang danh mục khác.`,
        ),
      });
    }
    // need handle
  };

  handleChange = (field, value) => {
    let { data } = this.state;
    data = data.set(field, value);
    if (field === 'name') data = data.set(field, value.toUpperCase());
    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultData, data));
  };

  handleCheckboxChange = (field) => {
    let { data } = this.state;
    data = data.set(field, !data.get(field));

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultData, data));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      srcData.get('isExtraGroupDish') !== data.get('isExtraGroupDish') ||
      srcData.get('isShownSupplyItemsForUser') !== data.get('isShownSupplyItemsForUser')

    );
  };

  toggleEditorInfo = () => {
    this.setState((state) => ({ ...state, showDetail: !state.showDetail }));
  };

  renderEditorInfo = () => {
    const { data } = this.state;
    const { createdAt, createdBy, updatedAt, updatedBy } = (data && data.toObject()) || {};

    return (
      <div className="history">
        {createdBy && updatedBy && (
          <div>
            <span>
              {this.getString('created_by')}{' '}
              <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + createdBy.username}>
                @{createdBy.username}
              </a>
              {this.getString('at')}
              {moment(createdAt).format('HH:mm, DD-MM-YYYY')}
            </span>
            <br />
            <span>
              {this.getString('updated_by')}{' '}
              <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + updatedBy.username}>
                @{updatedBy.username}
              </a>
              {this.getString('at')}
              {moment(updatedAt).format('HH:mm, DD-MM-YYYY')}
            </span>
          </div>
        )}

        <AuthByRole key="delete" roles={ROLES_CONTENT}>
          <div className="delete-group" style={{ marginTop: 3 }}>
            <a onClick={this.handleDeleteIndustry}>
              <i className="fas fa-trash" /> {this.getString('delete')}
            </a>
          </div>
        </AuthByRole>
      </div>
    );
  };

  render() {
    const { data, image, error } = this.state;
    return (
      <div className="content">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <div className="form-group">
            <label>
              {this.getString('Tên ngành')}<span className="required">*</span>
            </label>
            <div className="input">
              <input ref="txtName" value={data.get('name')} onChange={(e) => this.handleChange('name', e.target.value)} />
            </div>
          </div>
          <div key="active" className="form-group">
            <label>{this.getString('is_available', 'menuManage')}</label>
            <div className="input">
              <div>
                <a
                  title={this.getString('press_to_0_this_dish', '', [!data.get('isActive') ? 'bật' : 'tắt'])}
                  onClick={() => this.handleCheckboxChange('isActive')}
                >
                  <i
                    className={cx('availability-status', { active: data.get('isActive') })}
                  />
                </a>
              </div>
            </div>
          </div>
          <div key="order" className="form-group">
            <label>{this.getString('Vị trí')}</label>
            <div className="input">
              <div>
                <select
                  name="order"
                  id="select-order"
                  className="order-industry"
                  value={data.get('order')}
                  onChange={(e) => this.handleChange('order', parseInt(e.target.value))}
                >
                  {ORDER_INDUSTRY.map((order) => (
                    <option key={order} value={order}>
                      {order}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
          <div key="image" className="form-group">
            <label>
              {this.getString('picture')} <span className="required">*</span>
            </label>
            <div className="input">
              <IndustryImage
                image={image}
                setImage={(img) => this.setState({ image: img, isChangeImage: true })}
                preloadImage={data.get('image')}
              />
            </div>
          </div>
          {!!error && <div className="form-message">{error}</div>}
          <div className="form-action">
            <AuthByRole roles={ROLES_CONTENT}>{this.renderEditorInfo()}</AuthByRole>
            <Button onClick={this.props.handleClose}>{this.getString('abort')}</Button>
            <Button htmlType="submit" type="feature" onClick={this.handleUpdate}>
              {this.getString('save')}
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(IndustryUpdateContent, withRouter);
