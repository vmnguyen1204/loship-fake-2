import { createDishCustomOption, updateDishCustomOption } from 'actions/merchant-manager';
import cx from 'classnames';
import BaseComponent from 'components/base';
import Button from 'components/shared/button';
import AuthByRole from 'components/shared/button/auth-by-role';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import React from 'react';
import { LINK_DOMAIN_LOZI, ROLES_CONTENT } from 'settings/variables';
import { addCommas, removeCommas } from 'utils/format';
import moment from 'utils/shims/moment';
import PropTypes from 'prop-types';

const defaultDataObject = new Map({
  value: '',
  price: 0,
  limitQuantity: 1,
  active: false,
});

const mapPropsToState = (props) => {
  if (!props.data || props.isCreate) return { data: defaultDataObject.set('customId', props.custom.get('id')) };

  const { id, active, value, price, limitQuantity, createdBy, createdAt, updatedBy, updatedAt } = props.data.toJS();
  return {
    data: new Map({
      id,
      value,
      oldValue: value,
      price: addCommas(price),
      limitQuantity,
      active,
      createdBy,
      createdAt,
      updatedBy,
      updatedAt,
    }),
  };
};

class DishCustomOptionContent extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = { data: new Map(defaultDataObject) };
  }

  componentDidMount() {
    this.setState(mapPropsToState(this.props));

    if (this.refs.txtCustomOptionName) this.refs.txtCustomOptionName.focus();
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (e) => {
    const isCreate = this.props.isCreate;
    if (e.key === 'Enter' && !e.shiftKey) isCreate ? this.handleCreateDishCustomOption(e) : this.handleUpdateDishCustomOption(e);
  };

  handleClose = () => {
    return this.props.handleClose();
  };

  handleCreateDishCustomOption = async () => {
    let { data } = this.state;
    data = data.set('price', removeCommas(data.get('price')));

    await new Promise((resolve) => this.props.dispatch(
      createDishCustomOption({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.dish.get('id'),
        customId: this.props.custom.get('id'),
        customOptionData: data,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    return this.handleClose();
  };

  handleUpdateDishCustomOption = async (e) => {
    let { data } = this.state;
    data = data.set('price', removeCommas(data.get('price')));

    e.preventDefault();
    await new Promise((resolve) => this.props.dispatch(
      updateDishCustomOption({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.dish.get('id'),
        customId: this.props.custom.get('id'),
        customOptionId: data.get('id'),
        customOptionData: data,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    return this.handleClose();
  };

  handleInputChange = (field, e) => {
    let { data } = this.state;

    if (field === 'price') {
      data = data.set(field, addCommas(removeCommas(e.target.value)));
    } else {
      data = data.set(field, e.target.value);
    }

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultDataObject, data));
  };

  handleCheckboxChange = (newValue) => {
    let { data } = this.state;
    data = data.set('active', newValue);

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultDataObject, data));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('value') !== data.get('value') ||
      srcData.get('price') !== data.get('price') ||
      srcData.get('limitQuantity') !== data.get('limitQuantity')
    );
  };

  toggleEditorInfo = () => {
    this.setState((state) => ({ ...state, showDetail: !state.showDetail }));
  };

  renderEditorInfo = () => {
    const { data } = this.state;
    const { createdAt, createdBy, updatedAt, updatedBy } = (data && data.toObject()) || {};

    // VALIDATE INPUT DATA
    if (!createdBy || !updatedBy) return null;

    return (
      <div className="history">
        <span>
          {this.getString('created_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + createdBy.username}>
            @{createdBy.username}
          </a>
          {this.getString('at')}
          {moment(createdAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <br />
        <span>
          {this.getString('updated_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + updatedBy.username}>
            @{updatedBy.username}
          </a>
          {this.getString('at')}
          {moment(updatedAt).format('HH:mm, DD-MM-YYYY')}
        </span>
      </div>
    );
  };

  render() {
    const data = this.state.data;
    const isCreate = this.props.isCreate;
    const isSingleChoice = this.props.custom.get('selectionType') === 'single_choice';

    return (
      <div className="content">
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          {!isCreate && (
            <div className="form-group">
              <label>{this.getString('available')}</label>
              <div className="input">
                <div>
                  <a
                    title={this.getString('press_to_0_this_dish', '', [!data.get('active') ? 'bật' : 'tắt'])}
                    onClick={() => this.handleCheckboxChange(!data.get('active'))}
                  >
                    <i
                      className={cx('availability-status', { active: data.get('active') })}
                    />
                  </a>
                </div>
                <div className="input-note">
                  {this.getString('if_on_0_will_appear_on_the_menu_for_the_customers_to_order', '', [
                    data.get('value'),
                  ])}
                </div>
              </div>
            </div>
          )}
          <div className="form-group">
            <label>
              {this.getString('option_name')} <span className="required">*</span>
            </label>
            <div className="input">
              <input
                ref="txtCustomOptionName"
                value={data.get('value')}
                onChange={(e) => this.handleInputChange('value', e)}
              />
            </div>
          </div>
          <div className="form-group">
            <label>{this.getString('6vDaz_GcUI')}</label>
            <div className="input">
              <input value={data.get('price')} onChange={(e) => this.handleInputChange('price', e)} />
            </div>
          </div>
          {!isSingleChoice && (
            <div className="form-group">
              <label>
                {this.getString('ylARvvkqdz')} <span className="required">*</span>
              </label>
              <div className="input">
                <input
                  type="number"
                  min={1}
                  max={10}
                  value={data.get('limitQuantity')}
                  onChange={(e) => this.handleInputChange('limitQuantity', e)}
                />
                <div className="input-note">{this.getString('maximum_choices_for_customers')}</div>
              </div>
            </div>
          )}

          <div className="form-action">
            {!isCreate && <AuthByRole roles={ROLES_CONTENT}>{this.renderEditorInfo()}</AuthByRole>}

            <Button onClick={this.props.handleClose}>{this.getString('abort')}</Button>
            {!isCreate && (
              <Button type="feature" onClick={this.handleUpdateDishCustomOption}>
                {this.getString('save')}
              </Button>
            )}
            {isCreate && (
              <Button type="feature" onClick={this.handleCreateDishCustomOption}>
                {this.getString('add')}
              </Button>
            )}
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(DishCustomOptionContent);
