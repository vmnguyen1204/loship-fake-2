import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { MENU_WAREHOUSING_DISH, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import Button from 'components/shared/button';
import PropTypes from 'prop-types';
import { dishStock, API as merchantAPI } from 'actions/merchant-manager';
import { addCommas, removeCommas, slugify } from 'utils/format';
import callAPI from 'actions/helpers';

class MenuWareHousingPopup extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool, isMobile: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = {
      amount: '',
      note: '',
      err: '',
      vendor: '',
      price: '',
      currentPage: 1,
      isLoading: false,
    };
  }

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_WAREHOUSING_DISH },
  };

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.data && this.props.data) {
      this.fetchVendors();
    }
  }

  fetchVendors = async () => {
    const { currentPage } = this.state;
    const res = await callAPI('get', {
      url: merchantAPI.FETCH_VENDOR,
      query: { page: currentPage },
    });

    if (res.status !== 200) {
      const err = <span className='error'>{this.getString('error_when_fetch_vendor')}</span>;
      this.setState({ err });
      return;
    }

    const { data, pagination } = res.body;
    const { limit, page, total } = pagination;
    const vendorList = this.state.vendorList || [];
    this.setState({
      vendorList: [...vendorList, ...data],
      currentPage: page + 1,
    });

    const totalPages = Math.ceil(total / limit);
    if (currentPage < totalPages) {
      this.fetchVendors();
    }
  }

  close = (selectedVendor = false) => {
    this.setState(({
      amount: '',
      note: '',
      err: '',
      vendor: '',
      vendorSelected: undefined,
      price: '',
      isLoading: false,
    }), () => {
      this.props.dispatch(close(MENU_WAREHOUSING_DISH));
      if (!selectedVendor) {
        this.setState({
          vendorList: undefined,
          currentPage: 1,
        }, () => this.fetchVendors());
      }
    });
  }

  handleOnChange = (e) => {
    const { name, value } = e.target;
    const state = this.state;
    let newInput = value;

    if (name === 'amount') newInput = value.replace(/\D/g, '');

    if (name === 'price') {
      newInput = addCommas(removeCommas(value));
    }

    state[name] = newInput;

    if (name === 'vendor') {
      state.shouldFilterMenu = true;
      state.vendorSelected = undefined;
      state.vendor = newInput.toUpperCase();
    }

    this.setState({ ...state });
  }

  checkValidInput = () => {
    const { amount, price } = this.state;
    let isValid = true;
    let { err } = this.state;
    if (!price) {
      err = <span className='error'>Bắt buộc nhập tiền nhập hàng !</span>;
      isValid = false;
    }
    if (parseInt(amount) < 1) {
      err = <span className='error'>Số lượng phải lớn hơn 0 !</span>;
      isValid = false;
    }
    if (parseInt(amount) > 100000) {
      err = <span className='error'>Số lượng phải nhỏ hơn 100.000 !</span>;
      isValid = false;
    }
    if (amount === '') {
      err = <span className='error'>Bắt buộc nhập giá trị !</span>;
      isValid = false;
    }
    if (!isValid) {
      this.setState({ err });
      return false;
    }

    return true;
  }

  handleSubmit = () => {
    const { isAdmin } = this.context;
    const { amount, note, price, vendorSelected, vendor } = this.state;
    const params = this.props.data.get('params') || {};
    const isValid = this.checkValidInput();
    let dishVendorName = vendor;
    let dishVendorId = null;
    if (vendorSelected) {
      dishVendorName = vendorSelected.name;
      dishVendorId = vendorSelected.id;
    }
    if (!isValid) return;
    this.setState({
      isLoading: true,
    }, () => {
      this.props.dispatch(dishStock({
        dishId: params.dishId,
        amount: parseInt(amount),
        note,
        isAdmin,
        merchant: params.merchant,
        groupDishId: params.groupDishId,
        price: removeCommas(price),
        dishVendorName,
        dishVendorId,
        callback: () => this.close(!!vendorSelected),
      }));
    });
  }

  handleShowVendorMenu = () => {
    this.setState({ showVendorMenu: true, shouldFilterMenu: false });
  }

  chooseSupplier = (vendor) => {
    this.setState({
      showVendorMenu: false,
      vendor: vendor.name,
      vendorSelected: { ...vendor },
    });
  }

  handleClickOutside = (e) => {
    if (!this.nodeVendor || !this.nodeVendor.contains(e.target)) {
      this.setState({ showVendorMenu: false });
    }
  }

  filterVendorMenu = (vendor) => {
    if (!this.state.shouldFilterMenu) return true;
    const txtVendor = this.refs.txtVendor ? this.refs.txtVendor.value.trim() : '';
    if (txtVendor.length) {
      const vendorName = slugify(vendor.name);
      const regex = new RegExp(slugify(this.state.vendor), 'gi');
      return vendorName.search(regex) > -1;
    }
    return true;
  }

  render() {
    const params = this.props.data.get('params') || {};
    const { showVendorMenu, vendor, price, vendorList, amount, isLoading } = this.state;

    return (
      <Popup className="menu-warehouse-popup" handleClose={() => this.close(true)}>
        <div className="heading">
          <h3>{this.getString('Nhập kho sản phẩm')}</h3>
        </div>

        <div className='content'>
          <form className='form'>
            <div className='form-group'>
              <label>{this.getString('Mã sản phẩm')}</label>
              <div className='dish-id'>
                {params.dishId}
              </div>
            </div>
            <div className='form-group'>
              <label>{this.getString('Tên sản phẩm')}</label>
              <div className='dish-name'>
                {params.dishName}
              </div>
            </div>
            <div className='form-group'>
              <label>{this.getString('Tồn kho hiện tại')}</label>
              <div className='dish-stock'>
                {params.stockCount}
              </div>
            </div>
            <div className="form-group">
              <label>{this.getString('Số lượng nhập kho')}</label>
              <div className="input">
                <input
                  type="text"
                  value={amount}
                  name="amount"
                  onChange={this.handleOnChange}
                  autoComplete="off"
                />
              </div>
            </div>
            <div className="form-group">
              <label>{this.getString('Nhà cung cấp')}</label>
              <div
                className="input wrapper-supplier-menu"
                ref={(nodeVendor) => {
                  this.nodeVendor = nodeVendor;
                }}
              >
                <input
                  ref="txtVendor"
                  type="text"
                  name="vendor"
                  value={vendor}
                  onFocus={this.handleShowVendorMenu}
                  onChange={this.handleOnChange}
                  autoComplete="off"
                />
                {showVendorMenu && vendorList && (
                  <div className="suppliers-menu scroll-nice">
                    {vendorList
                      .filter(this.filterVendorMenu)
                      .map((ved) => {
                        return (
                          <div key={ved.id} className="item-supplier-menu" onClick={() => this.chooseSupplier(ved)}>
                            {ved.name}
                          </div>
                        );
                      })}
                  </div>
                )}
              </div>
            </div>
            <div className="form-group">
              <label>{this.getString('Tiền nhập hàng')}</label>
              <div className="input">
                <input
                  type="text"
                  name="price"
                  onChange={this.handleOnChange}
                  value={price ? price : '0'}
                  autoComplete="off"
                />
              </div>
            </div>
            <div className="form-group">
              <label>{this.getString('Ghi chú nhập kho')}</label>
              <div className="input">
                <textarea
                  name="note"
                  onChange={this.handleOnChange}
                />
              </div>
            </div>
          </form>
        </div>

        <div className="form-action">
          {this.state.err ? this.state.err : ''}
          <Button htmlType="submit" type="feature" disabled={isLoading} onClick={this.handleSubmit}>
            {this.getString(isLoading ? 'Đang nhập kho...' : 'Xác nhận nhập kho')}
          </Button>
        </div>
      </Popup>
    );
  }
}

export default initComponent(MenuWareHousingPopup);
