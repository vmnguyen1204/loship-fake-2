import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { MENU_REMOVE_DISH, close } from 'actions/popup';
import Button from 'components/shared/button';
import { get as dataGetter } from 'reducers/popup';
import { deleteDish } from 'actions/merchant-manager';
import PropTypes from 'prop-types';

class RemoveDishPopup extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_REMOVE_DISH },
  };

  close = () => {
    this.props.dispatch(close(MENU_REMOVE_DISH));
  };

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (e) => {
    // Prevent call when popup close
    if (!this.props.data || !this.props.data.get('params')) return;
    if (e.key === 'Enter' && !e.shiftKey) this.handleDelete(e);
  };

  handleDelete = (e) => {
    const params = this.props.data.get('params');
    if (!params.merchant || !params.groupDishId || !params.dish) return;
    e.preventDefault();
    this.props.dispatch(
      deleteDish({
        merchant: params.merchant,
        groupDishId: params.groupDishId,
        dishId: params.dish.get('id'),
        isAdmin: this.context.isAdmin,
      }),
    );
    this.close();
  };

  render() {
    const params = this.props.data.get('params');
    return (
      <Popup className="popup-custom" handleClose={this.close}>
        <div className="content">
          <p>
            {this.getString('are_you_sure_you_want_to_delete')}
            <b>&nbsp;{params.dish.get('name')} ?</b>
          </p>
        </div>

        <div className="popup-footer">
          <Button type="feature" onClick={this.handleDelete}>
            {this.getString('delete')}
          </Button>
        </div>
      </Popup>
    );
  }
}

export default initComponent(RemoveDishPopup);
