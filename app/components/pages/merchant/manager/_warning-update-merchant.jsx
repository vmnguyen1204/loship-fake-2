import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import Button from 'components/shared/button';

class WarningMerchant extends BaseComponent {
  render() {
    const { merchantSlug } = this.props;
    return (
      <div className="warning-zone">
        <h2>Bạn cần cập nhật một số thông tin cho nhà hàng để tiếp tục tạo menu</h2>

        <div className="wrap-description">
          <div className="description-item">
            <p className="txt">Nhập địa chỉ -&gt; Xác định vị trí -&gt; Thay đổi</p>
            <img src="/dist/images/update-lat-long.png" alt="lat-long-desc" />
            <Button type="primary" href={`https://lozi.vn/b/${merchantSlug}/edit`} target="_blank">
              Cập nhật toạ độ cửa hàng
            </Button>
          </div>
          <div className="description-item">
            <p className="txt">Chọn icon -&gt; up hình -&gt; Lưu thay đồi</p>
            <img src="/dist/images/change-avatar.png" alt="lat-long-desc" />
            <Button type="primary" href={`https://lozi.vn/b/${merchantSlug}`} target="_blank">
              Cập nhật avatar cửa hàng
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(WarningMerchant);
