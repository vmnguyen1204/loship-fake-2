import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import {
  activeMenuIndustry,
  fetchMerchantMenu,
  fetchMoreMerchantMenu,
  updateDish,
  updateMenuGroup,
  updateShownSupplyItem,
} from 'actions/merchant-manager';
import { getMerchantMenu, getMerchantMenuLosupply, getMerchantMenuUser } from 'reducers/menu-manager';
import { getCurrentUser } from 'reducers/user';
import {
  CUSTOM_CONTENT,
  MENU_CLONE_DISH,
  MENU_INDUSTRY,
  MENU_REMOVE_DISH,
  MENU_STOCK_DISH_HISTORY,
  MENU_UPDATE_DISH,
  MENU_UPDATE_DISH_CUSTOMS,
  MENU_UPDATE_FRANCHISED,
  MENU_UPDATE_GROUP,
  MENU_UPDATE_SURGE_PRICE,
  MENU_WAREHOUSING_DISH,
  open,
} from 'actions/popup';
import cx from 'classnames';
import { hasRole } from 'utils/access';
import { ROLES_CONTENT } from 'settings/variables';
import AuthByRole from 'components/shared/button/auth-by-role';
import { addCommas, removeCommas } from 'utils/format';
import { MIN_MAIN_DISH_PRICE, isGroupDishHasInvalidDish } from 'utils//menu-tool';
import ScrollBoundary from 'components/shared/scroll-boundary';
import { genCode, genShortId } from 'utils/data';
import Hint from 'components/statics/hint';
import FileUploader from 'components/statics/uploader';
import ManagerDishGroup from './_dish-group';
import EmptyMenu from './_menu-empty';
import PropTypes from 'prop-types';
import Button from 'components/shared/button';
import Spinner from 'components/statics/spinner';
import DishImage from './_dish-image';

function generateSrcMenu(fileContent, originalMenu = [], callback) {
  // Line 0 for title
  const dishes = fileContent.split('\n').slice(1);

  const menu = [];
  for (const dish of dishes) {
    try {
      let [groupDishName, name, price, image, additionalFee, description] = dish.split(',');
      const regex = /^['"][\w\W]+['"]$/g;

      if (regex.test(groupDishName)) groupDishName = groupDishName.slice(1, -1);
      if (regex.test(name)) name = name.slice(1, -1);
      if (regex.test(price)) price = price.slice(1, -1);
      if (regex.test(image)) image = image.slice(1, -1);
      if (regex.test(additionalFee)) additionalFee = additionalFee.slice(1, -1);
      if (regex.test(description)) description = description.slice(1, -1);

      if (Number.isNaN(price)) continue;
      if (!/https?:\/\//.test(image)) continue;
      price = parseInt(price);
      description = description.split('::').join('\n');

      const dishData = {
        id: genShortId(),
        active: true,
        name,
        price,
        additionalFee,
        description,
        image: '/external/images/' + encodeURIComponent(encodeURI(image)),
        customs: [],
      };

      let group = menu.find((g) => g.name === groupDishName);
      if (group) {
        group.dishes = (group.dishes || []).concat(dishData);
        continue;
      }

      group = originalMenu.find((g) => g.name === groupDishName);
      if (group) {
        menu.push({
          id: group.id,
          active: group.active,
          isPinned: group.isPinned,
          name: group.name,
          isExtraGroupDish: group.isExtraGroupDish,
          dishes: [dishData],
        });
        continue;
      }

      menu.push({
        id: genShortId(),
        active: true,
        isPinned: false,
        name: groupDishName,
        isExtraGroupDish: false,
        dishes: [dishData],
      });
    } catch (e) {
      console.error('ERROR: ', e, dish);
      continue;
    }
  }

  if (typeof callback === 'function') callback(menu);
  return menu;
}

class MerchantManagerMenu extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  static defaultProps = {
    actionCreator: fetchMerchantMenu,
    dataGetter: (state, params) => {
      let data = getMerchantMenuUser(state, params);
      if (params.isMenuLosupply) {
        data = getMerchantMenuLosupply(state, params);
      }
      const group = getMerchantMenu(state, params);
      const groupData = group?.get('data')?.get('group') || [];
      if (!data) return null;
      data = data.set('groupData', groupData);
      const currentUser = getCurrentUser(state);
      if (currentUser) {
        data = data.set('hasRoleContent', hasRole(currentUser.get('data'), ROLES_CONTENT));
      }
      return data;
    },
    customRender: true,
  };

  constructor(props) {
    super(props);
    this.state = { executeCode: '' };
  }

  goToMenuContent = (contentKey) => {
    const menuContent = document.getElementById(contentKey);
    menuContent.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  openCreateIndustryPopup = () => {
    this.props.dispatch(
      open(MENU_INDUSTRY, {
        merchant: this.getParams().merchant,
        isCreate: true,
      }),
    );
  };
  openUpdateIndustryPopup = (industry) => {
    this.props.dispatch(
      open(MENU_INDUSTRY, {
        merchant: this.getParams().merchant,
        industry,
      }),
    );
  }

  openCreateGroupPopup = (industry) => {
    this.props.dispatch(
      open(MENU_UPDATE_GROUP, {
        merchant: this.getParams().merchant,
        isCreate: true,
        industry,
      }),
    );
  };
  openUpdateGroupPopup = (group, industry) => {
    this.props.dispatch(
      open(MENU_UPDATE_GROUP, {
        merchant: this.getParams().merchant,
        group,
        industry,
      }),
    );
  };

  openCreateDishPopup = (group) => {
    this.props.dispatch(
      open(MENU_UPDATE_DISH, {
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        isExtraGroupDish: group.get('isExtraGroupDish'),
        isCreate: true,
        hasRoleContent: this.props.hasRoleContent,
      }),
    );
  };

  openUpdateDishPopup = (group, dish) => {
    this.props.dispatch(
      open(MENU_UPDATE_DISH, {
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        isExtraGroupDish: group.get('isExtraGroupDish'),
        hasRoleContent: this.props.hasRoleContent,
        dish,
      }),
    );
  };

  openCloneDishPopup = (group, dish) => {
    this.props.dispatch(
      open(MENU_CLONE_DISH, {
        merchant: this.getParams().merchant,
        merchantId: this.props.merchant.get('id'),
        groupDishId: group.get('id'),
        isExtraGroupDish: group.get('isExtraGroupDish'),
        groups: this.props.groupData,
        dish,
        hasRoleContent: this.props.hasRoleContent,
      }),
    );
  };

  openUpdateDishCustomPopup = (group, dish) => {
    this.props.dispatch(
      open(MENU_UPDATE_DISH_CUSTOMS, {
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        dishId: dish.get('id'),
        dishName: dish.get('name'),
      }),
    );
  };

  openUpdateSurgePricePopup = (group) => {
    const { groupData } = this.props;
    const groups = group ? [group] : (groupData || []);

    this.props.dispatch(
      open(MENU_UPDATE_SURGE_PRICE, {
        merchant: this.getParams().merchant,
        groups,
      }),
    );
  };

  openUpdateFranchised = () => {
    const { groupData } = this.props;

    this.props.dispatch(
      open(MENU_UPDATE_FRANCHISED, {
        merchant: this.getParams().merchant,
        groups: groupData?.toJS() || [],
      }),
    );
  };

  openWareHousingDish = (group, dishId, dishName, stockCount) => {
    this.props.dispatch(
      open(MENU_WAREHOUSING_DISH, {
        dishId,
        dishName,
        stockCount,
        merchant: this.props.merchant.toJS(),
        groupDishId: group.get('id'),
      }),
    );
  }

  openStockHistoryPopup = (dishId, dishName, lastStockAddedAt) => {
    this.props.dispatch(
      open(MENU_STOCK_DISH_HISTORY, {
        dishId,
        dishName,
        lastStockAddedAt,
        merchant: this.props.merchant.toJS(),
      }),
    );
  }

  handleFileUpload = async (e) => {
    const groupData = this.props.groupData?.toJS() || [];

    const input = e.target;
    if (!input.files || input.files.length === 0) {
      console.error('ERROR: invalid or file not found.');
      return null;
    }

    try {
      const reader = new FileReader();
      reader.onload = (loadEvent) => generateSrcMenu(loadEvent.target.result, groupData, (srcMenu) => {
        const CODE_LEN = 16;
        const code = genCode(CODE_LEN);

        const lastCode = localStorage.getItem('lastMenuSynchronizationCode');
        localStorage.removeItem(lastCode);
        localStorage.setItem(code, JSON.stringify(srcMenu));
        localStorage.setItem('lastMenuSynchronizationCode', code);

        this.props.dispatch(
          open(MENU_UPDATE_FRANCHISED, {
            merchant: this.getParams().merchant,
            groups: groupData,
            code,
            behavior: 'fetch_image',
          }),
        );

        input.value = '';
      });
      reader.onerror = (error) => {
        throw error;
      };
      reader.readAsText(input.files[0]);
    } catch (error) {
      console.error('ERROR', error.message);
    }
  };

  // ACTIVATION

  handleActiveIndustry = async (industry) => {
    const content = {
      active: !industry.get('isActive'),
      isPublished: !industry.get('isActive'),
    };
    await new Promise((resolve) => this.props.dispatch(
      activeMenuIndustry({
        merchant: this.getParams().merchant,
        id: industry.get('id'),
        content,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
  }

  handleActiveGroup = async (group) => {
    await new Promise((resolve) => this.props.dispatch(
      updateMenuGroup({
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        groupData: group.set('active', !group.get('active')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    // do something
  };
  handleActiveShownSupplyItems = async (group) => {
    const merchant = this.getParams().merchant;
    updateShownSupplyItem({
      merchant,
      groupDishId: group.get('id'),
      groupData: group.set('isShownSupplyItemsForUser', !group.get('isShownSupplyItemsForUser')),
      isAdmin: this.context.isAdmin,
      callback: () => {
        this.props.dispatch({
          type: 'MCM_UPDATE_SHOWN_LOSUPPLY',
          groupDishId: group.get('id'),
          key: merchant,
          data: { isShownSupplyItemsForUser: !group.get('isShownSupplyItemsForUser') },
        });
      },
    });
  };
  handleActivePinned = async (group) => {
    await new Promise((resolve) => this.props.dispatch(
      updateMenuGroup({
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        groupData: group.set('isPinned', !group.get('isPinned')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    // do something
  };
  renderButtonLoadMoreGroupDish = (extras) => {
    const { isLoading } = this.state;
    const message = isLoading ? (
      <Spinner type="flow">
        <span className="ml-8">{this.getString('Đang tải')}</span>
      </Spinner>
    ) : this.getString('view_more');

    return (
      <div className="load-more">
        <Button className={cx('btn-loadmore-group', { loading: isLoading })} disabled={isLoading} fw onClick={() => this.handleLoadMoreGroupDish(extras)}>
          {message}
        </Button>
      </div>
    );
  }
  handleActiveDish = async (group, dish) => {
    const dishImage = dish.get('image');
    if (!dish.get('active') && !dishImage) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('cannot_activate_this_item', 'menuManage'),
          content: () => {
            return (
              <div className="text-left">
                <p>
                  <b>{dish.get('name')}</b>
                  {this.getString('doesnt_have_a_photo_please_pick_one_for_it_before_activating', 'menuManage')}
                </p>
                <p>
                  {this.getString(
                    'an_item_on_the_menu_can_only_be_activated_when_it_has_name_price_and_photo',
                    'menuManage',
                  )}
                </p>
              </div>
            );
          },
        }),
      );
    }
    const isExtraGroupDish = group.get('isExtraGroupDish');
    const price = dish.get('price');
    const additionalFee = dish.get('additionalFee');
    if (
      !dish.get('active') &&
      !isExtraGroupDish &&
      removeCommas(price) + removeCommas(additionalFee) < MIN_MAIN_DISH_PRICE
    ) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('cannot_activate_this_item', 'menuManage'),
          content: () => {
            return (
              <div className="text-left">
                <p>
                  <b>{dish.get('name')}</b>
                  {this.getString(' đang được bán với giá thấp hơn mức giá tối thiểu.', 'menuManage')}
                </p>
                <p>
                  {this.getString('Món chính trên menu phải có giá bán từ {0}đ trở lên.', 'menuManage', [
                    addCommas(MIN_MAIN_DISH_PRICE),
                  ])}
                </p>
              </div>
            );
          },
        }),
      );
    }
    await new Promise((resolve) => this.props.dispatch(
      updateDish({
        dishId: dish.get('id'),
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        dishData: dish.set('active', !dish.get('active')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    // do something
  };

  toggleExtraGroupDish = async (group) => {
    const isExtraGroupDish = group.get('isExtraGroupDish');
    const invalidDish = isGroupDishHasInvalidDish(group.get('dishes'), MIN_MAIN_DISH_PRICE);
    // A bit different from other conditions:
    // At this time, isExtraGroupDish == true means it is being updated to false, so we need to stop the update if an invalid dish is found
    if (isExtraGroupDish && invalidDish !== null) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('Không thể đổi thành menu món chính', 'menuManage'),
          content: () => {
            return (
              <div className="text-left">
                <p>
                  <b>{invalidDish.name}</b>
                  {this.getString(' trên menu này đang bán với giá thấp hơn mức tối thiểu.', 'menuManage')}
                </p>
                <p>
                  {this.getString('Món chính trên menu phải có giá bán từ {0}đ trở lên.', 'menuManage', [
                    addCommas(MIN_MAIN_DISH_PRICE),
                  ])}
                </p>
                <p>
                  {this.getString(
                    'Anh chị vui lòng cập nhật giá món trước khi đổi thành menu món chính.',
                    'menuManage',
                  )}
                </p>
              </div>
            );
          },
        }),
      );
    }

    await new Promise((resolve) => this.props.dispatch(
      updateMenuGroup({
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        groupData: group.set('isExtraGroupDish', !group.get('isExtraGroupDish')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
  };

  // DELETE

  handleDeleteDish = (group, dish) => {
    this.props.dispatch(
      open(MENU_REMOVE_DISH, {
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        dish,
      }),
    );
  };

  render() {
    const { categories, data, merchant, originDomain } = this.props;
    const { isMenuLosupply } = this.getParams();
    if (!data) return <div className="mc-menu" />;
    if (categories?.length === 0) {
      return (
        <div className="submenu-empty">
          <h2>
            {this.getString('0_still_hasnt_created_categories_yet', '', [merchant.get('name')])}
          </h2>
          <p>
            {this.getString('had_to_create_categories_before_create_menu')}
          </p>
          <p>
            {this.getString('click_to_button_create_categories_above')}
            <a onClick={() => this.props.openCategoriesPopup()}>
              {this.getString('click_here_lower_case')}
            </a>
          </p>
        </div>
      );
    }
    if (data.size === 0) {
      return (
        <EmptyMenu
          merchant={this.props.merchant}
          createFirst={isMenuLosupply ? this.openCreateIndustryPopup : this.openCreateGroupPopup}
          isMenuLosupply={isMenuLosupply}
        />
      );
    }
    return (
      <div className="mcm-menu">
        <ScrollBoundary
          marginTop={16}
          marginBottom={0}
          fixedMarginTop={10}
          fixedMarginBottom={10}
          className={cx('mcm-submenu-heading-list')}
        >
          <div>
            {isMenuLosupply && (
              <div className="btn-create-industry">
                <a key="create" className="btn-create-group" onClick={this.openCreateIndustryPopup}>
                  <i className="lz lz-plus" />
                  {this.getString('create_new_menu_industry')}
                </a>
              </div>
            )}
            {this.renderGroup(data)}
          </div>
        </ScrollBoundary>
        <div className="mcm-submenu-content-list">
          <AuthByRole roles={ROLES_CONTENT}>
            <div className="admin-tool">
              <a
                onClick={() => {
                  this.openUpdateSurgePricePopup();
                }}
              >
                <i className="fas fa-angle-double-up" /> Giá tăng thêm toàn menu
              </a>

              {!isMenuLosupply && (
                <a name="dish" onClick={this.openUpdateFranchised}>
                  <i className="far fa-copy" /> Đồng bộ menu
                </a>
              )}

              {!isMenuLosupply && (
                <FileUploader className="d-inline-block" onFileChange={this.handleFileUpload} accept=".csv">
                  <i className="fas fa-upload" /> Tạo menu từ file
                  <Hint
                    text={() => (
                      <div className="hint-csv">
                        Tool hiện tại chỉ support đồng bộ menu từ file thỏa mãn các yêu cầu sau:
                        <ol>
                          <li>Định dạng *.csv</li>
                          <li>
                            Cấu trúc file quy ước sẵn: Dòng 1 thể hiện title, các dòng còn lại mỗi dòng biểu thị 1 món cần
                            đồng bộ
                          </li>
                          <li>Thứ tự các cột: group_dishes_name,name,price,image,additional_fee,description</li>
                          <li>
                            Với cột description, hệ thống có hỗ trợ format nhiều dòng. Khi cần xuống dòng, gõ &apos;::&apos;. Ví dụ:
                            &quot;xyz là món nổi bật::Được nhiều người tin dùng::Ghé shop ngay hôm nay để đặt nhé.&quot;
                          </li>
                          <li>
                            File mẫu:{' '}
                            <a href="/dist/sample.csv" download>
                              sample.csv
                            </a>
                          </li>
                        </ol>
                      </div>
                    )}
                  />
                </FileUploader>
              )}
            </div>
          </AuthByRole>

          {data.map((industryGroup, index) => {
            const industry = industryGroup.get('industry');
            const extras = industryGroup.get('extras');
            return (
              <div key={industry?.get('id')} className={cx('industry-content', { ['separate-industry-content'] : index !== 0 })}>
                {industry && (
                  <h3 id={`industryContent${industry.get('id')}`} className="industry-name">
                    <a
                      className="availability-toggle"
                      title={this.getString('press_to_0_this_dish', '', [!industry.get('isActive') ? 'bật' : 'tắt'])}
                      onClick={() => this.handleActiveIndustry(industry)}
                    >
                      <i
                        className={cx('availability-status', { active: industry.get('isActive') })}
                      />
                    </a>
                    {industry.get('name')}
                    <a
                      className="edit-group"
                      onClick={() => this.openUpdateIndustryPopup(industry)}
                    >
                      <i className="fas fa-pencil-alt" /> {this.getString('Sửa')}
                    </a>
                  </h3>
                )}
                {industryGroup.get('groups') &&
                  industryGroup.get('groups').map((item, key) => {
                    return (
                      <ManagerDishGroup
                        data={item}
                        key={item.get('id')}
                        index={key}
                        industry={industryGroup.get('industry')}
                        getString={this.getString}
                        openUpdateGroupPopup={this.openUpdateGroupPopup}
                        openUpdateSurgePricePopup={this.openUpdateSurgePricePopup}
                        handleActiveGroup={this.handleActiveGroup}
                        toggleExtraGroupDish={this.toggleExtraGroupDish}
                        openCreateDishPopup={this.openCreateDishPopup}
                        handleActiveDish={this.handleActiveDish}
                        handleDeleteDish={this.handleDeleteDish}
                        hasRoleContent={this.props.hasRoleContent}
                        openCloneDishPopup={this.openCloneDishPopup}
                        openUpdateDishCustomPopup={this.openUpdateDishCustomPopup}
                        openUpdateDishPopup={this.openUpdateDishPopup}
                        handleActivePinned={this.handleActivePinned}
                        openWareHousingDish={this.openWareHousingDish}
                        openStockHistoryPopup={this.openStockHistoryPopup}
                        originDomain={originDomain}
                        handleActiveShownSupplyItem={this.handleActiveShownSupplyItems}
                        merchantSlug={merchant.get('slug')}
                        dispatch={this.props.dispatch}
                      />
                    );
                  })}
                {extras?.get('nextUrl') && this.renderButtonLoadMoreGroupDish(extras?.get('nextUrl'))}
              </div>
            );
          })}
          <a href={originDomain}>
            <i className="fa fa-angle-left" />
            &nbsp;
            {this.getString('return_to_0s_homepage', 'menuManage', [merchant.get('name')])}
          </a>
        </div>
      </div>
    );
  }

  renderGroup(data) {
    return data.map((industryGroup) => {
      const industry = industryGroup.get('industry');
      const extras = industryGroup.get('extras');
      return (
        <div key={industry?.get('id')} className="industry-group">
          {industry && (
            <div className="industry-name">
              <DishImage image={industry.get('image')} />
              <div className="name" onClick={() => this.goToMenuContent('industryContent' + industry.get('id'))}>
                {industry.get('name')}
              </div>
              <i
                className="fas fa-pencil-alt"
                onClick={() => this.openUpdateIndustryPopup(industry)}
              />
            </div>
          )}
          <div className={cx({ ['pl-40']: industry })}>
            <a key="create" className="btn-create-group" onClick={() => this.openCreateGroupPopup(industry)}>
              <i className="lz lz-plus" />
              {this.getString('create_new_menu_category')}
            </a>
            {industryGroup.get('groups') &&
              industryGroup.get('groups').map((item) => {
                return (
                  <div key={item.get('id')} className="wrapper">
                    <a
                      className="availability-toggle"
                      title={this.getString('press_to_0_this_dish', '', [!item.get('active') ? 'bật' : 'tắt'])}
                      onClick={(e) => {
                        e.stopPropagation();
                        this.handleActiveGroup(item);
                      }}
                    >
                      <i
                        className={cx('availability-status', { active: item.get('active') })}
                      />
                    </a>
                    <a className="dish-group-name" onClick={() => this.goToMenuContent('groupContent' + item.get('id'))}>
                      <AuthByRole roles={ROLES_CONTENT}>
                        <i
                          className={cx('fa fa-star', { isPinned: item.get('isPinned') })}
                          onClick={(e) => {
                            e.stopPropagation();
                            this.handleActivePinned(item);
                          }}
                        />
                      </AuthByRole>
                      <div className="group-name">
                        {item.get('name')} {item.get('isExtraGroupDish') && <small>({this.getString('phụ')})</small>}
                      </div>
                    </a>
                    <i
                      className="fas fa-pencil-alt"
                      onClick={(e) => {
                        e.stopPropagation();
                        this.openUpdateGroupPopup(item, industry);
                      }}
                    />
                  </div>
                );
              })}
          </div>
          {extras?.get('nextUrl') && this.renderButtonLoadMoreGroupDish(extras?.get('nextUrl'))}
        </div>
      );
    });
  }
  handleLoadMoreGroupDish = async (nextUrl) => {
    if (!nextUrl) return;
    this.setState({ isLoading: true }, () => {
      this.props.dispatch(fetchMoreMerchantMenu({
        merchant: this.props.merchant.get('slug'),
        nextUrl, isAdmin: this.context.isAdmin,
        callback: () => {
          this.setState({ isLoading: false });
        },
        callbackFailure: () => {
          this.setState({ isLoading: false });
          this.props.dispatch(
            open(CUSTOM_CONTENT, {
              content: this.getString('fail_to_load_more_menu_pleasy_try_again'),
            }),
          );
        },
      }));
    });
  }
}

export default initComponent(MerchantManagerMenu);
