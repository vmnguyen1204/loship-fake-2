import React from 'react';
import initComponent from 'lib/initComponent';
import { Link, withRouter } from 'react-router-dom';
import {
  checkCanEditMenu,
  fetchDishInfo,
} from 'actions/merchant-manager';
import { Map } from 'immutable';
import { getCurrentUser } from 'reducers/user';
import { getDetailDishLosupply } from 'reducers/menu-manager';
import { addCommas, addCurrency } from 'utils/format';
import BasePage from 'components/pages/base';
import { hasRole } from 'utils/access';
import { ROLES_ADMIN } from 'settings/variables';
import ListStockHisory from './popup/dish-stock-history/_list-stock_history';
import { getImageUrl } from 'utils/image';
import { Helmet } from 'react-helmet';

if (process.env.BROWSER) {
  require('assets/styles/pages/losupply-product.scss');
  require('assets/styles/pages/gateway.scss');
}

class LosupplyDishItem extends BasePage {

  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();
      const detailLosupply = getDetailDishLosupply(state, params);
      if (detailLosupply) {
        data = data.set('dishesData', detailLosupply);
      }
      const currentUser = getCurrentUser(state);
      if (currentUser) {
        data = data.set('currentUser', currentUser);
      }
      return data;
    },
    customRender: true,
  };

  componentDidMount() {
    const { params } = this.props;
    if (!params) return;
    const { dishId } = params;
    const { username, slug } = this.props.merchant.toJS();

    if (dishId) {
      this.props.dispatch(
        fetchDishInfo({
          dishId, slug,
          isAdmin: this.isAdmin(),
        }),
      );
      checkCanEditMenu({
        username,
        callback: () => {
          this.setState({ canEditMenu: true });
        },
        isAdmin: this.isAdmin(),
      });
    }
  }

  isAdmin = () => {
    const { currentUser } = this.props;
    if (currentUser && currentUser.get('data') && hasRole(currentUser.get('data'), ROLES_ADMIN)) return true;
    return false;
  };

  render() {
    if (!this.state.canEditMenu) {
      return (
        <div>
          <div className="container">
            <div className="gateway-err">
              <div className="alone-message">
                <h3>{this.getString('request_for_a_view_product_detail')}</h3>
                <p>{this.getString('sorry_you_are_not_authorized_to_access_this_page')}</p>
                <p>{this.getString('thank_you_for_choosing_loship')}</p>
              </div>
            </div>
          </div>
        </div>
      );
    }
    const { dishesData, params, merchant } = this.props;
    const dishes = dishesData && dishesData.getIn(['data', 'group']).toJS();

    if (!dishes) return null;
    const { name, description, image, price, unit, unitQuantity, wallet } = dishes;

    const { id } = merchant.toJS();

    const { dishId, eatery } = params;

    return (
      <div className="dish-info">
        <Helmet title={this.getString(`#${dishId} | ${name} - ${unitQuantity} ${unit?.toLowerCase()}`)} />
        <div className="navigation">
          <div className="container">
            <ul className="bread-crumb">
              <li className="item">
                <Link to={'/'}>Trang chủ</Link>
              </li>
              <li className="item">
                <Link to={`/${eatery}/menu`}>Danh sách menu</Link>
              </li>
              <li className="item">
                <Link to={`/${eatery}/menu/dishes/${dishId}`}>{name}</Link>
              </li>
            </ul>
          </div>
        </div>
        <div className="container">
          <div className="dish">
            <img
              className="dish-image"
              src={getImageUrl(image, 480, undefined, undefined, '/dist/images/dish-placeholder.png')}
            />
            <div className="dish-content">
              <div className="name">{name}</div>
              <div className="description">{description}</div>
              <div className="price">
                {addCurrency(price) + '/' + unitQuantity + ' ' + unit?.toLowerCase()}
              </div>
              <div className="stock">Tồn kho hiện tại: <b>{addCommas(wallet?.stockCount)}</b></div>
            </div>
          </div>
          <div className="history-stock">
            <div className="table-title">Lịch sử nhập hàng</div>
            <ListStockHisory
              merchant={{ id }}
              params={{ dishId }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(LosupplyDishItem, withRouter);
