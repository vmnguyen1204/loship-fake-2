import React, { PureComponent } from 'react';

import AuthByRole from 'components/shared/button/auth-by-role';
import Hint from 'components/statics/hint';
import PropTypes from 'prop-types';
import { LIST_MERCHANT_LOSUPPLY, ROLES_CONTENT } from 'settings/variables';
import cx from 'classnames';
import Button from 'components/shared/button';
import DishItem from './_dish-item';
import { getRoute } from 'utils/routing';
import { fetchMoreDish } from 'actions/merchant-manager';
import Spinner from 'components/statics/spinner';
import { CUSTOM_CONTENT, open } from 'actions/popup';

class ManagerDishGroup extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = { isLoading: false };
  }

  renderDish = (group, data, key) => {
    const { match } = (typeof window !== 'undefined' && getRoute(null, window.location.pathname)) || {};
    const { merchantUsername, merchant } = match.params;

    const eatery = merchantUsername ? merchantUsername : `b/${merchant}`;
    return (
      <DishItem
        data={data}
        key={key}
        index={key}
        group={group}
        eatery={eatery}
        getString={this.props.getString}
        handleActiveDish={this.props.handleActiveDish}
        hasRoleContent={this.props.hasRoleContent}
        openCloneDishPopup={this.props.openCloneDishPopup}
        openUpdateDishCustomPopup={this.props.openUpdateDishCustomPopup}
        openUpdateDishPopup={this.props.openUpdateDishPopup}
        handleDeleteDish={this.props.handleDeleteDish}
        openWareHousingDish={this.props.openWareHousingDish}
        openStockHistoryPopup={this.props.openStockHistoryPopup}
        isValidListMerchantLosupply={this.isValidListMerchantLosupply(this.props.originDomain)}
      />
    );
  };

  render() {
    const {
      data: item,
      index: key,
      industry,
      openUpdateGroupPopup,
      openUpdateSurgePricePopup,
      handleActiveGroup,
      toggleExtraGroupDish,
      openCreateDishPopup,
      handleActivePinned,
      getString,
      originDomain,
      handleActiveShownSupplyItem,
      merchantSlug,
    } = this.props;
    const size = item.get('dishes') ? item.get('dishes').size : 0;
    const { isLoading } = this.state;
    const pagination = item.get('pagination')?.toJS() || {};
    return (
      <div className={cx('mc-submenu', { ['group-title-industry']: industry })} key={key}>
        <h3 id={`groupContent${item.get('id')}`}>
          <a
            className="availability-toggle"
            title={getString('press_to_0_this_dish', '', [!item.get('active') ? 'bật' : 'tắt'])}
            onClick={() => handleActiveGroup(item)}
          >
            <i
              className={cx('availability-status', { active: item.get('active') })}
            />
          </a>
          {item.get('name')}
          <a
            className="edit-group"
            onClick={() => {
              openUpdateGroupPopup(item, industry);
            }}
          >
            <i className="fas fa-pencil-alt" /> Sửa
          </a>
          {size > 0 && [
            <AuthByRole key="surgePrice" roles={ROLES_CONTENT}>
              <a
                className="edit-group"
                onClick={() => {
                  openUpdateSurgePricePopup(item);
                }}
              >
                <i className="fas fa-angle-double-up" /> {getString('additional_fee', 'menuManage')}
              </a>
            </AuthByRole>,
            <AuthByRole key="ghimTop" roles={ROLES_CONTENT}>
              <a
                className={cx('edit-group', { isPinned: item.get('isPinned') })}
                onClick={() => {
                  handleActivePinned(item);
                }}
              >
                <i
                  className={cx('fas', {
                    'fa-star': item.get('isPinned'),
                    'fa-arrow-up': !item.get('isPinned'),
                  })}
                />
                {getString(`${item.get('ispinned') ? 'Đã Ghim Top' : 'Ghim Top'}`)}&nbsp;
                <Hint text={getString('Danh mục được ghim top sẽ được ưu tiên xuất hiện đầu tiên trên menu.')} />
              </a>
            </AuthByRole>,
          ]}

          <a className="toggle-extra-dishes pull-right" onClick={() => toggleExtraGroupDish(item)}>
            <i className={`fa fa-check ${item.get('isExtraGroupDish') ? 'active' : ''}`} />{' '}
            {getString('these_are_extra_dishes', 'menuManage')}&nbsp;
            <Hint
              text={getString(
                'if_checked_all_dishes_inside_this_category_cannot_be_ordered_alone_they_must_be_ordered_along_with_at_least_1_non_extra_dish',
                'menuManage',
              )}
            />
          </a>
        </h3>
        {this.isValidListMerchantLosupply(this.props.originDomain) && (
          <AuthByRole key="shownSupplyItem" roles={ROLES_CONTENT}>
            <h3 id={`supply-item-menuContent${key}`}>
              <a
                className="availability-toggle"
                title={getString('press_to_0_this_dish', '', [!item.get('isShownSupplyItemsForUser') ? 'bật' : 'tắt'])}
                onClick={() => handleActiveShownSupplyItem(item)}
              >
                <i className={cx('availability-status', { active: !!item.get('isShownSupplyItemsForUser') })} />
              </a>
            Nguyên liệu hiển thị với người dùng
            </h3>
          </AuthByRole>
        )}
        <div>
          <a className="btn-create-dish" onClick={() => openCreateDishPopup(item)}>
            <i className="lz lz-plus" />
            {getString('add_new_dish', 'menuManage')}
          </a>
          <table className="menu-dish-list">
            <thead>
              <tr>
                <th>
                  <Hint
                    text={getString(
                      'click_to_the_button_at_the_beginning_of_the_dishes_to_turn_onoff_the_customers_can_only_see_the_dishes_that_are_turned_on',
                    )}
                  />
                </th>
                <th>{getString('information_about_dishes')}</th>
                {this.isValidListMerchantLosupply(originDomain) && (
                  <th className='item-inventory'>
                    <AuthByRole key="stock" roles={ROLES_CONTENT}>
                      Tồn kho
                    </AuthByRole>
                  </th>
                )}
                <th />
              </tr>
            </thead>
            <tbody>
              {item.get('dishes') &&
                item
                  .get('dishes')
                  .map((d, k) => this.renderDish(item, d, k))}
            </tbody>
          </table>
          <div className="load-more">
            {pagination?.nextUrl && (
              <Button disabled={isLoading} fw className={cx('outline-blue text-blue bg-transparent', { loading: isLoading })}
                onClick={() => this.handleLoadMoreDish({
                  merchant: merchantSlug,
                  groupDishId: item.get('id'),
                  nextUrl: pagination.nextUrl,
                })}>
                {isLoading ? <Spinner type="flow"><span className="ml-8">{getString('Đang tải')}</span></Spinner> : getString('view_more')}
              </Button>
            )}
          </div>

        </div>
      </div>
    );
  }

  handleLoadMoreDish = async ({ merchant, nextUrl, groupDishId }) => {
    if (!nextUrl || !groupDishId || !merchant) return;
    this.setState({ isLoading: true }, () => {
      this.props.dispatch(fetchMoreDish({
        merchant, nextUrl, groupDishId, isAdmin: this.context.isAdmin,
        callback: () => {
          this.setState({ isLoading: false });
        },
        callbackFailure: () => {
          this.setState({ isLoading: false });
          this.props.dispatch(
            open(CUSTOM_CONTENT, {
              content: this.props.getString('fail_to_load_more_menu_pleasy_try_again'),
            }),
          );
        },
      }));
    });
  }

  isValidListMerchantLosupply = (domain) => LIST_MERCHANT_LOSUPPLY.indexOf(domain.split('/').reverse()[0]) > -1;
}

export default ManagerDishGroup;
