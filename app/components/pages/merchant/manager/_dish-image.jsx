import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { getImageUrl } from 'utils/image';
import { Waypoint } from 'react-waypoint';

class DishImage extends PureComponent {
  static propTypes = { image: PropTypes.string.isRequired }

  constructor(props) {
    super(props);
    this.state = { enteredViewPort: false };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.image !== this.props.image) {
      this.handleImageSrc();
    }
  }


  render() {
    const image = this.props.image;
    const enteredViewPort = this.state.enteredViewPort;

    if (!enteredViewPort) {
      return (
        <div
          className="avatar"
          style={{ backgroundImage: 'url("/dist/images/dish-placeholder.png")' }}
        >
          <Waypoint onEnter={this.onViewPortEnter} onLeave={this.onViewPortLeave} />
        </div>
      );
    }

    return (
      <div className="avatar">
        <img className="dish-image" alt="s" ref="img" data-src={getImageUrl(image, 120)} />
        {image && (
          <div className="big-avatar">
            <div className="big-avatar-wrapper">
              <img alt="big-img" src={getImageUrl(image, 320, 'o')} />
            </div>
          </div>
        )}
      </div>
    );
  }

  handleImageSrc = () => {
    const img = this.refs.img;
    if (img && img.getAttribute('data-src')) {
      img.setAttribute('src', img.getAttribute('data-src'));
      img.onload = () => {
        img.removeAttribute('data-src');
      };
    }
  };

  onViewPortEnter = () => {
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({ enteredViewPort: true }, this.handleImageSrc);
    }, 500);
  };

  onViewPortLeave = () => {
    if (this.timeout) clearTimeout(this.timeout);
    this.setState({ enteredViewPort: false });
  };
}

export default DishImage;
