import React, { PureComponent } from 'react';
import { addCurrency } from 'utils/format';
import cx from 'classnames';
import AuthByRole from 'components/shared/button/auth-by-role';
import { ROLES_CONTENT } from 'settings/variables';
import UNITMENU from 'utils/json/unit_menu';
import DishImage from './_dish-image';
import { Link } from 'react-router-dom';
import QRCode from 'qrcode.react';
import moment from 'utils/shims/moment';

class ManagerDishItem extends PureComponent {
  //   shouldComponentUpdate(nextProps, nextState) {
  //     const data = this.props.data;
  //     const nextData = nextProps.data;

  //     if (!data && nextData) return true;
  //     if (data && nextData && data.selectedCount !== nextData.selectedCount)
  //       return true;
  //     return false;
  //   }

  render() {
    const {
      group,
      data: dish,
      index: key,
      eatery,
      getString,
      handleActiveDish,
      hasRoleContent,
      openCloneDishPopup,
      openUpdateDishCustomPopup,
      openUpdateDishPopup,
      handleDeleteDish,
      openWareHousingDish,
      openStockHistoryPopup,
      isValidListMerchantLosupply,
    } = this.props;
    const { id: dishId, name: dishName , wallet } = dish.toJS();
    const { stockCount = 0, lastStockAddedAt } = wallet || {};
    const dishDetailUrl = isValidListMerchantLosupply && `/${eatery}/menu/dishes/${dishId}`;

    const valueQRCode = { type: 'loship_merchant_losupply', id: dishId };
    const valueFormatJSON = JSON.stringify(valueQRCode);
    const formatLastStockAddedAt = moment(lastStockAddedAt).format('DD/MM/YYYY');

    const qrCode = <QRCode className="qr-code" value={valueFormatJSON} renderAs="svg" size={100} />;

    return (
      <tr key={key} className='menu-dish-row'>
        <td className='availability'>
          <a
            onClick={() => handleActiveDish(group, dish)}
            title={
              getString('Press to {0} this dish',
                '',
                [!dish.get('active') ? 'bật' : 'tắt'])
            }
          >
            <i
              className={cx('availability-status', { active: dish.get('active') })}
            />
          </a>
        </td>
        <td>
          <div className='dish-item-info'>
            <DishImage image={dish.get('image')} />
            <div className='name'>
              { isValidListMerchantLosupply ? (
                <Link
                  to={dishDetailUrl}>{dish.get('name')}
                </Link>
              ) : (dish.get('name'))}
              {this.isShowMinimumQuantity() &&
                ` - ${dish.get('unitQuantity')} ${dish.get('unit')}`}
            </div>
            {hasRoleContent && [
              <div key="metadata" className='idMenu'>{`[${dish.get('id')}]`}</div>,
              <div
                key="print"
                className={`divcontents-${dish.get('id')}`}
                style={{
                  display: 'none',
                  backgroundColor: 'white',
                  width: '268px',
                  height: '187px',
                  overflow: 'hidden',
                }}
              >
                <div
                  style={{
                    fontSize: '30px',
                    fontWeight: 'bold',
                    height: '102px',
                    lineHeight: '34px',
                    overflow: 'hidden',
                    paddingTop: '10px',
                    paddingLeft: '8px',
                  }}
                >
                  {dish.get('name')}
                </div>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <div
                    style={{
                      border: 0,
                      paddingTop: '20px',
                      paddingLeft: '20px',
                    }}
                  >
                    {qrCode}
                  </div>
                  <div
                    style={{
                      fontSize: '18px',
                      fontWeight: 'bold',
                      paddingLeft: '5px',
                    }}
                  >
                    <table
                      style={{ border: 0, width: '268px', marginTop: '10px' }}
                      cellPadding={0}
                      cellSpacing={0}
                    >
                      <tbody>
                        <tr>
                          <td style={{ border: 0, paddingTop: '0', paddingLeft: '20px' }}>
                            {this.getUnitInPrint()}
                          </td>
                          <td
                            style={{
                              border: 0,
                            }}
                          >
                            {addCurrency(dish.get('price'))}
                          </td>
                        </tr>
                        <tr>
                          <td style={{ border: 0, paddingTop: '0', paddingLeft: '20px' }}>
                          #{dish.get('id')}
                          </td>
                          <td
                            style={{
                              border: 0,
                            }}
                          >
                            <img
                              src='/dist/images/logo-lo-supply.png'
                              alt=''
                              style={{ height: '21px', verticalAlign: 'middle' }}
                            />
                          </td>
                        </tr>
                        <tr>
                          <td style={{ border: 0, paddingTop: '0', paddingLeft: '20px' }}>
                            {formatLastStockAddedAt}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>,
              <iframe
                key="iframe"
                title='print'
                id={`ifmcontentstoprint-${dish.get('id')}`}
                style={{ display: 'none' }}
              />,
            ]}
            <div className='price'>
              {addCurrency(dish.get('price'))}
              <AuthByRole roles={ROLES_CONTENT}>
                {dish.get('additionalFee') > 0 && (
                  <span className='additionalFee'>
                    {' '}
                    +{addCurrency(dish.get('additionalFee'))}
                  </span>
                )}
              </AuthByRole>
            </div>
          </div>
        </td>
        {isValidListMerchantLosupply && (
          <td>
            <AuthByRole key="stock" roles={ROLES_CONTENT}>
              <div className='dish-item-inventory'>
                {stockCount}
              </div>
            </AuthByRole>
          </td>
        )}
        <td>
          <div className='dish-item-actions'>
            <AuthByRole roles={ROLES_CONTENT}>
              <a
                className='btn-dish-action'
                onClick={() => handleDeleteDish(group, dish)}
              >
                <i className='fa fa-trash' /> {getString('delete')}
              </a>
            </AuthByRole>
            <AuthByRole roles={ROLES_CONTENT}>
              <a
                className='btn-dish-action'
                onClick={() => openCloneDishPopup(group, dish)}
              >
                <i className='fa fa-clone' /> {getString('duplicate')}
              </a>
            </AuthByRole>
            <a
              className='btn-dish-action'
              onClick={() => openUpdateDishCustomPopup(group, dish)}
            >
              <i className='fas fa-cogs' /> {getString('options')}
            </a>
            <a
              className='btn-dish-action'
              onClick={() => openUpdateDishPopup(group, dish)}
            >
              <i className='fas fa-pencil-alt' /> {getString('edit')}
            </a>
            <br />
            {isValidListMerchantLosupply && [
              <AuthByRole key={1} roles={ROLES_CONTENT}>
                <a
                  className='btn-dish-action'
                  onClick={() => openWareHousingDish(group, dishId, dishName, stockCount)}
                >
                  <i className='fa fa-plus' /> {getString('Nhập kho')}
                </a>
              </AuthByRole>,
              <AuthByRole key={2} roles={ROLES_CONTENT}>
                <a
                  className='btn-dish-action'
                  onClick={() => openStockHistoryPopup(dishId, dishName, lastStockAddedAt)}
                >
                  <i className="fa fa-history" /> {getString('Lịch sử nhập kho')}
                </a>
              </AuthByRole>,
            ]}
            <a
              className='btn-dish-action print'
              onClick={this.openPageInforBarcode(dish.get('id'))}
            >
              <i className='fas fa-print' /> {getString('printBarcode')}
            </a>
          </div>
        </td>
      </tr>
    );
  }

  isShowMinimumQuantity = () => {
    const { data: dish } = this.props;
    const unit = dish.get('unit');
    if (unit) {
      const res = UNITMENU.find((menu) => unit === menu.name);
      if (res) {
        const unitQuantity = parseFloat(dish.get('unitQuantity'));
        if (unitQuantity >= res.minimumQuantityToShow) {
          return true;
        }
        return false;
      }

      // if find not found
      return true;
    }
    return false;
  };

  openPageInforBarcode = (id) => () => {
    const content = document.getElementsByClassName(`divcontents-${id}`);
    const print = document.getElementById(`ifmcontentstoprint-${id}`)
      .contentWindow;
    print.document.open();
    print.document.write(
      '<style type="text/css">@media print {@page { margin: 0; size: 75mm 50mm landscape;  } body{ margin: 0; position: absolute; top: 0; left: 0; font-family: monospace, "Source Sans Pro", "Helvetica Neue", "Helvetica", "Arial", sans-serif}</style>' +
      content[0].innerHTML,
    );
    setTimeout(() => {
      print.document.close();
      print.focus();
      print.print();
    }, 250);
  };

  getUnitInPrint = () => {
    const { data } = this.props;
    const { unit, unitQuantity } = data.toJS();
    // let txt = '';
    // if (unit.length > 0 && unitQuantity > 0) txt += ' / ' + unitQuantity + ' ' + unit;
    return unit.length > 0 && unitQuantity > 0
      ? `(${unitQuantity} ${unit})`
      : '';
  };
}

export default ManagerDishItem;
