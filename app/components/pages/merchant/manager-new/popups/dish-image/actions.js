import callAPI from 'actions/helpers';
import { LINK_API_MANAGER, LINK_AUTH } from 'settings/variables';

const API = {
  SEARCH_LOSHIP_DISHS: LINK_API_MANAGER + '/search/dishes',
  SEARCH_LOZI_BLOCK: LINK_AUTH + '/search/blocks',
};

export const fetchLoziBlockImages = async (keyword) => {
  const res = await callAPI('get', {
    url: API.SEARCH_LOZI_BLOCK,
    query: { q: keyword },
  });
  if (res.status !== 200) {
    return {
      data: [],
      nextUrl: null,
    };
  }
  return {
    data: res.body.data.map((d) => {
      return { image: d.image };
    }),
    nextUrl: res.body.pagination.nextUrl,
  };
};

export const fetchMoreLoziBlockImages = async (nextUrl) => {
  const res = await callAPI('get', { url: LINK_AUTH + nextUrl });
  if (res.status !== 200) {
    return {
      data: [],
      nextUrl: null,
    };
  }
  return {
    data: res.body.data.map((d) => {
      return { image: d.image };
    }),
    nextUrl: res.body.pagination.nextUrl,
  };
};

export const fetchMoreLoshipDishImages = async (nextUrl) => {
  const res = await callAPI('get', { url: LINK_API_MANAGER + nextUrl.replace('/v1', '') });
  if (res.status !== 200) {
    return {
      data: [],
      nextUrl: null,
    };
  }
  return {
    data: res.body.data.map((d) => {
      return {
        id: d.id,
        image: d.image,
      };
    }),
    nextUrl: res.body.pagination.nextUrl,
  };
};

export const fetchLoshipDishImages = async (keyword) => {
  const res = await callAPI('get', {
    url: API.SEARCH_LOSHIP_DISHS,
    query: { name: keyword },
  });
  if (res.status !== 200) {
    return {
      data: [],
      nextUrl: null,
    };
  }
  return {
    data: res.body.data.map((d) => {
      return {
        id: d.id,
        image: d.image,
      };
    }),
    nextUrl: res.body.pagination.nextUrl,
  };
};
