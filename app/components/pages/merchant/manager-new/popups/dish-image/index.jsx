import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { MENU_SEARCH_DISH_IMAGE, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import SearchDishImagePopupContent from './_content';

class SearchDishImagePopup extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: MENU_SEARCH_DISH_IMAGE },
  };

  close = () => {
    const params = this.props.data.get('params');
    const onClose = params.onClose;

    this.props.dispatch(close(MENU_SEARCH_DISH_IMAGE));
    typeof onClose === 'function' && onClose();
  };

  render() {
    const params = this.props.data.get('params');
    return (
      <Popup className="menu-dish-image-search" handleClose={this.close}>
        <div className="heading">
          <h3>{this.getString('Tìm kiếm ảnh món ăn từ Loship')}</h3>
          <a onClick={this.close} className="btn-close">
            <i className="fa fa-times" />
          </a>
        </div>
        <SearchDishImagePopupContent
          handleClose={this.close}
          handleSelectDish={params.handleSelectDish}
          keyword={params.keyword}
        />
      </Popup>
    );
  }
}

export default initComponent(SearchDishImagePopup);
