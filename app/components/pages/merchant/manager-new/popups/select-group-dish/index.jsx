import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import Checkbox from 'components/shared/checkbox';

import { MENU_GROUP_DISH, MENU_QUICKLY_GROUP_DISH, close, open } from 'actions/popup';
import { get as getPopup } from 'reducers/popup';
import { getMerchantMenu } from 'reducers/menu-manager';

class SelectGroupDish extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = getPopup(state, params);
      if (data) {
        const menuParams = data.getIn(['data', 'params']) || {};
        const menuManager = getMerchantMenu(state, menuParams);
        if (menuManager) data = data.set('menuManager', menuManager.get('data'));
      }
      return data;
    },
    params: { popup: MENU_GROUP_DISH },
  };

  constructor(props) {
    super(props);
    this.state = { groupDish: null };
  }

  close = () => {
    this.setState({ groupDish: null });
    this.props.dispatch(close(MENU_GROUP_DISH));
  };

  renderCheckbox(item, key) {
    const { params } = this.props.data.toJS();
    const groupDishId = !this.state.groupDish ? params.groupDishId : this.state.groupDish.get('id');
    const isSelected = groupDishId === item.get('id');
    return (
      <div className="custom-option-item" key={key}>
        <Checkbox checked={isSelected} onChange={() => this.handleChangeOption(item)}>
          <span>
            {item.get('name')}
            <span className="food-extra">{!item.get('isExtraGroupDish') && this.getString('(món phụ)')}</span>
          </span>
        </Checkbox>
      </div>
    );
  }

  handleChangeOption = (item) => {
    this.setState({ groupDish: item });
  };

  handleSelectGroupDish = () => {
    const { params } = this.props.data.toJS();
    params.handleGroupDish(this.state.groupDish);
    this.close();
  };

  handleCreateQuicklyGroupDish = () => {
    const { params } = this.props.data.toJS();
    return this.props.dispatch(
      open(MENU_QUICKLY_GROUP_DISH, { dataGroupDishNew: params.handleDataGroupDishNew }),
    );
  };

  render() {
    const { menuManager } = this.props;
    return (
      <Popup className="popup-menu-group-dish menu-dish-customs" handleClose={this.close}>
        <div className="heading text-center">
          <i className="lz lz-arrow-head-left" onClick={this.close} />
          <h3>{this.getString('Danh mục')}</h3>
        </div>
        <div className="content">
          <div className="wrapper-group-dish">
            <div className="txt-note">{this.getString('*Chọn danh mục bạn đã tạo có sẵn hoặc tạo danh mục mới')}</div>
            <div className="create-group-dish" onClick={this.handleCreateQuicklyGroupDish}>
              <i className="lz lz-plus" />
              {this.getString('Tạo danh mục')}
            </div>
            {menuManager.get('group') &&
              menuManager
                .get('group')
                .toArray()
                .map((item, key) => {
                  return this.renderCheckbox(item, key);
                })}
          </div>
        </div>
        <div className="form-action text-center">
          <a className="btn btn-create" onClick={this.handleSelectGroupDish}>
            {this.getString('add')}
          </a>
        </div>
      </Popup>
    );
  }
}

export default initComponent(SelectGroupDish);
