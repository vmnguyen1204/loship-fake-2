import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import Popup from 'components/shared/popup';
import { MENU_UPDATE_DISH_CUSTOM, MENU_UPDATE_DISH_CUSTOMS, close, open } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import DishCustomsContent from './_content';

class UpdateMenuDishCustomsPopup extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      customs: [],
      couldEscape: true,
    };
  }

  static defaultProps = {
    dataGetter,
    params: { popup: MENU_UPDATE_DISH_CUSTOMS },
  };

  openUpdateDishCustomPopup = (custom, selectionType) => {
    typeof this.toggleCouldEscape === 'function' && this.toggleCouldEscape(false);
    const params = this.props.data.get('params');
    this.props.dispatch(
      open(MENU_UPDATE_DISH_CUSTOM, {
        merchant: params.merchant,
        groupDishId: params.groupDishId,
        isCreate: !custom,
        selectionType,
        custom,
        dish: params.dish,
        onClose: () => typeof this.toggleCouldEscape === 'function' && this.toggleCouldEscape(true),
      }),
    );
  };

  close = () => {
    if (!this.state.couldEscape) return;
    this.props.dispatch(close(MENU_UPDATE_DISH_CUSTOMS));
  };

  render() {
    const params = this.props.data.get('params');
    return (
      <Popup className="menu-dish-customs" handleClose={this.close}>
        <DishCustomsContent
          params={{
            merchant: params.merchant,
            groupDishId: params.groupDishId,
            dish: params.dish,
            group: params.group,
            dishId: params.dish.get('id'),
            openUpdateDishPopup: params.openUpdateDishPopup,
            openUpdateDishCustomPopup: this.openUpdateDishCustomPopup,
            handleActiveDish: params.handleActiveDish,
          }}
          handleClose={this.close}
          toggleCouldEscape={this.toggleCouldEscape}
        />
      </Popup>
    );
  }

  toggleCouldEscape = (could) => {
    this.setState({ couldEscape: could });
  };
}

export default initComponent(UpdateMenuDishCustomsPopup);
