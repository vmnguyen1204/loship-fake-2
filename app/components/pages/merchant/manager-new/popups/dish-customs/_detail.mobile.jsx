import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import DishImage from 'components/pages/merchant/manager-new/menus/dish/_dish-image';
import { addCurrency } from 'utils/format';
import { updateDishCustom, updateDishCustomOption } from 'actions/merchant-manager';
import { getDish } from 'reducers/menu-manager';
import { MENU_UPDATE_DISH_CUSTOM, MENU_UPDATE_DISH_CUSTOM_OPTION, open } from 'actions/popup';
import ButtonSwitch from 'components/shared/button/switcher';
import Button from 'components/shared/button';
import PropTypes from 'prop-types';

// TODO: ADD ACTIVE TOGGLE LOGIC

class DishCustomsContent extends BaseComponent {
  static defaultProps = { dataGetter: getDish };
  static contextTypes = { isAdmin: PropTypes.bool };

  openUpdateDishCustomPopup = (custom, selectionType) => {
    typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(false);
    this.props.dispatch(
      open(MENU_UPDATE_DISH_CUSTOM, {
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        isCreate: !custom,
        selectionType,
        custom,
        dish: this.props.data,
        onClose: () => typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(true),
      }),
    );
  };

  openUpdateDishCustomOptionPopup = (customOption, custom) => {
    typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(false);
    this.props.dispatch(
      open(MENU_UPDATE_DISH_CUSTOM_OPTION, {
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        isCreate: !customOption,
        custom,
        customOption,
        dish: this.props.data,
        onClose: () => typeof this.props.toggleCouldEscape === 'function' && this.props.toggleCouldEscape(true),
      }),
    );
  };

  // ACTIVATION

  handleActiveDishCustom = async (custom) => {
    if (!confirm(this.getString('are_you_sure_you_want_to_delete_0', '', [custom.get('name')]))) return;

    await new Promise((resolve) => this.props.dispatch(
      updateDishCustom({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.data.get('id'),
        customId: custom.get('id'),
        customData: custom.set('active', !custom.get('active')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    // do something
  };

  handleActiveDishCustomOption = async (customOption, custom) => {
    if (
      customOption.get('active') &&
      !confirm(this.getString('are_you_sure_you_want_to_delete_0', '', [customOption.get('value')]))
    ) return;
    await new Promise((resolve) => this.props.dispatch(
      updateDishCustomOption({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.data.get('id'),
        customId: custom.get('id'),
        customOptionId: customOption.get('id'),
        customOptionData: customOption.set('active', !customOption.get('active')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    // do something
  };

  componentDidMount = () => {
    const params = this.getParams();
    if (params.dish) {
      const active = params.dish.get('active');
      this.setState({ status: active });
    }
  };

  handleCheckboxChange = (newVal) => {
    const params = this.getParams();
    this.setState({ status: newVal });
    const dishNew = params.dish.set('active', !newVal);
    params.handleActiveDish(params.group, dishNew);
  };

  render() {
    const { data } = this.props;
    const params = this.getParams();
    return (
      <div className="content">
        <div className="heading text-center">
          <i className="lz lz-arrow-head-left" onClick={this.props.handleClose} />
          <div>{this.getString(`${params.dish.get('name')}`)}</div>
        </div>
        <div className="wrapper">
          <table className="menu-dish-list">
            <tbody>
              <tr className="menu-dish-row">
                <td>
                  <div className="dish-item-info">
                    <DishImage image={params.dish.get('image')} />
                    <div className="shorted name">{params.dish.get('name')}</div>
                    <div className="price">{addCurrency(params.dish.get('price'))}</div>
                    <div className="shorted desc">{params.dish.get('description')}</div>
                  </div>
                </td>
                <td>
                  <div className="dish-item-actions">
                    <div className="availability">
                      <ButtonSwitch
                        status={this.state.status}
                        toggle={() => this.handleCheckboxChange(!this.state.status)}
                      />
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="section-option">
            <Button
              type="link"
              className="btn text-red outline-light-gray"
              onClick={() => params.openUpdateDishPopup(params.group, params.dish)}
            >
              <i className="fas fa-pencil-alt" />
              <span>{this.getString('Chỉnh sửa')}</span>
            </Button>
            <Button
              type="link"
              className="btn text-red outline-light-gray"
              onClick={() => params.openUpdateDishCustomPopup(null, 'single_choice')}
            >
              <i className="lz lz-plus" />
              <span>{this.getString('Thêm phân loại')}</span>
            </Button>
          </div>
        </div>
        {data.get('customs') ? (
          <div>
            <h4>{this.getString('Quản lý phân loại của món')}</h4>
            <div className="form">
              <div className="dish-custom">
                <div className="section">{this.renderCustomList()}</div>
              </div>
            </div>
          </div>
        ) : (
          <div className="note-group-custom">
            <img src="/dist/images/ic_no_data.png" alt="No data" />
            <h5>{this.getString('Quản lý phân loại của món')}</h5>
            <p>
              {this.getString(
                'Món của bạn có phân loại kích thước (size), định lượng (đường, đá), hay có các món dùng kèm (như topping của trà sữa)?',
              )}
            </p>
            <p>{this.getString('Thêm phân loại cho món để khách hàng có nhiều lựa chọn khi đặt hàng nhé!')}</p>
          </div>
        )}
      </div>
    );
  }

  // CUSTOM

  renderCustomList() {
    const { data } = this.props;
    return (
      <div className="menu-dish-custom-list">
        <div className="section-custom">
          {data.get('customs') &&
            data
              .get('customs')
              .filter((c) => !!c.get('active'))
              .sortBy((f) => f.get('selectionType'))
              .reverse()
              .map((custom, key) => {
                return this.renderCustom(custom, key);
              })}
        </div>
      </div>
    );
  }

  renderCustom(custom, key) {
    const view = [
      <div className="custom-item" key={key}>
        <div className="name">
          {custom.get('name')}
          <a className="recycle" onClick={() => this.handleActiveDishCustom(custom)}>
            <i className="fa fa-trash" />
          </a>
          <div className="note-select-type">
            <div className="note-1">
              {custom.get('selectionType') === 'single_choice' ? (
                <p>
                  {this.getString('Phân loại bắt buộc ')}
                  <span>{this.getString('(Khách hàng chỉ được chọn 01 khi đặt hàng)')}</span>
                </p>
              ) : (
                <p>
                  {this.getString('Phân loại tùy chọn ')}
                  <span>{this.getString('(Khách hàng được quyền chọn 01 hay nhiều mục, hoặc không cần chọn)')}</span>
                </p>
              )}
            </div>
            <div className="note-2">
              {this.getString('Ghi chú:')}&nbsp;{custom.get('note')}
            </div>
            <div className="option-select">
              <a
                className="btn option-edit"
                onClick={() => this.openUpdateDishCustomPopup(custom, custom.get('selectionType'))}
              >
                <i className="fas fa-pencil-alt" />
                <span>{this.getString('Chỉnh sửa')}</span>
              </a>
              <a className="btn option-new" onClick={() => this.openUpdateDishCustomOptionPopup(null, custom)}>
                <i className="lz lz-plus" />
                <span>{this.getString('Thêm tùy chọn')}</span>
              </a>
            </div>
          </div>
        </div>
        <div className="note">{this.getString('*Nhấn vào tên lựa chọn để chỉnh sửa')}</div>
      </div>,
    ];
    const customOptions = custom.get('customOptions');
    if (customOptions) {
      customOptions
        .filter((c) => !!c.get('active'))
        .sortBy((c) => c.get('value'))
        .forEach((co) => {
          const coView = (
            <div className="custom-option-item" key={`${key}.${co.get('id')}`}>
              <div className="name">
                <a
                  className="title"
                  onClick={(e) => {
                    e.preventDefault();
                    this.openUpdateDishCustomOptionPopup(co, custom);
                  }}
                >
                  {co.get('value')}
                </a>
                <a
                  className="actions"
                  onClick={(e) => {
                    e.preventDefault();
                    this.handleActiveDishCustomOption(co, custom);
                  }}
                >
                  <i className="fa fa-trash" />
                </a>
                <br />
                {co.get('price') > 0 && <span>{addCurrency(co.get('price'))}</span>}
              </div>
            </div>
          );
          view.push(coView);
        });
    }

    return (
      <div className="section-custom-option-item">
        {view}
        <div className="clearfix" />
      </div>
    );
  }
}

export default initComponent(DishCustomsContent);
