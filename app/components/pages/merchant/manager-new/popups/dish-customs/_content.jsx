import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CustomsContentMobile from './_detail.mobile';

export default class CustomsManage extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <CustomsContentMobile {...this.props} />;
  }
}
