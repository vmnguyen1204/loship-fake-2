import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { createDish, createMenuGroup, updateDish } from 'actions/merchant-manager';
import { Map } from 'immutable';
import * as cookies from 'utils/shims/cookie';
import { addCommas, removeCommas } from 'utils/format';
import { MIN_MAIN_DISH_PRICE } from 'utils//menu-tool';
import { CUSTOM_CONTENT, MENU_GROUP_DISH, open } from 'actions/popup';
import DishImage from './_image';
import PropTypes from 'prop-types';

let defaultData = new Map({
  name: '',
  price: '0',
  additionalFee: '0',
  additionalFeePercent: cookies.get('additionalFeePercent') || '10',
  description: '',
  active: false,
});

const mapPropsToState = (props) => {
  if (!props.data) return { data: defaultData };

  const {
    name,
    price,
    description,
    image,
    active,
    additionalFee,
    createdAt,
    createdBy,
    updatedAt,
    updatedBy,
  } = props.data.toJS();
  const priceInt = parseInt(price, 10);
  const additionalFeeInt = parseInt(additionalFee, 10);

  return {
    data: new Map({
      name,
      price: addCommas(price),
      description,
      image,
      active,
      additionalFee: addCommas(additionalFee),
      additionalFeePercent: addCommas(Math.ceil((additionalFeeInt / priceInt) * 100).toString()),
      createdAt,
      createdBy,
      updatedAt,
      updatedBy,
    }),
  };
};

class CreateDishDetail extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = {
      ...mapPropsToState(props),
      groupName: this.getParams().groupName,
      image: null,
      error: '',
      dataGroupDish: null,
      dataGroupDishNew: new Map(),
      readyToView: true,
    };
  }

  componentDidMount() {
    if (this.props.data) this.setState(mapPropsToState(this.props));
    if (this.refs.txtDishName) this.refs.txtDishName.focus();
  }

  createDish = async (payload) => {
    let dishData = payload;
    const { checkIsCreate } = this.props;
    if (!this.props.hasRoleContent) {
      dishData = dishData.set('additionalFee', 0);
    }
    // Cast value to its real type, ex: remove commas in price
    dishData = dishData.set('price', parseInt(removeCommas(dishData.get('price'))));
    dishData = dishData.set('additionalFee', parseInt(removeCommas(dishData.get('additionalFee'))));
    dishData = dishData.set('additionalFeePercent', parseInt(removeCommas(dishData.get('additionalFeePercent'))));

    if (checkIsCreate && this.state.checkCreateQuicklyGroupDish) {
      const groupDishPromoise = await new Promise((resolve) => this.props.dispatch(
        createMenuGroup({
          merchant: this.getParams().merchant,
          groupData: new Map({
            name: this.state.dataGroupDishNew.get('name').trim(),
            isExtraGroupDish: this.state.dataGroupDishNew.get('isExtraGroupDish'),
          }),
          callback: resolve,
          isAdmin: this.context.isAdmin,
        }),
      ));
      if (groupDishPromoise && groupDishPromoise.body && groupDishPromoise.body.data) {
        this.setState({ dataGroupDish: new Map(groupDishPromoise.body.data) });
      }
    }

    if (
      checkIsCreate
        ? await new Promise((resolve) => this.props.dispatch(
          createDish({
            merchant: this.getParams().merchant,
            groupDishId: !this.state.dataGroupDish
              ? this.getParams().groupDishId
              : this.state.dataGroupDish.get('id'),
            dishData,
            callback: resolve,
            isAdmin: this.context.isAdmin,
          }),
        ))
        : await new Promise((resolve) => this.props.dispatch(
          updateDish({
            dishId: this.props.data.get('id'),
            merchant: this.getParams().merchant,
            groupDishId: this.getParams().groupDishId,
            dishData,
            callback: resolve,
            isAdmin: this.context.isAdmin,
          }),
        ))
    ) cookies.set('additionalFeePercent', this.state.data.get('additionalFeePercent'));
    defaultData = defaultData.set('additionalFeePercent', this.state.data.get('additionalFeePercent'));
    return this.props.handleClose();
  };

  handleDish = async (e) => {
    e.preventDefault();
    const { checkIsCreate } = this.props;
    if (checkIsCreate) this.setState({ readyToView: false });
    const { data: dish, image } = this.state;
    const isExtraGroupDish = this.getParams().isExtraGroupDish;

    let errorMessage = '';

    const { name, price, additionalFee, image: currentImage } = dish.toObject();

    if (!image && !currentImage) {
      errorMessage = 'Bạn chưa chọn ảnh món.';
    }

    if (!isExtraGroupDish && removeCommas(price) + removeCommas(additionalFee) < MIN_MAIN_DISH_PRICE) errorMessage = 'Món chính không được bán giá thấp hơn ' + addCommas(MIN_MAIN_DISH_PRICE) + 'đ';

    if (!price) errorMessage = 'Giá bán là bắt buộc nhập.';
    if (removeCommas(price) % 1000 !== 0 && !this.props.hasRoleContent) errorMessage = 'Giá bán cần được làm tròn đến phần nghìn. VD: 10.000, 15.000, 100.000,...';

    if (!name) errorMessage = 'Tên món là bắt buộc nhập.';

    if (errorMessage) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          content: () => {
            return (
              <div className="text-center">
                <p>{errorMessage}</p>
              </div>
            );
          },
        }),
      );
    }

    if (image && image.hasDish()) {
      return this.createDish(dish.set('copyImageFromDish', this.state.image.getDish()));
    } if (image && image.hasImage()) {
      return this.createDish(dish.set('image', await this.state.image.getURL()));
    }
    return this.createDish(dish);
  };

  handleGroupDish = (item) => {
    this.setState({ dataGroupDish: item, groupName: item.get('name') });
  };

  handleInputChange = (field, e) => {
    const rawValue = e.target.value;
    let { data } = this.state;

    if (field === 'price') {
      data = data.set(field, addCommas(removeCommas(rawValue)));
    } else {
      data = data.set(field, rawValue);
    }

    if (field === 'price' || field === 'additionalFeePercent') {
      const price = parseInt(removeCommas(data.get('price')), 10);
      const additionalFeePercent = parseInt(removeCommas(data.get('additionalFeePercent')), 10) / 100;
      if (!isNaN(price) && !isNaN(additionalFeePercent)) {
        data = data.set(
          'additionalFee',
          addCommas((Math.ceil(((price * additionalFeePercent) / 1000).toFixed(3)) * 1000).toString()),
        );
      } else {
        data = data.set('additionalFee', '0');
      }
    }
    if (field === 'additionalFee') {
      const price = parseInt(removeCommas(data.get('price')), 10);
      const additionalFee = parseInt(removeCommas(data.get('additionalFee')), 10);
      if (!isNaN(price) && !isNaN(additionalFee)) {
        data = data.set('additionalFeePercent', addCommas(Math.ceil((additionalFee / price) * 100).toString()));
      } else {
        data = data.set('additionalFeePercent', '0');
      }
    }

    this.setState({ data });
    const srcData = this.props.checkIsCreate ? defaultData : this.props.data;
    this.props.toggleEditing(this.isEditing(srcData, data));
  };

  setImage = (image) => {
    const srcData = this.props.checkIsCreate ? defaultData : this.props.data;
    this.setState({ image }, () => this.props.toggleEditing(this.isEditing(srcData, this.state.data)));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      parseInt(srcData.get('price')) !== parseInt(removeCommas(data.get('price'))) ||
      parseInt(srcData.get('additionalFee')) !== parseInt(removeCommas(data.get('additionalFee'))) ||
      srcData.get('description') !== data.get('description') ||
      !!this.state.image
    );
  };

  selectGroupDish = () => {
    return this.props.dispatch(
      open(MENU_GROUP_DISH, {
        groupDishId: !this.state.dataGroupDish ? this.getParams().groupDishId : this.state.dataGroupDish.get('id'),
        merchant: this.getParams().merchant,
        handleGroupDish: this.handleGroupDish,
        handleDataGroupDishNew: this.handleDataGroupDishNew,
      }),
    );
  };

  handleDataGroupDishNew = (data) => {
    this.setState({ groupName: data.get('name'), checkCreateQuicklyGroupDish: true, dataGroupDishNew: data });
  };

  render() {
    const { checkIsCreate } = this.props;
    const data = this.state.data;
    const hasRoleContent = this.props.hasRoleContent;
    return (
      <div>
        <div className="content">
          <div className="heading text-center">
            <i className="lz lz-arrow-head-left" onClick={this.props.handleClose} />
            <div>{checkIsCreate ? this.getString('Thêm món mới') : this.getString('Chỉnh sửa món')}</div>
          </div>
          <form className="form" onSubmit={(e) => e.preventDefault()}>
            <div className="group-image">
              <div className="input">
                <DishImage
                  preloadImage={data.get('image')}
                  image={this.state.image}
                  setImage={this.setImage}
                  keyword={data.get('name')}
                  isAllowedToCopyDishImage={hasRoleContent}
                  toggleCouldEscape={this.props.toggleCouldEscape}
                />
              </div>
            </div>
            {checkIsCreate && (
              <div className="group-dish">
                <label>{this.getString('Danh mục: ')}</label>
                <a className="btn-create-group" onClick={this.selectGroupDish}>
                  {this.state.groupName ? this.state.groupName : this.getString('Chọn danh mục')}
                </a>
              </div>
            )}

            <div className="group-name">
              <label>
                {this.getString('dish_name')} <span className="required">*</span>
              </label>
              <div className="input">
                <input ref="txtDishName" value={data.get('name')} onChange={(e) => this.handleInputChange('name', e)} />
              </div>
            </div>
            <div className="group-price">
              <label>
                {this.getString('6vDaz_GcUI')} <span className="required">*</span>
              </label>
              <div className="input">
                <input value={data.get('price')} onChange={(e) => this.handleInputChange('price', e)} />
              </div>
            </div>
            {!checkIsCreate && (
              <div className="group-dish">
                <label>{this.getString('Danh mục: ')}</label>
                <div className="btn-update-group">{this.getParams().groupName}</div>
              </div>
            )}

            <div className="group-note">
              <label>{this.getString('notes_if_any')}</label>
              <div className="input">
                <textarea
                  rows="3"
                  value={data.get('description')}
                  onChange={(e) => this.handleInputChange('description', e)}
                />
              </div>
            </div>
          </form>
          <div className="form-action text-center">
            {checkIsCreate ? (
              <a className="btn btn-create" onClick={this.handleDish}>
                {this.getString('add')}
              </a>
            ) : (
              <a className="btn btn-create" onClick={this.handleDish}>
                {this.getString('apply_changes')}
              </a>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(CreateDishDetail);
