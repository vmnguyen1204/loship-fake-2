import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DishManageMobile from './_detail.mobile';

export default class DishManage extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <DishManageMobile {...this.props} />;
  }
}
