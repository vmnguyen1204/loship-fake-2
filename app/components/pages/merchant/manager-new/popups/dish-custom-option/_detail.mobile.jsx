import { createDishCustomOption, updateDishCustomOption } from 'actions/merchant-manager';
import BaseComponent from 'components/base';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import React from 'react';
import { LINK_DOMAIN_LOZI } from 'settings/variables';
import moment from 'utils/shims/moment';
import PropTypes from 'prop-types';

const defaultDataObject = new Map({
  value: '',
  price: 0,
  limitQuantity: 1,
  active: false,
});

const mapPropsToState = (props) => {
  if (!props.data || props.isCreate) return { data: defaultDataObject.set('customId', props.custom.get('id')) };

  const { id, active, value, price, limitQuantity, createdBy, createdAt, updatedBy, updatedAt } = props.data.toJS();
  return {
    data: new Map({
      id,
      value,
      price,
      limitQuantity,
      active,
      createdBy,
      createdAt,
      updatedBy,
      updatedAt,
    }),
  };
};

class DishCustomOptionContent extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = {
      data: new Map(defaultDataObject),
      readyToView: true,
    };
  }

  componentDidMount() {
    this.setState(mapPropsToState(this.props));

    if (this.refs.txtCustomOptionName) this.refs.txtCustomOptionName.focus();
  }

  handleCreateDishCustomOption = async () => {
    await new Promise((resolve) => this.props.dispatch(
      createDishCustomOption({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.dish.get('id'),
        customId: this.props.custom.get('id'),
        customOptionData: this.state.data,
        callback: (data) => {
          if (data.status === 200) this.setState({ readyToView: false });
          resolve();
        },
        isAdmin: this.context.isAdmin,
      }),
    ));
    return this.props.handleClose();
  };

  handleUpdateDishCustomOption = async (e) => {
    e.preventDefault();
    await new Promise((resolve) => this.props.dispatch(
      updateDishCustomOption({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.dish.get('id'),
        customId: this.props.custom.get('id'),
        customOptionId: this.state.data.get('id'),
        customOptionData: this.state.data,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    return this.props.handleClose();
  };

  handleInputChange = (field, e) => {
    let { data } = this.state;
    data = data.set(field, e.target.value);

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultDataObject, data));
  };

  handleCheckboxChange = (newValue) => {
    let { data } = this.state;
    data = data.set('active', newValue);

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultDataObject, data));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('value') !== data.get('value') ||
      srcData.get('price') !== data.get('price') ||
      srcData.get('limitQuantity') !== data.get('limitQuantity')
    );
  };

  toggleEditorInfo = () => {
    this.setState((state) => ({ ...state, showDetail: !state.showDetail }));
  };

  renderEditorInfo = () => {
    const { data } = this.state;
    const { createdAt, createdBy, updatedAt, updatedBy } = (data && data.toObject()) || {};

    // VALIDATE INPUT DATA
    if (!createdBy || !updatedBy) return null;

    return (
      <div className="history">
        <span>
          {this.getString('created_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + createdBy.username}>
            @{createdBy.username}
          </a>
          {this.getString('at')}
          {moment(createdAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <br />
        <span>
          {this.getString('updated_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + updatedBy.username}>
            @{updatedBy.username}
          </a>
          {this.getString('at')}
          {moment(updatedAt).format('HH:mm, DD-MM-YYYY')}
        </span>
      </div>
    );
  };

  render() {
    const data = this.state.data;
    const isCreate = this.props.isCreate;
    const { custom } = this.props;
    return (
      <div className="content">
        <div className="heading text-center">
          <div>
            <i className="lz lz-arrow-head-left" onClick={this.props.handleClose} />
            {this.getString(isCreate ? 'Thêm lựa chọn' : 'Sửa thông tin lựa chọn')}
          </div>
        </div>
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <div className="txt-custom">
            {this.getString('Phân loại:')}
            <b>&nbsp;{custom.get('name')}&nbsp;</b>
            {this.getString('(tùy chọn)')}
          </div>
          <div className="">
            <label>{this.getString('Tên lựa chọn')}</label>
            <div className="input">
              <input
                placeholder={this.getString('option_name')}
                ref="txtCustomOptionName"
                value={data.get('value')}
                onChange={(e) => this.handleInputChange('value', e)}
              />
            </div>
          </div>
          <div className="">
            <label>{this.getString('Tiền cộng thêm (VNĐ)')}</label>
            <div className="input">
              <input
                placeholder={this.getString('6vDaz_GcUI')}
                value={data.get('price')}
                onChange={(e) => this.handleInputChange('price', e)}
              />
            </div>
          </div>
          <div className="form-action text-center">
            {!isCreate && (
              <a className="btn btn-create" onClick={this.handleUpdateDishCustomOption}>
                {this.getString('Lưu lựa chọn')}
              </a>
            )}
            {isCreate && (
              <a className="btn btn-create" onClick={this.state.readyToView && this.handleCreateDishCustomOption}>
                {this.getString('Thêm lựa chọn')}
              </a>
            )}
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(DishCustomOptionContent);
