import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CustomOptionMobile from './_detail.mobile';

export default class CustomOptionManage extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <CustomOptionMobile {...this.props} />;
  }
}
