import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CustomManageMobile from './_detail.mobile';

export default class CustomManage extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <CustomManageMobile {...this.props} />;
  }
}
