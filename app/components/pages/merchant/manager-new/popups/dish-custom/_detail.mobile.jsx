import { createDishCustom, updateDishCustom } from 'actions/merchant-manager';
import cx from 'classnames';
import BaseComponent from 'components/base';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import React from 'react';
import { LINK_DOMAIN_LOZI } from 'settings/variables';
import moment from 'utils/shims/moment';
import PropTypes from 'prop-types';

const defaultDataObject = new Map({
  name: '',
  price: 0,
  note: '',
  limitQuantity: 1,
  active: false,
});

const mapPropsToState = (props) => {
  if (!props.data || props.isCreate) return { data: defaultDataObject.set('selectionType', props.selectionType || 'single_choice') };

  const {
    id,
    name,
    price,
    note,
    limitQuantity,
    active,
    selectionType,
    createdBy,
    createdAt,
    updatedBy,
    updatedAt,
  } = props.data.toJS();
  return {
    data: new Map({
      id,
      name,
      price,
      note,
      limitQuantity,
      active,
      selectionType,
      createdBy,
      createdAt,
      updatedBy,
      updatedAt,
    }),
  };
};

class DishCustomContent extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = {
      data: new Map(defaultDataObject),
      selectionType: 'single_choice',
      activeCheckbox: props.selectionType === 'multi_choice',
      readyToView: true,
    };
  }

  componentDidMount() {
    this.setState(mapPropsToState(this.props));
    if (this.refs.txtCustomName) this.refs.txtCustomName.focus();
  }

  handleCreateDishCustom = async (e) => {
    e.preventDefault();
    await new Promise((resolve) => this.props.dispatch(
      createDishCustom({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.dish.get('id'),
        customData: this.state.data,
        callback: (data) => {
          if (data.status === 200) this.setState({ readyToView: false });
          resolve();
        },
        isAdmin: this.context.isAdmin,
      }),
    ));
    return this.props.handleClose();
  };

  handleUpdateDishCustom = async (e) => {
    e.preventDefault();
    await new Promise((resolve) => this.props.dispatch(
      updateDishCustom({
        merchant: this.getParams().merchant,
        groupDishId: this.getParams().groupDishId,
        dishId: this.props.dish.get('id'),
        customId: this.state.data.get('id'),
        customData: this.state.data,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    return this.props.handleClose();
  };

  handleInputChange = (field, e) => {
    let { data } = this.state;
    data = data.set(field, e.target.value);

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultDataObject, data));
  };

  handleCheckboxChange = (newValue) => {
    let { data } = this.state;
    data = data.set('active', newValue);

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultDataObject, data));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      srcData.get('note') !== data.get('note') ||
      srcData.get('limitQuantity') !== data.get('limitQuantity')
    );
  };

  toggleEditorInfo = () => {
    this.setState((state) => ({ ...state, showDetail: !state.showDetail }));
  };

  renderEditorInfo = () => {
    const { data } = this.state;
    const { createdAt, createdBy, updatedAt, updatedBy } = (data && data.toObject()) || {};

    // VALIDATE INPUT DATA
    if (!createdBy || !updatedBy) return null;

    return (
      <div className="history">
        <span>
          {this.getString('created_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + createdBy.username}>
            @{createdBy.username}
          </a>
          {this.getString('at')}
          {moment(createdAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <br />
        <span>
          {this.getString('updated_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + updatedBy.username}>
            @{updatedBy.username}
          </a>
          {this.getString('at')}
          {moment(updatedAt).format('HH:mm, DD-MM-YYYY')}
        </span>
      </div>
    );
  };

  handleActive = () => {
    const typeChoice = this.state.selectionType === 'single_choice' ? 'multi_choice' : 'single_choice';
    let { data } = this.state;
    data = data.set('selectionType', typeChoice);
    this.setState((state) => ({ data, activeCheckbox: !state.activeCheckbox, selectionType: typeChoice }));
  };

  render() {
    const data = this.state.data;
    const isSingleChoice = this.state.selectionType === 'single_choice';
    const isCreate = this.props.isCreate;
    const selectionType = this.props.selectionType === 'single_choice';
    const { dish } = this.props;
    return (
      <div className="content">
        <div className="heading text-center">
          <i className="lz lz-arrow-head-left" onClick={this.props.handleClose} />
          <div>{this.getString(isCreate ? 'Tạo mục phân loại' : 'Sửa thông tin phân loại')}</div>
        </div>
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <div className="txt-custom">
            {this.getString('Phân loại:')}
            <b>&nbsp;{dish.get('name')}&nbsp;</b>
            {this.getString('(tùy chọn)')}
          </div>
          <div className="">
            <label>{this.getString('Tên mục phân loại')}</label>
            <div className="input">
              <input
                placeholder={this.getString('option_name')}
                ref="txtCustomName"
                value={data.get('name')}
                onChange={(e) => this.handleInputChange('name', e)}
              />
            </div>
          </div>
          <div className="">
            <label>{this.getString('Mô tả (nếu có)')}</label>
            <div className="input">
              <textarea
                placeholder={this.getString('note')}
                value={data.get('note')}
                onChange={(e) => this.handleInputChange('note', e)}
              />
            </div>
          </div>
          <div className="extra-select-type">
            {isCreate && (
              <a onClick={this.handleActive}>
                <i
                  className={cx('fas fa-check', 'extra-dishes', { active: this.state.activeCheckbox })}
                />
                {this.getString('Đây là phân loại tùy chọn không bắt buộc')}
              </a>
            )}
            {!isCreate && !selectionType && (
              <a>
                <i
                  className={cx('edit fas fa-check ', 'extra-dishes', { active: this.state.activeCheckbox })}
                />
                {this.getString('Đây là phân loại tùy chọn không bắt buộc')}
              </a>
            )}
          </div>
          {isCreate ? (
            <span className="txt-note">
              {!isSingleChoice
                ? this.getString(
                  'Khi đặt món, khách của bạn có thể chọn đến số tối đa cho phép. Vui lòng nhập số lựa chọn tối đa mà khách có thể chọn khi đặt món. Ví dụ: tối đa 03 loại topping trà sữa.',
                )
                : this.getString(
                  'Mặc định, phân loại là bắt buộc chọn khi đặt món. Ví dụ: size, mức đường, mức đá. Đánh dấu vào ô này nếu mục phân loại này là không bắt buộc. Ví dụ: topping trà sữa, đồ ăn thêm,...',
                )}
            </span>
          ) : (
            <span className="txt-note">
              {!selectionType
                ? this.getString(
                  'Khi đặt món, khách của bạn có thể chọn đến số tối đa cho phép. Vui lòng chọn số lựa chọn tối đa mà khách có thể chọn khi đặt món. Ví dụ: tối đa 03 loại topping trà sữa.',
                )
                : this.getString(
                  'Đây là phận loại bắt buộc. Khi khách hàng đặt món, bắt buộc phải chọn 01 lựa chọn trong phân loại này. Ví dụ: size, mức đường, mức đá.',
                )}
            </span>
          )}
          {isCreate && !isSingleChoice && (
            <div className="">
              <lable className="allow-default">{this.getString('Cho phép chọn tối đa (mặc định: 3 lựa chọn)')}</lable>
              <div className="input">
                <input
                  type="number"
                  min={1}
                  max={3}
                  ref="txtCustomLimitQuantity"
                  value={data.get('limitQuantity')}
                  onChange={(e) => this.handleInputChange('limitQuantity', e)}
                />
              </div>
            </div>
          )}
          {!isCreate && !selectionType && (
            <div className="">
              <lable className="allow-default">{this.getString('Cho phép chọn tối đa (mặc định: 3 lựa chọn)')}</lable>
              <div className="input">
                <input
                  type="number"
                  min={1}
                  max={3}
                  ref="txtCustomLimitQuantity"
                  value={data.get('limitQuantity')}
                  onChange={(e) => this.handleInputChange('limitQuantity', e)}
                />
              </div>
            </div>
          )}
          <div className="form-action text-center">
            {!isCreate && (
              <a className="btn btn-create" onClick={this.handleUpdateDishCustom}>
                {this.getString('save')}
              </a>
            )}
            {isCreate && (
              <a className="btn btn-create" onClick={this.state.readyToView && this.handleCreateDishCustom}>
                {this.getString('add')}
              </a>
            )}
          </div>
        </form>
      </div>
    );
  }
}

export default initComponent(DishCustomContent);
