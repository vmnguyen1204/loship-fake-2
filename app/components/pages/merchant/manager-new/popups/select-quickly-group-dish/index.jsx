import React from 'react';
import initComponent from 'lib/initComponent';
import BaseComponent from 'components/base';
import cx from 'classnames';
import Popup from 'components/shared/popup';
import { Map } from 'immutable';

import { MENU_GROUP_DISH, MENU_QUICKLY_GROUP_DISH, close } from 'actions/popup';
import { get as dataGetter } from 'reducers/popup';
import Button from 'components/shared/button';

class SelectQuicklyGroupDish extends BaseComponent {
  static defaultProps = {
    dataGetter,
    params: { popup: MENU_QUICKLY_GROUP_DISH },
  };

  constructor(props) {
    super(props);
    this.state = { data: new Map({ isExtraGroupDish: false }) };
  }

  close = () => {
    this.props.dispatch(close(MENU_QUICKLY_GROUP_DISH));
  };

  handleChange = (field, value) => {
    let { data } = this.state;
    data = data.set(field, value);
    this.setState({ data });
  };

  handleCheckboxChange = (field) => {
    let { data } = this.state;
    data = data.set(field, !data.get(field));
    this.setState({ data });
  };

  handleQuicklyGroupDish = () => {
    const { params } = this.props.data.toJS();
    params.dataGroupDishNew(this.state.data);
    this.props.dispatch(close(MENU_QUICKLY_GROUP_DISH));
    this.props.dispatch(close(MENU_GROUP_DISH));
  };

  render() {
    const { data } = this.state;
    return (
      <Popup className="popup-menu-quickly-group-dish menu-dish-customs" handleClose={this.close}>
        <div className="heading text-center">
          <div>{this.getString('Tạo danh mục mới')}</div>
        </div>
        <div className="content">
          <div className="form-group">
            <div className="input">
              <input
                placeholder={this.getString('Tên danh mục')}
                ref="txtName"
                onChange={(e) => this.handleChange('name', e.target.value)}
              />
            </div>
          </div>
          <div className="extra-dishes-food">
            <a onClick={() => this.handleCheckboxChange('isExtraGroupDish')}>
              <i
                className={cx('fas fa-check', 'extra-dishes', { active: !!data.get('isExtraGroupDish') })}
              />
              <label>{this.getString('Đây là danh mục món phụ')}</label>
            </a>
            <span className="txt-note">
              {this.getString(
                'Những món nằm trong danh mục món phụ sẽ chỉ được chọn mua khi đơn hàng có ít nhất 01 món chính.',
              )}
            </span>
          </div>
        </div>

        <div className="form-action text-center px-16">
          <Button type="primary" fw onClick={this.handleQuicklyGroupDish}>
            {this.getString('add')}
          </Button>
          <Button type="link" className="btn text-gray p16_16" onClick={this.close}>
            {this.getString('abort')}
          </Button>
        </div>
      </Popup>
    );
  }
}

export default initComponent(SelectQuicklyGroupDish);
