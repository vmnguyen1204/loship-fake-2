import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DishDetailMobile from './_detail.mobile';

export default class GroupManage extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <DishDetailMobile {...this.props} />;
  }
}
