import { createMenuGroup, updateMenuGroup } from 'actions/merchant-manager';
import cx from 'classnames';
import BaseComponent from 'components/base';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import React from 'react';
import { LINK_DOMAIN_LOZI } from 'settings/variables';
import { MIN_MAIN_DISH_PRICE, isGroupDishHasInvalidDish } from 'utils//menu-tool';
import { addCommas } from 'utils/format';
import moment from 'utils/shims/moment';
import PropTypes from 'prop-types';

if (process.env.BROWSER) {
  require('assets/styles/components/_checkbox.scss');
}

const defaultData = new Map({
  name: '',
  active: false,
});

const mapPropsToState = (props) => {
  if (!props.data) return { data: defaultData };

  const { name, active, isExtraGroupDish, createdBy, createdAt, updatedBy, updatedAt, dishes } = props.data.toJS();
  return { data: new Map({ name, active, isExtraGroupDish, createdBy, createdAt, updatedBy, updatedAt, dishes }) };
};

class MobileUpdateMenuGroupPopup extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: new Map(),
      error: '',
      readyToView: true,
    };
  }

  static defaultProps = { customRender: true };
  static contextTypes = { isAdmin: PropTypes.bool };

  componentDidMount() {
    if (this.props.data) this.setState(mapPropsToState(this.props));
    if (this.refs.txtName) this.refs.txtName.focus();
  }

  handleCreate = async () => {
    await new Promise((resolve) => this.props.dispatch(
      createMenuGroup({
        merchant: this.getParams().merchant,
        groupData: new Map({
          name: this.refs.txtName.value.trim(),
          isExtraGroupDish: this.state.data.get('isExtraGroupDish'),
        }),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    this.props.handleClose();
  };

  handleUpdate = async (e) => {
    e.preventDefault();
    const invalidDish = isGroupDishHasInvalidDish(this.props.data.get('dishes'), MIN_MAIN_DISH_PRICE);
    const isExtraGroupDish = this.state.data.get('isExtraGroupDish');

    if (!isExtraGroupDish && invalidDish !== null) {
      return this.setState({
        error: this.getString(
          `Không thể đổi menu này thành menu món chính, do có món "${
            invalidDish.name
          }" có giá bán thấp hơn mức quy định dành cho món chính (${addCommas(
            MIN_MAIN_DISH_PRICE,
          )}đ). Anh chị vui lòng cập nhật lại giá bán trước.`,
        ),
      });
    }

    await new Promise((resolve) => this.props.dispatch(
      updateMenuGroup({
        merchant: this.getParams().merchant,
        groupDishId: this.props.data.get('id'),
        groupData: this.state.data,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    this.props.handleClose();
  };

  handleDelete = async () => {
    const data = this.props.data;
    if (data.get('dishes').size > 0) {
      return this.setState({
        error: this.getString(
          `Chỉ có thể xóa danh mục "${data.get('name')}" khi đã di chuyển toàn bộ món ăn sang danh mục khác.`,
        ),
      });
    }
    // need handle
  };

  handleChange = (field, value) => {
    let { data } = this.state;
    data = data.set(field, value);

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultData, data));
  };

  handleCheckboxChange = (field) => {
    let { data } = this.state;
    data = data.set(field, !data.get(field));

    this.setState({ data });
    this.props.toggleEditing(this.isEditing(this.props.data || defaultData, data));
  };

  isEditing = (srcData, data) => {
    if (typeof srcData.get !== 'function' || typeof data.get !== 'function') return false;

    return (
      srcData.get('active') !== data.get('active') ||
      srcData.get('name') !== data.get('name') ||
      srcData.get('isExtraGroupDish') !== data.get('isExtraGroupDish')
    );
  };

  toggleEditorInfo = () => {
    this.setState((state) => ({ ...state, showDetail: !state.showDetail }));
  };

  renderEditorInfo = () => {
    const { data } = this.state;
    const { createdAt, createdBy, updatedAt, updatedBy } = (data && data.toObject()) || {};

    // VALIDATE INPUT DATA
    if (!createdBy || !updatedBy) return null;

    return (
      <div className="history">
        <span>
          {this.getString('created_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + createdBy.username}>
            @{createdBy.username}
          </a>
          {this.getString('at')}
          {moment(createdAt).format('HH:mm, DD-MM-YYYY')}
        </span>
        <br />
        <span>
          {this.getString('updated_by')}{' '}
          <a target="_blank" rel="noopener" href={LINK_DOMAIN_LOZI + '/' + updatedBy.username}>
            @{updatedBy.username}
          </a>
          {this.getString('at')}
          {moment(updatedAt).format('HH:mm, DD-MM-YYYY')}
        </span>
      </div>
    );
  };

  render() {
    const isCreate = this.props.isCreate;
    const error = this.state.error;
    const data = this.state.data;
    return (
      <div className="content">
        <div className="heading text-center">
          <i className="lz lz-arrow-head-left" onClick={this.props.handleClose} />
          {this.getString(isCreate ? 'Tạo danh mục mới' : 'Chỉnh sửa danh mục')}
        </div>
        <form className="form" onSubmit={(e) => e.preventDefault()}>
          <label>{this.getString('Tên danh mục')}</label>
          <div className="input">
            {this.props.isCreate ? (
              <input ref="txtName" />
            ) : (
              <input ref="txtName" value={data.get('name')} onChange={(e) => this.handleChange('name', e.target.value)} />
            )}
          </div>
          <div className="extra-dishes-food">
            <a onClick={() => this.handleCheckboxChange('isExtraGroupDish')}>
              <i
                className={cx('fas fa-check', 'extra-dishes', { active: data.get('isExtraGroupDish') })}
              />
              <label>{this.getString('Menu món phụ')}</label>
            </a>
          </div>
          <span className="txt-note">
            {this.getString(
              'Những món nằm trong danh mục món phụ sẽ chỉ được chọn mua khi đơn hàng có ít nhất 01 món chính.',
            )}
          </span>
          {!!error && <div className="form-message">{error}</div>}
        </form>
        <div className="form-action">
          {isCreate && (
            <a className="btn btn-create text-center" onClick={this.state.readyToView && this.handleCreate}>
              {this.getString('add')}
            </a>
          )}
          {!isCreate && (
            <a className="btn btn-update text-center" onClick={this.handleUpdate}>
              {this.getString('Lưu thay đổi')}
            </a>
          )}
        </div>
      </div>
    );
  }
}

export default initComponent(MobileUpdateMenuGroupPopup);
