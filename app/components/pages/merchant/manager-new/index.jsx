import * as SEOTYPE from 'settings/seo-types';

import { CUSTOM_CONTENT, UPDATE_AVATAR, open } from 'actions/popup';
import { LINK_DOMAIN, LINK_DOMAIN_LOZI, ROLES_ADMIN } from 'settings/variables';
import { checkCanEditMenu, fetchMerchant } from 'actions/merchant-manager';

import BasePage from 'components/pages/base';
import Footer from 'components/shared/footer';
import { Map } from 'immutable';
import MobileNavMenuTool from 'components/shared/mobile-nav-menutool';
import PropTypes from 'prop-types';
import React from 'react';
import { fetchMerchantLoshipById } from 'actions/merchant';
import { getCurrentUser } from 'reducers/user';
import { getMerchant } from 'reducers/merchant-manager';
import { getMerchantLoship } from 'reducers/merchant';
import { hasRole } from 'utils/access';
import initComponent from 'lib/initComponent';
import { withRouter } from 'react-router-dom';
import SearchDishImagePopup from './popups/dish-image';
import UpdateMenuDishCustomOptionPopup from './popups/dish-custom-option';
import UpdateMenuDishCustomPopup from './popups/dish-custom';
import UpdateMenuDishCustomsPopup from './popups/dish-customs';
import UpdateMenuDishPopup from './popups/dish';
import UpdateMenuGroupPopup from './popups/group';
import UpdateSelectGroupDish from './popups/select-group-dish';
import UpdateSelectQuicklyGroupDish from './popups/select-quickly-group-dish';
import MerchantMenu from './menus';
import MerchantHeader from './menus/header';

if (process.env.BROWSER) {
  require('assets/styles/pages/merchant-manager.scss');
  require('assets/styles/pages/merchant-manager.mobile.scss');
}

class MobileMerchantManagerMenu extends BasePage {
  static contextTypes = { isMobile: PropTypes.bool };
  static childContextTypes = { isAdmin: PropTypes.bool };

  constructor(props, context) {
    super(props, {
      ...context,
      seo: {
        type: SEOTYPE.MERCHANT_MANAGER,
        merchant: props.data,
      },
    });
  }

  static defaultProps = {
    actionCreator: fetchMerchant,
    dataGetter: (state, params) => {
      let data = new Map();
      const merchant = getMerchant(state, params);
      if (merchant) {
        data = data.merge(merchant);

        const merchantLoship = getMerchantLoship(state, { ...params, merchant: merchant.getIn(['data', 'id']) });
        if (merchantLoship) data = data.set('merchantLoship', merchantLoship.get('data'));
      }
      const currentUser = getCurrentUser(state);
      if (currentUser) {
        data = data.set('currentUser', currentUser);
      }

      return data;
    },
  };

  componentDidMount() {
    this.checkValidMerchant(this.props);

    if (this.props.data && !this.props.merchantLoship) {
      this.props.dispatch(
        fetchMerchantLoshipById({ merchant: this.props.data.get('id') }),
      );

      checkCanEditMenu({
        merchant: this.props.data.get('id'),
        callback: () => {
          this.setState({ canEditMenu: true });
        },
        isAdmin: this.isAdmin(),
      });
    }
  }

  componentDidUpdate(prevProps) {
    this.checkValidMerchant(this.props);

    if (!prevProps.merchantLoship && !prevProps.data && this.props.data) {
      this.props.dispatch(
        fetchMerchantLoshipById({ merchant: this.props.data.get('id') }),
      );

      if (!this.state.canEditMenu) {
        checkCanEditMenu({
          merchant: this.props.data.get('id'),
          callback: () => {
            this.setState({ canEditMenu: true });
          },
          isAdmin: this.isAdmin(),
        });
      }
    }
  }

  checkValidMerchant = (props) => {
    if (!props.data) return;

    const { avatar, lat, lng, street, slug, id } = props.data.toObject();
    if (!lat || !lng || !street) {
      return props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('missing_proper_address', 'menuManage'),
          content: (
            <div className="menu-manager-warning">
              <p>
                {this.getString('a_merchant_must_have_an_address_and_geo_location_before_having_a_menu', 'menuManage')}
              </p>
              <p>
                {this.getString('reload_this_page_after_updating_or', 'menuManage')}
                <a onClick={() => window.location.reload()}>{this.getString('click_here', 'menuManage')}</a>
              </p>
              <img src="/dist/images/update-lat-long.png" alt="lat-long-desc" />
            </div>
          ),
          actionText: this.getString('click_to_update_address', 'menuManage'),
          canNotDismiss: true,
          action: () => {
            window.open(`${LINK_DOMAIN_LOZI}/b/${slug}/edit`);
          },
        }),
      );
    }

    if (!avatar) {
      return props.dispatch(
        open(UPDATE_AVATAR, {
          title: this.getString('missing_avatar', 'menuManage'),
          description: this.getString(
            'a_merchant_must_have_an_avatar_picture_for_better_display_on_loship_before_having_a_menu',
            'menuManage',
          ),
          merchantId: id,
        }),
      );
    }
  };

  getMetadata = (props, seoOptions) => {
    const seo = { ...seoOptions };
    seo.merchant = props.data;
    return seo;
  };

  isAdmin = () => {
    const { currentUser } = this.props;
    if (currentUser && currentUser.get('data') && hasRole(currentUser.get('data'), ROLES_ADMIN)) return true;
    return false;
  };

  getChildContext() {
    return { isAdmin: this.isAdmin() };
  }

  isAllowToManage = () => {
    // TODO: GET RID OF THIS BULLSHIT FUNCTION
    // HOW ABOUT A NEW MERCHANT, NO MENU CREATED YET?
    if (!this.props.merchantLoship) return false;
    return true;
  };

  render() {
    const { merchantUsername: username, merchant: slug } = this.getParams();
    const { redirect_url: redirectUrl } = this.getQuery();

    const originDomain = redirectUrl || `${LINK_DOMAIN}/${username || `b/${slug}`}`;

    if (!this.state.canEditMenu) {
      return (
        <div className="page-merchant page-merchant-manager">
          <MobileNavMenuTool
            location={this.props.location}
            history={this.props.history}
            checkEditMenu={this.state.canEditMenu}
            getString={this.getString}
          />
          <div className="container">
            <iframe
              title="Menu manager"
              width="100%"
              scrolling="no"
              frameBorder="0"
              src="https://loship.vn/doi-tac/quan-ly-menu/error-403/"
            />
          </div>
          <Footer location={this.props.location} />
        </div>
      );
    }

    const eaterySlug = this.props.data.get('slug');
    return (
      <div>
        <MobileNavMenuTool
          location={this.props.location}
          history={this.props.history}
          checkEditMenu={this.state.canEditMenu}
          getString={this.getString}
          merchant={eaterySlug}
          originDomain={originDomain}
        />
        <div className="container">
          <div className="page-merchant page-merchant-manager">
            <MerchantHeader data={this.props.data} originDomain={originDomain} />
            <div className="wrap-merchant-menu">
              <MerchantMenu
                checkDataMenu={this.canDataMenu}
                location={this.props.location}
                history={this.props.history}
                closed={this.props.data.get('closed')}
                params={{
                  ...this.getParams(),
                  merchant: eaterySlug,
                  isAdmin: this.isAdmin(),
                }}
                merchant={this.props.data}
                originDomain={originDomain}
              />
            </div>
            <SearchDishImagePopup />
            <UpdateMenuDishCustomsPopup />
            <UpdateMenuDishCustomPopup />
            <UpdateMenuDishCustomOptionPopup />
            <UpdateMenuDishPopup />
            <UpdateMenuGroupPopup />
            <UpdateSelectGroupDish />
            <UpdateSelectQuicklyGroupDish />
          </div>
          <div className="clearfix" />
        </div>
        <Footer location={this.props.location} />
      </div>
    );
  }
}

export default initComponent(MobileMerchantManagerMenu, withRouter);
