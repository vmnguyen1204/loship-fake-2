import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { fetchMerchantMenu, updateDish, updateMenuGroup } from 'actions/merchant-manager';
import { getMerchantMenu } from 'reducers/menu-manager';
import { getCurrentUser } from 'reducers/user';
import {
  CUSTOM_CONTENT,
  MENU_CLONE_DISH,
  MENU_UPDATE_DISH,
  MENU_UPDATE_DISH_CUSTOMS,
  MENU_UPDATE_GROUP,
  close,
  open,
} from 'actions/popup';
import cx from 'classnames';
import { hasRole } from 'utils/access';
import { ROLES_CONTENT } from 'settings/variables';
import { addCommas, removeCommas } from 'utils/format';
import { MIN_MAIN_DISH_PRICE, isGroupDishHasInvalidDish } from 'utils//menu-tool';
import MobileManagerDishGroup from './group';
import EmptyMenu from './_empty';
import PropTypes from 'prop-types';

class MobileMerchantManagerMenu extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  static defaultProps = {
    actionCreator: fetchMerchantMenu,
    dataGetter: (state, props) => {
      let data = getMerchantMenu(state, props);
      if (!data) return null;
      const currentUser = getCurrentUser(state, props);
      if (currentUser) {
        data = data.set('hasRoleContent', hasRole(currentUser.get('data'), ROLES_CONTENT));
      }
      return data;
    },
    customRender: true,
  };

  componentDidUpdate(prevProps) {
    const query = this.getQuery();
    const prevQuery = this.getQuery(prevProps);

    if (prevQuery.showpopup && query.showpopup) {
      this.props.dispatch(close(MENU_UPDATE_GROUP));
      this.props.dispatch(close(MENU_UPDATE_DISH));
      this.props.dispatch(close(MENU_UPDATE_DISH_CUSTOMS));
    }
  }

  goToMenuContent = (key) => {
    const menuContent = document.getElementById('menuContent' + key);
    menuContent.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  openCreateGroupPopup = () => {
    this.handleShowPopup();
    this.props.dispatch(
      open(MENU_UPDATE_GROUP, {
        merchant: this.getParams().merchant,
        isCreate: true,
      }),
    );
  };

  openUpdateGroupPopup = (group) => {
    this.handleShowPopup();
    this.props.dispatch(
      open(MENU_UPDATE_GROUP, {
        merchant: this.getParams().merchant,
        group,
      }),
    );
  };

  openCreateDishPopup = (group) => {
    this.handleShowPopup();
    this.props.dispatch(
      open(MENU_UPDATE_DISH, {
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        groupName: group.get('name'),
        isExtraGroupDish: group.get('isExtraGroupDish'),
        isCreate: true,
        hasRoleContent: this.props.hasRoleContent,
      }),
    );
  };

  openUpdateDishPopup = (group, dish) => {
    this.handleShowPopup();
    this.props.dispatch(
      open(MENU_UPDATE_DISH, {
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        groupName: group.get('name'),
        isExtraGroupDish: group.get('isExtraGroupDish'),
        hasRoleContent: this.props.hasRoleContent,
        dish,
      }),
    );
  };

  openCloneDishPopup = (group, dish) => {
    this.props.dispatch(
      open(MENU_CLONE_DISH, {
        merchant: this.getParams().merchant,
        merchantId: this.props.merchant.get('id'),
        groupDishId: group.get('id'),
        isExtraGroupDish: group.get('isExtraGroupDish'),
        groups: this.props.data.get('group'),
        dish,
      }),
    );
  };

  openUpdateDishCustomPopup = (group, dish) => {
    this.handleShowPopup();
    this.props.dispatch(
      open(MENU_UPDATE_DISH_CUSTOMS, {
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        dish,
        group,
        handleActiveDish: this.handleActiveDish,
        openUpdateDishPopup: this.openUpdateDishPopup,
      }),
    );
  };

  handleShowPopup = () => {
    this.props.history.push(this.props.location.pathname + '?showpopup=true');
  };

  // ACTIVATION

  handleActiveGroup = async (group) => {
    await new Promise((resolve) => this.props.dispatch(
      updateMenuGroup({
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        groupData: group.set('active', !group.get('active')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    // do something
  };

  handleActiveDish = async (group, dish) => {
    const dishImage = dish.get('image');
    if (!dishImage) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('cannot_activate_this_item', 'menuManage'),
          content: () => {
            return (
              <div className="text-left">
                <p>
                  <b>{dish.get('name')}</b>
                  {this.getString('doesnt_have_a_photo_please_pick_one_for_it_before_activating', 'menuManage')}
                </p>
                <p>
                  {this.getString(
                    'an_item_on_the_menu_can_only_be_activated_when_it_has_name_price_and_photo',
                    'menuManage',
                  )}
                </p>
              </div>
            );
          },
        }),
      );
    }
    const isExtraGroupDish = group.get('isExtraGroupDish');
    const price = dish.get('price');
    const additionalFee = dish.get('additionalFee');
    if (
      !dish.get('active') &&
      !isExtraGroupDish &&
      removeCommas(price) + removeCommas(additionalFee) < MIN_MAIN_DISH_PRICE
    ) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('cannot_activate_this_item', 'menuManage'),
          content: () => {
            return (
              <div className="text-left">
                <p>
                  <b>{dish.get('name')}</b>
                  {this.getString(' đang được bán với giá thấp hơn mức giá tối thiểu.', 'menuManage')}
                </p>
                <p>
                  {this.getString('Món chính trên menu phải có giá bán từ {0}đ trở lên.', 'menuManage', [
                    addCommas(MIN_MAIN_DISH_PRICE),
                  ])}
                </p>
              </div>
            );
          },
        }),
      );
    }
    await new Promise((resolve) => this.props.dispatch(
      updateDish({
        dishId: dish.get('id'),
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        dishData: dish.set('active', !dish.get('active')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    // do something
  };

  toggleExtraGroupDish = async (group) => {
    const isExtraGroupDish = group.get('isExtraGroupDish');
    const invalidDish = isGroupDishHasInvalidDish(group.get('dishes'), MIN_MAIN_DISH_PRICE);
    // A bit different from other conditions:
    // At this time, isExtraGroupDish == true means it is being updated to false, so we need to stop the update if an invalid dish is found
    if (isExtraGroupDish && invalidDish !== null) {
      return this.props.dispatch(
        open(CUSTOM_CONTENT, {
          title: this.getString('Không thể đổi thành menu món chính', 'menuManage'),
          content: () => {
            return (
              <div className="text-left">
                <p>
                  <b>{invalidDish.name}</b>
                  {this.getString(' trên menu này đang bán với giá thấp hơn mức tối thiểu.', 'menuManage')}
                </p>
                <p>
                  {this.getString('Món chính trên menu phải có giá bán từ {0}đ trở lên.', 'menuManage', [
                    addCommas(MIN_MAIN_DISH_PRICE),
                  ])}
                </p>
                <p>
                  {this.getString(
                    'Anh chị vui lòng cập nhật giá món trước khi đổi thành menu món chính.',
                    'menuManage',
                  )}
                </p>
              </div>
            );
          },
        }),
      );
    }

    await new Promise((resolve) => this.props.dispatch(
      updateMenuGroup({
        merchant: this.getParams().merchant,
        groupDishId: group.get('id'),
        groupData: group.set('isExtraGroupDish', !group.get('isExtraGroupDish')),
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
  };

  render() {
    const { data, merchant, originDomain } = this.props;
    if (!data) return <div className="mc-menu" />;
    if (data.get('group').size === 0) return <EmptyMenu merchant={this.props.merchant} openCreateGroupPopup={this.openCreateGroupPopup} />;
    return (
      <div className="mcm-menu">
        <div className={cx('mcm-submenu-heading-list')}>
          <div>
            <a key="create" className="btn-create-group text-center" onClick={this.openCreateGroupPopup}>
              <i className="lz lz-plus" />
              {this.getString('create_new_menu_category')}
            </a>
          </div>
        </div>
        <div className="mcm-submenu-content-list">
          {data.get('group') &&
            data.get('group').map((item, key) => {
              return (
                <MobileManagerDishGroup
                  data={item}
                  key={item.get('id')}
                  index={key}
                  getString={this.getString}
                  openUpdateGroupPopup={this.openUpdateGroupPopup}
                  handleActiveGroup={this.handleActiveGroup}
                  toggleExtraGroupDish={this.toggleExtraGroupDish}
                  openCreateDishPopup={this.openCreateDishPopup}
                  handleActiveDish={this.handleActiveDish}
                  hasRoleContent={this.props.hasRoleContent}
                  openCloneDishPopup={this.openCloneDishPopup}
                  openUpdateDishCustomPopup={this.openUpdateDishCustomPopup}
                  openUpdateDishPopup={this.openUpdateDishPopup}
                />
              );
            })}
          <a href={originDomain} className="back-merchant">
            <i className="fa fa-angle-left" />
            &nbsp;
            {this.getString('return_to_0s_homepage', 'menuManage', [merchant.get('name')])}
          </a>
        </div>
      </div>
    );
  }
}

export default initComponent(MobileMerchantManagerMenu);
