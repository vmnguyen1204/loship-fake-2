import AuthByRole from 'components/shared/button/auth-by-role';
import ButtonSwitch from 'components/shared/button/switcher';
import React, { PureComponent } from 'react';
import { ROLES_CONTENT } from 'settings/variables';
import { addCurrency } from 'utils/format';
import Link from 'utils/shims/link';
import DishImage from './_dish-image';

class MobileManagerDishItem extends PureComponent {
  render() {
    const {
      group,
      data: dish,
      index: key,
      getString,
      handleActiveDish,
      hasRoleContent,
      openUpdateDishCustomPopup,
    } = this.props;

    return (
      <tr key={key} className="menu-dish-row">
        <td>
          <div className="dish-item-info">
            <DishImage image={dish.get('image')} />
            <div className="name" onClick={() => openUpdateDishCustomPopup(group, dish)}>
              {dish.get('name')}
            </div>
            {hasRoleContent && [
              <div key="metadata" className="idMenu mobile">
                {`[${dish.get('id')}]`}
                {' - '}
                <Link className="printBarcode" onClick={this.openPageInforBarcode(dish.get('id'))}>
                  {getString('printBarcode')}
                </Link>
              </div>,
              <div key="print" className={`divcontents-${dish.get('id')}`} style={{ display: 'none' }}>
                <div style={{ fontSize: '40px', fontWeight: 'bold' }}>{dish.get('name')}</div>
                <div style={{ fontSize: '10px' }}>
                  {addCurrency(dish.get('price'))}
                  {this.getUnitInPrint()}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{dish.get('id')}
                </div>
              </div>,
              <iframe key="iframe" title="print" id={`ifmcontentstoprint-${dish.get('id')}`} style={{ display: 'none' }} />,
            ]}
            <div className="price">
              {addCurrency(dish.get('price'))}
              <AuthByRole roles={ROLES_CONTENT}>
                {dish.get('additionalFee') > 0 && (
                  <span className="additionalFee"> +{addCurrency(dish.get('additionalFee'))}</span>
                )}
              </AuthByRole>
            </div>
          </div>
        </td>
        <td>
          <div className="dish-item-actions">
            <div className="availability">
              <ButtonSwitch status={dish.get('active')} toggle={() => handleActiveDish(group, dish)} />
            </div>
          </div>
        </td>
      </tr>
    );
  }

  openPageInforBarcode = (id) => () => {
    const content = document.getElementsByClassName(`divcontents-${id}`);
    const print = document.getElementById(`ifmcontentstoprint-${id}`).contentWindow;
    print.document.open();
    print.document.write(
      '<style type="text/css">@media print {@page { margin: 0; } body{ margin: 1.6cm; position: absolute; top: 50%; left: 0; transform: translate(0%, -50%);}</style>' +
        content[0].innerHTML +
        '',
    );
    print.document.close();
    print.focus();
    print.print();
  };

  getUnitInPrint = () => {
    const { data } = this.props;
    const { unit, unitQuantity } = data.toJS();
    let txt = '';
    if (unit.length > 0 && unitQuantity > 0) txt += ' / ' + unitQuantity + ' ' + unit;
    return txt;
  };
}

MobileManagerDishItem.propTypes = {};

export default MobileManagerDishItem;
