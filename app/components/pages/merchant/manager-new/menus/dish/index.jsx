import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DishMobile from './_dish.mobile';

export default class Dish extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <DishMobile {...this.props} />;
  }
}
