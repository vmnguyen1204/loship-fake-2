import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MenuMobile from './_menu.mobile';

export default class Menus extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <MenuMobile {...this.props} />;
  }
}
