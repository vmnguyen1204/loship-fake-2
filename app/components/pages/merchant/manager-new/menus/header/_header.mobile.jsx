import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';

if (process.env.BROWSER) {
  require('assets/styles/pages/merchant-manager.scss');
}

class MobileMerchantHeader extends BaseComponent {
  render() {
    const { data } = this.props;
    const name = data.get('name');

    return (
      <div className="mc-header">
        <div className="mmc-info text-center">{name}</div>
      </div>
    );
  }
}

export default initComponent(MobileMerchantHeader);
