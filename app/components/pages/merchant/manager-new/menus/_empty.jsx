import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import { handle } from 'utils/link';
import callAPI from 'actions/helpers';
import { API as MCM_API, copyMenu } from 'actions/merchant-manager';
import Button from 'components/shared/button';
import PropTypes from 'prop-types';

class MerchantEmptyMenu extends BaseComponent {
  static contextTypes = { isAdmin: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = { error: '', isCopying: false };
  }

  handleCopyMenu = async () => {
    if (this.state.isCopying) return;
    this.setState({ error: '', isCopying: true });
    const txtMerchantUrl = this.refs.txtMerchantUrl;
    const merchantUrl = txtMerchantUrl.value.trim();
    if (!merchantUrl) return;
    const mc = handle(merchantUrl);

    if (mc.type !== 'eatery' && mc.type !== 'user') {
      return this.setState({
        isCopying: false,
        error: 'Link địa điểm không hợp lệ',
      });
    }

    let res = {};
    if (mc.type === 'eatery') {
      res = await callAPI('get', {
        url: MCM_API.MERCHANT,
        params: { merchant: mc.slug },
      });
    } else {
      res = await callAPI('get', {
        url: MCM_API.MERCHANT_BY_USERNAME,
        params: { username: mc.slug },
      });
    }

    if (res.status !== 200 || !res.body) return this.setState({ error: 'Địa điểm không tồn tại!' });

    const sourceMerchant = res.body.data;
    const merchant = this.props.merchant;
    await new Promise((resolve) => this.props.dispatch(
      copyMenu({
        merchant: merchant.get('slug'),
        merchantId: merchant.get('id'),
        sourceMerchantId: sourceMerchant.id,
        callback: resolve,
        isAdmin: this.context.isAdmin,
      }),
    ));
    this.setState({
      error: '',
      isCopying: false,
    });
  };

  render() {
    const merchant = this.props.merchant;
    const isCopying = this.state.isCopying;
    const error = this.state.error;
    return (
      <div className="mc-menu">
        <div className="submenu-empty">
          <h2>{this.getString('0_still_hasnt_had_a_menu_yet', '', [merchant.get('name')])}</h2>
          <p>
            {this.getString('you_can_create_a_new_menu')}
            <a onClick={this.props.openCreateGroupPopup}>nhấn vào đây</a>.
          </p>
          <p>
            <i>{this.getString('or')}</i>
          </p>
          <p>{this.getString('duplicate_it_from_the_old_ones')}</p>
          <p className="text-hint">{this.getString('staffs_only')}</p>
          <div className="copy-menu-form">
            <div className="form">
              <div className="form-group">
                <label>{this.getString('paste_the_link_here')}</label>
                <div className="input">
                  <input ref="txtMerchantUrl" placeholder="VD: https://loship.vn/b/tra-sua-phuc-long" />
                </div>
              </div>
            </div>
            <Button type="feature" disabled={isCopying} onClick={this.handleCopyMenu}>
              {this.getString(isCopying ? 'Đang sao chép...' : 'Sao chép menu')}
            </Button>
            {!!error && <div className="text-error">{error}</div>}
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(MerchantEmptyMenu);
