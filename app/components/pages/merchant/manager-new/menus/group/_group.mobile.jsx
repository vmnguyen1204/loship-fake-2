import Button from 'components/shared/button';
import ButtonSwitch from 'components/shared/button/switcher';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import MobileDishItem from '../dish';

class MobileManagerDishGroup extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,
    displayItemCount: PropTypes.number,
  }

  static defaultProps = { displayItemCount: 25 };

  constructor(props) {
    super(props);
    this.state = { isExpanded: false };
  }

  renderDish = (group, data, key) => {
    return (
      <MobileDishItem
        data={data}
        key={key}
        index={key}
        group={group}
        getString={this.props.getString}
        handleActiveDish={this.props.handleActiveDish}
        hasRoleContent={this.props.hasRoleContent}
        openCloneDishPopup={this.props.openCloneDishPopup}
        openUpdateDishCustomPopup={this.props.openUpdateDishCustomPopup}
        openUpdateDishPopup={this.props.openUpdateDishPopup}
      />
    );
  };

  render() {
    const {
      data: item,
      index: key,
      openUpdateGroupPopup,
      handleActiveGroup,
      openCreateDishPopup,
      displayItemCount,
      getString,
    } = this.props;
    const size = item.get('dishes') ? item.get('dishes').size : 0;
    const isExpanded = this.state.isExpanded;
    return (
      <div>
        <div className="clear" />
        <div className="mc-submenu" key={key}>
          <div className="head-group-dish">
            <h3 id={`menuContent${key}`}>
              {item.get('name')}
              <br />
              {item.get('isExtraGroupDish') && <span className="second-food">{getString('(Món phụ)')}</span>}
            </h3>
            <div className="text-right button-switch">
              <ButtonSwitch status={item.get('active')} toggle={() => handleActiveGroup(item)} />
            </div>
          </div>

          <div>
            <a className="btn-edit-group text-center" onClick={() => openUpdateGroupPopup(item)}>
              <i className="fas fa-pencil-alt" />
              {getString('Chỉnh sửa', 'menuManage')}
            </a>
            <a className="btn-create-dish text-center" onClick={() => openCreateDishPopup(item)}>
              <i className="lz lz-plus" />
              {getString('Thêm món', 'menuManage')}
            </a>
            {item.get('dishes') ? (
              <div className="note-edit-food">{getString('* Nhấn vào tên món để chỉnh sửa và tùy chọn')}</div>
            ) : (
              <div className="note-edit-food">
                <img src="/dist/images/ic_no_data.png" alt="Dish" />
                <p>
                  {getString('Danh mục của bạn hiện đang trống')}
                  <br />
                  {getString('Chọn thêm món để danh mục đa dạng hơn')}
                </p>
              </div>
            )}

            <table className="menu-dish-list">
              <tbody>
                {item.get('dishes') &&
                  item
                    .get('dishes')
                    .slice(0, isExpanded ? size : displayItemCount)
                    .sortBy((d) => d.get('name'))
                    .map((d, k) => this.renderDish(item, d, k))}
              </tbody>
            </table>
            {size > displayItemCount && !isExpanded && (
              <Button fw className="outline-blue text-blue bg-transparent" onClick={this.expandItemList}>
                {getString('see_more_0_1', 'default', [item.get('name'), size - displayItemCount])}
              </Button>
            )}
          </div>
        </div>
      </div>
    );
  }

  expandItemList = () => this.setState({ isExpanded: true });
}

export default MobileManagerDishGroup;
