import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GroupMobile from './_group.mobile';

export default class Group extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <GroupMobile {...this.props} />;
  }
}
