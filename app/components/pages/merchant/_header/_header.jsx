import cx from 'classnames';
import HonoredPartnerIcon from 'components/shared/honored-partner-icon';
import LoshipPartnerBadge from 'components/shared/loship-partner-badge';
import { GmapDistance } from 'components/shared/newsfeed/merchant-item';
import initComponent from 'lib/initComponent';
import QRCode from 'qrcode.react';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { LINK_DOMAIN, LINK_DOMAIN_LOSHIP, LINK_DOMAIN_LOZI, LINK_DOMAIN_MCSTATS } from 'settings/variables';
import Link from 'utils/shims/link';
import HeaderPhotos from './header-photos';
import Metadata from './metadata';
import ShortLinkButton from './short-link-button';
import MerchantHeaderBase from './_header-base';
import LoshipMerchantThoughtful from 'components/shared/loship-merchant-thoughtful';
import { MERCHANT_THOUGHTFUL_POPUP, open } from 'actions/popup';

class MerchantHeader extends MerchantHeaderBase {
  renderHeading = () => {
    const { data } = this.props;
    const { name, isHonored, username, slug } = data.toObject();

    const linkLoship = `${LINK_DOMAIN}/${username || `b/${slug}`}?source=qr_code`;

    return (
      <div className="title">
        <h1 itemProp="name" title={name}>
          {name}
          {isHonored && <HonoredPartnerIcon clickToNavigate getString={this.getString} />}
          <i className={cx('lz', data.get('isFavorite') ? 'lz-heart' : 'lz-heart-o')} onClick={this.handleToggleFavorite} />
        </h1>
        <QRCode className="qr-code" value={linkLoship} renderAs="svg" size={86} />
      </div>
    );
  };

  renderMerchantCover = (data) => {
    return data.map((item, index, self) => {
      return <span key={item.id}>{item.name}{self.length -1 !== index && ', '}</span>;
    });
  }

  renderContent() {
    const { data, params, isAllowedToEditMenu, isAllowedToSeeManager } = this.props;

    const {
      recommendedEnable,
      operatingTime,
      operatingStatus,
      username,
      slug,
      closed,
      isCheckedIn,
      isActive,
      isLoshipPartner,
      address,
      eateryChain,
      categories,
      supplyGroupDishes,
    } = data.toObject();
    const recommendedRatio = ~~(data.get('recommendedRatio') * 1000) / 10;
    const countComment = data.get('comment') - data.get('buyable');

    const linkMerchant = `/${username || `b/${slug}`}`;
    const linkViewMerchantRating = linkMerchant + (params.groupTopic ? `/${params.groupTopic}/danh-gia` : '/danh-gia');

    const backQuery = `?redirect_url=${LINK_DOMAIN}${linkMerchant}`;
    const linkMerchantEdit = `${LINK_DOMAIN_LOZI}/b/${slug}/edit${backQuery}`;
    const linkMerchantEditMenu = `${LINK_DOMAIN_LOSHIP}${linkMerchant}/cap-nhat-menu${backQuery}`;
    const linkManagerEditMenu = `${LINK_DOMAIN_LOSHIP}${linkMerchant}/menu${backQuery}`;
    const linkMerchantManager = `${LINK_DOMAIN_MCSTATS}${linkMerchant}`;

    return (
      <div className="content">
        <div className="d-heading">
          {isLoshipPartner && <LoshipPartnerBadge getString={this.getString} />}
          {supplyGroupDishes?.length > 0 && <span onClick={this.openPopupMerchant}><LoshipMerchantThoughtful getString={this.getString} /></span>}
          {this.renderHeading()}
        </div>

        <div className="d-container">
          {categories?.length > 0 && (
            <div className="categories-wrapper scroll-nice">
              <div className="categories">
                {categories.slice(0, 3).map((category) => (
                  <a key={category.id} className="category" onClick={this.switchToCategoryDetailPage(category)}>
                    {category.value}
                  </a>
                ))}
              </div>
            </div>
          )}
          <div className="row">
            <div className="col">
              <GmapDistance className="col-item d-inline-block mr-8" merchant={data.toObject()} />
              {recommendedEnable && (
                <span className="col-item d-inline-block">
                  <i className="lz lz-fw lz-like" />
                  {this.getString('recommended_0', '', [recommendedRatio])}
                  {countComment > 0 && (
                    <Link to={linkViewMerchantRating}>
                      <b> {this.getString('see_0_reviews', 'default', [countComment])}</b>
                    </Link>
                  )}
                </span>
              )}
              <div className="col-item">
                <Metadata
                  {...{
                    operatingTime,
                    operatingStatus,
                    closed,
                    isActive,
                    isCheckedIn,
                  }}
                />
              </div>
              <div className="col-item">{address}</div>
            </div>
            <div className="col">
              {!closed && isActive && isCheckedIn && <ShortLinkButton params={{ link: `${LINK_DOMAIN}/${username || 'b/' + slug}` }} />}
              {supplyGroupDishes?.length > 0 && (
                <div className="merchant-thoughtful">
                  <i className="lz lz-star-s" />
                  <div className='marchant-cover'>
                    <p>{this.getString('merchant_cover')}: {this.renderMerchantCover(supplyGroupDishes)}</p>
                  </div>
                </div>
              )}
              {eateryChain && (
                <a onClick={this.switchToEateryChainPage} className="col-item link-manager">
                  <i className="fa fa-home" />
                &nbsp;
                  {this.getString('view_eatery_chain_detail', '', [eateryChain.name])}
                </a>
              )}
            </div>
          </div>
          {(isAllowedToEditMenu || isAllowedToSeeManager || data.get('isManager')) && (
            <div className="row">
              <div className="col">
                {isAllowedToEditMenu && (
                  <a href={linkManagerEditMenu} target="_blank" rel="noopener" className="col-item link-manager">
                    <i className="fa fa-edit" />
              &nbsp;
                    {this.getString('menu_management')}
                  </a>
                )}
                {!isAllowedToEditMenu && data.get('isManager') && (
                  <a href={linkMerchantEditMenu} target="_blank" rel="noopener" className="col-item link-manager">
                    <i className="fa fa-edit" />
              &nbsp;
                    {this.getString('menu_management')}
                  </a>
                )}
                {isAllowedToSeeManager && (
                  <a href={linkMerchantEdit} className="col-item link-manager">
                    <i className="lz lz-gear" />
              &nbsp;
                    {this.getString('edit_your_merchant', 'eatery')}
                  </a>
                )}
              </div>
              <div className="col">
                {isAllowedToSeeManager && (
                  <a href={linkMerchantManager} target="_blank" rel="noopener" className="col-item link-manager">
                    <i className="fas fa-chart-line" />
              &nbsp;
                    {this.getString('click_to_see_sale_statistics')}
                  </a>
                )}
                {isAllowedToEditMenu && (
                  <a onClick={this.handleViewShippingDirection} className="col-item link-manager">
                    <i className="fa fa-images" />
              &nbsp;
                    {this.getString('see_merchant_photos')}
                  </a>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }

  render() {
    const { data, params, isAllowedToSeeManager } = this.props;

    return (
      <div className="mc-header">
        <HeaderPhotos params={params} avatar={data.get('avatar')} isAllowedToSeeManager={isAllowedToSeeManager} />
        {this.renderContent()}
      </div>
    );
  }

  openPopupMerchant = () => {
    return this.props.dispatch(
      open(MERCHANT_THOUGHTFUL_POPUP, {
        wrapperClassName: 'layer-toaa',
        eateryId: this.props.data.get('id'),
      }),
    );
  };
}

export default initComponent(MerchantHeader, withRouter);
