import { GENERIC_POPUP, MERCHANT_THOUGHTFUL_POPUP, open } from 'actions/popup';
import cx from 'classnames';
import Button from 'components/shared/button';
import HonoredPartnerIcon from 'components/shared/honored-partner-icon';
import LoshipPartnerBadge from 'components/shared/loship-partner-badge';
import LoshipMerchantThoughtful from 'components/shared/loship-merchant-thoughtful';
import { GmapDistance } from 'components/shared/newsfeed/merchant-item';
import Hint from 'components/statics/hint';
import ImageLazy from 'components/statics/img-lazy';
import initComponent from 'lib/initComponent';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import { LINK_DOMAIN } from 'settings/variables';
import { copyToClipboard } from 'utils/content';
import { addCurrency } from 'utils/format';
import Link from 'utils/shims/link';
import HeaderPhotos from './header-photos';
import Metadata from './metadata';
import MerchantHeaderBase from './_header-base';

class MerchantHeader extends MerchantHeaderBase {
  componentDidMount() {
    this.detectScrolling();
    window.addEventListener('scroll', this.detectScrolling);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.detectScrolling);
  }

  renderNavbar = () => {
    const { data } = this.props;

    return (
      <div
        className={cx('mc-header-nav flex-item justify-between center', { invert: this.state.invertNav })}
      >
        <a onClick={this.goBack}>
          <i className="lz lz-light-arrow-left" />
        </a>
        <span className="name">{!this.state.invertNav && data.get('name')}</span>
        <span className="action">
          <i className={cx('lz', data.get('isFavorite') ? 'lz-heart' : 'lz-heart-o')} onClick={this.handleToggleFavorite} />
          <i className="lz lz-search" onClick={this.toggleSearch} />
        </span>
      </div>
    );
  };

  renderHeading = () => {
    const { data } = this.props;
    const { name, isHonored } = data.toObject();

    return (
      <div className="title">
        <h1 itemProp="name" title={name}>
          {name}
          {isHonored && (
            <HonoredPartnerIcon clickToNavigate getString={this.getString} />
          )}
        </h1>
        <i
          className="lz lz-info-circle"
          onClick={this.handleViewMerchantInfo}
        />
      </div>
    );
  };

  renderContent() {
    const { data, params } = this.props;

    const {
      recommendedEnable,
      operatingTime,
      operatingStatus,
      username,
      slug,
      closed,
      isCheckedIn,
      isActive,
      isLoshipPartner,
      supplyGroupDishes,
      minimumShippingFee,
      currentShippingFee,
      extraFees = [],
      eateryChain,
      categories,
    } = data.toObject();
    const recommendedRatio = ~~(data.get('recommendedRatio') * 1000) / 10;
    const countComment = data.get('comment') - data.get('buyable');

    const linkMerchant = `/${username || `b/${slug}`}`;
    const linkViewMerchantRating = linkMerchant + (params.groupTopic ? `/${params.groupTopic}/danh-gia` : '/danh-gia');
    const hasExtraFees = currentShippingFee > 0 || extraFees.length > 0;

    return (
      <div className="content">
        <div className="heading">
          <div className="partner">
            {isLoshipPartner && <LoshipPartnerBadge getString={this.getString} />}
            {supplyGroupDishes?.length > 0 && <LoshipMerchantThoughtful getString={this.getString} />}
          </div>
          {this.renderHeading()}
        </div>

        <div className="col">
          {categories?.length > 0 && (
            <div className="categories-wrapper">
              <div className="categories">
                {categories.slice(0, 3).map((category) => (
                  <a key={category.id} className="category" onClick={this.switchToCategoryDetailPage(category)}>
                    {category.value}
                  </a>
                ))}
              </div>
            </div>
          )}
          <GmapDistance className="col-item d-inline-block mr-8" merchant={data.toObject()} />
          {recommendedEnable && (
            <span className="col-item d-inline-block">
              <i className="lz lz-fw lz-like" />
              {recommendedRatio}%
              {countComment > 0 && (
                <Link to={linkViewMerchantRating}>
                  <b> {this.getString('see_0_reviews', 'default', [countComment])}</b>
                </Link>
              )}
            </span>
          )}
          <div className="col-item d-inline-block" style={{ width: '100%' }}>
            <Metadata
              {...{
                operatingTime,
                operatingStatus,
                closed,
                isActive,
                isCheckedIn,
              }}
            />
          </div>
        </div>
        {eateryChain && (
          <div className="col eatery-chain" onClick={this.switchToEateryChainPage}>
            <ImageLazy src={eateryChain.avatar} size={50} shape="round-square" />
            <div className="metadata">
              <b className="name">{eateryChain.name}</b>
              <div>{this.getString('0_merchants', 'search', [eateryChain.count.eatery])}</div>
            </div>
            <i className="lz lz-arrow-head-right" />
          </div>
        )}
        {hasExtraFees && (
          <div className="col extra-fees">
            {currentShippingFee > 0 && (
              <div className="col-item">
                <div className="label">
                  {this.getString('shipping_fee', 'eatery')}&nbsp;
                  <Hint
                    text={[
                      `${this.getString('shipping_fee', 'eatery')}: ${currentShippingFee}/km`,
                      this.getString('minimum_0order', 'eatery', [minimumShippingFee]),
                    ]}
                  />
                </div>
                <span itemProp="priceRange">
                  {currentShippingFee}
                  /km
                </span>
              </div>
            )}
            {!!extraFees &&
              extraFees.map((extraFee) => (
                <div key={extraFee.id} className="col-item">
                  <div className="label">
                    {this.getString(extraFee.title, 'extraFee')}
                    :&nbsp;
                    <Hint
                      title={this.getString(extraFee.title, 'extraFee')}
                      text={this.getString(extraFee.description, 'extraFee')}
                    />
                  </div>
                  <span itemProp="priceRange">{addCurrency(extraFee.value, ' đ')}</span>
                </div>
              ))}
          </div>
        )}
      </div>
    );
  }

  render() {
    const { data, params } = this.props;

    return (
      <div className="mc-header">
        <HeaderPhotos params={params} avatar={data.get('avatar')} />
        {this.renderNavbar()}
        {this.renderContent()}
      </div>
    );
  }

  detectScrolling = () => {
    const wrappedElement = document.getElementById('mc-header-photo');
    const isPassedBanner = wrappedElement.getBoundingClientRect().bottom < 44;

    if (!isPassedBanner && !this.state.invertNav) {
      this.setState({ invertNav: true });
    } else if (isPassedBanner && this.state.invertNav) {
      this.setState({ invertNav: false });
    }
  };

  goBack = () => {
    return this.props.history.replace('/');
  };

  renderMerchantCover = (data) => {
    return data.map((item, index, self) => {
      return <span key={item.id}>{item.name}{self.length -1 !== index && ', '}</span>;
    });
  }

  handleViewMerchantInfo = () => {
    const { data } = this.props;
    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('view_merchant_info'),
        content: (
          <div>
            <div className="merchant-info">
              <b>{data.get('name')}</b>
            </div>
            <div className="merchant-info">{data.get('address')}</div>
            {data.get('supplyGroupDishes')?.length > 0 && (
              <div className="merchant-thoughtful" onClick={this.openPopupMerc}>
                <i className="lz lz-star-s" />
                <div className='merchant-cover'>
                  <div className="title">{this.getString('merchant_thoughtful')}</div>
                  <p>{this.getString('merchant_cover')}:{this.renderMerchantCover(data.get('supplyGroupDishes'))}</p>
                </div>
                <i className="fa fa-chevron-right text-gray" />
              </div>
            )}
            <div className="merchant-info link">
              <a className="main">
                <i className="lz lz-link-x" />
                {LINK_DOMAIN + `/${data.get('username') || `b/${data.get('slug')}`}`}
              </a>
              <Button
                className="btn-sm"
                onClick={() => {
                  copyToClipboard(LINK_DOMAIN + `/${data.get('username') || `b/${data.get('slug')}`}`);
                  toast(this.getString('copied'));
                }}
              >
                <i className="far fa-clone" />
                <span>{this.getString('copy')}</span>
              </Button>
            </div>
          </div>
        ),
        className: 'view-merchant-info-popup',
        closeBtnClassName: 'lz-close',
        animationStack: 1,
      }),
    );
  };
  openPopupMerc = () => {
    return this.props.dispatch(
      open(MERCHANT_THOUGHTFUL_POPUP, {
        wrapperClassName: 'layer-toaa',
        eateryId: this.props.data.get('id'),
      }),
    );
  };
}

export default initComponent(MerchantHeader, withRouter);
