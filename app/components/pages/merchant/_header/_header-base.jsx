import { deleteLikeEatery, updateLikeEatery } from 'actions/merchant';
import { GENERIC_POPUP, LOGIN, open } from 'actions/popup';
import BaseComponent from 'components/base';
import MerchantShippingDirection from 'components/pages/merchant/_header/_shipping-direction';
import { Map } from 'immutable';
import qs from 'qs';
import React from 'react';
import { getCurrentCity } from 'reducers/new/metadata';
import { getCurrentUser } from 'reducers/user';
import { APP_INFO } from 'settings/variables';
import { iife } from 'utils/helper';
import loziServices from 'utils/json/loziServices';

if (process.env.BROWSER) {
  require('assets/styles/pages/merchant.scss');
}

class MerchantHeaderBase extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map();

      const currentUser = getCurrentUser(state, params);
      if (currentUser) data = data.set('currentUser',currentUser.get('data'));

      const currentCity = getCurrentCity(state);
      if (currentCity) data = data.set('currentCity', currentCity);

      return data;
    },
    customRender: true,
  };

  state = { invertNav: false };

  toggleSearch = () => {
    typeof this.props.onSearch === 'function' && this.props.onSearch();
  };

  handleViewShippingDirection = () => {
    this.props.dispatch(
      open(GENERIC_POPUP, {
        title: this.getString('see_merchant_photos'),
        content: <MerchantShippingDirection params={this.props.params} />,
        className: 'mc-photos-popup',
        wrapperClassName: 'mc-photos-popup-wrapper',
        animationStack: 2,
      }),
    );
  };

  handleToggleFavorite = () => {
    const { data } = this.props;
    if (!data) return;

    if (!this.props.currentUser) {
      this.props.dispatch(open(LOGIN));
    }

    const isFavorite = !data.get('isFavorite');
    const handler = isFavorite ? updateLikeEatery : deleteLikeEatery;
    this.props.dispatch(handler({ eateryId: data.get('id') }));
  }

  switchToEateryChainPage = () => {
    const { data } = this.props;
    if (!data) return;

    const eateryChain = data.get('eateryChain');
    const eateryChainUrl = eateryChain.username
      ? `/c/${eateryChain.username}`
      : `/cid/${eateryChain.id}`;
    return this.props.history.push(eateryChainUrl);
  }

  switchToCategoryDetailPage = (category) => () => {
    const { data, currentCity } = this.props;
    if (!data) return;

    const serviceName = data.get('shippingService');
    const service = loziServices.find(((s) => s.name === serviceName));
    const cityId = data.get('cityId');

    const baseRoute = iife(() => {
      if (APP_INFO.serviceName === 'lomart') return '/danh-sach-cua-hang-';
      return '/danh-sach-dia-diem-phuc-vu-';
    });
    const query = {};
    if (APP_INFO.serviceName !== serviceName) query.superCategoryId = service?.superCategoryId;
    if (currentCity?.type === 'city' && currentCity.city.id === cityId)
      query.cityId = currentCity.city.id;
    if (currentCity?.type === 'district' && currentCity.district.city.id === cityId)
      query.cityId = currentCity.district.city.id;

    return this.props.history.push(
      `${baseRoute}${category.slug}-giao-tan-noi${qs.stringify(query, { addQueryPrefix: true })}`,
    );
  }
}

export default MerchantHeaderBase;
