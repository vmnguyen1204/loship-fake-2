import { CUSTOM_CONTENT, open } from 'actions/popup';
import { getNextOperatingStatus, getNextOperatingTime } from 'utils/time';

import BaseComponent from 'components/base';
import Hint from 'components/statics/hint';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import initComponent from 'lib/initComponent';
import weekdays from 'utils/json/weekday';
import moment from 'utils/shims/moment';

const OperatingTimeTable = ({ data, getString }) => {
  return (
    <div className="operating-time-table">
      <h3>{getString('operating_time', 'eatery')}</h3>
      {weekdays.map((weekday) => {
        const current = data.filter((item) => item.weekday === weekday.slug);
        return (
          <div key={weekday.slug} className="operating-time-row">
            <div className="operating-time-column header">{getString(weekday.value)}</div>
            <div className="operating-time-column">
              {current.map((item) => (
                <div
                  key={`${weekday.slug}-${moment(item.start, 'hh:mm A').format('HH:mm')}-${moment(item.finish, 'hh:mm A').format('HH:mm')}`} className="operating-time-column-item">
                  {moment(item.start, 'hh:mm A').format('HH:mm')} - {moment(item.finish, 'hh:mm A').format('HH:mm')}
                </div>
              ))}
            </div>
          </div>
        );
      })}
    </div>
  );
};

class MerchantMetadata extends BaseComponent {
  static propTypes = { operatingTime: PropTypes.arrayOf(PropTypes.shape({ start: PropTypes.string, finish: PropTypes.string })) };

  renderClosed() {
    return (
      <li>
        <span className="text-error">
          {this.getString('permanently_closed', 'eatery')}&nbsp;
          <Hint text={this.getString('you_cannot_order_at_this_shop_because_it_is_closed_sorry', 'eatery')} />
        </span>
      </li>
    );
  }

  renderOutOfOrder() {
    return (
      <li>
        <span className="text-warning">
          {this.getString('temporary_not_orders')}
          &nbsp;
          <Hint
            text={this.getString(
              'sorry_this_location_is_currently_out_of_service_and_cannot_take_new_orders_please_come_back_later',
              'eatery',
            )}
          />
        </span>
      </li>
    );
  }

  renderOutOfService() {
    return (
      <li>
        <span className="text-warning">
          {this.getString('temporary_closed', 'eatery')}
          &nbsp;
          <Hint
            text={this.getString(
              'sorry_this_location_is_currently_closed_please_choose_other_locations_or_come_back_later',
              'eatery',
            )}
          />
        </span>
      </li>
    );
  }

  renderOpenTime() {
    const nextOperatingTime = getNextOperatingTime(this.props.operatingTime);
    const { isOpening, openingStatus } = getNextOperatingStatus(this.getString, this.props.operatingStatus);

    if (!nextOperatingTime) return null;
    return (
      <div className="open-time">
        <span className={cx('time-status', { opening: isOpening })}>{openingStatus}</span>
        {!isOpening && (
          <a className="pull-right" onClick={this.showOperatingTime}>
            <b>{this.getString('view_operating_time', 'eatery')}</b>
          </a>
        )}
      </div>
    );
  }

  render() {
    if (this.props.closed) {
      return (
        <div className="metadata">
          <ul>{this.renderClosed()}</ul>
        </div>
      );
    }
    if (!this.props.isActive) {
      return (
        <div className="metadata">
          <ul>{this.renderOutOfService()}</ul>
        </div>
      );
    }
    if (!this.props.isCheckedIn) {
      return (
        <div className="metadata">
          <ul>{this.renderOutOfOrder()}</ul>
        </div>
      );
    }

    return <div className="metadata">{this.renderOpenTime()}</div>;
  }

  showOperatingTime = () => {
    this.props.dispatch(
      open(CUSTOM_CONTENT, { content: <OperatingTimeTable data={this.props.operatingTime} getString={this.getString} /> }),
    );
  };
}

export default initComponent(MerchantMetadata);
