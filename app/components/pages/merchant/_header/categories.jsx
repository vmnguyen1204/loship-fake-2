import React from 'react';
import BaseComponent from 'components/base';

class MerchantCategoryItem extends BaseComponent {
  render() {
    const { value } = this.props;
    if (!value) return false;
    return <span>{value}</span>;
  }
}

class MerchantCategories extends BaseComponent {
  render() {
    const { categories } = this.props;
    return (
      <div className="categories">
        <ul>
          {categories.map((item) => (
            <li key={item}>
              <MerchantCategoryItem value={item} />
            </li>
          ))}
        </ul>
        <span>
          {categories.map((item) => (
            <meta key={item} itemProp="servesCuisine" content={item} />
          ))}
        </span>
      </div>
    );
  }
}

export default MerchantCategories;
