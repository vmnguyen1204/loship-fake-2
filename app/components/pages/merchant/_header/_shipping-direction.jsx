import { CUSTOM_CONTENT, open } from 'actions/popup';

import BaseComponent from 'components/base';
import Carousel from 'components/shared/carousel';
import React from 'react';
import { fetchMerchantManager } from 'actions/merchant';
import { getMerchantManager } from 'reducers/merchant';
import initComponent from 'lib/initComponent';

class MerchantShippingDirection extends BaseComponent {
  static defaultProps = {
    actionCreator: fetchMerchantManager,
    dataGetter: getMerchantManager,
  };

  render() {
    const { photos } = (this.props.data && this.props.data.toJS().shippingDirection) || {};
    if (!photos || !photos.length) {
      return (
        <div className="mc-photos-not-found">
          <img src="/dist/images/not-found-404.png" alt="not-found" />
          <p>{this.getString('merchant_has_no_photo')}</p>
        </div>
      );
    }

    return (
      <div className="mc-photos-list">
        {photos.map((photo) => (
          <div key={photo.name} className="mc-photo" onClick={this.handleOpenPhotoPopup(photo)}>
            <img src={photo.url} alt={photo.name} />
          </div>
        ))}
      </div>
    );
  }

  handleOpenPhotoPopup = (photo) => () => {
    const { photos } = (this.props.data && this.props.data.toJS().shippingDirection) || {};

    this.props.dispatch(
      open(CUSTOM_CONTENT, {
        contentClassName: 'mc-photos-popup slider',
        content: () => (
          <Carousel
            className="mc-photos-slider"
            data={photos.map((cPhoto) => ({ key: cPhoto.name, image: cPhoto.url }))}
            currentSlide={photo.name}
            slideClassName="mc-photo-item"
            slidesToShow={1}
            dot={false}
            centerMode={false}
            autoplay={false}
            imgWidth={Math.min(window.innerWidth - 64, 800)}
            ratio={4 / 3}
            photoZoom
          />
        ),
        wrapperStyle: { zIndex: 201 },
      }),
    );
  };
}

export default initComponent(MerchantShippingDirection);
