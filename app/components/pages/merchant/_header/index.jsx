import React, { Component } from 'react';
import PropTypes from 'prop-types';
import HeaderDesktop from './_header';
import HeaderMobile from './_header.mobile';

export default class Header extends Component {
  static contextTypes = { isMobile: PropTypes.bool };

  render() {
    if (this.context.isMobile) return <HeaderMobile {...this.props} />;
    return <HeaderDesktop {...this.props} />;
  }
}
