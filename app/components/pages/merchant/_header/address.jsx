// import React from 'react';
// import BaseComponent from 'components/base';

// class MerchantAddress extends BaseComponent {
//   render() {
//     const { addressFull, coordinate } = this.props;
//     return (
//       <div className="address">
//         <h2 className="address-text" title={addressFull}>
//           <ul>{this.renderAddress()}</ul>
//           <span itemProp="address" itemScope itemType="http://schema.org/PostalAddress">
//             {this.renderMetaAddress()}
//           </span>
//           <span itemProp="geo" itemScope itemType="http://schema.org/GeoCoordinates">
//             <meta itemProp="latitude" content={coordinate.lat} />
//             <meta itemProp="longitude" content={coordinate.lng} />
//           </span>
//         </h2>
//       </div>
//     );
//   }

//   renderMetaAddress = () => {
//     const { street, district, city } = this.props;

//     const addressArr = [];

//     if (street) {
//       addressArr.push(<meta itemProp="streetAddress" className="address-street" content={street} />);
//     }
//     if (city) {
//       if (district) {
//         addressArr.push(<meta itemProp="addressLocality" content={district} />);
//       }
//       addressArr.push(<meta itemProp="addressRegion" content={city} />);
//     }
//     addressArr.push(<meta itemProp="addressCountry" itemType="http://schema.org/Text" content="Việt Nam" />);

//     return addressArr.map((item, key) => {
//       return React.cloneElement(item, { key });
//     });
//   };

//   renderAddress = () => {
//     const { street, district, city } = this.props;

//     const addressArr = [];
//     if (street) {
//       addressArr.push(<a className="address-street">{street}</a>);
//     }
//     if (city) {
//       if (district) {
//         addressArr.push(<a>{district}</a>);
//       }
//       addressArr.push(<a>{city}</a>);
//     }
//     return addressArr.map((item, key) => {
//       return <li key={key}>{item}</li>;
//     });
//   };
// }

// export default MerchantAddress;
