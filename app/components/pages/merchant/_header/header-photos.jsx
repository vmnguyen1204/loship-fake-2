import BaseComponent from 'components/base';
import ImageLazy from 'components/statics/img-lazy';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { getMerchantMenu } from 'reducers/menu';
import { LINK_DOMAIN_LOZI } from 'settings/variables';
import { genShortId } from 'utils/data';
import { getImageUrl } from 'utils/image';
import Link from 'utils/shims/link';

class HeaderPhotos extends BaseComponent {
  static defaultProps = {
    dataGetter: (state, params) => {
      let data = new Map({ status: 'SUCCESS' });

      const menu = getMerchantMenu(state, params);
      if (menu) data = data.set('menu', menu.get('data'));

      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  constructor(props) {
    super(props);
    this.state = { height: 195 };
  }

  componentDidMount = () => {
    if (!this.context.isMobile) {
      const width = this.refs.photoList.getBoundingClientRect().width / 4;
      this.setState({ height: width });
    }
  };

  render() {
    const { avatar, isAllowedToSeeManager } = this.props;
    const { isMobile } = this.context;
    const params = this.getParams();

    let photos = this.extractPhotoFromMenu();
    if (avatar) photos.unshift({ id: genShortId(), photo: avatar });
    photos = photos.slice(0, 4);

    if (photos.length === 0) {
      photos.push({ id: genShortId(), photo: '/dist/images/vector/merchant-ava.svg' });
    }

    const headerStyle = !isMobile ? { height: `${this.state.height}px` } : {};

    return (
      <div id="mc-header-photo" ref="photoList" className="mc-header-photo" style={headerStyle}>
        <div className="background" style={{ backgroundImage: `url('${getImageUrl(photos[0].photo, 480)}')` }} />
        {!isMobile &&
          photos.map((photo) => (
            <ImageLazy key={photo.id} src={photo.photo} shape="square" placeholder={false} size={200} type="f" />
          ))}

        {isAllowedToSeeManager && (
          <Link
            to={LINK_DOMAIN_LOZI + '/b/' + params.merchant + '/edit'}
            target="_blank"
            className="btn-mc-avatar"
            title={this.getString('update_your_merchants_avatar_on_lozivn', 'eatery')}
          >
            <i className="lz lz-camera" />
          </Link>
        )}
      </div>
    );
  }

  extractPhotoFromMenu = () => {
    const { menu } = this.props;
    if (!menu) return [];
    const photos = [];

    /** use Array.some is a trick. It will stop as soon as photos.length >= 4, no wasted computing */
    menu.get('group').some((g) => {
      g.dishes.some((dish) => {
        if (dish.image) photos.push({ id: genShortId(), photo: dish.image });
        return photos.length >= 4;
      });
      return photos.length >= 4;
    });

    return photos;
  };
}

export default initComponent(HeaderPhotos);
