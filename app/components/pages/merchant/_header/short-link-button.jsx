import React from 'react';
import BaseComponent from 'components/base';
import initComponent from 'lib/initComponent';
import cx from 'classnames';
import request from 'utils/shims/request';
import { DOMAIN_LZI, ROLES_CONTENT } from 'settings/variables';

import ActionButton from 'components/shared/action';

const SHORTLINK = { CREATE: '/short-links' };

export class ShortlinkButton extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      isCopied: false,
      isAlreadyHaveShortLink: false,
      isGetting: false,
      dataShortLink: this.getParams().link,
    };
  }

  copyProfileLink = () => {
    const { dataShortLink } = this.state;
    const tmp = document.createElement('input');
    document.body.appendChild(tmp);
    tmp.value = dataShortLink;
    tmp.select();
    try {
      document.execCommand('copy');
      if (this.copyTimer && !!dataShortLink) {
        window.clearInterval(this.copyTimer);
      }
    } catch (err) {
      console.error('Oops, unable to copy');
    }
    document.body.removeChild(tmp);
    this.setState({ isCopied: true });
  };

  createShortLink = async () => {
    this.setState({
      isCopied: false,
      isGetting: true,
    });
    const params = this.getParams();
    const content = { url: params.link };
    const shortLink = await request
      .post(DOMAIN_LZI + SHORTLINK.CREATE)
      .send(content)
      .end();
    if (shortLink.status === 200) {
      const dataShortLink = JSON.parse(shortLink.text);
      this.setState({
        isAlreadyHaveShortLink: true,
        dataShortLink: dataShortLink.data.shortLink,
        isGetting: false,
      });
    }
  };

  render() {
    const { isAlreadyHaveShortLink, dataShortLink, isGetting } = this.state;

    return (
      <div className="col-item short-link">
        <i className="lz lz-link-x" />
        <span className="link">{dataShortLink}</span>
        <div className="wrap-btn-copy-link">
          <a className={cx('btn-copy-link', { copied: this.state.isCopied })} onClick={this.copyProfileLink}>
            {this.getString(this.state.isCopied ? 'copied' : 'copy link')}
          </a>
          {!isAlreadyHaveShortLink && (
            <ActionButton roles={ROLES_CONTENT}>
              <a
                onClick={() => {
                  this.createShortLink();
                  this.copyTimer = setInterval(this.copyProfileLink, 500);
                }}
                className="btn-get-short-link"
              >
                {this.getString(isGetting ? 'getting' : 'get_short_link')}
              </a>
            </ActionButton>
          )}
        </div>
      </div>
    );
  }
}
export default initComponent(ShortlinkButton);
