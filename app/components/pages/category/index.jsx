import { sendGAEvent } from 'actions/factory';
import BasePage from 'components/pages/base';
import SearchGlobalAddress, { LoshipNotAvailableComponent } from 'components/pages/search/_globalAddress';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getCategories } from 'reducers/category';
import { getCurrentCity, getGlobalAddress } from 'reducers/new/metadata';
import { APP_INFO } from 'settings/variables';
import { iife } from 'utils/helper';
import { getImageUrl } from 'utils/image';
import Link from 'utils/shims/link';

if (process.env.BROWSER) {
  require('assets/styles/pages/categories.scss');
  require('assets/styles/pages/categories-mobile.scss');
}

class Category extends BasePage {
  static defaultProps = {
    dataGetter: (state) => {
      let data = new Map();
      const categories = getCategories(state);
      if (categories) data = data.set('categories', categories.get('data'));

      const currentCity = getCurrentCity(state);
      if (currentCity) data = data.set('currentCity', currentCity);

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      return data;
    },
    customRender: true,
  };

  static contextTypes = { isMobile: PropTypes.bool };

  renderCategory = (dataCategory) => {
    if (!dataCategory) return null;

    const baseRoute = iife(() => {
      if (APP_INFO.serviceName === 'lomart') return '/danh-sach-cua-hang-';
      return '/danh-sach-dia-diem-phuc-vu-';
    });

    return (
      <div className="panel">
        {dataCategory
          .filter((c) => c.order !== -1)
          .map((item) => {
            return (
              <div key={item.id}>
                <Link to={`${baseRoute}${item.slug}-giao-tan-noi`} className="item-menu">
                  {item.value}
                </Link>
              </div>
            );
          })}
      </div>
    );
  };

  checkingGACategories = (item) => {
    sendGAEvent('internal_navigation', 'Action_Tap_Category', item.id);
  };

  renderListItem = (dataCategory) => {
    if (!dataCategory) return false;

    const baseRoute = iife(() => {
      if (APP_INFO.serviceName === 'lomart') return '/danh-sach-cua-hang-';
      return '/danh-sach-dia-diem-phuc-vu-';
    });

    return (
      <div className="section">
        <div className="list-item">
          {dataCategory
            .filter((c) => c.order !== -1)
            .map((item) => {
              return (
                <div key={item.id} className="item">
                  <Link
                    to={`${baseRoute}${item.slug}-giao-tan-noi`}
                    onClick={() => this.checkingGACategories(item)}
                  >
                    <div className="avatar">
                      <div className="avatar-wrapper">
                        <img
                          src={getImageUrl(this.context.isMobile && item.imageMobile || item.imageWeb, 240)}
                          alt="Category"
                          onError={(e) => {
                            e.target.onError = null;
                            e.target.src = '/dist/images/dish-placeholder.png';
                          }}
                        />
                      </div>
                      {this.context.isMobile && (
                        <div>
                          <div className="bg-filter" />
                          <h3 className="info name">{item.value}</h3>
                        </div>
                      )}
                    </div>
                    {!this.context.isMobile && (
                      <div className="meta-data">
                        <h3 className="info name">{item.value}</h3>
                      </div>
                    )}
                  </Link>
                </div>
              );
            })}
        </div>
      </div>
    );
  };

  render() {
    if (!this.props.currentCity) {
      return (
        <div className="page-categories">
          <SearchGlobalAddress />
          <LoshipNotAvailableComponent globalAddress={this.props.globalAddress} />
        </div>
      );
    }

    return (
      <div className="page-categories">
        <div className="container">
          <h3>{this.getString('all_categories')}</h3>
          <div className="page-content">
            <div className="col-left">{this.renderCategory(this.props.categories)}</div>
            <div className="col-right">{this.renderListItem(this.props.categories)}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default initComponent(Category, withRouter);
