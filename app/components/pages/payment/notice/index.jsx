import BasePage from 'components/pages/base';
import { paymentQsManipulation } from 'components/pages/payment';
import Button from 'components/shared/button';
import initComponent from 'lib/initComponent';
import React from 'react';
import { phoneNumber } from 'utils/format';
import { getErrorStr } from 'utils/json/error';

class PaymentPageNotice extends BasePage {
  componentDidMount() {
    this.handlePaymentNotice();
  }

  render() {
    const { errorStr, queryStr } = this.state;
    if (!errorStr) return null;

    return (
      <div>
        <div className="process-item">
          <div>{errorStr}</div>

          <Button
            className="bg-transparent text-blue outline-blue mt-16"
            href={`/payment?${queryStr}`}
          >
            <span>{this.getString('go_to_order_review')}</span>
          </Button>
        </div>
        <div className="process-item info">
          {this.getString('order_failed_contact_info', '', [
            <b key="phone">{phoneNumber('1900638058', { injectZero: false })}</b>,
            <b key="email">hotroloship@lozi.vn</b>,
            <b key="facebook">facebook.com/LoshipVN</b>,
          ])}
        </div>
      </div>
    );
  }

  handlePaymentNotice = () => {
    const { queryStr, partnerCode, errorCode } = paymentQsManipulation(this.props.location.search);

    const errorStr = getErrorStr(errorCode, 'PY_001');
    if (!errorStr) {
      window.location = `/payment?${queryStr}`;
      return;
    }
    this.setState({ errorStr, queryStr, partnerCode });
  };
}

export default initComponent(PaymentPageNotice);
