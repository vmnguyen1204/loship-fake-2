import { sendGAEvent } from 'actions/factory';
import { fetchOrderByAppTransId, fetchPaymentCardByAppTransId } from 'actions/order';
import { LOGIN, open } from 'actions/popup';
import BasePage from 'components/pages/base';
import Button from 'components/shared/button';
import Spinner from 'components/statics/spinner';
import initComponent from 'lib/initComponent';
import qs from 'qs';
import React from 'react';
import { cartClear, getCartMetadataKey } from 'reducers/new/cart';
import { getCurrentUser } from 'reducers/user';
import { APP_INFO } from 'settings/variables';
import loziServices from 'utils/json/loziServices';
import * as cookie from 'utils/shims/cookie';
import { getLoXStorage } from 'utils/shims/localStorage';

if (process.env.BROWSER) {
  require('assets/styles/pages/payment.scss');
}

class PaymentPage extends BasePage {
  static defaultProps = {
    dataGetter: getCurrentUser,
    customRender: true,
  };

  state = { url: '/' };

  componentDidMount() {
    if (!this.isPaymentCallbackPage()) return;
    this.handlePaymentCallback();
  }

  render() {
    if (!this.isPaymentCallbackPage()) {
      return (
        <div className="payment-page">
          <div className="container">
            <div className="logo" style={{ backgroundImage: `url('${'/dist/images/logo.png'}')` }} />
            {this.props.children}
          </div>
        </div>
      );
    }

    const { status, url } = this.state;
    const linkProps = url.includes('http') ? { href: url } : { to: url };
    const currentAgent = process.env.BROWSER && localStorage.getItem('current-agent');

    return (
      <div className="payment-page">
        <div className="container">
          <div className="logo" style={{ backgroundImage: `url('${'/dist/images/logo.png'}')` }} />
          {(() => {
            if (currentAgent === 'web') {
              return (
                <div className="process-item">
                  <Spinner type="wander" />
                  <div>{this.getString('payment_processing')}</div>

                  {status === 'FAILED' && (
                    <Button className="bg-transparent text-blue outline-blue mt-16" {...linkProps}>
                      <span>{this.getString('return_to_homepage')}</span>
                    </Button>
                  )}
                  {status === 'SUCCESS' && (
                    <Button className="bg-transparent text-blue outline-blue mt-16" {...linkProps}>
                      <span>{this.getString('go_to_order_summary')}</span>
                    </Button>
                  )}
                </div>
              );
            }

            return (
              <div className="process-item">
                <Spinner type="wander" />
                <div>{this.getString('payment_processing')}</div>
              </div>
            );
          })()}
        </div>
      </div>
    );
  }

  isPaymentCallbackPage = () => {
    return ['/payment'].includes(this.props.location.pathname);
  };

  handlePaymentCallback = () => {
    const { queryStr, isQueryMutated } = paymentQsManipulation(this.props.location.search);

    if (isQueryMutated) {
      window.location = `${location.pathname}?${queryStr}`;
      return;
    }

    if (!this.props.data) {
      return this.props.dispatch(
        open(LOGIN, {
          canNotDismiss: true,
          callback: () => {
            this.handlePaymentCallback();
          },
        }),
      );
    }

    const { data } = this.props;
    const query = this.getQuery();
    const accessToken = cookie.get('tempData');

    const appTransId = query.appTransId;
    const merchant = localStorage.getItem(`${appTransId}.merchant`);
    const groupTopic = localStorage.getItem(`${appTransId}.groupTopic`) || undefined;
    const userId = parseInt(localStorage.getItem(`${appTransId}.user`));
    const paymentMethod = localStorage.getItem(`${appTransId}.paymentMethod`);
    const redirectUrl = localStorage.getItem(`${appTransId}.redirectUrl`);
    const addCard = localStorage.getItem(`${appTransId}.addCard`);
    const payDebt = localStorage.getItem(`${appTransId}.payDebt`);
    const serviceName = localStorage.getItem(`${appTransId}.service`);
    const service = serviceName &&
      APP_INFO.serviceName !== serviceName &&
      (loziServices.find((s) => s.name === serviceName) || loziServices.find((s) => s.name === 'loship'));

    localStorage.removeItem(`${appTransId}.merchant`);
    localStorage.removeItem(`${appTransId}.groupTopic`);
    localStorage.removeItem(`${appTransId}.user`);
    localStorage.removeItem(`${appTransId}.service`);
    localStorage.removeItem(`${appTransId}.paymentMethod`);
    localStorage.removeItem(`${appTransId}.redirectUrl`);
    localStorage.removeItem(`${appTransId}.tempQuery`);
    localStorage.removeItem(`${appTransId}.addCard`);
    localStorage.removeItem(`${appTransId}.payDebt`);

    const actionCallback = async (res) => {
      // Handle failed
      if (res.status !== 200) {
        const url = (service ? service.domain : '') +
          (() => {
            let confirmOrder = 'step=confirm-order';
            if (addCard) confirmOrder += '&addCard=failed';
            else if (payDebt) confirmOrder += '&payDebt=failed';
            else confirmOrder += `&paymentMethod=${paymentMethod}`;

            if (redirectUrl && !redirectUrl.includes('?')) return `${redirectUrl}?${confirmOrder}`;
            if (redirectUrl && redirectUrl.includes('?')) return `${redirectUrl}&${confirmOrder}`;
            if (merchant) return `/${merchant}${groupTopic ? '/' + groupTopic : ''}?${confirmOrder}`;
            return '/';
          })();

        this.setState({ status: 'FAILED', url });
        if (url !== '/') {
          if (redirectUrl) window.location = url;
          else if (url.includes('http')) window.location = url;
          else this.props.history.replace(url);
        }
        return;
      }

      // Handle success
      const url = (service ? service.domain : '') +
        (() => {
          if (addCard) {
            const confirmOrder = 'step=confirm-order&addCard=success';

            if (redirectUrl && !redirectUrl.includes('?')) return `${redirectUrl}?${confirmOrder}`;
            if (redirectUrl && redirectUrl.includes('?')) return `${redirectUrl}&${confirmOrder}`;
            if (merchant) return `/${merchant}${groupTopic ? '/' + groupTopic : ''}?${confirmOrder}`;
            return '/';
          }

          if (payDebt) {
            const confirmOrder = 'step=confirm-order&payDebt=success';

            if (redirectUrl && !redirectUrl.includes('?')) return `${redirectUrl}?${confirmOrder}`;
            if (redirectUrl && redirectUrl.includes('?')) return `${redirectUrl}&${confirmOrder}`;
            if (merchant) return `/${merchant}${groupTopic ? '/' + groupTopic : ''}?${confirmOrder}`;
            return '/';
          }

          const order = res.body.data || {};
          if (redirectUrl && !redirectUrl.includes('status=re-order')) {
            const success = redirectUrl.includes('?') ? '&status=success' : '?status=success';
            if (order.code) return `${redirectUrl}${success}&code=${order.code}`;
            return `${redirectUrl}${success}`;
          }
          if (data.get('id') === userId && order.code) return `/tai-khoan/don-hang/${order.code}`;
          return '/tai-khoan/don-hang';
        })();

      // Remove local data from Lo-x service
      const storage = await getLoXStorage(serviceName);

      if (addCard) {
        const payment = res.body.data || {};
        await storage.setItem('lastPaymentInfo', JSON.stringify({ method: paymentMethod, card: payment }));
      } else if (payDebt) {
        // Do nothing as user was successfully pay debt
      } else {
        const order = res.body.data || {};
        const eatery = order.eatery || {};
        const taggedUsers = (order.lineGroups || []).map((lineGroup) => lineGroup.taggedUser) || [];
        const payment = {
          method: order.paymentMethod,
          paymentOption: order.paymentOption,
          paymentCard: order.paymentCard,
        };

        this.sendGATracking({
          order,
          sourceOrderId: order.sourceOrderId,
        });
        if (payment.paymentOption === 'create_card_token' && !!payment.paymentCard) {
          await storage.setItem(
            'lastPaymentInfo',
            JSON.stringify({ method: payment.method, card: payment.paymentCard }),
          );
        }
        this.props.dispatch(cartClear({ merchant: eatery.slug, groupTopic, serviceName }));
        this.props.dispatch({ type: 'PROMOTION_CLEAR' });

        // Remove local data as we never use them again
        const cartMetadataKey = getCartMetadataKey({ groupTopic });
        await storage.removeItem(`${cartMetadataKey}`);
        taggedUsers.forEach(async (user) => {
          await storage.removeItem(`${cartMetadataKey}.${user.username}`);
          await storage.removeItem(`${cartMetadataKey}.${user.username}.promo`);
        });

        // Remove local data prepared for some partners
        await storage.removeItem('latestOrderCode');
        await storage.removeItem('latestGroupTopic');
      }

      // storage.disconnect();

      this.setState({ status: 'SUCCESS', url });
      if (url !== '/tai-khoan/don-hang') {
        if (redirectUrl) window.location = url;
        else if (url.includes('http')) window.location = url;
        else this.props.history.replace(url);
      }
    };

    // If have errorCode, we probably don't need to call API
    if (query.errorCode) {
      actionCallback({ status: 0 });
    } else if (addCard) {
      fetchPaymentCardByAppTransId({
        appTransId,
        partnerCode: query.partnerCode,
        accessToken,
        callback: actionCallback,
      });
    } else if (payDebt) {
      actionCallback({ status: 200 });
    } else {
      fetchOrderByAppTransId({
        appTransId,
        partnerCode: query.partnerCode,
        accessToken,
        callback: actionCallback,
      });
    }
  };

  sendGATracking = ({ order, sourceOrderId }) => {
    const { data } = this.props;
    const currentUserId = data && data.get('id');

    if (!currentUserId) return;

    sendGAEvent('loship_order', 'order_success', `U:${currentUserId}/C:${order.code}`);
    sourceOrderId &&
      sendGAEvent('loship_order', 'reorder_success', `U:${currentUserId}/C:${order.code}/S:${sourceOrderId}`);
    fbq('track', 'Purchase', {
      currency: 'VND',
      value: order.totalUserFee,
      extraParameters: { merchantId: order.eateryId, orderCode: order.code, purchaseType: 'order' },
    });
  };
}

export default initComponent(PaymentPage);

export const PROCESS_TIMEOUT = 10000;

export function paymentQsManipulation(queryStr) {
  let query = qs.parse(queryStr, { ignoreQueryPrefix: true });
  const isQueryMutated = (() => {
    let isNotMutated = false;
    // handle tempQuery as some partner does not allow long callbackUrl
    if (query.tmpId) {
      try {
        const tempQuery = qs.parse(localStorage.getItem(`${query.tmpId}.tempQuery`));
        if (tempQuery && typeof tempQuery === 'object') {
          query = { ...query, ...tempQuery };
        }
      } catch (e) {
        console.error('ERROR: cannot parse tempQuery', e);
      }
      delete query.tmpId;
    }

    // zalopay
    if (query.apptransid) {
      query.partnerCode = 'zp';
      query.appTransId = query.apptransid;
    }
    // momo
    else if (query.orderId) {
      query.partnerCode = 'mm';
      query.appTransId = query.orderId;
    }
    // epay
    else if (query.merTrxId) {
      query.partnerCode = 'epay';
      query.appTransId = query.merTrxId;
      if (query.resultCd && query.resultCd !== '00_000') query.errorCode = query.resultCd;
    }
    // viettelpay
    else if (query.billcode) {
      query.partnerCode = 'viettel';
      query.appTransId = query.billcode[0] ?? query.billcode;
      if (query.error_code && query.error_code !== '00') query.errorCode = query.error_code;
    }
    else {
      isNotMutated = true;
    }

    delete query.apptransid;
    delete query.orderId;
    delete query.merTrxId;
    delete query.billcode;
    delete query.resultCd;
    delete query.error_code;

    if (isNotMutated) return false;
    return true;
  })();

  return {
    queryStr: qs.stringify(query),
    isQueryMutated,
    errorCode: query.errorCode,
    partnerCode: query.partnerCode,
  };
}
