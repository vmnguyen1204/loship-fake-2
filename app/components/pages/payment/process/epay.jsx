import callAPI from 'actions/helpers';
import BaseComponent from 'components/base';
import { PROCESS_TIMEOUT } from 'components/pages/payment';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { EPAY_GATEWAY, LINK_DOMAIN } from 'settings/variables';

let openPayment = () => {};
let registerPaymentCancelCallback = () => {};
if (process.env.BROWSER) {
  window.$ = window.jQuery = require('jquery'); // eslint-disable-line no-multi-assign
  openPayment = require('assets/root/megapay').openPayment;
  registerPaymentCancelCallback = require('assets/root/megapay').registerPaymentCancelCallback;

  require('assets/styles/pages/payment.scss');
}

const FETCH_PAYMENT_EPAY_INFO = '/payment/epay-info/apptransid:${appTransId}';
class PaymentProcessEpay extends BaseComponent {
  static contextTypes = { isMobile: PropTypes.bool };

  componentDidMount() {
    this.fetchEpayInfo();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.isLoaded && this.state.isLoaded) {
      this.processPayment();
    }
  }

  renderForm = () => {
    const formData = this.getFormData();

    return (
      <form id="megapayForm" name="megapayForm" method="POST">
        {Object.entries(formData).map(([key, value]) => (
          <input key={key} type="hidden" name={key} value={value} />
        ))}
      </form>
    );
  };

  render() {
    return (
      <div className="process-item">
        {this.state.isLoaded && this.renderForm()}
        <div>{this.getString(this.context.isMobile ? 'payment_transferring_to_partner' : 'payment_processing')}</div>
      </div>
    );
  }

  fetchEpayInfo = async () => {
    const { appTransId } = this.getQuery();
    const callbackParams = qs.stringify({ merTrxId: appTransId });

    this.timeout = setTimeout(() => {
      window.location = `/payment/notice?${callbackParams}&resultCd=PY_004`;
    }, PROCESS_TIMEOUT);

    const res = await callAPI('get', {
      url: FETCH_PAYMENT_EPAY_INFO,
      params: { appTransId },
    });

    if (res.status >= 500) {
      window.location = `/payment/notice?${callbackParams}&resultCd=ER_001`;
      return;
    }
    if (res.status === 403) {
      window.location = `/payment/notice?${callbackParams}&resultCd=AU_002`;
      return;
    }
    if (res.status === 401) {
      window.location = `/payment/notice?${callbackParams}&resultCd=AU_001`;
      return;
    }
    if (res.status !== 200) {
      window.location = `/payment/notice?${callbackParams}&resultCd=PY_002`;
      return;
    }

    const data = res.body.data || {};
    if (data.isPaidLaterOrderTransaction) {
      window.location = `/payment/notice?${callbackParams}`;
      return;
    }

    this.setState({ data });
    this.setClientLoaded();
  };

  getFormData = () => {
    const { data } = this.state;
    const { appTransId } = this.getQuery();
    const callbackParams = qs.stringify({ merTrxId: appTransId });

    localStorage.setItem(`${appTransId}.tempQuery`, `${callbackParams}&resultCd=PY_001`);

    return {
      amount: data.extra.amount,
      bankCode: data.extra.bankCode,
      buyerEmail: data.extra.buyerEmail,
      buyerFirstNm: data.extra.buyerFirstNm,
      buyerLastNm: data.extra.buyerLastNm,
      callBackUrl: LINK_DOMAIN + '/payment/notice',
      cancelUrl: LINK_DOMAIN + `/payment/notice?tmpId=${appTransId}`,
      currency: 'VND',
      description: data.extra.description,
      discountCode: data.extra.discountCode,
      fee: data.extra.fee,
      goodsAmount: data.extra.goodsAmount,
      goodsNm: data.extra.goodsNm,
      invoiceNo: data.extra.invoiceNo,
      merId: data.extra.merId,
      merTrxId: data.extra.merTrxId,
      merchantToken: data.extra.merchantToken,
      notax: data.extra.notax,
      notiUrl: data.extra.notiUrl,
      payCode: data.extra.payCode,
      payOption: data.extra.payOption,
      payToken: data.extra.payToken,
      payType: data.extra.payType,
      reqDomain: LINK_DOMAIN + `/payment/notice?tmpId=${appTransId}`,
      timeStamp: data.extra.timeStamp,
      userFee: data.extra.userFee,
      userIP: data.extra.userIP,
      userId: data.appuser,
      userLanguage: data.extra.userLanguage,
      vat: data.extra.vat,
      windowType: this.context.isMobile ? 1 : 0,
    };
  };

  setClientLoaded = () => {
    this.setState({ isLoaded: true });
    if (this.timeout) clearTimeout(this.timeout);
  };

  processPayment = () => {
    const { appTransId } = this.getQuery();
    const callbackParams = qs.stringify({ merTrxId: appTransId });

    openPayment(1, EPAY_GATEWAY);
    registerPaymentCancelCallback(() => {
      window.location = `/payment/notice?${callbackParams}&resultCd=PY_001`;
    });
  };
}

export default initComponent(PaymentProcessEpay);
