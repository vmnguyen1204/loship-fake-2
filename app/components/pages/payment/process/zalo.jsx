import callAPI from 'actions/helpers';
import BaseComponent from 'components/base';
import { PROCESS_TIMEOUT } from 'components/pages/payment';
import Diacritics from 'diacritic';
import initComponent from 'lib/initComponent';
import qs from 'qs';
import React from 'react';
import { ZALOPAY_GATEWAY } from 'settings/variables';
import { getErrorQs } from 'utils/json/error';

const FETCH_PAYMENT_ZP_INFO = '/payment/zp-info/apptransid:${appTransId}';
class PaymentProcessZalopay extends BaseComponent {
  componentDidMount() {
    this.fetchZpInfo();
  }

  render() {
    return (
      <div className="process-item">
        <div>{this.getString(this.context.isMobile ? 'payment_transferring_to_partner' : 'payment_processing')}</div>
      </div>
    );
  }

  fetchZpInfo = async () => {
    const { appTransId } = this.getQuery();
    const callbackParams = qs.stringify({ apptransid: appTransId });

    this.timeout = setTimeout(() => {
      window.location = `/payment?${callbackParams}&${getErrorQs('PY_004')}`;
    }, PROCESS_TIMEOUT);

    const res = await callAPI('get', {
      url: FETCH_PAYMENT_ZP_INFO,
      params: { appTransId },
    });

    if (res.status >= 500) {
      window.location = `/payment?${callbackParams}&${getErrorQs('ER_001')}`;
      return;
    }
    if (res.status === 403) {
      window.location = `/payment?${callbackParams}&${getErrorQs('AU_002')}`;
      return;
    }
    if (res.status === 401) {
      window.location = `/payment?${callbackParams}&${getErrorQs('AU_001')}`;
      return;
    }
    if (res.status !== 200) {
      window.location = `/payment?${callbackParams}&${getErrorQs('PY_002')}`;
      return;
    }

    const data = res.body.data || {};
    data.description = Diacritics.clean(data.description);

    let _order = JSON.stringify(data);
    _order = encodeURIComponent(_order).replace(/%([0-9A-F]{2})/g, (match, p1) => {
      return String.fromCharCode('0x' + p1);
    });
    _order = Buffer.from(_order).toString('base64');
    _order = encodeURIComponent(_order);

    window.location = `${ZALOPAY_GATEWAY}?${qs.stringify({ order: _order })}`;
  };
}

export default initComponent(PaymentProcessZalopay);
