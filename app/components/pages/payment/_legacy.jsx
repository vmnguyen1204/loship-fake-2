import BaseComponent from 'components/base';
import ERROR from 'utils/json/error';
import qs from 'qs';

class PaymentPageLegacy extends BaseComponent {
  componentDidMount() {
    const query = this.getQuery();
    if (query.errorCode !== 'undefined' && !ERROR[query.errorCode]) delete query.errorCode;

    window.location = `/payment?${qs.stringify(query)}`;
  }

  render() {
    return null;
  }
}

export default PaymentPageLegacy;
