import { fetchItemById } from 'actions/buy';
import { fetchMerchant } from 'actions/merchant';
import { fetchEventById } from 'actions/newsfeed';
import BaseComponent from 'components/base';
import { HomeBannerPopupHandler } from 'components/pages/home/_banner';
import { MerchantItemPopupHandler } from 'components/pages/merchant/_item';
import { MerchantPromoPopupHandler } from 'components/pages/merchant/_promo';
import { ReferralPopupHandler } from 'components/pages/user/profile/_referral';
import OrderCart from 'components/shared/order/cart';
import { OrderPriceDetailHandler } from 'components/shared/order/detail/order-price-detail';
import { OrderCancelPopupHandler } from 'components/shared/order/detail/_cancel';
import OrderDebt from 'components/shared/order/payment/debt-order';
import { Map } from 'immutable';
import initComponent from 'lib/initComponent';
import PropTypes from 'prop-types';
import qs from 'qs';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { getItemById } from 'reducers/buy';
import { getMerchant } from 'reducers/merchant';
import { getCartMetadata } from 'reducers/new/cart';
import { getGlobalAddress } from 'reducers/new/metadata';
import { getEventById } from 'reducers/newsfeed';
import { getCurrentUser } from 'reducers/user';
import { getCityByIdOrNameOrSlug, isCitySlug } from 'utils/data';
import { getRoute } from 'utils/routing';

const isWhitelistNeedFetchGlobalAddress = (isMobile, force) => {
  const { match } = (getRoute(null, globalThis.location?.pathname || globalThis.originalUrl)) || {};
  if (!match) return false;

  if (!isMobile || force) return true;
  if (match.path === '/') return true;
  if (match.path.includes('/cong-dong-loship') && !match.params.ratingId) return true;
  return ['/danh-sach', '/tim-kiem', '/danh-muc', '/videos'].some((path) => {
    return match.path.includes(path);
  });
};

class AppHandler extends BaseComponent {
  static contextTypes = {
    isMobile: PropTypes.bool,
    setGlobalAddressAuto: PropTypes.func.isRequired,
  };

  static defaultProps = {
    dataGetter: (state, params) => {
      const { match } = (typeof window !== 'undefined' && getRoute(null, window.location.pathname)) || {};
      let data = new Map();

      const currentUser = getCurrentUser(state);
      if (currentUser) data = data.set('currentUser', currentUser.get('data'));

      if (params.itemId || params.pItem) {
        params.itemId = params.itemId || params.pItem;
        const currentItem = getItemById(state, params);
        if (currentItem) {
          data = data.set('currentItem', currentItem.get('data'));
          const merchant = currentItem.getIn(['data', 'eatery']) || {};
          params.merchant = merchant.slug;
          params.merchantUsername = merchant.username;
          params.groupTopic = match && match.params.groupTopic;
        }

        if (params.merchant || params.merchantUsername) {
          const merchant = getMerchant(state, params);
          if (merchant) data = data.set('merchant', merchant.get('data'));
        }

        const cartMetadata = getCartMetadata(state, params);
        if (cartMetadata) data = data.set('cartMetadata', cartMetadata);
      }

      if (match && match.params.eventId) {
        params.eventId = match.params.eventId;
        const event = getEventById(state, params);
        if (event) data = data.set('event', event.get('data'));
      }

      const globalAddress = getGlobalAddress(state);
      if (globalAddress) data = data.set('globalAddress', globalAddress.data);

      return data;
    },
    queryParams: ['pItem'],
    customRender: true,
  };

  componentDidMount() {
    this.handleViewItemDetail(this.props, true);
    this.handleViewOrderPriceDetail(this.props, true);
    this.handleViewOrderCancelPopup(this.props, true);
    this.handleViewUserReferral(this.props, true);
    this.handleViewMerchantPromo(this.props, true);
    this.handleViewBannerDetail(this.props, true);
    this.handleFetchGlobalAddress();

    const query = this.getQuery();
    if (query.refresh) {
      const { location: { pathname } } = this.props;
      delete query.refresh;
      this.props.history.replace(`${pathname}${qs.stringify(query, { addQueryPrefix: true })}`);
    }
  }

  componentDidUpdate(prevProps) {
    this.handleViewItemDetail(prevProps);
    this.handleViewOrderPriceDetail(prevProps);
    this.handleViewOrderCancelPopup(prevProps);
    this.handleViewUserReferral(prevProps);
    this.handleViewMerchantPromo(prevProps);
    this.handleViewBannerDetail(prevProps);
    this.handleFetchGlobalAddress();
  }

  render() {
    return (
      <div className="app-handler">
        <OrderCart mode="icon" />
        {!this.context.isMobile && <OrderDebt />}
      </div>
    );
  }

  handleViewItemDetail = (prevProps, isFirstSetup) => {
    const query = this.getQuery();

    const params = this.getParams();
    params.itemId = params.itemId || params.pItem;
    if (!this.props.currentItem) return this.props.dispatch(fetchItemById(params));
    if (!this.props.merchant) {
      const merchant = this.props.currentItem.get('eatery');
      this.props.dispatch(fetchMerchant({ merchantUsername: merchant.username, merchant: merchant.slug }));
    }

    const shouldGoForward = (() => {
      if (this.state.pItem) return false;
      if (isFirstSetup) return true;
      if (!this.props.merchant || !this.props.cartMetadata) return false;
      if (this.props.currentItem && !prevProps.currentItem) return true;
      if (!query.pItem) return false;
      return true;
    })();

    if (shouldGoForward) {
      this.setState({ pItem: true });
      return MerchantItemPopupHandler.call(this, {
        isMobile: this.context.isMobile,
        callback: () => this.setState({ pItem: false }),
      });
    }
  };

  handleViewOrderPriceDetail = (prevProps, isFirstSetup) => {
    const { location: { pathname } } = this.props;
    const query = this.getQuery();

    if (isFirstSetup && query.t) {
      delete query.t;
      this.props.history.push(`${pathname}?${qs.stringify(query)}`);
      return;
    }
    const shouldGoForward = (() => {
      if (this.state.t) return false;
      if (!query.t) return false;
      if (isFirstSetup) return false;
      return true;
    })();
    if (shouldGoForward) {
      this.setState({ t: true });
      return OrderPriceDetailHandler.call(this, {
        isMobile: this.context.isMobile,
        callback: () => this.setState({ t: false }),
      });
    }
  };

  handleViewOrderCancelPopup = (prevProps, isFirstSetup) => {
    const { location: { pathname } } = this.props;
    const query = this.getQuery();

    if (isFirstSetup && query.pCancel) {
      delete query.pCancel;
      this.props.history.push(`${pathname}?${qs.stringify(query)}`);
      return;
    }

    const shouldGoForward = (() => {
      if (this.state.pCancel) return false;
      if (!query.pCancel) return false;
      if (isFirstSetup) return false;
      return true;
    })();

    if (shouldGoForward) {
      this.setState({ pCancel: true });
      return OrderCancelPopupHandler.call(this, {
        isMobile: this.context.isMobile,
        callback: () => this.setState({ pCancel: false }),
      });
    }
  };

  handleViewUserReferral = (prevProps, isFirstSetup) => {
    const {
      currentUser,
      location: { pathname },
    } = this.props;
    const { location: { pathname: prevPathname } } = prevProps;

    const shouldGoForward = (() => {
      if (this.state.pReferral) return false;
      if (!pathname.includes('/tai-khoan/gioi-thieu')) return false;
      if (!currentUser) return false;
      if (!prevPathname.includes('/tai-khoan/gioi-thieu') || isFirstSetup) return true;
      return false;
    })();

    if (shouldGoForward) {
      this.setState({ pReferral: true });
      return ReferralPopupHandler.call(this, {
        isMobile: this.context.isMobile,
        callback: () => this.setState({ pReferral: false }),
      });
    }
  };

  handleViewMerchantPromo = (prevProps, isFirstSetup) => {
    const { location: { pathname } } = this.props;
    const query = this.getQuery();

    if (isFirstSetup && query.pPromo) {
      delete query.pPromo;
      if (query.step === 'confirm-order') delete query.step;
      this.props.history.push(`${pathname}?${qs.stringify(query)}`);
      return;
    }

    const shouldGoForward = (() => {
      if (this.state.pPromo) return false;
      if (!query.pPromo) return false;
      if (isFirstSetup) return false;
      return true;
    })();

    if (shouldGoForward) {
      this.setState({ pPromo: true });
      MerchantPromoPopupHandler.call(this, {
        isMobile: this.context.isMobile,
        callback: () => this.setState({ pPromo: false }),
        isViewingPromoDetail: query.pPromo && !+query.pPromo, // +A like parseInt(A), but return NaN if has any non-digit
      });
    }
  };

  handleViewBannerDetail = (prevProps, isFirstSetup) => {
    const { match } = (typeof window !== 'undefined' && getRoute(null, window.location.pathname)) || {};
    if (!match || !match.params.eventId) return;

    if (!this.props.event) return this.props.dispatch(fetchEventById({ eventId: match.params.eventId }));

    const { location } = this.props;
    const { location: prevLocation } = prevProps;

    const shouldGoForward = (() => {
      if (this.state.pEvent) return false;
      if (isFirstSetup) return true;
      if (!prevProps.event && this.props.event) return true;
      if (location.pathname === prevLocation.pathname) return false;
      return true;
    })();


    if (shouldGoForward) {
      this.setState({ pEvent: true });
      HomeBannerPopupHandler.call(this, {
        isMobile: this.context.isMobile,
        callback: () => this.setState({ pEvent: false }),
      });
    }
  }

  handleFetchGlobalAddress = async () => {
    const { globalAddress, location } = this.props;
    const query = this.getQuery();

    const { match } = (typeof window !== 'undefined' && getRoute(null, window.location.pathname)) || {};
    const citySlug = match?.params.merchantUsername;
    const nextPathname = location.pathname.replace(`/${citySlug}`, '');
    if (isCitySlug(citySlug) && nextPathname !== location.pathname) {
      this.props.history.replace(`${nextPathname}${location.search}`);
    }

    const shouldUseDefault = query.step === 'confirm-order';
    const shouldGoForward = (() => {
      if (this.isAlreadyTriedFetchGlobalAddress) return false;
      if (!isWhitelistNeedFetchGlobalAddress(this.context.isMobile, shouldUseDefault)) return false;
      if (globalAddress?.cityId && globalAddress?.lat && globalAddress?.lng && globalAddress?.address) return false;
      return true;
    })();

    if (shouldGoForward) {
      this.isAlreadyTriedFetchGlobalAddress = true;
      if (isCitySlug(citySlug)) {
        const city = getCityByIdOrNameOrSlug(citySlug);
        this.context.setGlobalAddressAuto({
          cityId: city.type === 'city' ? city.city.id : city.district.city.id,
          districtId: city.type === 'district' ? city.district.id : undefined,
          force: shouldUseDefault,
        });
      } else {
        this.context.setGlobalAddressAuto({ useDefault: shouldUseDefault });
      }
    }
  }
}

export default initComponent(AppHandler, withRouter);
