import App from 'components';
import BuyPage from 'components/pages/buy';
import CategoryPage from 'components/pages/category';
import VideoPage from 'components/pages/discover';
import EateryChainPage from 'components/pages/eatery-chain';
import EateryRatingsPage from 'components/pages/eatery-ratings';
import EateryRatingDetailPage from 'components/pages/eatery-ratings/detail';
import GatewayMomoPage from 'components/pages/gateway/momo';
import GatewayMomoOrderPage from 'components/pages/gateway/momo/order-detail';
import Home from 'components/pages/home';
import MerchantEditPage from 'components/pages/merchant/edit';
import Merchant from 'components/pages/merchant';
import MenuManagerItemPage from 'components/pages/merchant/manager/item-losupply';
import NotFoundPage from 'components/pages/not-found';
import PartnerPage from 'components/pages/partner';
import PartnerAuthPage from 'components/pages/partner/auth';
import PartnerAuthOrderPage from 'components/pages/partner/auth/order';
import PartnerTrialPage from 'components/pages/partner/trial';
import PaymentPage from 'components/pages/payment';
import PaymentNoticePage from 'components/pages/payment/notice';
import PaymentProcessEpayPage from 'components/pages/payment/process/epay';
import PaymentProcessZaloPage from 'components/pages/payment/process/zalo';
import PaymentProcessPageLegacy from 'components/pages/payment/_legacy';
import Search from 'components/pages/search';
import UserPage from 'components/pages/user/profile';
import UserProfileFavoritePage from 'components/pages/user/profile/favorite-eateries';
import UserOrderPage from 'components/pages/user/profile/orders';
import UserProfilePaymentPage from 'components/pages/user/profile/payment';
import UserProfileUpdatePage from 'components/pages/user/profile/update';
import UserOrderDetailPage from 'components/shared/order/detail';
import { APP_INFO } from 'settings/variables';
import { lazyComp } from 'utils/async';
import { isCitySlug } from 'utils/data';

const MenuManagerPage = lazyComp(() => import('components/pages/merchant/manager'));

function getVendorRoutes() {
  if (APP_INFO.serviceName === 'lomart') {
    return [
      {
        path: [
          '/danh-sach-cua-hang-bach-hoa-va-sieu-thi',
          '/danh-sach-cua-hang-quanh-day',
          '/danh-sach-cua-hang-mua-voi-nhom',
          '/danh-sach-cua-hang-tuong-tu-gan-day/:eateryId',
          '/danh-sach-cua-hang{-:dishGroup}-giao-tan-noi{-o-:district}',
          '/danh-sach-cua-hang(-.*)-giao-tan-noi',
          '/danh-sach{-:theme}-giao-tan-noi{-o-:district}',
        ],
        component: Search,
      },
    ];
  }

  return [
    {
      path: '/videos',
      component: VideoPage,
    },
    {
      path: '/payment',
      component: PaymentPage,
      routes: [
        {
          path: '/notice',
          component: PaymentNoticePage,
        },
        {
          path: '/process/zp',
          component: PaymentProcessZaloPage,
        },
        {
          path: '/process/epay',
          component: PaymentProcessEpayPage,
        },
        {
          path: ['/zp', '/mm'],
          component: PaymentProcessPageLegacy,
        },
      ],
    },
    {
      path: '/doi-tac',
      component: PartnerPage,
      routes: [
        {
          path: '/dang-ky',
          component: PartnerTrialPage,
        },
      ],
    },
    {
      path: '/g',
      component: PartnerPage,
      routes: [
        {
          path: '/auth',
          component: PartnerAuthPage,
        },
        {
          path: '/momo',
          component: GatewayMomoPage,
        },
        {
          path: '/momo/orders',
          component: GatewayMomoOrderPage,
        },
        {
          path: '/:partnerName/order',
          component: PartnerAuthOrderPage,
        },
        {
          path: '/:partnerName',
          component: PartnerAuthPage,
        },
      ],
    },
    {
      path: '/m/buy/:blockOfferItemId',
      component: BuyPage,
    },
    {
      path: '/d/buy/:blockOfferItemId',
      component: BuyPage,
    },
    {
      path: '/m/tai-khoan/don-hang/:order',
      component: UserOrderDetailPage,
    },
    {
      path: '/d/tai-khoan/don-hang/:order',
      component: UserOrderDetailPage,
    },
    {
      path: '/b/:merchant/quan-ly',
      component: MenuManagerPage,
    },
    {
      path: '/b/:merchant/menu',
      component: MenuManagerPage,
      routes: [
        {
          path: '/dishes/:dishId',
          component: MenuManagerItemPage,
        },
      ],
    },
    {
      path: '/b/:merchant/cap-nhat-menu',
      component: MenuManagerPage,
    },
    {
      path: '/:merchantUsername/menu',
      component: MenuManagerPage,
      routes: [
        {
          path: '/dishes/:dishId',
          component: MenuManagerItemPage,
        },
      ],
    },
    {
      path: '/:merchantUsername/cap-nhat-menu',
      component: MenuManagerPage,
    },
    {
      path: [
        '/danh-sach-sieu-thi-bach-hoa',
        '/danh-sach-dia-diem-an-vat-giao-tan-noi',
        '/danh-sach-dia-diem-quanh-day',
        '/danh-sach-dia-diem-an-voi-nhom',
        '/danh-sach-dia-diem-tuong-tu-gan-day/:eateryId',
        '/danh-sach-dia-diem{-phong-cach-:theme}{-phuc-vu-:dishGroup}-giao-tan-noi{-o-:district}',
        '/danh-sach-dia-diem(-.*)-giao-tan-noi',
      ],
      component: Search,
    },
  ];
}

const routes = [
  {
    path: '/',
    component: App,
    routes: [
      {
        // ! path array order is intended. Do not try to rearrange this
        path: ['/su-kien/:eventId', '/'],
        exact: true,
        component: Home,
      },
      {
        path: '/tai-khoan',
        component: UserPage,
        routes: [
          {
            path: '/don-hang',
            component: UserOrderPage,
            routes: [
              {
                path: '/:order',
                component: UserOrderDetailPage,
              },
            ],
          },
          {
            path: '/cap-nhat',
            component: UserProfileUpdatePage,
          },
          {
            path: '/quan-ly-the',
            component: UserProfilePaymentPage,
          },
          {
            path: '/cua-hang-yeu-thich',
            component: UserProfileFavoritePage,
          },
        ],
      },
      {
        path: ['/c/:username', '/cid/:id'],
        component: EateryChainPage,
      },
      {
        path: '/danh-muc',
        component: CategoryPage,
      },
      {
        path: '/tim-kiem',
        component: Search,
      },
      {
        path: '/khong-tim-thay',
        component: NotFoundPage,
      },
      {
        path: '/cong-dong-loship',
        component: EateryRatingsPage,
        routes: [
          {
            path: '/:ratingId',
            component: EateryRatingDetailPage,
          },
        ],
      },
      {
        path: '/b/create',
        component: MerchantEditPage,
        exact: true,
      },
      {
        path: '/b/:merchant/edit',
        component: MerchantEditPage,
        exact: true,
      },
      /** VENDOR ROUTES
       * ! Do NOT rearrange these sections to upper or lower, as it might break.
       */
      ...getVendorRoutes(),
      /** VENDOR ROUTES - END */
      {
        path: [
          '/items/:shareId',
          '/b/:merchant/:groupTopic?{/danh-gia}',
          '/i/:eateryId/:groupTopic?{/danh-gia}',
          '/:merchantUsername/:groupTopic?{/danh-gia}',
        ],
        resolver(match) {
          /** BACKWARD COMPATIBLE */
          const citySlug = match?.params.merchantUsername;
          if (isCitySlug(citySlug)) return null;
          return Merchant;
        },
      },
    ],
  },
];

export default routes;
