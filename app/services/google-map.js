let _instance = null;

class GoogleMapSDK {
  constructor(google) {
    this.google = google;
    this.directionsDisplay = new this.google.maps.DirectionsRenderer();
    this.autocompleteService = new this.google.maps.places.AutocompleteService();
    this.directionsService = new this.google.maps.DirectionsService();
    this.geocoder = new this.google.maps.Geocoder();
    this.distanceSvc = new this.google.maps.DistanceMatrixService();
  }

  addListener = (ref, event, cb) => {
    this.google.maps.event.addListener(ref, event, cb);
  };

  addListenerOnce = (ref, event, cb) => {
    this.google.maps.event.addListenerOnce(ref, event, cb);
  };

  createGoogleMap = (ref, options) => {
    return new this.google.maps.Map(ref, options);
  };

  createMarker = (options) => {
    return new this.google.maps.Marker(options);
  };

  createInfoWindow = (options) => {
    return new this.google.maps.InfoWindow(options);
  };

  createIcon = (options) => {
    const { size = 8 } = options;

    return new this.google.maps.Icon({
      origin: null,
      anchor: null,
      ...options,
      size: new this.google.maps.Size(size, size),
      scaleSize: new this.google.maps.Size(size, size),
    });
  };

  createLatLng = (lat, lng) => {
    return new this.google.maps.LatLng(lat, lng);
  };

  createLatLngBounds = () => {
    return new this.google.maps.LatLngBounds();
  };

  createPoint = (x, y) => {
    return new this.google.maps.Point(x, y);
  }

  getAutocomplete = (text) => {
    const sessionToken = new this.google.maps.places.AutocompleteSessionToken();
    return new Promise((resolve, reject) => {
      this.autocompleteService.getQueryPredictions(
        { input: text, sessionToken },
        (predictions, status) => {
          if (status === 'OK') {
            resolve(predictions);
          } else reject(status);
        },
      );
    });
  };

  attachAutocomplete = (ref) => {
    const _addEventListener = ref.addEventListener ? ref.addEventListener : ref.attachEvent;
    function addEventListenerWrapper(type, listener) {
      // to let user choose first item when hit enter
      let overrideListener = listener;
      if (type === 'keydown') {
        const origListener = listener;
        overrideListener = function (event) {
          const suggestionSelected = document.querySelector('.pac-item-selected');
          if (event.which === 13 && !suggestionSelected) {
            const keydownEvent = new Event('keydown');
            keydownEvent.keyCode = 40;
            keydownEvent.which = keydownEvent.keyCode;
            origListener.apply(ref, [keydownEvent]);
          }
          origListener.apply(ref, [event]);
        };
      }

      // HACK BLUR
      // to let user choose first item when hit blur
      // if (type == "blur") {
      //   var origListener = listener;
      //   listener = function (event) {
      //     var suggestionSelected = document.querySelector('.pac-item-selected');
      //     if (!suggestionSelected) {
      //       var keydownEvent = new Event("keydown");
      //       keydownEvent.keyCode = 40;
      //       keydownEvent.which = keydownEvent.keyCode;
      //       ref.dispatchEvent(keydownEvent);

      //       keydownEvent = new Event("keydown");
      //       keydownEvent.keyCode = 13;
      //       keydownEvent.which = keydownEvent.keyCode;
      //       ref.dispatchEvent(keydownEvent);
      //     }
      //     origListener.apply(ref, [event]);
      //   };
      // }
      _addEventListener.apply(ref, [type, overrideListener]);
    }
    ref.addEventListener = addEventListenerWrapper;
    ref.attachEvent = addEventListenerWrapper;

    return new this.google.maps.places.Autocomplete(ref);
  };

  getRoute = (options) => {
    return new Promise((resolve, reject) => {
      this.directionsService.route(options, (res) => {
        if (res && res.status === 'OK') {
          resolve(res);
        } else reject(status);
      });
    });
  };

  renderRoute = (map, route) => {
    this.directionsDisplay.setMap(map);
    this.directionsDisplay.setDirections(route);
  };

  clearRoute = () => {
    this.directionsDisplay.setMap(null);
  };

  getDistanceMatrix = (options) => {
    return new Promise((resolve, reject) => {
      this.distanceSvc.getDistanceMatrix(options, (res, status) => {
        if (status === 'OK') resolve(res);
        else reject(status);
      });
    });
  };

  getAddressComponent = (addressComponents, type) => {
    const ac = addressComponents.find((comp) => {
      return comp.types.findIndex((t) => t === type) !== -1;
    });
    if (ac) return ac.short_name;
    return null;
  };

  getFullAddress = (options) => {
    return new Promise((resolve, reject) => {
      if (!options.componentRestrictions) options.componentRestrictions = {};

      this.geocoder.geocode(options, (res, status) => {
        if (status === 'OK') {
          resolve(
            res.map((route) => {
              const district = this.getAddressComponent(route.address_components, 'administrative_area_level_2');
              const city = this.getAddressComponent(route.address_components, 'administrative_area_level_1');
              const streetNumber = this.getAddressComponent(route.address_components, 'street_number');
              const street = this.getAddressComponent(route.address_components, 'route');

              let geometry = null;
              if (route.geometry && route.geometry.location) {
                geometry = {
                  location: {
                    lng: route.geometry.location.lng,
                    lat: route.geometry.location.lat,
                  },
                };
              }
              return {
                formattedAddress: route.formatted_address,
                addressComponents: route.address_components,
                geometry,
                placeId: route.place_id,
                city,
                district,
                streetNumber,
                street,
              };
            }),
          );
        }
        reject();
      });
    });
  };
}

let injectMapSDKPromise = null;

const injectMapSDK = () => {
  if (!injectMapSDKPromise) {
    injectMapSDKPromise = new Promise((resolve) => {
      if (!window.google || !window.google.maps) {
        window.injectMapSDKCallBack = () => {
          delete window.injectMapSDKCallBack;
          resolve();
        };
        const scriptElement = document.createElement('script');
        scriptElement.type = 'text/javascript';
        scriptElement.async = true;
        scriptElement.src = `https://maps.googleapis.com/maps/api/js?key=${window.__MAP_KEY__}&libraries=places&language=vi&region=VN&callback=injectMapSDKCallBack`;
        document.body.appendChild(scriptElement);
      } else {
        resolve();
      }
    });
  }
  return injectMapSDKPromise;
};

export const getGoogleMap = async () => {
  await injectMapSDK();
  if (!window.google || !window.google.maps) throw new Error('Cannot inject Google Map SDK!');
  if (!_instance) _instance = new GoogleMapSDK(window.google);
  return _instance;
};
