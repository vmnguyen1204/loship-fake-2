module.exports = {
  files: ['./icons/*.svg'],
  fontName: 'LoziFonts',
  classPrefix: 'lz-',
  baseSelector: '.lz',
  types: ['eot', 'woff', 'ttf', 'svg'],
  embed: true,
  fileName: 'LoziFonts.[ext]',
};
