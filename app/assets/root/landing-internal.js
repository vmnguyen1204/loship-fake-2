import cookie from 'js-cookie';

window.onload = function () {
  cskhSuggestMerchantHandler();
  cskhSuggestFeatureHandler();
  cskhSuggestLosupplyHandler();
  authAppleIdHandler();
  communityDriverHandler();
  merchantLandingMenuToolHandler();
};

/** SECTION HELPER */
function sendGAEvent(eventCategory, eventAction, eventLabel) {
  if (!window || !window.gtag) return;
  window.gtag('event', eventAction, {
    event_category: eventCategory,
    event_label: eventLabel,
  });
}

function getDevicePlatform() {
  if (!!window && !!navigator) {
    let platform;
    if (navigator.userAgent.match(/iPod|iPhone|iPad/) && !navigator.userAgent.match(/IEMobile/)) {
      platform = 'ios';
    }
    if (navigator.userAgent.match(/Android/)) {
      platform = 'android';
    }

    return platform;
  }
}

function copyToClipboard(str) {
  const el = document.createElement('textarea');
  el.value = str;

  // make element outside of viewport
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';

  // clipboard manipulating
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
}

/** SECTION HELPER - END */

function cskhSuggestMerchantHandler() {
  if (window.location.pathname !== '/cskh/de-xuat-cua-hang') return;

  const cpnElem = document.getElementById('cpn');
  sendGAEvent('loship_user_contribute', 'ACTION_CONTRIBUTE_MERCHANT', `P:${cpnElem.value}/T:merchant`);
}

function cskhSuggestFeatureHandler() {
  if (window.location.pathname !== '/cskh/de-xuat-tinh-nang') return;

  const cpnElem = document.getElementById('cpn');
  sendGAEvent('loship_user_contribute', 'ACTION_CONTRIBUTE_FEATURE', `P:${cpnElem.value}/T:feature`);
}

function cskhSuggestLosupplyHandler() {
  if (window.location.pathname !== '/cskh/de-xuat-losupply') return;

  const cpnElem = document.getElementById('cpn');
  sendGAEvent('loship_merchant_contribute', 'ACTION_CONTRIBUTE_LOSUPPLY', `P:${cpnElem.value}/T:losupply`);
}

function authAppleIdHandler() {
  if (window.location.pathname !== '/auth/apple-id') return;

  try {
    const customData = JSON.parse(window.__CUSTOM_DATA__);
    const query = Object.keys(customData).map((key) => `${key}=${customData[key]}`).join('&');
    const callbackUrl = `/auth/apple-id/callback?${query}`;

    // if (!customData.sub) throw new Error('Auth token not invalid');

    // let callbackUrl = `/auth/apple-id/callback?sub=${customData.sub}`;
    // if (customData.email) callbackUrl += `&email=${customData.email}`;
    // if (customData.email_verified) callbackUrl += `&email_verified=${customData.email_verified}`;
    // if (customData.is_private_email) callbackUrl += `&is_private_email=${customData.is_private_email}`;

    window.location.replace(callbackUrl);
  } catch (e) {
    window.location.replace('/auth/apple-id/callback?errorCode=AU_001');
  }
}

function communityDriverHandler() {
  const stickyBanner = document.querySelector('.banner-sticky');
  const { iosUrl, androidUrl } = stickyBanner && stickyBanner.dataset || {};
  const btnLanguage = document.querySelectorAll('.change-language');

  const platform = getDevicePlatform();

  let redirectTimeout;
  window.addEventListener('blur', function() {
    if (redirectTimeout) clearTimeout(redirectTimeout);
  });

  function handleOpenApp() {
    if (platform === 'ios' && iosUrl) {
      window.location = iosUrl;
    } else if (platform === 'android' && androidUrl) {
      window.location = androidUrl;
    }

    redirectTimeout = setTimeout(() => {
      if (!document.hidden) {
        window.location = 'https://chienbinh.loship.vn/use-app';
      }
    }, 2000);
  }

  (function handleBtnlike(){
    const btnClap = document.querySelector('.btn-clap');
    const poupWrapper = document.querySelector('.popup-wrapper');
    const popup = document.querySelector('.popup-like');
    const btnBackLike = document.querySelector('.popup-like .btn-back');
    const popupBg = document.querySelector('.popup-clap .popup-bg');

    function onCloseLike() {
      document.body.style.overflow = '';
      popup.classList.remove('active');
      setTimeout(() => poupWrapper.classList.remove('show'));
    }

    if (btnClap) {
      btnClap.onclick = function() {
        if (!['android', 'ios'].includes(platform)) {
          poupWrapper.classList.add('show');
          return;
        }
        handleOpenApp();
      };
    }

    if (popupBg) popupBg.onclick = onCloseLike;
    if (btnClap) btnBackLike.onclick = onCloseLike;
  }());

  (function handleBtnshare(){
    const btnShare = document.querySelector('.btn-share');
    const poupWrapper = document.querySelector('.popup-wrapper.popup-link');
    const popup = document.querySelector('.popup-share');
    const btnBack = document.querySelector('.popup-share .btn-back');
    const popupBg = document.querySelector('.popup-link .popup-bg');

    const sharingUrlElem = document.querySelector('.popup-link .input-link-value');
    const btnCopy = document.querySelector('.popup-link .btn-copy');

    function onCloseLike() {
      document.body.style.overflow = '';
      popup.classList.remove('active');
      setTimeout(() => poupWrapper.classList.remove('show'));
    }

    if (btnShare) {
      btnShare.onclick = function() {
        poupWrapper.classList.add('show');
      };
    }

    if (popupBg) popupBg.onclick = onCloseLike;
    if (btnBack) btnBack.onclick = onCloseLike;
    if (btnCopy) btnCopy.onclick = function() {
      const { sharingUrl } = sharingUrlElem && sharingUrlElem.dataset || {};
      copyToClipboard(sharingUrl);
    };
  }());

  (function handleStickyBanner() {

    const btnClose = document.querySelector('.banner-sticky > .close');
    const btnOpenApp = document.querySelector('.banner-sticky > .btn-open-app');

    if (btnClose) btnClose.onclick = function() {
      stickyBanner.parentNode.removeChild(stickyBanner);
    };
    if (btnOpenApp) btnOpenApp.onclick = handleOpenApp;
  })();

  (function handleChangeLanguages() {

    if (!btnLanguage) return;
    for (let i = 0; i < btnLanguage.length; i++) {
      btnLanguage[i].onclick = () => {
        const { change } = btnLanguage[i].dataset;
        cookie.set('locale', change || 'vi_VN');
        return location.reload();
      };
    }
  })();
}

function merchantLandingMenuToolHandler() {
  const stickyBanner = document.querySelector('.banner-sticky');
  const { iosUrl, androidUrl } = stickyBanner && stickyBanner.dataset || {};
  const platform = getDevicePlatform();

  let redirectTimeout;
  window.addEventListener('blur', function() {
    if (redirectTimeout) clearTimeout(redirectTimeout);
  });

  function handleOpenApp() {
    if (platform === 'ios' && iosUrl) {
      window.location = iosUrl;
    } else if (platform === 'android' && androidUrl) {
      window.location = androidUrl;
    }

    redirectTimeout = setTimeout(() => {
      if (!document.hidden) {
        window.location = 'https://mc.loship.vn/use-app';
      }
    }, 2000);
  }
  (function handleStickyBanner() {
    const btnClose = document.querySelector('.banner-sticky > .close');
    const btnOpenApp = document.querySelector('.banner-sticky > .btn-open-app');

    if (btnClose) btnClose.onclick = function() {
      stickyBanner.parentNode.removeChild(stickyBanner);
    };
    if (btnOpenApp) btnOpenApp.onclick = handleOpenApp;
  })();
}
