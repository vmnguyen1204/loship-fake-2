/* eslint-disable */
var element = '';

function payment_open(el) {
  // console.log(el);
  element = el;
  var payment_box = $('#' + el);
  var payment_bg = payment_box.prev().hasClass('payment_bg'); //dimmed 레이어를 감지하기 위한 boolean 변수

  if (payment_bg) {
    $('.payment_layer').fadeIn(); //'bg' 클래스가 존재하면 레이어가 나타나고 배경은 dimmed 된다.
  } else {
    payment_box.fadeIn();
  }

  // 화면의 중앙에 레이어를 띄운다.
  if (payment_box.outerHeight() < $(document).height())
    payment_box.css('margin-top', '-' + payment_box.outerHeight() / 2 + 'px');
  else payment_box.css('top', '0px');
  if (payment_box.outerWidth() < $(document).width())
    payment_box.css('margin-left', '-' + payment_box.outerWidth() / 2 + 'px');
  else payment_box.css('left', '0px');
}

function closeLayer() {
  $('.payment_layer').fadeOut();
}

export function openPayment(sel, domain) {
  //	var url;
  //var paymentForm = document.getElementById('requestForm');
  var paymentForm;

  if (sel == '1') {
    paymentForm = document.getElementById('megapayForm');
    //HuanNM: description field is mandatory
    var desc = paymentForm.elements['description'];
    if (desc == null || desc.value == '' || desc.value == null) {
      alert('description should not be empty or undefined. please check this field in your form');
      return;
    }
    //
  } else {
    paymentForm = document.getElementById('requestForm2');
    paymentForm.elements['version'].value = 'true';
  }

  var windowType = paymentForm.elements['windowType'].value;
  var payType = paymentForm.elements['payType'].value;

  // console.log('windowType = ' + windowType);
  // console.log('payType = ' + payType);

  if (windowType != null && windowType != '') {
    // console.log('Have windowType');
    if (windowType == '0') {
      $('#megapayForm').attr('action', domain + '/pg_was/order/init.do');
      init(paymentForm);
      $('#megapayForm').submit();
    } else if (windowType == '1') {
      if (payType == 'NO') {
        $('#megapayForm').attr('action', domain + '/pg_was/order/Minit.do');
      } else {
        $('#megapayForm').attr('action', domain + '/pg_was/order/init.do');
      }

      $('#megapayForm').submit();
    }
  } else {
    // console.log('None windowType');

    var filter = 'win16|win32|win64|mac|macintel';

    if (navigator.platform) {
      if (0 > filter.indexOf(navigator.platform.toLowerCase())) {
        if (payType == 'NO') {
          $('#megapayForm').attr('action', domain + '/pg_was/order/Minit.do');
        } else {
          $('#megapayForm').attr('action', domain + '/pg_was/order/init.do');
        }

        $('#megapayForm').submit();
      } else {
        $('#megapayForm').attr('action', domain + '/pg_was/order/init.do');
        init(paymentForm);
        $('#megapayForm').submit();
      }
    }
  }
  return false;
}

function init(paymentForm) {
  var div_paymentLayer = document.getElementById('paymentLayer');
  if (div_paymentLayer == null || div_paymentLayer == undefined) {
    var temp = document.createElement('div');

    let innerData = '<div id="paymentLayer" class="payment_layer">';
    innerData += '<div class="payment_bg"></div>';
    innerData += '<div id="payment_box" class="payment_pop_layer">';
    innerData += '<div class="payment_pop_container">';
    innerData += '<div class="payment_pop_conts">';
    innerData +=
      '<iframe name="paymentF" id="paymentF" width="100%" height="100%" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" style="background-image: url(../images/progress.gif); background-repeat: no-repeat; background-position: center center;" ></iframe>';
    innerData += '</div>';
    innerData += '</div>';
    innerData += '</div>';
    innerData += '</div>';

    temp.innerHTML = innerData;
    paymentForm.appendChild(temp);
  }
  payment_open('payment_box');

  $('#paymentF').attr('src', $('#megapayForm').attr('action'));
  $('#megapayForm').attr('target', 'paymentF');
}

function deleteToken() {
  var deleteForm = document.deleteTokenForm;
  if (
    (deleteForm.token.value != '' && deleteForm.token.value != null) ||
    (deleteForm.merId.value != '' && deleteForm.merId.value != null)
  ) {
    deleteForm.submit();
  } else {
    return;
  }
}

function inquiryTrans() {
  var inquiryForm = document.inqTransForm;
  if (
    (inquiryForm.trxId.value != '' && inquiryForm.trxId.value != null) ||
    (inquiryForm.merId.value != '' && inquiryForm.merId.value != null)
  ) {
    inquiryForm.submit();
  } else {
    return;
  }
}

export function registerPaymentCancelCallback(callback) {
  window.callback = callback;
}

window.addEventListener('message', function(e) {
  // console.log('Sample Listener');
  if (e.data.closeLayer == 'close') {
    //window.top.closeLayer();
    closeLayer();
    typeof window.callback === 'function' && this.window.callback();
  }
});
