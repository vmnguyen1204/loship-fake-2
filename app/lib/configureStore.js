import * as api from 'lib/api';

import { List, Map } from 'immutable';

import { MQTTMiddleware } from 'lib/mqtt';
import apiMiddleware from 'lib/apiMiddleware';
import { configureStore } from '@reduxjs/toolkit';
import rootReducer from 'reducers';
import thunkMiddleware from 'redux-thunk';

const initialState = (() => {
  if (typeof window === 'undefined') return {};

  global.availableAreas = (() => {
    try {
      return JSON.parse(window.__AVAILABLE_AREAS__) || [];
    } catch (e) {
      return [];
    }
  })();
  try {
    return JSON.parse(window.__INITIAL_STATE__) || {};
  } catch (e) {
    return {};
  }
})();

// Transform into Immutable.js collections,
// but leave top level keys untouched for Redux
Object.keys(initialState).forEach((key) => {
  if (['cart', 'eatery', 'metadataNew', 'map'].includes(key)) return;

  initialState[key] = new Map(
    Object.entries(initialState[key]).reduce((res, [pKey, pValue]) => {
      if (typeof pValue !== 'object') res[pKey] = pValue;
      else if (Array.isArray(pValue.data)) {
        res[pKey] = new Map({ ...pValue, data: new List(pValue.data) });
      } else if (typeof pValue.data === 'object') {
        res[pKey] = new Map({ ...pValue, data: new Map(pValue.data) });
      } else res[pKey] = new Map(pValue);

      return res;
    }, {}),
  );
});

const store = configureStore({
  reducer: rootReducer,
  preloadedState: initialState,
  middleware: [MQTTMiddleware, thunkMiddleware.withExtraArgument({ api }), apiMiddleware],
  devTools: process.env.NODE_ENV !== 'production' ? true : false,
});

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('reducers', () => {
    const newRootReducer = require('reducers').default;
    store.replaceReducer(newRootReducer);
  });
}

export default store;

export function getServerStore(initialServerState) {
  return configureStore({
    reducer: rootReducer,
    preloadedState: initialServerState,
    middleware: [MQTTMiddleware, thunkMiddleware.withExtraArgument({ api }), apiMiddleware],
  });
}
