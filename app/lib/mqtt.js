import { Map } from 'immutable';
import { LINK_DOMAIN, MQTT as MQTT_CONFIG } from 'settings/variables';
import { getCurrentUser } from 'reducers/user';
import { v4 } from 'uuid';
import * as cookie from 'utils/shims/cookie';

let client = null;
let reconnectDelay = null;

let injectPahoPromise = null;
const injectPaho = () => {
  if (!window.Paho || !window.Paho.MQTT) {
    if (!injectPahoPromise) {
      injectPahoPromise = new Promise((resolve) => {
        const scriptElement = document.createElement('script');
        scriptElement.type = 'text/javascript';
        scriptElement.src = `${LINK_DOMAIN}/dist/mqtt-client.js`;
        scriptElement.async = true;
        scriptElement.onload = resolve;
        document.body.appendChild(scriptElement);
      });
    }
  }
  return injectPahoPromise;
};

const handlers = [];

function onMessageArrived({ payloadString, destinationName }) {
  const handler = handlers[destinationName];
  // console.info('MQTT',{
  //   action: 'message arrived',
  //   destinationName,
  //   payloadString
  // });
  if (handler) {
    handler.forEach((h) => {
      h(payloadString);
    });
  }
}

let initClientPromise = null;
const initClient = (onConnectionLost) => {
  if (!initClientPromise) {
    if (!client) {
      client = new window.Paho.MQTT.Client(MQTT_CONFIG.host, MQTT_CONFIG.port, '/mqtt', v4());

      if (onConnectionLost) {
        client.onConnectionLost = onConnectionLost;
        client.onFailedConnect = onConnectionLost;
      }
      client.onMessageArrived = onMessageArrived;
      client.disconnectedPublishing = true;
      client.disconnectedBufferSize = 100;
      initClientPromise = new Promise((resolve) => {
        resolve(client);
      });
    }
  }
  return initClientPromise;
};

const connectMQTT = async (onSuccess, onConnectionLost) => {
  await injectPaho();
  const mqttClient = await initClient(onConnectionLost);
  onSuccess(mqttClient);
};

const subscribeMQTT = async (channel, handler) => {
  if (!client || !client.isConnected()) {
    console.error('client not connected');
  } else {
    client.subscribe(channel);
    if (!handlers[channel]) handlers[channel] = [];
    handlers[channel].push(handler);
  }
};

const publishMQTT = async (channel, message, retained = true, qos = 2) => {
  if (!client || !client.isConnected()) {
    console.error('client not connected');
  } else {
    client.send(channel, message || new ArrayBuffer(0), qos, retained);
  }
};

export const MQTT = Symbol('MQTT');

export const publicData = (channel, data, callback) => {
  return {
    type: 'MQTT',
    [MQTT]: {
      command: 'PUB',
      payload: {
        channel,
        data,
      },
      callback,
    },
  };
};

export const subscribe = (channel, { type, ...rest }, callback) => {
  return {
    type: 'MQTT',
    [MQTT]: {
      command: 'SUB',
      payload: {
        type,
        channel,
        callback,
        ...rest,
      },
    },
  };
};

export const connect = () => {
  return { [MQTT]: { command: 'CON' } };
};

export const MQTTReducer = (
  state = new Map({ status: 'INIT' }),
  action,
) => {
  switch (action.type) {
    case 'MQTT_CONNECTING': {
      return state.set('status', 'CONNECTING');
    }
    case 'MQTT_CONNECTED': {
      return state.set('status', 'CONNECTED');
    }
    case 'MQTT_DISCONNECTED': {
      return state.set('status', 'DISCONNECTED');
    }
    default:
      return state;
  }
};

export const getMQTT = (state) => {
  return state.mqtt.toJS();
};

export const MQTTMiddleware = (store) => (next) => async (action) => {
  if (!action) return;
  if (!action[MQTT]) return next(action);

  const { command, payload, callback } = action[MQTT];

  switch (command) {
    case 'CON': {
      // next({ type: 'MQTT_CONNECTING' });
      const currentUser = getCurrentUser(store.getState())
        .get('data')
        .toJS();

      connectMQTT(
        () => {
          client.connect({
            onSuccess: () => {
              console.info('MQTT_CONNECTED');
              if (reconnectDelay) clearTimeout(reconnectDelay);
              next({ type: 'MQTT_CONNECTED' });
            },
            cleanSession: false,
            userName: currentUser.id.toString(),
            password: cookie.get('accessToken'),
            reconnect: true,
            keepAliveInterval: MQTT_CONFIG.keepAliveInterval,
            useSSL: MQTT_CONFIG.useSSL,
          });
        },
        (e) => {
          console.info('MQTT_DISCONNECTED');
          if (e.reconnect) {
            console.info('MQTT_RECONNECTING');
            if (reconnectDelay) clearTimeout(reconnectDelay);
            reconnectDelay = setTimeout(() => next({ type: 'MQTT_CONNECTING' }), 3000);
          } else next({ type: 'MQTT_DISCONNECTED' });
        },
      );
      break;
    }
    case 'SUB': {
      const { channel, type, callback: subCallback, ...rest } = payload;
      subscribeMQTT(channel, (payloadString) => {
        try {
          const data = payloadString ? JSON.parse(payloadString) : [];
          if (subCallback) subCallback(data);
          next({
            type,
            ...rest,
            ...data,
          });
        } catch (e) {
          console.error(type, e);
        }
      });
      break;
    }
    case 'PUB': {
      publishMQTT(payload.channel, (payload.data && JSON.stringify(payload.data)) || null);
      if (typeof callback === 'function') callback();
      break;
    }
    default:
      break;
  }
};
