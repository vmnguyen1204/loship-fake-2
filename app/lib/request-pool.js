const concurrentQueue = 16;
const pool = [...Array(concurrentQueue).keys()].map(() => []);
const isRunning = {};

async function dequeue(line) {
  if (isRunning[line]) return false;
  const request = pool[line].shift();
  if (!request) return false;

  try {
    isRunning[line] = true;

    const res = await request.handler();
    request.callback(res);
    isRunning[line] = false;
  } catch (err) {
    request.callback(null, err);
    isRunning[line] = false;
  }

  dequeue(line);
  return true;
}

export default function enqueue(handler, callback, { priority = false, level = 1 } = {}) {
  let minLevel, poolLine, poolLineIndex;

  pool.forEach((cPoolLine, idx) => {
    const poolLineLevel = cPoolLine.reduce((sum, task) => sum + task.level, 0);

    if (minLevel === undefined || minLevel > poolLineLevel) {
      minLevel = poolLineLevel;
      poolLine = cPoolLine;
      poolLineIndex = idx;
    }
  });

  if (priority) poolLine.unshift({ handler, callback, level });
  else poolLine.push({ handler, callback, level });

  dequeue(poolLineIndex);
}
