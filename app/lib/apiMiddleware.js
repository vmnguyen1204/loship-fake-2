import * as identifiers from 'settings/identifiers';
import callAPI from 'actions/helpers';

function processList(next, action) {
  const { data, child, ...rest } = action;

  // Loop data array. For each item, dispatch action to save every single of them
  const { type: childType, ...options } = child;
  const idKey = identifiers[child.store] || 'id';
  // When saving children is done, what left is an array of their IDs.
  let childIds = data.map((d) => {
    const childRef = d._id || d.id;
    const childAction = {
      ...rest,
      data: d,
      key: d[idKey],
      type: childType,
      ref: childRef,
      ...options,
    };
    next(childAction);
    return childRef;
  });

  if (typeof action.getChildIds === 'function') childIds = action.getChildIds(data);

  // Save this array as data of the main action
  const { key, type, extras, ...otherParams } = rest;
  return next({ ...otherParams, key, type, data: childIds, extras });
}

function processData(next, action) {
  // Save data or separate array to data
  const { data, child } = action;
  if (!data) return;
  if (Array.isArray(data) && child) return processList(next, action);
  action.ref = data._id || data.id;
  next(action);
}

export const API_CALL = Symbol('API_CALL');

export default () => (next) => (action) => {
  if (!action) return;

  if (!action[API_CALL]) {
    if (!action.data) return next(action);
    return processData(next, action);
  }

  const { type, url, dataGetter, method, params, query, content, ...options } = action[API_CALL];

  const SUCCESS = type;
  const PENDING = type + '_PENDING';
  const FAILED = type + '_FAILED';

  next({ ...options, type: PENDING });

  const promise = callAPI(method, {
    url,
    params,
    query,
    content,
    accessToken: options.accessToken,
    additionalHeaders: options.additionalHeaders,
  });
  if (!promise) return;

  return promise
    .then((res) => {
      let data;
      let extras = {};

      if (typeof dataGetter === 'function') data = dataGetter(res);
      else {
        const { body } = res;
        if (body) {
          data = body.data;

          const { pagination } = body;
          if (pagination) {
            extras = { ...pagination };
            if (!data || !data.length) extras.loadMore = false;
            else if (extras.nextUrl) extras.loadMore = true;
            else extras.loadMore = pagination.limit * pagination.page < pagination.total;
          }
        }
      }

      if (res.status >= 400) throw { status: res.status, message: res.text, url };

      if (!data) next({ ...options, extras, type: SUCCESS });
      else processData(next, { ...options, data, extras, type: SUCCESS });

      // Execute Callback
      if (typeof options.callback === 'function') {
        options.callback(res);
      } else if (typeof options.callback === 'object') {
        next(options.callback);
      }

      return true;
    })
    .catch((error) => {
      if (process.env.NODE_ENV !== 'production') {
        console.error(error);
      }

      // Error here can be caused any where through Actions & Reducers
      next({ ...options, error, type: FAILED });

      // Execute Failure callback
      if (typeof options.callbackFailure === 'function') {
        options.callbackFailure(error);
      } else if (typeof options.callbackFailure === 'object') {
        next(options.callbackFailure);
      }

      // Another benefit is being able to log all failures here
      if (typeof error === 'object') {
        error.params = params;
        error.type = type;
      }
      if (!process.env.BROWSER) if (error && error.status !== 401) throw error;
    });
};
