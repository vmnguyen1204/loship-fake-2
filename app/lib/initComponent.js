import { connect } from 'react-redux';
import qs from 'qs';

function mapStateToProps(state, props) {
  const { dataGetter, params, queryParams, match } = props;
  if (typeof dataGetter !== 'function') return {};

  let dataGetterParams = { ...params };
  if (match && match.params) dataGetterParams = { ...dataGetterParams, ...match.params };
  if (Array.isArray(queryParams) && props.location && props.location.search) {
    const query = qs.parse(props.location.search, { ignoreQueryPrefix: true });
    queryParams.forEach((p) => {
      if (query[p]) dataGetterParams[p] = query[p];
    });
  }

  const record = dataGetter(state, dataGetterParams);
  if (!record) return {};
  if (typeof record.toObject === 'function') return record.toObject();
  return record;
}

export default function initComponent(comp, otherWrapper) {
  let newComp = connect(
    comp.mapStateToProps || mapStateToProps,
    null,
    null,
    { forwardRef: true },
  )(comp);

  if (typeof otherWrapper === 'function') {
    newComp = otherWrapper(newComp);
  }

  // connect function does not handle defaultProps
  // we need to copy it manually
  newComp.defaultProps = comp.defaultProps;

  const regex = /withRouter\((.*?)\)/;
  const pieces = regex.exec(comp.displayName);
  const displayName = pieces && pieces[1] ? pieces[1] : comp.displayName;
  newComp.displayName = displayName;

  return newComp;
}
