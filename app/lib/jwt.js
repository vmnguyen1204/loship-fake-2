import jwt from 'jsonwebtoken';

// http://travistidwell.com/blog/2013/09/06/an-online-rsa-public-and-private-key-generator/
const privateKEY = null;
const publicKEY = 'vwZwJo1fV9guw7qegUj1ccsckEp9wLtAMaR5BM2yFPvK26mk0ODlWKKxWsthbT5';

const jwtOption = {
  issuer: '',
  audience: '',
  subject: '',
};

const JWT = {
  sign: (payload, options) => {
    const signOptions = {
      expiresIn: '30d', // 30 days validity
      algorithm: 'HS256', // RSASSA options[ "RS256", "RS384", "RS512" ],
      ...jwtOption,
      ...options,
    };
    return jwt.sign(payload, options.privateKEY || privateKEY, signOptions);
  },

  verify: (token, options) => {
    const verifyOptions = {
      expiresIn: '30d',
      algorithm: ['HS256'],
      ...jwtOption,
      ...options,
    };

    try {
      return jwt.verify(token, options.publicKEY || publicKEY, verifyOptions);
    } catch (err) {
      console.error('ERROR: cannot verify', err);
      return false;
    }
  },

  decode: (token) => {
    return jwt.decode(token, { complete: true });
  },
};

export default JWT;
