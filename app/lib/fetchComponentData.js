export default function fetchComponentData({ dispatch, components, params, serverParams, query }) {
  const needs = components.reduce((prev, current) => {
    return (current.needs || []).concat(prev);
  }, []);

  const promises = needs
    .filter((need) => typeof need === 'function')
    .map((need) => dispatch(need({ ...params, ...serverParams }, query)));
  return Promise.all(promises);
}
