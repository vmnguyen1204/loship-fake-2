import request from 'utils/shims/request';
import * as cookie from 'utils/shims/cookie';

// Inject params to url
function injectParams(url, params) {
  if (!url) return console.error('Empty url', url, params);

  if (!params) return url;

  let parsedUrl = url;
  Object.keys(params).forEach((key) => {
    parsedUrl = parsedUrl.replace('${' + key + '}', params[key]);
  });

  if (parsedUrl.indexOf('$') > -1) return console.error('Missing params for url', parsedUrl, params);

  return parsedUrl;
}

export function callAPI(method, options) {
  const { url, params, query, content } = options;
  let accessToken = cookie.get('accessToken') || 'unknown';
  const link = injectParams(url, params);

  if (options.accessToken) {
    accessToken = options.accessToken;
  }
  const req = request(method, link)
    .query(query)
    .send(content)
    .accessToken(accessToken);

  if (query && query.cityId) req.set('X-City-ID', query.cityId);

  return req.end(options.end);
}
