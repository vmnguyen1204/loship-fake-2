export const GOOD_RATING_LENGTH = 3;
export const BAD_RATING_LENGTH = 15;
export const DEADLINE_INTRODUCE_EATERY_RATINGS = '23:59:59 12/07/2020';

export const COOKIE_01 = '__f_01';
export const COOKIE_02 = '__f_02';
export const COOKIE_03 = '__f_03';
export const COOKIE_04 = '__f_04';
