// Source: https://www.w3resource.com/javascript-exercises/javascript-array-exercise-30.php
export function mergeArray(array1, array2) {
  const resultArray = [];
  const arr = array1.concat(array2);
  let len = arr.length;
  const assoc = {};

  while (len--) {
    const item = arr[len];

    if (!assoc[item]) {
      resultArray.unshift(item);
      assoc[item] = true;
    }
  }
  return resultArray;
}

export function copyToClipboard(str) {
  const el = document.createElement('textarea');
  el.value = str;

  // make element outside of viewport
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';

  // clipboard manipulating
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
}
