import { LINK_ASSETS, LINK_AUTH } from 'settings/variables';
import fileApi from 'utils/shims/fileapi';
import qs from 'qs';

export function getAvatarUrl(src, width = 320) {
  if (!src) {
    return '/dist/images/avatar.png';
  }

  let overideSrc = src;
  if (overideSrc.indexOf('http') < 0) {
    // Relative path
    overideSrc = LINK_ASSETS + '/' + overideSrc;
  } else {
    // Facebook or Full URL
    overideSrc = overideSrc.replace(/=80/g, '=' + width);
  }
  return overideSrc;
}

export function getImageUrl(src, width, type = 's', effect = undefined, placeholder) {
  const defaultSrc = placeholder === false ? false : placeholder || '/dist/images/placeholder.png';

  if (!src) return defaultSrc;
  if (!width) return src;
  if (src.indexOf('?') >= 0) {
    const url = src.split('?')[0];
    const query = qs.parse(src.split('?')[1]);

    if (query.width && query.height) {
      query.height = (query.height / query.width) * width;
      query.width = width;
    }
    return url + '?' + qs.stringify(query);
  }

  const query = [];
  if (width) query.push('w=' + width);
  if (type) query.push('type=' + type);
  if (effect) query.push('effect=' + effect);

  return src + '?' + query.join('&');
}

export function uploadImage(file, callback, apiUrl, imageTransform) {
  fileApi
    .image(file)
    .resize(1600, 1600, 'max')
    .get((error, img) => {
      if (error) return console.error(error);
      const imageObject = {
        url: apiUrl || `${LINK_AUTH}/upload/images`,
        files: { files: fileApi.image(img) },
        complete: async (err, xhr) => {
          if (err) return console.error(err);

          const res = JSON.parse(xhr.response);

          if (!res) return;

          callback(res.data[0].url);
        },
      };
      if (imageTransform) imageObject.imageTransform = { ...imageTransform };
      fileApi.upload(imageObject);
    });
}

export function getBestFixCropPos(dimension) {
  const afterCrop = {};
  afterCrop.width = dimension.width;
  afterCrop.height = dimension.height;
  if (afterCrop.width > afterCrop.height) {
    afterCrop.width = Math.round(((afterCrop.height * 100) / afterCrop.width) * 100) / 100;
    afterCrop.height = 100;
    afterCrop.x = Math.round(((((dimension.width - dimension.height) / 2) * 100) / dimension.width) * 100) / 100;
    afterCrop.y = 0;
  } else {
    afterCrop.height = Math.round(((afterCrop.width * 100) / afterCrop.height) * 100) / 100;
    afterCrop.width = 100;
    afterCrop.y = Math.round(((((dimension.height - dimension.width) / 2) * 100) / dimension.height) * 100) / 100;
    afterCrop.x = 0;
  }
  return afterCrop;
}
