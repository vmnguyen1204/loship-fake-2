import { getGoogleMap } from 'services/google-map';

export const getDistance = async (origin, destination) => {
  const googleMap = await getGoogleMap();

  try {
    const response = await googleMap.getRoute({
      origin,
      destination,
      provideRouteAlternatives: false,
      travelMode: 'WALKING',
    });

    if (response.routes[0]) {
      return response.routes[0].legs[0].distance;
    }
  } catch (e) {
    console.error(e);
  }
};

export function getDistanceBirdFlies(lat1, lng1, lat2, lng2) {
  function deg2rad(deg) {
    return deg * (Math.PI / 180);
  }

  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(lat2 - lat1);
  const dLng = deg2rad(lng2 - lng1);

  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return Math.round(R * c * 10) / 10; // Distance in km
}
