import immerProduce from 'immer';

const MAX_MOBILE_WIDTH = 767;

export function debouncer(timer, callback, timeout = 1000) {
  if (timer) clearTimeout(timer);
  return setTimeout(callback, timeout);
}

export function produce(initialState, callback) {
  if (typeof callback !== 'function') return immerProduce(initialState, () => {});
  return immerProduce(initialState, callback);
}

export function iife(func) {
  return typeof func === 'function' && func();
}

export function encodeState(str) {
  const js = /<(\/?)script>|\u2028|\u2029/g;
  const encodedStr = JSON.stringify(JSON.stringify(str));
  return encodedStr.replace(js, '');
}

export function detectMobileDevice(ua, screenWidth) {
  if (screenWidth) {
    return screenWidth <= MAX_MOBILE_WIDTH ? 'mobile' : 'desktop';
  }

  if (!ua && process.env.AGENT) return process.env.AGENT;
  if (!ua && globalThis.navigator) ua = globalThis.navigator.userAgent;
  if (!ua) return 'mobile';

  const isMobile = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(ua.toLowerCase());
  return isMobile ? 'mobile' : 'desktop';
}

export function isWebBotClient(ua) {
  if (!ua && globalThis.navigator) ua = globalThis.navigator.userAgent;

  const isWebBot = /Googlebot|Bingbot|Slurp|DuckDuckBot|Baiduspider|YandexBot|Konqueror|Exabot|facebot|facebookexternalhit|ia_archiver/i.test(ua.toLowerCase());
  return isWebBot;
}
