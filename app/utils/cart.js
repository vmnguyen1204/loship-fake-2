import { PRICE_ROUND_THREHOLD } from 'settings/variables';
import moment from 'utils/shims/moment';
import { iife } from 'utils/helper';
import { isEqual } from 'lodash';

/**
 * Dish {
 *   id,
 *   customs: [{
 *     id, customOptions: [{ id }]
 *   }],
 * }
 *
 * OrderLine {
 *   dishId, dishQuantity, groupDishId, taggedUserId, ...,
 *   customs: [{
 *     customId, options: [{ optionId, optionQuantity, ... }]
 *   }]
 * }
 *
 * OrderLineData {
 *   dishId, dishQuantity, groupDishId, taggedUserId,
 *   customs: [{
 *     customId, options: [{ optionId, optionQuantity }]
 *   }]
 * }
 *
 * getOrderLines
 * - input: Cart { [username]: { status, data: [OrderLine] } }
 * - output: [OrderLine]
 *
 * getOrderLinesData
 *  input: Cart
 * - output: [OrderLineData]
 *
 * getOrderLinesQuantity
 * - input: Cart
 * - output [OrderLine].length
 *
 * getOrderLine
 * - input: Dish, extraData {...}
 * - output: OrderLine
 *
 * getOrderLineData
 * - input: OrderLine
 * - output: OrderLineData
 */

const filterOptionQuantity = (line) => {
  (line.customs || []).forEach((custom) => {
    custom.options = (custom.options || []).filter((c) => !!c);

    if (custom.selectionType === 'single_choice') {
      if (custom.options.length > 1) {
        custom.options = custom.options.slice(0, 1);
      }
      if (custom.options[0]) {
        if (custom.options[0].optionQuantity !== 1) {
          custom.options[0].optionQuantity = 1;
        }
      }
    } else {
      custom.options = custom.options.map((option) => {
        if (option.optionQuantity > option.limitQuantity) {
          option.optionQuantity = option.limitQuantity;
        }
        if (option.optionQuantity < 1) option.optionQuantity = 1;
        return option;
      });
    }
  });

  return { ...line };
};


export function calculateDishesPrice(order) {
  if (!order || !order.length) return 0;
  let sum = 0;

  order.forEach((dish) => {
    sum += dish ? calculateDishPrice(dish, dish.customs, dish.dishQuantity) : 0;
  });
  return sum;
}

export function getOrderLines(cart) {
  if (!cart) return [];
  return Object.entries(cart).reduce((acc, userOrder) => {
    if (!userOrder || !userOrder[1]) return acc;
    return acc.concat(userOrder[1].data);
  }, []);
}
export function getOrderLinesData(cart, taggedUserId) {
  return getOrderLines(cart).map((line) => getOrderLineData(line, taggedUserId));
}
export function getOrderLinesQuantity(cart) {
  return getOrderLines(cart).reduce((acc, line) => acc + line.dishQuantity, 0);
}
export function getOrderLine(dish, lineData = {}, extraData = {}) {
  if (!dish) return {};

  // custom.customId, option.optionId came from order detail
  const customs = (dish.customs || []).map((custom) => {
    const lineCustom = (lineData.customs || []).find((lCustom) => custom.id === lCustom.customId || custom.customId === lCustom.customId);
    if (!lineCustom) return;

    const options = (custom.customOptions || []).map((option) => {
      const lineOption = (lineCustom.options || []).find(
        (lOption) => option.id === lOption.optionId || option.optionId === lOption.optionId,
      );
      if (!lineOption) return;
      return {
        optionId: option.id,
        optionQuantity: lineOption.optionQuantity || 1,
        limitQuantity: option.limitQuantity,
        value: option.value,
        price: option.price,
        note: option.note,
      };
    });
    return {
      customId: custom.id,
      options: options.filter((o) => !!o),
    };
  });

  return {
    dishId: dish.id,
    dishQuantity: lineData.dishQuantity || 1,
    groupDishId: lineData.groupDishId || dish.groupDishId,
    taggedUserId: lineData.taggedUserId,
    soldOut: lineData.soldOut,
    name: dish.name,
    image: dish.image,
    price: dish.price,
    rawPrice: dish.rawPrice,
    isAppliedFixedPricePromotionCampaign: dish.isAppliedFixedPricePromotionCampaign,
    customs: customs.filter((c) => !!c),
    note: lineData.note,
    ...extraData,
  };
}
export function getOrderLineData(line, taggedUserId) {
  // customOptions, option.quantity came from order detail
  const customs = (line.customs || []).map((custom) => ({
    customId: custom.customId,
    options: (custom.options || custom.customOptions || []).map((option) => ({
      optionId: option.optionId,
      optionQuantity: option.optionQuantity || option.quantity,
    })),
  }));
  return {
    dishId: line.dishId,
    dishQuantity: line.dishQuantity,
    groupDishId: line.groupDishId,
    taggedUserId: line.taggedUserId || taggedUserId,
    customs,
    note: line.note,
  };
}

export function saveCartLocal({ key, data, merchant, serviceName, overwrite }) {
  const localData = iife(() => {
    try {
      return JSON.parse(localStorage.getItem(key)) || {};
    } catch (e) {
      return {};
    }
  });

  localData.serviceName = serviceName;
  const hasChangedMerchant = merchant && merchant !== localData.merchant;
  if (hasChangedMerchant) localData.merchant = merchant;

  if (!Array.isArray(localData.data) || overwrite || hasChangedMerchant) {
    const newData = {
      data,
      merchant: data.length > 0 && localData.merchant,
      serviceName: data.length > 0 && serviceName,
    };
    localStorage.setItem(key, JSON.stringify(newData));
    return newData;
  }

  data.forEach((dish) => {
    const localDish = localData.data.find((d) => {
      if (d.dishId !== dish.dishId) return false;
      return isEqual(getOrderLineData(dish).customs, getOrderLineData(d).customs) &&
        dish.note === d.note;
    });

    if (!localDish) return localData.data.push(dish);
    if (dish.mode === 'set') localDish.dishQuantity = dish.dishQuantity;
    else localDish.dishQuantity += dish.dishQuantity;
  });

  const newData = { ...localData, merchant: localData.data.length > 0 && localData.merchant };
  localStorage.setItem(key, JSON.stringify(newData));
  return newData;
}

export const filterDishQuantity = (lines) => {
  if (!lines) return [];
  return lines.filter((line) => line?.dishQuantity > 0).map(filterOptionQuantity);
};

// ////////////////////////////////////////////

export function calculateDishPrice(dish, customs = {}, dishQuantity = 1, useRawPrice) {
  let sum = (useRawPrice && dish.rawPrice) || dish.price;

  customs && Object.keys(customs).forEach((k) => {
    const options = customs[k].options || customs[k].customOptions;
    if (Array.isArray(options)) {
      options.forEach((o) => {
        if (!o) return;
        const { quantity, optionQuantity, price = 0, rawPrice = 0 } = o;
        const oQuantity = quantity || optionQuantity || 1;
        sum += ((useRawPrice && rawPrice) || price) * oQuantity;
      });
    }
  });
  sum *= dishQuantity;

  return sum;
}

export function roundPrice(price) {
  const mod = price % 1000;
  if (mod > PRICE_ROUND_THREHOLD) {
    return price - mod + 1000;
  }
  return price - mod;
}

export function roundPriceWithThreshold(price, threshold = 500) {
  const mod = price % 1000;
  if (mod > threshold) {
    return price - mod + 1000;
  }
  if (mod > 0 && mod <= threshold) {
    return price - mod + 500;
  }
  return price;
}

export function calculateDiscountPrice(sum, discount) {
  if (!discount || !discount.value) return 0;
  if (discount.promotionType === 'direct') return discount.value;

  const rawDiscount = Math.min((sum * discount.value) / 100, discount.maxDiscount);
  return Math.floor(rawDiscount / 1000) * 1000;
}

export function calculateOrderPrice({ order, ship = 0, extraFees = [], discount = 0 } = {}) {
  let sum = calculateDishesPrice(order);

  sum += ship;
  if (extraFees && extraFees.length > 0) {
    extraFees.forEach((extraFee) => {
      sum += extraFee.value;
    });
  }

  if (discount) {
    const discountValue = calculateDiscountPrice(sum, discount);
    sum -= discountValue;
  }

  sum = Math.max(0, sum);
  return sum;
}

export function getNextOpenTime(timeStamp) {
  const now = moment();
  const openTime = moment(timeStamp, ['HH:mm A', 'H:mm A']);
  const hourDiff = now.diff(openTime, 'hours', true);
  if (hourDiff > 0) return openTime.add(1, 'days');
  return openTime;
}

export function getDeliveryTime(willBeDeliveredAt, isPreOrder = false) {
  const time = moment(willBeDeliveredAt);
  const minute = time.minute();
  time.minute(parseInt(minute / 5) * 5);

  // TIME IS MUTABLE
  const processTime = time.subtract(1, 'hour').format('HH:mm');
  const deliverTime = time.add(50, 'minutes').format('HH:mm');

  if (!isPreOrder) {
    return {
      string: '{0}',
      processTime,
      time: deliverTime,
    };
  }

  return {
    string: time
      .calendar(null, {
        sameDay: '[Today_at_0]',
        nextDay: '[Tomorrow_at_0]',
        nextWeek: 'dddd[_at_0]',
      })
      .toLowerCase(),
    time: deliverTime,
    processTime,
  };
}

export function getProcessStep(status) {
  switch (status) {
    case 'purchasing':
    case 'deliverying':
      return 2;
    case 'done':
      return 3;
    case 'cancel':
    case 'return':
      return -1;
    case 'pre-order':
      return 0;
    default:
      return 1;
  }
}

export function getMerchantProcessStep(status, isBillValuePaid) {
  switch (status) {
    case 'ordered':
    case 'assigning':
    case 'purchasing':
      return 'wait-shipper';
    case 'pre-order':
      return 'pre-order';
    default:
      return isBillValuePaid ? 'done' : 'cancel';
  }
}
