import { LINK_DOMAIN_LOSHIP } from 'settings/variables';

export const HOME = 0;
export const USER = 1;
export const EATERY = 2;
export const ALBUM = 3;
export const BLOCK = 4;
export const EVENT = 5;
export const HASHTAG = 6;
export const TOPIC = 7;
export const CATEGORY = 8;

export function check(url) {
  const urlArr = url
    .replace('lozi.vn', '{DOMAIN}')
    .replace('loship.vn', '{DOMAIN}')
    .split('{DOMAIN}/');
  if (urlArr.length <= 1) {
    return HOME;
  }

  const valueArr = urlArr[1].split('/');

  if (valueArr.length === 1) {
    // this is USER
    return USER;
  }

  if (valueArr.length === 2) {
    // this is EATERY
    if (valueArr[0] === 'b') {
      return EATERY;
    }
    // this is BLOCK
    if (valueArr[0] === 'photos') {
      return BLOCK;
    }
    // this is event (contest)
    if (valueArr[0] === 'e') {
      return EVENT;
    }
    // this is HASHTAG
    if (valueArr[0] === 'hashtag') {
      return HASHTAG;
    }
    // this is ALBUM
    return ALBUM;
  }

  if (valueArr.length >= 2) {
    // this is EATERY
    if (valueArr[0] === 'b') {
      return EATERY;
    }
  }

  if (valueArr.length === 4) {
    // this is TOPIC
    if (valueArr[2] === 'photos') {
      return TOPIC;
    }
    // this is CATEGORY
    if (valueArr[2] === 'b') {
      return CATEGORY;
    }
  }
  return false;
}

export function handle(url) {
  const result = {
    type: 'lozi',
    slug: 'lozi',
  };
  const urlArr = url.split('/');
  const slugArr = urlArr[urlArr.length - 1].split('?');

  result.slug = slugArr[0];

  switch (check(url)) {
    case HOME:
      result.type = 'homepage';
      break;
    case USER:
      result.type = 'user';
      break;
    case EATERY:
      result.type = 'eatery';

      if (['news', 'items', 'photos', 'reviews', 'videos', 'branches', 'promotions'].indexOf(result.slug) > -1) {
        result.slug = urlArr[urlArr.length - 2];
      }

      break;
    case ALBUM:
      result.type = 'album';
      result.USER = urlArr[1];
      break;
    case BLOCK:
      result.type = 'block';
      break;
    case EVENT:
      result.type = 'contest'; // api not supported "event" yet
      break;
    case HASHTAG:
      result.type = 'hashtag';
      break;
    case TOPIC:
      result.type = 'topic';
      break;
    case CATEGORY:
      result.type = 'category';
      break;
    default:
      break;
  }
  return result;
}

export function getSlug(type = HOME, url) {
  let reg;
  switch (type) {
    case USER:
      reg = /lozi.vn\/([^?]*)/; // http://lozi.vn/{username}
      break;
    case EATERY:
      reg = /b\/(.*?)$/; // http://lozi.vn/{{b/merchant-name}}
      break;
    case BLOCK:
      reg = /photos\/(.*?)$/; // http://lozi.vn/{{b/merchant-name}}
      break;
    case ALBUM:
    case EVENT:
    case HASHTAG:
    case TOPIC:
    case CATEGORY:
    case HOME:
    default:
      return false;
  }

  const urlPieces = reg.exec(url);

  if (!urlPieces || !urlPieces[1]) {
    console.error('Invalid URL');
    return false;
  }
  return urlPieces[1];
}

export function getDesktopUrl(url) {
  return LINK_DOMAIN_LOSHIP + url;
}
