import { merge } from 'lodash';
import { APP_INFO } from 'settings/variables';
import { iife } from 'utils/helper';
import enUS from './default/en_US';
import viVN from './default/vi_VN';
import lomartEnUS from './lomart/en_US';
import lomartViVN from './lomart/vi_VN';

const lang = {
  vi_VN: {
    name: 'Vietnamese',
    data: viVN,
  },
  en_US: {
    name: 'English',
    data: enUS,
  },
};

export default iife(() => {
  if (APP_INFO.serviceName === 'lomart') {
    merge(lang.vi_VN.data, lomartViVN);
    merge(lang.en_US.data, lomartEnUS);
  }
  return lang;
});
