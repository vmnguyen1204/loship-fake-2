export default {
  this_place_has_not_supported_ordering_on_loship_yet: 'This place has not supported ordering on Lomart yet!',
  thank_you_for_choosing_loship: 'Thank you for choosing Lomart!',
  find_your_food_beverages_sweets_for_loship_to_deliver: 'Find your food, beverages, sweets for Lomart to deliver',
  loshipvn_bring_your_orders_to_your_house_at_0: 'Lomart.vn - Bring your orders to your house at {0}',
  cooperate_with_loship: 'Cooperate with Lomart',
  popular_stores: 'POPULAR STORES',
  category: 'CATEGORY',
  were_sorry_to_inform_that_loship_are_not_yet_available_in_this_area:
    'We\'re sorry to inform that Lomart are not yet available in this area.',
  your_group_orders_are_received_thank_you_for_using_loship:
    'Your group orders are received. Thank you for using Lomart!',
  menu_management_tools_help_the_ownersadministrators_to_easily_optimize_and_update_the_menu_of_0_that_appears_on_loship:
    'Menu management tools help the owner(s)/administrators to easily optimize and update the menu of {0} that appears on Lomart.',
  loship_an_app_that_help_deliver_milk_tea_and_food_within_1_hour_using_your_own_motorbike_and_smart_phone_android_or_iphone:
    'Lomart - an app that help deliver milk tea and food within 1 hour - using your own motorbike and smart phone (Android or iPhone).',
  become_a_loship_warrior_fast_and_free: 'Become a Lomart warrior - fast and free',
  easily_become_a_loship_warrior: 'Easily become a Lomart warrior',
  become_a_loship_warrior_now: 'Become a Lomart warrior now!',
  become_a_loship_warrior: 'Become a Lomart warrior',
  download_loship_app: 'Download App:',
  as_a_loship_driver: 'As a Lomart driver',
  become_a_loship_partner: 'Become a Lomart partner',
  loship_buy_groceries_for_you_anything_you_need_we_deliver:
    'Lomart - Buy groceries for you. Anything you need, we deliver.',
  download_loship_app_now: 'Download Loship app now',
  find_photos_on_loship: 'Find photos on Lomart',
  your_account_has_been_suspended_in_violation_of_loships_policy:
    'Your account has been suspended in violation of Lomart\'s policy.',
  your_phone_number_has_been_blocked_in_violation_of_loships_policy:
    'Your phone number has been blocked in violation of Lomart\'s policy.',
  error_code_0: 'Error code: {0}',
  share_with_your_friends_about_loship_to_be_unlocked: 'Share with your friends about Lomart, to be unlocked!',
  allow_buy_in_same_branch_description: 'Lomart will automatically be located at your nearest branch to save time and ship costs',
  meat_and_eggs: 'Meat & Eggs',
  seafood: 'Seafood',
  rice_and_noodles: 'Rice & Noodles',
  spices: 'Spices',
  grocery_stores: 'Grocery stores',
  fruit: 'Fruit',
  new_locations: 'New Locations',
  promoted_locations: 'Promoted Locations',
  vegetables: 'Vegetables',
  fruits: 'Fruits',
  rice: 'Rice',
  meat_poultry_and_eggs: 'Meat, Poultry & Eggs',
  fish_seafood_and_frozen_food: 'Fish, Seafood & Frozen Food',
  fish_seafood: 'Fish & Seafood',
  milk_and_dairy_products: 'Milk & Dairy Products',
  grocery_stores_and_supermarkets: 'Grocery Stores & Supermarkets',
  convenience_stores: 'Convenience Stores',
  spices_and_seasonings: 'Spices & Seasonings',
  drinks_and_beverages: 'Drinks & Beverages',
  sweets_and_snacks: 'Sweets & Snacks',
  stationaries: 'Stationaries',
  households: 'Households',
  electronic_products: 'Electronic Products',
  mom_baby_and_kids: 'Mom, Baby & Kids',
  loshipvn_buy_and_deliver_food_groceries_and_more_for_you_within_1_hour:
    'Lomart.vn - Buy and deliver food, groceries and more for you within 1 hour',
  we_proudly_present_to_you_the_best_shipping_service_ever_made_for_everyone_see_more:
    'We proudly present to you the best shipping service ever made for everyone. See more:',
  what_is_loship: 'What is Lomart?',
  boost_your_business_by_partnering_with_loship: 'Boost your business by partnering with Lomart.',
  recruiting_loship_warrior: 'Recruiting Lomart warrior',
  become_a_loship_warrior_today: 'Become a Lomart warrior today!',
  rules_of_loship_warrior: 'Rules of Lomart warrior',
  loship_driver: 'Lomart driver',
  take_loads_of_orders_without_worrying_how_to_deliver_them_to_the_customers_for_loship_is_here:
    'Take loads of orders, without worrying how to deliver them to the customers, for Lomart is here!',
  promotions_occur_on_loship_oftenly_dont_forget_to_use_loship_everyday_to_get_great_deals:
    'Promotions occur on Lomart oftenly, don\'t forget to use Lomart everyday to get great deals!',
  most_favored_products: 'MOST FAVORED PRODUCTS',
  see_all_products_at_0: 'See all products at {0}',
  no_products_found: 'No products found.',
  item_added: 'Item added!',
  do_you_want_to_continue_shopping_at: 'Do you want to continue shopping at',
  click_go_to_store_to_see_all_products_listed_here: 'Click "Go to store" to see all products listed here.',
  go_to_store: 'Go to store',
  select: 'Select',
  failed_to_refund_we_are_sorry_for_this_inconvenience_please_contact_us_via_hotroloshiplozivn_for_support:
    'Failed to refund (We are sorry for this inconvenience. Please contact us via hotroloship@lozi.vn for support)',
  a_merchant_must_have_an_avatar_picture_for_better_display_on_loship_before_having_a_menu:
    'A merchant must have an avatar picture for better display on Lomart before having a menu.',
  a_merchant_on_loship_must_have_a_username_in_order_to_create_short_link_for_it_like_loshipvntencuahang:
    'A merchant on Lomart must have a username in order to create short link for it, like lomart.vn/tencuahang.',
  edit_shop_information: 'Edit shop information',
  your_order_was_shipped_by: 'Your order was shipped by:',
  thank_you_for_using_loship_you_will_receive_a_message_to_confirm_your_order_in_a_few_seconds:
    'Thank you for using Lomart. You will receive a message to confirm your order in a few seconds.',
  thank_you_for_using_loship_as_your_food_delivery_service_now_you_just_need_to_login_and_your_order_will_be_placed:
    'Thank you for using Lomart as your food delivery service. Now you just need to login, and your order will be placed!',
  you_are_now_pre_ordering_at_0_your_pre_order_will_be_delivered:
    'you are now pre-ordering at {0}. Your pre-order will be delivered',
  unknown_error_please_contact_us_at_hotroloshiplozivn_sorry_and_thank_you_for_choosing_loship_today:
    'Unknown error. Please contact us at hotroloship@lozi.vn. Sorry and thank you for choosing Lomart today!',
  thank_you_for_using_loship: 'Thank you for using Lomart!',
  tell_us_more_how_was_your_food_was_the_shipper_nice_do_you_think_there_is_anything_loship_should_improve_min_3_words:
    'Tell us more! How was your food? Was the shipper nice? Do you think there is anything Lomart should improve? (Min. 3 words)',
  allow_location_title: 'Allow Lomart access your location',
  promo_notice_minimum_total:
    'Notice: Code only applied for order from {0} and up. Please pick more items to get your discount!',
  add_dish: 'Add item',
  item_sharing_content: 'Hey, check out {0} on Lomart, what a hot deal!',
  contact_loship: 'Contact Lomart',
};
