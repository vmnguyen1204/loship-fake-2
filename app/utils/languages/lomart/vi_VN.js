export default {
  this_place_has_not_supported_ordering_on_loship_yet: 'Cửa hàng này chưa hỗ trợ đặt món trên Lomart!',
  if_you_think_this_is_a_mistake_please_let_us_know_by_contacting:
    'Nếu bạn nghĩ đây là lỗi bất thường, hãy báo cho Đội ngũ Lomart qua email ',
  sorry_the_seller_did_not_provided_enough_information_order_cancelled:
    'Xin lỗi quý khách, Lomart không thể xử lý đơn hàng này do người bán không cung cấp đủ thông tin. Quý khách vui lòng liên hệ trực tiếp người bán.',
  in_case_you_merchant_owner_need_to_update_your_menu_please_contact_us_via:
    'Quý đối tác có nhu cầu cập nhật menu, vui lòng liên hệ Đội ngũ Lomart qua email ',
  thank_you_for_choosing_loship: 'Cảm ơn quý đối tác đã chọn và đồng hành cùng Lomart!',
  order_should_not_be_empty: 'Bạn chưa chọn món nào! Hãy chọn một món để Lomart giao tận nơi bạn nhé.',
  find_your_food_beverages_sweets_for_loship_to_deliver: 'Tìm siêu thị, cửa hàng tiện lợi để đặt Lomart giao ngay',
  loshipvn_bring_your_orders_to_your_house_at_0: 'Lomart.vn - Mua rau thịt cá và mang đến tận nơi cho bạn tại {0}',
  recently_joined_merchants: 'CỬA HÀNG MỚI THAM GIA LOMART',
  cooperate_with_loship: 'Hợp tác cùng Lomart',
  popular_stores: 'CỬA HÀNG ĐƯỢC MUA NHIỀU',
  hot_deals: 'CỬA HÀNG CÓ KHUYẾN MÃI',
  hot_locations: 'CỬA HÀNG ĐƯỢC ĐẶT NHIỀU',
  trending_locations: 'CỬA HÀNG VỪA ĐƯỢC ĐẶT',
  recently_viewed_locations: 'CỬA HÀNG XEM GẦN ĐÂY',
  recently_ordered_locations: 'CỬA HÀNG MUA GẦN ĐÂY',
  this_location_needs_a_parking_fee_please_support_the_shippers_on_this:
    'Cửa hàng này cần gửi xe để mua. Anh/chị vui lòng hỗ trợ anh em shipper.',
  were_sorry_to_inform_that_loship_are_not_yet_available_in_this_area:
    'Xin lỗi bạn! Hiện tại Lomart chưa thể phục vụ giao hàng tại địa điểm này.',
  this_location_is_temporarily_unavailable: 'Cửa hàng đã ngưng hoạt động',
  sorry_this_location_has_stopped_operation_for_awhile_please_re_order_from_another_location:
    'Xin lỗi bạn! Cửa hàng này đã ngừng kinh doanh, bạn hãy đặt món ở quán khác nhé!',
  your_group_orders_are_received_thank_you_for_using_loship:
    'Đơn hàng của nhóm bạn đã đặt thành công. Cảm ơn các bạn đã sử dụng Lomart!',
  menu_management_tools_help_the_ownersadministrators_to_easily_optimize_and_update_the_menu_of_0_that_appears_on_loship:
    'Công cụ quản lý menu cho phép chủ địa điểm / người quản lý dễ dàng điều chỉnh và cập nhật menu của {0} xuất hiện trên Lomart.',
  you_can_easily_create_new_dishes_update_the_new_price_for_old_dishes_or_turning_on_and_off_that_dishes_you_dont_want_to_appear_every_changes_will_be_immediately_update_for_the_customers_to_order:
    'Bạn có thể dễ dàng tạo món mới, cập nhật giá món cũ hoặc đánh dấu bật / tắt tình trạng phục vụ một món bất kỳ. Mọi thay đổi trên menu sẽ được cập nhật tức thời cho khách hàng khi đặt món trên Lomart.',
  duplicate_it_from_the_old_ones: 'Sao chép menu từ địa điểm có sẵn trên Lomart',
  loship_an_app_that_help_deliver_milk_tea_and_food_within_1_hour_using_your_own_motorbike_and_smart_phone_android_or_iphone:
    'Lomart - ứng dụng giao thực phẩm trong vòng 1 giờ - với xe máy của chính mình và điện thoại Android hoặc iPhone.',
  become_a_loship_warrior_fast_and_free: 'Trở thành chiến binh Lomart - nhanh và miễn phí',
  easily_become_a_loship_warrior: 'Dễ dàng trở thành chiến binh của Lomart',
  become_a_loship_warrior_now: 'Trở thành Chiến binh Lomart ngay hôm nay!',
  become_a_loship_warrior: 'Trở thành chiến binh Lomart!',
  download_loship_app: 'Tải ứng dụng Lomart:',
  for_eatery_owners: 'Hợp tác cửa hàng',
  become_a_loship_partner: 'Trở thành đối tác Lomart',
  with_loship_app: 'với ứng dụng Loship',
  find_photos_on_loship: 'Tìm ảnh trên Lomart',
  your_account_has_been_suspended_in_violation_of_loships_policy:
    'Tài khoản của bạn đã bị khóa do vi phạm quy định của Lomart.',
  your_phone_number_has_been_blocked_in_violation_of_loships_policy:
    'Số điện thoại của bạn đã bị khóa do vi phạm quy định của Lomart.',
  share_with_your_friends_about_loship_to_be_unlocked: 'Hãy chia sẻ Lomart cho nhiều người hơn, để được mở khóa nhé!',
  please_go_back_and_choose_at_least_1_dish_to_process:
    'Hãy quay lại menu và chọn ít nhất 1 món để tiếp tục đặt hàng với Lomart nhé!',
  allow_buy_in_same_branch_description: 'Lomart sẽ tự động đặt tại chi nhánh gần bạn hơn để tiết kiệm thời gian và phí ship',
  new_location: 'Cửa hàng mới tham gia lomart',
  promoting_locations: 'Cửa hàng có khuyến mãi',
  recently_ordered: 'Cửa hàng vừa được đặt',
  most_ordered_locations: 'Cửa hàng được đặt nhiều',
  meat_and_eggs: 'Thịt & trứng',
  seafood: 'Hải sản tươi',
  rice_and_noodles: 'Gạo & mì',
  spices: 'Gia vị',
  grocery_stores: 'Bách hóa & Siêu thị',
  fruit: 'Trái cây',
  nearby_location_search: 'Cửa hàng quanh đây',
  new_locations: 'Địa Điểm Mới',
  promoted_locations: 'Khuyến Mãi',
  vegetables: 'Rau Củ',
  fruits: 'Trái Cây',
  rice: 'Gạo',
  meat_poultry_and_eggs: 'Thịt & Trứng',
  fish_seafood_and_frozen_food: 'Thủy Hải Sản & Đông Lạnh',
  fish_seafood: 'Hải Sản',
  milk_and_dairy_products: 'Sữa & Sản Phẩm Làm Từ Sữa',
  grocery_stores_and_supermarkets: 'Bách Hóa & Siêu Thị',
  convenience_stores: 'Cửa hàng tiện ích',
  spices_and_seasonings: 'Gia Vị & Phụ Liệu',
  drinks_and_beverages: 'Nước Giải Khát & Rượu Bia',
  sweets_and_snacks: 'Bánh Kẹo',
  stationaries: 'Văn Phòng Phẩm',
  households: 'Đồ Dùng Gia Đình',
  electronic_products: 'Đồ Điện Tử',
  mom_baby_and_kids: 'Mẹ & Bé',
  we_would_love_to_hear_from_you_in_order_for_us_to_improve_our_service_quality_please_kindly_leave_a_feedback_about_your_experience:
    'Nhằm nâng cao chất lượng phục vụ, Lomart rất mong nhận được phản hồi của quý khách về shipper cũng như dịch vụ của Lomart.',
  this_shipper_is_a_new_comer_and_may_lack_of_experiences_should_there_be_any_inconveniences_regards_of_our_shippers_services_loship_would_like_to_offer_our_sincerest_apologize_to_you_and_hope_you_understand:
    'Đây là shipper mới, nên chưa có nhiều kinh nghiệm. Nếu xảy ra sơ sót trong quá trình phục vụ, Lomart mong quý khách thông cảm và bỏ qua.',
  what_is_loship: 'Lomart là gì?',
  contact_our_customer_service: 'Tổng đài giải đáp thắc mắc về Lomart:',
  boost_your_business_by_partnering_with_loship:
    'Cơ hội để phát triển thương hiệu/cửa hàng bằng việc hợp tác cùng Lomart.',
  recruiting_loship_warrior: 'Trở thành chiến binh LOMART',
  become_a_loship_warrior_today: 'Trở thành Chiến binh Lomart ngay hôm nay!',
  rules_of_loship_warrior: 'Quy tắc ứng xử của Chiến binh Lomart',
  dont_forget_to_follow_us: 'Cùng theo dõi Lomart ngay tại đây!',
  loship_driver: 'Tài xế Lomart',
  take_loads_of_orders_without_worrying_how_to_deliver_them_to_the_customers_for_loship_is_here:
    'Có thêm nhiều đơn hàng, mà không cần phải lo lắng về việc giao nhận, vì đã có Lomart giúp bạn!',
  for_support: ' để Lomart giải quyết giúp bạn nhé!',
  promotions_occur_on_loship_oftenly_dont_forget_to_use_loship_everyday_to_get_great_deals:
    'Chương trình khuyến mãi thuờng xuyên diễn ra trên Lomart. Đừng quên truy cập Lomart mỗi ngày để nhận được nhiều ưu đãi hấp dẫn nhé!',
  most_favored_products: 'TOP MẶT HÀNG ĐƯỢC YÊU THÍCH',
  see_all_products_at_0: 'Xem tất cả sản phẩm tại {0}',
  no_products_found: 'Không tìm thấy sản phẩm nào.',
  item_added: 'Đã thêm sản phẩm!',
  do_you_want_to_continue_shopping_at: 'Bạn có muốn tiếp tục mua hàng tại',
  click_go_to_store_to_see_all_products_listed_here: 'Nhấn "Vào cửa hàng" để xem toàn bộ sản phẩm có bán tại đây nhé.',
  go_to_store: 'Vào cửa hàng',
  select: 'Chọn',
  error_occurred_please_place_a_new_order_we_are_sorry_for_this_inconvenient:
    'Đã xảy ra lỗi khi đặt hàng, bạn vui lòng đặt lại đơn hàng mới. Lomart xin lỗi bạn vì sự bất tiện này!',
  a_merchant_must_have_an_address_and_geo_location_before_having_a_menu:
    'Cửa hàng bắt buộc phải có địa chỉ và xác định tọa độ trên bản đồ trước khi tạo menu.',
  a_merchant_must_have_an_avatar_picture_for_better_display_on_loship_before_having_a_menu:
    'Cửa hàng bắt buộc phải có ảnh đại diện để hiển thị trên Lomart tốt hơn, trước khi tạo menu.',
  this_merchant_is_not_ready_for_order: 'Cửa hàng chưa đủ thông tin!',
  a_merchant_on_loship_must_have_a_username_in_order_to_create_short_link_for_it_like_loshipvntencuahang:
    'Cửa hàng trên Lomart bắt buộc phải có username, để tạo đường link gọn đẹp, VD: lomart.vn/tencuahang',
  thank_you_for_using_loship_you_will_receive_a_message_to_confirm_your_order_in_a_few_seconds:
    'Cảm ơn bạn đã sử dụng dịch vụ của Lomart, bạn sẽ nhận được tin nhắn xác nhận đơn hàng trong vài giây tới.',
  thank_you_for_using_loship_as_your_food_delivery_service_now_you_just_need_to_login_and_your_order_will_be_placed:
    'Cảm ơn bạn đã đặt món giao tận nơi với Lomart.vn. Bây giờ, bạn chỉ cần đăng nhập là hoàn tất việc đặt món rồi!',
  log_in_to_place_orders_and_get_your_food_delivered_within_1_hour:
    'Đăng nhập và đặt đồ ăn thức uống giao tận nơi trong một giờ với Lomart!',
  register_to_place_orders_and_get_your_food_delivered_within_1_hour:
    'Đăng ký và đặt đồ ăn thức uống giao tận nơi trong một giờ với Lomart!',
  cannot_create_username_please_contact_us_at_hotroloshiplozivn_for_support:
    'Không thể tạo tài khoản, rất xin lỗi bạn! Hãy liên hệ Lomart qua email hotroloship@lozi.vn để được hỗ trợ nhé.',
  unknown_error_please_contact_us_at_hotroloshiplozivn_sorry_and_thank_you_for_choosing_loship_today:
    'Lỗi không xác định. Vui lòng liên hệ hotroloship@lozi.vn để được hỗ trợ. Rất xin lỗi và cảm ơn quý khách đã chọn Lomart hôm nay!',
  thank_you_for_using_loship: 'Cảm ơn bạn đã sử dụng dịch vụ Lomart!',
  we_are_currently_overloaded_and_could_not_serve_all_of_your_orders_we_are_temporarily_stop_taking_new_orders_in_2_hours_in_order_to_finish_all_the_remaining_orders_we_thank_you_for_your_patience_and_please_come_back_after:
    'Xin lỗi vì Lomart đang quá tải nên chưa phục vụ được tốt tất cả yêu cầu đặt món từ anh/chị. Lomart sẽ tạm thời ngưng nhận đặt đơn hàng trong vòng 2 giờ, để phục vụ tất cả các đơn hiện có. Nhằm đảm bảo việc phục vụ anh/chị yêu mến Lomart tốt nhất có thể, mời anh/chị quay lại sau ',
  how_do_you_think_about_our_shipper: 'Quý khách có hài lòng với shipper của Lomart không?',
  please_write_your_feedback_about_our_shipper_to_help_us_improve_our_service_optional:
    'Quý khách hãy viết phản hồi hoặc góp ý về shipper để Lomart nâng cao chất lượng dịch vụ nhé! (không bắt buộc)',
  please_write_your_feedback_about_our_shipper_to_help_us_improve_our_service:
    'Quý khách hãy viết phản hồi hoặc góp ý về shipper để Lomart nâng cao chất lượng dịch vụ nhé!',
  oh_no_we_are_sorry_for_your_bad_experience_what_happened_tell_us_more_and_we_wont_let_that_happen_again_min_15_words:
    'Ôi không, Lomart rất tiếc khi quý khách đã có những trải nghiệm không tốt. Chuyện gì đã xảy ra? Xin hãy kể cho Lomart, để chúng tôi có thể mang đến cho quý khách dịch vụ tốt hơn nhé! (Tối thiểu 15 từ)',
  tell_us_more_how_was_your_food_was_the_shipper_nice_do_you_think_there_is_anything_loship_should_improve_min_3_words:
    'Hãy viết thêm chút nữa! Đồ ăn giao cho bạn vẫn trong tình trạng tốt? Shipper thân thiện? Có điều gì mà Lomart cần cải thiện hơn không? (Tối thiểu 3 từ)',
  allow_location_title: 'Lomart chưa thể truy cập vào định vị của bạn',
  promo_notice_minimum_total:
    'Chú ý: Mã giảm giá chỉ áp dụng với đơn hàng từ {0} trở lên. Hãy chọn thêm sản phẩm để hưởng ưu đãi nhé!',
  promo_notice_campain_fixed_price:
    'Chú ý: Mã giảm giá không thể áp dụng đồng thời với chương trình đồng giá. Hãy chọn lại sản phẩm hoặc dùng mã lần sau để hưởng ưu đãi tối đa nhé.',
  item_sharing_content: 'Xem thử {0} đang bán trên Lomart nhé, giá hấp dẫn cực!',
  contact_loship: 'Liên hệ Lomart',
  please_let_us_know_why_cancel: 'Hãy cho Lomart biết lý do hủy đơn, nhằm cải thiện dịch vụ tốt hơn nhé.',
  group_order: 'Mua Với Nhóm',
  treats_for_groups: 'Địa điểm mua với nhóm',
};
