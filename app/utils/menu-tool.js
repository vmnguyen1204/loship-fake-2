export const MIN_MAIN_DISH_PRICE = 12000; // (Main) Dish price must not lower than this value

export function isGroupDishHasInvalidDish(dishes = [], minPrice = MIN_MAIN_DISH_PRICE) {
  let isInvalidDishExist = false;
  let index = 0;
  const groupDishes = typeof dishes.toJS === 'function' ? dishes.toJS() : dishes;
  if (groupDishes && groupDishes.length > 0) {
    for (let i = 0; i < groupDishes.length; i++) {
      index = i;
      const additionalFee = groupDishes[i].additionalFee || 0;
      if (groupDishes[i].active && groupDishes[i].price + additionalFee < minPrice) {
        isInvalidDishExist = true;
        i = groupDishes.length;
        break;
      }
    }
  }
  if (isInvalidDishExist) return groupDishes[index];
  return null;
}
