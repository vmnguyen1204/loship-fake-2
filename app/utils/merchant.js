import { CONFIRM_POPUP, ORDER_FAILED, open } from 'actions/popup';

import Checkbox from 'components/shared/checkbox';
import React from 'react';
import { cloneDeep } from 'lodash';
import cx from 'classnames';
import { getString } from 'utils/i18n';
import moment from 'utils/shims/moment';
import { getDeliveryTime } from 'utils/cart';
import { addCurrency } from 'utils/format';
import { servicesAvailable } from 'utils/json/loziServices';

export function isMerchantReady(
  merchant = {},
  { callback, dispatch, history = {}, suggestEateriesUrl } = {},
) {
  const {
    shippingService = 'loship',
    isAvailable = true,
    closed,
    isActive,
    isCheckedIn,
    operatingStatus: { isOpening, minutesUntilNextStatus } = {},
  } = merchant;

  const isServiceAvailable = servicesAvailable.includes(shippingService);
  const merchantReadyStatus = isServiceAvailable &&
    isAvailable && !closed && isCheckedIn && isActive && isOpening;
  if (typeof dispatch !== 'function' || typeof getString !== 'function' || typeof history === 'undefined') return merchantReadyStatus;

  if (!isServiceAvailable) {
    dispatch(
      open(ORDER_FAILED, {
        title: getString('only_available_on_mobile_app'),
        error: () => (
          <div>
            <p>
              {getString(
                'the_service_you_are_requesting_is_only_available_on_loship_app_download_app_on_your_phone_to_use_the_service',
              )}
            </p>
            <div className="promo-btns">
              <a href="https://goo.gl/N5Vs4V" className="app-img app-android" target="_blank" rel="noopener">
                <img src="/dist/images/app-android.png" alt="android" style={{ maxWidth: 120, marginRight: 8 }} />
              </a>
              <a href="https://goo.gl/CDFMhZ" className="app-img app-ios" target="_blank" rel="noopener">
                <img src="/dist/images/app-ios.png" alt="ios" style={{ maxWidth: 120 }} />
              </a>
            </div>
          </div>
        ),
        hideBanner: true,
        action: () => { window.location.href = '/'; },
        actionText: getString('return_to_homepage'),
      }),
    );
  } else if (!isAvailable) {
    dispatch(
      open(CONFIRM_POPUP, {
        title: getString('this_location_is_not_available_for_ordering'),
        content: getString('were_sorry_to_inform_that_loship_are_not_yet_available_in_this_area'),
        contentClassName: 'mw-400',
        actionClassName: 'flex-column-reverse',
        confirmBtn: suggestEateriesUrl
          ? {
            text: getString('view_similar_merchant', 'order'),
            className: 'btn-fw',
          }
          : { className: 'd-none' },
        cancelBtn: { text: getString('go_back', 'order'), type: 'link', className: 'text-dark-gray p8_8' },
        onConfirm: () => {
          if (suggestEateriesUrl) history.push(suggestEateriesUrl);
        },
      }),
    );
  } else if (closed) {
    dispatch(
      open(CONFIRM_POPUP, {
        title: getString('permanently_closed', 'eatery'),
        content: getString('you_cannot_order_at_this_shop_because_it_is_closed_sorry', 'eatery'),
        contentClassName: 'mw-400',
        actionClassName: 'flex-column-reverse',
        confirmBtn: suggestEateriesUrl
          ? {
            text: getString('view_similar_merchant', 'order'),
            className: 'btn-fw',
          }
          : { className: 'd-none' },
        cancelBtn: { text: getString('go_back', 'order'), type: 'link', className: 'text-dark-gray p8_8' },
        onConfirm: () => {
          if (suggestEateriesUrl) history.push(suggestEateriesUrl);
        },
      }),
    );
  } else if (!isActive) {
    dispatch(
      open(CONFIRM_POPUP, {
        title: getString('temporary_closed', 'eatery'),
        content: getString(
          'sorry_this_location_is_currently_closed_please_choose_other_locations_or_come_back_later',
          'eatery',
        ),
        contentClassName: 'mw-400',
        actionClassName: 'flex-column-reverse',
        confirmBtn: suggestEateriesUrl
          ? {
            text: getString('view_similar_merchant', 'order'),
            className: 'btn-fw',
          }
          : { className: 'd-none' },
        cancelBtn: { text: getString('go_back', 'order'), type: 'link', className: 'text-dark-gray p8_8' },
        onConfirm: () => {
          if (suggestEateriesUrl) history.push(suggestEateriesUrl);
        },
      }),
    );
  } else if (!isCheckedIn) {
    dispatch(
      open(CONFIRM_POPUP, {
        title: getString('temporary_not_orders'),
        content: getString(
          'sorry_this_location_is_currently_out_of_service_and_cannot_take_new_orders_please_come_back_later',
          'eatery',
        ),
        contentClassName: 'mw-400',
        actionClassName: 'flex-column-reverse',
        confirmBtn: suggestEateriesUrl
          ? {
            text: getString('view_similar_merchant', 'order'),
            className: 'btn-fw',
          }
          : { className: 'd-none' },
        cancelBtn: { text: getString('go_back', 'order'), type: 'link', className: 'text-dark-gray p8_8' },
        onConfirm: () => {
          if (suggestEateriesUrl) history.push(suggestEateriesUrl);
        },
      }),
    );
  } else if (!isOpening) {
    if (merchant.isAllowPreOrder) return true;

    const deliveryTime = getDeliveryTime(
      moment()
        .add(minutesUntilNextStatus, 'minutes')
        .add(1, 'hour'),
      true,
    );
    dispatch(
      open(CONFIRM_POPUP, {
        content: () => {
          return (
            <div className="text-left">
              <p>
                {getString('0_is_out_of_service_for_now_but_you_can_place_a_pre_order', 'order', [
                  <b key="name">{merchant.name}</b>,
                ])}
              </p>
              <p>
                {getString('your_ordered_items_will_be_delivered', 'order', [
                  <b key="name">{getString(deliveryTime.string, 'order', [deliveryTime.time])}</b>,
                ])}
              </p>
            </div>
          );
        },
        contentClassName: 'mw-400',
        actionClassName: 'flex-column-reverse',
        confirmBtn: suggestEateriesUrl
          ? { text: getString('view_similar_merchant', 'order'), className: 'btn-fw' }
          : { text: getString('continue_ordering', 'order'), className: 'btn-fw' },
        cancelBtn: suggestEateriesUrl
          ? { text: getString('continue_ordering', 'order'), type: 'link', className: 'text-dark-gray p8_8' }
          : { text: getString('go_back', 'order'), type: 'link', className: 'text-dark-gray p8_8' },
        onConfirm: () => {
          if (suggestEateriesUrl) {
            history.push(suggestEateriesUrl);
          } else {
            dispatch({
              type: 'MERCHANT_ALLOW_PRE_ORDER',
              key: merchant.slug,
            });
          }
          typeof callback === 'function' && callback();
        },
        onCancel: () => {
          if (suggestEateriesUrl) {
            dispatch({
              type: 'MERCHANT_ALLOW_PRE_ORDER',
              key: merchant.slug,
            });
          }
        },
      }),
    );
  }

  return merchantReadyStatus;
}

export const isLoshipClients = (data = []) => {
  return data.every((item) => {
    return item === 20 || item === 30;
  });
};

export function prepareOrderDetail() {}

// MENU TOOL

export function generateCompactMenu(srcMenu, merchant, allowInactive = true) {
  const oldData = fetchOldData(merchant);

  function getCustomOptionsData(customOptions) {
    return customOptions.reduce((res, customOption) => {
      if (!allowInactive && !customOption.active) return res;
      const oldItem = oldData.find((item) => item.id === customOption.id && item.type === 'customOption');
      return res.concat({
        id: customOption.id,
        active: customOption.active,
        name: customOption.value,
        oldName: oldItem && oldItem.name,
        price: customOption.price,
        limitQuantity: customOption.limitQuantity,
      });
    }, []);
  }
  function getCustomsData(customs) {
    return customs.reduce((res, custom) => {
      if (!allowInactive && !custom.active) return res;
      const oldItem = oldData.find((item) => item.id === custom.id && item.type === 'custom');
      return res.concat({
        id: custom.id,
        active: custom.active,
        name: custom.name,
        oldName: oldItem && oldItem.name,
        selectionType: custom.selectionType,
        limitQuantity: custom.limitQuantity,
        note: custom.note,
        customOptions: getCustomOptionsData(custom.customOptions || []),
      });
    }, []);
  }
  function getDishesData(dishes) {
    return dishes.reduce((res, dish) => {
      if (!allowInactive && !dish.active) return res;
      const oldItem = oldData.find((item) => item.id === dish.id && item.type === 'dish');
      return res.concat({
        id: dish.id,
        active: dish.active,
        name: dish.name,
        oldName: oldItem && oldItem.name,
        price: dish.price,
        additionalFee: dish.additionalFee,
        description: dish.description,
        image: dish.image,
        customs: getCustomsData(dish.customs || []),
        unit: dish.unit,
        unitQuantity: dish.unitQuantity,
      });
    }, []);
  }

  return srcMenu.reduce((res, group) => {
    if (!allowInactive && !group.active) return res;
    const oldItem = oldData.find((item) => item.id === group.id && item.type === 'group');
    return res.concat({
      id: group.id,
      active: group.active,
      isPinned: group.isPinned,
      name: group.name,
      oldName: oldItem && oldItem.name,
      isExtraGroupDish: group.isExtraGroupDish,
      dishes: getDishesData(group.dishes || []),
    });
  }, []);
}

export function acquireDiffInfo(src, res) {
  const srcMenu = cloneDeep(src);
  const destMenu = cloneDeep(res);

  function acquireDiffInfoCustomOptions(srcCustomOptions, destCustomOptions) {
    srcCustomOptions.forEach((srcCustomOption) => {
      const destCustomOption = destCustomOptions.find(
        (c) => (c.name === srcCustomOption.oldName || c.name === srcCustomOption.name) && !c.srcMap,
      );
      if (!destCustomOption) {
        return destCustomOptions.push({
          ...srcCustomOption,
          id: `new_${srcCustomOption.id}`,
          srcMap: srcCustomOption.id,
          method: 'POST',
        });
      }

      destCustomOption.srcMap = srcCustomOption.id;
      const { active, name, oldName, price, limitQuantity } = srcCustomOption;
      if (
        active === destCustomOption.active &&
        name === destCustomOption.name &&
        price === destCustomOption.price &&
        limitQuantity === destCustomOption.limitQuantity
      ) return;

      destCustomOption.method = 'PUT';
      destCustomOption.newData = { active, name, oldName, price, limitQuantity };
      srcCustomOption.method = 'PUT';
    });
    return [srcCustomOptions, destCustomOptions];
  }

  function acquireDiffInfoCustoms(srcCustoms, destCustoms) {
    srcCustoms.forEach((srcCustom) => {
      const destCustom = destCustoms.find(
        (c) => (c.name === srcCustom.oldName || c.name === srcCustom.name) && !c.srcMap,
      );
      if (!destCustom) return destCustoms.push({ ...srcCustom, id: `new_${srcCustom.id}`, srcMap: srcCustom.id, method: 'POST' });

      destCustom.srcMap = srcCustom.id;
      [srcCustom.customOptions, destCustom.customOptions] = acquireDiffInfoCustomOptions(
        srcCustom.customOptions || [],
        destCustom.customOptions || [],
      );
      const { active, name, oldName, selectionType, limitQuantity, note } = srcCustom;
      if (
        active === destCustom.active &&
        name === destCustom.name &&
        selectionType === destCustom.selectionType &&
        limitQuantity === destCustom.limitQuantity &&
        note === destCustom.note
      ) return;

      destCustom.method = 'PUT';
      destCustom.newData = { active, name, oldName, selectionType, limitQuantity, note };
      srcCustom.method = 'PUT';
    });
    return [srcCustoms, destCustoms];
  }

  function acquireDiffInfoDishes(srcDishes, destDishes) {
    srcDishes.forEach((srcDish) => {
      const destDish = destDishes.find((d) => (d.name === srcDish.oldName || d.name === srcDish.name) && !d.srcMap);
      if (!destDish) return destDishes.push({ ...srcDish, id: `new_${srcDish.id}`, srcMap: srcDish.id, method: 'POST' });

      destDish.srcMap = srcDish.id;
      [srcDish.customs, destDish.customs] = acquireDiffInfoCustoms(srcDish.customs || [], destDish.customs || []);
      const { active, name, oldName, price, additionalFee, description, image, unit, unitQuantity } = srcDish;
      if (
        active === destDish.active &&
        name === destDish.name &&
        price === destDish.price &&
        additionalFee === destDish.additionalFee &&
        description === destDish.description &&
        image === destDish.image &&
        unit === destDish.unit &&
        unitQuantity === destDish.unitQuantity
      ) return;

      destDish.method = 'PUT';
      destDish.newData = { active, name, oldName, price, additionalFee, description, image, unit, unitQuantity };
      srcDish.method = 'PUT';
    });
    return [srcDishes, destDishes];
  }

  srcMenu.forEach((srcGroup) => {
    let destGroup = destMenu.find(
      (g) => g.active === srcGroup.active && (g.name === srcGroup.oldName || g.name === srcGroup.name) && !g.srcMap,
    );
    if (!destGroup) destGroup = destMenu.find((g) => (g.name === srcGroup.oldName || g.name === srcGroup.name) && !g.srcMap);
    if (!destGroup) {
      return destMenu.push({
        ...srcGroup,
        id: `new_${srcGroup.id}`,
        srcMap: srcGroup.id,
        method: 'POST',
        dishes: srcGroup.dishes.map((d) => ({ ...d, srcMap: d.id })), // Add srcMap for copyImageFromDish
      });
    }

    destGroup.srcMap = srcGroup.id;
    [srcGroup.dishes, destGroup.dishes] = acquireDiffInfoDishes(srcGroup.dishes || [], destGroup.dishes || []);
    const { active, isPinned, name, oldName, isExtraGroupDish } = srcGroup;
    if (
      active === destGroup.active &&
      isPinned === destGroup.isPinned &&
      name === destGroup.name &&
      isExtraGroupDish === destGroup.isExtraGroupDish
    ) return;

    destGroup.method = 'PUT';
    destGroup.newData = { active, name, isPinned, oldName, isExtraGroupDish };
    srcGroup.method = 'PUT';
  });
  return [srcMenu, destMenu];
}

export function acquireDiffCount(menu) {
  let diffCount = 0;

  function acquireDiffCountFromCustomOptions(customOptions, inheritPost) {
    customOptions.forEach((customOption) => {
      if (inheritPost || ['POST', 'PUT'].includes(customOption.method)) diffCount++;
      else if (!customOption.srcMap) diffCount++;
    });
  }
  function acquireDiffCountFromCustoms(customs, inheritPost) {
    customs.forEach((custom) => {
      if (inheritPost || ['POST', 'PUT'].includes(custom.method)) diffCount++;
      else if (!custom.srcMap) diffCount++;
      acquireDiffCountFromCustomOptions(custom.customOptions || [], inheritPost || custom.method === 'POST');
    });
  }
  function acquireDiffCountFromDishes(dishes, inheritPost) {
    dishes.forEach((dish) => {
      if (inheritPost || ['POST', 'PUT'].includes(dish.method)) diffCount++;
      else if (!dish.srcMap) diffCount++;
      acquireDiffCountFromCustoms(dish.customs || [], inheritPost || dish.method === 'POST');
    });
  }
  menu.forEach((group) => {
    if (['POST', 'PUT'].includes(group.method)) diffCount++;
    else if (!group.srcMap) diffCount++;
    acquireDiffCountFromDishes(group.dishes || [], group.method === 'POST');
  });

  return diffCount;
}

export function generateDiffEditor(
  srcMenu,
  destMenu,
  {
    onToggleAcceptDiff,
    onToggleAcceptGroup,
    onExpandCustom,
    acceptedDiff = {},
    expandedCustom = {},
    isPerforming,
  } = {},
) {
  const diffLines = [];
  function extraGenerator(type, data, src, option = {}) {
    const srcData = src || data.newData || data;
    const isNew = !!src; // New side (right) have to send `src`
    if (type === 'group') {
      return (
        <span>
          {data.isExtraGroupDish && (
            <small>({getString('phụ')})</small>
          )}
          {data.isPinned !== srcData.isPinned && (
            <i
              className={cx('fa fa-star pull-right', { isPinned: data.isPinned })}
              title={getString('ispinned', 'menuManage')}
            />
          )}
        </span>
      );
    }
    if (type === 'dish') {
      if (data.method === 'POST' || srcData.price !== data.price || srcData.additionalFee !== data.additionalFee) {
        return (
          <span className="extra">
            {addCurrency(data.price)}
            {data.additionalFee > 0 && (
              <small>
              (+
                {addCurrency(data.additionalFee)}
              )
              </small>
            )}
          </span>
        );
      }
      if (isNew && srcData.image !== data.image) {
        return (
          <span className="extra">
            (
            {getString('thay đổi ảnh')}
            )
          </span>
        );
      }
      if (isNew && srcData.description !== data.description) {
        return (
          <span className="extra">
            (
            {getString('thay đổi ghi chú')}
            )
          </span>
        );
      }
      if (isNew && (srcData.unit !== data.unit || srcData.unitQuantity !== data.unitQuantity)) {
        return (
          <span className="extra">
            (
            {getString('Thay đổi quy cách')}
            )
          </span>
        );
      }
      return;
    }
    if (type === 'custom') {
      const showCaret = option.renderExpand && Array.isArray(data.customOptions) && data.customOptions.length > 0;
      const extra = (() => {
        if (data.method === 'POST' || srcData.limitQuantity !== data.limitQuantity) {
          return (
            <span className="extra">
              {getString('Đặt tối đa')}
              :
              {data.limitQuantity}
            </span>
          );
        }
        if (srcData.note !== data.note) {
          return (
            <span className="extra">
              (
              {getString('thay đổi ghi chú')}
              )
            </span>
          );
        }
      })();
      return (
        <span>
          {showCaret && (
            <i
              className={cx('far', expandedCustom[`custom-${data.id}`] ? 'fa-caret-square-up' : 'fa-caret-square-down')}
              onClick={() => onExpandCustom(`custom-${data.id}`)}
            />
          )}
          {extra}
        </span>
      );
    }
    if (type === 'customOption') {
      if (data.method === 'POST' || srcData.price !== data.price) {
        return <span className="extra">{addCurrency(data.price)}</span>;
      }
      if (srcData.limitQuantity !== data.limitQuantity) {
        return (
          <span className="extra">
            {getString('Đặt tối đa')}
            :
            {data.limitQuantity}
          </span>
        );
      }
    }
  }
  function checkLinkGenerator(isAccepted, lineId, handler) {
    const isChecked = isAccepted !== false;
    return {
      checked: isChecked,
      onChange: (checked) => handler(lineId, checked),
    };
  }
  function generateDiffLine(srcData, destData = {}, { type, inheritPost } = {}) {
    const targetSrcData = srcData || destData.newData || destData;
    const showIndicator = type === 'group' || type === 'dish';
    const isPost = destData.method === 'POST' || inheritPost;
    const isDelete = !isPost && !destData.srcMap && destData.active;
    const isChanged = !!(isPost || isDelete || destData.method);

    const showCheckboxAcceptGroup = typeof onToggleAcceptGroup === 'function' && type === 'group';
    const showCheckboxAcceptDiff = (() => {
      if (showIndicator || typeof onToggleAcceptDiff !== 'function') return false;
      if (type === 'custom') return true;
      if (type === 'customOption') return isChanged;
      return isChanged;
    })();

    const lineId = `${type}-${destData.id}`;
    const [diff, childrenDiff] = [[], []];
    let shouldRenderLine = showIndicator || isChanged;
    let shouldRenderExpand = false;

    const [childType, childKey] = (() => {
      if (type === 'group') return ['dish', 'dishes'];
      if (type === 'dish') return ['custom', 'customs'];
      if (type === 'custom') return ['customOption', 'customOptions'];
      return [];
    })();
    if (childKey) {
      (targetSrcData[childKey] || []).forEach((item) => {
        const destItem = destData[childKey].find((c) => c.srcMap === item.id || (isPost && c.name === item.name));
        if (destItem) {
          const tmpDiff = generateDiffLine(item, destItem, {
            type: childType,
            inheritPost: isPost,
          });
          if (tmpDiff.length > 0) {
            shouldRenderLine = true;
            shouldRenderExpand = true;
          }
          childrenDiff.push(...tmpDiff);
        }
      });
      (destData[childKey] || [])
        .filter((c) => !c.srcMap && !isPost)
        .forEach((destItem) => {
          const tmpDiff = generateDiffLine(undefined, destItem, {
            type: childType,
            inheritPost: isPost,
          });
          if (tmpDiff.length > 0) {
            shouldRenderLine = true;
            shouldRenderExpand = true;
          }
          childrenDiff.push(...tmpDiff);
        });
    }

    if (showIndicator || expandedCustom[`${type}-${destData.id}`] || expandedCustom[`${type}-${targetSrcData.id}`]) diff.push(...childrenDiff);

    if (shouldRenderLine) {
      diff.unshift(
        <div key={lineId} className={cx('diff-line', type)}>
          {isPost ? (
            <div className="diff-line-item empty" />
          ) : (
            <div className={cx('diff-line-item', destData.method, { DELETE: isDelete })}>
              <div className="diff-line-item-content">
                {showIndicator && <span className={cx('status-indicator', { active: destData.active })} />}
                {destData.name}
                {extraGenerator(type, { ...destData, method: isPost ? 'POST' : destData.method }, undefined, { renderExpand: shouldRenderExpand })}
              </div>
            </div>
          )}
          <div className={cx('diff-line-item new', isPost ? 'POST' : destData.method, { DELETE: isDelete })}>
            <div className="diff-line-item-content">
              {showIndicator && <span className={cx('status-indicator', { active: !isDelete && targetSrcData.active })} />}
              {showCheckboxAcceptDiff && (
                <Checkbox
                  className="reverse"
                  {...checkLinkGenerator(acceptedDiff[lineId], lineId, onToggleAcceptDiff)}
                  disabled={isPerforming}
                >
                  {targetSrcData.name}
                </Checkbox>
              )}
              {showCheckboxAcceptGroup && (
                <Checkbox
                  className="reverse"
                  {...checkLinkGenerator(acceptedDiff[lineId], lineId, onToggleAcceptGroup)}
                  disabled={isPerforming}
                >
                  {targetSrcData.name}
                </Checkbox>
              )}
              {!showCheckboxAcceptDiff && !showCheckboxAcceptGroup && targetSrcData.name}
              {extraGenerator(type, { ...targetSrcData, method: isPost ? 'POST' : targetSrcData.method }, destData, { renderExpand: shouldRenderExpand })}
            </div>
          </div>
        </div>,
      );
    }
    return diff;
  }

  (srcMenu || []).forEach((srcGroup) => {
    const destGroup = destMenu.find((g) => g.srcMap === srcGroup.id);
    if (destGroup) diffLines.push(...generateDiffLine(srcGroup, destGroup, { type: 'group' }));
  });
  (destMenu || [])
    .filter((g) => !g.srcMap)
    .forEach((destGroup) => {
      diffLines.push(...generateDiffLine(undefined, destGroup, { type: 'group' }));
    });

  return diffLines;
}

export function keepOldData(data, merchant) {
  const key = `${merchant}.menu.oldData`;
  try {
    const oldData = JSON.parse(localStorage.getItem(key)) || [];
    const dIndex = oldData.findIndex((item) => item.id === data.id && item.type === data.type);
    if (dIndex < 0) oldData.push(data);
    else oldData.splice(dIndex, 1, data);

    localStorage.setItem(key, JSON.stringify(oldData));
  } catch (e) {
    console.error('ERROR: cannot save old data', e, localStorage.getItem(key));
    localStorage.removeItem(key);
  }
}
export function fetchOldData(merchant) {
  const key = `${merchant}.menu.oldData`;
  try {
    const oldData = localStorage.getItem(key);
    return JSON.parse(oldData) || [];
  } catch (e) {
    console.error('ERROR: cannot parse old data', e, localStorage.getItem(key));
    localStorage.removeItem(key);
  }
}
export function cleanUpOldData(merchant) {
  localStorage.removeItem(`${merchant}.menu.oldData`);
}
export function mergeWithOldData(newData, merchant) {
  const oldData = fetchOldData(merchant);
  const oldItem = oldData.find((item) => {
    if (newData.type === 'custom' && item.type !== 'custom') return false;
    if (newData.type === 'customOption' && item.type !== 'customOption') return false;
    return item.id === newData.id;
  });
  return { ...newData, oldName: oldItem && oldItem.name };
}
