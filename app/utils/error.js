export function parseError(rawErrorString) {
  try {
    const errorObj = JSON.parse(rawErrorString.trim());
    if (!errorObj.errors || errorObj.errors.length < 1) {
      throw new Error('Cannot parse error');
    }
    return errorObj.errors[0].message;
  } catch (e) {
    console.error(e);
    console.error('RAW:', rawErrorString);
    return 'unknown_error_please_contact_us_at_hotroloshiplozivn_sorry_and_thank_you_for_choosing_loship_today';
  }
}

export function parseErrorV2(error) {
  try {
    const { errors } = JSON.parse(error.message);

    if (!Array.isArray(errors)) {
      return { message: errors };
    }

    return {
      code: errors[0].code,
      codeEnum: (errors[0].codeEnum || '').toLowerCase(),
      errorData: errors[0].errorData,
      message: errors[0].message.toLowerCase().replace(/ /g, '_'),
    };
  } catch (e) {
    console.error(e);
    return {
      codeEnum: 'unknown_error',
      message: 'unknown_error_please_contact_us_at_hotroloshiplozivn_sorry_and_thank_you_for_choosing_loship_today',
    };
  }
}
