export function trimContent(content) {
  if (!content) return '';
  return content
    .trim()
    .replace(/(\r\n|\n|\r)/g, '\n')
    .replace(/[\n]{1,}/g, '\n\n')
    .replace(/\n\n/, '\n');
}

export function commentLineBreak(content) {
  if (!content) return '';
  return content
    .trim()
    .replace(/(\r\n|\n|\r)/g, '\n')
    .replace(/[\n]{1,}/g, '\n\n');
}

export function removeQuotes(content) {
  if (!content) return '';
  return content
    .trim()
    .replace(/( "| ')/g, ' “')
    .replace(/(" |' )/g, '” ')
    .replace(/"/g, '');
}

export function plains(content) {
  if (!content) return '';
  return removeQuotes(content)
    .replace(/(\r\n|\n|\r)/g, ' ')
    .replace(/ {2,}/g, ' ');
}

export function flats(arr) {
  const normalizedArr = arr.filter((n) => !!n);
  if (normalizedArr && normalizedArr.length > 0) return normalizedArr.join(', ');
  return '';
}

export function addCurrency(n, currency = 'đ') {
  let num = parseInt(n);

  if (!num && num !== 0) return '';

  const isNegative = num < 0;

  if (isNegative) {
    num *= -1;
  }

  return (
    (isNegative ? '-' : '') +
    num.toFixed(0).replace(/./g, (c, i, a) => {
      return i > 0 && c !== ',' && (a.length - i) % 3 === 0 ? '.' + c : c;
    }) +
    ' ' +
    currency
  );
  // ₫
}

export function numberThousand(n) {
  const num = parseInt(n);

  if (!num && num !== 0) return '';

  return num.toFixed(0).replace(/./g, (c, i, a) => {
    return i > 0 && c !== ',' && (a.length - i) % 3 === 0 ? '.' + c : c;
  });
}

export function couplePrice(price) {
  if (!price || price <= 0) return '';

  return `${addCurrency(price)}/for two`;
}

// ucs-2 string to base64 encoded ascii
export function utoa(str) {
  return window.btoa(unescape(encodeURIComponent(str.toString())));
}
// base64 encoded ascii to ucs-2 string
export function atou(str) {
  return decodeURIComponent(escape(window.atob(str.toString())));
}

export function round(value, precision) {
  const c = 10 ** precision;
  const r = Math.round(value * c) / c;
  return r.toFixed(precision);
}

export function roundNumber(num, precision) {
  const c = 10 ** precision;
  return Math.round(num * c) / c;
}

// ! Only send countryCode for display purpose.
export function phoneNumber(n, { countryCode = '84', injectZero = true } = {}) {
  if (!n) return '';

  function parse(num) {
    const result = [];
    const len = num.length;
    for (let i = len - 1; i >= 0; i--) {
      result.unshift(num[i]);
      if ((len - i) % 3 === 0) {
        if (num[0] === '+') i >= 4 && result.unshift('.');
        else if (i >= 3) result.unshift('.');
      }
    }

    if (num[0] === '+') return result.join('');
    if (num[0] !== '0') {
      if (countryCode !== '84') result.unshift('+' + countryCode);
      else if (injectZero) result.unshift(0);
    }
    return result.join('');
  }
  const phoneList = n
    .replace(/\.|\s/g, '')
    .split('-')
    .map((s) => parse(s));
  return phoneList.join(' - ');
}

export function getRawPhoneNumber(n) {
  const formattedPhone = phoneNumber(n, { autoFormat: true });
  return formattedPhone.replace(/\./g, '');
}

export function ucFirstAllWords(string) {
  const pieces = string.split(' ');
  for (let i = 0; i < pieces.length; i++) {
    const j = pieces[i].charAt(0).toUpperCase();
    pieces[i] = j + pieces[i].substr(1).toLowerCase();
  }
  return pieces.join(' ');
}

export function sliceText(txt, quantiyChar = 0) {
  if (quantiyChar >= txt.length) return txt;
  const res = txt.slice(0, quantiyChar);
  return res + '...';
}

export function shuffleArray(array) {
  let currentIndex = array.length;
  let temporaryValue;
  let randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

export function addCommas(nStr = '0') {
  const x = String(nStr).split('.');
  let x1 = x[0];
  const x2 = x.length > 1 ? '.' + x[1] : '';
  const rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1.$2');
  }
  return x1 + x2;
}

export function removeCommas(nStr = '0') {
  if (Number.isInteger(nStr)) return nStr;
  return parseFloat(nStr.replace(/\./g, '').replace(/k/g, '000')) || 0;
}

export function b64EncodeUnicode(str) {
  // first we use encodeURIComponent to get percent-encoded UTF-8,
  // then we convert the percent encodings into raw bytes which
  // can be fed into btoa.
  return btoa(
    encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, (match, p1) => {
      return String.fromCharCode('0x' + p1);
    }),
  );
}

export function capitalizes(str) {
  if (!str) return '';
  return str
    .split(/\s+/)
    .map((v) => v[0] && v[0].toUpperCase() + v.slice(1))
    .join(' ');
}

export function isValidPhoneNumber(number, countryCode) {
  const phoneRegex = /^(0|84|840)?\d{8,11}$/;
  return phoneRegex.test(phoneNumber(number, { autoFormat: true, countryCode }).replace(/\D/g, ''));
}

export function isValidEmail(email) {
  const regex = /^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
  return regex.test(email);
}

// https://stackoverflow.com/a/37511463
export function normalize(str) {
  if (typeof str !== 'string') return str;
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}

// https://stackoverflow.com/a/48000411
export function slugify(str, connector = '-') {
  if (typeof str !== 'string') return str;
  const normalized = normalize(str);
  return normalized
    .toLowerCase()
    .replace(/\s+/g, '-') // Change whitespace to dashes
    .toLowerCase() // Change to lowercase
    .replace(/&/g, '-and-') // Replace ampersand
    .replace(/_/g, '-') // Replace underscore
    .replace(/[^a-z0-9-]/g, '') // Remove anything that is not a letter, number or dash
    .replace(/-+/g, '-') // Remove duplicate dashes
    .replace(/^-*/, '') // Remove starting dashes
    .replace(/-*$/, '') // Remove trailing dashes
    .replace(/-/g, connector);
}

export function formatCard(prefix, postfix) {
  return [prefix || '****', '****', '****', postfix || '****'].join(' ');
}
