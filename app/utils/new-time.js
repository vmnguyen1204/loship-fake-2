export function isPast(time) {
  if (!time) return true;
  const current = Date.parse(time);
  const now = Date.now();
  return current < now;
}

export const isInRange = (start, end) => {
  if (!start || !end) return false;
  const startTime = Date.parse(start);
  const endTime = Date.parse(end);
  const now = Date.now();
  return now >= startTime && now <= endTime;
};
