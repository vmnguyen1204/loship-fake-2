import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { lazy } from 'utils/async';
import * as cookie from 'utils/shims/cookie';

let Languages;
export async function loadLanguage() {
  if (Languages) return;
  const res = await lazy(() => import('utils/languages'), 'utils/languages');
  Languages = res.default;
}

export const defaultLocale = 'vi_VN';

export function getClientLocale() {
  let locale;
  if (cookie) locale = cookie.get('locale');
  else if (global.locale) locale = global.locale;
  return locale || defaultLocale;
}

export function getNextLocale() {
  let locale;
  if (cookie) locale = cookie.get('locale');
  else if (global.locale) locale = global.locale;
  const nextLocale = locale !== 'vi_VN' ? 'vi_VN' : 'en_US';
  return nextLocale;
}

export function getString(field, context, values, forceReactElement = false) {
  const locale = getClientLocale();

  if (!locale || !Languages) return field;
  if (!field) return '';
  const lang = Languages[locale]?.data;
  if (!lang) return field;

  const string = lang[field] || field;

  // need to check if 'field' contains {s} to replace with 'values'
  const regx = /{([0-9]+)}/g;
  let isElement = !!forceReactElement;

  let str = string || '';
  if (!values || values.length === 0) {
    str = str.replace(regx, '');
  } else {
    let tmp;
    do {
      tmp = regx.exec(string);
      if (!tmp || !tmp[1]) break;
      const idx = parseInt(tmp[1]);
      if (React.isValidElement(values[idx])) {
        str = str.replace(tmp[0], ReactDOMServer.renderToString(values[idx]));
        isElement = true;
      } else str = str.replace(tmp[0], values[idx] ?? '');
    } while (tmp);
  }

  if (isElement) {
    const lines = str.split('\n');
    if (lines.length === 1) return <span dangerouslySetInnerHTML={{ __html: str }} />;
    return <span dangerouslySetInnerHTML={{ __html: lines.map((line) => `<p>${line}</p>`).join('') }} />;
  }
  return str;
}

export function hasString(field) {
  const locale = getClientLocale();

  if (!locale || !Languages) return false;
  if (!field) return false;
  const lang = Languages[locale]?.data;
  if (!lang) return false;

  return !!lang[field];
}

export function changeLanguage(locale) {
  if (cookie) {
    cookie.set('locale', locale);
    location.reload();
  }
}

export function getLanguageList() {
  if (!Languages) return [];
  return Object.keys(Languages).map((key) => {
    const lang = Languages[key];
    return {
      name: lang.name,
      locale: key,
    };
  });
}

export function localeToString(locale) {
  return Languages && Languages[locale]?.name;
}
