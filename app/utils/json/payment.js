export const LIST_PAYMENT_BANKCODE = [
  'fullpayment',
  'CC',
  'epayfullpayment',
  'epayCC',
  'zalopayapp',
  'momopay',
  'momopayinapp',
  'sacombankpay',
  'viettelpay',
];

export const LIST_PAYMENT_TYPES = {
  cod: 'cash_on_delivery_cod',
  atm: 'atm_internet_banking',
  cc: 'credit_card_visamasterjcb',
  zalopay_wallet: 'zalopay_wallet',
  momo_wallet: 'momo_wallet',
  sacombank_wallet: 'sacombank_wallet',
  viettel_wallet: 'viettel_wallet',
};

export const LIST_PAYMENT_TEXT = {
  cod: 'cash_on_delivery_cod',
  fullpayment: 'atm_internet_banking',
  CC: 'credit_card_visamasterjcb',
  epayfullpayment: 'atm_internet_banking',
  epayCC: 'credit_card_visamasterjcb',
  zalopayapp: 'zalopay_wallet',
  momopay: 'momo_wallet',
  momopayinapp: 'momo_wallet',
  sacombankpay: 'sacombank_wallet',
  viettelpay: 'viettel_wallet',
};

export const LIST_PAYMENT_STATUS = {
  paid: 'paid',
  unpaid: 'not_paid',
  refunded: 'refunded',
  refunding: 'refunding',
  refundfail: 'failed_to_refund',
};

export const PARTNER_CONFIGS = {
  momo: {
    partnerName: 'momo',
    partnerCode: 'mm',
    paymentMethod: 'momopayinapp',
    paymentType: 'momo_wallet',
    availablePaymentMethods: ['momopayinapp'],
    paymentFee: 0,
    allowSelectOtherPaymentMethod: false,
    allowToggleBranchSwitch: false,
    clientId: 51,
  },
  sacombankpay: {
    partnerName: 'sacombankpay',
    partnerCode: 'sacombank',
    paymentMethod: 'sacombankpay',
    paymentType: 'sacombank_wallet',
    availablePaymentMethods: ['sacombankpay'],
    paymentFee: 0,
    allowSelectOtherPaymentMethod: false,
    allowToggleBranchSwitch: false,
    clientId: 53,
  },
  viettelpay: {
    partnerName: 'viettelpay',
    partnerCode: 'viettel',
    paymentMethod: 'viettelpay',
    paymentType: 'viettel_wallet',
    availablePaymentMethods: ['viettelpay'],
    paymentFee: 0,
    allowSelectOtherPaymentMethod: false,
    allowToggleBranchSwitch: false,
    clientId: 112,
  },
};
