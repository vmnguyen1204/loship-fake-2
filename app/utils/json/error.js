import qs from 'qs';
import { getString } from 'utils/i18n';

const error = {
  PY_001: {
    code: 'PY_001',
    type: 'payment_failed',
  },
  PY_002: {
    code: 'PY_002',
    type: 'invalid_apptransid',
    message: 'payment_invalid_order',
  },
  PY_003: {
    code: 'PY_003',
    type: 'invalid_partnercode',
    message: 'payment_invalid_order',
  },
  PY_004: {
    code: 'PY_004',
    type: 'payment_timeout',
  },
  //
  AU_001: {
    code: 'AU_001',
    type: 'unauthorized',
    message: 'payment_failed',
  },
  AU_002: {
    code: 'AU_002',
    type: 'forbidden',
    message: 'payment_failed',
  },
  //
  ER_001: {
    code: 'ER_001',
    type: 'internal_error',
  },
  //
  DC_119: {
    code: 'DC_119',
    type: 'payment_gateway_busy',
  },
  IC_101: {
    code: 'IC_101',
    type: 'payment_transaction_unsuccessful',
  },
  IC_124: {
    code: 'IC_124',
    type: 'payment_invalid_token',
  },
  PG_ER1: {
    code: 'PG_ER1',
    type: 'payment_transaction_failed',
  },
  PG_ER3: {
    code: 'PG_ER3',
    type: 'payment_timeout',
  },
  PG_ER18: {
    code: 'PG_ER18',
    type: 'payment_card_expired',
  },
  PG_ER21: {
    code: 'PG_ER21',
    type: 'payment_card_inactive',
  },
  PG_ER23: {
    code: 'PG_ER23',
    type: 'payment_card_denied',
  },
  PG_ER28: {
    code: 'PG_ER28',
    type: 'payment_card_inactive_temporary',
  },
  PG_ER17: {
    code: 'PG_ER17',
    type: 'payment_card_not_veriried',
  },
  PG_ER19: {
    code: 'PG_ER19',
    type: 'payment_not_enough_money',
  },
  PG_ER20: {
    code: 'PG_ER20',
    type: 'payment_amount_reach_limit',
  },
  PG_ER2: {
    code: 'PG_ER2',
    type: 'payment_card_invalid',
  },
  PG_ER22: {
    code: 'PG_ER22',
    type: 'payment_card_name_invalid',
  },
  PG_ER7: {
    code: 'PG_ER7',
    type: 'payment_card_number_invalid',
  },
  PG_ER8: {
    code: 'PG_ER8',
    type: 'payment_card_date_invalid',
  },
  PG_ER16: {
    code: 'PG_ER16',
    type: 'payment_otp_invalid',
  },
};

export function getErrorQs(code) {
  const data = error[code];

  if (!data) return '';
  return qs.stringify({ errorType: data.type, errorCode: data.code });
}

export function getErrorStr(code, fallback = 'ER_001') {
  if (!code) return '';

  // fallback only used if !!code && !error[code]
  const data = error[code] || error[fallback];
  return getString(data.message || data.type, '', [code]);
}

export default error;
