export const lunchFood = [
  {
    id: 59,
    slug: 'pizza',
  },
  {
    id: 61,
    slug: 'bit-tet',
  },
  {
    id: 62,
    slug: 'chay',
  },
  {
    id: 63,
    slug: 'ga',
  },
  {
    id: 64,
    slug: 'de',
  },
  {
    id: 68,
    slug: 'burger',
  },
  {
    id: 70,
    slug: 'sushi',
  },
  {
    id: 71,
    slug: 'dimsum',
  },
  {
    id: 72,
    slug: 'hu-tieu-and-mi',
  },
  {
    id: 73,
    slug: 'pho',
  },
  {
    id: 74,
    slug: 'vit',
  },
  {
    id: 75,
    slug: 'chao',
  },
];

export const lunchThemes = [
  {
    id: 1036,
    slug: 'com-trua-van-phong',
  },
];

export const foodCuisines = [
  {
    id: 4,
    slug: 'chau-au',
  },
  {
    id: 9,
    slug: 'y',
  },
  {
    id: 16,
    slug: 'nhat-ban',
  },
  {
    id: 17,
    slug: 'viet-nam',
  },
  {
    id: 26,
    slug: 'han-quoc',
  },
  {
    id: 32,
    slug: 'thai-lan',
  },
  {
    id: 33,
    slug: 'phap',
  },
  {
    id: 39,
    slug: 'trung-quoc',
  },
  {
    id: 40,
    slug: 'singapore',
  },
  {
    id: 41,
    slug: 'an-do',
  },
  {
    id: 42,
    slug: 'nga',
  },
  {
    id: 43,
    slug: 'mong-co',
  },
  {
    id: 44,
    slug: 'mexico',
  },
  {
    id: 45,
    slug: 'my-latin',
  },
  {
    id: 46,
    slug: 'my',
  },
  {
    id: 47,
    slug: 'indonesia',
  },
  {
    id: 48,
    slug: 'malaysia',
  },
  {
    id: 49,
    slug: 'a-rap-trung-dong',
  },
  {
    id: 50,
    slug: 'tay-ban-nha',
  },
  {
    id: 1067,
    slug: 'dai-loan',
  },
];

export const foodThemes = [
  {
    id: 1,
    slug: 'quan-an',
  },
  {
    id: 12,
    slug: 'quan-an-via-he',
  },
  {
    id: 3,
    slug: 'an-vat-via-he',
  },
  {
    id: 35,
    slug: 'nha-hang',
  },
  {
    id: 77,
    slug: 'quan-nhau',
  },
  {
    id: 23,
    slug: 'thuc-an-nhanh',
  },
  {
    id: 10,
    slug: 'quay-thuc-an-trong-trung-tam-thuong-mai',
  },
  {
    id: 2,
    slug: 'cafe-kem',
  },
  {
    id: 11,
    slug: 'tiem-banh',
  },
  {
    id: 6,
    slug: 'banh-mi',
  },
  {
    id: 1036,
    slug: 'com-trua-van-phong',
  },
  {
    id: 36,
    slug: 'beer-club-beer-garden',
  },
  {
    id: 15,
    slug: 'bar-pub-club',
  },
];

export const foodDishes = [
  {
    id: 59,
    slug: 'pizza',
  },
  {
    id: 60,
    slug: 'hai-san',
  },
  {
    id: 61,
    slug: 'bit-tet',
  },
  {
    id: 62,
    slug: 'chay',
  },
  {
    id: 63,
    slug: 'ga',
  },
  {
    id: 64,
    slug: 'de',
  },
  {
    id: 65,
    slug: 'lau',
  },
  {
    id: 66,
    slug: 'nuong',
  },
  {
    id: 67,
    slug: 'bbq',
  },
  {
    id: 68,
    slug: 'burger',
  },
  {
    id: 69,
    slug: 'oc',
  },
  {
    id: 70,
    slug: 'sushi',
  },
  {
    id: 71,
    slug: 'dimsum',
  },
  {
    id: 72,
    slug: 'hu-tieu-and-mi',
  },
  {
    id: 58,
    slug: 'mon-trang-mieng',
  },
  {
    id: 73,
    slug: 'pho',
  },
  {
    id: 74,
    slug: 'vit',
  },
  {
    id: 75,
    slug: 'chao',
  },
];

export const drinkThemes = [
  {
    id: 21,
    slug: 'cafe',
  },
  {
    id: 22,
    slug: 'cafe-take-away',
  },
  {
    id: 76,
    slug: 'cafe-shisha',
  },
  {
    id: 24,
    slug: 'cafe-tra-sua',
  },
  {
    id: 2,
    slug: 'cafe-kem',
  },
  {
    id: 80,
    slug: 'tiem-tra',
  },
  {
    id: 36,
    slug: 'beer-club-beer-garden',
  },
  {
    id: 54,
    slug: 'phong-tra',
  },
  {
    id: 53,
    slug: 'cafe-hat-voi-nhau',
  },
  {
    id: 52,
    slug: 'cafe-acoustic',
  },
  {
    id: 55,
    slug: 'cafe-sach',
  },
  {
    id: 56,
    slug: 'cafe-phim',
  },
];

export const drinkDishes = [
  {
    id: 58,
    slug: 'mon-trang-mieng',
  },
];

export const dessertDishes = [
  {
    id: 58,
    slug: 'mon-trang-mieng',
  },
];
