export default [
  {
    _id: 1,
    slug: 'sun',
    name: 'Sunday',
    value: 'sunday',
  },
  {
    _id: 2,
    slug: 'mon',
    name: 'Monday',
    value: 'monday',
  },
  {
    _id: 3,
    slug: 'tue',
    name: 'Tuesday',
    value: 'tuesday',
  },
  {
    _id: 4,
    slug: 'wed',
    name: 'Wednesday',
    value: 'wednesday',
  },
  {
    _id: 5,
    slug: 'thu',
    name: 'Thursday',
    value: 'thursday',
  },
  {
    _id: 6,
    slug: 'fri',
    name: 'Friday',
    value: 'friday',
  },
  {
    _id: 7,
    slug: 'sat',
    name: 'Saturday',
    value: 'saturday',
  },
];
