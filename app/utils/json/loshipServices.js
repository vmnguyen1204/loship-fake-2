export default [
  {
    id: 1222,
    name: 'loship',
    title: 'Loship',
    superCategoryId: 1,
  },
  {
    id: 1223,
    name: 'lomart',
    title: 'Lomart',
    superCategoryId: 2,
  },
  {
    id: 1224,
    name: 'lozat',
    title: 'Lozat',
    superCategoryId: 3,
  },
  {
    id: 1226,
    name: 'lomed',
    title: 'Lomed',
    superCategoryId: 4,
  },
  {
    id: 1324,
    name: 'lopet',
    title: 'Lo-pet',
    superCategoryId: 8,
  },
  {
    id: 1325,
    name: 'lohoa',
    title: 'Lo-hoa',
    superCategoryId: 7,
  },
  {
    id: 1664,
    name: 'lobeauty',
    title: 'LoBeauty',
    superCategoryId: 15,
  },
  {
    id: 1302,
    name: 'losupply',
    title: 'Losupply',
    superCategoryId: 11,
  },
  {
    id: 1542,
    name: 'lozi',
    title: 'Lozi',
    superCategoryId: 5,
  },

];

