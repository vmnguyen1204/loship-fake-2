export default [
  { id: 1, slug: 'ha-noi', name: 'Hà Nội', lat: 21.0282739, lng: 105.853886 },
  { id: 2, slug: 'ha-giang', name: 'Hà Giang', lat: 22.8282081, lng: 104.980896 },
  { id: 3, slug: 'cao-bang', name: 'Cao Bằng', lat: 22.668634, lng: 106.257845 },
  { id: 4, slug: 'bac-kan', name: 'Bắc Kạn', lat: 22.145032, lng: 105.828165 },
  { id: 5, slug: 'tuyen-quang', name: 'Tuyên Quang', lat: 21.821563, lng: 105.217233 },
  { id: 6, slug: 'lao-cai', name: 'Lào Cai', lat: 22.476104, lng: 103.973463 },
  { id: 7, slug: 'dien-bien', name: 'Điện Biên', lat: 21.386117, lng: 103.016355 },
  { id: 8, slug: 'lai-chau', name: 'Lai Châu', lat: 22.399207, lng: 103.44533 },
  { id: 9, slug: 'son-la', name: 'Sơn La', lat: 21.33162, lng: 103.90289 },
  { id: 10, slug: 'yen-bai', name: 'Yên Bái', lat: 21.725782, lng: 104.913649 },
  { id: 11, slug: 'hoa-binh', name: 'Hòa Bình', lat: 20.8115669, lng: 105.335322 },
  { id: 12, slug: 'thai-nguyen', name: 'Thái Nguyên', lat: 21.593689, lng: 105.844519 },
  { id: 13, slug: 'lang-son', name: 'Lạng Sơn', lat: 21.846392, lng: 106.753309 },
  { id: 14, slug: 'quang-ninh', name: 'Quảng Ninh', lat: 20.9416489, lng: 107.123658 },
  { id: 15, slug: 'bac-giang', name: 'Bắc Giang', lat: 21.275831, lng: 106.200285 },
  { id: 16, slug: 'phu-tho', name: 'Phú Thọ', lat: 21.322495, lng: 105.400748 },
  { id: 17, slug: 'vinh-phuc', name: 'Vĩnh Phúc', lat: 21.309961, lng: 105.607572 },
  { id: 18, slug: 'bac-ninh', name: 'Bắc Ninh', lat: 21.1855819, lng: 106.07592 },
  { id: 19, slug: 'hai-duong', name: 'Hải Dương', lat: 20.9401009, lng: 106.33309 },
  { id: 20, slug: 'hai-phong', name: 'Hải Phòng', lat: 20.8647119, lng: 106.683842 },
  { id: 21, slug: 'hung-yen', name: 'Hưng Yên', lat: 20.6546279, lng: 106.057428 },
  { id: 22, slug: 'thai-binh', name: 'Thái Bình', lat: 20.4539449, lng: 106.347061 },
  { id: 23, slug: 'ha-nam', name: 'Hà Nam', lat: 20.5461559, lng: 105.912372 },
  { id: 24, slug: 'nam-dinh', name: 'Nam Định', lat: 20.4349609, lng: 106.177694 },
  { id: 25, slug: 'ninh-binh', name: 'Ninh Bình', lat: 20.2579789, lng: 105.97595 },
  { id: 26, slug: 'thanh-hoa', name: 'Thanh Hóa', lat: 19.8075949, lng: 105.775927 },
  { id: 27, slug: 'nghe-an', name: 'Nghệ An', lat: 18.6733908, lng: 105.69316 },
  { id: 28, slug: 'ha-tinh', name: 'Hà Tĩnh', lat: 18.3415468, lng: 105.904641 },
  { id: 29, slug: 'quang-binh', name: 'Quảng Bình', lat: 17.4668277, lng: 106.623176 },
  { id: 30, slug: 'quang-tri', name: 'Quảng Trị', lat: 16.8176057, lng: 107.099581 },
  { id: 31, slug: 'thua-thien-hue', name: 'Thừa Thiên Huế', lat: 16.4621577, lng: 107.58481 },
  { id: 32, slug: 'da-nang', name: 'Đà Nẵng', lat: 16.0756686, lng: 108.224465 },
  { id: 33, slug: 'quang-nam', name: 'Quảng Nam', lat: 15.5728316, lng: 108.470991 },
  { id: 34, slug: 'quang-ngai', name: 'Quảng Ngãi', lat: 15.1178446, lng: 108.797156 },
  { id: 35, slug: 'binh-dinh', name: 'Bình Định', lat: 13.7690305, lng: 109.228381 },
  { id: 36, slug: 'phu-yen', name: 'Phú Yên', lat: 13.0955445, lng: 109.322169 },
  { id: 37, slug: 'khanh-hoa', name: 'Khánh Hòa', lat: 12.2437125, lng: 109.192682 },
  { id: 38, slug: 'ninh-thuan', name: 'Ninh Thuận', lat: 11.5656264, lng: 108.990826 },
  { id: 39, slug: 'binh-thuan', name: 'Bình Thuận', lat: 10.9237964, lng: 108.100049 },
  { id: 40, slug: 'kon-tum', name: 'Kon Tum', lat: 14.3461916, lng: 108.003489 },
  { id: 41, slug: 'gia-lai', name: 'Gia Lai', lat: 13.9808145, lng: 108.001424 },
  { id: 42, slug: 'dak-lak', name: 'Đắk Lắk', lat: 12.6743795, lng: 108.04313 },
  { id: 43, slug: 'dak-nong', name: 'Đắk Nông', lat: 12.0034275, lng: 107.685046 },
  { id: 44, slug: 'lam-dong', name: 'Lâm Đồng', lat: 11.9364585, lng: 108.443642 },
  { id: 45, slug: 'binh-phuoc', name: 'Bình Phước', lat: 11.5396864, lng: 106.900785 },
  { id: 46, slug: 'tay-ninh', name: 'Tây Ninh', lat: 11.3145644, lng: 106.09426 },
  { id: 47, slug: 'binh-duong', name: 'Bình Dương', lat: 10.9816684, lng: 106.650501 },
  { id: 48, slug: 'dong-nai', name: 'Đồng Nai', lat: 10.9452734, lng: 106.816537 },
  { id: 49, slug: 'ba-ria-vung-tau', name: 'Bà Rịa - Vũng Tàu', lat: 10.3496754, lng: 107.07267 },
  { id: 50, slug: 'ho-chi-minh', name: 'Hồ Chí Minh', lat: 10.7765194, lng: 106.700987 },
  { id: 51, slug: 'long-an', name: 'Long An', lat: 10.5366014, lng: 106.413011 },
  { id: 52, slug: 'tien-giang', name: 'Tiền Giang', lat: 10.3528914, lng: 106.363224 },
  { id: 53, slug: 'ben-tre', name: 'Bến Tre', lat: 10.2360434, lng: 106.373899 },
  { id: 54, slug: 'tra-vinh', name: 'Trà Vinh', lat: 9.93626843, lng: 106.341127 },
  { id: 55, slug: 'vinh-long', name: 'Vĩnh Long', lat: 10.2542454, lng: 105.972312 },
  { id: 56, slug: 'dong-thap', name: 'Đồng Tháp', lat: 10.4592074, lng: 105.631685 },
  { id: 57, slug: 'an-giang', name: 'An Giang', lat: 10.3898824, lng: 105.434568 },
  { id: 58, slug: 'kien-giang', name: 'Kiên Giang', lat: 10.0093214, lng: 105.082311 },
  { id: 59, slug: 'can-tho', name: 'Cần Thơ', lat: 10.0361904, lng: 105.788036 },
  { id: 60, slug: 'hau-giang', name: 'Hậu Giang', lat: 9.78407042, lng: 105.467978 },
  { id: 61, slug: 'soc-trang', name: 'Sóc Trăng', lat: 9.59995442, lng: 105.970841 },
  { id: 62, slug: 'bac-lieu', name: 'Bạc Liêu', lat: 9.29024341, lng: 105.725128 },
  { id: 63, slug: 'ca-mau', name: 'Cà Mau', lat: 9.1772094, lng: 105.151069 },
];
