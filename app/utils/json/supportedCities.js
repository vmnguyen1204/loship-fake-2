import { LINK_API } from 'settings/variables';

export const mainCitiesSlug = ['ho-chi-minh', 'ha-noi'];

export const defaultCity = {
  id: 50,
  slug: 'ho-chi-minh',
  type: 'city',
  name: 'Hồ Chí Minh',
};

export const defaultShippingAdministration = LINK_API.includes('mocha')
  ? {
    id: 0,
    type: 'city',
    city: { id: 50, name: 'Hồ Chí Minh', slug: 'ho-chi-minh' },
  } : {
    id: 0,
    type: 'district',
    district: {
      id: 771, name: 'Quận 10', slug: 'quan-10',
      city: { id: 50, name: 'Hồ Chí Minh', slug: 'ho-chi-minh' },
    },
  };

export const DEFAULT_CITY_ID = 50;
