import { slugify } from 'utils/format';
import cities from 'utils/json/cities';
import districts from 'utils/json/districts';

export function getDistrictId(district) {
  const rawDistrictSlug = slugify(district);
  if (!rawDistrictSlug) return 0;
  const targetDistrict = districts.find((d) => {
    const districtSlug = slugify(d.name);
    return rawDistrictSlug.includes(districtSlug) || districtSlug.includes(rawDistrictSlug);
  });
  return targetDistrict?.id ?? 0;
}

export function getCityId(city) {
  const rawCitySlug = slugify(city);
  if (!rawCitySlug) return 0;
  const targetCity = cities.find((d) => {
    const citySlug = slugify(d.name);
    return rawCitySlug.includes(citySlug) || citySlug.includes(rawCitySlug);
  });
  return targetCity?.id ?? 0;
}
