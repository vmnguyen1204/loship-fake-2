import { LINK_DOMAIN } from 'settings/variables';
import qs from 'qs';

export const LANDING = 0;
export const USER = 1;
export const MERCHANT = 2;
export const MERCHANT_COMMENT = 14;
export const ALBUM = 3;
export const BLOCK = 4;
export const EVENT = 5;
export const HASHTAG = 6;
export const NEWSFEED_PHOTO = 7;
export const NEWSFEED_MERCHANT = 8;
export const STORY = 9;
export const SEARCH_PHOTO = 10;
export const SEARCH_MERCHANT = 11;
export const SEARCH_ALBUM = 12;
export const SEARCH_USER = 13;

export function check(url) {
  const urlArr = url.split('lozi.vn/');
  if (urlArr.length <= 1) {
    return LANDING;
  }

  const valueArr = urlArr[1].split('/');

  if (valueArr.length === 1) {
    // this is USER
    return USER;
  }

  if (valueArr.length === 2) {
    if (valueArr[0] === 'a') return LANDING;
    if (valueArr[0] === 'b') return MERCHANT;
    if (valueArr[0] === 'photos') return BLOCK;
    if (valueArr[0] === 'e') return EVENT;
    if (valueArr[0] === 'hashtag') return HASHTAG;
    if (valueArr[0] === 'search') {
      switch (valueArr[1]) {
        case 'b':
          return SEARCH_MERCHANT;
        case 'albums':
          return SEARCH_ALBUM;
        case 'people':
          return SEARCH_USER;
        case 'photos':
        default:
          return SEARCH_PHOTO;
      }
    }
    if (valueArr[0] === 'stories') return STORY;
    // if (valueArr[0] === 'career')   return ;
    // if (valueArr[0] === 'explore')   return ;
    // if (valueArr[0] === 'about')   return ;
    return ALBUM;
  }
  if (valueArr.length >= 3) {
    // this is NEWSFEED
    if (valueArr[0] === 'a') {
      if (valueArr[2] === 'photos' || valueArr[3] === 'photos') {
        return NEWSFEED_PHOTO;
      } if (valueArr[2] === 'b' || valueArr[3] === 'b') {
        return NEWSFEED_MERCHANT;
      }
    }
  }

  if (valueArr.length >= 4) {
    // this is MERCHANT COMMENT
    if (valueArr[0] === 'b') return MERCHANT_COMMENT;
  }
  return false;
}

export function handle(url) {
  const result = {
    type: 'lozi',
    slug: 'lozi',
  };
  const urlArr = url.split('/');
  const slugArr = urlArr[urlArr.length - 1].split('?');

  result.slug = slugArr[0];

  switch (check(url)) {
    case LANDING:
      result.type = 'lp';
      break;
    case USER:
      result.type = 'pf';
      break;
    case MERCHANT:
      result.type = 'me';
      break;
    case SEARCH_PHOTO:
      result.type = 'sp';
      break;
    case SEARCH_MERCHANT:
      result.type = 'sme';
      break;
    case SEARCH_ALBUM:
      result.type = 'sal';
      break;
    case SEARCH_USER:
      result.type = 'sus';
      break;
    case ALBUM:
      switch (urlArr[4]) {
        case 'albums':
          result.type = 'pfa';
          break;
        case 'following-albums':
          result.type = 'pffa';
          break;
        case 'following':
          result.type = 'pffu';
          break;
        case 'followers':
          result.type = 'pff';
          break;
        default:
          result.type = 'al';
      }
      break;
    case BLOCK:
      result.type = 'bl';
      break;
    case EVENT:
      result.type = 'ev'; // api not supported "event" yet
      break;
    case HASHTAG:
      result.type = 'ht';
      break;
    case NEWSFEED_PHOTO:
      result.type = 'nf';
      break;
    case NEWSFEED_MERCHANT:
      result.type = 'menf';
      break;
    case STORY:
      result.type = 'sd';
      break;
    default:
      break;
  }
  // For mobile
  result.type = 'm_' + result.type;
  return result;
}

export function getTrackingQuery(lzPos, noQuestionMark) {
  let query = noQuestionMark ? '' : '?';
  // Add current page info into the url
  if (typeof window !== 'undefined') {
    const origin = handle(LINK_DOMAIN + location.pathname);
    if (origin.type) {
      query += `lzr=${origin.type}`;
      if (lzPos) query += `&lzp=${lzPos}`;
    }
  }
  return query === '?' ? '' : query;
}

export const PosType = {
  ALBUM_HEADER: 'm_al_he',
  ALBUM_BLOCK: 'm_al_bl',
  ALBUM_MERCHANT: 'm_al_me',
  ALBUM_RELATED: 'm_al_re',
  ALBUM_LANDINGPAGE: 'm_al_lp',
  //  ALBUM_FOLLOWER: "m_al_fl",
  ALBUM_TRENDING: 'al_tr',
  ALBUM_LATEST: 'al_lt',
  ALBUM_TRENDING_BOTTOM: 'al_tr_bt',
  ALBUM_EVENT: 'al_ev',
  ALBUM_NEWSFEED_BLOCK: 'al_nf_bl',

  PROFILE_HEADER: 'm_pf_he',
  PROFILE_ALBUM: 'm_pf_al',
  PROFILE_SHOW_ALL_ALBUM: 'm_pfa_li',
  PROFILE_BLOCK: 'm_pf_bl',

  NEWSFEED_BLOCK: 'm_nf_bl',
  NEWSFEED_MERCHANT_BLOCK: 'm_nf_me',
  NEWSFEED_FILTER_CATEGORY: 'm_nf_nf_fil',
  NEWSFEED_FILTER_LOCATION: 'm_nf_lo',
  NEWSFEED_FILTER_SORT: 'm_nf_so',
  NEWSFEED_SIDEBAR: 'nf_si',
  NEWSFEED_DISTRICT: 'nf_di',
  NEWSFEED_BREADCRUMBS: 'nf_br',
  MERCHANT_COMMENT: 'm_me_co',
  MERCHANT_META: 'm_me_mt',
  MERCHANT_BLOCK: 'm_me_bl',
  MERCHANT_PHOTO: 'm_me_ph',
  MERCHANT_MENU: 'me_mn',
  MERCHANT_RELATED: 'm_me_re',
  MERCHANT_NEARBY: 'me_ne',
  MERCHANT_ALBUM: 'me_al',
  MERCHANT_SIDEBAR: 'me_si',
  MERCHANT_SEEN: 'me_se',
  MERCHANT_HOTFOOD: 'm_me_hf',
  MERCHANT_REVIEWER: 'm_me_rv',
  MERCHANT_SUGGEST: 'me_su',

  LANDING_BANNER: 'm_lp_ba',
  LANDING_TOPIC: 'm_lp_to',
  LANDING_BLOCK: 'lp_bl',
  LANDING_HEADER: 'lp_he',

  BLOCK_HEADER: 'bl_he',
  BLOCK_META: 'm_bl_mt',
  BLOCK_RELATE: 'm_bl_re',
  BLOCK_COMMENT: 'bl_co',
  BLOCK_USER: 'bl_us',
  BLOCK_CATEGORY: 'bl_ca',
  BLOCK_SEEN: 'bl_se',
  BLOCK_NAVIGATOR: 'bl_na',
  BLOCK_CHAT: 'm_block_chat',

  PROFILE_ALBUM_LIST: 'pfa_li',
  PROFILE_FOLLOWER_LIST: 'pff_li',
  PROFILE_FOLLOWING_USER_LIST: 'pffu_li',
  PROFILE_FOLLOWING_ALBUM_LIST: 'pffa_li',
  EVENT_BLOCK: 'm_ev_bl',
  EVENT_RELATED: 'ev_re',
  HASHTAG_BLOCK: 'm_ht_bl',
  NAVBAR: 'm_nav',
  MAIN_MENU: 'ma_mn',
  FOOTER: 'fo',
  NOTIFICATION_ITEM: 'nt_it',
  // MERCHANT_PHOTO_META: "mep_mt",
  // MERCHANT_PHOTO_PHOTO: "mep_ph",
  // MERCHANT_PHOTO_MENU: "mep_mn",
  // MERCHANT_PHOTO_RELATED: "mep_re",
  // MERCHANT_PHOTO_NEARBY: "mep_ne",
  // MERCHANT_PHOTO_ALBUM: "mep_al",
  // PROFILE_ALBUM_HEADER: "pfa_he",
  // PROFILE_FOLLOWER_HEADER: "pff_he",
  // PROFILE_FOLLOWING_USER_HEADER: "pffu_he",
  // PROFILE_FOLLOWING_ALBUM_HEADER: "pffa_he",
  // MERCHANT_NEWSFEED_SIDEBAR: "menf_si",
  // MERCHANT_NEWSFEED_DISTRICT: "menf_di",

  SEARCH_PHOTO_BLOCK: 'm_sp_bl',

  SEARCH_PHOTO_HEADER: 'sp_he',
  SEARCH_PHOTO_SIDEBAR: 'sp_si',
  SEARCH_PHOTO_FILTER: 'sp_fi',
  SEARCH_PHOTO_PAGER: 'sp_pa',
  // SEARCH_PHOTO_SEEN: "sp_se",
  SEARCH_MERCHANT_MERCHANT: 'm_sme_me',
  SEARCH_MERCHANT_FILTER: 'sme_fi',
  SEARCH_MERCHANT_DISTRICT: 'sme_di',
  SEARCH_MERCHANT_PAGER: 'sme_pa',
  SEARCH_USER_PROFILE: 'sus_pf',
  SEARCH_USER_PAGER: 'sus_pa',
  SEARCH_ALBUM_ALBUM: 'sal_al',
  SEARCH_ALBUM_PAGER: 'sal_pa',
  SEARCH_PHOTOS_SUGGEST: 'm_pho_sug',
  SEARCH_ALBUM_SUGGEST: 'al_sug',
  SEARCH_MERCHANT_SUGGEST: 'm_me_sug',
  SEARCH_MERCHANTCATEGORY_SUGGEST: 'mecat_sug',
  SEARCH_USER_SUGGEST: 'us_sug',
  SEARCH_ACTIVE: 'm_sea_act',
  SEARCH_CANCLE: 'm_sea_ca',

  STORIES_LIST: 'st_li',
  STORY_RELATED: 'sd_re',
  STORY_POPULAR: 'sd_po',

  MY_PROFILE: 'm_my_pf',
  MESSAGES: 'mess',

  POPUP_FILTER_TOPIC: 'm_pu_tp',
  POPUP_FILTER_LOCATION: 'm_pu_lo',
  SEARCH_FILTER_LOCATION: 'm_sme_lo_fil',
  SEARCH_FILTER_SORT: 'm_s_so_fil',
  SEARCH_FILTER_CATEGORY: 'm_s_cat_fil',
};

export function getRefererType(url) {
  const domain = url.match(/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:/\n]+)/im);
  if (!domain || !domain[1]) return 'na';
  if (domain[1].indexOf('facebookhitlist') > -1) return 'fb';
  if (domain[1].indexOf('facebook') > -1) return 'fh';
  if (domain[1].indexOf('google') > -1) return 'gg';
  if (domain[1].indexOf('coccoc') > -1) return 'cc';
  if (domain[1].indexOf('instagram') > -1) return 'ins';
  if (domain[1].indexOf('tumblr') > -1) return 'tb';
  if (domain[1].indexOf('purevolume') > -1) return 'pv';
  if (domain[1].indexOf('over-blog') > -1) return 'ob';
  if (domain[1].indexOf('ning') > -1) return 'ng';
  if (domain[1].indexOf('livejournal') > -1) return 'lj';
  return domain[1];
}

export function getFBShareUrl(url, encode = true) {
  let x = url.split('?');
  if (x.length > 1) {
    x = qs.parse(url[1]);
  } else {
    x = {};
  }
  const utm = {
    utm_source: 'facebook',
    utm_medium: 'social',
    utm_campaign: 'share_btn',
  };
  const shareUrl = url + '?' + qs.stringify({ ...x, ...utm });
  return encode ? encodeURIComponent(shareUrl) : shareUrl;
}

/*
import { PosType } from 'utils/link-type';
 lzPos={this.props.lzPos}
 lzPos={PosType.}
*/
