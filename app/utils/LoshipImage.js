import { LINK_AUTH } from 'settings/variables';
import FileApi from 'utils/shims/fileapi';

const MAX_IMAGE_SIZE = 1600; // 2560*2560 px
const ORI_EXIF = {
  1: 0,
  3: 180,
  6: 90,
  8: -90,
};

class LoshipImage {
  __URL = null;

  __hasImage = false;

  __hasDish = false;

  __rawFile = null;

  __rawFileInfo = null;

  __imageFile = null;

  __dishId = null;

  __dishImageUrl = null;

  __uploadImage = () => {
    return new Promise((resolve, reject) => {
      const request = {
        url: `${LINK_AUTH}/upload/images`,
        files: { files: this.__imageFile },
        complete: (err, xhr) => {
          if (err) return reject(err);
          const res = JSON.parse(xhr.response);
          if (!res) return reject();
          resolve(res.data[0].url);
        },
      };
      FileApi.upload(request);
    });
  };

  setDish(id) {
    this.__dishId = id;
    this.__hasDish = true;
  }

  setDishImageUrl(url) {
    this.__dishImageUrl = url;
  }

  async setImageFile(file) {
    const image = FileApi.image(file);
    this.__hasImage = true;
    this.__rawFile = image;

    try {
      await this.getInfo(file);
      await this.setSize(MAX_IMAGE_SIZE, MAX_IMAGE_SIZE);
      await this.fixOrientation();
    } catch (e) {
      !!e && console.error(e);
    }
  }

  async getInfo(file) {
    return new Promise((resolve, reject) => {
      FileApi.getInfo(file, (err, info) => {
        if (err) {
          console.error(err);
          return reject(err);
        }
        this.__rawFileInfo = info;
        resolve();
      });
    });
  }

  async fixOrientation() {
    return new Promise((resolve, reject) => {
      if (!this.__rawFileInfo) return reject();

      const { exif } = this.__rawFileInfo;
      if (!exif || !exif.Orientation) return reject();

      const degree = ORI_EXIF[exif.Orientation];

      this.__imageFile.rotate(degree).get((rotateErr, img) => {
        if (rotateErr) return reject(rotateErr);
        this.__imageFile = FileApi.image(img);
        this.__URL = null;
        resolve();
      });
    });
  }

  async setSize(width, height, option = 'max') {
    return new Promise((resolve, reject) => {
      this.__rawFile.resize(width, height, option).get((err, img) => {
        if (err) return reject(err);
        this.__imageFile = FileApi.image(img);
        this.__URL = null;
        resolve();
      });
    });
  }

  async getThumb(width, height) {
    const thumb = await this.__imageFile.file.toDataURL('image/jpeg');

    if (!width || !height) return thumb;
    return resizeThumbnail(thumb, width, height);
  }

  async getURL() {
    if (this.__URL === null && this.__hasImage) {
      this.__URL = await this.__uploadImage();
    }
    return this.__URL;
  }

  getDish() {
    return this.__dishId;
  }

  getDishImageUrl() {
    return this.__dishImageUrl;
  }

  hasImage() {
    return this.__hasImage;
  }

  hasDish() {
    return this.__hasDish;
  }
}

const getFileBlob = async function (url) {
  const res = await fetch(url, { credentials: 'same-origin' });
  return res.blob();
};

export const getImageFromFile = async (input) => {
  const files = FileApi.getFiles(input);
  if (files.length === 0) {
    return null;
  }
  const image = new LoshipImage();
  await image.setImageFile(files[0]);
  return image;
};

export const getImageFromUrl = async (url) => {
  const image = new LoshipImage();
  await image.setImageFile(await getFileBlob(url));
  return image;
};

export const getImageFromDish = async (id, url) => {
  const image = new LoshipImage();
  image.setDish(id);
  image.setDishImageUrl(url);
  return image;
};

export async function resizeThumbnail(thumbnail, resizedWidth, resizedHeight) {
  const image = await new Promise((resolve) => {
    const img = new Image();
    img.src = thumbnail;

    img.onload = function () {
      resolve(img);
    };
  });

  let { width } = image;
  let { height } = image;

  if (width > height) {
    if (width > resizedWidth) {
      height *= resizedWidth / width;
      width = resizedWidth;
    }
  } else if (height > resizedHeight) {
    width *= resizedHeight / height;
    height = resizedHeight;
  }

  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  const ctx = canvas.getContext('2d');
  ctx.drawImage(image, 0, 0, width, height);

  return canvas.toDataURL();
}

export default LoshipImage;
