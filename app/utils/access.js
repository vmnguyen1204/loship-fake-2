import qs from 'qs';

export function hasRole(user, roles = []) {
  if (!user) return false;
  const userRoles = typeof user.get === 'function' ? user.get('roles') : user.roles;
  let valid = false;
  if (userRoles && typeof userRoles.map === 'function') {
    userRoles.forEach((role) => {
      if (roles.indexOf(role) > -1) valid = true;
    });
  }
  return valid;
}

export function preparePopupQs(location, query, key) {
  delete query[key];

  const q = qs.stringify(query) ? `?${qs.stringify(query)}` : '';
  const qPopup = `?${qs.stringify({ ...query, [key]: true })}`;

  return [q, qPopup].map((x) => location.pathname + x);
}
