import shortId from 'shortid';
import districts from 'utils/json/districts';
import { defaultShippingAdministration } from 'utils/json/supportedCities';

const CHARMAP = '23456789ABCEGJKLMNPQRVWXYZ';
const SHORTID_SEED = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@';

export const isCitySlug = (slug) => {
  return global.availableAreas?.some((area) => {
    if (area.type === 'city') return slug === area.city.slug;
    return slug === area.district.slug || slug === area.district.city.slug;
  });
};

export const getCityByIdOrNameOrSlug = (isOrNameOrSlug = '') => {
  if (!global) return null;
  return (
    global.availableAreas?.find((area) => {
      const city = area.type === 'city' ? area.city : area.district;
      return parseInt(isOrNameOrSlug) === city.id || isOrNameOrSlug === city.name || isOrNameOrSlug === city.slug;
    }) || defaultShippingAdministration
  );
};

export const getDistrictInCity = (city = 0) => {
  return districts.filter((d) => d.cityId === city);
};

export const getDistrictById = (districtId = 0) => {
  return districts.find((d) => d.id === parseInt(districtId));
};

export const getDistrictBySlug = (slug = '') => {
  return districts.find((d) => d.slug.toLocaleLowerCase().includes(slug));
};

export function genCode(codeLength = 8) {
  function generate() {
    return [...Array(codeLength).keys()]
      .map(() => {
        const index = Math.floor(Math.random() * CHARMAP.length);
        return CHARMAP[index];
      })
      .join('');
  }
  return generate();
}

export function genShortId() {
  function generate() {
    shortId.characters(SHORTID_SEED);
    return shortId.generate();
  }
  return generate();
}

export const REGEX_STRING = (str) => {
  const reStr = str
    .replace(/[^a-zA-Z0-9]/g, '')
    .toLowerCase()
    .split('')
    .map((c) => `[^${c}]*${c}`)
    .join('');

  return RegExp(reStr);
};

export async function sha256(str) {
  const buf = await crypto.subtle.digest('SHA-256', new TextEncoder('utf-8').encode(str));
  return Array.prototype.map.call(new Uint8Array(buf), (x) => (('00' + x.toString(16)).slice(-2))).join('');
}
