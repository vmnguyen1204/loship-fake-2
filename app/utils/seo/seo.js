import { APP_INFO } from 'settings/variables';
import { iife } from 'utils/helper';
import { getImageUrl } from 'utils/image';
import { getDesktopUrl } from 'utils/link';

const MERCHANT_AVATAR_SIZE = 640;
const BANNER_SIZE = 640;

export function metadataSearch() {
  return {};
}

export function metadataMerchant(options) {
  const { merchant: mc, location } = options;
  if (!mc) return {};

  const id = mc.get('id');
  const username = mc.get('username');
  const slug = mc.get('slug');
  const merchant = mc.get('name');
  const freeship = iife(() => {
    const freeShippingMilestone = mc.get('freeShippingMilestone');
    if (!freeShippingMilestone || freeShippingMilestone <= 0) return 0;
    return Math.max(Math.floor(freeShippingMilestone / 100) / 10, 0.1);
  });

  const title = `${merchant} - Freeship`;
  const description = `Đặt món nào cũng freeship ${freeship > 0 ? `${freeship}km` : ''} toàn bộ tại ${merchant}`;
  const keywords = 'địa điểm ăn uống, nhà hàng, quán ăn, ẩm thực';
  const image = getImageUrl(mc.get('avatar'), MERCHANT_AVATAR_SIZE, 's');
  const imageWidth = 600;
  const imageHeight = 314;
  const link = [
    {
      rel: 'canonical',
      href: getDesktopUrl(username ? `/${username}` : `/b/${slug}`),
    },
  ];
  const showDishId = location.query && location.query.showDishId === 'true';

  return {
    title,
    description,
    keywords,
    image,
    imageWidth,
    imageHeight,
    link,
    iosUrl: `loshipuser://loship.vn/eatery?eateryId=${id}&shipServiceId=${APP_INFO.shipServiceId}`,
    androidUrl: `loshipuser://loship.vn/eatery?eateryId=${id}&shipServiceId=${APP_INFO.shipServiceId}`,
    nofollow: showDishId,
  };
}

export function metadataGroupOrder(options) {
  const { merchant: mc, groupTopic, location } = options;
  if (!mc) return {};

  const merchantMeta = metadataMerchant(options);
  const eateryId = mc.get('id');
  const eateryUsername = mc.get('username');
  const eaterySlug = mc.get('slug');
  const showDishId = location.query && location.query.showDishId === 'true';

  return {
    ...merchantMeta,
    iosUrl: `loshipuser://loship.vn/groupOrder?eateryId=${eateryId}&topic=${groupTopic}&shipServiceId=${APP_INFO.shipServiceId}`,
    androidUrl: `loshipuser://loship.vn/groupOrder?eateryId=${eateryId}&topic=${groupTopic}&shipServiceId=${APP_INFO.shipServiceId}`,
    nofollow: showDishId,
    link: [
      {
        rel: 'canonical',
        href: getDesktopUrl(eateryUsername ? `/${eateryUsername}/${groupTopic}` : `/b/${eaterySlug}/${groupTopic}`),
      },
    ],
  };
}

export function metadataMerchantManager(options) {
  const { merchant: mc } = options;
  if (!mc) return {};

  const merchant = mc.get('name');
  const title = `Quản lý ${merchant}`;
  const description = `Trang quản lý menu của ${merchant} trên Loship.vn`;
  const image = getImageUrl(mc.get('avatar'), MERCHANT_AVATAR_SIZE, 's');
  const imageWidth = 600;
  const imageHeight = 314;

  return {
    title,
    description,
    image,
    imageWidth,
    imageHeight,
    nofollow: true,
  };
}

export function metadataOrderHistory() {
  const title = 'Lịch sử đặt hàng';
  const description = 'Lịch sử đặt hàng giao tận nơi của bạn trên Loship.vn';
  const keywords = 'địa điểm ăn uống, nhà hàng, quán ăn, ẩm thực';

  return {
    title,
    description,
    keywords,
    nofollow: true,
    iosUrl: 'loshipuser://loship.vn/profile',
    androidUrl: 'loshipuser://loship.vn/profile',
  };
}

export function metadataOrderDetail(options) {
  const { order, location } = options;

  if (!order) {
    return {
      title: 'Chi tiết đơn hàng',
      description: 'Chi tiết đơn hàng',
      nofollow: true,
    };
  }

  const code = order.get('code');
  const shareId = order.get('sharing') && order.get('sharing').shareId;
  const serviceName = order.get('serviceName');
  const eatery = order.get('eatery');
  const routes = order.get('routes');
  const {
    source: { name: sourceName = 'Người dùng ẩn danh' },
    destination: { name: recipientName = 'Người dùng ẩn danh' },
  } = routes || {};

  const deeplink = shareId && location.search.includes('usp=sharing')
    ? `loshipuser://loship.vn/order?shareId=${shareId}&usp=sharing`
    : `loshipuser://loship.vn/order?orderCode=${code}`;

  return {
    ...iife(() => {
      if (serviceName === 'loxe') {
        return {
          title: `Đơn chở khách của ${recipientName}`,
          description: `Xem thông tin đơn chở khách của ${recipientName}`,
        };
      }
      if (serviceName === 'losend') {
        return {
          title: `Đơn gửi đồ của ${sourceName}`,
          description: `Xem thông tin đơn gửi đồ của ${sourceName}`,
        };
      }
      return {
        title: `Đơn hàng ${eatery ? eatery.name : ''} của ${recipientName}`,
        description: `Xem thông tin đơn hàng của ${recipientName}`,
      };
    }),
    nofollow: true,
    iosUrl: deeplink,
    androidUrl: deeplink,
  };
}

export function metadataPartnerTrial() {
  const title = 'Chương trình dùng thử dành cho đối tác';
  const description = 'Trở thành đối tác Loship';

  return {
    title,
    description,
    nofollow: true,
  };
}

export function metadataNotFound() {
  const title = 'Không tìm thấy cửa hàng';
  return {
    title,
    nofollow: true,
  };
}

export function metadataItem(options) {
  const { item } = options;

  if (!item) {
    return {
      title: 'Chi tiết món',
      description: 'Chi tiết món',
      nofollow: true,
    };
  }

  const deeplink = `loshipuser://loship.vn/item?itemId=${item.get('id')}`;
  const eatery = item.get('eatery') || {};

  return {
    title: `${item.get('name')}${eatery.name ? ` - ${eatery.name}` : ''}`,
    description: item.get('description'),
    iosUrl: deeplink,
    androidUrl: deeplink,
  };
}

export function metadataEvent(options) {
  const { currentEvent } = options;

  if (!currentEvent) return;

  const deeplink = `loshipuser://loship.vn/event?eventId=${currentEvent.get('id')}`;
  return {
    title: currentEvent.get('name'),
    description: currentEvent.get('description'),
    image: getImageUrl(currentEvent.get('image'), BANNER_SIZE, 'o'),
    iosUrl: deeplink,
    androidUrl: deeplink,
  };
}

export function metadataEateryChain(options) {
  const { data } = options;
  if (!data) return;

  const freeship = iife(() => {
    const freeShippingMilestone = data.freeShippingMilestone;
    if (!freeShippingMilestone || freeShippingMilestone <= 0) return 0;
    return Math.max(Math.floor(freeShippingMilestone / 100) / 10, 0.1);
  });
  const title = `${data.name} - Freeship`;
  const description = `Đặt món nào cũng freeship ${freeship > 0 ? `${freeship}km` : ''} toàn bộ tại chuỗi ${data.name}`;
  const deeplink = `loshipuser://loship.vn/c/${data.username}`;

  return {
    title,
    description,
    image: getImageUrl(data.avatar, BANNER_SIZE, 'o'),
    iosUrl: deeplink,
    androidUrl: deeplink,
  };
}

export function metadataEateryRating(options) {
  const { data } = options;
  if (!data) return;

  const createdBy = data.createdBy || {};
  const title = `${createdBy.name.short} nhận xét về ${data.eatery?.name}`;
  const description = data.content;
  const image = data.photos?.[0] || createdBy.avatar;
  const deeplink = `loshipuser://loship.vn/eatery-rating?id=${data.id}`;

  return {
    title,
    description,
    image: getImageUrl(image, BANNER_SIZE, 'o'),
    iosUrl: deeplink,
    androidUrl: deeplink,
  };
}
