import qs from 'qs';
import * as SEOTYPE from 'settings/seo-types';
import { APP_INFO, LINK_DOMAIN } from 'settings/variables';
import * as format from 'utils/format';
import { iife } from 'utils/helper';
import { getDesktopUrl } from 'utils/link';

const METADATA_PAGES = iife(() => {
  if (APP_INFO.serviceName === 'lomart') return require('./seo.lomart');
  return require('./seo');
});

const MAX_DESCRIPTION_LENGTH = 160;
const ALLOWED_QUERIES = ['q', 'page', 'usp'];

const METADATA_BASE = {
  type: 'website',
  iosUrl: 'loshipuser://loship.vn/section?sectionId=' + APP_INFO.shipServiceId,
  iosName: 'Loship - Ship đồ ăn rất nhanh',
  iosStoreId: '1348206645',
  androidUrl: 'loshipuser://loship.vn/section?sectionId=' + APP_INFO.shipServiceId,
  androidName: 'Loship - Ship đồ ăn rất nhanh',
  androidPackage: 'lozi.loship_user',
  fbAppId: '600859170003466',
  // pageId: '380760968645981',
  link: [],
};

const METADATA_GLOBAL = iife(() => {
  if (APP_INFO.serviceName === 'lomart') {
    return {
      ...METADATA_BASE,
      title: 'Đặt mua rau thịt cá trực tuyến và giao rau thịt cá tận nơi chỉ trong 1 giờ với Lomart',
      description: 'Tìm siêu thị, cửa hàng tiện lợi để đặt Lomart giao ngay trong 1 giờ với Lomart',
      keywords: 'lomart, lomart.vn, siêu thị, cửa hàng tiện lợi',
      image: 'https://lomart.vn/dist/images/cover-lomart.png',
      siteName: 'LOMART',
    };
  }

  return {
    ...METADATA_BASE,
    title: 'Loship - Đặt đồ ăn là miễn phí ship',
    description: 'Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ với Loship',
    keywords: 'loship, loship.vn, trà sữa ship, trà sữa giao tận nơi',
    image: 'https://loship.vn/dist/images/cover-loship-14052020.jpg',
    siteName: 'LOSHIP',
  };
});

export function injectSource(url) {
  const source = 'source=mweb';
  const alreadyHasQuery = url.indexOf('?') !== -1;
  return url + (alreadyHasQuery ? '&' : '?') + source;
}

export function head(metaData) {
  let { title, description } = metaData;
  const {
    keywords,
    image,
    type,
    siteName,
    fbAppId,
    // pageId,
    iosUrl,
    iosName,
    iosStoreId,
    androidUrl,
    androidName,
    androidPackage,
    link,
    fbTitle,
    fbDescription,
    ogUrl,
  } = metaData;

  title = format.plains(title);
  description = format.plains(description);
  const headObj = {
    title,
    meta: [
      { name: 'description', content: description.slice(0, MAX_DESCRIPTION_LENGTH) },
      { name: 'keywords', content: format.plains(keywords) },
      { property: 'og:title', content: fbTitle || title },
      { property: 'og:description', content: fbDescription || description },
      { property: 'og:image', content: image },
      { property: 'og:type', content: type },
      { property: 'og:site_name', content: siteName },
      // { property: 'fb:pages', content: pageId },
      { property: 'fb:app_id', content: fbAppId },
      { property: 'og:url', content: ogUrl },
    ],
    link,
  };

  if (metaData.imageWidth) headObj.meta.push({ property: 'og:image:width', content: metaData.imageWidth });
  if (metaData.imageHeight) headObj.meta.push({ property: 'og:image:height', content: metaData.imageHeight });

  if (metaData.nofollow === true) headObj.meta.push({ name: 'robots', content: 'noindex' });
  else headObj.meta.push({ name: 'robots', content: 'index,follow' });

  if (iosUrl) headObj.meta.push({ property: 'al:ios:url', content: injectSource(iosUrl) });
  if (androidUrl) headObj.meta.push({ property: 'al:android:url', content: injectSource(androidUrl) });

  if (iosName) headObj.meta.push({ property: 'al:ios:app_name', content: iosName });
  if (iosStoreId) headObj.meta.push({ property: 'al:ios:app_store_id', content: iosStoreId });

  if (androidName) headObj.meta.push({ property: 'al:android:app_name', content: androidName });
  if (androidPackage) headObj.meta.push({ property: 'al:android:package', content: androidPackage });

  // Remove undefined metatags
  headObj.meta = headObj.meta.filter((n) => !!n.content);
  return headObj;
}

export function metadata(seoOptions = {}) {
  const { type, ...rest } = seoOptions;
  let tmpOptions = {};

  if (type) {
    switch (type) {
      case SEOTYPE.SEARCHPAGE:
        tmpOptions = METADATA_PAGES.metadataSearch(rest);
        break;
      case SEOTYPE.MERCHANT:
        tmpOptions = METADATA_PAGES.metadataMerchant(rest);
        break;
      case SEOTYPE.MERCHANT_MANAGER:
        tmpOptions = METADATA_PAGES.metadataMerchantManager(rest);
        break;
      case SEOTYPE.ORDER_HISTORY:
        tmpOptions = METADATA_PAGES.metadataOrderHistory(rest);
        break;
      case SEOTYPE.ORDER_DETAIL:
        tmpOptions = METADATA_PAGES.metadataOrderDetail(rest);
        break;
      case SEOTYPE.PARTNER_TRIAL:
        tmpOptions = METADATA_PAGES.metadataPartnerTrial(rest);
        break;
      case SEOTYPE.GROUP_ORDER:
        tmpOptions = METADATA_PAGES.metadataGroupOrder(rest);
        break;
      case SEOTYPE.NOT_FOUND:
        tmpOptions = METADATA_PAGES.metadataNotFound(rest);
        break;
      case SEOTYPE.ITEM:
        tmpOptions = METADATA_PAGES.metadataItem(rest);
        break;
      case SEOTYPE.EVENT:
        tmpOptions = METADATA_PAGES.metadataEvent(rest);
        break;
      case SEOTYPE.EATERY_CHAIN:
        tmpOptions = METADATA_PAGES.metadataEateryChain(rest);
        break;
      case SEOTYPE.EATERY_RATING:
        tmpOptions = METADATA_PAGES.metadataEateryRating(rest);
        break;
      default:
        tmpOptions = {};
        break;
    }
  } else {
    tmpOptions = {};
  }

  const ttt = { ...METADATA_GLOBAL, ...tmpOptions };
  if (rest.location && rest.location.pathname) {
    if (!ttt.link) ttt.link = [];
    if (!ttt.excludedQueries) ttt.excludedQueries = [];

    ttt.link = ttt.link.filter((s) => s.rel === 'canonical' || s.rel === 'alternate');
    let fullPath = rest.location.pathname;
    // Generate canonical url for tracked url

    if (ttt.link.length === 0) {
      if (rest.location.search) {
        const queryObj = qs.parse(rest.location.search.slice(1, rest.location.search.length));

        const filteredQueryObj = { ...queryObj };
        // Remove all invalid query params (not in ALLOWED_QUERIES)
        Object.keys(queryObj).forEach((k) => {
          if (ALLOWED_QUERIES.indexOf(k) < 0 || ttt.excludedQueries.indexOf(k) >= 0) {
            delete filteredQueryObj[k];
          }
        });

        if (Object.keys(filteredQueryObj).length > 0) fullPath += `?${qs.stringify(filteredQueryObj)}`;
        const canonical = {
          rel: 'canonical',
          href: getDesktopUrl(fullPath),
        };
        const can = ttt.link.findIndex((l) => l.rel === 'canonical');
        if (can >= 0) ttt.link[can] = canonical;
        else ttt.link.push(canonical);
      } else {
        const canonical = {
          rel: 'canonical',
          href: getDesktopUrl(fullPath),
        };
        ttt.link.push(canonical);
      }
    }

    ttt.ogUrl = LINK_DOMAIN + fullPath;
  }

  return ttt;
}
