import moment from 'utils/shims/moment';
import * as cookie from 'utils/shims/cookie';
import weekdays from 'utils/json/weekday';

const locale = cookie.get('locale');
const formatString = !locale || locale === 'vi_VN' ? 'Do [tháng] M' : 'Do MMM';

function isOverlap({ start, finish, day, matrix }) {
  if (start >= finish) return false;
  if (!Array.isArray(matrix[day])) return false;

  return matrix.some((tuple) => {
    if (tuple[0] - 30 < start && tuple[1] + 30 > start) return true;
    if (tuple[0] - 30 < finish && tuple[0] + 30 > finish) return true;
    return false;
  });
}

export const convertFromNow = function (time) {
  const x = moment(time);
  const t = moment(new Date());
  if (t.diff(x, 'days') > 0) {
    if (t.year() - x.year() > 0) {
      return x.format(`H:mm, ${formatString}, YYYY`);
    }

    return x.format(`H:mm, ${formatString}`);
  }
  return x.fromNow(true);
};

export const convertMinuteToMoment = function (minutes) {
  const h = minutes / 60;
  const m = (h - Math.floor(h)) * 60;
  return moment({ h: Math.floor(h), m });
};

export const convertToMinutes = function (time) {
  const momentTime = moment(time, ['HH:mm A', 'H:mm A']);
  const h = momentTime.hour() * 60;
  const m = momentTime.minutes();
  return h + m;
};

export const convertDate = function (time) {
  const x = moment(time);
  return x.format(`${formatString}, YYYY`);
};

export const converShortDate = function (time) {
  const x = moment(time);
  return x.format('DD/MM/YYYY');
};

export function isMerchantClosing(startTime, closeTime) {
  const begin = moment(startTime, ['HH:mm A', 'H:mm A']);
  const end = moment(closeTime, ['HH:mm A', 'H:mm A']);
  const current = moment();

  const open = begin.hour() * 60 + begin.minute();
  let close = end.hour() * 60 + end.minute();
  let now = current.hour() * 60 + current.minute();

  if (open > close) {
    if (now >= 0 && now <= close) {
      now += 24 * 60;
    }
    close += 24 * 60; // to the next day
  }
  return now < close && now + 30 > close;
}

export function isNowMerchantOpen(startTime, closeTime) {
  if (!startTime) {
    return false;
  }
  const begin = moment(startTime, ['HH:mm A', 'H:mm A']);
  const end = moment(closeTime, ['HH:mm A', 'H:mm A']);
  const current = moment();

  const open = begin.hour() * 60 + begin.minute();
  let close = end.hour() * 60 + end.minute();
  let now = current.hour() * 60 + current.minute();

  if (open > close) {
    if (now >= 0 && now <= close) {
      now += 24 * 60;
    }
    close += 24 * 60; // to the next day
  }

  if (now >= open && now < close) return true;

  return false;
}

export const timeRangesBefore = function (dateTime, getString) {
  const now = moment();
  let stringTime = '';
  const years = now.diff(dateTime, 'years');
  const months = now.diff(dateTime, 'months');
  const days = now.diff(dateTime, 'days');
  const hours = now.diff(dateTime, 'hours');
  if (hours > 0) {
    stringTime = hours + getString(' hours ago');
  }
  if (days > 0) {
    stringTime = days + getString(' days ago');
  }
  if (months > 0) {
    stringTime = months + getString(' months ago');
  }
  if (years > 0) {
    stringTime = years + getString(' years ago');
  }

  return stringTime;
};

// HANDLE OPERATING TIME ---------------------------------------------------------------
// PASS MOMENT OBJECT HERE
export function getTimeValueInWeek(time) {
  if (!moment.isMoment(time)) return 0;

  // HANDLE 11:59 PM from data
  if (time.format('h:mm A') === '11:59 PM') time.add(1, 'minute');

  return time.diff(moment().startOf('week'), 'minute');
}

export function getTimeFormatFromValue(value = 0, format) {
  const time = moment().startOf('week');
  time.add(value, 'minutes');
  return time.format(format || 'h:mm A');
}

export function getDayIndexFromValue(value) {
  const time = moment().startOf('week');
  time.add(value, 'minutes');
  return time.format('e');
}

export function getMatrix(operatingTimes, sort = true) {
  if (!Array.isArray(operatingTimes) || !operatingTimes.length) return [];
  const doubleChecked = { sun: 0, mon: 0, tue: 0, wed: 0, thu: 0, fri: 0, sat: 0 };
  const matrix = [];

  operatingTimes.forEach((operatingTime) => {
    const { start, finish, weekday } = operatingTime;
    const weekdayIndex = (weekdays.find((x) => x.slug === weekday) || {})._id;

    const startValue = getTimeValueInWeek(moment(`${weekdayIndex - 1} ${start}`, 'e h:mm A'));
    const finishValue = getTimeValueInWeek(moment(`${weekdayIndex - 1} ${finish}`, 'e h:mm A'));
    if (isOverlap({ start: startValue, finish: finishValue, day: weekday, matrix })) return;

    if (doubleChecked[weekday] < 2) {
      matrix.push([startValue, finishValue]);
      doubleChecked[weekday] += 1;
    }
  });

  return sort ? sortMatrix(matrix) : matrix;
}

export function getDefaultMatrix() {
  const operatingTimes = [
    { start: '10:00 AM', finish: '10:00 PM', weekday: 'sun' },
    { start: '10:00 AM', finish: '10:00 PM', weekday: 'mon' },
    { start: '10:00 AM', finish: '10:00 PM', weekday: 'tue' },
    { start: '10:00 AM', finish: '10:00 PM', weekday: 'wed' },
    { start: '10:00 AM', finish: '10:00 PM', weekday: 'thu' },
    { start: '10:00 AM', finish: '10:00 PM', weekday: 'fri' },
    { start: '10:00 AM', finish: '10:00 PM', weekday: 'sat' },
  ];

  return getMatrix(operatingTimes);
}

// Reduce stress when parsing data
export function reduceMatrix(matrix) {
  if (!Array.isArray(matrix)) return {};

  return matrix.reduce((res, tuple) => {
    const dayIndex = getDayIndexFromValue(tuple[0]);
    res[dayIndex] = (res[dayIndex] || []).concat([tuple]);

    return res;
  }, {});
}

export function getOriginMatrix(reducedMatrix, sort = true) {
  const matrix = Object.keys(reducedMatrix).reduce((res, data) => {
    return res.concat(reducedMatrix[data]);
  }, []);

  return sort ? sortMatrix(matrix) : matrix;
}

export function sortMatrix(matrix) {
  const newMatrix = [...matrix];
  newMatrix.sort((a, b) => a[0] - b[0]);

  return newMatrix;
}

export function getOperatingTimes(matrix, filterValue = {}) {
  if (!Array.isArray(matrix)) return [];
  const validWeekdays = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

  const parsedData = sortMatrix(matrix)
    .reduce((res, tuple) => {
      const start = getTimeFormatFromValue(tuple[0], 'h:mm A');
      let finish = getTimeFormatFromValue(tuple[1], 'h:mm A');
      if (finish === '12:00 AM') finish = '11:59 PM';
      const dayIndex = getDayIndexFromValue(tuple[0]);

      if (filterValue[dayIndex]) return res;
      const isMaximum = res.filter(
        (operatingTime) => operatingTime.weekday === validWeekdays[dayIndex],
      ).length >= 2;

      // HANDLE OVERNIGHT CONDITION
      if (tuple[1] < tuple[0] && finish !== '11:59 PM') {
        const nextDayIndex = (dayIndex + 1) % 7;
        return res.concat([
          !isMaximum && { start, finish: '11:59 PM', weekday: validWeekdays[dayIndex] },
          { start: '12:00 AM', finish, weekday: validWeekdays[nextDayIndex] },
        ]);
      }

      // PREVENT TIME OVERLAP
      return res.concat(!isMaximum && { start, finish, weekday: validWeekdays[dayIndex] });
    }, [])
    .filter((c) => !!c);

  return parsedData.sort((time1, time2) => {
    const id1 = (weekdays.find((x) => x.slug === time1.weekday) || {})._id - 1;
    const id2 = (weekdays.find((x) => x.slug === time2.weekday) || {})._id - 1;

    const start1 = getTimeValueInWeek(moment(`${id1} ${time1.start}`, 'e h:mm A'));
    const start2 = getTimeValueInWeek(moment(`${id2} ${time2.start}`, 'e h:mm A'));

    return start1 - start2;
  });
}

export function getNextOperatingTime(input) {
  const operatingTime = typeof input.toJS === 'function' ? input.toJS() : [...input];
  if (!Array.isArray(operatingTime) || operatingTime.length === 0) return null;

  const validWeekdays = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
  const MINUTES_IN_WEEK = 24 * 60 * 7;
  const matrix = getMatrix(operatingTime);
  const currentValue = getTimeValueInWeek(moment());

  let target;
  matrix.some((tuple) => {
    const newTuple = [...tuple];
    // PAST TIME. CONVERT TO NEXT-WEEK VALUE
    if (newTuple[0] < currentValue && newTuple[1] < currentValue) {
      (newTuple[0] += MINUTES_IN_WEEK) && (newTuple[1] += MINUTES_IN_WEEK);
    }

    const dayIndex = getDayIndexFromValue(newTuple[0]);
    if (newTuple[0] <= currentValue && newTuple[1] >= currentValue) {
      target = { tuple: newTuple, weekday: validWeekdays[dayIndex], isOpening: true };
      return true;
    }

    if (!target) target = { tuple: newTuple, weekday: validWeekdays[dayIndex] };
    if (target.tuple[0] > newTuple[0]) {
      target = { tuple: newTuple, weekday: validWeekdays[dayIndex] };
    }

    return false;
  });

  const nextOperatingTime = {
    start: getTimeFormatFromValue(target.tuple[0], 'HH:mm'),
    finish: getTimeFormatFromValue(target.tuple[1], 'HH:mm'),
    weekday: target.weekday,
  };

  return nextOperatingTime;
}

export function getNextOperatingStatus(getString, status = {}) {
  const operatingStatus = typeof status.toJS === 'function' ? status.toJS() : { ...status };
  if (typeof getString !== 'function') return operatingStatus;

  const { isOpening, minutesUntilNextStatus } = operatingStatus;
  operatingStatus.openingStatus = getString(isOpening ? 'opening' : 'not_opened_yet', 'eatery');

  if (minutesUntilNextStatus <= 60 && minutesUntilNextStatus > 0) {
    const time = moment().add(minutesUntilNextStatus, 'minutes');
    operatingStatus.openingStatus = getString(isOpening ? 'close_soon' : 'open_soon', 'eatery', [time.format('HH:mm')]);
  }

  return operatingStatus;
}

export function isOpenAllWeek(matrix, checkOpen24h = false) {
  if (!Array.isArray(matrix) || matrix.length === 0) return true;

  const sortedMatrix = sortMatrix(matrix);
  if (sortedMatrix.length % 7 !== 0) return false;

  const MINUTES_IN_A_DAY = 24 * 60;
  const hasSecond = sortedMatrix.length === 14;

  const baseValues = sortedMatrix[0];
  const baseValuesSecond = (hasSecond && sortedMatrix[1]) || [];

  const isOpeningAllWeek = sortedMatrix.every((tuple, index) => {
    // index is zero-based
    const base = hasSecond && index % 2 !== 0 ? baseValuesSecond : baseValues;
    const distanceFromBase = MINUTES_IN_A_DAY * (hasSecond ? Math.floor(index / 2) : index);

    if (base[0] + distanceFromBase !== tuple[0]) return false;
    if (base[1] + distanceFromBase !== tuple[1]) return false;

    return true;
  });

  const isOpening24h = isOpeningAllWeek && baseValues[0] === 0 &&
    baseValues[1] === MINUTES_IN_A_DAY - 1;
  return checkOpen24h ? isOpening24h : isOpeningAllWeek;
}
// // HANDLE OPERATING TIME - END ---------------------------------------------------------
