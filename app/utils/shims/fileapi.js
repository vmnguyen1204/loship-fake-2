const canUseDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

function fileAPI() {
  require('fileapi');
  require('fileapi/plugins/FileAPI.exif');
  window.FileAPI.image = window.FileAPI.Image;
  return window.FileAPI;
}

export default canUseDOM ? fileAPI() : {};
