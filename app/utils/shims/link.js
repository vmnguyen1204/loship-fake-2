import React from 'react';
import { Link } from 'react-router-dom';
import { LINK_DOMAIN } from 'settings/variables';
import { iife } from 'utils/helper';

class CustomLink extends React.Component {
  render() {
    this.handleGrepLink();

    const { children, onClick, lzPos, ...rest } = this.props;
    let href = rest.href;
    if (rest.to && rest.to.includes('http')) href = rest.to;

    if (href) {
      return (
        <a {...rest} href={href} onClick={this.handleClick(onClick, lzPos)}>
          {children}
        </a>
      );
    }
    return (
      <Link {...rest} onClick={this.handleClick(onClick, lzPos)}>
        {children}
      </Link>
    );
  }

  handleClick = (onClick, lzPos) => (e) => {
    if (typeof onClick === 'function') onClick(e);
    if (typeof lzPos !== 'undefined') window._clickPos = lzPos;
  };

  handleGrepLink = () => {
    let link = this.props.href || this.props.to;
    if (!link) return;
    link = link.split('?')[0];

    const isValidSiteLink = iife(() => {
      if (link.includes(LINK_DOMAIN)) return true;
      if (link[0] === '/') {
        link = LINK_DOMAIN + link;
        return true;
      }
      return false;
    });

    if (isValidSiteLink) globalThis.siteLinks = (globalThis.siteLinks || []).concat(link);
  }
}

export default CustomLink;
