import { APP_INFO } from 'settings/variables';

import crossStorage from '@colorye/cross-storage';
import loziServices from 'utils/json/loziServices';

function localStorage() {
  if (typeof window === 'undefined') return null;

  if (!window.localStorage) {
    Object.defineProperty(
      window,
      'localStorage',
      new (function () {
        const aKeys = [],
              oStorage = {};
        Object.defineProperty(oStorage, 'getItem', {
          value(sKey) {
            return sKey ? this[sKey] : null;
          },
          writable: false,
          configurable: false,
          enumerable: false,
        });
        Object.defineProperty(oStorage, 'key', {
          value(nKeyId) {
            return aKeys[nKeyId];
          },
          writable: false,
          configurable: false,
          enumerable: false,
        });
        Object.defineProperty(oStorage, 'setItem', {
          value(sKey, sValue) {
            if (!sKey) {
              return;
            }
            document.cookie = escape(sKey) + '=' + escape(sValue) + '; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/';
          },
          writable: false,
          configurable: false,
          enumerable: false,
        });
        Object.defineProperty(oStorage, 'length', {
          get() {
            return aKeys.length;
          },
          configurable: false,
          enumerable: false,
        });
        Object.defineProperty(oStorage, 'removeItem', {
          value(sKey) {
            if (!sKey) {
              return;
            }
            document.cookie = escape(sKey) + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/';
          },
          writable: false,
          configurable: false,
          enumerable: false,
        });
        this.get = function () {
          let iThisIndx;
          for (const sKey in oStorage) {
            if ({}.hasOwnProperty.call(oStorage, sKey)) {
              iThisIndx = aKeys.indexOf(sKey);
              if (iThisIndx === -1) {
                oStorage.setItem(sKey, oStorage[sKey]);
              } else {
                aKeys.splice(iThisIndx, 1);
              }
              delete oStorage[sKey];
            }
          }
          for (aKeys; aKeys.length > 0; aKeys.splice(0, 1)) {
            oStorage.removeItem(aKeys[0]);
          }
          for (
            let aCouple, iKey, nIdx = 0, aCouples = document.cookie.split(/\s*;\s*/);
            nIdx < aCouples.length;
            nIdx++
          ) {
            aCouple = aCouples[nIdx].split(/\s*=\s*/);
            if (aCouple.length > 1) {
              oStorage[(iKey = unescape(aCouple[0]))] = unescape(aCouple[1]);
              aKeys.push(iKey);
            }
          }
          return oStorage;
        };
        this.configurable = false;
        this.enumerable = true;
      })(),
    );
  }
  return window.localStorage;
}

export default localStorage();

export async function getLoXStorage(serviceName, { onConnect } = {}) {
  if (serviceName && serviceName !== APP_INFO.serviceName) {
    const service = loziServices.find((cService) => cService.name === serviceName);
    return crossStorage.connect(service.domain, { callback: onConnect });
  }
  const storage = localStorage();
  storage.__proto__.disconnect = function () {}; // TODO: hacking
  typeof onConnect === 'function' && onConnect(storage);
  return storage;
}
