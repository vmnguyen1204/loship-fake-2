import cookies from 'js-cookie';

export function get(key) {
  return cookies.get(key) || global[key];
}

export function set(key, value, expires = 31536000) {
  const expiredTime = new Date(new Date().getTime() + expires * 1000);
  return cookies.set(key, value, { expires: expiredTime, sameSite: 'none', secure: true });
}

export function expire(key) {
  return cookies.remove(key);
}
