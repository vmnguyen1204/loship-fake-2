import moment from 'moment';
import * as cookie from 'utils/shims/cookie';

const locale = cookie.get('locale');
// FIX: suffix appear in `vừa xong`
moment.fn.fromNowOrNow = function fromNowOrNow(a) {
  if ((!locale || locale === 'vi_VN') && Math.abs(moment().diff(this)) < 44000) {
    // 44 seconds before or after now
    return 'vừa xong';
  }
  return this.fromNow(a);
};

if (!locale || locale === 'vi_VN') {
  moment.locale('vi', {
    relativeTime: {
      future: 'trong %s',
      past: '%s trước',
      s: 'vừa xong',
      ss: 'vừa xong',
      m: '1 phút',
      mm: '%d phút',
      h: '1 giờ',
      hh: '%d giờ',
      d: '1 ngày',
      dd: '%d ngày',
      M: '1 tháng',
      MM: '%d tháng',
      y: '1 năm',
      yy: '%d năm',
    },
    months: [
      'tháng 1',
      'tháng 2',
      'tháng 3',
      'tháng 4',
      'tháng 5',
      'tháng 6',
      'tháng 7',
      'tháng 8',
      'tháng 9',
      'tháng 10',
      'tháng 11',
      'tháng 12',
    ],
  });
}

export default moment;
