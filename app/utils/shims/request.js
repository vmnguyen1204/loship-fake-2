import request, { Request } from 'superagent';
import { LINK_API } from 'settings/variables';
import * as cookie from 'utils/shims/cookie';
import { getClientLocale } from 'utils/i18n';

const end = Request.prototype.end;

function appendPrefix(req) {
  const { url } = req;

  if (url && url[0] === '/') {
    req.url = LINK_API + url.replace(/\/v1\//g, '/');
  } else if (url && url[0] === '_') req.url = url.slice(1);
}

function setHeader(req) {
  // Set Lozi client Id
  if (!req.header['X-Lozi-Client']) req.set('X-Lozi-Client', 1);
  // Set CityId only if not defined
  if (!req.header['X-City-ID']) req.set('X-City-ID', 50);
  // Set language
  req.set('Accept-Language', getClientLocale());
  // Set accessToken
  if (!req.header['X-Access-Token']) {
    if (cookie.get('accessToken')) req.set('X-Access-Token', cookie.get('accessToken'));
  }
}

Request.prototype.accessToken = function (accessToken) {
  if (!accessToken) return this;
  return this.set('X-Access-Token', accessToken).withCredentials();
};

Request.prototype.additionalHeaders = function (additionalHeaders) {
  if (!additionalHeaders || Object.keys(additionalHeaders).length === 0) return this;
  Object.keys(additionalHeaders).forEach((header) => {
    this.set(header, additionalHeaders[header]);
  });

  return this;
};

Request.prototype.end = function (options = {}) {
  appendPrefix(this);

  if (!options.noHeader) setHeader(this);

  return new Promise((resolve, reject) => {
    end.call(this, (err, res) => {
      if (!res) {
        if (!res) return reject(err);
        console.error('Error!', res.error);
        console.error('User-agent:', global.ua);
        resolve(res.error.status);
      }
      return resolve(res);
    });
  });
};

export default request;
