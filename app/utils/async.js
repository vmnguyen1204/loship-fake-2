import Spinner from 'components/statics/spinner';
import React from 'react';

class AsyncComponent extends React.Component {
  state = { component: null };

  componentDidMount() {
    this._isMount = true;
    this.loadComponent();
  }

  componentWillUnmount() {
    this._isMount = false;
  }

  render() {
    return this.props.children(this.state.component);
  }

  loadComponent = async () => {
    const component = await this.props.load();
    if (!this._isMount) return;
    this.setState({ component: component.default ? component.default : component });
  };
}

export default AsyncComponent;

export function lazyComp(loader, Loading) {
  if (typeof loader !== 'function') return null;
  const LoadedComponent = (props) => (
    <AsyncComponent load={loader}>
      {(Component) => (() => {
        if (Component) return <Component {...props} />;
        if (Loading !== undefined) return Loading;
        return (
          <div className="async-loading-container">
            <Spinner type="wave" />
          </div>
        );
      })()}
    </AsyncComponent>
  );

  return LoadedComponent;
}

export function lazy(loader, path) {
  if (process.env.BROWSER) return loader();
  return require(path);
}
