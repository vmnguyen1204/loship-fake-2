const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { StatsWriterPlugin } = require('webpack-stats-plugin');

const TESTING = !!process.env.BUNDLE_TEST;

const config = {
  mode: 'production',
  devtool: false,
  stats: { children: false },
  entry: { bundle: ['./client'] },
  output: {
    path: path.join(__dirname, 'dist'),
    pathinfo: false,
    filename: '[name].[contenthash].js',
    publicPath: '/dist/',
  },
  resolve: {
    modules: ['node_modules', 'app'],
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: { plugins: () => [autoprefixer] },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpg|svg|gif)$/,
        use: ['file-loader'],
      },
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        use: ['file-loader?name=fonts/[name].[ext]'],
      },
      {
        test: /\.font\.js/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'webfonts-loader'],
      },
    ],
  },
  plugins: [
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /vi/),
    new webpack.DefinePlugin({
      'process.env': {
        BROWSER: JSON.stringify(true),
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new MiniCssExtractPlugin({
      filename: 'styles/main.[contenthash].css',
      chunkFilename: 'styles/[id].[hash].css',
    }),
    new StatsWriterPlugin({
      filename: 'resource-stats.json',
      transform(data) {
        return JSON.stringify({
          ...data.assetsByChunkName,
          bundle: data.assetsByChunkName.bundle.find(
            (file) => file.includes('bundle'),
          ),
          style: data.assetsByChunkName.bundle.find(
            (file) => file.includes('main'),
          ),
        }, null, 2);
      },
    }),
    TESTING && new BundleAnalyzerPlugin(),
  ].filter((plugin) => !!plugin),
  optimization: {
    moduleIds: 'hashed',
    removeAvailableModules: false,
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/](?!react|react-dom|moment|immutable).*[\\/]/,
          name: 'vendor',
          chunks: 'all',
        },
        core: {
          test: /[\\/]node_modules[\\/](react|react-dom|moment|immutable).*[\\/]/,
          name: 'core',
          chunks: 'all',
        },
      },
    },
  },
};

exports.default = TESTING ? new SpeedMeasurePlugin().wrap(config) : config;
