import fs from 'fs';
import map from './dist/map';
import path from 'path';

function getFileList() {
  const fileListPath = path.join(__dirname, 'dist', 'filenames.txt');
  if (!fs.existsSync(fileListPath)) return console.error(`Unable to scan directory: ${fileListPath}`);
  const content = fs.readFileSync(fileListPath, 'utf8');
  return content.split('\n');
}

function getFileContent(filePath) {
  if (!fs.existsSync(filePath)) return console.error(`Unable to read file: ${filePath}`);
  return fs.readFileSync(filePath, 'utf8');
}

const filenames = getFileList();
filenames.forEach((filePath, index) => {
  // if (index !== 140) return;
  let fileContent = getFileContent(filePath);
  const fileContentSrc = fileContent;

  const regex = /getString\([^)]+\)/gm;
  let matched;
  do {
    matched = regex.exec(fileContentSrc);
    if (!matched) break;

    let matchedGroup = matched[0];
    const matchedGroupSrc = matchedGroup;
    matchedGroup = matchedGroup.replace(/\n/g, '::');

    const regex1 = /\'(\\'|[^'])+\'/g;
    const regex2 = /\"(\\"|[^"])+\"/g;
    const regex3 = /\`(\\`|[^`])+\`/g;
    let matchedStr;
    do {
      matchedStr = regex1.exec(matchedGroupSrc) || regex2.exec(matchedGroupSrc) || regex3.exec(matchedGroupSrc);
      if (!matchedStr) break;

      const key = matchedStr[0].slice(1, -1).replace(/\\'/g, "'");
      const apostrophe = matchedStr[0][0];

      if (map[key]) matchedGroup = matchedGroup.replace(matchedStr[0], `${apostrophe}${map[key]}${apostrophe}`);
      else console.error(`Cannot find string in map file: ${key}`);
    } while (matchedStr);

    matchedGroup = matchedGroup.replace(/::/g, '\n');
    fileContent = fileContent.replace(matchedGroupSrc, matchedGroup);
  } while (matched);

  fs.writeFileSync(filePath, fileContent, { mode: 0o755 });
});
