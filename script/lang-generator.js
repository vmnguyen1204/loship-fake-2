import en_US from '../site/app/utils/languages/default/en_US';
import fs from 'fs';
import path from 'path';
import shortId from 'shortid';
import vi_VN from '../site/app/utils/languages/default/vi_VN';
import { slugify } from '../core/utils/format';

const FILENAME = {
  VI: 'vi_VN.js',
  EN: 'en_US.js',
};

function getLanguageKeyPairs(lang, rootKeyPairs) {
  if (!rootKeyPairs) rootKeyPairs = { vi: {}, en: {}, map: {} };

  Object.entries(lang).forEach(([key, value]) => {
    if (typeof value !== 'string') {
      return getLanguageKeyPairs(value, rootKeyPairs);
    }

    let newKey = slugify(key, '_');
    if (!newKey || rootKeyPairs.map[newKey]) newKey = shortId.generate();

    rootKeyPairs.vi[newKey] = value;
    rootKeyPairs.en[newKey] = key;
    rootKeyPairs.map[key] = newKey;
  }, rootKeyPairs);

  return rootKeyPairs;
}

function getEnKeyPairs(lang, rootKeyPairs) {
  Object.entries(lang).forEach(([key, value]) => {
    if (typeof value !== 'string') {
      return getEnKeyPairs(value, rootKeyPairs);
    }

    const existedKey = rootKeyPairs.map[key];
    if (existedKey) rootKeyPairs.en[existedKey] = value;
    // existedKey must exist, ignore if not.
  });

  return rootKeyPairs;
}

export function generateFileContent(src) {
  const srcStr = Object.entries(src)
    .map(([key, value]) => {
      const keyApostrophe = key.includes('"') ? "'" : '"';
      const valueApostrophe = value.includes('"') ? "'" : '"';

      const newKey = (() => {
        if (keyApostrophe === "'" && key.includes("'")) return key.replace(/'/g, "\\'");
        return key;
      })().replace('\n', '\\n');
      const newValue = (() => {
        if (valueApostrophe === "'" && value.includes("'")) return value.replace(/'/g, "\\'");
        return value;
      })().replace('\n', '\\n');

      return `${keyApostrophe}${newKey}${keyApostrophe}: ${valueApostrophe}${newValue}${valueApostrophe},`;
    })
    .join('\n  ');

  return `export default {\n  ${srcStr}\n}`;
}

const keyPairs = getLanguageKeyPairs(vi_VN);
getEnKeyPairs(en_US, keyPairs);

const [viFile, enFile, mapFile] = [
  generateFileContent(keyPairs.vi),
  generateFileContent(keyPairs.en),
  generateFileContent(keyPairs.map),
];

try {
  const dir = path.join(__dirname, 'dist');
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  fs.writeFileSync(`${dir}/${FILENAME.VI}`, viFile, { mode: 0o755 });
  fs.writeFileSync(`${dir}/${FILENAME.EN}`, enFile, { mode: 0o755 });
  fs.writeFileSync(`${dir}/map.js`, mapFile, { mode: 0o755 });
} catch (err) {
  // An error occurred
  console.error(err);
}
