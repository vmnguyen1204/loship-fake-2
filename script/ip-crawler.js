import axios from 'axios';
import fs from 'fs';
import path from 'path';

axios
  .get('https://lite.ip2location.com/viet-nam-ip-address-ranges')
  .then(response => {
    if (response.status === 200) {
      const html = response.data;
      let tbody = html.split('<tbody>')[1] || '';
      tbody = tbody.split('</tbody>')[0] || '';
      tbody = tbody.replace(/[\t\n]/g, '');
      tbody = tbody.replace(/\<\/tr\>/g, '</tr>\n');

      const ipRange = tbody
        .split('\n')
        .map(line => {
          const regex = /([0-9]+).([0-9]+).([0-9]+).([0-9]+)/g;
          const [from] = regex.exec(line) || [];
          const [to] = regex.exec(line) || [];
          if (from && to) return [from, to];
        })
        .filter(Boolean);

      try {
        const dir = path.join(__dirname, 'dist');
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }

        fs.writeFileSync(`${dir}/ip-range-vn.json`, JSON.stringify(ipRange), { mode: 0o755 });
      } catch (err) {
        // An error occurred
        console.error(err);
      }
    }
  })
  .catch(err => {
    throw new Error(err);
  });
