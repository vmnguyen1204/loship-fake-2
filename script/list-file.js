import fs from 'fs';
import path from 'path';

const BASE_PATH = {
  CORE: path.join(__dirname, '..', 'core'),
  APP: path.join(__dirname, '..', 'site/app'),
};

function getAllJSFileInDirectory(basePath) {
  if (!fs.existsSync(basePath)) return console.error(`Unable to scan directory: ${basePath}`);

  const files = fs.readdirSync(basePath);
  return files
    .flatMap(file => {
      const filePath = `${basePath}/${file}`;
      if (fs.statSync(filePath).isDirectory()) return getAllJSFileInDirectory(filePath);
      return /.jsx?$/.test(file) ? filePath : []; // only match js/jsx
    })
    .filter(Boolean);
}

let filenames = [];
filenames = filenames.concat(getAllJSFileInDirectory(BASE_PATH.CORE));
filenames = filenames.concat(getAllJSFileInDirectory(BASE_PATH.APP));

try {
  const dir = path.join(__dirname, 'dist');
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  fs.writeFileSync(`${dir}/filenames.txt`, filenames.join('\n'), { mode: 0o755 });
} catch (err) {
  // An error occurred
  console.error(err);
}
