import en_US from '../site/app/utils/languages/lomart/en_US';
import fs from 'fs';
import path from 'path';
import shortId from 'shortid';
import vi_VN from '../site/app/utils/languages/lomart/vi_VN';
import { slugify } from '../core/utils/format';

import loshipMap from './dist/map';
import loshipEn from './dist/en_US';
import loshipVi from './dist/vi_VN';
import { generateFileContent } from './lang-generator';

const FILENAME = {
  VI: 'vi_VN.lomart.js',
  EN: 'en_US.lomart.js',
};

function getLanguageKeyPairs(lang, rootKeyPairs) {
  if (!rootKeyPairs) rootKeyPairs = { vi: {}, en: {}, map: {} };

  Object.entries(lang).forEach(([key, value]) => {
    if (typeof value !== 'string') {
      return getLanguageKeyPairs(value, rootKeyPairs);
    }

    const loshipKey = key.replace(/lomart/g, 'loship').replace(/Lomart/g, 'Loship');

    let newKey = slugify(loshipKey, '_');
    if (!newKey || rootKeyPairs.map[newKey]) newKey = shortId.generate();

    if (loshipVi[newKey] !== value) rootKeyPairs.vi[newKey] = value;
    if (loshipEn[newKey] !== key) rootKeyPairs.en[newKey] = key;
    if (!loshipMap[loshipKey]) rootKeyPairs.map[key] = newKey;
  });

  return rootKeyPairs;
}

function getEnKeyPairs(lang, rootKeyPairs) {
  Object.entries(lang).forEach(([key, value]) => {
    if (typeof value !== 'string') {
      return getEnKeyPairs(value, rootKeyPairs);
    }

    const loshipKey = key.replace(/lomart/g, 'loship').replace(/Lomart/g, 'Loship');
    const existedKey = loshipMap[loshipKey] || rootKeyPairs.map[loshipKey];
    if (existedKey) rootKeyPairs.en[existedKey] = value;
    // existedKey must exist, ignore if not.
  });

  return rootKeyPairs;
}

const keyPairs = getLanguageKeyPairs(vi_VN);
getEnKeyPairs(en_US, keyPairs);

const [viFile, enFile, mapFile] = [
  generateFileContent(keyPairs.vi),
  generateFileContent(keyPairs.en),
  generateFileContent(keyPairs.map),
];

try {
  const dir = path.join(__dirname, 'dist');
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  fs.writeFileSync(`${dir}/${FILENAME.VI}`, viFile, { mode: 0o755 });
  fs.writeFileSync(`${dir}/${FILENAME.EN}`, enFile, { mode: 0o755 });
  fs.writeFileSync(`${dir}/map.lomart.js`, mapFile, { mode: 0o755 });
} catch (err) {
  // An error occurred
  console.error(err);
}
