const server = require('./dist-server');

// It will valid in production mode
let { PORT } = process.env;
if (!PORT) {
  // Development setup
  PORT = parseInt(process.env.DEV_PORT || 4000) + 1;
}

// I don't know why but we need server.default instead of server :'(
// This issue appeared after upgrading to babel6
server.default.listen(PORT, () => {
  console.info('Server is listening on', PORT);
});
