import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import express from 'express';
import hbs from 'hbs';
import path from 'path';
import { ALLOWED_DOMAINS } from 'settings/variables';
import { iife } from 'utils/helper';
import preProcessMiddleware from './middlewares/preprocess';
import routes from './routes';

globalThis.flags = {
  foreign: true,
};

globalThis.resourceStats = iife(() => {
  try {
    if (process.env.NODE_ENV !== 'production') throw new Error('Development mode');
    return require('./metadata/resource-stats.json'); // eslint-disable-line import/no-unresolved
  } catch (e) {
    return { bundle: 'bundle.js' };
  }
});

const app = express();

// Public the /dist folder to client to access domain.com/bundle.js
app.use('/dist', express.static(path.join(__dirname, '../dist')));

// Middlewares apply here
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  if (!req.headers.origin) return next();
  if (ALLOWED_DOMAINS.some((regex) => regex.test(req.headers.origin)))
    res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Headers', 'Content-Type, X-Access-Token');
  res.header('Access-Control-Allow-Credentials', 'true');
  next();
});

// Setup hbs as view engine
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));

hbs.registerHelper('assets', (filePath) => {
  if (filePath[0] === '/') return path.join('/dist', filePath.slice(1));
  return path.join('/dist', filePath);
});
hbs.registerHelper('ifEq', function(v1, v2, options) {
  if (v1 === v2) return options.fn(this);
  return options.inverse(this);
});

// Setup route
const [preRoutes, postRoutes] = routes.reduce((acc, route) => {
  if (route.enforcePre) return [acc[0].concat(route), acc[1]];
  return [acc[0], acc[1].concat(route)];
}, [[], []]);

preRoutes.forEach((route) => {
  app[route.method.toLowerCase()](route.path, route.handler);
});

app.use(preProcessMiddleware);

postRoutes.forEach((route) => {
  app[route.method.toLowerCase()](route.path, route.handler);
});

export default app;
