import CashbagApp from 'components/statics/cashbag-app';
import qs from 'qs';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { LINK_DOMAIN_LOSHIP } from 'settings/variables';
import { version } from '../../package.json';
import { generateSEO } from '../utils/seo';

export const handleCashBag = (req, res) => {
  const lang = 'vi';
  const fullUrl = LINK_DOMAIN_LOSHIP + req.originalUrl;
  const deepCashbag = qs.stringify(req.query);

  const { title, meta, link } = generateSEO({
    iosUrl: 'loshipuser://loship.vn/partners?' + deepCashbag,
    androidUrl: 'loshipuser://loship.vn/partners?' + deepCashbag,
    canonical: fullUrl,
  });

  const InitialComponent = <CashbagApp />;
  const body = renderToString(InitialComponent);
  return res.render('cashbag', {
    title,
    meta,
    link,
    body,
    lang,
    version,
    iosUrl: 'loshipuser://loship.vn/partners?' + deepCashbag,
    androidUrl: 'loshipuser://loship.vn/partners?' + deepCashbag,
  });
};

export async function handleAuthAppleId(req, res) {
  const lang = 'vi';
  const fullUrl = LINK_DOMAIN_LOSHIP + req.originalUrl;
  const { title, meta, link } = generateSEO({ canonical: fullUrl });

  const authData = req.method === 'POST' ? req.body : req.query;

  const body = `${JSON.stringify(authData)}`;
  return res.render('landing', {
    title,
    meta,
    link,
    body,
    lang,
    version,
    customData: JSON.stringify(authData),
  });
}
