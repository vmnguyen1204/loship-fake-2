import { loginWithData } from 'actions/access-control';
import { getServerStore } from 'lib/configureStore';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Helmet from 'react-helmet';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { getMerchant } from 'reducers/merchant';
import { setPartnerConfig } from 'reducers/metadata';
import { callAPI } from 'reducers/new/helper';
import routes from 'routes';
import { LINK_AUTH } from 'settings/variables';
import { encodeState } from 'utils/helper';
import { matchRoutes, register, render as renderRoutes } from 'utils/routing';
import { MAP_API_KEYS } from '../settings/variables';

export async function handleRequest (req, res) {
  const store = getServerStore(res.locals.initialServerState);

  const originalUrl = req.baseUrl + req.path;
  register(routes, originalUrl);
  const matchedRoutes = matchRoutes(routes, originalUrl);
  const { match } = matchedRoutes[matchedRoutes.length - 1] || {};

  const accessToken = res.locals.accessToken;
  const partnerConfig = res.locals.partnerConfig;
  if (partnerConfig) store.dispatch(setPartnerConfig(partnerConfig));

  function renderView() {
    const storeData = store.getState();
    const merchantUsername = match && match.params.merchantUsername;
    const merchantSlug = match && match.params.merchant;
    const currentPath = req.url;

    if (process.env.NODE_ENV === 'production' && req.hostname === 'loship.vn') {
      // this is merchant
      if (currentPath === `/${merchantUsername}` || currentPath === `/b/${merchantSlug}`) {
        const mc = getMerchant(storeData, {
          merchant: merchantSlug,
          merchantUsername,
        });

        if (mc && mc.get('data')) {
          const mcData = mc.get('data');
          const shippingService = mcData.get('shippingService');
          if (shippingService === 'lomart') return res.redirect(307, `https://lomart.vn/b/${mcData.get('slug')}`);
        }
      }
    }

    const context = {};
    const body = ReactDOMServer.renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={context}>
          {renderRoutes({
            routes,
            path: matchedRoutes[0] && matchedRoutes[0].route.path,
            originalUrl,
          })}
        </StaticRouter>
      </Provider>,
    );
    const state = encodeState(storeData);

    // Set metadata & render
    const header = Helmet.rewind();
    const title = header.title.toString();
    const meta = header.meta.toString();
    const link = header.link.toString();
    const lang = 'vi';
    const mapKey = MAP_API_KEYS[Math.floor(Math.random() * MAP_API_KEYS.length)];

    const metadata = {
      title,
      meta,
      link,
      body,
      state,
      lang,
      mapKey,
      availableAreas: encodeState(global.availableAreas),
      ...res.locals.stats,
    };
    return res.render('main', { ...metadata, env: process.env.NODE_ENV });
  }

  function renderNotFound() {
    const storeData = store.getState();
    const matchNotFoundRoutes = matchRoutes(routes, '/khong-tim-thay');

    const context = {};
    const body = ReactDOMServer.renderToString(
      <Provider store={store}>
        <StaticRouter location={req.url} context={context}>
          {renderRoutes({
            routes,
            path: matchNotFoundRoutes[0] && matchNotFoundRoutes[0].route.path,
            originalUrl: '/khong-tim-thay',
          })}
        </StaticRouter>
      </Provider>,
    );
    const state = encodeState(storeData);

    // Set metadata & render
    const header = Helmet.rewind();
    const title = header.title.toString();
    const meta = header.meta.toString();
    const link = header.link.toString();
    const lang = 'vi';
    const mapKey = MAP_API_KEYS[Math.floor(Math.random() * MAP_API_KEYS.length)];

    const metadata = {
      title,
      meta,
      link,
      body,
      state,
      lang,
      mapKey,
      availableAreas: encodeState(global.availableAreas),
      ...res.locals.stats,
    };
    return res.status(404).render('main', { ...metadata, env: process.env.NODE_ENV });
  }

  function handleError(e) {
    switch (e.status) {
      case 404:
        return renderNotFound();
      default:
        console.error('Requested URL:', originalUrl || req.url);
        console.error('Timestamp:', new Date());
        console.error('ERROR', e.status || '', ':', { type: e.type, params: e.params });
        if (e.stack) {
          if (e.filename) console.error(`Error at ${e.filename}:${e.lineNumber}:${e.columnNumber}`);
          console.error(e.stack);
        } else {
          console.error('ERROR THROWN:', JSON.stringify(e || {}));
        }
        console.error('-----------------------------');
        return res.status(503).render('fainted', {});
    }
  }

  async function serverPrefetch({ dispatch, params, query }) {
    const prefetchs = matchedRoutes
      .reduce((acc, { route }) => {
        if (!route.component?.needs) return acc;
        if (!Array.isArray(route.component.needs)) return [...acc, route.component.needs];
        return [...acc, ...route.component.needs];
      }, [])
      .filter((prefetch) => typeof prefetch === 'function');

    return Promise.all(prefetchs.map((prefetch) => dispatch(prefetch(params, query))));
  }

  try {
    const userRaw = await callAPI({
      method: 'GET',
      url: LINK_AUTH + '/users/me',
      accessToken,
    });
    if (userRaw.ok) {
      const userRes = await userRaw.json();
      store.dispatch(loginWithData(userRes.data, accessToken));
    }
    await serverPrefetch({
      dispatch: store.dispatch,
      params: { ...match?.params, ...res.locals.serverParams },
      query: req.query,
    });

    renderView();
  } catch (e) {
    handleError(e);
  }
}
