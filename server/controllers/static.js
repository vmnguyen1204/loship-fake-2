import path from 'path';
import axios from 'axios';
import fs from 'fs';

const WAIT_FOR_UNSYNC = 2000;
const RANDOM_FILENAME_LENGTH = 36;

export const serve = (req, res) => {
  let p = req.url.replace(/\?.+?$/, '');
  if (!p.match(/\./)) p += '.html';
  res.sendFile(p, {
    root: path.join(__dirname, '../../dist'),
  });
};

export const servePublic = (req, res) => {
  res.sendFile(req.params[0], {
    root: path.join(__dirname, '../../dist'),
  });
};

export const ignore = (req) => {
  console.warn('Ignore request:', req.url);
  return;
};

export const serveExternalImage = async (req, res) => {
  const url = req.params.imageUrl;
  const outputUrl = path.join(__dirname, '../../dist', 'external');
  const filename = Math.random().toString(RANDOM_FILENAME_LENGTH);

  if (!fs.existsSync(outputUrl)) fs.mkdirSync(outputUrl);
  const writer = fs.createWriteStream(outputUrl + '/' + filename);

  try {
    const response = await axios({
      url,
      method: 'GET',
      responseType: 'stream',
    });

    response.data.pipe(writer);

    await new Promise((resolve, reject) => {
      writer.on('finish', resolve);
      writer.on('error', reject);
    });

    res.sendFile(filename, { root: outputUrl });
    setTimeout(() => fs.unlinkSync(outputUrl + '/' + filename), WAIT_FOR_UNSYNC);
  } catch (e) {
    console.error('ERROR: ', e);
    res.end();
  }
};

export const handleToggle = (req, res) => {
  const { foreign } = req.query;
  globalThis.flags.foreign = foreign === 'true';
  res.redirect(303, '/');
};
