import { handleRequest } from './request';

export const delegate = (req, res, next) => {
  return handleRequest(req, res, next);
};
