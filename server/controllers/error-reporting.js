import callAPI from 'actions/helpers';

const TELEGRAM_API = 'https://api.telegram.org/bot${botToken}/sendMessage';
const BOT_API_KEY = '1001156449:AAEAlCUQwjqy7HAV4ylk8kOhNGehZH1WAyg';
const CHAT_ID = '-1001394205849';

const MAX_STACK_SIZE = 1000;
const RESEND_NOTI_AFTER_TIMES = 10;
let errorStack = {};

// TODO: save first / last timestamp -> clean up better
function cleanErrorStack() {
  if (Object.keys(errorStack).length > MAX_STACK_SIZE) errorStack = {};
}


export function handleErrorReporting(req, res) {
  cleanErrorStack();

  const { hash, message } = req.body;
  if (errorStack[hash]) {
    errorStack[hash]++;

    // Bypass and Resend notification for urgent errors
    if (errorStack[hash] % RESEND_NOTI_AFTER_TIMES !== 0) {
      res.status(409);
      return res.end();
    }
  } else {
    errorStack[hash] = 1;
  }

  callAPI('post', {
    url: TELEGRAM_API,
    params: { botToken: BOT_API_KEY },
    content: {
      chat_id: CHAT_ID,
      parse_mode: 'MarkdownV2',
      text: `*LOSHIP*: ${errorStack[hash] > 1 ? '*[URGENT]*' : ''} ${message}`,
    },
  });

  res.status(200);
  return res.end();
}
