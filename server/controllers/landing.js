import CongDongChienBinh from 'components/statics/community-driver-loship';
import CSKHSuggestFeature from 'components/statics/cskh-suggest-feature';
import CSKHSuggestLosupply from 'components/statics/cskh-suggest-losupply';
import CSKHSuggestMerchant from 'components/statics/cskh-suggest-merchant';
import MerchantLandingMenuTool from 'components/statics/merchant-landing-menu-tool';
import { getServerStore } from 'lib/configureStore';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { LINK_API_SHIPPER, LINK_AUTH, LINK_DOMAIN, LINK_DOMAIN_LOSHIP, LINK_DOMAIN_MERCHANT, LINK_DOMAIN_SHIPPER } from 'settings/variables';
import { detectMobileDevice } from 'utils/helper';
import request from 'utils/shims/request';
import { version } from '../../package.json';
import { generateSEO, metaInfoLoshipDriver, metaInfoLoshipMerchant } from '../utils/seo';

export function cskhSuggestMerchantHandler(req, res) {
  const lang = 'vi';
  const fullUrl = LINK_DOMAIN_LOSHIP + req.originalUrl;
  const { cpn, cc, countryCode } = req.query;

  const { title, meta, link } = generateSEO({ canonical: fullUrl });

  const InitialComponent = <CSKHSuggestMerchant phoneNumber={cpn} countryCode={cc || countryCode} />;
  const body = renderToString(InitialComponent);
  return res.render('landing', {
    title,
    meta,
    link,
    body,
    lang,
    version,
    gtagId: 'UA-109902111-2',
  });
}

export async function merchantLandingMenuToolHandler(req, res) {
  process.env.AGENT = detectMobileDevice(req.headers['user-agent']);
  const { username } = req.params;
  const { accessToken } = req.cookies;

  const getMerchant = request
    .get(LINK_AUTH + `/eateries/username:${username}`)
    .accessToken(accessToken)
    .end();

  const [infoMerchant] = await Promise.all([
    getMerchant.then((cRes) => {
      if (cRes.status === 200) return Promise.resolve(cRes.body.data);
      return Promise.resolve();
    }),
  ]);

  const { title, meta, link } = generateSEO({
    title: `Quản lý ${username}`,
    iosUrl: `loshipmerchant://loship.vn/${username}/menu`,
    androidUrl: `loshipmerchant://loship.vn/${username}/menu`,
    image: infoMerchant.avatar,
    description: infoMerchant.description || infoMerchant.category,
    canonical: `${LINK_DOMAIN_MERCHANT}/${username}/menu`,
  }, metaInfoLoshipMerchant);

  const lang = 'vi';

  const store = getServerStore();

  const InitialComponent = (
    <Provider store={store}>
      <MerchantLandingMenuTool isMobile={process.env.AGENT === 'mobile'} username={username} />
    </Provider>
  );
  const body = renderToString(InitialComponent);
  return res.render('landing', {
    title,
    meta,
    link,
    body,
    lang,
    version,
  });
}

export async function communityDriverHandler(req, res) {
  process.env.AGENT = detectMobileDevice(req.headers['user-agent']);
  const { id } = req.params;
  const { accessToken } = req.cookies;

  const getShipperRatingData = request
    .get(LINK_API_SHIPPER + `/best-ratings/${id}`)
    .accessToken(accessToken)
    .end();

  const [shipperRatingData] = await Promise.all([
    getShipperRatingData.then((cRes) => {
      if (cRes.status === 200) return Promise.resolve(cRes.body.data);
      return Promise.resolve();
    }),
  ]);

  if (!shipperRatingData) {
    return res.redirect(303, LINK_DOMAIN);
  }

  const { title, meta, link } = generateSEO({
    title: `Nhận xét dành cho ${shipperRatingData.ratedShipper?.name}`,
    description: shipperRatingData.content,
    image: shipperRatingData.ratedShipper?.avatar,
    iosUrl: `loshipdriver://loship.vn/rating?id=${id}`,
    androidUrl: `loshipdriver://loship.vn/rating?id=${id}`,
    canonical: `${LINK_DOMAIN_SHIPPER}/danh-gia/${id}`,
  }, metaInfoLoshipDriver);

  const lang = 'vi';

  const store = getServerStore();

  const InitialComponent = (
    <Provider store={store}>
      <CongDongChienBinh isMobile={process.env.AGENT === 'mobile'} driver={shipperRatingData} />
    </Provider>
  );
  const body = renderToString(InitialComponent);
  return res.render('landing', {
    title,
    meta,
    link,
    body,
    lang,
    version,
  });
}

export function cskhSuggestFeatureHandler(req, res) {
  const lang = 'vi';
  const fullUrl = LINK_DOMAIN_LOSHIP + req.originalUrl;
  const { cpn, cc, countryCode } = req.query;

  const { title, meta, link } = generateSEO({ canonical: fullUrl });

  const InitialComponent = <CSKHSuggestFeature phoneNumber={cpn} countryCode={cc || countryCode} />;
  const body = renderToString(InitialComponent);
  return res.render('landing', {
    title,
    meta,
    link,
    body,
    lang,
    version,
    gtagId: 'UA-109902111-2',
  });
}

export function cskhSuggestLosupplyHandler(req, res) {
  const lang = 'vi';
  const fullUrl = LINK_DOMAIN_LOSHIP + req.originalUrl;
  const { cpn, cc, countryCode } = req.query;

  const { title, meta, link } = generateSEO({ canonical: fullUrl });

  const InitialComponent = <CSKHSuggestLosupply phoneNumber={cpn} countryCode={cc || countryCode} />;
  const body = renderToString(InitialComponent);
  return res.render('landing', {
    title,
    meta,
    link,
    body,
    lang,
    version,
    gtagId: 'UA-109902111-2',
  });
}
