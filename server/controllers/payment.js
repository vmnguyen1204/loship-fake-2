import qs from 'qs';
import callAPI from 'actions/helpers';
import { LINK_DOMAIN, ZALOPAY_GATEWAY } from 'settings/variables';
import Diacritics from 'diacritic';
import request from 'utils/shims/request';

const FETCH_ORDER_BY_APPTRANSID = '/payment/zp-info/apptransid:${apptransid}';

const sendSlack = async (messagePayload = {}) => {
  const payload = {
    channel: '#payment_error',
    username: 'web-lozi',
    icon_emoji: ':scream:',
    ...messagePayload,
  };

  return request
    .post('https://hooks.slack.com/services/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')
    .send(JSON.stringify(payload))
    .end();
};

export const handleZaloPayTransaction = async (req, res) => {
  const { accessToken: cookieAccessToken } = req.cookies;
  const accessToken = req.header('X-Access-Token') || cookieAccessToken;
  if (!accessToken) return res.status(401).end('Unauthorized');
  const apptransid = req.query.apptransid;
  if (!apptransid) return res.status(400).end('Bad Request');

  const orderInfo = await callAPI('get', {
    url: FETCH_ORDER_BY_APPTRANSID,
    params: {
      apptransid,
    },
    accessToken,
  });

  if (orderInfo.status !== 200) {
    console.info('FAILED TO LOAD ORDER INFO WITH apptransid = ', apptransid);
    console.error(orderInfo.error);
    console.info('====================');
    sendSlack({
      text: `[Error ${orderInfo.status}] Failed to load order by apptransid \`${apptransid}\``,
      attachments: [
        {
          text: `accessToken: \`${accessToken}\``,
        },
        {
          text: `URL: ${LINK_DOMAIN}/payment/zp-gateway?apptransid=${apptransid}`,
        },
      ],
    });
    return res.status(400).end(`Invalid App Trans ID ${apptransid}`);
  }

  const orderInfoBody = { ...orderInfo.body.data };
  // TODO: Find a way to encode base64 that does not affect utf-8
  orderInfoBody.description = Diacritics.clean(orderInfoBody.description);
  let _order = JSON.stringify(orderInfoBody);

  _order = encodeURIComponent(_order).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
    return String.fromCharCode('0x' + p1);
  });

  _order = Buffer.from(_order).toString('base64');

  _order = encodeURIComponent(_order);

  const query = qs.stringify({ order: _order });

  const zaloPayUrl = ZALOPAY_GATEWAY + '?' + query;

  return res.redirect(307, zaloPayUrl);
};
