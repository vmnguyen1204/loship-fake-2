import { getErrorQs } from 'utils/json/error';
import qs from 'qs';

export const handleGateway = async (req, res) => {
  const accessToken = req.header('X-Access-Token') || req.cookies.accessToken;
  if (accessToken) res.cookie('accessToken', accessToken, { maxAge: 31536000000, encode: String });
  const { partnerCode, appTransId, ...rest } = req.query;

  if (!accessToken)
    return res.redirect(307, `/payment?${qs.stringify({ appTransId, partnerCode, ...rest })}&${getErrorQs('AU_001')}`);
  if (!appTransId)
    return res.redirect(307, `/payment?${qs.stringify({ appTransId, partnerCode, ...rest })}&${getErrorQs('PY_002')}`);

  switch (partnerCode) {
    case 'zp':
    case 'epay':
      return res.redirect(307, `${req.baseUrl}/payment/process/${partnerCode}?${qs.stringify({ appTransId, ...rest })}`);
    default:
      return res.redirect(307, `/payment/notice?${qs.stringify({ appTransId, partnerCode, ...rest })}&${getErrorQs('PY_003')}`);
  }
};
