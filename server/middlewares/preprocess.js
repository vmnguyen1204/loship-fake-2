import { fetchRawLinkBySharedId } from 'actions/buy';
import { injectParams } from 'actions/helpers';
import { fetchGlobalConfigs, fetchUserOrderDetailByAppTransId, fetchUserOrderDetailByMoMoTransID } from 'actions/order';
import { getServerStore } from 'lib/configureStore';
import jwt from 'lib/jwt';
import qs from 'qs';
import routes from 'routes';
import { LINK_API } from 'settings/variables';
import { detectMobileDevice, iife, isWebBotClient } from 'utils/helper';
import { defaultLocale, loadLanguage } from 'utils/i18n';
import loziServices from 'utils/json/loziServices';
import { matchRoutes } from 'utils/routing';
import moment from 'utils/shims/moment';
import request from 'utils/shims/request';
import { version } from '../../package.json';
import serverRoutes from '../routes';
import * as cache from '../utils/caching';
import { isValidIpAddress } from '../utils/helper';

function getPartnerName(req) {
  const PARTNER_NAME_MAPPING = {
    'Lozi': 'lozi',
    'MoMo': 'momo',
    'SacombankPay': 'sacombankpay',
    'ViettelPay': 'viettelpay',
  };

  if (['/m/buy', '/m/tai-khoan/don-hang', '/d/buy', '/d/tai-khoan/don-hang'].some(
    (item) => item === req.path)
  ) return PARTNER_NAME_MAPPING['Lozi'];
  if (['/g/momo'].includes((item) => item === req.path)) return PARTNER_NAME_MAPPING['MoMo'];
  if (req.query.merchant_code === 'VIETTEL') return PARTNER_NAME_MAPPING['ViettelPay'];

  const { client } = req.query;
  if (PARTNER_NAME_MAPPING[client]) return PARTNER_NAME_MAPPING[client];
}

async function handleSharingUrl(req, res, originalUrl) {
  const SHARING_URL_PATTERN = ['/order-shared/', '/danh-gia/', '/s/'];
  const SHARING_URL_MAPPER = {
    '/orders/': '/tai-khoan/don-hang/',
    '/danh-gia/': '/cong-dong-loship/',
  };
  if (!SHARING_URL_PATTERN.some((sharingUrl) => req.url.includes(sharingUrl))) return;

  const matchedRoutes = matchRoutes(routes, originalUrl);
  const { match } = matchedRoutes[matchedRoutes.length - 1] || {};
  const shortLinkId = match && match.params.groupTopic;
  if (!shortLinkId) return;

  const rawLink = await iife(async () => {
    let link;
    if (req.url.includes('/danh-gia/')) link = req.url; // case eatery-rating detail

    const store = getServerStore(res.locals.initialServerState);
    const rawData = !link && await new Promise((resolve) => {
      store.dispatch(
        fetchRawLinkBySharedId({
          sharedId: shortLinkId,
          callback: (resCallback) => resolve(resCallback.body.data),
          callbackFailure: resolve,
        }),
      );
    });
    if (rawData?.rawLink) link = rawData.rawLink;

    Object.entries(SHARING_URL_MAPPER).forEach(([pattern, to]) => {
      link = link.replace(pattern, to);
    });
    return link;
  });

  if (rawLink.includes('/tai-khoan/don-hang')) res.cookie('order-viewer', true);
  return rawLink;
}

async function handleRequestMetadata(req, res) {
  res.locals.stats = {
    userCountry: getUserCountryLocation(req),
    ignoreGeoIp: shouldIgnoreGeoIP(),
    shouldLoadSDK: getPartnerName(req) !== 'lozi',
    version,
    resourceStats: globalThis.resourceStats,
  };
  res.locals.device = detectMobileDevice(req.headers['user-agent']);
  process.env.AGENT = res.locals.device;

  // Handle locales, for i18n
  res.locals.locale = iife(() => {
    res.locals.locale = req.cookies.locale || defaultLocale;
    if (req.query.hl && ['vi', 'en'].indexOf(req.query.hl) >= 0) {
      const locale = req.query.hl === 'vi' ? 'vi_VN' : 'en_US';
      res.cookie('locale', locale);
      res.locals.locale = locale;
    }
    globalThis.locale = res.locals.locale;
  });
  await loadLanguage();

  // Helper functions
  function shouldIgnoreGeoIP() {
    const WHITELIST_URL = ['/payment'];
    return WHITELIST_URL.some((path) => req.url.includes(path)) || isWebBotClient(req.headers['user-agent']);
  }

  function getUserCountryLocation() {
    if (globalThis.flags.foreign && process.env.NODE_ENV === 'production') {
      const ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      if (!isValidIpAddress(ipAddress)) return 'foreign';
    }
    return 'Vietnam';
  }
}

async function handleGlobalConfigs(res) {
  const store = getServerStore(res.locals.initialServerState);
  globalThis.availableAreas = await new Promise((resolve) => {
    const dataCached = cache.get('data', { availableAreas: true });
    if (dataCached) return resolve(dataCached);

    store.dispatch(
      fetchGlobalConfigs({
        callback: (cRes) => {
          const dataForCache = (() => {
            try {
              return cRes.body.data.shippingAdministrations || [];
            } catch (e) {
              return [];
            }
          })();
          cache.insert('data', { availableAreas: true }, dataForCache);
          resolve(dataForCache);
        },
        callbackFailure: () => {
          resolve([]);
        },
      }),
    );
  });
  res.locals.initialServerState = store.getState();
}

async function handleRequestFromPartner(req, res) {
  const PARTNER_URL = ['/g/momo', '/g/auth', '/g/viettelpay'];
  const isPartner = PARTNER_URL.some((item) => item === req.path);

  const PARTNER_AUTHEN_REQUEST = {
    momo: (query) => {
      const { client, phoneNumber, requestId, signature, username, newuser } = query;
      const link = LINK_API + injectParams('/partners/momopay/${phoneNumber}/verify', { phoneNumber });
      const content = { client, requestId, signature, newuser, username };
      const parsedQuery = { client, phoneNumber, requestId, signature, username, newuser };
      return [link, content, parsedQuery];
    },
    sacombankpay: (query) => {
      const { client, phoneNumber, requestId, signature, name, newuser = 0, lat, lng, fallbackUrl } = query;
      const link = LINK_API + injectParams('/partners/sacombankpay/verify/${phoneNumber}', { phoneNumber });
      const content = { client, requestId, signature, name, newuser, phoneNumber };
      const parsedQuery = { client, phoneNumber, requestId, signature, name, newuser, lat, lng, fallbackUrl };
      return [link, content, parsedQuery];
    },
    viettelpay: (query) => {
      const { merchant_code, msisdn, time, check_sum, lat, lng } = query;
      const link = LINK_API + injectParams('/partners/viettelpay/verify/${phoneNumber}', { phoneNumber: msisdn });
      const content = { client: merchant_code, time, checksum: check_sum, name: msisdn, phoneNumber: msisdn };
      const parsedQuery = {
        client: merchant_code,
        phoneNumber: msisdn,
        name: msisdn,
        requestId: time,
        signature: check_sum,
        lat, lng,
      };
      return [link, content, parsedQuery];
    },
  };

  const result = {}; // partnerConfig | accessToken | status | redirectUrl
  const { accessToken: savedToken, partnerConfig: savedPartnerConfig } = req.cookies;

  if (typeof savedPartnerConfig !== 'undefined') {
    try {
      result.partnerConfig = JSON.parse(savedPartnerConfig);
    } catch (e) {
      console.error(Date.now(), 'ERROR: Failed to parse savedPartnerConfig');
    }
  }

  const parsedToken = await iife(async () => {
    if (getPartnerName(req) === 'lozi') {
      const decoded = jwt.verify(req.query.token);
      if (!decoded || !decoded.accessToken) return;
      return decodeURIComponent(decoded.accessToken);
    }

    // TODO: viettelpay inject errorCode to fallbackUrl
    if (isPartner) {
      const partnerName = getPartnerName(req);
      const [link, content, query] = PARTNER_AUTHEN_REQUEST[partnerName]?.(req.query) ?? [];
      const { signature, lat, lng, fallbackUrl } = query;

      const tokenRes = link && content && await request
        .post(link)
        .query(content)
        .end();
      if (tokenRes?.status !== 200) {
        result.status = 'FAILED';
        result.redirectUrl = fallbackUrl;
        return;
      }

      // Save partnerConfig to define the current environment. Max age: 30 minutes
      result.partnerConfig = { partner: partnerName, signature };
      res.cookie('partnerConfig', JSON.stringify(result.partnerConfig), { maxAge: 1800000, encode: String });

      if (lat && lng) {
        res.cookie('geoLocation', JSON.stringify({ latitude: lat, longitude: lng }), {
          encode: String,
        });
      }

      if (!tokenRes.body || !tokenRes.body.data) return;
      return tokenRes.body.data.accessToken;
    }

    return false;
  });

  if (parsedToken === false) {
    res.clearCookie('tempData');
    result.accessToken = savedToken;
  } else if (result.status === 'FAILED') {
    res.clearCookie('tempData');
    res.clearCookie('accessToken');
    res.clearCookie('partnerConfig');
    delete result.partnerConfig;
  } else {
    res.cookie('tempData', parsedToken, { maxAge: 31536000000, encode: String });
    res.cookie('accessToken', parsedToken, { maxAge: 31536000000, encode: String });
    result.accessToken = parsedToken;
  }

  return result;
}

function getServerParams(req, { partnerConfig, accessToken } = {}) {
  const serverParams = { accessToken };

  let serviceName = undefined;
  if (getPartnerName(req) === 'lozi') serviceName = 'lozi';
  else if (partnerConfig?.partner) {
    serviceName = partnerConfig.partner;
  }

  if (serviceName) {
    const service = loziServices.find((s) => s.name === serviceName);
    if (service) serverParams.additionalHeaders = { 'X-Loship-Service': service.appApiHeader };
  }

  return serverParams;
}

function isPartnerMomoOrderDetail(url) {
  return ['/g/momo/orders']
    .some((path) => url === path);
}

function isPartnerOrder(url) {
  return ['/g/viettelpay/order']
    .some((path) => url === path);
}

async function getMomoOrderDetail(req, res) {
  if (!isPartnerMomoOrderDetail(req.path)) return;

  const { loziMomoTransId, phoneNumber, token } = req.query;
  if (!loziMomoTransId || !phoneNumber || !token) {
    return res.json({
      reqUrl: req.path,
      query: req.query,
      error: 'Required params: loziMomoTransId, phoneNumber, token',
    });
  }

  const store = getServerStore(res.locals.initialServerState);
  await store.dispatch(
    fetchUserOrderDetailByMoMoTransID({ loziMomoTransId, phoneNumber, token }),
  );
  res.locals.initialServerState = store.getState();
}

async function getPartnerOrder(req, res) {
  if (!isPartnerOrder(req.path)) return;

  const { appTransId, phoneNumber, token } = req.query;
  const partnerName = getPartnerName(req);
  // if (!appTransId || !phoneNumber || !token) {
  //   return res.json({
  //     reqUrl: req.path,
  //     query: req.query,
  //     error: 'Required params: appTransId, phoneNumber, token',
  //   });
  // }

  const store = getServerStore(res.locals.initialServerState);
  await store.dispatch(
    fetchUserOrderDetailByAppTransId({ appTransId, partnerName, phoneNumber, token }),
  );
  res.locals.initialServerState = store.getState();
}


export default async function preProcessMiddleware(req, res, next) {
  const originalUrl = req.baseUrl + req.path;
  globalThis.originalUrl = originalUrl;
  const matchedServer = matchRoutes(serverRoutes, originalUrl);
  const { match: matchServerRoute } = matchedServer[matchedServer.length - 1] || {};

  // '/*' mean web app route
  if (matchServerRoute?.path === '/*') {
    const fullUrl = req.protocol + '://' + req.header('host') + originalUrl + qs.stringify(req.query, { addQueryPrefix: true });
    console.debug(moment().format('HH:mm:ss DD/MM/YYYY') + ':', fullUrl);

    // Handle sharing url. Redirect immediately without other computation
    const sharingUrl = await handleSharingUrl(req, res, originalUrl);
    if (sharingUrl) return res.redirect(308, sharingUrl);

    // Define locals data for current request
    const userAccessData = await handleRequestFromPartner(req, res);
    if (userAccessData.status === 'FAILED' && userAccessData.redirectUrl) {
      return res.render('navigate', { fallbackUrl: userAccessData.redirectUrl });
    }

    await handleRequestMetadata(req, res);
    await handleGlobalConfigs(res);

    await getMomoOrderDetail(req, res);
    await getPartnerOrder(req, res);

    res.locals.partnerConfig = userAccessData.partnerConfig;
    res.locals.accessToken = userAccessData.accessToken;
    res.locals.serverParams = getServerParams(req, userAccessData);
  }

  return next();
}
