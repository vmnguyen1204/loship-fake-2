import { APP_INFO } from 'settings/variables';
import * as errorController from './controllers/error-reporting';
import * as gatewayController from './controllers/gateway';
import * as landingController from './controllers/landing';
import * as mainController from './controllers/main';
import * as partnersController from './controllers/partners';
import * as paymentController from './controllers/payment';
import * as staticController from './controllers/static';

function getVendorRoutes() {
  if (APP_INFO.serviceName === 'lomart') return [];
  return [
    {
      method: 'GET',
      path: '/payment/zp-gateway',
      handler: paymentController.handleZaloPayTransaction,
      enforcePre: true,
    },
    {
      method: 'GET',
      path: '/payment/gateway',
      handler: gatewayController.handleGateway,
      enforcePre: true,
    },
    {
      method: 'GET',
      path: '/partners',
      handler: partnersController.handleCashBag,
    },
    {
      method: 'GET',
      path: '/cskh/de-xuat-cua-hang',
      handler: landingController.cskhSuggestMerchantHandler,
    },
    {
      method: 'GET',
      path: '/cskh/de-xuat-tinh-nang',
      handler: landingController.cskhSuggestFeatureHandler,
    },
    {
      method: 'GET',
      path: '/d/danh-gia/:id',
      handler: landingController.communityDriverHandler,
    },
    {
      method: 'GET',
      path: '/cskh/de-xuat-losupply',
      handler: landingController.cskhSuggestLosupplyHandler,
    },
    {
      method: 'GET',
      path: '/mc/:username/menu',
      handler: landingController.merchantLandingMenuToolHandler,
    },
    {
      method: 'GET',
      path: '/auth/apple-id',
      handler: partnersController.handleAuthAppleId,
    },
    {
      method: 'POST',
      path: '/auth/apple-id',
      handler: partnersController.handleAuthAppleId,
    },
  ];
}

export default [
  {
    method: 'GET',
    path: '/service-worker.js',
    handler: staticController.serve,
  },
  {
    method: 'GET',
    path: '/manifest.json',
    handler: staticController.serve,
  },
  {
    method: 'GET',
    path: '/favicon.ico',
    handler: staticController.serve,
  },
  {
    method: 'GET',
    path: '/apple-touch-icon.png',
    handler: staticController.serve,
  },
  {
    method: 'GET',
    path: '/dist/*',
    handler: staticController.servePublic,
  },
  {
    method: 'GET',
    path: '/external/images/:imageUrl',
    handler: staticController.serveExternalImage,
  },
  {
    method: 'POST',
    path: '/er',
    handler: errorController.handleErrorReporting,
  },
  {
    method: 'GET',
    path: '/toggle',
    handler: staticController.handleToggle,
  },
  ...getVendorRoutes(),
  {
    method: 'GET',
    path: '/*',
    handler: mainController.delegate,
  },
];
