import NodeCache from 'node-cache';

const cache = new NodeCache();

const MILLISECONDS_IN_A_DAY = 24 * 60 * 60 * 1000;

export function get(type, keys) {
  const key = getKey(keys);
  return cache.get(`${type}::${key}`);
}

// TODO: cache specify time + determine to remove old cache
export function insert(type, keys, data) {
  const key = getKey(keys);
  cache.set(`${type}::${key}`, data, MILLISECONDS_IN_A_DAY);
}

export function remove(type, keys) {
  const key = getKey(keys);
  cache.del(`${type}::${key}`);
}

// TODO: cache flush type only if defined
export function flush() {
  cache.flushAll();
}

export function getKey(keys) {
  if (!keys) return '';

  const key = Object.entries(keys)
    .sort()
    .map(([k, v]) => {
      if (Array.isArray(v)) return v.length > 0 ? `${k}_${v.join('-')}` : '';
      return v ? `${k}_${v}` : '';
    })
    .filter(Boolean)
    .join('-');
  return key;
}
