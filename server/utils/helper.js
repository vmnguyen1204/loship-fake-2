import ipRangeVN from '../metadata/ip-range-vn.json';

const IP_MAX_VALUE = 256;
const IP_SEGMENT_LENGTH = 4;

function getIpValue(ipAddress) {
  const ipSegment = ipAddress.match(/([0-9]+)/g);
  return ipSegment.reduce((res, segment) => res * IP_MAX_VALUE + +segment, 0);
}

export function isValidIpAddress(ipAddress) {
  const ipSegment = ipAddress.match(/([0-9]+)/g);
  if (ipSegment.length !== IP_SEGMENT_LENGTH ||
    ipSegment.some((segment) => +segment < 0 ||
    +segment > IP_MAX_VALUE - 1)
  ) return false;

  const ipValue = getIpValue(ipAddress);
  const isValid = ipRangeVN.some((range) => {
    const [from, to] = range.map(getIpValue);
    return from <= ipValue && ipValue <= to;
  });
  if (!isValid) console.info(`${new Date(Date.now()).toLocaleString()}:`, ipAddress, 'REJECTED');

  return isValid;
}
