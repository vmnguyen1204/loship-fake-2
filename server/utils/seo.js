import { LINK_DOMAIN_LOSHIP } from 'settings/variables';

const metaData = {
  title: 'Loship - Đặt đồ ăn là miễn phí ship',
  description: 'Đặt mua trà sữa trực tuyến và giao trà sữa tận nơi chỉ trong 1 giờ với Loship',
  keywords: 'loship, loship.vn, trà sữa ship, trà sữa giao tận nơi',
  image: 'https://loship.vn/dist/images/cover-loship-14052020.jpg',
  iosUrl: 'loshipuser://loship.vn/section?sectionId=1',
  androidUrl: 'loshipuser://loship.vn/section?sectionId=1',
  canonical: LINK_DOMAIN_LOSHIP,
};

const metaInfo = {
  siteName: 'LOSHIP',
  type: 'website',
  fbAppId: '600859170003466',
  iosName: 'Loship - Ship đồ ăn rất nhanh',
  iosStoreId: '1348206645',
  androidName: 'Loship - Ship đồ ăn rất nhanh',
  androidPackage: 'lozi.loship_user',
};

export const metaInfoLoshipDriver = {
  siteName: 'LOSHIP',
  type: 'website',
  fbAppId: '600859170003466',
  iosName: 'Loship - Chiến Binh',
  iosStoreId: '1335050744',
  androidName: 'Loship Chiến binh',
  androidPackage: 'lozi.loship_driver',
};

export const metaInfoLoshipMerchant = {
  siteName: 'LOSHIP',
  type: 'website',
  fbAppId: '600859170003466',
  iosName: 'Loship - Chủ cửa hàng',
  iosStoreId: '1377478517',
  androidName: 'Loship-Chủ cửa hàng',
  androidPackage: 'lozi.loship_merchant',
};

export function generateSEO(meta = {}, info = {}) {
  const fullMeta = { ...metaData, ...meta };
  const fullInfo = { ...metaInfo, ...info };

  const seoTitle = `<title>${fullMeta.title}</title>`;
  const seoMeta = `<meta name="description" content="${fullMeta.description}">
    <meta name="keywords" content="${fullMeta.keywords}">
    <meta property="og:title" content="${fullMeta.title}">
    <meta property="og:description"
      content="${fullMeta.description}">
    <meta property="og:url" content="${fullMeta.canonical}">
    <meta property="og:image" content="${fullMeta.image}">
    <meta property="og:type" content="${fullInfo.type}">
    <meta property="og:site_name" content="${fullInfo.siteName}">
    <meta property="fb:app_id" content="${fullInfo.fbAppId}">
    <meta name="robots" content="index,follow">
    <meta property="al:ios:url"
      content="${fullMeta.iosUrl}&amp;source=mweb">
    <meta property="al:android:url"
      content="${fullMeta.androidUrl}&amp;source=mweb">
    <meta property="al:ios:app_name" content="${fullInfo.iosName}">
    <meta property="al:ios:app_store_id" content="${fullInfo.iosStoreId}">
    <meta property="al:android:app_name" content="${fullInfo.androidName}">
    <meta property="al:android:package" content="${fullInfo.androidPackage}">
  `;
  const seoLink = `<link rel="canonical" href="${fullMeta.canonical}">`;
  return { title: seoTitle, meta: seoMeta, link: seoLink };
}
