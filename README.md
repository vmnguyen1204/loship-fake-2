# INTRODUCTION

Loship is a service to buy and ship milk tea for you within 1 hour.

> Loship.vn joined the market with the ambition to bring users a new experience in using home delivery services.

# PREREQUISITES

- NodeJS v12.10.0
- Yarn v1.17.3 (You could use NPM instead, but Yarn is preferred for fast package intallation)
- Add `lozi.local` as an alias of `localhost` (or `127.0.0.1`))

## Clone project

```
$ git clone git@git.lozi.space:lozi/loship.git
$ cd loship
```

- Create `site/app/settings/variables.js` with the right API base path for the environment. Example is given in the same directory.
- Create `site/server/settings/variables.js` with the right map API keys for the environment. Example is given in the same directory.

## Fetch dependencies

Run command `yarn install`

# DEVELOPMENT

- Run command `yarn dev`
- If you want to change port (default is 4000), run the following command:

```
DEV_PORT=xyz yarn dev
```

> Please note that running in different port might cause API not working, due to CORS.

Open `http://lozi.local:4000` in browser (or with your custom port)..

## HTTPS on development

> Supported OS: macOS, Windows (with WSL)

**Step 0: Create root folder for storing local certificates**

```
$ mkdir /etc/ssl/localhost
$ mkdir /mnt/c/ssl (for Windows only)
```

**Step 1: Create a certificate generator script**

Save following file content to `/etc/ssl/localhost/localhost-ssl.sh`:

> You need to remove some content in below script, which are not for your OS.

```
#!/bin/bash

if [ "$#" -ne 1 ]
then
  echo "You must supply a domain..."
  exit 1
fi

DOMAIN=$1
WINDOWS: {{CERT_DIR=/mnt/c/ssl}}
MACOS: {{CERT_DIR=/etc/ssl/localhost}}

WINDOWS: {{
cat > $CERT_DIR/$DOMAIN.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = $DOMAIN
EOF
}}

MACOS: {{
cat > $CERT_DIR/$DOMAIN.ext << EOF
[req]
default_bits = 1024
distinguished_name = req_distinguished_name
req_extensions = v3_req

[req_distinguished_name]

[v3_req]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = $DOMAIN
EOF
}}

openssl genrsa -out $CERT_DIR/$DOMAIN.key 2048

WINDOWS: {{
openssl req -new -key $CERT_DIR/$DOMAIN.key -out $CERT_DIR/$DOMAIN.csr
openssl x509 -req -in $CERT_DIR/$DOMAIN.csr -CA $CERT_DIR/localhost.pem -CAkey $CERT_DIR/localhost.key -CAcreateserial -out $CERT_DIR/$DOMAIN.crt -days 3650 -sha256 -extfile $CERT_DIR/$DOMAIN.ext
}}

MACOS: {{
openssl rsa -in $CERT_DIR/$DOMAIN.key -out $CERT_DIR/$DOMAIN.key.rsa
openssl req -new -key $CERT_DIR/$DOMAIN.key.rsa -subj /CN=$DOMAIN -out $CERT_DIR/$DOMAIN.csr -config $CERT_DIR/$DOMAIN.ext
openssl x509 -req -extensions v3_req -in $CERT_DIR/$DOMAIN.csr -signkey $CERT_DIR/$DOMAIN.key.rsa -out $CERT_DIR/$DOMAIN.crt -days 3650 -extfile $CERT_DIR/$DOMAIN.ext
}}

rm $CERT_DIR/$DOMAIN.csr
rm $CERT_DIR/$DOMAIN.ext
```

Save the file, and make it executable:

```
$ chmod u+x /etc/ssl/localhost/localhost-ssl.sh
```

Finally, move the file to `bin` for executable:

```
$ mv /etc/ssl/localhost/localhost-ssl.sh /usr/local/bin/localhost-ssl.sh
```

**Step 2: Install certificate to OS Certificate Manager**

PREREQUISITE (Window only): Generate a CA (Certificate Authority) key:

```
$ openssl genrsa -out /etc/ssl/localhost/localhost.key 2048 (1)
$ openssl req -x509 -new -nodes -key /etc/ssl/localhost/localhost.key -sha256 -days 3650 -out /etc/ssl/localhost/localhost.pem (2)
```

> Note that (1) command required to create new password, then you will be asked when fire command (2).

- Copy pem / key files to `$CERT_DIR` (in step 1):

```
cp /etc/ssl/localhost/localhost.pem /mnt/c/ssl/localhost.pem
cp /etc/ssl/localhost/localhost.key /mnt/c/ssl/localhost.key
```

---

Run the script to generate certificate for `lozi.local`:

```
$ localhost-ssl.sh lozi.local
```

On Windows:

- Launch "Manage Computer Certificates" -> "Trusted Root Certification Authorities" -> "Certificates".
- Right click on "Certificates" -> "All Task" -> "Import".
- Find and import `localhost.pem` inside `$CERT_DIR` (`C:/ssl/localhost.pem`).
- Proceed and complete.

On macOS: Simply run following command:

```
$ security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain /etc/ssl/localhost/lozi.local.crt
```

**Step 4: Use newly created certificate**

Create file .env (root project dir)

For Windows

```
HTTPS=true
HTTPS_KEY=/mnt/c/ssl/lozi.local.key
HTTPS_CERT=/mnt/c/ssl/lozi.local.crt
```

For macOS

```
HTTPS=true
HTTPS_KEY=/etc/ssl/localhost/lozi.local.key
HTTPS_CERT=/etc/ssl/localhost/lozi.local.crt
```

# PRODUCTION

- Install global NPM package

```
yarn global add pm2
```

- Run command `yarn start`.
- If you want to change port (default is 5566), run the following command:

```
PORT=xyz yarn start
```

> Default PROC_NAME: loship-prod

# Step to Launch new Lo-service

- Define new APP_INFO inside `variables.js`.
- Define routes inside `getVendorRoutes` of `site/app/routes.js`.
- Define SEO inside `utils/seo`.
- Define Navbar items (`components/shared/navbar`).
- Features have already configured for Lomart only: Home, Search.
- Features for Loship only: configured inside `routes.js`

> Basically, newly created service will use Loship's configuration -> no crash.
