import './loader';

import Spinner from 'components/statics/spinner';
import store from 'lib/configureStore';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { LINK_LOSHIP_LANDING } from 'settings/variables';
import { loadLanguage } from 'utils/i18n';

if (process.env.BROWSER) {
  require('assets/lzFonts.font');
  require('assets/styles/base/_base.scss');
  require('assets/styles/pages/index.scss');
}

const shouldRedirectToLanding = window.__GEO_COUNTRY__ !== 'Vietnam' && !window.__IGNORE_GEOIP__;

function renderApp() {
  const App = require('./app').default;
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('body'),
  );
}
function renderSpinner() {
  ReactDOM.render(
    <div className="async-loading-container">
      <Spinner type="wave" />
    </div>,
    document.getElementById('body'),
  );
}

if (shouldRedirectToLanding) {
  renderSpinner();
  window.location.replace(LINK_LOSHIP_LANDING);
} else {
  loadLanguage().then(renderApp);
}

if (module.hot) module.hot.accept();
