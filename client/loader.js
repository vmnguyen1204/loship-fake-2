import { loadSDKs, loadSentry } from 'actions/factory';
import { enableES5 } from 'immer';
import { LINK_API } from 'settings/variables';

if (window.__LOAD_SDK__) loadSDKs();

if (process.env.NODE_ENV === 'production') {
  loadSentry();
  enableES5();
  window.globalThis = require('globalthis')();
} else {
  console.info(
    '%c%s',
    'color: #0897ee; font-size: 18px; font-weight: bold;',
    'Đây là Loship! Nếu code không chạy thì 99% là code nhầm project mẹ rồi <3',
  );

  if (LINK_API.includes('mocha.lozi.vn')) {
    console.info(
      '%c%s',
      'color: #f7001e; font-size: 18px; font-weight: bold;',
      'Đang chạy API prod nhé babe, cẩn thận không vỡ mồm <3',
    );
  }
}
