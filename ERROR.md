# Rules set

| Code          | Explanation |
| ---           | ----------- |
| default-case  | Require all `switch` statements have a `default` case. [View more](https://eslint.org/docs/rules/default-case) |
| eqeqeq        | It is considered good practice to use the type-safe equality operators === and !== instead of their regular counterparts == and !=. [View more](https://eslint.org/docs/rules/eqeqeq) |
