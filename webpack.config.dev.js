require('dotenv').config();
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const DEV_PORT = process.env.DEV_PORT || 4000;
const PORT = parseInt(process.env.PORT || DEV_PORT) + 1;
const TESTING = !!process.env.BUNDLE_TEST;
const LEGACY_WATCH = !!process.env.POLLING;
const USE_HTTPS = !!process.env.HTTPS;
const LEGACY_SUPPORT = !!process.env.LEGACY_SUPPORT;

const config = {
  mode: 'development',
  devtool: 'eval',
  entry: { bundle: ['webpack-dev-server/client?http://lozi.local:' + DEV_PORT + '/', './client'] },
  output: {
    path: path.join(__dirname, 'dist'),
    pathinfo: false,
    filename: '[name].js',
    publicPath: '/dist/',
  },
  resolve: {
    modules: ['node_modules', 'app'],
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: LEGACY_SUPPORT ? [
          'babel-loader?cacheDirectory',
        ] : [
          {
            loader: '@sucrase/webpack-loader',
            options: { transforms: ['jsx'] },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          // {
          //   loader: 'postcss-loader',
          //   options: { plugins: () => [autoprefixer] },
          // },
          { loader: 'sass-loader' },
        ],
      },
      {
        test: /\.(png|jpg|svg|gif)$/,
        use: ['file-loader'],
      },
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        use: ['file-loader?name=fonts/[name].[ext]'],
      },
      {
        test: /\.font.(js|json)$/,
        use: ['style-loader', 'css-loader', 'webfonts-loader'],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        BROWSER: JSON.stringify(true),
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
      },
    }),
    TESTING && new BundleAnalyzerPlugin(),
  ].filter((plugin) => !!plugin),
  devServer: {
    proxy: { '*': 'http://127.0.0.1:' + PORT },
    allowedHosts: ['lozi.local'],
    hot: true,
    https: USE_HTTPS
      ? {
        key: fs.readFileSync(process.env.HTTPS_KEY),
        cert: fs.readFileSync(process.env.HTTPS_CERT),
      }
      : false,
    clientLogLevel: 'silent',
    stats: 'minimal',
    ...(LEGACY_WATCH && {
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
      },
    }),
  },
};

exports.default = TESTING ? new SpeedMeasurePlugin().wrap(config) : config;
